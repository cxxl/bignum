﻿
The BIGNUM Library for Multiple Precision Arithmetic
Copyright 2015, 2023 Christoph Lüders
Version 7.62, 2023-11-29


This archive contains the source files of BIGNUM, a library for multiple
precision arithmetic.  So far, the main purpose of this library is to 
implement fast multiplication.  It was used for my diploma thesis [1]
and for a subsequent paper for the ISSAC conference [2].

The source code is available under the GNU Lesser General Public License,
see COPYING.txt and COPYING.LESSER.txt.

BIGNUM features fast implementations of 
   addition,
   substraction,
   some logical bit-operations,
   grade-school multiplication,
   Karatsuba multiplication,
   Toom-Cook 3-way multiplication,
   Modular FFT-based multiplication,
   Schönhage-Strassen multiplication,
   DKSS multiplication.

Right now, the full source code of the arithmetic functions is included,
but it does not compile, since some header files are still missing.
I am working on that.

Also included is a compiled x64 version of my Lucas-Lehmer Mersenne prime 
tester in lltest\lltest.exe.  It has a lot of overly cryptic options for 
benchmarking.  I used it to make the speed tests for [1] and [2].  Have a 
look at lltest\main.cpp to find out more.

The code is written for Visual Studio 2012 and is much faster in x64 mode
(factor of 4?), but will run in 32-bit mode as well.  Detailed technical
information can be found in [1, Appendix A] and [2, sec. 3.2].

I can be contacted via email as:
   chris at cfos dot de 
Furthermore, I have a blog:
   http://cxxl.de 
The latest version of BIGNUM is available under:
   http://cxxl.de/bignum 


[1] http://cxxl.de/fast-multiplication-of-large-integers-implementation-and-analysis-of-the-dkss-algorithm/
[2] http://dx.doi.org/10.1145/2755996.2756643
