// $+HEADER$
// 
// Copyright 2015 Christoph Lueders
// 
// This file is part of the BIGNUM Library.
// 
// The BIGNUM Library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
// 
// The BIGNUM Library is distributed in the hope that it will be useful, but 
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public 
// License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with the BIGNUM Library.  If not, see <http://www.gnu.org/licenses/>.
// 
// $-HEADER$

// $Date: 2015-04-24 11:31:21 +0200 (Fr, 24 Apr 2015) $
// $Rev: 26591 $

#include "cl/hrtime.h"
#include "cl/vio.h"
#include "cl/format.h"
#include "../bignum/impl.h"
#include "../bignum/smul.h"
#include "../bignum/dkss.h"
#include "cl/str_conv.h"
#include "cl/str_trim.h"
#include "cl/filestrm.h"
#include "cl/exehdr.h"
#include "cl/timefunc.h"
#include "cl/crc.h"
#include "cl/tracer.h"
#include "cl/filestrm.h"
#include "cl/memzero.h"
#include "cl/lcm.h"
#include "cl/cpu.h"
#include "cl/proc_mgmt.h"

// rpcndr.h defines small
#undef small

using namespace cl;
using namespace std;
using namespace bignum;


#ifdef _M_X64
#define ARCH "x64"
#define ARCH_EXT "64"
#else
#define ARCH "x86"
#define ARCH_EXT ""
#endif

#define VERSION "7.61"
const unsigned num_ver = 76;

extern const char svn_revision[];

const unsigned DB_FLAG_64BIT = 2;
const unsigned DB_FLAG_RELEASE = 4;
const unsigned DB_FLAG_DEBUG = 8;
const unsigned DB_FLAG_KSQU  = 0x10;
const unsigned DB_FLAG_T3SQU = 0x20;
const unsigned DB_FLAG_SSQU  = 0x40;
const unsigned DB_FLAG_32BIT = 0x80;
const unsigned DB_FLAG_RESTARTED = 0x100;
const unsigned DB_FLAG_NOT_RESTARTED = 0x200;
const unsigned DB_FLAG_QSQU  = 0x400;
const unsigned DB_FLAG_DKSS_SQU = 0x800;

function<void()> break_func = nullptr;
uint32 idle_timeout = UINT_MAX;
unsigned nice_perc = 0;

namespace bignum {
   void _CL_CALL test_bignum();
   void _CL_CALL test_calibrate();

   dword _CL_CALL bignum_selftest();
}


// ------ eta calculation

class eta_tracker {
   hr_time& hrt;
   vector<pair<uint64,uint64>> snapshots;
   uint64 total_iterations;
   uint64 start_ticks;
   uint64 end_ticks;
   uint64 last_ticks;
   const uint64 hrtime_halfsec1;
   unsigned keep_snapshots;
   uint64 snapshot_ticks;
   //uint64 first_snapshot_ticks;
public:
   eta_tracker(hr_time& _hrt=hrtime) : hrt(_hrt), start_ticks(0), end_ticks(0), last_ticks(0),
      hrtime_halfsec1(hrt.from_msec(500)-1) { }
   void start(const uint64 _total_iterations, const unsigned estimation_base_time, const unsigned _keep_snapshots) {
      clear_snapshots();
      start_ticks = hrt.get();
      total_iterations = _total_iterations;
      keep_snapshots = _keep_snapshots;
      snapshot_ticks = hrt.from_msec(estimation_base_time / _keep_snapshots);
      //first_snapshot_ticks = sqrt(snapshot_ticks);
   }
   bool snapshot(const uint64 iteration) {
      classert(start_ticks != 0);
      last_ticks = hrt.get();
      if (snapshots.empty() || last_ticks - snapshots.back().second >= (/*snapshots.size() <= 1 ? first_snapshot_ticks :*/ snapshot_ticks)) {
         if (snapshots.size() >= keep_snapshots) snapshots.erase(snapshots.begin(), snapshots.begin() + snapshots.size() - keep_snapshots + 1);
         snapshots.push_back(make_pair(iteration, last_ticks));
         classert(snapshots.size() <= keep_snapshots);
         return true;
      }
      return false;
   }
   void clear_snapshots() {
      snapshots.clear();
   }
   int64 eta_ticks(const uint64 now_iteration, const uint64 now_ticks) const {  // calculate estimated time to arrival in ticks based on first snapshot and the arguments provided
      if (snapshots.size() < 1) return -1;
      const pair<uint64,uint64>& first = snapshots.front();
      const int64 iter_diff = now_iteration - first.first;
      if (iter_diff <= 0) return -1;
      const int64 tick_diff = now_ticks - first.second;
      if (tick_diff <= 0) return -1;
      const uint64 iter_to_go = total_iterations - now_iteration;
#ifdef NTDDK
#error handle floating point usage!
#endif
      return ((double)tick_diff * iter_to_go + iter_diff - 1) / iter_diff;
   }
   int64 eta_ticks() const {  // calculate estimated time to arrival in ticks based on first and last snapshot
      const pair<uint64,uint64>& last = snapshots.back();
      return eta_ticks(last.first, last.second);
   }
   int64 eta_secs(const uint64 now_iteration, const uint64 now_ticks) const {  // calculate estimated time to arrival in seconds based on first and last snapshot
      const int64 ticks = eta_ticks(now_iteration, now_ticks);
      return ticks == -1 ? -1i64 : hrt.to_sec(ticks + hrtime_halfsec1);  // round
   }
   int64 eta_secs() const {  // calculate estimated time to arrival in seconds based on first and last snapshot
      const int64 ticks = eta_ticks();
      return ticks == -1 ? -1i64 : hrt.to_sec(ticks + hrtime_halfsec1);  // round
   }
   int64 ett_ticks(const uint64 now_iteration, const uint64 now_ticks) const {  // calculate estimated total time in ticks based on first and last snapshot
      const int64 ticks = eta_ticks(now_iteration, now_ticks);
      if (ticks == -1) return -1;
      const uint64 tick_diff = now_ticks - start_ticks;
      return tick_diff + ticks;
   }
   int64 ett_ticks() const {  // calculate estimated total time in ticks based on first and last snapshot
      const pair<uint64,uint64>& last = snapshots.back();
      return ett_ticks(last.first, last.second);
   }
   int64 ett_secs(const uint64 now_iteration, const uint64 now_ticks) const {  // calculate estimated total time in seconds based on first and last snapshot
      const int64 ticks = ett_ticks(now_iteration, now_ticks);
      return ticks == -1 ? -1i64 : hrt.to_sec(ticks + hrtime_halfsec1);  // round
   }
   int64 ett_secs() const {  // calculate estimated total time in seconds based on first and last snapshot
      const int64 ticks = ett_ticks();
      return ticks == -1 ? -1i64 : hrt.to_sec(ticks + hrtime_halfsec1);  // round
   }
   int64 run_secs() const {  // calculate running time up to now
      const int64 ticks = last_ticks - start_ticks;
      return hrt.to_sec(ticks + hrtime_halfsec1);  // round
   }
   bool has_snapshots() const {
      return !snapshots.empty();
   }
   uint64 get_last_tick() const {
      return last_ticks;
   }
   void stop() {
      end_ticks = hrt.get();
   }
   uint64 total_tick_diff() const {
      return end_ticks - start_ticks;
   }
   int64 iterations_per_msecs(const unsigned msecs) {  // actual iterations done in the last
      if (snapshots.size() < 2) return -1;
      const pair<uint64,uint64>& last = snapshots.back();
      const pair<uint64,uint64>& second_last = snapshots[snapshots.size()-2];
      const uint64 tick_diff = last.second - second_last.second;
      if (tick_diff == 0) return -1;
      const uint64 iter_diff = last.first - second_last.first;
      return iter_diff * hrt.from_msec(msecs) / tick_diff;
   }
};

#ifdef CL_TEST_CODE
void test_eta_tracker() {
   hr_time myhrt(hrtime_style::fake);
   eta_tracker eta(myhrt);

   // 1 iterations needs ~ 0.1 secs = 1000000 fake ticks
   // fake timer runs with 10MHz
   myhrt.set_fake(1);
   classert(!eta.has_snapshots());
   // start: ticks=1
   eta.start(1000, 10000, 10);
   // ticks=2: iteration=1
   eta.snapshot(1);

   // ticks=1000000: iteration=2
   myhrt.set_fake(1000000);
   bool r = eta.snapshot(2);
   classert(r == false);
   int64 t = eta.eta_secs();
   classert(t == -1);  // second snapshot wasn't saved
   t = eta.eta_secs(2, eta.get_last_tick());
   classert(t == 100);
   t = eta.ett_secs();
   classert(t == -1);
   t = eta.ett_secs(2, eta.get_last_tick());
   classert(t == 100);
   int64 steps = eta.iterations_per_msecs(2000);
   classert(steps == -1);

   // ticks=11000000: iteration=12
   myhrt.set_fake(11000000);
   r = eta.snapshot(12);
   classert(r == true);
   t = eta.eta_secs();
   classert(t == 99);
   t = eta.ett_secs();
   classert(t == 100);
   steps = eta.iterations_per_msecs(2000);
   classert(steps == 20);
}
#endif


// ------ speed test ----------------------------------------------------------

const char save_signature[32] = "lltest save file v1!";

struct save_header {
   char signature[32];
   uint32 p;
   uint32 iteration;
   uint32 bytes;
   uint32 _filler;
};

template<typename small>
class myll : public lucas_lehmer<small> {
protected:
   eta_tracker eta;
   uint64 memory;
   string mem_str;
   index steps;
   index next_step;
   size_t olde;
   index last_ll;
   uint64 next_save;
   uint64 save_interval;  // in msec
   uint64 res;
   string md5;
   const char* window_title;
   const wstring worker_name;
   wstring savefile_dir;
   const bool optimise;
   bool use_savefile;
   bool verbose;
   bool restarted;
   string ret_res;
   string ret_md;
   unsigned ret_flags;
   uint32 idle_timeout_msec;
   static const unsigned callback_time = 100;
   unsigned nice_perc;
   uint32 next_disp;
   enum mul_algos { omul, kmul, t3mul, smul, qmul, dkss_mul };
   mul_algos mul_algo;
   index (*mem_ptr)(const index alen, const index blen);
   const char* algo_name;

public:
   tracer& t;
   myll(tracer& _t, const char* _window_title, const wstring& _worker_name) : t(_t), last_ll(0), verbose(false), ret_flags(0), use_savefile(false),
      window_title(_window_title), worker_name(_worker_name), optimise(true), idle_timeout_msec(UINT_MAX), nice_perc(0) {
   }
   void set_savefile(const bool activate_savefile, const wstring& _savefile_dir=wstring()) {
      use_savefile = activate_savefile;
      savefile_dir = _savefile_dir;
      add_bslash(savefile_dir);
   }
   void print1(const string& s = string()) {
      out_puts(s);
      size_t e = s.size();
      while (e++ < olde) out_puts(" ");
      olde = s.size();
      out_puts("\r");
   }
   void set_params(const bool _verbose) {
      verbose = _verbose;
   }
   void set_nice_params(const uint32 _idle_timeout_sec, const unsigned _nice_perc) {
      idle_timeout_msec = _idle_timeout_sec * 1000;
      nice_perc = bound(_nice_perc, 0u, 100u);
   }
   bool run_iterations(const index _p, index i, small* buf, const index len, bool& finished) {
      p = _p;
      restarted = i != 2;  // needed for later
      const index start_iter = i;
      const index ll = (p + bits(small) - 1) / bits(small);  // length in small's
      steps = est_mul_rounds(ll, callback_time) / CL_IF_DEBUG(4, 1);  // first shot, until there is eta data
      //tout.puts(format("initial steps = %?\n") << steps);
      next_step = tmax<index>(ceil(log2(p) + 1), i);
      olde = 0;
      save_interval = 10*60*1000;  // 10 min
      next_save = hrtime.getm() + save_interval;
      next_disp = 0;

      ret_flags = 0;
      if (use_dkss_squ(ll)) {
         mul_algo = dkss_mul;
         algo_name = "dsqu";
         ret_flags = DB_FLAG_DKSS_SQU;
         mem_ptr = q_dkss_mul_mem<small, true>;
         squfunc = dkss_squ;
         steps /= 40;  // adjust for slow algo
      }
      else if (use_qsqu(ll)) {
         mul_algo = qmul;
         algo_name = "qsqu";
         ret_flags = DB_FLAG_QSQU;
         mem_ptr = omul_mem;  // dummy
         squfunc = qsqu;
         steps /= 4;  // adjust for slow algo
      }
      else if (use_ssqu(ll)) {
         mul_algo = smul;
         algo_name = "ssqu";
         ret_flags = DB_FLAG_SSQU;
         mem_ptr = smul_mem<small, true>;
         squfunc = ssqu;
      }
      else if (use_t3squ(ll)) {
         mul_algo = t3mul;
         algo_name = "t3squ";
         ret_flags = DB_FLAG_T3SQU;
         mem_ptr = t3mul_mem<small, true>;
         squfunc = t3squ;
      }
      else if (use_ksqu(ll)) {
         mul_algo = kmul;
         algo_name = "ksqu";
         ret_flags = DB_FLAG_KSQU;
         mem_ptr = kmul_mem<small, true>;
         squfunc = ksqu;
      }
      else {
         mul_algo = omul;
         algo_name = "osqu";
         mem_ptr = omul_mem;
         squfunc = osqu;
      }

      string s;
      strformat ss(s);
      if (optimise && mul_algo == smul && p > 50000 && ll != last_ll) {
         print1(format("[%?] optimizing...") << p);
         if (verbose) tout_puts("\n");
         smul_optimise<limb>(ll, true, verbose);
         last_ll = ll;
      }

      ss("[%?]") << p;
      print1(s);
      if (window_title) SetConsoleTitleA((string(window_title) + " " + s).c_str());
      memory = mem_ptr(ll, ll);
      mem_str = format(", %?b") << scaled_count(ll * sizeof(small), SCT_MAX4);
      if (memory > 0) mem_str += format(" + %?b mem") << scaled_count(memory, SCT_MAX4);
      eta.start(p, 1*60*1000, 12);   // last 1 min
      uint64 cycles = cyc.get_cycles();
      const bool r = lucas_lehmer<small>::run_iterations(p, i, buf, len, finished);
      cycles = cyc.get_cycles() - cycles;
      eta.stop();
      print1();
      if (window_title) SetConsoleTitleA(window_title);
      if (!finished) return false;

      s.clear();
      const uint64 diff = eta.total_tick_diff();
      const uint64 nsec = hrtime.to_nsec64(diff);
      ss("2^%?-1 %?, %,.6? sec (%?)") << p << (r ? "is prime" : "is composite") << nsec/1000 << algo_name;
      if (nsec > 10000) {
         const index bitlen = ll * bits(small);
         const double c = (double)cycles / (bitlen * (p - start_iter + 1));
         const double idx = xlog2(bitlen) * xlog2(xlog2(bitlen)) / c;
         ss(", %.2? cyc/bit, idx=%.3?") << c << idx;
      }

      ret_res = format("%016x") << res;
      ret_md = md5;
      ret_flags |=
         CL_IF_DEBUG(DB_FLAG_DEBUG, DB_FLAG_RELEASE) |
         CL_IF_X64(DB_FLAG_64BIT, DB_FLAG_32BIT) |
         (restarted ? DB_FLAG_RESTARTED : DB_FLAG_NOT_RESTARTED);

      t.puts(s);
      if (memory > 0) file_trc.puts(mem_str);
      file_trc.puts(format(", res=%? md=%? flags=%x") << ret_res << ret_md << ret_flags);
      switch (mul_algo) {
         case smul: {
            unsigned lg;
            index mm;
            index l;
            index k;
            const bool ok = smul_set_params<limb>(true, ll, ll, lg, mm, l, k);
            caassert(ok);
            file_trc.puts(format(" lg=%? k=%?") << lg << k/bits(small));
            break;
         }
         case dkss_mul: {
            dkss_parm<small> p;
            dkss_set_mmu<small>(p, ll, ll);
            file_trc.puts(format(" M=%? m=%? u=%? pc.bits=%?") << p.M << p.m << p.u << p.pc.bit_length());
            break;
         }
      }
      t.puts("\n");
      return r;
   }
   virtual bool callback(const index i, const small* buf, const index l) {
      if (i >= next_step) {
         if (!eta.has_snapshots()) {  // first call to callback out of the initial phase
            extern bool do_profiling;
            do_profiling = true;  // switch on smul profiling (if enabled in smul)
         }
         if (eta.snapshot(i)) {  // it did save a new sample
            const int64 estep = eta.iterations_per_msecs(callback_time);
            if (estep != -1) {
               steps = tmax<index>(estep, 1);
               //tout.puts(format("steps = %?  \n") << steps);
            }
         }

         const uint64 msec = hrtime.to_msec(eta.get_last_tick());
         if (use_savefile) {
            if (break_signalled || msec > next_save) {
               save_to_file(p, i, buf, l);
               next_save = msec + save_interval;
               if (break_signalled) {
                  t.puts(format("ctrl-c pressed: 2^%?-1 iteration %? intermediate result saved, exiting.\n") << p << i);
                  return false;
               }
            }
         }

         const unsigned sleep_msec = nice_perc > 0 && user_active() ? callback_time * nice_perc / 100 : 0;

         const int64 eta_secs = eta.eta_secs(i, eta.get_last_tick());
         if (eta_secs > 0 && eta.run_secs() >= 2 && msec > next_disp) {
            string s;
            strformat ss(s);
            ss("[%?] %,.2?%%") << p << 10000ui64 * i / p;
            if (window_title) SetConsoleTitleA((string(window_title) + " " + s).c_str());
            ss(" (%?), ETA %?") << algo_name << scaled_secs(eta_secs, SCS_MAX2);
            if (!restarted) {
               const int64 total = eta.ett_secs(i, eta.get_last_tick());
               ss(", total %?s") << total;
            }
            if (sleep_msec > 0) ss(" (nice %?)") << nice_perc;
            print1(s);
            next_disp = msec + 2000;
         }

         if (sleep_msec > 0) Sleep(sleep_msec);

         next_step = i + steps;
      }
      return true;
   }
   virtual void abort_req() {
      next_step = 0;
   }
   void done(const small* v, const index len) {
      caassert(len > 0);
      res = 0;
      memcpy(&res, v, tmin<size_t>(sizeof(res), len * sizeof(small)));
      crc_md5 md;
      md5out mdo;
      const byte pre[] = { 0xBC, 0x97, 0x77, 0x9F, 0x0B, 0x1C, 0xC0, 0x89 };
      md.updmem(pre, sizeof(pre));
      md.updmem(v, normu<byte>((const byte*)v, len * sizeof(small)));
      const byte post[] = { 0xFF, 0xA9, 0xB7, 0x4E, 0x4D, 0xBD, 0xDB, 0x44 };
      md.updmem(post, sizeof(post));
      md.final(mdo);
      md5 = "1-" + md.print();
   }
   const string& get_res() const {
      return ret_res;
   }
   const string& get_md() const {
      return ret_md;
   }
   unsigned get_flags() const {
      return ret_flags;
   }
   wstring save_name() const {
      return wformat(L"%?savefile-%?.lltest") << savefile_dir << worker_name;
   }
   wstring old_save_name() const {
      return wformat(L"%?savefile-%?.1.lltest") << savefile_dir << worker_name;
   }
   bool save_to_file(const index p, const index i, const small* buf, const index len) {
      const wstring name = save_name();
      const wstring old_name = old_save_name();
      if (file_exist(old_name) && uremove(old_name) != 0) terr.puts(wformat(L"failed to remove old save file \"%?\"\n") << old_name);
      else if (file_exist(name) && urename(name, old_name) != 0) terr.puts(wformat(L"failed to move save file \"%?\" to \"%?\"\n") << name << old_name);

      file_hnd f;
      if (!f.open(name, O_CREAT|O_TRUNC|O_WRONLY|_O_SEQUENTIAL, SH_DENYWR)) {
         terr.puts(wformat(L"failed to open save file \"%?\"\n") << name);
         return false;
      }

      // file is saved in intel byte order
      save_header h;
      memcpy(h.signature, save_signature, sizeof(save_signature));
      h.p = p;
      h.iteration = i;
      h.bytes = normu(buf, len)*sizeof(small);
      h._filler = 0;
      crc_32 crc;
      crc.updmem(&h, sizeof(h));
      crc.updmem(buf, h.bytes);
      uint32 crc32 = crc.get();
      f.put(h);
      f.write(buf, h.bytes);
      f.put(crc32);
      return true;
   }
   bool savefile_found() const {
      return file_exist(save_name()) || file_exist(old_save_name());
   }
   void del_savefiles() const {
      uremove(save_name());
      uremove(old_save_name());
   }
   // try to load from 'file'
   // if it's ok, return file data and remove file
   // if it's not ok, remove at least file
   bool load_from_file(const tracer& t, const wstring& file, index& p, index& i, small*& rbuf, index& rlen) {
      bool ok = false;
      file_hnd f;
      if (f.open(file, O_RDONLY|_O_SEQUENTIAL, SH_DENYWR)) {
         // support save files up to 1000m bits so far
         uint64 sz = f.filelength();
         if (sz > tmin<uint64>(1000000000 / 8 + 1000, numeric_limits<sindex>::max()))
            t.puts(wformat(L"file \"%?\" too long!\n") << file);
         else {
            byte* mem = new byte[sz + sizeof(small)];  // 1 extra small, so we have space for a zero
            const ptrdiff_t bytes = f.read(mem, sz);
            if (bytes <= sizeof(save_header))
               t.puts(wformat(L"file \"%?\" too short!\n") << file);
            else {
               const save_header& h = *(save_header*)mem;
               if (memcmp(h.signature, save_signature, sizeof(save_signature)) != 0)
                  t.puts(wformat(L"signature bad in file \"%?\"!\n") << file);
               else if (bytes < h.bytes + sizeof(save_header) + 4 || h.iteration > h.p)
                  t.puts(wformat(L"header data bad in file \"%?\"!\n") << file);
               else if (crc_32::crc(mem, bytes - 4) != *(uint32*)(mem + sizeof(save_header) + h.bytes))
                  t.puts(wformat(L"crc32 bad in file \"%?\"!\n") << file);
               else {
                  p = h.p;
                  i = h.iteration;
                  *(small*)(mem + sizeof(save_header) + h.bytes) = 0;  // make sure highest small is zero
                  rlen = normu((small*)(mem + sizeof(save_header)), (h.bytes + sizeof(small) - 1) / sizeof(small));
                  rbuf = bignum_alloc<small>(rlen);
                  memcpy(rbuf, (small*)(mem + sizeof(save_header)), rlen*sizeof(small));
                  ok = true;
                  t.puts(wformat(L"loaded 2^%?-1 iteration %? intermediate result.\n") << p << i);
               }
            }
            delete[] mem;
         }
      }
      return ok;
   }
   bool load_save_file(const tracer& t, index& p, index& i, small*& rbuf, index& rlen) {
      const wstring name = save_name();
      if (load_from_file(t, name, p, i, rbuf, rlen)) return true;
      const wstring old_name = old_save_name();
      if (load_from_file(t, old_name, p, i, rbuf, rlen)) return true;
      return false;
   }
   bool user_active() {
      LASTINPUTINFO lii;
      lii.cbSize = sizeof(LASTINPUTINFO);
      if (!GetLastInputInfo(&lii)) return true;
      const int32 diff = GetTickCount() - lii.dwTime;
      if (diff < 0) return false;  // user away for 24.8 days or more
      return diff <= idle_timeout_msec;
   }
};

#ifdef CL_TEST_CODE
template<class small>
class test_ll : public myll<small> {
public:
   bool do_save;
   test_ll(tracer& t, const char* window_title, const wstring& worker_name) : myll(t, window_title, worker_name) { }
   virtual bool callback(const index iter, const small* buf, const index l) {
      if (do_save && iter == p/2) {
         save_to_file(p, iter, buf, l);
         return false;
      }
      return true;
   }
};

template<class small>
class test_ll2 : public myll<small> {
public:
   bool do_save;
   small res;
   test_ll2(tracer& t, const char* window_title, const wstring& worker_name) : myll(t, window_title, worker_name) { }
   bool run_iterations(const index _p, index i, small* buf, const index len, bool& finished) {
      do_save = false;
      return lucas_lehmer<small>::run_iterations(p, i, buf, len, finished);
   }
   virtual bool callback(const index iter, const small* buf, const index l) {
      //if (buf[l-1] == 0) tout_puts(format("iter=%?\n") << iter);
      if (do_save) {
         switch (iter) {
            case 252:
            case 1388:
            case 2391:
            case 3338:
            case 4091:
            case 4733:
            case 5701:
               caassert(buf[l-1] == 0);
               save_to_file(p, iter, buf, l);
               //tout_puts(format("iter=%?\n") << iter);
               return false;
         }
      }
      do_save = true;
      return true;
   }
   virtual void done(const small* v, const index len) {
      res = v[0];
   }
};

static void test_myll() {
   tracer tempty;
   myll<limb> ll(tempty, nullptr, L"$testcode$");
   for (size_t i=0; ; ++i) {
      const unsigned p = mersenne_prime_exps[i];
      if (p == 0 || p > 3000) break;
      bool finished;
      bool r = ll.test(p, finished);
      classert(r);
   }

   ll.del_savefiles();
   myll<uint16> ll16(tempty, nullptr, L"$testcode$");  // same name, so it finds the other's savefile

   {
      // save native size (32 or 64 bits)
      index p = 0xffff;
      index iter = 1234;
      const index len = (p + 8*sizeof(limb) - 1) / 8 / sizeof(limb);
      tape_alloc<limb> tmp(len);
      for (index i=0; i<len; ++i) tmp.p[i] = (limb)(0x55aa00bada550000 + i);
      classert(ll.save_to_file(p, iter, tmp.p, len));

      // read native size
      index p2;
      index iter2;
      limb* buf2 = 0;
      index len2;
      classert(ll.load_save_file(tempty, p2, iter2, buf2, len2));

      classert(p == p2);
      classert(iter == iter2);
      classert(len == len2);
      //string s1 = dump_hex(tmp.p, len); tout.puts(s1 + "\n");
      //string s2 = dump_hex(buf2, len2); tout.puts(s2 + "\n");
      classert(compu_nn(tmp.p, len, buf2, len2) == 0);
      bignum_free(buf2);

      // read smaller size
      ctassert(sizeof(limb) > sizeof(uint16));
      index p3;
      index iter3;
      uint16* buf3 = 0;
      index len3;
      classert(ll16.load_save_file(tempty, p3, iter3, buf3, len3));

      classert(p == p3);
      classert(iter == iter3);
      //string s1 = dump_hex(tmp.p, len); tout.puts(s1 + "\n");
      //string s2 = dump_hex(buf2, len2); tout.puts(s2 + "\n");
      classert(compu_nn((uint16*)tmp.p, len*sizeof(limb)/sizeof(uint16), buf3, len3) == 0);
      bignum_free(buf3);
   }
   {
      // save smaller size
      index p4 = 0xffff;
      index iter4 = 1234;
      const index len4 = (p4 + 8*sizeof(uint16) - 1) / 8 / sizeof(uint16);
      tape_alloc<uint16> tmp4(len4);
      for (index i=0; i<len4; ++i) tmp4.p[i] = (uint16)(0x55aa00bada5500 + i);
      classert(ll16.save_to_file(p4, iter4, tmp4.p, len4));

      // read native size
      index p5;
      index iter5;
      limb* buf5 = 0;
      index len5;
      classert(ll.load_save_file(tempty, p5, iter5, buf5, len5));

      classert(p4 == p5);
      classert(iter4 == iter5);
      //string s1 = dump_hex(tmp.p, len); tout.puts(s1 + "\n");
      //string s2 = dump_hex(buf2, len2); tout.puts(s2 + "\n");
      classert(compu_nn(tmp4.p, len4, (uint16*)buf5, len5*sizeof(limb)/sizeof(uint16)) == 0);
      bignum_free(buf5);
   }
   {
      tracer tempty;
      test_ll<limb> ll(tempty, nullptr, L"$testcode$");
      ll.do_save = true;
      bool finished;
      ll.test(1279, finished);
      classert(!finished);
      index p;
      index i;
      limb* buf = 0;
      index len;
      ll.do_save = false;
      bool r = ll.load_save_file(tempty, p, i, buf, len);
      classert(r);
      bool prime = ll.run_iterations(p, i, buf, len, finished);
      classert(finished);
      classert(prime);
      bignum_free(buf);
   }
   if (sizeof(limb) == sizeof(uint32) || sizeof(limb) == sizeof(uint64)) {
      // 2^5953-1, upper word is often zero
      tracer tempty;
      test_ll2<limb> ll(tempty, nullptr, L"$testcode$");
      bool finished;
      ll.test(5953, finished);
      classert(!finished);
      do {
         index p;
         index iter;
         limb* buf = 0;
         index len;
         bool r = ll.load_save_file(tempty, p, iter, buf, len);
         classert(r);
         ll.run_iterations(p, iter, buf, len, finished);
         bignum_free(buf);
      } while (!finished);
      if (sizeof(limb) == sizeof(uint32)) classert(ll.res == 0x5474b531);
      if (sizeof(limb) == sizeof(uint64)) classert(ll.res == 0x2143cf295474b531);
   }

   ll.del_savefiles();
}
#endif

static void prime_lltests(uint64 from, uint64 to, const int verbose, const char* window_title, const wstring& worker_name, const bool use_savefile) {
   set_prio_class prio(IDLE_PRIORITY_CLASS);
   out_puts(system_info() + "\n");

   //tout_puts(format("test exponents from %? to %?\n") << from << to);

   myll<limb> ll(tout, window_title, worker_name);
   ll.set_savefile(use_savefile, working_directory);
   ll.set_params(verbose > 0);
   ll.set_nice_params(idle_timeout, nice_perc);
   for (size_t i=0; ; ++i) {
      const unsigned p = mersenne_prime_exps[i];
      if (p == 0) break;
      bool finished;
      if (p >= from && p <= to) {
         ll.test(p, finished);
         if (!finished) break;
         ll.del_savefiles();
      }
   }
}


static bool is_prime(const index n) {
   if (n < 4) return true;  // 0, 1, 2, 3 are prime
   if (n % 2 == 0 || n % 3 == 0) return false;
   const index to = sqrt((double)n);
   for (index i = 5; i <= to; i += 6)
      if (n % i == 0 || n % (i+2) == 0) return false;
   return true;
}

#ifdef CL_TEST_CODE
static void test_is_prime() {
   classert(is_prime(2));
   classert(is_prime(3));
   classert(is_prime(5));
   classert(is_prime(7));
   classert(is_prime(11));
   classert(is_prime(13));
   classert(is_prime(17));
   classert(is_prime(19));
   classert(is_prime(23));
   classert(is_prime(29));
   classert(is_prime(31));
   classert(is_prime(37));
   classert(is_prime(41));
   classert(is_prime(43));
   classert(is_prime(47));
   classert(is_prime(53));
   classert(is_prime(59));
   classert(is_prime(61));
   classert(is_prime(67));
   classert(is_prime(71));
   classert(is_prime(73));
   classert(is_prime(79));
   classert(is_prime(83));
   classert(is_prime(89));
   classert(is_prime(97));

   classert(!is_prime(4));
   classert(!is_prime(6));
   classert(!is_prime(8));
   classert(!is_prime(9));
   classert(!is_prime(10));
   classert(!is_prime(12));
   classert(!is_prime(14));
   classert(!is_prime(15));
   classert(!is_prime(16));
   classert(!is_prime(18));
   classert(!is_prime(20));
   classert(!is_prime(21));
   classert(!is_prime(22));
   classert(!is_prime(28));
   classert(!is_prime(30));
   classert(!is_prime(36));
   classert(!is_prime(40));
   classert(!is_prime(44));
   classert(!is_prime(48));
   classert(!is_prime(54));
   classert(!is_prime(60));
   classert(!is_prime(62));
   classert(!is_prime(68));
   classert(!is_prime(72));
   classert(!is_prime(74));
   classert(!is_prime(80));
   classert(!is_prime(82));
   classert(!is_prime(88));
   classert(!is_prime(96));
}
#endif

bool is_all_digits(const char* p, size_t len) {
   while (len-- > 0) {
      if (!is_digit(*p++)) return false;
   }
   return true;
}


wstring prime_db_name;
const index max_exp = 2000000;
#define SIG_STRING " lltest mersenne exponent database v5"

const size_t db_reserved_off = 0;
const size_t db_reserved_len = 1;
const size_t db_isprime_off = db_reserved_off + db_reserved_len + 1;
const size_t db_isprime_len = 1;
const size_t db_exp_off = db_isprime_off + db_isprime_len + 1;
const size_t db_exp_len = 9;
const size_t db_res_off = db_exp_off + db_exp_len + 1;
const size_t db_res_len = 16;
const size_t db_md_off = db_res_off + db_res_len + 1;
const size_t db_md_len = 34;
const size_t db_cnt_off = db_md_off + db_md_len + 1;
const size_t db_cnt_len = 3;
const size_t db_mis_cnt_off = db_cnt_off + db_cnt_len + 1;
const size_t db_mis_cnt_len = 3;
const size_t db_flags_off = db_mis_cnt_off + db_mis_cnt_len + 1;
const size_t db_flags_len = 3;
const size_t db_ver_off = db_flags_off + db_flags_len + 1;
const size_t db_ver_len = 3;
const size_t db_line_len = db_ver_off + db_ver_len;
ctassert(db_line_len == 81);

index do_exp(index in_exp, const bool in_status, const string& in_res, const string& in_md, const unsigned in_flags, const unsigned in_ver, const bool want_new,
   const uint64 from, const uint64 to, const unsigned check, const unsigned flag_mask, const unsigned flag_val)
{
   file_stream f;
   while (1) {
      if (!f.open(prime_db_name, O_CREAT|O_RDWR|_O_SEQUENTIAL, SH_DENYWR)) {
         if (f.sharing_violation()) {
            Sleep(lib_rand.get(100));
            continue;
         }
         terr_puts(wformat(L"could not open database \"%?\"\n") << prime_db_name);
         return 0;
      }
      break;
   }

   index new_exp = 0;
   index line = 0;
   //index high = 0;
   int64 fpos = 0;
   bool sig_ok = false;
   string s;
   while (fpos = f.tell(), f.getline(s)) {
      ++line;
      rtrim(s);
      if (s.size() == 0) continue;  // line empty

      bool free = false;
      bool tested = false;
      switch (s[db_reserved_off]) {
      case '.':  // untested
         free = true;
         break;
      case '+':  // being tested
         break;
      case '*':  // tested
         tested = true;
         break;
      case '#':  // comments
      case ';':
         continue;
      case '$':
         sig_ok = line == 1 && s == "$" SIG_STRING;
         continue;
      default:
         continue;
      }
      if (s.size() != db_line_len) continue;  // line has invalid length
      if (!is_space(s[1]) || !is_all_digits(&s[db_exp_off], db_exp_len)) continue;

      const index p = s_to<index>(&s[db_exp_off]);
      //high = tmax(high, p);
      if (sig_ok) {
         index cnt = s_to<index>(&s[db_cnt_off]);
         index mis_cnt = s_to<index>(&s[db_mis_cnt_off]);
         unsigned flags = s_to<16,unsigned>(&s[db_flags_off]);
         unsigned ver = s_to<unsigned>(&s[db_ver_off]);
         if (ver < 71 && cnt + mis_cnt > 1) flags = 0;  // version before 7.1 read flags field wrong
         if (in_exp == p) {
            f.seek(fpos);
#ifndef NDEBUG
            string s2;
            classert(f.getline(s2));
            classert(s2 == s);
            f.seek(fpos);
#endif
            classert(is_prime(p));
            bool status = s[db_isprime_off] == 'p';
            string res = s.substr(db_res_off, db_res_len);
            string md = s.substr(db_md_off, db_md_len);
            const char status_char = status ? 'p' : 'c';
            if (cnt == 0) {
               cnt = 1;
               mis_cnt = 0;
               flags = in_flags;
               status = in_status;
               res = in_res;
               md = in_md;
               ver = in_ver;
            }
            else {
               const char md_ver = md[0];
               const char in_md_ver = in_md[0];
               if (in_status == status && in_res == res && (md == in_md || md_ver != in_md_ver)) {
                  cnt = tmin<index>(999, cnt + 1);
                  flags |= in_flags;
                  if (in_ver >= ver) {
                     md = in_md;
                     ver = in_ver;
                  }
               }
               else {
                  const char in_status_char = in_status ? 'p' : 'c';
                  tout_puts(format(
                     "  2^%?-1: *** checksum mismatch! ***\n"
                     "  old: status=%c res=%? md=%?\n"
                     "  new: status=%c res=%? md=%?\n")
                     << p
                     << status_char << res << md
                     << in_status_char << in_res << in_md
                     );
                  mis_cnt = tmin<index>(999, mis_cnt + 1);
               }
            }
            const string s = format("* %c %0*? %0*? %*? %0*? %0*? %0*x %0*?")
               << (status ? 'p' : 'c')
               << db_exp_len << p
               << db_res_len << res
               << db_md_len << md
               << db_cnt_len << cnt
               << db_mis_cnt_len << mis_cnt
               << db_flags_len << flags
               << db_ver_len << ver;
            classert(s.size() == db_line_len);
            f.puts(s);
            in_exp = 0;
            if (new_exp != 0 || !want_new) return new_exp;
         }
         else if (want_new && new_exp == 0) {
            if (check && tested) {
               if (flag_mask == 0 ? (cnt + mis_cnt <= check) : ((flags & flag_mask) == flag_val)) free = true;
            }
            if (free) {
               if (p > to && in_exp == 0) return 0;
               if (p < from) continue;
               f.seek(fpos);
#ifndef NDEBUG
               string s2;
               classert(f.getline(s2));
               classert(s2 == s);
               f.seek(fpos);
#endif
               classert(is_prime(p));
               f.put('+');
               new_exp = p;
               if (in_exp == 0) return new_exp;
            }
         }
      }
   }

   if (line == 0) {
      // file was empty
      classert(in_exp == 0);
      tout_puts(wformat(L"creating new database \"%?\"\n") << prime_db_name);
      f.putline("$" SIG_STRING);
      for (index i=3; i<max_exp; ++i) {
         if (is_prime(i)) {
            const string s = format("%c ? %0*? rrrrrrrrrrrrrrrr M-mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm %0*? %0*? %0*x %0*?")
               << (i == 3 ? '+' : '.')
               << db_exp_len << i
               << db_cnt_len << 0
               << db_mis_cnt_len << 0
               << db_flags_len << 0
               << db_ver_len << 0;
            classert(s.size() == db_line_len);
            f.puts(s + "\r\n");
         }
      }
      return 3;  // first exponent
   }
   else {
      tout_puts(wformat(L"database \"%?\" has unknown format\n") << prime_db_name);
      return 0;
   }
}

extern volatile bool break_signalled;

static BOOL WINAPI ctrl_break(DWORD ctrl_type) {
   static unsigned pressed = 0;
   switch (ctrl_type) {
   case CTRL_C_EVENT:
      break_signalled = true;
      if (break_func != nullptr) break_func();
      else {
         ++pressed;
         switch (pressed) {
         case 1:
            out_puts("\n* ctrl-c pressed, program will exit after finishing this exponent *\n");
            break;
         case 2:
            out_puts("\n* as i said, the program will exit after finishing this exponent... *\n");
            break;
         case 3:
            out_puts("\n* please wait, i'm still working on it! *\n");
            break;
         case 4:
         default:
            out_puts("\n* will you fucking wait until i'm done?! *\n");
            break;
         }
      }
      return TRUE;
   }
   return FALSE;
}

static void all_lltests(uint64 from, const uint64 to, const int verbose, const unsigned check, const unsigned flag_mask, const unsigned flag_val,
      const char* window_title, const wstring& worker_name, const bool use_savefile)
{
   set_prio_class prio(IDLE_PRIORITY_CLASS);
   from = tmax<index>(from, 3);
   tout.puts(system_info() + "\n");
   tout_puts(format("%? exponents from %? to %?\n") << (check ? "double-checking" : "testing") << from << to);

   myll<limb> ll(tout, window_title, worker_name);
   ll.set_savefile(use_savefile, working_directory);
   ll.set_params(verbose > 0);
   ll.set_nice_params(idle_timeout, nice_perc);
   index exp = 0;
   bool status = false;
   string res;
   string md;
   unsigned flags = 0;
   // restart support
   index iter;
   index len;
   limb* buf = 0;
   bool finished = true;
   break_func = bind(&myll<limb>::abort_req, &ll);
   SetConsoleCtrlHandler(ctrl_break, TRUE);
   if (ll.load_save_file(tout, exp, iter, buf, len)) {
      status = ll.run_iterations(exp, iter, buf, len, finished);
      bignum_free(buf);
   }
   while (finished) {
      exp = do_exp(exp, status, ll.get_res(), ll.get_md(), ll.get_flags(), num_ver, !break_signalled, from, to, check, flag_mask, flag_val);
      ll.del_savefiles();
      if (exp == 0) break;
      status = ll.test(exp, finished);
   }
   break_func = nullptr;
   SetConsoleCtrlHandler(NULL, FALSE);
}


void show_exponents(int estimate) {
   out_puts(system_info() + "\n");

   uint64 diff = 0;
#ifdef _M_X64
   const index accu_p = 44497;
   const index fast_p = 23209;
#else
   const index accu_p = 19937;
   const index fast_p = 9689;
#endif
   const index testp = estimate > 2 ? accu_p : fast_p;
   if (estimate > 1) {
      tout_puts(format("timing 2^%?-1... ") << testp);
      const uint64 start = hrtime.get();
      lucas_lehmer_test<limb>(testp);
      diff = hrtime.to_usec64(hrtime.get() - start);
      tout_puts("done.\n\n");
   }
   const double fact = ((double)diff / testp) / estimate_smul_time((testp + bits(limb) - 1) / bits(limb));

   // see http://v5www.mersenne.org/report_milestones/
   const unsigned once_checked = 44457869;
   const unsigned double_checked = 26114633;

   string s;
   strformat ss(s);
   for (size_t i=0; ; ++i) {
      const unsigned p = mersenne_prime_exps[i];
      if (p == 0) break;
      const index ll = (p + bits(limb) - 1) / bits(limb);

      s.clear();
      ss("%2? %-2?   %9?") << i+2 << (p < double_checked ? "" : p < once_checked ? "?" : "??") << p;
      if (diff > 0) {
         const uint64 est = estimate_smul_time(ll) * fact * p;
         uint64 pest = est;
         unsigned s = 0;
         for (; s<6; ++s) {
            if (pest < 1000000) break;
            else pest /= 10;
         }
         ss("   %-9s") << (format("%#,.*?") << 6-s << pest);
         ss("   %-9s") << scaled_secs(est, SCS_USEC|SCS_MAX2);
      }

      const index memory = squ_mem<limb>(ll);
      ss("   %-10?") << scaled_count(ll * sizeof(limb), SCT_MAX4) + "b";
      if (memory > 0) ss(" %?b mem") << scaled_count(memory, SCT_MAX4);

      tout_puts(s + "\n");
   }
}


template<uint64 thousand>
uint64 unit_factor(char c) {
   switch (c) {
   case 'k':
   case 'K':
      return thousand;
   case 'm':
   case 'M':
      return thousand*thousand;
   case 'g':
   case 'G':
      return thousand*thousand*thousand;
   case 't':
   case 'T':
      return thousand*thousand*thousand*thousand;
   case 'p':
   case 'P':
      return thousand*thousand*thousand*thousand*thousand;
   case 'e':
   case 'E':
      return thousand*thousand*thousand*thousand*thousand*thousand;
   }
   return 1;
}

template<class C>
uint64 static read_num(const C* w) {
   if (to_upper(*w) == 'M') {
      const uint64 n = s_to<uint64>(w + 1);
      for (size_t i=0; ; ++i) {
         const unsigned p = mersenne_prime_exps[i];
         if (p == 0) break;
         if (i+2 == n) return p;
      }
      return 0;
   }
   uint64 n = s_to<uint64>(w);
   while (is_digit(*w)) ++w;
   const uint64 f = unit_factor<1000>(*w);
   if (f != 1) {
      n *= f;
      ++w;
   }
   if (to_upper(*w) == 'W') n *= bits(limb);
   return n;
}

#ifdef CL_TEST_CODE
static void test_read_num() {
   uint64 n = read_num(L"1234");
   classert(n == 1234);

   n = read_num(L"1234k");
   classert(n == 1234000);

   n = read_num(L"1234M");
   classert(n == 1234000000);

   n = read_num(L"1234w");
   classert(n == 1234*bits(limb));
}

static void test_lltest() {
   test_eta_tracker();
   test_is_prime();
   test_read_num();
   test_myll();
   test_calibrate();
   tout_puts("test_lltest() passed.\n");
}
#endif


int __cdecl wmain(int argc, const wchar** argv) {
   start_cycles = cyc.is_thread_cycles() ? cyc.get_cycles() : 0;
   start_datetime = get_actual_string_datetime();
   command_line = GetCommandLineW();
   
   bench_param bp;
   bool help = false;
   unsigned primes = 0;
   bool selftest = false;
   unsigned k_mul_calibrate = 0, t3_mul_calibrate = 0, s_mul_calibrate = 0, mod_mul_calibrate = 0;
   unsigned k_squ_calibrate = 0, t3_squ_calibrate = 0, s_squ_calibrate = 0, mod_squ_calibrate = 0;
   sindex k_mul_thresh = -1, t3_mul_thresh = -1, s_mul_thresh = -1, q_mul_thresh = -1, dkss_mul_th = -1, mod_mul_thresh = -1;
   sindex k_squ_thresh = -1, t3_squ_thresh = -1, s_squ_thresh = -1, q_squ_thresh = -1, dkss_squ_th = -1, mod_squ_thresh = -1;
   bool test_align = false;
   unsigned show_exps = 0;
   uint64 from = 0;
   uint64 to = _UI64_MAX;
   unsigned cnt = 0;
   unsigned show_smul_fft_size = 0;
   unsigned fft_lg = 0;
   unsigned show_smul_rfft_size = 0;
   unsigned show_qmul_fft_size = 0;
   unsigned check = 0;
   unsigned check_mask_flag = 0;
   unsigned show_threshs = 0;
   int affinity = -1;
   unsigned set_window_title = 0;
   bool use_savefile = true;
#ifdef _M_X64
   int omul_routine = -1;
   int osqu_routine = -1;
#endif

#if 0
   bp.all_sizes = 1;
   bp.wildcards.push_back("omul_asmx64");
#endif

   tout_puts(format("This is lltest v" VERSION " (" ARCH ") (r%?, %?) by chris@cfos.de\n\n") << svn_revision << unixtime_to_string(get_pe_timestamp(get_current_module())));
   classert(floor(atof(VERSION) * 10) == num_ver);

   argc--;
   argv++;
   while (argc > 0) {
      auto opt = argv[0];
      if (*opt == '-') {
         ++opt;
         switch (*opt) {
         case 'B':
            ++opt;
            bp.all_sizes = 1;
            while (*opt) {
               switch (*opt) {
                  case 'B': ++bp.all_sizes; break;
                  case 'L': ++bp.run_long; break;
                  case 'H': ++bp.half_sized; break;
                  case 'R': bp.realtime = true; break;
                  case 'D':
                  case 'Q':
                  case 'A':
                  case 'F': {
                     double* v = nullptr;
                     if (*opt == 'A') {
                        v = &bp.step_sz;
                        ++opt;
                     }
                     else if (*opt == 'F') {
                        v = &bp.factor;
                        ++opt;
                     }
                     else {
                        while (*opt == 'D') {
                           ++bp.dkss_steps;
                           ++opt;
                        }
                        while (*opt == 'Q') {
                           ++bp.qmul_steps;
                           ++opt;
                        }
                     }
                     auto start = opt;
                     if (v) {
                        while (is_digit(*opt) || *opt == '.') ++opt;
                        *v = _wtof(wstring(start, opt-start).c_str());
                        if (*opt != ',') break;
                        ++opt;
                        start = opt;
                     }
                     while (is_digit(*opt)) ++opt;
                     bp.start_sz = _wtof(wstring(start, opt-start).c_str());
                     if (*opt == ',') {
                        start = ++opt;
                        while (is_digit(*opt)) ++opt;
                        bp.end_sz = _wtof(wstring(start, opt-start).c_str());
                     }
                     --opt;
                     break;
                  }
                  case 'a': ++bp.add_test; break;
                  case 's': ++bp.sub_test; break;
                  case 'm': ++bp.mul_test; break;
                  case 'q': ++bp.squ_test; break;
                  case 'd': ++bp.div_test; break;
                  case 'l': ++bp.logic_test; break;
                  case 'c': ++bp.smul_test; break;
                  case '*': bp.inc_all(); break;
                  case ':': {
                     auto start = ++opt;
                     while (*opt && *opt != ':') ++opt;
                     bp.wildcards.push_back(wstr2str(start, opt-start) + "*");
                     --opt;
                     break;
                  }
               }
               ++opt;
            }
            break;
         case 'M':
         case 'S': {
            const bool is_mul = *opt == 'M';
            ++opt;
            while (*opt) {
               switch (*opt) {
                  case 'K': ++(is_mul ? k_mul_calibrate : k_squ_calibrate); break;
                  case 'T': ++(is_mul ? t3_mul_calibrate : t3_squ_calibrate); break;
                  case 'S': ++(is_mul ? s_mul_calibrate : s_squ_calibrate); break;
                  case 'M': ++(is_mul ? mod_mul_calibrate : mod_squ_calibrate); break;
                  case 'l': ++bp.list_only; break;
                  case ':':
                     ++opt;
                     bp.start_sz = s_to<index>(opt);
                     while (is_digit(*opt)) ++opt;
                     if (!is_punct(*opt)) {
                        --opt;
                        break;
                     }
                     ++opt;
                     bp.fft_len = s_to<index>(opt);
                     while (is_digit(*opt)) ++opt;
                     if (!is_punct(*opt)) {
                        --opt;
                        break;
                     }
                     ++opt;
                     bp.end_sz = s_to<index>(opt);
                     while (is_digit(*opt)) ++opt;
                     --opt;
                     break;
               }
               ++opt;
            }
            break;
         }
         case 'm':
            switch (opt[1]) {
               case 'k': if (is_digit(opt[2])) k_mul_thresh = s_to<index>(opt+2); break;
               case 't': if (is_digit(opt[2])) t3_mul_thresh = s_to<index>(opt+2); break;
               case 's': if (is_digit(opt[2])) s_mul_thresh = s_to<index>(opt+2); break;
               case 'q': if (is_digit(opt[2])) q_mul_thresh = s_to<index>(opt+2); break;
               case 'd': if (is_digit(opt[2])) dkss_mul_th = s_to<index>(opt+2); break;
               case 'm': if (is_digit(opt[2])) mod_mul_thresh = s_to<index>(opt+2); break;
#ifdef _M_X64
               case 'o': if (is_digit(opt[2])) omul_routine = s_to<int>(opt+2); break;
#endif
            }
            break;
         case 's':
            switch (opt[1]) {
               case 'k': if (is_digit(opt[2])) k_squ_thresh = s_to<index>(opt+2); break;
               case 't': if (is_digit(opt[2])) t3_squ_thresh = s_to<index>(opt+2); break;
               case 's': if (is_digit(opt[2])) s_squ_thresh = s_to<index>(opt+2); break;
               case 'q': if (is_digit(opt[2])) q_squ_thresh = s_to<index>(opt+2); break;
               case 'd': if (is_digit(opt[2])) dkss_squ_th = s_to<index>(opt+2); break;
               case 'm': if (is_digit(opt[2])) mod_squ_thresh = s_to<index>(opt+2); break;
#ifdef _M_X64
               case 'o':
                  if (is_digit(opt[2])) osqu_routine = s_to<index>(opt+2);
                  else if (opt[2] == 'm' && is_digit(opt[3])) osqu_routine = s_to<int>(opt+3) + 100;
                  break;
#endif
            }
            break;
         case 'P':
            while (*opt) {
               switch (*opt) {
                  case 'P': ++primes; break;
                  case 'N': use_savefile = false; break;
               }
               ++opt;
            }
            break;
         case 'T': selftest = true; break;
         case 'A': test_align = true; break;
         case 'F':
            while (*opt == 'F') {
               ++show_smul_fft_size;
               ++opt;
            }
            if (*opt == 0) break;
            if (is_punct(*opt)) ++opt;
            if (*opt == 0) break;
            fft_lg = s_to<index>(opt);
            break;
         case 'R':
            while (*opt++ == 'R') ++show_smul_rfft_size;
            break;
         case 'Q':
            while (*opt++ == 'Q') ++show_qmul_fft_size;
            break;
         case 'E':
            while (*opt++ == 'E') ++show_exps;
            break;
         case 'h':
         case '?': help = true; break;
         case 'v':
            while (*opt++ == 'v') ++bp.verbose;
            break;
         case 'w': worker_name = opt+1; break;
         case 'd': working_directory = opt+1; break;
         case 'c':
            do {
               switch (*opt) {
                  case 'c': ++check; break;
                  case 'd': check_mask_flag |= DB_FLAG_DEBUG; break;
                  case 'r': check_mask_flag |= DB_FLAG_RESTARTED; break;
                  case 'K': check_mask_flag |= DB_FLAG_KSQU; break;
                  case 'T': check_mask_flag |= DB_FLAG_T3SQU; break;
                  case 'S': check_mask_flag |= DB_FLAG_SSQU; break;
                  case 'Q': check_mask_flag |= DB_FLAG_QSQU; break;
                  case 'D': check_mask_flag |= DB_FLAG_DKSS_SQU; break;
               }
            } while (*++opt);
            break;
         case 'I': ++show_threshs; break;
         case 'a': affinity = s_to<index>(opt+1); break;
         case 'W': ++set_window_title; break;
         case 'n':
            ++opt;
            idle_timeout = _wtoi(opt);
            while (is_digit(*opt)) ++opt;
            if (is_punct(*opt)) nice_perc = _wtoi(++opt);
            else idle_timeout = UINT_MAX;
            break;
         }
      }
      else {
         switch (cnt++) {
         case 0: from = read_num(opt); break;
         case 1: to = read_num(opt); break;
         case 2: help = true;
         }
      }
      argc--;
      argv++;
   }

   if (primes == 0 && !selftest &&
       !k_mul_calibrate && !t3_mul_calibrate && !s_mul_calibrate && !mod_mul_calibrate &&
       !k_squ_calibrate && !t3_squ_calibrate && !s_squ_calibrate && !mod_squ_calibrate &&
       !test_align && !show_exps && !bp.all_sizes && show_smul_fft_size == 0 &&
       show_smul_rfft_size == 0 && show_qmul_fft_size == 0 && show_threshs == 0)
      help = true;
   if ((show_smul_fft_size || show_smul_rfft_size || show_qmul_fft_size) && (cnt != 1 || from <= 0)) help = true;
   if (help) {
      out_puts(
         "usage: lltest [<from> <to>|<exp>] [options]\n"
         "\n"
         "enter a range of \"<from> 0\" to start from <from> without upper bound.\n"
         "\n"
         "benchmarking options:\n"
         "  -B...   benchmark, more options may follow\n"
         "    B     benchmark more sizes\n"
         "    L     run benchmarks longer\n"
         "    H     run benchmarks on half-sized limbs\n"
         "    F<f>[,<s>[,<e>]]  factor between steps, start at <s>, end at <= <e>\n"
         "    A<f>[,<s>[,<e>]]  summand between steps, start at <s>, end at <= <e>\n"
         "    D<s>[,<e>]  steps are dkss_mul steps (DD = compare all parameters)\n"
         "    a     benchmark add's\n"
         "    s     benchmark sub's\n"
         "    m     benchmark mul's\n"
         "    d     benchmark div's\n"
         "    l     benchmark logical operations\n"
         "    c     benchmark smul cyclic shifts\n"
         "    *     benchmark all\n"
         "\n"
         "calibration & speedtest options:\n"
         "  -M...   run calibration for mul:\n"
         "    K     run omul:kmul calibration\n"
         "    T     run kmul:t3mul calibration (TT = preciser run)\n"
         "    S     run smul fft calibration (SS = preciser run, SSS = even preciser)\n"
         "    M     run smul_mod fft calibration (MM = larger range)\n"
         "  -S...   run calibration for square, suboptions as for -M...\n"
         "  -m...   set threshold for mul:\n"
         "    k<N>  set kmul:omul threshold to <N> (0 = never, 1 = always)\n"
         "    t<N>  set t3mul:kmul threshold to <N> (0 = never, 1 = always)\n"
         "    s<N>  set smul:t3mul threshold to <N> (0 = never, 1 = always)\n"
         "    q<N>  set qmul:smul threshold to <N> (0 = never, 1 = always)\n"
         "    d<N>  set dkss_mul:smul threshold to <N> (0 = never, 1 = always)\n"
         "    m<N>  set smul_mod:t3mul_mod threshold to <N> (0 = never)\n"
         "    o<N>  use omul routine <N>\n"
         "  -s...   set threshold for square, suboptions as for -m...\n"
         "  -A      run alignment test\n"
         "  -F      show smul fft parameters for length <exp> bits (-FF = optimise fft size)\n"
         "  -R      show smul recursive fft parameters for length <exp> bits\n"
         "  -Q      show qmul fft parameters for length <exp> bits\n"
         "\n"
         "other options:\n"
         "  -T      just run selftest\n"
         "  -I      show CPU and calibration information\n"
         "  -E      show all known Mersenne primes (-EE = estimate time to check, -EEE = more accurate)\n"
         "  -P      run Lucas-Lehmer test; use database to get exponents & save results (honours <from>-<to>)\n"
         "  -PP     run Lucas-Lehmer test only on known primes (honours <from>-<to>, -PPN = no savefile)\n"
         "  -c      do double-checking instead of testing new exponents\n"
         "  -v      be verbose\n"
         "  -w\"xx\"  set worker name to \"xx\"\n"
         "  -d\"dd\"  set working directory to \"dd\"\n"
         "  -a<N>   set process run only on processor <N> (starts to count with 0)\n"
         "  -W      set console window's title\n"
         "  -n<t:p>  give away <p>%% of time if user is active and <t> secs later\n"
         );
      return 1;
   }

   set_thresholds();
   if (affinity != -1) {
      DWORD_PTR proc;
      DWORD_PTR sys;
      DWORD_PTR want = 1ui64 << affinity;
      //DTOUT_PUTS(format("running on processor %?\n") << GetCurrentProcessorNumber());
      if (GetProcessAffinityMask(GetCurrentProcess(), &proc, &sys) && (sys & want) != 0 && SetProcessAffinityMask(GetCurrentProcess(), want)) {
         run_proc = affinity;
         SetThreadIdealProcessor(GetCurrentThread(), affinity);
         Sleep(0);  // give system time to change the processor
         //DTOUT_PUTS(format("now running on processor %?\n") << GetCurrentProcessorNumber());
      }
      else {
         terr_puts("unable to set process affinity\n");
      }
   }

#ifdef CL_TEST_CODE
   test_bignum();
   test_lltest();
#endif

   if (working_directory.empty()) get_exec_dir(working_directory);
   add_bslash(working_directory);

   const auto test_r = bignum_selftest();
   if (test_r != 0) {
      out_puts(format("bignum selftest failed with code %x\n") << test_r);
      return 100;
   }
   if (selftest) {
      if (test_r == 0) out_puts("bignum selftest passed\n");
      return 0;
   }

   if (!cyc.is_ok()) {
      tout_puts("We need a proper cycle counter!\n");
      return 2;
   }

   // mul calibrations
#ifdef _M_X64
   switch (omul_routine) {
      case 0: omul_ptr = omul_unrolled; break;
      case 1: omul_ptr = omul_asmx64; break;
      case 2: omul_ptr = omul2_asmx64; break;
      case 3: omul_ptr = omul3_asmx64; break;
      case 4: omul_ptr = omul4_asmx64; break;
   }
   switch (osqu_routine) {
      case 0: osqu_ptr = osqu_plain; break;
      case 4: osqu_ptr = osqu4_asmx64; break;
      case 101: osqu_ptr = osqu_omul_asmx64; break;
      case 102: osqu_ptr = osqu_omul2_asmx64; break;
      case 103: osqu_ptr = osqu_omul3_asmx64; break;
      case 104: osqu_ptr = osqu_omul4_asmx64; break;
   }
#endif

   if (k_mul_calibrate) xmul_calibrate(k_mul_calibrate, 2, false);
   else if (k_mul_thresh == 0) switch_kmul(always_off);
   else if (k_mul_thresh != -1) switch_kmul(k_mul_thresh);
   else switch_kmul(kmul_thresh_init);

   if (t3_mul_calibrate) xmul_calibrate(t3_mul_calibrate, 3, false);
   else if (t3_mul_thresh == 0) switch_t3mul(always_off);
   else if (t3_mul_thresh != -1) switch_t3mul(t3_mul_thresh);
   else switch_t3mul(t3mul_thresh_init);

   if (q_mul_thresh == 0) switch_qmul(always_off);
   else if (q_mul_thresh != -1) switch_qmul(q_mul_thresh);
   else switch_qmul(qmul_thresh_init);

   if (mod_mul_calibrate) qsmul_mod_calibrate(mod_mul_calibrate, false);
   else if (mod_mul_thresh == 0) smul_mod_lg.clear_all();
   else if (mod_mul_thresh != -1) smul_mod_lg.set_packed_lg(mod_mul_thresh, 4);
   else smul_mod_lg.reset();

   if (s_mul_calibrate) qsmul_calibrate(s_mul_calibrate, false, bp);
   else if (s_mul_thresh == 0) switch_smul(always_off);
   else if (s_mul_thresh != -1) switch_smul(s_mul_thresh);
   else switch_smul(smul_thresh_init);

   if (dkss_mul_th == 0) switch_dkss_mul(always_off);
   else if (dkss_mul_th != -1) switch_dkss_mul(dkss_mul_th);
   else switch_dkss_mul(dkss_mul_thresh_init);

   // square calibrations
   if (k_squ_calibrate) xmul_calibrate(k_squ_calibrate, 2, true);
   else if (k_squ_thresh == 0) switch_ksqu(always_off);
   else if (k_squ_thresh != -1) switch_ksqu(k_squ_thresh);
   else switch_ksqu(ksqu_thresh_init);

   if (t3_squ_calibrate) xmul_calibrate(t3_squ_calibrate, 3, true);
   else if (t3_squ_thresh == 0) switch_t3squ(always_off);
   else if (t3_squ_thresh != -1) switch_t3squ(t3_squ_thresh);
   else switch_t3squ(t3squ_thresh_init);

   if (q_squ_thresh == 0) switch_qsqu(always_off);
   else if (q_squ_thresh != -1) switch_qsqu(q_squ_thresh);
   else switch_qsqu(qsqu_thresh_init);

   if (mod_squ_calibrate) qsmul_mod_calibrate(mod_squ_calibrate, true);
   else if (mod_squ_thresh == 0) ssqu_mod_lg.clear_all();
   else if (mod_squ_thresh != -1) ssqu_mod_lg.set_packed_lg(mod_squ_thresh, 4);
   else ssqu_mod_lg.reset();

   if (s_squ_calibrate) qsmul_calibrate(s_squ_calibrate, true, bp);
   else if (s_squ_thresh == 0) switch_ssqu(always_off);
   else if (s_squ_thresh != -1) switch_ssqu(s_squ_thresh);
   else switch_ssqu(ssqu_thresh_init);

   if (dkss_squ_th == 0) switch_dkss_squ(always_off);
   else if (dkss_squ_th != -1) switch_dkss_squ(dkss_squ_th);
   else switch_dkss_squ(dkss_squ_thresh_init);

   // more
   if (show_threshs) out_puts(system_info() + "\n");
   if (test_align) align_test();
   if (show_exps) show_exponents(show_exps);

   if (bp.all_sizes) {
      if (!bp.any_selected()) bp.inc_all();
      if (bp.half_sized) bignum_benchmark_half(bp);
      else bignum_benchmark(bp);
   }
   if (show_smul_fft_size) {
      tout_puts(system_info() + "\n");
      uint64 iter_time = 0;   // in nsec
      smul_print_params<limb>(from, fft_lg, true, show_smul_fft_size > 1, &iter_time);
      if (iter_time != 0) {
         tout_puts(format("\n""estimated run time for verification is %s\n") << scaled_secs((double)iter_time * from / 1000000000, SCS_MAX2));
         tout_puts(format("speed:\n   %.3? mbit/s\n   %.3? mbit/s/GHz\n") << 1000.0 * from / iter_time << 1000000000000.0 / clock_rate * from / iter_time);
         const double c = (double)iter_time * clock_rate / (1000000000.0 * from);
         const double idx = xlog2(from) * xlog2(xlog2(from)) / c + 0.5;
         tout_puts(format("   %.2? cyc/bit\n   idx=%.3?\n") << c + 0.5 << idx);
      }
   }
   else if (show_smul_rfft_size) {
      tout_puts(system_info() + "\n");
      smul_mod_print_params2<limb>(from, true, show_smul_rfft_size > 1);
   }
   else if (show_qmul_fft_size) {
      tout_puts(system_info() + "\n");
      qmul_print_params<limb>(from);
   }
   if (primes) {
      if (cnt == 1) to = from;  // only one arg entered
      else if (cnt > 1 && to == 0) to = physical_mem(false) * 8 / 3;  // second arg means "no upper bound"
      PROFILE_CLEAR();
      prime_db_name = working_directory + L"lltest-db.txt";
      file_trc.set_time(false, false);
      file_trc.set_name(working_directory + L"lltest-" + worker_name + L"-log.txt");
      const string window_title_string = format("lltest %?") << worker_name;
      const char* window_title = set_window_title ? window_title_string.c_str() : nullptr;
      //tapemgr.set_chunk_sz(squ_mem<limb>(4000000 / bits(limb)));
      //tapemgr.free_all_tapes();
      if (primes > 1) prime_lltests(from, to, bp.verbose, window_title, worker_name, use_savefile);
      else all_lltests(from, to, bp.verbose, check, check_mask_flag, 0, window_title, worker_name, use_savefile);
      PROFILE_PRINT();
   }

   tout.puts(format("\nMax memory allocated: %?b\n") << scaled_count(tapemgr.max_total_sz, SCT_MAX4|SCT_BIGGEST_UNIT));
   if (start_cycles != 0) {
      const uint64 diff = cyc.get_cycles() - start_cycles;
      const uint64 clock_rate = cpuid.query_clock_speed();  // in MHz
      tout.puts(format("Thread time: %? cycles, %? at %#,.3? GHz\n") 
         << scaled_count(diff, SCT_MAX4|SCT_BIGGEST_UNIT|SCT_DEC) 
         << scaled_secs(diff / clock_rate, SCS_USEC|SCS_MAX4)
         << clock_rate);
   }
   
   return 0;
}


// ------ set up file_trc style before first print ----------------------------

static void __cdecl _init_tracer(void) {
   // keep file_trc from being inited with ugly values
   file_trc_time = false;
   file_trc_time_msec = false;
}

// must be at the end and followed by some (junk) data item to prevent intel compiler bug
// segment must be before ".CRT$XCL_CLLIB_D9"
#pragma data_seg(".CRT$XCL_CLLIB_D")
#ifdef _WIN64
#pragma section(".CRT$XCL_CLLIB_D", read)
#endif
atexit_func _init_tracer_ptr[] = { _init_tracer };

#ifdef __INTEL_COMPILER
static int _intel_fix;
#endif
