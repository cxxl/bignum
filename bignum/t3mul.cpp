// $+HEADER$
// 
// Copyright 2015 Christoph Lueders
// 
// This file is part of the BIGNUM Library.
// 
// The BIGNUM Library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
// 
// The BIGNUM Library is distributed in the hope that it will be useful, but 
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public 
// License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with the BIGNUM Library.  If not, see <http://www.gnu.org/licenses/>.
// 
// $-HEADER$

// $Date: 2015-04-24 11:28:16 +0200 (Fr, 24 Apr 2015) $
// $Rev: 26590 $

/*
 *  arbitrary length number arithmetic - toom-cook 3 multiplication
 */

#include "precomp.h"
#include "impl.h"
#include "cl/int_math.h"
#ifdef CL_TEST_CODE
#include "cl/format.h"
#endif

// we must switch off "unreachable code" warning, since bool template parameters generate code paths that are never used
#pragma warning (disable:4702)

namespace bignum {

index _CL_CALL switch_t3mul(index set) {
   const index r = t3mul_thresh;
   t3mul_thresh = tmax(set, t3mul_min_len);
   return r;
}

index _CL_CALL switch_t3squ(index set) {
   const index r = t3squ_thresh;
   t3squ_thresh = tmax(set, t3mul_min_len);
   return r;
}


// ------ calc mem for TOOM-COOK 3 MUL ----------------------------------------

template<typename small, bool square>
index t3mul_mem(const index alen, const index blen) {
   if (!use_qt3mul<square>(alen, blen)) return kmul_mem<small, square>(alen, blen);

   const index llen = (alen + 2) / 3;  // length of the two lowest parts
   const index slen = llen + 1;  // all intermediate evaluation results will fit in here
   const index plen = 2*slen;  // all intermediate interpolation results will fit in here

   return (3*plen) * sizeof(small) + t3mul_mem<small, square>(slen, slen);
}


// ------ TOOM-COOK 3 MULTIPLICATION ------------------------------------------

// unsigned multiplication of two const input numbers 'a' and 'b' into a pre-reserved result 'r'.
// 'a' must be longer than or equal the size of 'b'.
// 'r' must be long enough to hold the 'alen+blen' long result.  there is no padding.
// length MUST NOT be zero, numbers don't need to be normalised

// from Marco Bodrato, "Towards Optimal Toom-Cook Multiplication for Univariate and Multivariate Polynomials in Characteristic 2 and 0"
// http://marco.bodrato.it/papers/Bodrato2007-OptimalToomCookMultiplicationForBinaryFieldAndIntegers.pdf

// U = U2*x^2 + U1*x + U0
// V = V2*x^2 + V1*x + V0
// \\ Evaluation: 5*2 add, 2 shift; 5mul
// W0 = U2 + U0 ; W4 = V2 + V0
// W2 = W0 - U1 ; W1 = W4 - V1
// W0 = W0 + U1 ; W4 = W4 + V1
// W3 = W2 * W1 ; W1 = W0 * W4
// W0 =(W0 + U2)<<1-U0; W4 =(W4 + V2)<<1-V0
// W2 = W0 * W4
// W0 = U0 * V0 ; W4 = U2 * V2
// \\ Interpolation: 8 add, 3 shift, 1 Sdiv
// W2 =(W2 - W3)/3
// W3 =(W1 - W3)>>1
// W1 = W1 - W0
// W2 =(W2 - W1)>>1 - W4<<1
// W1 = W1 - W3 - W4
// W3 = W3 - W2
// \\ Recomposition:
// W = W4*x^4+ W2*x^3+ W1*x^2+ W3*x + W0
// W == U*V

template<typename small, bool square>
void qt3mul_ls(small* r, const small* a, index alen, const small* b, index blen) {
   caassert(!square || a == b);
   caassert(!square || alen == blen);

   caassert(r != 0);
   caassert(a != 0);
   caassert(b != 0);
   caassert(alen >= blen);
   caassert((small)-1 > 0);  // must be unsigned type

   if (!use_qt3mul<square>(alen, blen)) {
      kmul_ls<small, square>(r, a, alen, b, blen);
      return;
   }
   caassert(alen >= t3mul_min_len);
   caassert(blen >= t3mul_min_len);

/* it doesn't pay off
   // uppermost word is zero?
   const index nalen = normu1(a, alen);
   const index nblen = normu1(b, blen);
   if (alen + blen != nalen + nblen) {
      zero_in(r, alen+blen, nalen+nblen);
      if (nblen > nalen) {
         alen = nblen;
         blen = nalen;
         swap(a, b);
      }
      else {
         alen = nalen;
         blen = nblen;
      }
   }
*/

   // llen is at least as long as all other parts
   const index llen = (alen + 2) / 3;  // length of the two lowest parts
   caassert(llen > 0);
   const index ahlen = alen - 2*llen;  // a high part length
   caassert(ahlen <= llen);
   caassert(ahlen >= 1);
   if (!square && blen <= 2*llen) {
      mul_unbal(r, a, alen, b, blen);
      return;
   }
   const index bhlen = blen - 2*llen;  // b high part length
   caassert(bhlen >= 1);
   caassert(bhlen <= llen);
   const index rlen = alen + blen;

   const small* a0 = a;
   const small* a1 = a + llen;
   const small* a2 = a + 2*llen;
   const small* b0 = b;
   const small* b1 = b + llen;
   const small* b2 = b + 2*llen;

   const index slen = llen + 1;  // all intermediate evaluation results will fit in here
   const index plen = 2*slen;  // all intermediate interpolation results will fit in here
   tape_alloc<small> tmp(3*plen);
   caassert(plen <= rlen);  // w0 must fit into result
   small* w0 = r;  // already in place
   small* w1 = tmp.p;
   small* w2 = tmp.p + plen;
   small* w3 = tmp.p + 2*plen;
   small* w4 = r + 4*llen;  // already in place

   // W0 = A2 + A0  (non-negative)
   addu(w0, slen, a2, ahlen, a0, llen);

   // W4 = B2 + B0  (non-negative)
   if (!square) addu(w4, slen, b2, bhlen, b0, llen);

   // W2 = W0 - A1
   //   out W2: (A2 + A0 - A1)  (maybe negative)
   bool w2sign = add_or_subu(w2, slen, w0, slen, false, a1, llen, true);

   // W1 = W4 - B1
   //   out W1: (B2 + B0 - B1)  (maybe negative)
   bool w1sign = square ? w2sign : add_or_subu(w1, slen, w4, slen, false, b1, llen, true);

   // W0 = W0 + A1
   //   out W0: (A2 + A0 + A1)  (non-negative)
   addu_on(w0, slen, a1, llen);

   // W4 = W4 + B1
   //   out W4: (B2 + B0 + B1)  (non-negative)
   if (!square) addu_on(w4, slen, b1, llen);

   // W3 = W1 * W2
   //   in W1 : B2 + B0 - B1  (maybe negative)
   //   in W2 : A2 + A0 - A1  (maybe negative)
   //   out W3: (B2 + B0 - B1) * (A2 + A0 - A1)  =  A0*B0 - A0*B1 + A0*B2 - A1*B0 + A1*B1 - A1*B2 + A2*B0 - A2*B1 + A2*B2
   // signs are kept in w1sign and w2sign, w1 and w2 are always non-negative, as is the result in w3
   qt3mul_ls<small, square>(w3, w2, slen, square ? w2 : w1, slen);
   bool w3sign = square ? false : w1sign != w2sign;  // sign of W3, set if negative

   // W1 = W0 * W4
   //   in W0 : A2 + A0 + A1  (non-negative)
   //   in W4 : B2 + B0 + B1  (non-negative)
   //   out W1: (A2 + A0 + A1) * (B2 + B0 + B1)  =  A0*B0 + A0*B1 + A0*B2 + A1*B0 + A1*B1 + A1*B2 + A2*B0 + A2*B1 + A2*B2  (non-negative)
   qt3mul_ls<small, square>(w1, w0, slen, square ? w0 : w4, slen);

   // W0 = (W0 + A2)<<1-A0
   //   in W0 : A2 + A0 + A1  (non-negative)
   //   out W0: (4*A2 + 2*A1 + A0)  (non-negative)
   addu_on(w0, slen, a2, ahlen);
   shl1_on(w0, slen);
   subu_on(w0, slen, a0, llen);

   // W4 = (W4 + B2)<<1-B0
   //   in W4 : B2 + B0 + B1  (non-negative)
   //   out W4: (4*B2 + 2*B1 + B0)  (non-negative)
   if (!square) {
      addu_on(w4, slen, b2, bhlen);
      shl1_on(w4, slen);
      subu_on(w4, slen, b0, llen);
   }

   // W2 = W0 * W4
   //   in W0 : A2 + A0 + A1  (non-negative)
   //   in W4 : B2 + B0 + B1  (non-negative)
   //   out W2: (4*A2 + 2*A1 + A0) * (4*B2 + 2*B1 + B0)  =  A0*B0 +2*A0*B1 + 4*A0*B2 + 2*A1*B0 + 4*A1*B1 + 8*A1*B2 + 4*A2*B0 + 8*A2*B1 + 16*A2*B2  (non-negative)
   qt3mul_ls<small, square>(w2, w0, slen, square ? w0 : w4, slen);
   w2sign = false;

   // W0 = A0 * B0  (non-negative)
   caassert(rlen >= 2*llen);
   qt3mul_ls<small, square>(w0, a0, llen, square ? a0 : b0, llen);

   // W4 = A2 * B2  (non-negative)
   caassert(rlen >= 4*llen + ahlen + bhlen);
   qt3mul_ls<small, square>(w4, a2, ahlen, square ? a2 : b2, square ? ahlen : bhlen);

   // W2 = (W2 - W3)/3
   //   in W2 : A0*B0 + 2*A0*B1 + 4*A0*B2 + 2*A1*B0 + 4*A1*B1 + 8*A1*B2 + 4*A2*B0 + 8*A2*B1 + 16*A2*B2  (non-negative)
   //   in W3 : A0*B0 - A0*B1 + A0*B2 - A1*B0 + A1*B1 - A1*B2 + A2*B0 - A2*B1 + A2*B2  (maybe negative)
   //   out W2: A0*B1 + A0*B2 + A1*B0 + A1*B1 + 3*A1*B2 + A2*B0 + 3*A2*B1 + 5*A2*B2  (non-negative)
   w2sign = add_or_subu_on(w2, plen, false, w3, plen, !w3sign);
   caassert(w2sign == false);
   div_by3_fast(w2, w2, plen);

   // W3 = (W1 - W3)>>1
   //   in W1 : A0*B0 + A0*B1 + A0*B2 + A1*B0 + A1*B1 + A1*B2 + A2*B0 + A2*B1 + A2*B2  (non-negative)
   //   in W3 : A0*B0 - A0*B1 + A0*B2 - A1*B0 + A1*B1 - A1*B2 + A2*B0 - A2*B1 + A2*B2  (maybe negative)
   //   out W3: A0*B1 + A1*B0 + A1*B2 + A2*B1  (non-negative)
   w3sign = add_or_subu_on(w3, plen, !w3sign, w1, plen, false);
   caassert(w3sign == false);
   shr_on(w3, plen, 1);

   // W1 = W1 - W0
   //   in W1 : A0*B0 + A0*B1 + A0*B2 + A1*B0 + A1*B1 + A1*B2 + A2*B0 + A2*B1 + A2*B2  (non-negative)
   //   in W0 : A0 * B0  (non-negative)
   //   out W1: A0*B1 + A0*B2 + A1*B0 + A1*B1 + A1*B2 + A2*B0 + A2*B1 + A2*B2  (non-negative)
   subu_on(w1, plen, w0, 2*llen);

   // W2 = (W2 - W1)>>1 - W4<<1
   //   in W2 : A0*B1 + A0*B2 + A1*B0 + A1*B1 + 3*A1*B2 + A2*B0 + 3*A2*B1 + 5*A2*B2  (non-negative)
   //   in W1 : A0*B1 + A0*B2 + A1*B0 + A1*B1 + A1*B2 + A2*B0 + A2*B1 + A2*B2  (non-negative)
   //   in W4 : A2 * B2  (non-negative)
   //   out W2: A1*B2 + A2*B1  (non-negative)
   sub_fast(w2, w2, w1, plen);
   shr_on(w2, plen, 1);
   subu_on(w2, plen, w4, ahlen+bhlen);
   subu_on(w2, plen, w4, ahlen+bhlen);  // improve: sub with shift

   // W1 = W1 - W3 - W4
   //   in W1 : A0*B1 + A0*B2 + A1*B0 + A1*B1 + A1*B2 + A2*B0 + A2*B1 + A2*B2  (non-negative)
   //   in W3 : A0*B1 + A1*B0 + A1*B2 + A2*B1  (non-negative)
   //   in W4 : A2 * B2  (non-negative)
   //   out W1: A0*B2 + A1*B1 + A2*B0
   sub_fast(w1, w1, w3, plen);
   subu_on(w1, plen, w4, ahlen+bhlen);

   // W3 = W3 - W2
   //   in W3 : A0*B1 + A1*B0 + A1*B2 + A2*B1  (non-negative)
   //   in W2 : A1*B2 + A2*B1  (non-negative)
   //   out W3: A0*B1 + A1*B0  (non-negative)
   sub_fast(w3, w3, w2, plen);

   classert(normu(w1, plen) <= 2*llen+1);
   classert(normu(w2, plen) <= 2*llen+1);
   classert(normu(w3, plen) <= 2*llen+1);

   // W = W4*x^4 + W2*x^3 + W1*x^2 + W3*x + W0  (attention with the Wx indices!)
   // w0 and w4 are already in place
   // i.e. 0..2*llen-1 and 4*llen..rlen are set.  2*llen..4*llen-1 are uninitialised
   //copy(r, w0, 2*llen);  // already there
   //if (rlen > 4*llen) copy(r+4*llen, w4, rlen-4*llen);
   // copy & add middle part
   copy(r+2*llen, w1, 2*llen);
   addu_on(r+4*llen, rlen-4*llen, w1+2*llen, 1);  // improve: use addu1()
   // add on other parts
   addu_on(r+llen, rlen-llen, w3, 2*llen+1);
   addu_on(r+3*llen, rlen-3*llen, w2, ahlen+llen+1);
}


template<typename small>
void mul_unbal(small* r, const small* a, index alen, const small* b, index blen) {
   //classertm(0, "not yet supported");
   kmul(r, a, alen, b, blen);  // improve!
}


#pragma warning(push)
#pragma warning(disable:4305)
#pragma warning(disable:4309)

template<typename small>
dword _CL_CALL selftest_t3mul() {
#ifndef NDEBUG
   // check if w4 might be used safely for addition
   // w4 has length ahlen+bhlen and we need length llen+1
   for (index i=t3mul_min_len; i<1000; ++i) {
      index llen = (i+2) / 3;
      index hlen = i - 2*llen;
      classert(hlen > 0);
      classert(2*hlen >= llen + 1);
      //if (2*hlen < llen + 1) tout_puts(format("length %? would not work\n") << i);
   }
#endif

   const index oldt3 = switch_t3mul(always_on);
   /* not used because of t3mul_min_len
   {
      small a[] = { 2, 5, 11 };
      small b[] = { 3, 7, 13 };
      small r1[6], r2[6];

      qt3mul_ls<small, false>(r1, a, 3, b, 3);
      omul_ls(r2, a, 3, b, 3);
      classert(compu(r1, 6, r2, 6) == 0);
      if (compu(r1, 6, r2, 6) != 0) return 1;

      qt3mul_ls<small, false>(r1, b, 3, a, 3);
      classert(compu(r1, 6, r2, 6) == 0);
      if (compu(r1, 6, r2, 6) != 0) return 2;
   }
   {
      small a[] = { 2, 7, 11 };
      small b[] = { 3, 5, 13 };
      small r1[6], r2[6];

      qt3mul_ls<small, false>(r1, a, 3, b, 3);
      omul_ls(r2, a, 3, b, 3);
      classert(compu(r1, 6, r2, 6) == 0);
      if (compu(r1, 6, r2, 6) != 0) return 3;
   }
   {
      small a[] = { 2, 7, 11 };
      small b[] = { 255, 254, 253 };
      small r1[6], r2[6];

      qt3mul_ls<small, false>(r1, a, 3, b, 3);
      omul_ls(r2, a, 3, b, 3);
      classert(compu(r1, 6, r2, 6) == 0);
      if (compu(r1, 6, r2, 6) != 0) return 5;
   }
   {
      small a[] = { 2, 7, 11, 19, 23, 31 };
      small b[] = { 3, 5, 13, 17, 29, 37 };
      small r1[NBEL(a)+NBEL(b)], r2[NBEL(a)+NBEL(b)];

      qt3mul_ls<small, false>(r1, a, NBEL(a), b, NBEL(b));
      omul_ls(r2, a, NBEL(a), b, NBEL(b));
      classert(compu(r1, NBEL(r1), r2, NBEL(r2)) == 0);
      if (compu(r1, NBEL(r1), r2, NBEL(r2)) != 0) return 7;

      qt3mul_ls<small, false>(r1, b, NBEL(b), a, NBEL(a));
      classert(compu(r1, NBEL(r1), r2, NBEL(r2)) == 0);
      if (compu(r1, NBEL(r1), r2, NBEL(r2)) != 0) return 8;
   }
   {
      small a[] = { 2, 7, 11, 19, 23 };
      small b[] = { 3, 5, 13, 17, 29 };
      small r1[NBEL(a)+NBEL(b)], r2[NBEL(a)+NBEL(b)];

      qt3mul_ls<small, false>(r1, a, NBEL(a), b, NBEL(b));
      omul_ls(r2, a, NBEL(a), b, NBEL(b));
      classert(compu(r1, NBEL(r1), r2, NBEL(r2)) == 0);
      if (compu(r1, NBEL(r1), r2, NBEL(r2)) != 0) return 9;

      qt3mul_ls<small, false>(r1, b, NBEL(b), a, NBEL(a));
      classert(compu(r1, NBEL(r1), r2, NBEL(r2)) == 0);
      if (compu(r1, NBEL(r1), r2, NBEL(r2)) != 0) return 10;
   }
   {
      small a[] = { 2, 7, 11, 19 };
      small b[] = { 3, 5, 13, 17 };
      small r1[NBEL(a)+NBEL(b)], r2[NBEL(a)+NBEL(b)];

      qt3mul_ls<small, false>(r1, a, NBEL(a), b, NBEL(b));
      omul_ls(r2, a, NBEL(a), b, NBEL(b));
      classert(compu(r1, NBEL(r1), r2, NBEL(r2)) == 0);
      if (compu(r1, NBEL(r1), r2, NBEL(r2)) != 0) return 11;

      qt3mul_ls<small, false>(r1, b, NBEL(b), a, NBEL(a));
      classert(compu(r1, NBEL(r1), r2, NBEL(r2)) == 0);
      if (compu(r1, NBEL(r1), r2, NBEL(r2)) != 0) return 12;
   }
   {
      small a[] = { 28253, 12446, 11078, 63587, 38901, 8254 };
      small b[] = { 52059, 46194, 46001, 5334, 9281, 48278 };
      small r1[NBEL(a)+NBEL(b)], r2[NBEL(a)+NBEL(b)];

      qt3mul_ls<small, false>(r1, a, NBEL(a), b, NBEL(b));
      omul_ls(r2, a, NBEL(a), b, NBEL(b));
      classert(compu(r1, NBEL(r1), r2, NBEL(r2)) == 0);
      if (compu(r1, NBEL(r1), r2, NBEL(r2)) != 0) return 13;
   }
   // test different lengths
   {
      small a[] = { 28253, 12446, 11078, 63587, 38901, 48278 };
      small b[] = { 52059, 46194, 46001, 19825, 1 };
      small r1[NBEL(a)+NBEL(b)], r2[NBEL(a)+NBEL(b)];

      qt3mul_ls<small, false>(r1, a, NBEL(a), b, NBEL(b));
      omul_ls(r2, a, NBEL(a), b, NBEL(b));
      classert(compu(r1, NBEL(r1), r2, NBEL(r2)) == 0);
      if (compu(r1, NBEL(r1), r2, NBEL(r2)) != 0) return 15;
   }
   // test even more differently lengths
   {
      small a[] = { 28253, 12446, 11078, 63587, 38901, 8254, 5334, 9281, 48278, 46001, 35723, 12756, 37568, 27343 };
      small b[] = { 52059, 46194 };
      small r1[NBEL(a)+NBEL(b)], r2[NBEL(a)+NBEL(b)];

      qt3mul_ls<small, false>(r1, a, NBEL(a), b, NBEL(b));
      omul_ls(r2, a, NBEL(a), b, NBEL(b));
      classert(compu(r1, NBEL(r1), r2, NBEL(r2)) == 0);
      if (compu(r1, NBEL(r1), r2, NBEL(r2)) != 0) return 17;
   } */
   {
      small a[40]; rand_fill(a, NBEL(a));
      small b[40]; rand_fill(b, NBEL(b));
      small r1[NBEL(a)+NBEL(b)], r2[NBEL(a)+NBEL(b)];

      for (unsigned i=0; i<NBEL(a); ++i) {
         // test square
         qt3mul_ls<small, true>(r1, a, NBEL(a), a, NBEL(a));
         osqu(r2, a, NBEL(a));
         classert(compu(r1, NBEL(r1), r2, NBEL(r2)) == 0);
         if (compu(r1, NBEL(r1), r2, NBEL(r2)) != 0) return 20;

         // test mul
         for (unsigned j=0; j<NBEL(b); ++j) {
            qt3mul_ls<small, false>(r1, a, NBEL(a), b, NBEL(b));
            omul_ls(r2, a, NBEL(a), b, NBEL(b));
            classert(compu(r1, NBEL(r1), r2, NBEL(r2)) == 0);
            if (compu(r1, NBEL(r1), r2, NBEL(r2)) != 0) return 21;

            qt3mul_ls<small, false>(r1, b, NBEL(b), a, NBEL(a));
            classert(compu(r1, NBEL(r1), r2, NBEL(r2)) == 0);
            if (compu(r1, NBEL(r1), r2, NBEL(r2)) != 0) return 22;
         }
      }
   }
   switch_t3mul(oldt3);

   return 0;
}

#ifdef CL_TEST_CODE
static void test_t3squ() {
   auto old = switch_t3squ(always_on);
   {
      uint8 a[] = { 1, 2, 3 };
      uint8 r[2*NBEL(a)];
      t3squ(r, a, NBEL(a));
      string s = dump_hex(r, NBEL(r));
      classert(s == "0x90c0a0401");
   }
   {
      uint8 a[] = { 1, 2, 3, 4, 5, 6, 7 };
      uint8 r[2*NBEL(a)];
      t3squ(r, a, NBEL(a));
      string s = dump_hex(r, NBEL(r));
      classert(s == "0x31546a747368543823140a0401");
   }
   {
      uint8 a[] = { 0xff, 0xfe, 0xfd, 0xfc, 0xfb, 0xfa, 0xf9, 0xf8, 0xf7, 0xf6, 0xf5 };
      uint8 r[2*NBEL(a)];
      t3squ(r, a, NBEL(a));
      string s = dump_hex(r, NBEL(r));
      classert(s == "0xec52a4e4112d39362506dab98a64462f1e120a050201");
   }
   switch_t3squ(old);
}
#endif


// ------ multiply mod 2^n+1 --------------------------------------------------

template<typename small, bool square>
void qt3mul_mod(small* r, const small* a, const small* b, const index len, const index n) {
   caassert(!square || a == b);
   caassert(len == n / bits(small) + 1);  // len must be big enough to hold 2^n
   tape_alloc<small> tmp(2*len);
   small* t = tmp.p;
   const index alen = normu(a, len);
   const index blen = square ? alen : normu(b, len);
   const index tlen = alen + blen;
   qt3mul<small, square>(t, a, alen, b, blen);
   if (tlen >= len) {
      mod_pow2p1_aligned_on(t, tlen, n);
      copy(r, t, len);
   }
   else {
      copy(r, t, tlen);
      zero(r, len, tlen);
   }
   classert(cmp_pow2(r, len, n) <= 0);
}

#ifdef CL_TEST_CODE
static void test_t3mul_mod() {
   {
      uint8 a[] = { 1, 2, 3, 0 };  // 0x30201
      uint8 b[NBEL(a)] = { 2, 3, 4, 0 };  // 0x40302
      uint8 r[NBEL(a)];
      // 0x30201 * 0x40302 = 0xc11100702
      t3mul_mod(r, a, b, NBEL(a), sizeof(a)*8-8);
      string s = dump_hex(r, NBEL(r));
      classert(s == "0xffaf1");
   }
   {
      uint8 a[] = { 1, 2, 3, 4, 0 };  // 0x4030201
      uint8 b[NBEL(a)] = { 2, 3, 4, 5, 0 };  // 0x5040302
      uint8 r[NBEL(a)];
      // 0x4030201 * 0x5040302 = 0x141f221e100702
      t3mul_mod(r, a, b, NBEL(a), sizeof(a)*8-8);
      string s = dump_hex(r, NBEL(r));
      classert(s == "0x1dfbe7e0");
   }
   {
      uint8 a[] = { 0xff, 0, 0, 0 };
      uint8 b[NBEL(a)] = { 0xfe, 0, 0, 0 };
      uint8 r[NBEL(a)];
      // 0xff * 0xfe = 0xfd02
      t3mul_mod(r, a, b, NBEL(a), sizeof(a)*8-8);
      string s = dump_hex(r, NBEL(r));
      classert(s == "0xfd02");
   }
}
#endif


// ------ estimate t3mul() time -----------------------------------------------

// use these timings:

// CPU: Intel(R) Core(TM)2 Quad CPU Q9550 @ 2.83GHz, CPUID: 06_17h
// clock speed is about 2.833 GHz
// small has 64 bits
// kmul_thresh = 25, 1600 bits
// t3mul_thresh = 128, 8192 bits
//
// benchmarking t3mul(n,n):
//  size/u  mem/b rounds    total      ns/u      cyc/u   cyc/O
//       1     32  42.7M   0.9998     23.37      66.22   66.22
//       2     64    34M   1.0003     14.68      41.61   27.74
//       4    128  18.2M   1.0010     13.69      38.80   19.40
//       8    256  6.66M   0.9705     18.20      51.56   18.74
//      16    512  1.96M   1.0005     31.76      89.98   24.40
//      32     1K   592K   0.9995     52.68     149.25   29.66
//      64     2K   183K   0.9988     85.10     241.09   34.83
//     128     4K  57.5K   0.9970    135.25     383.18   40.13
//     256     8K  19.3K   0.9964    200.80     568.87   43.17
//     512    16K  6.98K   0.9997    279.69     792.37   43.56
//      1K    32K     2K   0.8297    405.14    1147.77   45.72
//      2K    64K    875   0.9998    557.97    1580.73   45.62
//      4K   128K    313   1.0004    780.38    2210.81   46.22
//      8K   256K    100   0.9012   1100.21    3116.89   47.21
//     16K   512K     40   0.9915   1512.96    4286.23   47.04
//     32K     1M     14   0.9833   2143.60    6072.82   48.28
//     64K     2M      5   0.9880   3015.39    8542.60   49.21
//    128K     4M      2   1.0470   3994.06   11315.18   47.22
//    256K     8M      1   1.4690   5603.92   15875.92   48.00

static double t3mul_cyc_per_u64[] = {
     66.22,   //    1 = 2^0
     41.61,   //    2 = 2^1
     38.80,   //    4 = 2^2
     51.56,   //    8 = 2^3
     89.98,   //   16 = 2^4
    149.25,   //   32 = 2^5
    241.09,   //   64 = 2^6
    383.18,   //  128 = 2^7
    568.87,   //  256 = 2^8
    792.37,   //  512 = 2^9
   1147.77,   //   1K = 2^10
   1580.73,   //   2K = 2^11
   2210.81,   //   4K = 2^12
   3116.89,   //   8K = 2^13
   4286.23,   //  16K = 2^14
   6072.82,   //  32K = 2^15
   8542.60,   //  64K = 2^16
  11315.18,   // 128K = 2^17
  15875.92    // 256K = 2^18
};

uint64 _CL_CALL estimate_t3mul_time(index len) {
   return estimate_time(len, t3mul_cyc_per_u64, NBEL(t3mul_cyc_per_u64), log3_5);
}

#ifdef CL_TEST_CODE
static void test_estimate_t3mul_time() {
   uint64 c = estimate_t3mul_time(1);
   classert(c == 66);
   c = estimate_t3mul_time(2);
   classert(c == 83);
   c = estimate_t3mul_time(16384);
   classert(c == 70225592);
   c = estimate_t3mul_time(64);
   classert(c == 15429);
   c = estimate_t3mul_time(96);
   classert(c == 29964);
   c = estimate_t3mul_time(128);
   classert(c == 49047);
   c = estimate_t3mul_time(512*1024);
   classert(c == 11488935628);
}
#endif


#ifdef CL_TEST_CODE
void _CL_CALL test_bignum_t3mul() {
   switch_t3mul(always_on);
   selftest_t3mul<uint16>();
   selftest_t3mul<uint8>();
   selftest_t3mul<uint32>();
#ifdef _M_X64
   selftest_t3mul<uint64>();
#endif
   switch_t3mul(t3mul_thresh_init);
   test_estimate_t3mul_time();
   test_t3squ();
   test_t3mul_mod();
}
#endif

#pragma warning(pop)


// ------ generate template functions -----------------------------------------

//#if _MSC_VER < 1300
#pragma inline_depth(0)

template<typename small>
void generate_bignum_t3muls() {
   t3mul<small>(0, 0, 0, 0, 0);
   t3squ<small>(0, 0, 0);
   t3mul_mem<small, false>(0, 0);
   t3mul_mem<small, true>(0, 0);
   selftest_t3mul<small>();
   t3mul_mod<small>(0, 0, 0, 0, 0);
   t3squ_mod<small>(0, 0, 0, 0);
}

void _generate_bignum_t3muls() {
   classertm(0, "must not be called");
   generate_bignum_t3muls<uint8>();
   generate_bignum_t3muls<uint16>();
   generate_bignum_t3muls<uint32>();
#ifdef _M_X64
   generate_bignum_t3muls<uint64>();
#endif
}

#pragma inline_depth()
//#endif

}  // namespace bignum
