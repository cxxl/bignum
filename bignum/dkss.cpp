// $+HEADER$
// 
// Copyright 2015 Christoph Lueders
// 
// This file is part of the BIGNUM Library.
// 
// The BIGNUM Library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
// 
// The BIGNUM Library is distributed in the hope that it will be useful, but 
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public 
// License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with the BIGNUM Library.  If not, see <http://www.gnu.org/licenses/>.
// 
// $-HEADER$

// $Date: 2015-04-24 11:28:16 +0200 (Fr, 24 Apr 2015) $
// $Rev: 26590 $

// $dissect$ support.txt
#include "precomp.h"
#include "dkss.h"
#include "fft.h"
#include "prime.h"
#include "pro_filer.h"
#include "cl/int_math.h"
#include "cl/memzero.h"
#include "cl/str_conv.h"

namespace bignum {

bool dkss_pc_bits_roundup = true;
   
index dkss_mul_thresh_init = always_off;
index dkss_mul_thresh = dkss_mul_thresh_init;

index dkss_squ_thresh_init = always_off;
index dkss_squ_thresh = dkss_squ_thresh_init;

index _CL_CALL switch_dkss_mul(index set) {
   const index r = dkss_mul_thresh;
   dkss_mul_thresh = tmax(set, dkss_mul_min_len);
   return r;
}

index _CL_CALL switch_dkss_squ(index set) {
   const index r = dkss_squ_thresh;
   dkss_squ_thresh = tmax(set, dkss_mul_min_len);
   return r;
}

pro_filer dkss_prof;

// #define DO_PROF 1

#ifdef DO_PROF
#define PROF(a) a

const size_t profid_dkss_mul3 = dkss_prof.register_counter(" dkss_mul3");
const size_t profid_dkss_fft = dkss_prof.register_counter("  dkss_fft");
const size_t profid_dkss_inner_fft = dkss_prof.register_counter("   dkss_inner_fft");
const size_t profid_dkss_bad_muls = dkss_prof.register_counter("   dkss_bad_muls");
const size_t profid_dkss_point_mul = dkss_prof.register_counter("  dkss_point_mul");
const size_t profid_modpoly_mul = dkss_prof.register_counter(" modpoly_mul");
const size_t profid_modpoly_mul_imul = dkss_prof.register_counter("  modpoly_mul_imul");
const size_t profid_modpoly_mul_mod = dkss_prof.register_counter("  modpoly_mul_mod");

#else
#define PROF(a) NOP()
#endif



// $dissect$ poly.txt
template<typename small>
string dump_poly_hex(const small* a, const index sz, const index len) {
   string s;
   s.reserve(sz * (2 * sizeof(small) * len + 4));
   s = "[";

   const small* ai = a;
   for (index i=0; i<sz; ++i) {
      if (i > 0) s += ", ";
      s += dump_hex(ai, len);
      ai += len;
   }
   s += "]";
   return s;
}

template<typename small>
string dump_poly(const small* a, const index sz, const index len) {
   string s;
   s.reserve(sz * (3 * sizeof(small) * len + 2));
   s = "[";

   const small* ai = a;
   for (index i=0; i<sz; ++i) {
      if (s.size() > 1) s += ", ";
      s += i_to<string>(basic_bignum<small, false>(ai, len));
      ai += len;
   }
   s += "]";
   return s;
}

#ifdef CL_TEST_CODE
static void test_dump_poly_hex() {
   string s;
   {
      uint16 a[] = { 0 };
      s = dump_poly_hex(a, 0, 1);
      classert(s == "[]");
   }
   {
      uint16 a[] = { 8 };
      s = dump_poly_hex(a, NBEL(a), 1);
      classert(s == "[8]");
   }
   {
      uint16 a[] = { 1, 4, 7, 3, 8 };
      s = dump_poly_hex(a, NBEL(a), 1);
      classert(s == "[1, 4, 7, 3, 8]");
   }
   {
      uint16 a[] = { 1, 0, 4, 0, 7, 0, 3, 0, 8, 0 };
      s = dump_poly_hex(a, NBEL(a)/2, 2);
      classert(s == "[1, 4, 7, 3, 8]");
   }
   {
      uint16 a[] = { 255, 13, 7, 0xffff };
      s = dump_poly_hex(a, NBEL(a), 1);
      classert(s == "[0xff, 0xd, 7, 0xffff]");
   }
   {
      uint16 a[] = { 0x1234, 0x5678, 0xabcd, 0xcafe};
      s = dump_poly_hex(a, NBEL(a)/2, 2);
      classert(s == "[0x56781234, 0xcafeabcd]");
   }
}
#endif


template<typename small>
string dump_polypoly_hex(const small* a, const index osz, const index isz, const index len) {
   string s;
   s.reserve(osz * (isz * (2 * sizeof(small) * len + 4) + 2));

   s = "[ ";
   const small* ai = a;
   for (index j=0; j<osz; ++j) {
      if (j > 0) s += ", ";
      s += "[";
      for (index i=0; i<isz; ++i) {
         if (i > 0) s += ", ";
         s += dump_hex(ai, len);
         ai += len;
      }
      s += "]";
   }
   s += " ]";

   return s;
}

#ifdef CL_TEST_CODE
static void test_dump_polypoly_hex() {
   string s;
   {
      uint16 a[] = { 0 };
      s = dump_polypoly_hex(a, 0, 2, 1);
      classert(s == "[  ]");
   }
   {
      uint16 a[] = { 0, 1, 2, 3 };
      s = dump_polypoly_hex(a, 2, 2, 1);
      classert(s == "[ [0, 1], [2, 3] ]");
   }
   {
      uint16 a[] = { 0xabcd, 0xfffe, 0x1234, 0x56ab, 0x55aa, 0xaa55 };
      s = dump_polypoly_hex(a, 3, 2, 1);
      classert(s == "[ [0xabcd, 0xfffe], [0x1234, 0x56ab], [0x55aa, 0xaa55] ]");
   }
}
#endif


// ------ ADDITION ------------------------------------------------------------

// add two polynomials with 'sz' coeffs in Z/mod Z, with each coeff 'len' small's long and < 'mod'
// overlapping use is permitted (a += b usage ok)!

template<typename small>
void modpoly_add(small* r, const small* a, const small* b, const index sz, const index len, const small* mod) {
   caassert(r != nullptr);
   caassert(a != nullptr);
   caassert(b != nullptr);
   caassert(len > 0);
   caassert(mod != nullptr);
   caassert((small)-1 > 0);  // must be unsigned type

   small* ri = r;
   const small* ai = a;
   const small* bi = b;
   for (index i=0; i<sz; ++i) {
      classert(compu(ai, len, mod, len) < 0);  // a[i] < mod?
      classert(compu(bi, len, mod, len) < 0);  // b[i] < mod?
      const small of = add_fast(ri, ai, bi, len);
      if (of || compu(ri, len, mod, len) >= 0) {
         CLDEBUG(const small of2 =) sub_fast(ri, ri, mod, len);
         classert((of != 0) == (of2 != 0));  // add overflow must result in sub underflow
      }
      classert(compu(ri, len, mod, len) < 0);  // r[i] < mod?
      ri += len;
      ai += len;
      bi += len;
   }
}

#ifdef CL_TEST_CODE
static void test_modpoly_add() {
   {
      uint16 mod[] = { 13 };
      uint16 a[] = { 1, 2, 3, 4, 5 };
      uint16 b[NBEL(a)] = { 2, 3, 4, 5, 6 };
      uint16 r[NBEL(a)];
      memzero(r);
      modpoly_add(r, a, b, NBEL(a)/NBEL(mod), NBEL(mod), mod);
      classert(dump_poly_hex(r, NBEL(r)/NBEL(mod), NBEL(mod)) == "[3, 5, 7, 9, 0xb]");
   }
   {
      uint16 mod[] = { 13 };
      uint16 a[] = { 12, 11, 10, 9, 8 };
      uint16 b[NBEL(a)] = { 2, 3, 4, 5, 6 };
      uint16 r[NBEL(a)];
      memzero(r);
      modpoly_add(r, a, b, NBEL(a)/NBEL(mod), NBEL(mod), mod);
      classert(dump_poly_hex(r, NBEL(r)/NBEL(mod), NBEL(mod)) == "[1, 1, 1, 1, 1]");
   }
   {
      uint8 mod[] = { 251 };
      uint8 a[] = { 200, 201, 202 };
      uint8 b[NBEL(a)] = { 100, 50, 3 };
      uint8 r[NBEL(a)];
      memzero(r);
      modpoly_add(r, a, b, NBEL(a)/NBEL(mod), NBEL(mod), mod);
      classert(dump_poly_hex(r, NBEL(r)/NBEL(mod), NBEL(mod)) == "[0x31, 0, 0xcd]");
   }
}
#endif


// ------ SUBTRACTION ---------------------------------------------------------

// subtract two polynomials with 'sz' coeffs in Z/mod Z, with each coeff 'len' small's long and < 'mod'
// overlapping use is permitted (a -= b usage ok)!

template<typename small>
void modpoly_sub(small* r, const small* a, const small* b, const index sz, const index len, const small* mod) {
   caassert(r != nullptr);
   caassert(a != nullptr);
   caassert(b != nullptr);
   caassert(len > 0);
   caassert(mod != nullptr);
   caassert((small)-1 > 0);  // must be unsigned type

   small* ri = r;
   const small* ai = a;
   const small* bi = b;
   for (index i=0; i<sz; ++i) {
      classert(compu(ai, len, mod, len) < 0);  // a[i] < mod?
      classert(compu(bi, len, mod, len) < 0);  // b[i] < mod?
      const small of = sub_fast(ri, ai, bi, len);
      if (of) {
         CLDEBUG(const small of2 =) add_fast(ri, ri, mod, len);
         classert(of2 != 0);
      }
      classert(compu(ri, len, mod, len) < 0);  // r[i] < mod?
      ri += len;
      ai += len;
      bi += len;
   }
}

#ifdef CL_TEST_CODE
static void test_modpoly_sub() {
   {
      uint16 mod[] = { 13 };
      uint16 a[] = { 2, 3, 4, 5, 6 };
      uint16 b[NBEL(a)] = { 1, 2, 3, 4, 5 };
      uint16 r[NBEL(a)];
      memzero(r);
      modpoly_sub(r, a, b, NBEL(a)/NBEL(mod), NBEL(mod), mod);
      classert(dump_poly_hex(r, NBEL(r)/NBEL(mod), NBEL(mod)) == "[1, 1, 1, 1, 1]");
   }
   {
      uint16 mod[] = { 13 };
      uint16 a[] = { 10, 9, 8 };
      uint16 b[NBEL(a)] = { 12, 9, 6 };
      uint16 r[NBEL(a)];
      memzero(r);
      modpoly_sub(r, a, b, NBEL(a)/NBEL(mod), NBEL(mod), mod);
      classert(dump_poly_hex(r, NBEL(r)/NBEL(mod), NBEL(mod)) == "[0xb, 0, 2]");
   }
}
#endif


// ------ COMPARISON ----------------------------------------------------------

template<typename small>
int comp_poly(const small* a, const index asz, const index alen, const small* b, const index bsz, const index blen) {
   const index minsz = tmin(asz, bsz);
   const small* ai = a;
   const small* bi = b;
   index i = 0;
   for (; i<minsz; ++i) {
      if (compu_nn(ai, alen, bi, blen) != 0) return 1;
      ai += alen;
      bi += blen;
   }
   if (asz > bsz) return normu(ai, (asz - i) * alen) == 0 ? 0 : 1;
   else if (bsz > asz) return normu(bi, (bsz - i) * blen) == 0 ? 0 : 1;
   else return 0;
}

#ifdef CL_TEST_CODE
static void test_comp_poly() {
   {
      uint8 a[] = { 1, 2, 3, 4 };
      uint8 b[] = { 1, 2, 3, 4, 0, 0 };
      classert(comp_poly(a, 4, 1, b, 4, 1) == 0);
      classert(comp_poly(a, 2, 2, b, 2, 2) == 0);
      classert(comp_poly(a, 4, 1, b, 6, 1) == 0);
      classert(comp_poly(a, 2, 2, b, 3, 2) == 0);
      classert(comp_poly(b, 6, 1, a, 4, 1) == 0);
      classert(comp_poly(b, 3, 2, a, 2, 2) == 0);
   }
   {
      uint8 a[] = { 1, 2, 3, 4 };
      uint8 b[] = { 1, 2, 3, 3 };
      classert(comp_poly(a, 4, 1, b, 4, 1) != 0);
      classert(comp_poly(a, 2, 2, b, 2, 2) != 0);
   }
   {
      uint8 a[] = { 1, 2, 3, 4 };
      uint8 b[] = { 1, 2, 3, 4, 1, 0 };
      classert(comp_poly(b, 6, 1, a, 4, 1) != 0);
      classert(comp_poly(b, 3, 2, a, 2, 2) != 0);
   }
}
#endif


// ------ KRONECKER-SCH�NHAGE SUBSTITUTION ------------------------------------

template<typename small>
index poly_to_int_zbits(const index psz) {
   caassert(psz > 0);
   const index zbits = int_ceil_log2(psz);  // number of extra bits needed for convolution sum
   caassert(zbits < bits(index));
   return zbits;
}

template<typename small>
__forceinline index poly_to_int_len(const index psz, const index plen, const index zbits) {
   return bits_to_small<small>((plen * bits(small) + zbits) * psz);
}

// convert polynomial with 'psz' coeffs, each 'plen' smalls long
// into an integer by concaternating the integer coeffs and padding 'zbits' zero bits between coeffs.

template<typename small>
void poly_to_int(small* r, const small* p, const index psz, const index plen, const index zbits) {
   caassert(r != nullptr);
   caassert(p != nullptr);
   caassert(psz > 0);
   caassert(plen > 0);
   caassert(zbits > 0);
   caassert((index)1 << zbits >= psz);

   CLDEBUG(small* rend = r + poly_to_int_len<small>(psz, plen, zbits));
   const small* pi = p;
   small* ri = r;
   unsigned sh = 0;  // shift count
   unsigned sh2 = bits(small);  // inverse shift count
   small accu = 0;
   for (index i=0; i<psz; ++i) {
      if (sh == 0) {  // must handle special case, since >> bits(register_size) doesn't work
         // copy small-aligned part
         copy_in(ri, pi, plen);
         ri += plen;
         pi += plen;
         // zero smalls
         zero_in(ri, zbits/bits(small));
         ri += zbits/bits(small);
         // mark the rest of bits as used in accu
         accu = 0;
         sh = zbits % bits(small);
         sh2 = bits(small) - sh;
      }
      else {
         caassert(sh + sh2 == bits(small));
         // copy smalls bit-shifted
         const small* pend = pi + plen;
         do {  // loop will at least run once, since plen >= 1
            const register small pp = *pi;
            accu |= pp << sh;
            *ri++ = accu;
            accu = pp >> sh2;
         } while (++pi < pend);
         // zero bits
         caassert(sh != 0);
         if (sh + zbits >= bits(small)) {  // more bits than fit into accu
            *ri++ = accu;
            index fullz = (sh + zbits - bits(small)) / bits(small);
            zero_in(ri, fullz);
            ri += fullz;
            accu = 0;
            sh = (sh + zbits) & (bits(small) - 1);
            sh2 = bits(small) - sh;
         }
         else {
            sh += zbits;
            sh2 -= zbits;
         }
      }
   }
   if (sh > 0) *ri++ = accu;
   classert(ri == rend);
}

#ifdef CL_TEST_CODE
static void test_poly_to_int() {
   {
      uint8 a[] = { 1, 2, 3, 4 };
      tape_alloc<uint8> tmp(100);
      uint8* r = tmp.p;
      index zbits = poly_to_int_zbits<uint8>(NBEL(a));
      classert(zbits == 2);  // 2 bits
      poly_to_int(r, a, NBEL(a), 1, 8);  // ignore zbits, shift by small bits size
      classert(dump_hex(r, NBEL(a)*(1+1)) == "0x4000300020001");
   }
   {
      uint16 a[] = { 0xf1f1, 0xf1f1, 0xf2f2, 0xf2f2, 0xf3f3, 0xf3f3 };
      const index alen = 2;
      const index asz = NBEL(a) / alen;
      tape_alloc<uint16> tmp(100);
      uint16* r = tmp.p;
      index zbits = poly_to_int_zbits<uint16>(asz);
      classert(zbits == 2);  // 2 bits
      poly_to_int(r, a, asz, alen, 16);  // ignore zbits, shift by small bits size
      classert(dump_hex(r, asz*(alen+1)) == "0xf3f3f3f30000f2f2f2f20000f1f1f1f1");
   }
   {
      uint8 a[] = { 1, 2, 3, 4 };
      const index alen = 1;
      const index asz = NBEL(a) / alen;
      tape_alloc<uint8> tmp(100);
      uint8* r = tmp.p;
      index zbits = poly_to_int_zbits<uint8>(NBEL(a));
      classert(zbits == 2);  // 2 bits
      poly_to_int(r, a, asz, alen, zbits);
      index rlen = bits_to_small<uint8>(asz * (alen * bits(uint8) + zbits));
      classert(dump_hex(r, rlen) == "0x100300801");
   }
   {
      uint16 a[] = { 0xf1f1, 0xf1f1, 0xf2f2, 0xf2f2, 0xf3f3, 0xf3f3 };
      const index alen = 2;
      const index asz = NBEL(a) / alen;
      tape_alloc<uint16> tmp(100);
      uint16* r = tmp.p;
      index zbits = poly_to_int_zbits<uint16>(asz);
      classert(zbits == 2);  // 2 bits
      poly_to_int(r, a, asz, alen, zbits);
      index rlen = bits_to_small<uint16>(asz * (alen * bits(uint16) + zbits));
      classert(dump_hex(r, rlen) == "0xf3f3f3f33cbcbcbc8f1f1f1f1");
   }
   {
      uint8 a[] = { 1, 2, 3, 4, 5 };
      const index alen = 1;
      const index asz = NBEL(a) / alen;
      tape_alloc<uint8> tmp(100);
      uint8* r = tmp.p;
      index zbits = poly_to_int_zbits<uint8>(NBEL(a));
      classert(zbits == 3);
      poly_to_int(r, a, asz, alen, zbits);
      index rlen = bits_to_small<uint8>(asz * (alen * bits(uint8) + zbits));
      // py: hex(1 + (2 << 8+3) + (3 << 16+6) + (4 << 24+9) + (5 << 32+12))
      classert(dump_hex(r, rlen) == "0x500800c01001");
   }
}
#endif


// convert integer back to polynom
// each packed coefficient has 'pbits' bits and is stretched to 'plen' smalls
// the polynom has 'psz' coeffs
// 'r' is 'plen' * 'psz' smalls long
// input 'a' has to have all data available, i.e. must not be normalized

template<typename small>
void int_to_poly(small* rp, const small* a, const index alen, const index pbits, const index psz, const index plen) {
   caassert(rp != nullptr);
   caassert(a != nullptr);
   caassert(alen > 0);
   caassert(psz > 0);
   caassert(plen > 0);
   caassert(plen * bits(small) >= pbits);
   caassert(pbits * psz <= alen * bits(small));  // all bits must be there, even if they are zero
   classert(pbits * psz >= bit_length(a, alen));  // still, number must not exceed bits * coeffs

   small* ri = rp;
   small* rend = rp + psz * plen;
   const small* ai = a;
   const index fullp = pbits / bits(small);
   const unsigned restp = pbits % bits(small);
   unsigned sh = 0;

   do {
      if (sh == 0) {  // must handle special case, since >> bits(register_size) doesn't work
         copy_in(ri, ai, fullp);
         ai += fullp;
         if (restp != 0) {
            ri[fullp] = *ai & (((small)1 << restp) - 1);
            ri = zero_in(ri, plen, fullp+1);
            sh = restp;
         }
         else ri = zero_in(ri, plen, fullp);
      }
      else {
         // 'ai' points to small that has the upper 'sh2' bits for consumption (lower 'sh' already used)
         unsigned sh2 = bits(small) - sh;
         if (sh2 > pbits) {
            *ri = (*ai >> sh) & (((small)1 << pbits) - 1);
            ri = zero_in(ri, plen, 1);
            sh += pbits;
         }
         else {
            register small accu = *ai++ >> sh;
            const index full = (pbits - sh2) / bits(small);  // number of full smalls from 'a' we will consume
            const small* aend = ai + full;
            while (ai < aend) {
               accu |= *ai << sh2;
               *ri++ = accu;
               accu = *ai++ >> sh;
            }
            // accu has still 'sh2' bits to be saved
            caassert(sh2 > 0 && sh2 < bits(small));
            unsigned missing = pbits - sh2 - full * bits(small);  // number of bits that still need to be read
            if (missing == 0) {  // stopped at small boundary
               *ri++ = accu;
               sh = 0;
               ri = zero_in(ri-full-1, plen, full+1);
            }
            else if (sh2 + missing <= bits(small)) {
               accu |= (*ai & (((small)1 << missing) - 1)) << sh2;
               *ri++ = accu;
               sh = missing;
               ri = zero_in(ri-full-1, plen, full+1);
            }
            else {
               accu |= *ai << sh2;
               *ri++ = accu;
               *ri++ = (*ai & (((small)1 << missing) - 1)) >> sh;
               sh = missing;
               ri = zero_in(ri-full-2, plen, full+2);
            }
         }
      }
   } while (ri < rend);
   classert(ai == a + psz * pbits / bits(small));
   classert(ri == rp + psz * plen);
}

#ifdef CL_TEST_CODE
static void test_int_to_poly() {
   {
      uint8 a[] = { 1, 0xff, 2, 0xff, 3, 0xff, 4, 0xff };
      uint8 r[4*2];
      int_to_poly(r, a, NBEL(a), 16, 4, 2);
      classert(dump_poly_hex(r, 4, 2) == "[0xff01, 0xff02, 0xff03, 0xff04]");
   }
   {
      uint8 a[] = { 1, 0xff, 2, 0xff, 3, 0xff, 4, 0xff };
      uint8 r[4*3];
      int_to_poly(r, a, NBEL(a), 16, 4, 3);
      classert(dump_poly_hex(r, 4, 3) == "[0xff01, 0xff02, 0xff03, 0xff04]");
   }
   {
      uint8 a[] = { 0xf1, 0x2e, 0xef, 0xf3, 0x4e, 0xef };
      uint8 r[4*2];
      int_to_poly(r, a, NBEL(a), 12, 4, 2);
      classert(dump_poly_hex(r, 4, 2) == "[0xef1, 0xef2, 0xef3, 0xef4]");
   }
   {
      uint16 a[] = { 0xfff1, 0xff2e, 0xf3ef, 0x4eff, 0xefff };
      uint16 r[4*2];
      int_to_poly(r, a, NBEL(a), 20, 4, 2);
      classert(dump_poly_hex(r, 4, 2) == "[0xefff1, 0xefff2, 0xefff3, 0xefff4]");
   }
   {
      uint8 a[] = { 0xf0, 0x3f, 0xfe, 0xcb, 0xff, 0xf9, 0xf };
      uint8 r[4*2];
      int_to_poly(r, a, NBEL(a), 13, 4, 2);
      classert(dump_poly_hex(r, 4, 2) == "[0x1ff0, 0x1ff1, 0x1ff2, 0x1ff3]");
   }
   {
      // test conversion with no additional space (zbits == 0)
      uint8 a[] = { 0xf0, 0xf1, 0xf2, 0xf3 };
      uint8 r[4];
      int_to_poly(r, a, NBEL(a), 8, NBEL(r), 1);
      classert(dump_poly_hex(r, NBEL(r), 1) == "[0xf0, 0xf1, 0xf2, 0xf3]");
   }
   {
      // test conversion with pbits < bits(small)
      uint8 a[] = { 0xef, 0xcd };
      uint8 r[4];
      int_to_poly(r, a, NBEL(a), 4, NBEL(r), 1);
      classert(dump_poly_hex(r, NBEL(r), 1) == "[0xf, 0xe, 0xd, 0xc]");
   }
   {
      // test conversion with pbits < 2*bits(small)
      uint8 a[] = { 0xef, 0xcd };
      uint8 r[8];
      int_to_poly(r, a, NBEL(a), 2, NBEL(r), 1);
      classert(dump_poly_hex(r, NBEL(r), 1) == "[3, 3, 2, 3, 1, 3, 0, 3]");
   }
}

template<typename small>
static void test_poly_to_int_and_back2(const index psz, const index plen, const index zbits) {
   tape_alloc<small> tmpa(psz*plen);
   small* pa = tmpa.p;
   rand_fill(pa, psz*plen);

   const index z2bits = poly_to_int_zbits<small>(psz);
   classert(z2bits >= zbits);
   const index rlen = poly_to_int_len<small>(psz, plen, zbits);
   tape_alloc<small> tmpr(rlen);
   small* r = tmpr.p;

   poly_to_int(r, pa, psz, plen, zbits);
   const index p2len = (plen * bits(small) + zbits + bits(small) - 1) / bits(small);
   tape_alloc<small> tmpb(psz*p2len);
   small* pb = tmpb.p;
   int_to_poly(pb, r, rlen, plen * bits(small) + zbits, psz, p2len);
   //tout_puts(format("in =%?\n") << dump_poly_hex(pa, psz, plen));
   //tout_puts(format("out=%?\n") << dump_poly_hex(pb, psz, p2len));
   classert(comp_poly(pa, psz, plen, pb, psz, p2len) == 0);
}

static void test_poly_to_int_and_back() {
   test_poly_to_int_and_back2<uint8>(4, 1, 2);
   test_poly_to_int_and_back2<uint8>(4, 2, 2);
   test_poly_to_int_and_back2<uint8>(4, 3, 2);
   test_poly_to_int_and_back2<uint8>(17, 1, 5);
   test_poly_to_int_and_back2<uint8>(17, 2, 5);
   test_poly_to_int_and_back2<uint8>(17, 3, 5);

   test_poly_to_int_and_back2<uint32>(4, 1, 2);
   test_poly_to_int_and_back2<uint32>(4, 2, 2);
   test_poly_to_int_and_back2<uint32>(4, 3, 2);
   test_poly_to_int_and_back2<uint32>(17, 1, 5);
   test_poly_to_int_and_back2<uint32>(17, 2, 5);
   test_poly_to_int_and_back2<uint32>(17, 3, 5);
}
#endif


// ------ POLYNOMIAL MULTIPLICATION -------------------------------------------

template<typename small>
index modpoly_mul_mod_mp1_size(const index sz, const index len) {
   index zbits = poly_to_int_zbits<small>(sz);  // zero bits needed to avoid overflow
   index mbits = zbits + 2 * len * bits(small);  // bits needed for convolution product
   index ilen = (mbits * sz + bits(small) - 1) / bits(small);  // length of input integers
   return ilen;
}


// multiply two polynomials with 'sz' coeffs in Z/mod Z, with each coeff 'len' small's long and < 'mod'
// intermediate result has 2*'sz' coeffs and is reduced mod (x^m+1), i.e. x^m == -1
// 'r' may be 'a' or 'b' !

template<typename small, bool square>
void q_modpoly_mul_mod_mp1(small* r, const small* a, const small* b, const index sz, const index len, const small* mod) {
   PROF(dkss_prof.start(profid_modpoly_mul));
   caassert(r != nullptr);
   caassert(a != nullptr);
   caassert(b != nullptr);
   caassert(len > 0);
   caassert(mod != nullptr);
   caassert(!square || a == b);
   caassert((small)-1 > 0);  // must be unsigned type
   classert((basic_bignum<small, false>(mod, len) != 0));

   index zbits = poly_to_int_zbits<small>(sz);  // zero bits needed to avoid overflow
   index mbits = zbits + 2 * len * bits(small);  // bits needed for convolution product
   index ilen = (mbits * sz + bits(small) - 1) / bits(small);  // length of input integers
   index ipclen = (mbits + bits(small) - 1) / bits(small);  // intermediate result polynomial coeff len
   tape_alloc<small> tmp((square ? 3 : 4) * ilen + ipclen * 2 * sz + ilen);
   small* ia = tmp.p;                     // length ilen
   small* ib = square ? ia : ia + ilen;   // length ilen
   small* ir = ib + ilen;                 // length 2*ilen
   small* ip = ir + 2*ilen;               // length ipclen*2*sz
   small* mod_sh = ip + ipclen * 2 * sz;  // length ilen

   // convert to integer
   poly_to_int(ia, a, sz, len, mbits - len * bits(small));
   if (!square) poly_to_int(ib, b, sz, len, mbits - len * bits(small));

   // mul integers
   PROF(dkss_prof.start(profid_modpoly_mul_imul));
   smul_tsq<small, square>(ir, ia, ilen, square ? ia : ib, ilen);
   PROF(dkss_prof.stop(profid_modpoly_mul_imul));

   // convert integer result to polynomial
   int_to_poly(ip, ir, 2 * ilen, mbits, 2 * sz, ipclen);

   // prepare shifted mod
   shl(mod_sh, ilen, mod, len, mbits - len * bits(small));  // mod_sh >= sz * mod^2
   classert((basic_bignum<small,false>(mod_sh, ilen) >= basic_bignum<small,false>(mod, len).squ() * sz));

   PROF(dkss_prof.start(profid_modpoly_mul_mod));
   small* ipi = ip;  // pointer to low 'sz' coeffs
   const small* ipi2 = ip + sz * ipclen;  // pointer to high 'sz' coeffs
   small* ri = r;
   for (index i=0; i<sz; ++i) {
      const small of = sub_fast(ipi, ipi, ipi2, ipclen);  // ip[i] -= ip[i+sz]
      if (of)  // we had underflow, add a multiple of the modulus larger than any absolute difference could be
         addu_on(ipi, ipclen, mod_sh, ipclen);
      modu(ri, len, ipi, ipclen, mod, len);
      ipi += ipclen;
      ipi2 += ipclen;
      ri += len;
   }
   PROF(dkss_prof.stop(profid_modpoly_mul_mod));
   PROF(dkss_prof.stop(profid_modpoly_mul));
}

template<typename small>
void modpoly_mul_mod_mp1(small* r, const small* a, const small* b, const index sz, const index len, const small* mod) {
   q_modpoly_mul_mod_mp1<small, false>(r, a, b, sz, len, mod);
}

template<typename small>
void modpoly_squ_mod_mp1(small* r, const small* a, const index sz, const index len, const small* mod) {
   q_modpoly_mul_mod_mp1<small, true>(r, a, a, sz, len, mod);
}


#ifdef CL_TEST_CODE
static void test_modpoly_mul_mod_mp1() {
   {
      uint8 a[] = { 2, 3 };
      uint8 b[] = { 5, 7 };
      uint8 mod[] = { 251 };
      uint8 r[NBEL(a)];
      modpoly_mul_mod_mp1<uint8>(r, a, b, NBEL(a), 1, mod);
      uint8 rc[] = { 240, 29 };
      classert(comp_poly(r, NBEL(r), 1, rc, NBEL(rc), 1) == 0);
   }
   {
      uint8 a[] = { 241, 229 };
      uint8 b[] = { 239, 233 };
      uint8 mod[] = { 251 };
      uint8 r[NBEL(a)];
      modpoly_mul_mod_mp1<uint8>(r, a, b, NBEL(a), 1, mod);
      uint8 rc[] = { 226, 193 };  // (241 + 229x)(239 + 233x) = 53357 x^2+110884 x+57599.  _ mod (x^2 + 1) = 4242 + 110884 x
      //DTOUT_PUTS(dump_poly_hex(r, NBEL(a), 1));
      classert(comp_poly(r, NBEL(r), 1, rc, NBEL(rc), 1) == 0);
   }
   {
      uint8 a[] = { 191, 197, 229, 223, 199, 241 };  // 50623 + 57317 x + 61895 x^2
      uint8 b[] = { 193, 181, 233, 227, 211, 239 };  // 46529 + 58345 x + 61395 x^2
      uint8 mod[] = { 0xf1, 0xff };  // 65521
      uint8 r[NBEL(a)];
      modpoly_mul_mod_mp1<uint8>(r, a, b, NBEL(a)/NBEL(mod), NBEL(mod), mod);
      uint8 rc[] = { 0x1c, 0x9a, 0x6f, 0x58, 0x45, 0xb7 };  // 3800043525 x^4 + 7130240990 x^3 + 9332071905 x^2 + 5620501628 x + 2355437567  \equiv  -4774803423 + 1820458103 x + 9332071905 x^2  \equiv  39452 + 22639 x + 46917 x^2
      //DTOUT_PUTS(dump_poly_hex(r, NBEL(a)/NBEL(mod), NBEL(mod)));
      classert(comp_poly(r, NBEL(r)/NBEL(mod), NBEL(mod), rc, NBEL(rc)/NBEL(mod), NBEL(mod)) == 0);
   }
   {
      uint8 a[] = { 191, 197, 229, 223, 199, 241 };  // 50623 + 57317 x + 61895 x^2
      uint8 mod[] = { 0xf1, 0xff };  // 65521
      uint8 r[NBEL(a)];
      modpoly_squ_mod_mp1<uint8>(r, a, NBEL(a)/NBEL(mod), NBEL(mod), mod);
      uint8 rc[] = { 0x15, 0x6f, 0xa2, 0x24, 0xc4, 0x2d };
      //DTOUT_PUTS(dump_poly_hex(r, NBEL(a)/NBEL(mod), NBEL(mod)));
      classert(comp_poly(r, NBEL(r)/NBEL(mod), NBEL(mod), rc, NBEL(rc)/NBEL(mod), NBEL(mod)) == 0);
   }
}
#endif


// ------ POLYNOMIAL EXPONENTIATION -------------------------------------------

// put polynomial with 'sz' coeffs in Z/mod Z, with each coeff 'len' small's long and < 'mod' to the 'exp' power
// result is reduced mod (x^m+1), i.e. x^m == -1
// 'r' may be 'a' !

template<typename small>
void modpoly_pow_mod_mp1(small* r, const small* a, index exp, const index sz, const index len, const small* mod) {
   caassert(r != nullptr);
   caassert(a != nullptr);
   caassert(exp >= 0);
   caassert(sz > 0);
   caassert(len > 0);
   caassert(mod != nullptr);

   zero(r, sz * len);
   r[0] = 1;
   if (exp <= 0) return;

   tape_alloc<small> tmp(sz * len);
   small* base = tmp.p;
   copy(base, a, sz * len);

   while (1) {
      if ((exp & 1) != 0) modpoly_mul_mod_mp1(r, r, base, sz, len, mod);  // r *= base
      exp >>= 1;
      if (exp == 0) break;
      modpoly_squ_mod_mp1(base, base, sz, len, mod);  // base = base^2
   }
}

#ifdef CL_TEST_CODE
static void test_modpoly_pow_mod_mp1() {
   {
      uint8 a[] = { 2, 3 };
      uint8 mod[] = { 251 };
      uint8 r[NBEL(a)];
      modpoly_pow_mod_mp1<uint8>(r, a, 0, NBEL(a), 1, mod);
      uint8 rc[] = { 1, 0 };
      classert(comp_poly(r, NBEL(r), 1, rc, NBEL(rc), 1) == 0);
   }
   {
      uint8 a[] = { 2, 3 };
      uint8 mod[] = { 251 };
      uint8 r[NBEL(a)];
      modpoly_pow_mod_mp1<uint8>(r, a, 1, NBEL(a), 1, mod);
      uint8 rc[] = { 2, 3 };
      classert(comp_poly(r, NBEL(r), 1, rc, NBEL(rc), 1) == 0);
   }
   {
      uint8 a[] = { 2, 3 };
      uint8 mod[] = { 251 };
      uint8 r[NBEL(a)];
      modpoly_pow_mod_mp1<uint8>(r, a, 2, NBEL(a), 1, mod);
      uint8 rc[] = { 246, 12 };
      classert(comp_poly(r, NBEL(r), 1, rc, NBEL(rc), 1) == 0);
   }
   {
      uint8 a[] = { 211, 129 };
      uint8 mod[] = { 251 };
      // 35723051649 x^5 + 292153639455 x^4 + 955727409690 x^3 + 1563244057710 x^2 + 1278467039445 x + 418227202051
      // 235 x^5 + 233 x^4 + 17 x^3 + 236 x^2 + 14 x + 219
      // -235 x^3 - 233 x^2 - 17 x - 236 + 14 x + 219
      // 235 x + 233 - 17 x - 236 + 14 x + 219
      // 232 x + 216
      uint8 r[NBEL(a)];
      modpoly_pow_mod_mp1<uint8>(r, a, 5, NBEL(a), 1, mod);
      uint8 rc[] = { 216, 232 };
      classert(comp_poly(r, NBEL(r), 1, rc, NBEL(rc), 1) == 0);
   }
}
#endif


// negate in Z/(mod Z)

template<typename small>
inline void negmod_fast(small* r, const small* mod, const small* a, const index len) {
   caassert(r != nullptr);
   caassert(mod != nullptr);
   caassert(a != nullptr);

   if (normu(a, len) == 0) zero_in(r, len);
   else sub_fast(r, mod, a, len);
}

#ifdef CL_TEST_CODE
static void test_negmod_fast() {
   {
      uint8 a[] = { 2, 3 };
      uint8 mod[] = { 0xf1, 0xff };  // 65521 is prime
      uint8 r[NBEL(a)];
      negmod_fast<uint8>(r, mod, a, NBEL(a));
      uint8 rc[] = { 0xef, 0xfc };
      classert(compu_nn(r, NBEL(r), rc, NBEL(rc)) == 0);
   }
   {
      uint8 a[] = { 0xef, 0xfc };
      uint8 mod[] = { 0xf1, 0xff };  // 65521 is prime
      uint8 r[NBEL(a)];
      negmod_fast<uint8>(r, mod, a, NBEL(a));
      uint8 rc[] = { 2, 3 };
      classert(compu_nn(r, NBEL(r), rc, NBEL(rc)) == 0);
   }
   {
      uint8 a[] = { 0, 0 };
      uint8 mod[] = { 0xf1, 0xff };  // 65521 is prime
      uint8 r[NBEL(a)];
      negmod_fast<uint8>(r, mod, a, NBEL(a));
      uint8 rc[] = { 0, 0 };
      classert(compu_nn(r, NBEL(r), rc, NBEL(rc)) == 0);
   }
}
#endif


// ------ POLYNOMIAL MUL BY POWER OF x ----------------------------------------

// multiply polynomial by power of x mod (x^m+1)
// therefore x^m == -1 and hence coeffs shifted out at the upper end will enter negative on the low end
// r and a MUST NOT overlap!

template<typename small>
void modpoly_mul_xpow_mod_mp1(small* r, const small* a, const index pow, const index m, const index len, const small* mod) {
   caassert(r != nullptr);
   caassert(a != nullptr);
   caassert(pow < 2*m);  // otherwise, use pow %= 2*m
   caassert(mod != nullptr);
   caassert(r != a);

   if (pow == 0) {
      copy(r, a, m*len);
      return;
   }
   if (pow < m) {
      copy(r + pow * len, a, (m - pow) * len);
      const small* ai = a + (m - pow) * len;
      small* ri = r;
      for (index i=0; i<pow; ++i) {
         negmod_fast(ri, mod, ai, len);
         ai += len;
         ri += len;
      }
      classert(ai == a + m * len);
   }
   else {  // pow >= m
      const small* ai = a;
      small* ri = r + (pow - m) * len;
      for (index i=0; i<2*m-pow; ++i) {
         negmod_fast(ri, mod, ai, len);
         ai += len;
         ri += len;
      }
      classert(ri == r + m * len);
      copy(r, ai, (pow - m) * len);
   }
}

#ifdef CL_TEST_CODE
static void test_modpoly_mul_xpow_mod_mp1() {
   {
      uint8 a[] = { 2, 3, 0, 7 };
      uint8 mod[] = { 251 };
      uint8 r[NBEL(a)];
      modpoly_mul_xpow_mod_mp1<uint8>(r, a, 0, NBEL(a), 1, mod);
      uint8 rc[] = { 2, 3, 0, 7 };
      classert(comp_poly(r, NBEL(r), 1, rc, NBEL(rc), 1) == 0);
   }
   {
      uint8 a[] = { 2, 3, 0, 7 };
      uint8 mod[] = { 251 };
      uint8 r[NBEL(a)];
      modpoly_mul_xpow_mod_mp1<uint8>(r, a, 1, NBEL(a), 1, mod);
      uint8 rc[] = { 251-7, 2, 3, 0 };
      classert(comp_poly(r, NBEL(r), 1, rc, NBEL(rc), 1) == 0);
   }
   {
      uint8 a[] = { 2, 3, 0, 7 };
      uint8 mod[] = { 251 };
      uint8 r[NBEL(a)];
      modpoly_mul_xpow_mod_mp1<uint8>(r, a, 2, NBEL(a), 1, mod);
      uint8 rc[] = { 0, 251-7, 2, 3 };
      classert(comp_poly(r, NBEL(r), 1, rc, NBEL(rc), 1) == 0);
   }
   {
      uint8 a[] = { 2, 3, 0, 7 };
      uint8 mod[] = { 251 };
      uint8 r[NBEL(a)];
      modpoly_mul_xpow_mod_mp1<uint8>(r, a, 3, NBEL(a), 1, mod);
      uint8 rc[] = { 251-3, 0, 251-7, 2 };
      classert(comp_poly(r, NBEL(r), 1, rc, NBEL(rc), 1) == 0);
   }
   {
      uint8 a[] = { 2, 3, 0, 7 };
      uint8 mod[] = { 251 };
      uint8 r[NBEL(a)];
      modpoly_mul_xpow_mod_mp1<uint8>(r, a, 4, NBEL(a), 1, mod);
      uint8 rc[] = { 251-2, 251-3, 0, 251-7 };
      classert(comp_poly(r, NBEL(r), 1, rc, NBEL(rc), 1) == 0);
   }
   {
      uint8 a[] = { 2, 3, 0, 7 };
      uint8 mod[] = { 251 };
      uint8 r[NBEL(a)];
      modpoly_mul_xpow_mod_mp1<uint8>(r, a, 5, NBEL(a), 1, mod);
      uint8 rc[] = { 7, 251-2, 251-3, 0 };
      classert(comp_poly(r, NBEL(r), 1, rc, NBEL(rc), 1) == 0);
   }
   {
      uint8 a[] = { 2, 3, 0, 7 };
      uint8 mod[] = { 251 };
      uint8 r[NBEL(a)];
      modpoly_mul_xpow_mod_mp1<uint8>(r, a, 6, NBEL(a), 1, mod);
      uint8 rc[] = { 0, 7, 251-2, 251-3 };
      classert(comp_poly(r, NBEL(r), 1, rc, NBEL(rc), 1) == 0);
   }
   {
      uint8 a[] = { 2, 3, 0, 7 };
      uint8 mod[] = { 251 };
      uint8 r[NBEL(a)];
      modpoly_mul_xpow_mod_mp1<uint8>(r, a, 7, NBEL(a), 1, mod);
      uint8 rc[] = { 3, 0, 7, 251-2 };
      classert(comp_poly(r, NBEL(r), 1, rc, NBEL(rc), 1) == 0);
   }
   {
      uint8 a[] = { 5, 0xb1 };
      uint8 mod[] = { 0xc1 };
      uint8 r[NBEL(a)];
      modpoly_mul_xpow_mod_mp1<uint8>(r, a, 1, NBEL(a), 1, mod);
      uint8 rc[] = { 0x10, 5 };
      classert(comp_poly(r, NBEL(r), 1, rc, NBEL(rc), 1) == 0);
   }
}
#endif


// $dissect$ dkssmul.txt
// ------ DKSS MUL ------------------------------------------------------------

template<typename small>
void dkss_set_mmu(dkss_parm<small>& parm, const index alen, const index blen) {
   classert(alen > 0);
   classert(blen > 0);
   CLDEBUG(const index max_len = parm.max_len);  // number of bits of result 2N must fit in 'index' type
   classert(alen < max_len);
   classert(blen < max_len);

   parm.N = t2max(alen, blen) * bits(small);       // input size in bits
   caassert(parm.N >= 4);  // log log N must be >= 1
   const double logN = log2(parm.N);
   const double loglogN = log2(logN);
   parm.logN = t2max(ceil(logN), 1);
   classert(parm.logN > 0);

   // we need a ring with 2M|p-1.  but also we need p^c >= Mm/2*2^(2u) ~ 1/2 N^5 / log N
   const double pc_bits = 5.0 * logN - loglogN - 1;
   const unsigned min_bits = dkss_pc_bits_roundup ? 
      ceil(pc_bits / bits(small)) * bits(small) :  // round up to small size
      pc_bits;
   auto ring = dkss_find_pc(min_bits);
   parm.p = ring->make_prime();
   parm.c = 1;
   parm.pc = parm.p;
   parm.zeta = ring->generator;
   //DTOUT_PUTS(format("p=%? zeta=%?\n") << parm.p << parm.zeta);

   // find the best fitting values for u, M, m
   // first find a fitting u
   caassert(ring->prime_bit_length >= 5);
   index u = (ring->prime_bit_length - 1) / 2 - 1;  // sub 1, otherwise logmm is surely too small
   index logmm = ring->prime_bit_length - 1 - 2*u;
   while (u >= 2) {
      classert(logmm + 2*u == ring->prime_bit_length - 1);  // must add up
      caassert(logmm <= int_log2(numeric_limits<index>::max() / u));  // make sure we don't overflow
      caassert(logmm > 0);
      const index max_N = u << (logmm - 1);  // maximum N we can handle with these settings
      if (parm.N <= max_N) break;  // it fits!
      caassert(u > 1);
      --u;
      logmm += 2;
   }
   classert(parm.pc > ((index)1 << (logmm - 1)) * (basic_bignum<small,false>::pow2(u) - 1).squ());  // max value for an inner coeff is M(m/2)(2^u-1)^2
   // see if logmm can be lowered
   while (logmm >= 1) {
      caassert(logmm > 1);
      --logmm;
      const index max_N = u << (logmm - 1);  // maximum N we can handle with these settings
      if (max_N < parm.N) break;  // it no longer fits!
   }
   ++logmm;
   classert(parm.pc > ((index)1 << (logmm - 1)) * (basic_bignum<small,false>::pow2(u) - 1).squ());  // max value for an inner coeff is M(m/2)(2^u-1)^2
   parm.u = u;

   // split logmm into logM and logm
   const double q = parm.N / pow(logN, 3);  // quotient M/m
   const unsigned logq = int_log2((index)(q * 1.4142)) + 0;  // floor(log2(x * sqrt(2))) rounds to nearest int; +0 seems to work quite fast
   if (logq >= logmm) {
      parm.logm = 1;
      parm.logM = t2max(logmm, 2) - parm.logm;
   }
   else {
      parm.logm = (logmm - logq) / 2;  // round down
      parm.logM = logmm - parm.logm;
   }
   // if p-1 doesn't contain a high enough power of 2, redistribute between M and m
   if (parm.logM + 1 > ring->pow_2) {
      const index v = (parm.logM - ring->pow_2) / 2;
      parm.logM -= v;
      parm.logm += v;
   }

   parm.m = (index)1 << parm.logm;              // ~ log N, power of 2
   caassert(parm.m >= 2);
   classert(is_pow2(parm.m));

   parm.M = (index)1 << parm.logM;              // ~ N / log^2 N, power of 2
   caassert(parm.M >= 1);
   classert(is_pow2(parm.M));

   classert(parm.M >= parm.m);

   classert(parm.M*parm.u*parm.m/2 >= parm.N);
   classert((parm.p - 1) % (basic_bignum<small,false>(parm.M) * 2) == 0);
   classert(parm.pc > parm.M * parm.m / 2 * (basic_bignum<small,false>::pow2(parm.u) - 1).squ());  // max value for an inner coeff is M(m/2)(2^u-1)^2
}

#ifdef CL_TEST_CODE
template<typename small>
void dkss_set_mmu_dbg(dkss_parm<small>& parm, const index N, const index M, const index m, const index u, const char* prime_str, const small c, const small zeta) {
   classert(N > 1);

   caassert(m >= 2);
   classert(is_pow2(m));

   caassert(M >= 1);
   classert(is_pow2(M));

   classert(M*u*m/2 >= N);

   basic_bignum<small,false> p = s_to<basic_bignum<small,false>>(prime_str);
   classert((p - 1) % (M * 2) == 0);  // 2M | (p-1)
   classert(p.pow(c) > M * m / 2 * (basic_bignum<small,false>::pow2(u) - 1).squ());  // max value for an inner coeff is M(m/2)(2^u-1)^2

   parm.N = N;
   parm.logN = int_ceil_log2(N);
   parm.M = M;
   parm.logM = int_log2(M);
   parm.m = m;
   parm.logm = int_log2(m);
   parm.u = u;
   parm.p = p;
   parm.c = c;
   parm.pc = parm.p.pow(c);
   parm.zeta = zeta;
}
#endif


#ifndef NDEBUG
template<typename T>
static void check_primitivity(T _a, T _exp, T _n) {
   ubignum_t a = _a, exp = _exp, n = _n;
   classert(a.mod_pow(exp, n) == 1);  // otherwise it's no root at all
   ubignum_t ord = n - 1;
   classert(ord % exp == 0);  // order of root must divide group order

   vector<pair<ubignum_t, index>> fs;
   factorize(exp, fs);  // factorize order of root

   size_t i = 0;
   for (; i<fs.size(); ++i) {  // cycle through all prime factors
      auto& par = fs[i];
      auto& q = par.first;
      classert(exp % q == 0);
      classert(a.mod_pow(exp / q, n) != 1);  // otherwise it's not primitive, since a power < exp-1
   }
}
#endif

#ifdef CL_TEST_CODE
static void test_check_primitivity() {
   {
      int cnt = 0;
      dbg_assert_cnt = &cnt;
      check_primitivity<ubignum_t>(2, 4, 6);  // 2^4 == 4 (mod 6), hence no root
      dbg_assert_cnt = nullptr;
      classert(cnt > 0);
   }
   {
      int cnt = 0;
      dbg_assert_cnt = &cnt;
      check_primitivity<ubignum_t>(2, 8, 5);  // 2^8 == 1 (mod 5), but order is too high
      dbg_assert_cnt = nullptr;
      classert(cnt > 0);
   }
   {
      int cnt = 0;
      dbg_assert_cnt = &cnt;
      check_primitivity<ubignum_t>(2, 4, 5);  // 2^4 == 1 (mod 5) and primitive
      dbg_assert_cnt = nullptr;
      classert(cnt == 0);
   }
   {
      int cnt = 0;
      dbg_assert_cnt = &cnt;
      check_primitivity<ubignum_t>(4, 4, 5);  // 4^4 == 1 (mod 5), but not primitive
      dbg_assert_cnt = nullptr;
      classert(cnt > 0);
   }
}
#endif


template<typename small>
void dkss_set_parm2(dkss_parm<small>& parm) {
   if (!(
      (parm.logN > 0) &&
      (parm.m >= 2) &&
      (is_pow2(parm.m)) &&
      (parm.M >= 1) &&
      (is_pow2(parm.M)) &&
      (parm.M >= parm.m) &&
      (parm.M*parm.u*parm.m/2 >= parm.N) &&
      ((parm.p - 1) % (parm.M * 2) == 0) &&  // 2M | (p-1)
      (parm.pc > parm.M * parm.m / 2 * (basic_bignum<small,false>::pow2(parm.u) - 1).squ()) &&  // max value for an inner coeff is M(m/2)(2^u-1)^2
      1)) {
      terr_puts(format("dkss parameter error: N=%? M=%? m=%? u=%? p=%?\n") << parm.N << parm.M << parm.m << parm.u << parm.p);
   }

   // length of inner coeff
   parm.iclen = (parm.pc.bit_length() + bits(small) - 1) / bits(small);
   classert(parm.iclen * bits(small) >= parm.logM + parm.logm + 2 * parm.u);
   parm.oclen = parm.iclen << parm.logm;  // m * iclen
   parm.polylen = parm.oclen << (parm.logM + 1);  // 2M * m * iclen
   parm.pc.reserve(parm.iclen);  // make sure pc has length iclen in memory

   // calculate 2M-th root of unity
#ifndef NDEBUG
   check_primitivity(parm.zeta, parm.p - 1, parm.p);
#endif
   parm.omega = parm.zeta.mod_pow((parm.p - 1) / (2 * parm.M), parm.pc);
   //DTOUT_PUTS(format("omega=%?\n") << parm.omega);
#ifndef NDEBUG
   check_primitivity(parm.omega, (basic_bignum<small,false>)(2*parm.M), parm.p);
#endif
   tape_alloc<small> tmp(2 * parm.oclen);

   // calculate rho
   {
      basic_bignum<small,false> gamma = parm.omega.mod_pow(parm.M / parm.m, parm.pc);
      small* prod = tmp.p;  // product
      small* numer = tmp.p + parm.oclen;  // numerator of polynomial fraction
      bignum_free<small>(parm.rho);
      parm.rho = bignum_alloc<small>(parm.oclen);
      zero(parm.rho, parm.oclen);

      caassert(parm.m >= 2);
      for (index i = 1; i < 2*parm.m; i += 2) {
         // init prod to 1
         zero(prod, parm.oclen);
         basic_bignum<small,false> opi = parm.omega.mod_pow(i, parm.pc);  // omega^i
         opi.copy_to(prod, parm.iclen);  // init prod to omega^i
         basic_bignum<small,false> gpi = gamma.mod_pow(i, parm.pc);  // gamma^i
         // multiply up the whole product
         basic_bignum<small,false> gamma2 = gamma.mod_pow(2, parm.pc);  // gamma^2
         basic_bignum<small,false> gpowj = gamma;  // init to gamma^1
         for (index j = 1; j < 2*parm.m; j += 2) {
            if (j != i) {
               // calculate inverse to denominator
               basic_bignum<small,false> denom = gpi > gpowj ? gpi - gpowj : parm.pc +  gpi - gpowj;  // (gamma^i - gamma^j) % p^c
               basic_bignum<small,false> inv_denom = denom.mod_inv(parm.pc);  // 1 / ((gamma^i - gamma^j) % p^c)
               // set numerator to alpha - gamma^j
               zero(numer, parm.oclen);
               classert(gpowj == gamma.mod_pow(j, parm.pc));
               basic_bignum<small,false> g = ((parm.pc - gpowj) * inv_denom) % parm.pc;  // (- gamma^j) / (gamma^i - gamma^j)
               classert(g.bit_length() <= parm.iclen * bits(small));
               g.copy_to(numer, parm.iclen);  // numer absolute coeff
               inv_denom.copy_to(numer + parm.iclen, parm.iclen);  // numer linear coeff: alpha / (gamma^i - gamma^j)
               modpoly_mul_mod_mp1(prod, prod, numer, parm.m, parm.iclen, parm.pc.ptr);
            }
            gpowj = (gpowj * gamma2) % parm.pc;  // set to gamma^(j+2)
         }
         modpoly_add(parm.rho, parm.rho, prod, parm.m, parm.iclen, parm.pc.ptr);
      }
   }
#ifndef NDEBUG
   parm.assert_rho();
#endif

   // precompute powers of rho in [0 : M/m-1]
   const index mu = parm.M / parm.m;
   bignum_free<small>(parm.rho_pow);
   parm.rho_pow = bignum_alloc<small>(mu * parm.oclen);
   small* rhop = parm.rho_pow;
   zero(rhop, parm.oclen);
   rhop[0] = 1;
   for (index i=1; i<mu; ++i) {
      rhop += parm.oclen;
      modpoly_mul_mod_mp1(rhop, rhop - parm.oclen, parm.rho, parm.m, parm.iclen, parm.pc.ptr);  // rho_pow[i] = rho_pow[i-1] * rho
   }
#ifndef NDEBUG
   // rho^(M/m) must be \alpha
   small* t = tmp.p;
   modpoly_mul_mod_mp1(t, rhop, parm.rho, parm.m, parm.iclen, parm.pc.ptr);  // t = rho_pow[M/m-1] * rho
   for (index i=0; i<parm.m; ++i)
      classert(compu1_nn<small>(t + i*parm.iclen, parm.iclen, i == 1 ? 1 : 0) == 0);
#endif

   parm.len_inv = basic_bignum<small,false>(2*parm.M).mod_inv(parm.pc);  // 1 / (2*M) (mod p^c)
   parm.len_inv.reserve(parm.iclen);
   classert((parm.len_inv * 2 * parm.M) % parm.pc == 1);

   parm.do_assert();
}

template<typename small>
void dkss_set_parm(dkss_parm<small>& parm, const index alen, const index blen) {
   dkss_set_mmu<small>(parm, alen, blen);
   dkss_set_parm2<small>(parm);
}

#ifndef NDEBUG
template<typename small>
void dkss_parm<small>::assert_rho() const {
   tape_alloc<small> tmp(oclen);
   small* r = tmp.p;

   // rho^(2M/2m) == alpha
   index exp = M/m;
   caassert(exp >= 1);
   modpoly_pow_mod_mp1(r, rho, exp, m, iclen, pc.ptr);
   for (index i=0; i<m; ++i)
      classert(compu1_nn<small>(r+i*iclen, iclen, i == 1 ? 1 : 0) == 0);

   // rho^(2M) == 1
   modpoly_pow_mod_mp1(r, r, m, m, iclen, pc.ptr);
   for (index i=0; i<m; ++i)
      classert(compu1_nn<small>(r+i*iclen, iclen, i == 0 ? 1 : 0) == 0);
}
#endif

#ifdef CL_TEST_CODE
static void test_dkss_set_parm() {
   {
      // M > m
      dkss_parm<uint32> p;
      //dkss_set_parm<uint32>(p, 7, 6);
      dkss_set_mmu_dbg<uint32>(p, 224, 16, 8, 4, "65537", 1, 3);
      dkss_set_parm2<uint32>(p);
      // find_root.py 65537 1 16 8 1 3
      classert(p.omega == 65529);
      string s = dump_poly<uint32>(p.rho, p.m, p.iclen);
      classert(s == "[11443, 54094, 13133, 54614, 11083, 11083, 54614, 13133]");
   }
   {
      // M == m
      dkss_parm<uint32> p;
      //dkss_set_parm<uint32>(p, 7, 6);
      dkss_set_mmu_dbg<uint32>(p, 224, 8, 8, 7, "23068673", 1, 3);
      dkss_set_parm2<uint32>(p);
      // find_root.py 23068673 1 8 8 1 3
      classert(p.omega == 3028610);
      string s = dump_poly<uint32>(p.rho, p.m, p.iclen);
      classert(s == "[0, 1, 0, 0, 0, 0, 0, 0]");
   }
   {
      dkss_parm<uint32> p;
      //dkss_set_parm<uint32>(p, 700, 699);
      dkss_set_mmu_dbg<uint32>(p, 22400, 128, 16, 22, "1945555039024054273", 1, 5);
      classert((i_to<16,i_to_charset,string>(p.pc) == "1b00000000000001"));
      dkss_set_parm2<uint32>(p);
      // find_root.py 1945555039024054273 1 128 16 1 5
      classert(p.iclen == 2);
      classert(p.oclen == 32);
      classert(p.omega == 31389055562920820);
      classert(i_to<string>(basic_bignum<uint32,false>(p.pc.ptr, p.iclen)) == "1945555039024054273");
      string s = dump_poly<uint32>(p.rho, p.m, p.iclen);
      p.assert_rho();
      classert(s == "[1288504133045182581, 186585447698650069, 1124121237588314549, 1560581842953934569, 713917599046357089, 499424201089704706, 775188711215603869, 1311885261441114696, 826939695846383220, 1438622351875484012, 752003246901035892, 836033296247715662, 417915744154051186, 634176771540403411, 466712076607158559, 1848626506555227986]");
   }
   {
      dkss_parm<uint32> p;
      //dkss_set_parm<uint32>(p, 1300000, 1300001);
      dkss_set_mmu_dbg<uint32>(p, 41600032, 65536, 32, 40, "294094939252949221147235143647233", 1, 3);
      dkss_set_parm2<uint32>(p);
   }
}
#endif


// ------ DKSS - encode number as double structure polynomial -----------------

template<typename small>
void dkss_encode(small* rp, const small* a, const index alen, const index M, const index m, const index u, const index clen) {
   classert(is_pow2(M));
   caassert(m >= 2);
   classert(is_pow2(m));
   classert(clen * bits(small) >= 2*u + int_log2(M) + int_log2(m));
   caassert((bit_index)M * m * u / 2 >= (bit_index)alen * bits(small));

   const index m_half = m/2;
   const index um_half = u * m_half;  // bits per outer coeff

   // make sure 'a' is zero padded at the top
   const index azlen = (um_half * M + bits(small) - 1) / bits(small);
   tape_alloc_maybe<small> tmp_az(azlen > alen, azlen);
   const small* az;
   if (azlen > alen) {
      copy(tmp_az.p, a, alen);
      zero(tmp_az.p, azlen, alen);
      az = tmp_az.p;
   }
   else az = a;

   // snip 'a' into 'M' parts (outer coeffs) each 'um_half' bit long, stored each in 'umh_len' smalls
   const index umh_len = (um_half + bits(small) - 1) / bits(small);
   tape_alloc<small> tmp_oc(umh_len * M);
   small* oc = tmp_oc.p;
   int_to_poly(oc, az, azlen, um_half, M, umh_len);

   // distribute each outer coeff to 'm/2' inner coeffs each 'u' bit long and stored in 'cnum' smalls
   small* rpi = rp;
   const small* oci = oc;
   for (index i=0; i<M; ++i) {
      // distribute the outer coeff at 'oci' (stored in 'umh_len' smalls)
      // into 'm/2' pieces, each 'u' bit long and stored in 'clen' smalls
      int_to_poly(rpi, oci, umh_len, u, m_half, clen);
      oci += umh_len;
      rpi += m_half * clen;
      // zero the upper half of coeffs
      zero(rpi, m_half * clen);
      rpi += m_half * clen;
   }

   // zero the upper 'M' outer coeffs
   zero(rpi, M*m*clen);
}

#ifdef CL_TEST_CODE
static void test_dkss_encode() {
   {
      const index m = 2;
      const index M = 4;
      const index u = 4;   // 4 bit
      const index N = 16;  // 16 bit
      uint8 a[2] = { 0xfe, 0xca };
      classert(bit_length(a, NBEL(a)) <= N);
      const index clen = (2*u + int_log2(M) + int_log2(m) + bits(uint8) - 1) / bits(uint8);
      tape_alloc<uint8> tmprp(2*M*m*clen);
      uint8* rp = tmprp.p;
      dkss_encode(rp, a, NBEL(a), M, m, u, clen);
      classert(dump_poly_hex(rp, 2*M*m, clen) == "[0xe, 0, 0xf, 0, 0xa, 0, 0xc, 0, 0, 0, 0, 0, 0, 0, 0, 0]");
   }
   {
      const index m = 4;
      const index M = 16;
      const index u = 7;   // bit
      const index N = 224;  // bit
      uint32 a[7] = { 0xB5726F97, 0x065F4162, 0xAF219034, 0xB8363C52, 0xF822BFDB, 0xC226A29F, 0xfB1361E3 };
      // >>> a=0xB5726F97+(0x065F4162<<32)+(0xAF219034<<32*2)+(0xB8363C52<<32*3)+(0xF822BFDB<<32*4)+(0xC226A29F<<32*5)+(0x7B1361E3<<32*6)
      // >>> m=2**7
      // >>> for i in range(0,32):
      // ...  print(hex(a%m))
      // ...  a //= m
      // ...
      classert(bit_length(a, NBEL(a)) <= N);
      const index clen = (2*u + int_log2(M) + int_log2(m) + bits(uint32) - 1) / bits(uint32);
      tape_alloc<uint32> tmprp(2*M*m*clen);
      uint32* rp = tmprp.p;
      dkss_encode(rp, a, NBEL(a), M, m, u, clen);
      classert(dump_poly_hex(rp, 2*M*m, clen) == "[0x17, 0x5f, 0, 0, 0x49, 0x2b, 0, 0, 0x2b, 0x2c, 0, 0, 0x50, 0x2f, 0, 0, 6, 0x68, 0, 0, 0x40, 0xc, 0, 0, 0x72, 0x55, 0, 0, 0x14, 0x1e, 0, 0, 0x36, 0x70, 0, 0, 0x6e, 0x7e, 0, 0, 0x2b, 4, 0, 0, 0x7e, 0x4f, 0, 0, 0x22, 0x4d, 0, 0, 8, 0x1e, 0, 0, 0x1e, 0x6c, 0, 0, 0x44, 0x7d, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]");
   }
   {
      // these parameters work only for encoding, but not for the FFT later on, since m > M
      const index m = 8;
      const index M = 4;
      const index u = 23;   // bit
      const index N = 368;  // bit
      // def bit_snip(a, u):
      //    for i in range(0, (a.bit_length()+u-1)//u):
      //       sh = i*u
      //       by = (a >> sh) % (2**u)
      //       print(hex(by), end=", ")
      //    print()
      //
      // a = 0xB8626C055F33347AA2259101F89DD4B8B99DB5B6E9DB46650290E998AA538BB92167A8F5B665E06847A5B469888C
      // bit_snip(a, 8)
      // bit_snip(a, 23)
      uint8 a[46] = { 0x8c, 0x88, 0x69, 0xb4, 0xa5, 0x47, 0x68, 0xe0, 0x65, 0xb6, 0xf5, 0xa8, 0x67, 0x21, 0xb9, 0x8b, 0x53, 0xaa, 0x98, 0xe9, 0x90, 0x2, 0x65, 0x46, 0xdb, 0xe9, 0xb6, 0xb5, 0x9d, 0xb9, 0xb8, 0xd4, 0x9d, 0xf8, 0x1, 0x91, 0x25, 0xa2, 0x7a, 0x34, 0x33, 0x5f, 0x5, 0x6c, 0x62, 0xb8 };
      classert(bit_length(a, NBEL(a)) <= N);
      const index clen = (2*u + int_log2(M) + int_log2(m) + bits(uint8) - 1) / bits(uint8);
      tape_alloc<uint8> tmprp(2*M*m*clen);
      uint8* rp = tmprp.p;
      dkss_encode(rp, a, NBEL(a), M, m, u, clen);
      classert(dump_poly_hex(rp, 2*M*m, clen) == "[0x69888c, 0xf4b68, 0x1781a1, 0x47adb3, 0, 0, 0, 0, 0x12167a, 0x4a7177, 0x3a662a, 0x328148, 0, 0, 0, 0, 0x69db46, 0x3b6b6d, 0x52e2e6, 0xfc4ee, 0, 0, 0, 0, 0x225910, 0x668f54, 0x157cc, 0x5c3136, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]");
   }
}
#endif


// ------ DKSS inner FFT ------------------------------------------------------

// perform a 2m-point FFT of 'e'.
// 2m-th root of unity is alpha, since we work in P[alpha]/(alpha^m+1)

// input:
// 'e' is a polynomial with 2*'m' (outer) coeffs, each 'oclen' small's long; 'e' is already shuffled!
// 't' has oclen small's temp storage

template<typename small>
static void dkss_inner_fft_eval(
   small* e,               // input vector
   const index n_half,     // half of FFT size
   const index m,
   const index oclen,      // outer coeff length = m * iclen
   const index iclen,      // inner cooeff length >= bit_length(pz) / bits(word)
   const small* pz,        // p^z
   small* t)               // temp storage
{
   caassert(n_half >= 1);
   caassert(m >= 1);
   caassert(m * iclen == oclen);
   classert(is_pow2(n_half));
   caassert(m >= n_half);
   caassert(m % n_half == 0);  // n|(2m), otherwise, we don't have a primitive root

   if (n_half == 1) {
      // lowest layer: butterfly of two outer coeffs,
      // i.e. add and sub of two inner polynomials
      small* e2 = e + oclen;              // second inner polynomial
      copy(t, e2, oclen);
      modpoly_sub(e2, e, t, m, iclen, pz);  // e2 = e - t
      modpoly_add(e, e, t, m, iclen, pz);  // e = e + t
      return;
   }

   dkss_inner_fft_eval(e, n_half/2, m, oclen, iclen, pz, t);
   dkss_inner_fft_eval(e + n_half*oclen, n_half/2, m, oclen, iclen, pz, t);

   const index inc = m / n_half;          // increment for each loop
   small* e1 = e;                         // first inner polynomial
   small* e2 = e + n_half*oclen;          // second inner polynomial
   index pow = 0;
   for (index i=0; i<n_half; ++i) {
      // w = omega_n^i, t = w*e2
      modpoly_mul_xpow_mod_mp1(t, e2, pow, m, iclen, pz);  // cyclic shift by pow
      modpoly_sub(e2, e1, t, m, iclen, pz);  // e2 = e1 - t
      modpoly_add(e1, e1, t, m, iclen, pz);  // e1 = e1 + t
      e1 += oclen;
      e2 += oclen;
      pow += inc;
   }
}

#ifdef CL_TEST_CODE
static void test_dkss_inner_fft_eval() {
   // m == 1, iclen == 1, n == 2.  2-nd root is -1
   {
      uint8 e[] = { 1, 2 };
      uint8 mod[] = { 251 };
      uint8 t[1];
      dkss_inner_fft_eval(e, NBEL(e)/NBEL(t)/2, NBEL(t)/NBEL(mod), NBEL(t), NBEL(mod), mod, t);
      uint8 rc[] = { 1+2, 1-2+251 };
      classert(memcmp(e, rc, sizeof(e)) == 0);
   }
   {
      uint8 e[] = { 2, 2 };
      uint8 mod[] = { 251 };
      uint8 t[1];
      dkss_inner_fft_eval(e, NBEL(e)/NBEL(t)/2, NBEL(t)/NBEL(mod), NBEL(t), NBEL(mod), mod, t);
      uint8 rc[] = { 2+2, 2-2 };
      classert(memcmp(e, rc, sizeof(e)) == 0);
   }
   {
      uint8 e[] = { 200, 51 };
      uint8 mod[] = { 251 };
      uint8 t[1];
      dkss_inner_fft_eval(e, NBEL(e)/NBEL(t)/2, NBEL(t)/NBEL(mod), NBEL(t), NBEL(mod), mod, t);
      uint8 rc[] = { 200+51-251, 200-51 };
      classert(memcmp(e, rc, sizeof(e)) == 0);
   }
   // m == 2, iclen == 1, n == 2.  2-nd root is -1
   {
      uint8 e[] = { 1, 4, 9, 16 };
      uint8 mod[] = { 251 };
      uint8 t[2];
      dkss_inner_fft_eval(e, NBEL(e)/NBEL(t)/2, NBEL(t)/NBEL(mod), NBEL(t), NBEL(mod), mod, t);
      uint8 rc[] = { 1+9, 4+16, 1-9+251, 4-16+251 };
      classert(memcmp(e, rc, sizeof(e)) == 0);
   }
   // m == 3, iclen == 1, n == 2.  2-nd root is -1
   {
      uint8 e[] = { 1, 4, 9, 16, 25, 36 };
      uint8 mod[] = { 251 };
      uint8 t[3];
      dkss_inner_fft_eval(e, NBEL(e)/NBEL(t)/2, NBEL(t)/NBEL(mod), NBEL(t), NBEL(mod), mod, t);
      uint8 rc[] = { 1+16, 4+25, 9+36, 1-16+251, 4-25+251, 9-36+251 };
      classert(memcmp(e, rc, sizeof(e)) == 0);
   }
   // m == 2, iclen == 1, n == 4.  4-th root is alpha
   {
      uint8 e[] = { 1, 4,  25, 36,  9, 16,  49, 64 };  // already pre-shuffled.  original is { 1, 4,  9, 16,  25, 36,  49, 64 }
      uint8 mod[] = { 251 };
      uint8 t[2];
      dkss_inner_fft_eval(e, NBEL(e)/NBEL(t)/2, NBEL(t)/NBEL(mod), NBEL(t), NBEL(mod), mod, t);
      uint8 rc[] = {
         84, 120,  // 120*x + 84,
         1-41+64, 13-85+251,  // 64*x^4 + 85*x^3 + 41*x^2 + 13*x + 1
         1-9+25-49+251, 4-16+36-64+251,  // 64*x^7 + 49*x^6 + 36*x^5 + 25*x^4 + 16*x^3 + 9*x^2 + 4*x + 1
         1+16-25-64+251, 4-9-36+49,  // 64*x^10 + 49*x^9 + 36*x^7 + 25*x^6 + 16*x^4 + 9*x^3 + 4*x + 1
      };
      classert(memcmp(e, rc, sizeof(e)) == 0);
   }
   // m == 4, iclen == 2, n == 4.  4-th root is alpha^2
   {
      uint8 e[] = {
         2, 2, 3, 3, 5, 5, 7, 7,             // 514, 771, 1285, 1799
         23, 23, 29, 29, 31, 31, 37, 37,     // 5911, 7453, 7967, 9509
         11, 11, 13, 13, 17, 17, 19, 19,     // 2827, 3341, 4369, 4883
         41, 41, 43, 43, 47, 47, 53, 53      // 10537, 11051, 12079, 13621
      };  // already pre-shuffled.  original is { 2, 2, 3, 3, 5, 5, 7, 7,  11, 11, 13, 13, 17, 17, 19, 19,  23, 23, 29, 29, 31, 31, 37, 37,  41, 41, 43, 43, 47, 47, 53, 53 }
      //for (unsigned i=0; i<sizeof(e); i+=sizeof(uint16)) DTOUT_PUTS(format("%?, ") << *(uint16*)(e+i));
      uint8 mod[] = { 0xf1, 0xff };  // 65521
      uint8 t[8];
      dkss_inner_fft_eval(e, NBEL(e)/NBEL(t)/2, NBEL(t)/NBEL(mod), NBEL(t), NBEL(mod), mod, t);
      // see test.py @r24461 for independant py code
      uint8 rc[] = {
         77, 77, 88, 88, 100, 100, 116, 116,          // 29812*x^3 + 25700*x^2 + 22616*x + 19789
         9, 9, 8, 8, 185, 199, 181, 195,              // 24158*x^3 + 62437*x^2 + 47531*x + 42134
         214, 228, 217, 231, 213, 227, 213, 227,      // 50101*x^3 + 51129*x^2 + 2056*x + 2313
         190, 204, 181, 195, 4, 4, 0, 0,              // 9252*x^3 + 1542*x^2 + 16448*x + 57040
      };
      classert(memcmp(e, rc, sizeof(e)) == 0);
   }
}
#endif


// ------ DKSS FFT (3 steps) --------------------------------------------------

// #define PI_CHECK 1

// input:
// 'a' is a polynomial with 2*'M' (outer) coeffs, each 'oclen' small's long

template<typename small>
static void dkss_fft(
   small* a,               // input vector
   const index M,
   const index m,
   const index oclen,      // outer coeff length = m * iclen
   const index iclen,      // inner cooeff length >= bit_length(pz) / bits(word)
   const small* pz,        // p^z
   const small* rho_pow,   // powers [0 : m-1] of \rho
   const index base_pow)
{
   caassert(a != nullptr);
   classert(is_pow2(M));
   classert(is_pow2(m));
   caassert(iclen > 0);
   caassert(oclen > 0);
   caassert(oclen == iclen * m);
   caassert(rho_pow != nullptr);
   caassert(base_pow > 0);
   caassert(is_pow2(base_pow));

   tape_alloc<small> tmp(oclen);
   small* t = tmp.p;  // length oclen, temp storage
   if (M <= m) {  // use inner DFT right away
      const unsigned log_M = int_log2(M);
      fft_shuffle(a, log_M + 1, oclen);   // pre-shuffle the values
      PROF(dkss_prof.start(profid_dkss_inner_fft));
      dkss_inner_fft_eval(a, M, m, oclen, iclen, pz, t);
      PROF(dkss_prof.stop(profid_dkss_inner_fft));
      return;
   }

   caassert(M > 1);
   caassert(m > 1);
   const index mu = M / m;
   caassert(mu * m == M);
   caassert(mu > 1);
   const unsigned log2m = int_log2(m) + 1;
   classert((index)1 << log2m == 2*m);

   // perform inner DFT's
   // transpose matrix, so the e_l(y) are stored back-to-back
   transpose_in_place_pow2(a, mu, 2*m, oclen);
   for (index l=0; l<mu; ++l) {  // cycle through all 2M/2m values for l
      // perform inner DFT on e_l(y) with alpha as 2m-th root of unity
      small* elx = a + l*2*m*oclen;
      PROF(dkss_prof.start(profid_dkss_inner_fft));
      fft_shuffle(elx, log2m, oclen);  // shuffle for FFT eval
      dkss_inner_fft_eval(elx, m, m, oclen, iclen, pz, t);
      PROF(dkss_prof.stop(profid_dkss_inner_fft));
   }
   // transpose back
   transpose_in_place_pow2(a, 2*m, mu, oclen);

   // perform bad muls and outer DFTs
   small* abar_v = a;
   small* rho_vl = t;  // just for the name
   const index top_mu = mu * base_pow;   // top level \mu
   const unsigned psh = int_log2(top_mu);
#ifdef RHO_POW_CHECK
   const small* rho = rho_pow + oclen;  // level 0 rho^1
#endif
   // cycle through all a_v to perform bad muls and outer DFTs
   for (index v=0; v<2*m; ++v) {
      PROF(dkss_prof.start(profid_dkss_bad_muls));
      caassert(mu >= 1);
      // skip first loop iteration: v == 0, i.e. abar_{v,l} *= rho^0 = 1
      small* abar_vl = abar_v;
      index vlbase = 0;
      for (index l=1; l<mu; ++l) {  // cycle thru all \mu values for l
         vlbase += v * base_pow;
         abar_vl += oclen;
         const index pi = vlbase & (((index)1 << psh) - 1);  // vlbase % top_mu
         const index pe = vlbase >> psh;                     // vlbase / top_mu
         classert(pe * top_mu + pi == v * l * base_pow);
         if (pi == 0) {
            // if pi == 0, we access rho^0 == 1 and shift it cyclicly.
            // we can do the cyclic shift on abar_vl directly.
#ifdef PI_CHECK
            tape_alloc<small> tmp(2*oclen);
            small* t2 = tmp.p;
            small* oabar_vl = t2 + oclen;
            modpoly_mul_xpow_mod_mp1(rho_vl, rho_pow + pi*oclen, pe, m, iclen, pz);
            modpoly_mul_mod_mp1(t2, abar_vl, rho_vl, m, iclen, pz);
            copy(oabar_vl, abar_vl, oclen);
#endif
            modpoly_mul_xpow_mod_mp1(t, abar_vl, pe, m, iclen, pz);
            copy(abar_vl, t, oclen);
#ifdef PI_CHECK
            if (memcmp(t2, abar_vl, sizeof(small) * oclen) != 0)
               NOP();
            classert(memcmp(t2, abar_vl, sizeof(small) * oclen) == 0);
#endif
         }
         else {
            // select right rho_pow and do cyclic shift
            modpoly_mul_xpow_mod_mp1(rho_vl, rho_pow + pi*oclen, pe, m, iclen, pz);
#ifdef RHO_POW_CHECK
            {
               tape_alloc<small> tmp(oclen);
               small* t = tmp.p;
               modpoly_pow_mod_mp1(t, rho, vlbase, m, iclen, pz);
               classert(memcmp(t, rho_vl, sizeof(small) * oclen) == 0);
            }
#endif
            // abar_{v,l} *= rho^{vl}
            modpoly_mul_mod_mp1(abar_vl, abar_vl, rho_vl, m, iclen, pz);
         }
      }
      PROF(dkss_prof.stop(profid_dkss_bad_muls));

      // now abar_v contains \tilde{a}_v.  ready to do outer DFT: recursive call
      dkss_fft(abar_v, mu/2, m, oclen, iclen, pz, rho_pow, base_pow * 2*m);

      abar_v += mu * oclen;
   }
   
   // final transpose
   transpose_in_place_pow2(a, mu, 2*m, oclen);
}

#ifdef CL_TEST_CODE
static void test_dkss_fft() {
   uint8 dummy_rho[] = { 0 };
   // these tests are used from test_dkss_inner_fft_eval() ...
   // m == 1, iclen == 1, n == 2.  2-nd root is -1
   {
      const index m = 1;
      uint8 e[] = { 1, 2 };
      uint8 mod[] = { 251 };
      dkss_fft(e, NBEL(e)/m/2, m, m*NBEL(mod), NBEL(mod), mod, dummy_rho, 1);
      uint8 rc[] = { 1+2, 1-2+251 };
      classert(memcmp(e, rc, sizeof(e)) == 0);
   }
   // m == 2, iclen == 1, n == 2.  2-nd root is -1
   {
      const index m = 2;
      uint8 e[] = {
         1, 4,
         9, 16
      };
      uint8 mod[] = { 251 };
      dkss_fft(e, NBEL(e)/m/2, m, m*NBEL(mod), NBEL(mod), mod, dummy_rho, 1);
      uint8 rc[] = {
         1+9, 4+16,
         1-9+251, 4-16+251
      };
      classert(memcmp(e, rc, sizeof(e)) == 0);
   }
   // m == 4, iclen == 1, n == 2.  2-nd root is alpha^4
   {
      const index m = 4;
      uint8 e[] = {
         2, 3, 5, 7,
         11, 13, 17, 19
      };
      uint8 mod[] = { 251 };
      dkss_fft(e, NBEL(e)/m/2, m, m*NBEL(mod), NBEL(mod), mod, dummy_rho, 1);
      // see test.py @r24497 for independant py code
      uint8 rc[] = {
         13, 16, 22, 26,
         242, 241, 239, 239
      };
      classert(memcmp(e, rc, sizeof(e)) == 0);
   }
   // m == 8, iclen == 1, n == 4.  4-th root is alpha^4
   {
      const index m = 8;
      uint8 e[] = {
         2, 3, 5, 7, 11, 13, 17, 19,
         23, 29, 31, 37, 41, 43, 47, 53,
         59, 61, 67, 71, 73, 79, 83, 89,
         97, 101, 103, 107, 109, 113, 127, 131
      };
      uint8 mod[] = { 251 };
      dkss_fft(e, NBEL(e)/m/2, m, m*NBEL(mod), NBEL(mod), mod, dummy_rho, 1);
      // see test.py @r24499 for independant py code
      uint8 rc[] = {
         181, 194, 206, 222, 234, 248, 23, 41,
         11, 12, 18, 14, 115, 113, 113, 111,
         192, 185, 189, 185, 185, 187, 177, 175,
         126, 123, 109, 109, 12, 6, 6, 0
      };
      classert(memcmp(e, rc, sizeof(e)) == 0);
   }

   // test full dkss_fft()
   // m == 2, iclen == 1, n == 8, M == 4.  2-nd root is -1
   {
      dkss_parm<uint8> p;
      dkss_set_mmu_dbg<uint8>(p, 8, 4, 2, 2, "193", 1, 5);
      dkss_set_parm2<uint8>(p);
      //DTOUT_PUTS(format("omega=%?\n") << p.omega);
      classert(p.omega == 43);
      //DTOUT_PUTS(format("rho=%?\n") << dump_poly<uint8>(p.rho, p.m, p.iclen));
      string s = dump_poly<uint8>(p.rho, p.m, p.iclen);
      classert(s == "[17, 176]");
      const index m = 2;
      uint8 e[] = {
         2, 3,    // a_0
         5, 7,    // a_1
         11, 13,  // a_2
         17, 19,  // a_3
         23, 29,  // a_4
         31, 37,  // a_5
         41, 43,  // a_6
         47, 53,  // a_7
      };
      uint8 mod[] = { 193 };
      dkss_fft(e, NBEL(e)/m/2, m, m*NBEL(mod), NBEL(mod), mod, p.rho_pow, 1);
      // test.py @r24514 contains code to calculate correct result
      uint8 rc[] = {
         177, 11,
         90, 139,
         1, 141,
         87, 142,
         170, 165,
         121, 135,
         138, 4,
         4, 59,
      };
      classert(memcmp(e, rc, sizeof(e)) == 0);
   }
   // m == 2, iclen == 1, n == 32, M == 16.  dkss_fft() must recurse twice
   {
      dkss_parm<uint8> p;
      dkss_set_mmu_dbg<uint8>(p, 8, 16, 2, 1, "193", 1, 5);
      dkss_set_parm2<uint8>(p);
      //DTOUT_PUTS(format("omega=%?\n") << p.omega);
      classert(p.omega == 185);
      //DTOUT_PUTS(format("rho=%?\n") << dump_poly<uint8>(p.rho, p.m, p.iclen));
      string s = dump_poly<uint8>(p.rho, p.m, p.iclen);
      classert(s == "[126, 147]");
      const index m = 2;
      uint8 e[] = {
         2, 3,      // a_0
         5, 7,      // a_1
         11, 13,    // a_2
         17, 19,    // a_3
         23, 29,    // a_4
         31, 37,    // a_5
         41, 43,    // a_6
         47, 53,    // a_7
         59, 61,    // a_8
         67, 71,    // a_9
         73, 79,    // a_10
         83, 89,    // a_11
         97, 101,   // a_12
         103, 107,  // a_13
         109, 113,  // a_14
         127, 131,  // a_15
         137, 139,  // a_16
         149, 151,  // a_17
         157, 163,  // a_18
         167, 173,  // a_19
         179, 181,  // a_20
         191, 0,    // a_21
         4, 9,      // a_22
         16, 25,    // a_23
         36, 49,    // a_24
         64, 81,    // a_25
         100, 121,  // a_26
         144, 169,  // a_27
         1, 2,      // a_28
         3, 4,      // a_29
         5, 6,      // a_30
         7, 8,      // a_31
      };
      uint8 mod[] = { 193 };
      dkss_fft(e, NBEL(e)/m/2, m, m*NBEL(mod), NBEL(mod), mod, p.rho_pow, 1);
      // test.py @r24524 contains code to calculate correct result
      uint8 rc[] = {
         132, 114,
         92, 185,
         169, 120,
         121, 76,
         78, 11,
         71, 132,
         35, 50,
         139, 43,
         50, 23,
         20, 134,
         188, 172,
         181, 172,
         7, 173,
         185, 146,
         37, 174,
         63, 11,
         6, 180,
         58, 35,
         2, 93,
         176, 121,
         152, 38,
         74, 23,
         149, 46,
         90, 141,
         18, 13,
         96, 181,
         161, 159,
         52, 64,
         78, 113,
         158, 190,
         190, 21,
         124, 30,
      };
      classert(memcmp(e, rc, sizeof(e)) == 0);
   }
   {
      // m = 2, M = 8, u = 8, N <= 64
      dkss_parm<uint8> p;
      dkss_set_mmu_dbg<uint8>(p, 64, 8, 2, 8, "5767169", 1, 3);
      dkss_set_parm2<uint8>(p);
      //DTOUT_PUTS(format("omega=%?\n") << p.omega);
      //classert(p.omega == 185);
      classert(p.oclen == 6);
      //DTOUT_PUTS(format("rho=%?\n") << dump_poly<uint8>(p.rho, p.m, p.iclen));
      classert(dump_poly<uint8>(p.rho, p.m, p.iclen) == "[2829314, 1423300]");
      uint8 e[] = {
         196, 0, 0, 0, 0, 0,
         221, 0, 0, 0, 0, 0,
         10, 0, 0, 0, 0, 0,
         121, 0, 0, 0, 0, 0,
         232, 0, 0, 0, 0, 0,
         208, 0, 0, 0, 0, 0,
         50, 0, 0, 0, 0, 0,
         52, 0, 0, 0, 0, 0,
         0, 0, 0, 0, 0, 0,
         0, 0, 0, 0, 0, 0,
         0, 0, 0, 0, 0, 0,
         0, 0, 0, 0, 0, 0,
         0, 0, 0, 0, 0, 0,
         0, 0, 0, 0, 0, 0,
         0, 0, 0, 0, 0, 0,
         0, 0, 0, 0, 0, 0
      };
      dkss_fft(e, p.M, p.m, p.oclen, p.iclen, p.pc.ptr, p.rho_pow, 1);
      // test.py @r24554 contains code to calculate correct result
      uint8 rc[] = {
         66, 4, 0, 0, 0, 0,
         111, 104, 33, 28, 112, 49,
         61, 37, 67, 132, 131, 11,
         255, 214, 78, 30, 249, 23,
         112, 1, 0, 0, 1, 0,
         251, 94, 64, 15, 55, 11,
         61, 37, 67, 125, 124, 76,
         193, 233, 31, 247, 212, 83,
         143, 255, 87, 0, 0, 0,
         248, 32, 54, 244, 150, 67,
         124, 218, 20, 45, 124, 76,
         104, 178, 8, 212, 255, 34,
         112, 1, 0, 1, 255, 87,
         176, 26, 24, 131, 197, 47,
         124, 218, 20, 212, 131, 11,
         234, 143, 56, 121, 46, 33,
      };
      classert(memcmp(e, rc, sizeof(e)) == 0);
   }
}
#endif


// ------ DKSS decode polynomial to number ------------------------------------

template<typename small>
static void dkss_sumup2(small* r, const index rlen, bit_index sh, const small* p, const index m, const index u, const index iclen, const small* len_inv,
      const small* mod, small* t)
{
   //tout_puts(format("r=%? sh=%? m=%? u=%? iclen=%?\np=%?\n") << dump_hex(r, rlen) << sh << m << u << iclen << dump_poly_hex(p, m, iclen));
   const small* pi = p;
   for (index i=0; i<m; ++i) {
      //tout_puts(format("pi=%?\n") << dump_hex(pi, iclen));
      const index pi_len = normu(pi, iclen);  // inner coeff len
      if (pi_len > 0) {
         // normalize
         mulu(t, pi, pi_len, len_inv, iclen);  // t = p[i] * inv_len
         modu(t, iclen, t, pi_len+iclen, mod, iclen);  // t %= mod
         CLDEBUG(const small of =) addu_on_shl(r, rlen, t, normu(t, iclen), sh);
         classert(of == 0);
      }
      sh += u;
      pi += iclen;
   }
}

#ifdef CL_TEST_CODE
static void test_dkss_sumup2() {
   {
      uint8 r[] = { 0, 0 };
      uint8 p[4] = { 2, 3, 5, 7 };
      uint8 len_inv[] = { 1 };  // unity
      uint8 mod[] = { 193 };
      uint8 t[2];  // len 2*iclen
      dkss_sumup2(r, NBEL(r), 0, p, NBEL(p), 4, 1, len_inv, mod, t);
      uint8 rc[NBEL(r)] = { 0x32, 0x75 };  // 0x7532
      classert(memcmp(r, rc, sizeof(r)) == 0);
   }
   {
      uint8 r[] = { 0, 0, 0 };
      uint8 p[4] = { 2, 3, 5, 7 };
      uint8 len_inv[] = { 145 };  // 1/4 mod 193
      uint8 mod[] = { 193 };
      uint8 t[2];  // len 2*iclen
      dkss_sumup2(r, NBEL(r), 0, p, NBEL(p), 4, 1, len_inv, mod, t);
      uint8 rc[NBEL(r)] = { 0x71, 0xb5, 3 };  // [97, 49, 146, 50] = 0x3b571
      classert(memcmp(r, rc, sizeof(r)) == 0);
   }
}
#endif


template<typename small>
static void dkss_sumup(small* r, const index rlen, const small* p, const index M, const index m, const index u, const index oclen, const index iclen,
      const small* len_inv, const small* mod) {
   caassert(M > 0);
   caassert(m > 1);
   caassert(iclen > 0);
   caassert(oclen > 0);

   tape_alloc<small> tmp(2*iclen);
   small* t = tmp.p;  // len 2*iclen

   zero(r, rlen);
   dkss_sumup2(r, rlen, 0, p, m, u, iclen, len_inv, mod, t);  // coeff 0

   // find uppermost coeff != 0
   const small* pi = p + oclen;  // skip coeff 0
   const small* p_end = p + 2*M * oclen;
   while (pi < p_end) {
      if (*pi != 0) break;
      ++pi;
   }
   p_end = p + (pi - p) / oclen * oclen;  // point to beginning of highest outer coeff != 0

   // rest of coeffs
   const index obits = u*m/2;
   bit_index bit_pos = obits;
   pi = p + (2*M - 1) * oclen;  // point to uppermost coeff
   while (pi >= p_end) {
      dkss_sumup2(r, rlen, bit_pos, pi, m, u, iclen, len_inv, mod, t);
      bit_pos += obits;
      pi -= oclen;
   }
}

#ifdef CL_TEST_CODE
template<typename small>
static void fft_invert(small* p, size_t n, size_t len) {
   classert(is_pow2(n));
   for (size_t i=1; i<n/2; ++i) {
      swap_in(p + i*len, p + (n-i)*len, len);
   }
}

static void test_fft_invert() {
   {
      uint8 a[] = { 1, 2, 3, 4 };
      fft_invert(a, NBEL(a), 1);
      uint8 rc[] = { 1, 4, 3, 2 };
      classert(memcmp(a, rc, sizeof(rc)) == 0);
   }
   {
      uint32 a[] = { 1, 2, 3, 4, 5, 6, 7, 8 };
      fft_invert(a, NBEL(a)/2, 2);
      uint32 rc[] = { 1, 2, 7, 8, 5, 6, 3, 4 };
      classert(memcmp(a, rc, sizeof(rc)) == 0);
   }
}

static void test_dkss_sumup() {
   test_fft_invert();
   {
      const index M = 4;
      const index m = 2;
      const index N = 16;
      const index u = 4;
      const index iclen = (2*u + int_log2(M) + int_log2(m) + bits(uint8) - 1) / bits(uint8);
      caassert(iclen == 2);
      const uint8 a[] = { 0x23, 0x57 };
      classert(bit_length(a, NBEL(a)) <= N);
      tape_alloc<uint8> tmp(2*M*m*iclen);
      uint8* p = tmp.p;
      dkss_encode(p, a, NBEL(a), M, m, u, iclen);

      fft_invert(p, 2*M, m*iclen);

      uint8 r[NBEL(a)];
      uint8 len_inv[] = { 1, 0 };  // unity
      uint8 mod[] = { 193, 0 };
      dkss_sumup(r, NBEL(r), p, M, m, u, m*iclen, iclen, len_inv, mod);
      classert(memcmp(r, a, sizeof(a)) == 0);
   }
   {
      const index M = 8;
      const index m = 4;
      const index N = 128;
      const index u = 8;
      const index iclen = (2*u + int_log2(M) + int_log2(m) + bits(uint16) - 1) / bits(uint16);
      caassert(iclen == 2);
      const uint16 a[] = { 0xfbf1, 0xefe9, 0xe5e3, 0xdfd3, 0xc7c5, 0xc1bf, 0xb5b3, 0xada7 };
      classert(bit_length(a, NBEL(a)) <= N);
      tape_alloc<uint16> tmp(2*M*m*iclen);
      uint16* p = tmp.p;
      dkss_encode(p, a, NBEL(a), M, m, u, iclen);

      fft_invert(p, 2*M, m*iclen);

      uint16 r[NBEL(a)];
      uint16 len_inv[] = { 1, 0 };  // unity
      uint16 mod[] = { 65521, 0 };
      dkss_sumup(r, NBEL(r), p, M, m, u, m*iclen, iclen, len_inv, mod);
      classert(memcmp(r, a, sizeof(a)) == 0);
   }
}
#endif


// ------ DKSS MULTIPLICATION - main routine ----------------------------------

#if 0
#define FFT_OUT(a) tout_puts(format a)
#else
#define FFT_OUT(a)
#endif

template<typename small, bool square>
void dkss_mul3(dkss_parm<small>& p, small* r, const small* a, const index alen, const small* b, const index blen) {
   PROF(dkss_prof.start(profid_dkss_mul3));
   caassert(r != nullptr);
   caassert(a != nullptr);
   caassert(b != nullptr);
   caassert(!square || a == b);
   caassert(!square || alen == blen);
   caassert((small)-1 > 0);  // must be unsigned type

   FFT_OUT(("pc=%?\n") << p.pc);
   FFT_OUT(("rho=%?\n") << dump_poly(p.rho, p.m, p.iclen));

   // allocate storage for degree 2M-1 polynomials over R and encode them
   tape_alloc<small> tmpa((square ? 1 : 2) * p.polylen);
   small* pa = tmpa.p;
   small* pb = square ? pa : tmpa.p + p.polylen;
   dkss_encode(pa, a, alen, p.M, p.m, p.u, p.iclen);
   FFT_OUT(("pa=%?\n") << dump_polypoly_hex(pa, 2*p.M, p.m, p.iclen));
   if (!square) {
      dkss_encode(pb, b, blen, p.M, p.m, p.u, p.iclen);
      FFT_OUT(("pb=%?\n") << dump_polypoly_hex(pb, 2*p.M, p.m, p.iclen));
   }

   // do the FFTs
   PROF(dkss_prof.start(profid_dkss_fft));
   dkss_fft(pa, p.M, p.m, p.oclen, p.iclen, p.pc.ptr, p.rho_pow, 1);
   FFT_OUT(("ffted pa=%?\n") << dump_polypoly_hex(pa, 2*p.M, p.m, p.iclen));
   if (!square) {
      dkss_fft(pb, p.M, p.m, p.oclen, p.iclen, p.pc.ptr, p.rho_pow, 1);
      FFT_OUT(("ffted pb=%?\n") << dump_polypoly_hex(pb, 2*p.M, p.m, p.iclen));
   }
   PROF(dkss_prof.stop(profid_dkss_fft));

   // pointwise multiplications
   PROF(dkss_prof.start(profid_dkss_point_mul));
   small* ai = pa;
   small* bi = square ? pa : pb;
   for (index i=0; i<2*p.M; ++i) {
      if (square) modpoly_squ_mod_mp1(ai, ai, p.m, p.iclen, p.pc.ptr);  // pa = pa^2
      else modpoly_mul_mod_mp1(ai, ai, bi, p.m, p.iclen, p.pc.ptr);  // pa *= pb
      ai += p.oclen;
      bi += p.oclen;
   }
   FFT_OUT(("prod=%?\n") << dump_polypoly_hex(pa, 2*p.M, p.m, p.iclen));
   PROF(dkss_prof.stop(profid_dkss_point_mul));

   // backwards FFT
   PROF(dkss_prof.start(profid_dkss_fft));
   dkss_fft(pa, p.M, p.m, p.oclen, p.iclen, p.pc.ptr, p.rho_pow, 1);
   FFT_OUT(("ffted prod=%?\n") << dump_polypoly_hex(pa, 2*p.M, p.m, p.iclen));
   PROF(dkss_prof.stop(profid_dkss_fft));

   // normalize and sum up
   dkss_sumup(r, alen+blen, pa, p.M, p.m, p.u, p.oclen, p.iclen, p.len_inv.ptr, p.pc.ptr);   
   PROF(dkss_prof.stop(profid_dkss_mul3));
}

template<typename small, bool square>
void dkss_mul2(dkss_parm<small>& p, small* r, const small* a, const index alen, const small* b, const index blen) {
   dkss_set_parm2<small>(p);
   dkss_mul3<small, square>(p, r, a, alen, b, blen);
}

template<typename small, bool square>
void q_dkss_mul1(small* r, const small* a, const index alen, const small* b, const index blen) {
   // set parameters
   dkss_parm<small> p;
   dkss_set_mmu<small>(p, alen, blen);
   dkss_mul2<small, square>(p, r, a, alen, b, blen);
}

template<typename small, bool square>
void q_dkss_mul(small* r, const small* a, const index alen, const small* b, const index blen) {
   caassert(!square || a == b);
   caassert(!square || alen == blen);

   /*if (!q_use_dkss_mul<square>(alen, blen)) {
      smul_ls<small, square>(r, a, alen, b, blen);
      return;
   }*/

   // call the main routine
   q_dkss_mul1<small, square>(r, a, alen, b, blen);
}


#ifdef CL_TEST_CODE
static void test_dkss_mul() {
   {
      uint8 a[] = { 2, 3, 5, 7 };      // 0x7050302
      uint8 b[] = { 11, 13, 17, 19 };  // 0x13110d0b
      uint8 r[NBEL(a) + NBEL(b)];
      q_dkss_mul1<uint8, false>(r, a, NBEL(a), b, NBEL(b));
      uint8 rc[NBEL(a) + NBEL(b)] = { 0x16, 0x3b, 0x80, 0xe7, 0xe9, 0xd6, 0x85, 0 };  // 0x85d6e9e7803b16
      classert(memcmp(r, rc, sizeof(r)) == 0);
   }
   {
      // m = 2, M = 4, u = 2, N <= 8
      uint8 a[1] = { 254 };
      uint8 b[1] = { 253 };
      uint8 r[NBEL(a) + NBEL(b)];
      dkss_parm<uint8> parm;
      dkss_set_mmu_dbg<uint8>(parm, 8, 4, 2, 2, "193", 1, 5);
      dkss_mul2<uint8, false>(parm, r, a, NBEL(a), b, NBEL(b));
      uint8 rc[NBEL(a) + NBEL(b)] = { 6, 0xfb };
      classert(memcmp(r, rc, sizeof(r)) == 0);
   }
   {
      // m = 2, M = 2, u = 8, N <= 16
      uint8 a[2] = { 250, 254 };
      uint8 b[2] = { 249, 253 };
      uint8 r[NBEL(a) + NBEL(b)];
      dkss_parm<uint8> parm;
      dkss_set_mmu_dbg<uint8>(parm, 16, 2, 2, 8, "786433", 1, 10);
      dkss_mul2<uint8, false>(parm, r, a, NBEL(a), b, NBEL(b));
      uint8 rc[NBEL(a) + NBEL(b)] = { 0x2a, 0x13, 0xf5, 0xfc };  // 0xfcf5132a
      classert(memcmp(r, rc, sizeof(r)) == 0);
   }
   {
      // m = 4, M = 16, u = 1, N <= 32
      uint16 a[2] = { 0xfff1, 0xfff2 };  // 0xfff2fff1
      uint16 b[2] = { 0xfff3, 0xfff4 };  // 0xfff4fff3
      uint16 r[NBEL(a) + NBEL(b)];
      dkss_parm<uint16> parm;
      dkss_set_mmu_dbg<uint16>(parm, 32, 16, 4, 1, "193", 1, 5);
      dkss_mul2<uint16, false>(parm, r, a, NBEL(a), b, NBEL(b));
      uint16 rc[NBEL(a) + NBEL(b)] = { 0xc3, 0x14e, 0x73, 0xffe8 };  // 0xffe8'0073'014e'00c3
      classert(memcmp(r, rc, sizeof(r)) == 0);
   }
   {
      // m = 2, M = 8, u = 8, N <= 64
      uint8 a[8] = { 0xc4, 0xdd, 0x0a, 0x79, 0xe8, 0xd0, 0x32, 0x34 };
      uint8 b[8] = { 0x97, 0xd0, 0xf5, 0x53, 0x6b, 0x81, 0x8d, 0xf6 };
      //tout_puts(dump_hex(a, NBEL(a)) + "\n" + dump_hex(b, NBEL(b)) + "\n");
      uint8 r[NBEL(a) + NBEL(b)];
      uint8 rc[NBEL(a) + NBEL(b)];
      omul(rc, a, NBEL(a), b, NBEL(b));
      //tout_puts(dump_hex(rc, NBEL(rc)) + "\n");
      dkss_parm<uint8> parm;
      dkss_set_mmu_dbg<uint8>(parm, 64, 8, 2, 8, "5767169", 1, 3);
      dkss_mul2<uint8, false>(parm, r, a, NBEL(a), b, NBEL(b));
      classert(memcmp(r, rc, sizeof(r)) == 0);
   }
   {
      // m = 2, M = 8, u = 8, N <= 64
      uint8 a[8] = { 0xc4, 0xdd, 0x0a, 0x79, 0xe8, 0xd0, 0x32, 0x34 };
      //tout_puts(dump_hex(a, NBEL(a)) + "\n" + dump_hex(b, NBEL(b)) + "\n");
      uint8 r[2*NBEL(a)];
      uint8 rc[2*NBEL(a)];
      osqu(rc, a, NBEL(a));
      //tout_puts(dump_hex(rc, NBEL(rc)) + "\n");
      dkss_parm<uint8> parm;
      dkss_set_mmu_dbg<uint8>(parm, 64, 8, 2, 8, "5767169", 1, 3);
      dkss_mul2<uint8, true>(parm, r, a, NBEL(a), a, NBEL(a));
      classert(memcmp(r, rc, sizeof(r)) == 0);
   }
   {
      // N == 4096 should get us an M > m
      uint32 a[128] = {
         0xc4dd0a79, 0xe8d03234, 0xcf1fb68c, 0x29ee3f96, 0xa3de4e58, 0x2d8711a2, 0xaec8c86, 0xaf2332e0, 0x58ecbbee, 0x381acf9c, 0x3eacc57f, 0x5f96d88, 0xddfbe84d, 0xf7ef35c9, 0x98c85e5a, 0x6b9096a2,
         0x97d0f553, 0x6b818df6, 0xaca77f3c, 0x60e66f43, 0x1167cda2, 0xdd1d38fe, 0xce4773bc, 0xa6852e41, 0xf3a2983d, 0xdbc32a68, 0xf95ce16e, 0x8fa9f8e, 0x2cc3b8a1, 0x416284ec, 0x2ee25c69, 0x6ad24098,
         0x75dfb58e, 0xae356d88, 0x7352a5e8, 0x6bd8078c, 0xaeb6b316, 0xb00fc79a, 0x1ab9c287, 0x71c1a86e, 0x5ba09655, 0xb664113d, 0x11a91dc9, 0x136501bd, 0x32709305, 0xb16f1100, 0x6dcec55, 0x8bb75f0d,
         0xcb9ca9ae, 0xcaf351e8, 0x10c01fe8, 0xe8e6407a, 0x8377dfb4, 0xada63325, 0x9133d585, 0xef14ac32, 0x2a774527, 0xf1ebb4c9, 0xa69d1ebf, 0xf9da38d8, 0x7090d1bb, 0x364e9d42, 0xb356050e, 0x19e1c7d,
         0xed8e11cd, 0x277d57bc, 0xd457b6b6, 0x9cbf04cf, 0xa31bfe5d, 0x58f78d9d, 0x3c63b3eb, 0xa8714c62, 0x259d0c8a, 0x9b90d30d, 0x40b4e171, 0xfde15b18, 0xa133b5ed, 0x4d3ceca4, 0x847ba61, 0x846d578a,
         0x670b181a, 0x6686df64, 0x399fc81, 0x5d11cf7e, 0x5603ae62, 0x57fc972, 0x8bacb291, 0x46b30acd, 0x6f862a7a, 0xb28e9945, 0xdbf2dc2a, 0x61b7af5e, 0xfce18f7a, 0x6cd294f4, 0x72e9f786, 0x69e81c66,
         0x5d1a53c9, 0xe5598af, 0x6d1ea6aa, 0x5cfd99cd, 0x80506220, 0x787c02d7, 0x5af990b7, 0x9254f186, 0x8ae161c4, 0x7b3241a1, 0x208020f7, 0x607e4f93, 0xa0caeb9e, 0xc1ba9352, 0xe0443477, 0x8fb09444,
         0x4ffbee07, 0xc20acecd, 0xb4b19b70, 0x36d4eea6, 0x767a8364, 0xf7270de6, 0xf140b597, 0xb3b2dd6c, 0x3de9900e, 0x3bd1e35f, 0x919a37d6, 0x153fee65, 0xc364988d, 0xdc50a2e0, 0xe5d6d59c, 0xf704d85f
      };
      uint32 b[128] = {
         0x95ade248, 0xdd8e8ffa, 0xe87cbb55, 0x6508ef69, 0x3c52a0f6, 0x9de071fa, 0x406eb45c, 0xbe7ff355, 0x190d5927, 0xb9c323df, 0x1a5882ea, 0x433ab3ae, 0x7517810c, 0xfdad8b60, 0xf9278a14, 0x516eaacb,
         0x45232a43, 0x8339491b, 0x27f0e6fa, 0x65d99f34, 0x3bc70e5a, 0xc9022c24, 0x37d8bf6f, 0x1e5bf01b, 0x42f93f18, 0x4ab88a01, 0x5a332afd, 0x9bcd6ecf, 0xee70c460, 0x8b153500, 0x39707bc5, 0x2b037143,
         0x5426081f, 0x64aa57ed, 0x55750ec4, 0xcf6b24d8, 0x1cb72a6f, 0x323b6e03, 0xe53a5b, 0x84cf806f, 0x5cbbed93, 0xd047592a, 0xa1e2d544, 0x20b47d31, 0xd93bcc6f, 0x5bf51c82, 0x4da1689f, 0x2ba05bfb,
         0x371ea97a, 0x5588b9bd, 0x123fad1a, 0x848c97af, 0x5fe177e5, 0x54ad56b8, 0x86466680, 0x61c9971d, 0x2d02043e, 0xcebd6298, 0xbb22647d, 0x37fc5330, 0x5a297934, 0xccbada36, 0xc15a2772, 0x31e19689,
         0xead1a426, 0x43d653bf, 0xe410549a, 0xd577023d, 0xed15ea4d, 0xaf675a5e, 0xc19de3d4, 0x7bbffc66, 0x99dcf856, 0xda8ba92, 0x17a19b0e, 0x2c9b2b2f, 0xbc56ce1, 0x99142a80, 0x917f2e1d, 0xbb99def9,
         0xcefcb831, 0xa120bbac, 0x537e8448, 0xc76b4bdf, 0x78aeebd7, 0xe58e4f3e, 0x75d8e000, 0x7a6b2783, 0x7fe8330b, 0x835450d2, 0xe087987a, 0x416c341, 0x71d57dde, 0x1108591c, 0x72327a3a, 0x1e9e7ad1,
         0xd8500350, 0xcdc71d53, 0x6a87dac, 0xfc565f8, 0x4e8115e9, 0x44987181, 0xa93211b8, 0x9bda36f7, 0xed62d6eb, 0xd176b3af, 0xe2c94135, 0x884d9645, 0xd2cb6df0, 0xd5004d0c, 0x83bb35e9, 0x376e60a,
         0xea19665, 0x26daeae3, 0x5a696eff, 0xf9e6d104, 0x1f1ddbed, 0x3bf01958, 0x6cc2e998, 0x54c5f159, 0x2efcab9, 0xd0f9b4f8, 0x71c6abf5, 0x2d034000, 0x4b0dca2e, 0xb3fdb7d0, 0x96468dfb, 0xf8a68439
      };
      uint32 r[NBEL(a) + NBEL(b)];
#if 0
      omul(r, a, NBEL(a), b, NBEL(b));
      for (unsigned i=0; i<NBEL(a)+NBEL(b); ++i) {
         tout_puts(format("%#x, ") << r[i]);
         if (i % 8 == 7) tout_puts("\n");
      }
#endif
      q_dkss_mul1<uint32, false>(r, a, NBEL(a), b, NBEL(b));
      uint32 rc[NBEL(a) + NBEL(b)] = {
         0x22ec408, 0xe7f23645, 0x3e09a266, 0x73af73f0, 0x70b95eec, 0x29a5449a, 0xa0e59210, 0xb9f8c5ef, 0xd0aa7377, 0x551f5f1c, 0xde211696, 0x5ee98f01, 0xd6108eb, 0x7f73bd41, 0xd0e38175, 0xaf0f7c6,
         0x1e06c2be, 0x15ddd341, 0x512a117a, 0xf04761a2, 0x37290130, 0x57af8cc2, 0xc3b58458, 0xf9a321bf, 0xf3bc2656, 0xae01dc2c, 0xb04a8620, 0x2d976d3b, 0x2bba3c86, 0x359b149e, 0xfc6639e5, 0x74ef0c16,
         0x7f2c89d7, 0x3b5ff79f, 0xd6bf1897, 0x639d93b6, 0x1e522ed7, 0x93a12abc, 0x36c19e1, 0x309f9865, 0x13d19cd6, 0x4faa7e5f, 0xd58fa741, 0x233b35f0, 0xaea096bc, 0x718b9383, 0x16715b23, 0xe26f6434,
         0x4596756f, 0x7a546e77, 0xbdd869d6, 0x2003ac71, 0x89f9597f, 0x37c7029e, 0x4a50b435, 0xf0f50603, 0x6c2d59af, 0x3252d688, 0xe8098926, 0x3f0aa84e, 0x89e5790d, 0x6d906e24, 0xcc0935fb, 0x1ca70064,
         0x335f765e, 0x27759f70, 0xaf6ba6e9, 0x754845be, 0xddc45425, 0x3c80223f, 0x24236119, 0x4ca037a9, 0xf72dc378, 0x5c7b38ed, 0x48db7176, 0xf67f7759, 0x24d14479, 0x76282388, 0x1d76e72d, 0x3fc7cd31,
         0x69075e13, 0x4b39c9a, 0xd5353264, 0xcc8a43c3, 0xd16cc8bf, 0xc301f52b, 0x5e475894, 0x1dcb2331, 0x2d311879, 0xac89bf79, 0x5e7627c8, 0x7193939e, 0xfcb12db1, 0x2bc55de3, 0x23364b4d, 0x7332617c,
         0x337e6dd0, 0x8a193a4d, 0x3e298b45, 0xc76a263b, 0x73e4de44, 0xa7324ad5, 0x7270dbb5, 0xdfcc6ea0, 0x8a17b176, 0x2d863e0f, 0xa7bf3e4f, 0x61db330e, 0x5aa44ba6, 0x7f4ee913, 0xf347b802, 0xa0f1eee5,
         0x538bae92, 0x3280b035, 0x10a75400, 0xca3d4f84, 0x5b72af42, 0x65ee61e1, 0xa043a3eb, 0xccbe11e2, 0x7aa45152, 0x440ed893, 0x133cd84b, 0x46f912d5, 0x383f07f5, 0x11f0d83f, 0x3274eec1, 0x3e3f388f,
         0x456849cb, 0x82124d26, 0xe75c6c6d, 0xc2f79b7b, 0x1335a96e, 0x55a2aab6, 0xc3f2a1bd, 0xd7606860, 0xb9e67e80, 0xd8a3bbf4, 0xa2dfcc44, 0xfdd2bbb8, 0x2a2f0d1b, 0x2c832990, 0x87a360c8, 0x11376091,
         0xf0857d36, 0x2b0654f, 0xf9c2ab82, 0x9eb1f17f, 0x4b7817b2, 0xbcc80361, 0x395e429b, 0x8e0abb3e, 0x3a7d5a04, 0x8c512c9d, 0xc2f6e187, 0x1828edbe, 0xf208643a, 0x7553edf2, 0xf8bc7173, 0x3b05de60,
         0xc86e8d56, 0x7ae17633, 0x4f5e5f22, 0x2566d91b, 0xc88cc851, 0xcd60a34e, 0x36daf990, 0x880b74a, 0xb6b2f90b, 0x11824100, 0xd626e533, 0x708200ec, 0xd697c539, 0x1b5ff3d6, 0xd146b431, 0x63ea38f6,
         0x81c98abe, 0x8a44126a, 0xeacc7803, 0xb2f04d8e, 0x6feb0ef7, 0x2361d3b4, 0x22409522, 0x28e968d1, 0x8ff2a887, 0xc135bdef, 0x42d0bc14, 0x8c9ef156, 0x166d3082, 0x6c7a8fbc, 0xca15a954, 0xe3849f87,
         0x31a369e4, 0x34daca16, 0xba0c2151, 0x71d4d2d3, 0x714975d, 0x7d196b95, 0x95806311, 0x4f750db5, 0xa3b77d9f, 0x9893b90f, 0x8cde5cd4, 0xef87dc0e, 0x5bdf90c5, 0xfcb284b8, 0x76c20587, 0x5023deca,
         0xb4515b2b, 0x72daa271, 0xa6e7bc16, 0xd31e613b, 0x85a3d68, 0x33b9aa4c, 0x74ee4344, 0x5c0600c5, 0xa69cfa3, 0xb87cda4, 0x1f2586b3, 0xb6dcb635, 0x1b1a45fe, 0x57bd6676, 0x8e2bd7d1, 0x4f0bbe0a,
         0xe35ca121, 0x2f21a43e, 0x83a728d9, 0xc1cb414, 0x33afcf41, 0xd1bc269a, 0x15508266, 0x11fb080d, 0x73bc9118, 0x7c91dc28, 0xeef740ec, 0x6c3c36cb, 0x18e202c4, 0x5894f63e, 0x7920cf5, 0x445d8fd0,
         0x18767e6d, 0xb4aba499, 0x75b856b9, 0x315c5158, 0x5e46a50b, 0x682ea635, 0x723ff3cc, 0xba41c89d, 0x7bedbc42, 0xbd36f92e, 0x778680e9, 0x32693e1a, 0x53c2150e, 0x519e5192, 0x457db337, 0xefed5e57,
      };
      classert(memcmp(r, rc, sizeof(r)) == 0);
   }
}
#endif



// $dissect$ support.txt
// ------ last words ----------------------------------------------------------

#ifdef CL_TEST_CODE
void _CL_CALL test_bignum_dkss() {
   test_dump_poly_hex();
   test_dump_polypoly_hex();
   test_modpoly_add();
   test_modpoly_sub();
   test_comp_poly();
   test_poly_to_int();
   test_int_to_poly();
   test_poly_to_int_and_back();
   test_modpoly_mul_mod_mp1();
   test_modpoly_pow_mod_mp1();
   test_negmod_fast();
   test_modpoly_mul_xpow_mod_mp1();

   test_dkss_inner_fft_eval();
   test_dkss_fft();
   test_dkss_sumup2();
   test_dkss_sumup();
   test_dkss_mul();

   test_check_primitivity();
   test_dkss_set_parm();
   test_dkss_encode();
}
#endif


// ------ memory requirement --------------------------------------------------

static index dkss_mul_fft_oc(const index M, const index m) {
   if (M <= m) return 1;  // use inner DFT right away
   else return 1 + dkss_mul_fft_oc(M/m/2, m);
}

template<typename small, bool square>
index q_dkss_mul_mem1(const index alen, const index blen) {
   caassert(!square || alen == blen);

   dkss_parm<small> p;
   dkss_set_mmu<small>(p, alen, blen);
   // are only set in dkss_set_parm2()
   p.iclen = (p.pc.bit_length() + bits(small) - 1) / bits(small);
   p.oclen = p.iclen * p.m;  // m * iclen
   p.polylen = p.oclen * p.M * 2;  // 2M * m * iclen

   return sizeof(small) * (
      (1 + p.M/p.m) * p.oclen +  // for dkss_parm::rho and rho_pow
      (square ? 1 : 2) * p.polylen +  // encoding
      dkss_mul_fft_oc(p.M, p.m) * p.oclen +  // for FFT
      0);
}

template<typename small, bool square>
index q_dkss_mul_mem(const index alen, const index blen) {
   caassert(!square || alen == blen);

   //if (!q_use_dkss_mul<square>(alen, blen)) return smul_mem<small, square>(alen, blen);

   return q_dkss_mul_mem1<small,square>(alen, blen);
}



// ------ generate template functions -----------------------------------------

#pragma inline_depth(0)

template<typename small>
void generate_bignum_dkss() {
   small u;
   q_dkss_mul<small,false>(&u, &u, 0, &u, 0);
   q_dkss_mul<small,true>(&u, &u, 0, &u, 0);
   dkss_parm<small> parm;
   dkss_set_parm<small>(parm, 0, 0);
   modpoly_mul_mod_mp1_size<small>(0, 0);
   q_dkss_mul_mem<small,false>(0, 0);
   q_dkss_mul_mem<small,true>(0, 0);
}

void _generate_bignum_dkss() {
   classertm(0, "must not be called");
   generate_bignum_dkss<uint8>();
   generate_bignum_dkss<uint16>();
   generate_bignum_dkss<uint32>();
#ifdef _M_X64
   generate_bignum_dkss<uint64>();
#endif
}

#pragma inline_depth()

}  // namespace bignum
