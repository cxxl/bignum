// $+HEADER$
// 
// Copyright 2015 Christoph Lueders
// 
// This file is part of the BIGNUM Library.
// 
// The BIGNUM Library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
// 
// The BIGNUM Library is distributed in the hope that it will be useful, but 
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public 
// License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with the BIGNUM Library.  If not, see <http://www.gnu.org/licenses/>.
// 
// $-HEADER$

// $Date: 2015-04-24 11:28:16 +0200 (Fr, 24 Apr 2015) $
// $Rev: 26590 $

/*
 *  arbitrary length number arithmetic - addition/subtraction in one
 */

#include "precomp.h"
#include "impl.h"
#include "small.h"
#include "cl/memzero.h"


// we must switch off "unreachable code" warning, since bool template parameters generate code paths that are never used
#pragma warning (disable:4702)

namespace bignum {


// ------ ADDITION/SUBTRACTION IN ONE ----------------------------------------

// addition and subtraction of two const input numbers 'a' and 'b' into pre-reserved results 'sum' and 'diff'
// all numbers have the same length 'len'

// length can be zero, numbers don't need to be normalised

template<typename small, bool hassign>
small taddsub(small* sum, small* diff, const small* a, const small* b, index len) {
   caassert(sum != 0);
   caassert(diff != 0);
   caassert(a != 0);
   caassert(b != 0);
   caassert((small)-1 > 0);  // must be unsigned type
   //classert(a != sum);
   classert(a != diff);
   //classert(b != sum);
   classert(b != diff);

   // perform sub first, then add: this is needed for use in smul, where sum == a
   small c = tsub_ls<small, false, hassign>(diff, len, a, len, b, len) << 1;
   c |= tadd_ls<small, hassign>(sum, len, a, len, b, len);

   return c;
}


#ifdef CL_TEST_CODE
template<typename small>
static void test_bignum_addsubu() {
   const unsigned sz = 20;
   small a[sz];
   small b[sz];
   small sum1[sz];
   small sum2[sz];
   small diff1[sz];
   small diff2[sz];

   memzero(a);
   a[0] = 1;
   memzero(b);
   b[0] = 3;
   small c = addsubu(sum1, diff1, a, b, 4);
   small csum = addu(sum2, 4, a, 4, b, 4);
   small cdiff = subu(diff2, 4, a, 4, b, 4);
   classert(memcmp(sum1, sum2, 4*sizeof(small)) == 0);
   classert((c & 1) == csum);
   classert(memcmp(diff1, diff2, 4*sizeof(small)) == 0);
   classert((c >> 1) == cdiff);

   // fill a, b
   rand_fill(a, sz);
   rand_fill(b, sz);

   for (unsigned i=0; i<sz; ++i) {
      c = addsubu(sum1, diff1, a, b, i);
      csum = addu(sum2, i, a, i, b, i);
      cdiff = subu(diff2, i, a, i, b, i);
      classert(memcmp(sum1, sum2, i*sizeof(small)) == 0);
      classert((c & 1) == csum);
      classert(memcmp(diff1, diff2, i*sizeof(small)) == 0);
      classert((c >> 1) == cdiff);

      // other direction
      c = addsubu(sum1, diff1, b, a, i);
      csum = addu(sum2, i, b, i, a, i);
      cdiff = subu(diff2, i, b, i, a, i);
      classert(memcmp(sum1, sum2, i*sizeof(small)) == 0);
      classert((c & 1) == csum);
      classert(memcmp(diff1, diff2, i*sizeof(small)) == 0);
      classert((c >> 1) == cdiff);
   }
}

void _CL_CALL test_bignum_addsub() {
   test_bignum_addsubu<uint8>();
   test_bignum_addsubu<uint16>();
   test_bignum_addsubu<uint32>();
#ifdef _M_X64
   test_bignum_addsubu<uint64>();
#endif
}
#endif


// ------ generate template functions -----------------------------------------

//#if _MSC_VER < 1300
#pragma inline_depth(0)

template<typename small>
void generate_bignum_addsubs() {
   addsubu<small>(0, 0, 0, 0, 0);
}

void _generate_bignum_addsubs() {
   classertm(0, "must not be called");
   generate_bignum_addsubs<uint8>();
   generate_bignum_addsubs<uint16>();
   generate_bignum_addsubs<uint32>();
#ifdef _M_X64
   generate_bignum_addsubs<uint64>();
#endif
}

#pragma inline_depth()
//#endif


}  // namespace bignum
