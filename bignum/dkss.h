// $+HEADER$
// 
// Copyright 2015 Christoph Lueders
// 
// This file is part of the BIGNUM Library.
// 
// The BIGNUM Library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
// 
// The BIGNUM Library is distributed in the hope that it will be useful, but 
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public 
// License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with the BIGNUM Library.  If not, see <http://www.gnu.org/licenses/>.
// 
// $-HEADER$

// $Date: 2015-04-24 11:47:36 +0200 (Fr, 24 Apr 2015) $
// $Rev: 26592 $

// $dissect$ support.txt
#ifndef CL_BIGNUM_DKSS_H
#define CL_BIGNUM_DKSS_H 1

#pragma pack(push, 8)  // arbitrary but fixed

#include "bignum_t.h"

void _CL_CALL find_roots(ubignum_t from_bits, ubignum_t to_bits);

namespace bignum {

struct ring_data {
   uint64 factor;
   bit_index pow_2;       // (power of 2) that is contained in prime
   bit_index prime_bit_length;  // prime.bit_length()
   //bignum_t prime;      // 2^pow2 | prime - 1
   bignum_t generator;
   bignum_t make_prime() const {
      bignum_t p = factor;
      p <<= pow_2;
      ++p;
      return p;
   }
};

const ring_data* _CL_CALL dkss_find_pc(const unsigned min_prime_bits);


template<typename small>
struct dkss_parm {
   typedef basic_bignum<small,false> bn_type;
   index N;          // size in bits
   index M;          // ~ N / log^2 N
   index m;          // ~ log N
   unsigned logN;
   unsigned logM;
   unsigned logm;
   index u;          // ~ 2 log N
   bn_type p;
   unsigned c;
   bn_type pc;     // p^c
   bn_type zeta;   // generator of Z/pZ and primitive (p-1)-th root
   bn_type omega;  // primitive 2M-th root
   index iclen;      // inner coefficient length (in small's)
   index oclen;      // outer coefficient length (in small's)
   index polylen;    // length of whole degree (2M-1) polynomial over R
   small* rho;       // has length 'oclen'
   small* rho_pow;   // M/m * length 'oclen'
   bn_type len_inv;  // inverse of FFT length 2M
   static const index max_len = (index)-1 / (2 * bits(small));

   dkss_parm() {
      rho = nullptr;
      rho_pow = nullptr;
   }
   ~dkss_parm() {
      bignum_free<small>(rho);
      bignum_free<small>(rho_pow);
   }

   void do_assert() const {
#ifdef CL_TEST_CODE
      classert(is_pow2(M));
      classert(is_pow2(m));
      classert((index)1 << logM == M);
      classert((index)1 << logm == m);
      classert(M*u*m/2 >= N);
      classert(c >= 1);
      classert(p.pow(c) == pc);
      classert((p & (bn_type(M) * 2 - 1)) == 1);
      classert(pc.bit_length() >= 2 * u + logM + logm);
      classert(pc.bit_length() <= iclen * bits(small));
#endif
   }
   bool is_same(const dkss_parm& q, const bool precise) const {
      if (precise && (u != q.u || pc != q.pc)) return false;
      return M == q.M && m == q.m && pc.word_length() == q.pc.word_length();
   }
#ifndef NDEBUG
   inline void assert_rho() const;
#endif
};

template<typename small>
void dkss_set_mmu(dkss_parm<small>& parm, const index alen, const index blen);

template<typename small>
void dkss_set_mmu_dbg(dkss_parm<small>& parm, const index N, const index M, const index m, const index u, const char* prime_str, const small c, const small zeta);

template<typename small>
void dkss_set_parm(dkss_parm<small>& parm, const index alen, const index blen);

template<typename small>
index modpoly_mul_mod_mp1_size(const index sz, const index len);

template<typename small, bool square>
void dkss_mul3(dkss_parm<small>& p, small* r, const small* a, const index alen, const small* b, const index blen);

template<typename small>
void dkss_mul2(dkss_parm<small>& p, small* r, const small* a, const index alen, const small* b, const index blen);

template<typename small, bool square>
index q_dkss_mul_mem1(const index alen, const index blen);

extern bool dkss_pc_bits_roundup;

}  // namespace bignum

#pragma pack(pop)

#endif  // ndef CL_BIGNUM_DKSS_H
