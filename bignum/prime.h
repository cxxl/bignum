/*
 *  arbitrary length number arithmetic
 */

// $+HEADER$
// 
// Copyright 2015 Christoph Lueders
// 
// This file is part of the BIGNUM Library.
// 
// The BIGNUM Library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
// 
// The BIGNUM Library is distributed in the hope that it will be useful, but 
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public 
// License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with the BIGNUM Library.  If not, see <http://www.gnu.org/licenses/>.
// 
// $-HEADER$

// $Date: 2015-04-24 11:47:36 +0200 (Fr, 24 Apr 2015) $
// $Rev: 26592 $

#ifndef CL_BIGNUM_PRIME_H
#define CL_BIGNUM_PRIME_H 1

#ifndef CL_CLLIB_H
#include "cl/cllib.h"
#endif

#include "bignum_t.h"

#pragma pack(push, 8)  // arbitrary but fixed

namespace bignum {

   using namespace cl;

   template<class C>
   bool _CL_CALL is_prime(const C n, C& factor, bool force_factor=false, C start=3);
   template<class C>
   bool _CL_CALL is_prime(const C n) {
      C f;
      return is_prime(n, f, false, (C)3);
   }

   void _CL_CALL build_small_siv(size_t max=128*1024*16);  // uses 128kb memory, takes 0.002143s on HT IBR core

   uint64 _CL_CALL _is_prime_eta(const bignum_t& n);

   template<typename T>
   uint64 _CL_CALL is_prime_eta(const T _n) {
      bignum_t n = _n;
      return _is_prime_eta(n);
   }


   // ------ sieving, odd bit-sieve -------------------------------------------

   template<typename T>
   struct prime_sieve {
      T* buf;        // buffer to odd bit-sieve
      size_t max;    // max value included in sieve
      size_t bytes;  // bytes actually allocated
      static const size_t div = sizeof(T) * 8 * 2;

      prime_sieve(size_t to=0);
      void dealloc();
      bool alloc(size_t m);
      inline bool is_set(size_t idx) const;
      inline void set(size_t idx);
      inline bool is_prime(size_t idx) const;
      inline bool _is_prime(size_t idx) const;
      void sieve(size_t to);
      size_t count_primes() const;
   };
   extern prime_sieve<limb> *small_siv;

   template<typename T>
   struct cycle_sieve : public prime_sieve<T> {
      size_t prod;
      size_t start;
      size_t highp;
      template<class C> void sieve_part(const prime_sieve<C>& siv, size_t from, size_t to, size_t highp);
      template<class C> void init_cycle(unsigned prime_cnt, const prime_sieve<C>& siv);
   };


   // ------ factorize --------------------------------------------------------

   template<typename N, typename P>
   bool factorize(N n, vector<pair<N, P>>& factors);


   // ------ lucas primality test ------------------------------------------------

   enum lucas_ret { lucas_error = -1, lucas_is_prime = 0, lucas_is_composite, lucas_unknown  };
   lucas_ret _CL_CALL lucas_prime_test(const ubignum_t& n);

}  // namespace bignum

#pragma pack(pop)

#endif  // ndef CL_BIGNUM_PRIME_H
