/*
 *  arbitrary length number arithmetic - fft functions helper header
 */

// $+HEADER$
// 
// Copyright 2015 Christoph Lueders
// 
// This file is part of the BIGNUM Library.
// 
// The BIGNUM Library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
// 
// The BIGNUM Library is distributed in the hope that it will be useful, but 
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public 
// License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with the BIGNUM Library.  If not, see <http://www.gnu.org/licenses/>.
// 
// $-HEADER$

// $Date: 2015-04-24 11:47:36 +0200 (Fr, 24 Apr 2015) $
// $Rev: 26592 $

#ifndef CL_BIGNUM_FFT_H
#define CL_BIGNUM_FFT_H 1

#include "cl/int_math.h"

#pragma pack(push, 8)  // arbitrary but fixed

namespace bignum {

using namespace cl;

// reverse the low k bits
template<typename small>
static small bit_rev(const small i, const index k) {
   caassert(k > 0);
   caassert(k <= bits(i));
   caassert(k == bits(i) || (i & ~(((small)1 << k) - 1)) == 0);  // upper bits must be zero
   const small r = bit_reverse(i) >> (bits(i) - k);
   caassert(k == bits(i) || (r & ~(((small)1 << k) - 1)) == 0);  // upper bits must be zero
   return r;
}


// shuffle FFT input vector 'r',
// consisting of '1 << lg' items, each 'len' small's long

template<typename small>
static void fft_shuffle(small* r, const unsigned lg, const index len) {
   const index sz = (index)1 << lg;

   for (index i = 0; i < sz; ++i) {
      const index j = bit_rev(i, lg);
      if (i < j) swap_in(r+i*len, r+j*len, len);
   }
}

}  // namespace bignum

#pragma pack(pop)

#endif  // ndef CL_BIGNUM_FFT_H
