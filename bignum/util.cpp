// $+HEADER$
// 
// Copyright 2015 Christoph Lueders
// 
// This file is part of the BIGNUM Library.
// 
// The BIGNUM Library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
// 
// The BIGNUM Library is distributed in the hope that it will be useful, but 
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public 
// License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with the BIGNUM Library.  If not, see <http://www.gnu.org/licenses/>.
// 
// $-HEADER$

// $Date: 2015-04-24 11:28:16 +0200 (Fr, 24 Apr 2015) $
// $Rev: 26590 $

/*
 *  arbitrary length number arithmetic - utility functions
 */

#include "precomp.h"
#include <random>
#include "impl.h"
#include "small.h"
#include "cl/int_math.h"
#include "cl/format.h"
#include "cl/cpu.h"
#include "cl/tracer.h"
#include "cl/osver.h"


// we must switch off "unreachable code" warning, since bool template parameters generate code paths that are never used
#pragma warning (disable:4702)

namespace bignum {

// must be inited before tape_alloc is used, i.e. before all other bignum routines
#pragma warning(disable: 4075)
#pragma init_seg(".CRT$XCM_BIGNUM_B")

wstring worker_name = L"0";
wstring working_directory;
wstring command_line;
string start_datetime;
uint64 start_cycles;
int run_proc = -1;
const size_t page_size = osver.page_size();


// ------ memcpy --------------------------------------------------------------

// http://blog.kaetemi.be/post/2009/10/25/SSE2-memcpy
// http://stackoverflow.com/questions/4260602/how-to-increase-performance-of-memcpy



// ------ math ----------------------------------------------------------------

const double LOG_2_RECIPROCAL = 1.4426950408889634;  // 1.0/ln(2.0)
double _CL_CALL log2(const double x) {
   return log(x) * LOG_2_RECIPROCAL;
}

double _CL_CALL xlog2(double x) {
   if (x < 2.0) return 1.0;
   return log2(x);
}

double _CL_CALL slog(double base, double z) {
/*
def logs(n):
   if n <= 1:
      return 0
   if n <= 2:
      return -1 + log2(n)
   return 1 + logs(log2(n))
*/
   if (z <= 0.0) return slog(base, pow(base, z)) - 1.0;
   if (z <= 1.0) return z - 1.0;
   return slog(base, log(z) / log(base)) + 1.0;
}

// from http://en.wikipedia.org/wiki/Iterated_logarithm
// the iterated logarithm of n is the number of times the logarithm function
// must be iteratively applied before the result is less than or equal to 1.

double _CL_CALL log2_star(double x) {
   return slog(2, x);
}

double _CL_CALL xlog2_star(double x) {
   if (x <= 2.0) return 1.0;
   return log2_star(x);
}

#ifdef CL_TEST_CODE
static void test_log2_star() {
   static double eps = 0.000001;

   double x = log2_star(1.0);
   classert(x <= eps);

   x = log2_star(2.0);
   classert(fabs(1.0 - x / 1.0) <= eps);

   x = log2_star(4.0);
   classert(fabs(1.0 - x / 2.0) <= eps);

   x = log2_star(16.0);
   classert(fabs(1.0 - x / 3.0) <= eps);

   x = log2_star(65536.0);
   classert(fabs(1.0 - x / 4.0) <= eps);
}
#endif



// ------ estimations ---------------------------------------------------------

const uint64 clock_rate = cpuid.query_clock_speed() * 1000000ui64;

// estimate the number of cycles per multiplication
double _CL_CALL est_mul_cycles(const index ll) {  // input is number of max-sized small's for this architecture
   caassert(xlog2(0) >= 1);
   const index bitlen = bits(limb) * ll;
   const double bitlog = xlog2(bitlen);
   const double r = bitlen * bitlog * xlog2(bitlog) / (CL_IF_X64(6.61, 0.75) / CL_IF_DEBUG(2.5, 1.0));
   //DTOUT_PUTS(format("est_mul_cycles(%?)=%,.6? \n") << ll << (int64)(r * 1000000i64));
   return r;
}

double _CL_CALL est_mul_time(const index ll) {  // input is number of max-sized small's for this architecture
   caassert(clock_rate > 0);
   return clock_rate / est_mul_cycles(ll);
}

uint64 _CL_CALL est_mul_rounds(const index ll, const uint64 msec) {  // input is number of max-sized small's for this architecture
   caassert(msec > 0);
   return ceil((double)msec * clock_rate / (1000.0 * est_mul_cycles(ll)));
}


// ------ ALLOCATION ----------------------------------------------------------

struct mem_lal {
   struct mem_descr {
      void* p;
      size_t sz;
      mem_descr(void* _p, size_t _sz) : p(_p), sz(_sz) {}
   };

   static const size_t max_sz = 8;
   vector<mem_descr> lal;

   ~mem_lal() {
      clear();
   }
   void* alloc(size_t sz, size_t& got_sz) {
      size_t i = lal.size();
      while (i > 0) {
         auto* descr = &lal[--i];
         if (descr->sz >= sz && (descr->sz <= sz + 64 || descr->sz <= sz + sz/8)) {
            // match
            got_sz = descr->sz;
            void* p = descr->p;
            lal.erase(lal.begin() + i);
            return p;
         }
      }
      got_sz = sz;
      return _aligned_malloc(sz, bignum_align);
   }
   void free(void* p, size_t sz) {
      if (lal.size() >= max_sz) _aligned_free(p);
      else lal.push_back(mem_descr(p, sz));
   }
   void clear() {
      for (size_t i = 0; i < lal.size(); ++i) _aligned_free(lal[i].p);
      lal.clear();
   }
};
static mem_lal lal;

#ifdef CL_TEST_CODE
static void test_bignum_alloc_lal() {
   {
      mem_lal lal;
      size_t got;
      size_t want = 1000;
      void* p = lal.alloc(want, got);
      classert(got >= want);
      lal.free(p, got);

      want -= 16;
      void* p2 = lal.alloc(want, got);
      classert(got >= want);
      classert(p2 == p);

      void* p3 = lal.alloc(want, got);
      classert(got >= want);
      classert(p3 != p);

      lal.free(p3, got);
      lal.free(p2, got);
   }
   {
      lal.clear();
      uint32* a = bignum_alloc<uint32>(20);
      bignum_free(a);
      uint32* b = bignum_alloc<uint32>(20);
      classert(a == b);
      bignum_free(b);
   }
}
#endif


CLDEBUG(size_t alloc_cnt = 0);

// lingo: 'len' is in smalls, 'sz' is in bytes

template<typename small>
small* bignum_alloc(index len) {
   index max_sz = (len * sizeof(small) + bignum_align - 1) / bignum_align * bignum_align;
   index got_sz;
   small* p = (small*)lal.alloc(bignum_aligned_head_sz + max_sz, got_sz);
   if (p == nullptr) {
      terr_puts("out of memory\n");
      return 0;
   }
   CLDEBUG(++alloc_cnt);
   got_sz -= bignum_aligned_head_sz;
   //DTOUT_PUTS(format("alloc(%?) = %p\n") << got_sz

/*   caassert((uint_ptr)p % MEMORY_ALLOCATION_ALIGNMENT == 0);

   // http://msdn.microsoft.com/en-us/library/ycsb6wwf.aspx says:
   // malloc() only has a 16-byte alignment guarantee on x64 platform. On x86 it guarantees 8-byte alignment.
#ifdef _M_X86
   caassert((uint_ptr)p % 8 == 0);
#elif defined _M_X64
   caassert((uint_ptr)p % 16 == 0);
#endif*/

   bignum_head* head = (bignum_head*)p;
   head->refcnt = 1;       // refcnt
   //head->front_pad = 0;    // front_pad
   head->maxlen = got_sz / sizeof(small);  // maxlen
   head->len = 0;          // len
   return (small*)((char*)p + bignum_aligned_head_sz);
}

template<typename small>
void bignum_free(small* p) {
   if (p == nullptr) return;
   bignum_head* head = (bignum_head*)((char*)p - bignum_aligned_head_sz);
   lal.free((char*)p - bignum_aligned_head_sz, head->maxlen * sizeof(small) + bignum_aligned_head_sz);
   CLDEBUG(--alloc_cnt);
}

void bignum_lal_clear() {
   lal.clear();
}


// ------ rand_alloc

rand_maker make_rand;

template<typename small>
void _CL_CALL rand_fill(small* p, index len) {
   for (index i=0; i<len; ++i)
      p[i] = make_rand.get<small>();
}

template<typename small>
small* _CL_CALL rand_alloc(index len) {
   small* p = bignum_alloc<small>(len);
   rand_fill(p, len);
   return p;
}


// ------ TAPE ALLOCATION -----------------------------------------------------

tape::tape(const size_t _len) : len(_len), used(0) {
   p = (byte*)_aligned_malloc(len, page_size);
   if (p == 0) {
      char buf[100];
      sformat(buf, sizeof(buf), "out of memory: len=%? total_sz=%? max_total_sz=%?\n") << len << tapemgr.total_sz << tapemgr.max_total_sz;
      terr_puts(buf);
      tapemgr.print_tapes(terr);
      end = 0;
      exit(98);  // error exit
      return;
   }
   //tout_puts(format("[M: max=%'? total=%'? new=%'?]\n") << tapemgr.max_total_sz << tapemgr.total_sz << len);
   end = p + len;
}

string tape::print_layout() const {
   string s;
   s = format("len=%? used=%? p=%p") << len << used << p;
   return s;
}

tape::tape(bool) : len(0), used(0), p(0), end(0) { }  // just for init

tape::~tape() {
   _aligned_free(p);
}

tape_mgr::tape_mgr() {
   tape_idx = 0;
   tapes.push_back(new tape(init_max_total_sz));  // push an empty tape at the end
   max_total_sz = total_sz = init_max_total_sz;
}

tape_mgr::~tape_mgr() {
   for (size_t i=0; i<tapes.size(); ++i)
      delete tapes[i];
}

byte* tape_mgr::alloc(const size_t sz, byte*& old) {  // in bytes
   classert(sz < 10000000000 / 8);
   tape* t = tapes[tape_idx];
   old = t->p + t->used;
   do {
      byte* p = (byte*)((uint_ptr)(t->p + t->used + bignum_align - 1) & ~(bignum_align-1));
      if (p + sz <= t->end) {
         // there is enough space, use it and return
         t->used = p + sz - t->p;
         return p;
      }
      // not enough space on actual tape
      ++tape_idx;
      if (tape_idx < tapes.size()) {
         // we still have another free tape allocated
         classert(tape_idx + 1 == tapes.size());  // exactly one more tape!
         t = tapes[tape_idx];
         t->used = 0;  // and the tape is empty
         if (sz + bignum_align - 1 < t->len) continue;  // enough space on that tape, cycle and use it
         free_last_tape();  // it's too small, dealloc
      }
      caassert(tape_idx == tapes.size());
      // free all empty tapes at the end
      while (tape_idx > 1 && tapes.back()->used == 0) {
         free_last_tape();
         --tape_idx;
      }
      // allocate new tape
      const size_t alloc_sz = (t2max(sz + t2max(64*1024, sz/16), max_total_sz - total_sz) + page_size - 1) & ~(page_size - 1);
      tapes.push_back(new tape(alloc_sz));
      total_sz += alloc_sz;
      max_total_sz = t2max(max_total_sz, total_sz);
      t = tapes[tape_idx];
   } while (t->p != 0);  // don't cycle if out of memory
   return 0;  // out of memory
}

void tape_mgr::unroll(byte* old) {
   caassert(old != nullptr);
   tape* t = tapes[tape_idx];
   if (old >= t->p && old <= t->end) {
      // 'old' is in range of the actual tape
      t->used = old - t->p;
      return;
   }
   // allocation must come from one tape further down
   caassert(tape_idx > 0);
   t = tapes[--tape_idx];
   caassert(old >= t->p && old <= t->end);  // must match
   t->used = old - t->p;
   if (tapes.size() <= 1) {
      caassert(tapes.size() == 1);
      return;
   }

   // let's see if we can free a tape:
   // let's say tape_idx just went down to 1, but we still have 4 tapes: the start tape and 3 more tapes.
   // the actual tape 1 we want to keep
   // all tapes 2 and 3 can be freed.  the next alloc will allocate a tape that has the summed size
   if (tapes.size() > tape_idx + 2)
      free_unused_tapes();
}

void tape_mgr::free_unused_tapes() {
   // free unused tapes
   while (tapes.size() > tape_idx + 1)
      free_last_tape();
}

void tape_mgr::free_last_tape() {
   caassert(!tapes.empty());
   caassert(total_sz >= tapes.back()->len);
   total_sz -= tapes.back()->len;
   delete tapes.back();
   tapes.pop_back();
}

void tape_mgr::free_all_tapes() {
   // free all tapes
   while (!tapes.empty())
      free_last_tape();
   tapes.push_back(new tape(max_total_sz));  // push an empty tape at the end
   tape_idx = 0;
}

void tape_mgr::reset_max_total_sz() {
   max_total_sz = init_max_total_sz;
}

void tape_mgr::set_max_total_sz(size_t new_sz) {
   max_total_sz = tmax((new_sz + page_size - 1) & ~(page_size - 1), init_max_total_sz);
}

void tape_mgr::print_tapes(const tracer& t) const {
   for (size_t i=0; i<tapes.size(); ++i) {
      t.puts(format("[%?]: %?\n") << i << tapes[i]->print_layout());
   }
}

tape_mgr tapemgr;

#ifdef CL_TEST_CODE
/*const size_t test_alloc_sz = 10000;

static void test_bignum_tape_alloc2() {
   size_t old_idx = tapemgr.tape_idx;
   classert(tapemgr.tapes[old_idx]->free() >= test_alloc_sz);
   size_t old_used = tapemgr.tapes[old_idx]->used;
   {
      const index nb = 500;
      classert(nb*sizeof(uint32) < test_alloc_sz);
      tape_alloc<uint32> a(nb);
      classert(old_idx == tapemgr.tape_idx);
      size_t used = tapemgr.tapes[old_idx]->used;
      classert(used >= old_used + nb*sizeof(uint32));
      classert(used < old_used + nb*sizeof(uint32) + bignum_align);
   }
   classert(old_idx == tapemgr.tape_idx);
   size_t used = tapemgr.tapes[old_idx]->used;
   classert(used == old_used);

   const size_t old_max_total_sz = tapemgr.max_total_sz;
   // force allocation of new tape
   {
      const index nb = tapemgr.max_total_sz + 4096;
      tape_alloc<byte> a(nb);
      classert(tapemgr.tape_idx == old_idx + 1);
      classert(tapemgr.max_total_sz > old_max_total_sz);
   }
   // tape_idx must have reverted
   classert(tapemgr.tape_idx == old_idx);
   // but allocated tape must still be there
   classert(tapemgr.tapes.size() == old_idx + 2);

   // test if a new, even bigger allocation will free and alloc an even bigger tape
   // force allocation of again a new tape
   {
      const size_t ochunk = tapemgr.max_total_sz;  // save increased chunk size
      const size_t olen = tapemgr.tapes.back()->len;
      const index nb = tapemgr.max_total_sz + 4096;
      tape_alloc<byte> a(nb);
      classert(tapemgr.tape_idx == old_idx + 1);
      classert(tapemgr.max_total_sz > ochunk);
      classert(tapemgr.tapes.back()->len > olen);
   }
   // tape_idx must have reverted
   classert(tapemgr.tape_idx == old_idx);
}

static void test_bignum_tape_alloc() {
   tapemgr.free_unused_tapes();
   tapemgr.reset_max_total_sz();

   size_t old_idx = tapemgr.tape_idx;
   if (tapemgr.tapes[old_idx]->free() > test_alloc_sz)
      test_bignum_tape_alloc2();
   else {
      tape_alloc<uint32> a(test_alloc_sz);
      test_bignum_tape_alloc2();
   }

   tapemgr.free_unused_tapes();
   tapemgr.reset_max_total_sz();
}*/
#endif


// ------ physical memory -----------------------------------------------------

size_t _CL_CALL physical_mem(bool available) {
   MEMORYSTATUSEX mem_stat;
   mem_stat.dwLength = sizeof(mem_stat);
   GlobalMemoryStatusEx(&mem_stat);
   return available ? mem_stat.ullAvailPhys : mem_stat.ullTotalPhys;
}



// ------ NORMALISE -----------------------------------------------------------

template<typename small, bool hassign>
index tnorm(const small* p, index len) {
   caassert(p != nullptr);

   if (len == 0) return 0;
   if (p[len-1] == 0) {  // non-negative value
      while (len > 0 && p[len-1] == 0)
         --len;
      if (is_negative<small, hassign>(p, len)) ++len;  // highest bit must not be set for non-negative value
   }
   else if (hassign && p[len-1] == (small)-1) {  // negative value
      while (len > 1 && p[len-1] == (small)-1)
         --len;
      caassert(len > 0);
      if (!top_set(p[len-1])) ++len;
   }
   return len;
}

#ifdef CL_TEST_CODE
static void test_bignum_norm() {
   uint16 a[] = { 0, 0, 0, 0, 0 };
   index l = tnorm<uint16, true>(a, 5);
   classert(l == 0);

   l = normu(a, 5);
   classert(l == 0);

   l = normi(a, 0);
   classert(l == 0);

   a[0] = 1;
   l = normi(a, 5);
   classert(l == 1);

   l = normu(a, 5);
   classert(l == 1);

   a[0] = 0x8000;
   l = normi(a, 5);
   classert(l == 2);

   l = normu(a, 5);
   classert(l == 1);

   a[3] = 0xffff;
   a[4] = 0xffff;
   l = normi(a, 5);
   classert(l == 4);

   a[2] = 0x8000;
   l = normi(a, 5);
   classert(l == 3);
}

static void test_bignum_normu1() {
   uint16 a[] = { 0, 0 };
   index l = normu1<uint16>(a, 1);
   classert(l == 0);

   l = normu1(a, 2);
   classert(l == 1);

   a[0] = 1;
   l = normu1(a, 2);
   classert(l == 1);

   l = normu1(a, 1);
   classert(l == 1);

   a[0] = 0x8000;
   l = normu1(a, 2);
   classert(l == 1);

   l = normu1(a, 1);
   classert(l == 1);
}
#endif


// ------ NEGATION ------------------------------------------------------------

template<typename small>
void neg(small* r, const small* a, index len) {
   caassert(r != 0);
   caassert(a != 0);

   // improve: check when loop does only not operation
   small of = 0;
   index i = 0;
   while (i < len) {
      of = ops<small>::subc(r[i], 0, a[i], of);
      ++i;
   }
}

#ifdef CL_TEST_CODE
static void test_bignum_neg() {
   uint16 a[] = { 0, 0, 0, 0 };
   uint16 r[] = { 0, 0, 0, 0 };
   int c;

   neg(r, a, 4);
   c = compu_nn(r, 4, a, 4);
   classert(c == 0);

   uint16 aa = -1;
   a[0] = -aa;
   neg(r, a, 1);
   c = compi_nn(r, 1, &aa, 1);
   classert(c == 0);

   aa = 9999;
   one(a, 4);
   a[0] = -aa;
   neg(r, a, 4);
   c = compi_nn(r, 4, &aa, 1);
   classert(c == 0);
}
#endif


// ------ COMPARISON ----------------------------------------------------------

template<typename small, bool hassign, bool isnormal>
int tcomp(const small* a, index alen, const small* b, index blen) {
   caassert(a != nullptr);
   caassert(b != nullptr);
   caassert((small)-1 > 0);  // must be unsigned type

   if (!isnormal) {
      if (!hassign) {
         // only cycle down as low as other length
         if (alen > blen) while (alen > blen && a[alen-1] == 0) --alen;
         else if (blen > alen) while (blen > alen && b[blen-1] == 0) --blen;
      }
      else {
         alen = tnorm<small, hassign>(a, alen);
         blen = tnorm<small, hassign>(b, blen);
      }
   }

   const bool asf = is_negative<small, hassign>(a, alen);
   const bool bsf = is_negative<small, hassign>(b, blen);
   if (asf != bsf) return asf ? -1 : 1;  // a < 0, b >= 0; i.e. a < b

   if (alen > blen) return asf ? -1 : 1;
   if (alen < blen) return asf ? 1 : -1;
   if (alen == 0) return 0;  // both 0

   index i = alen;
   do {
      --i;
      // no need to check sign bit, since compares are always unsigned
      if (a[i] > b[i]) return 1;
      if (a[i] < b[i]) return -1;
   } while (i > 0);

   return 0;
}

#ifdef CL_TEST_CODE
static void test_bignum_comp() {
   uint16 a[] = { 0, 0, 0, 0 };
   uint16 b[] = { 0, 0, 0, 0 };
   int c;

   c = compu(a, 0, b, 0);
   classert(c == 0);

   c = compu_nn(a, 4, b, 4);
   classert(c == 0);

   a[0] = 1;
   c = compu(a, 1, b, 0);
   classert(c > 0);

   c = compu(b, 0, a, 1);
   classert(c < 0);

   c = compu_nn(a, 2, b, 3);
   classert(c > 0);

   b[0] = 1;
   c = compu(a, 1, b, 1);
   classert(c == 0);

   c = compu_nn(a, 2, b, 2);
   classert(c == 0);

   b[0] = 2;
   c = compu(a, 1, b, 1);
   classert(c < 0);

   c = compi(a, 1, b, 1);
   classert(c < 0);

   a[0] = -2;
   c = compi(a, 1, b, 1);
   classert(c < 0);

   b[0] = -1;
   c = compi(a, 1, b, 1);
   classert(c < 0);

   a[1] = -1;
   c = compi_nn(a, 2, b, 1);
   classert(c < 0);

   a[0] = -1;
   c = compi(a, 1, b, 1);
   classert(c == 0);
}
#endif


// ------ compare v with 2^k --------------------------------------------------

template<typename small>
int cmp_pow2(const small* v, index len, const bit_index k) {
   caassert(k / bits(small) < numeric_limits<index>::max());
   const index klen = k / bits(small) + 1;
   len = normu(v, len);
   if (len < klen) return -1;
   if (len > klen) return 1;

   caassert(len == klen);
   const small ptop = (small)1 << (k % bits(small));
   const small vtop = v[len-1];
   if (vtop < ptop) return -1;
   if (vtop > ptop) return 1;

   caassert(vtop == ptop);
   return is_zero(v, klen - 1) ? 0 : 1;
}

#ifdef CL_TEST_CODE
static void test_cmp_pow2() {
   uint8 v[] = { 0, 0 };
   int c = cmp_pow2(v, NBEL(v), 1);  // 0 < 2^1
   classert(c == -1);

   v[1] = 1;
   c = cmp_pow2(v, NBEL(v), 1);  // 256 > 2^1
   classert(c == 1);

   v[1] = 2;
   c = cmp_pow2(v, NBEL(v), 8);  // 512 > 2^8
   classert(c == 1);

   c = cmp_pow2(v, NBEL(v), 10);  // 512 < 2^10
   classert(c == -1);

   c = cmp_pow2(v, NBEL(v), 9);  // 512 == 2^9
   classert(c == 0);

   v[0] = 1;
   c = cmp_pow2(v, NBEL(v), 9);  // 513 > 2^9
   classert(c == 1);

   v[0] = 0;
   v[1] = 0;
   c = cmp_pow2(v, NBEL(v), 0);  // 0 < 2^1
   classert(c == -1);
}
#endif


// ------ HEX DUMP ------------------------------------------------------------

// byteorder!
static char hexdigits[] = "0123456789abcdef";

static void _dump_hex(char*& s, const byte* a, index len) {
   for (index i = len; i > 0; ) {
      const byte b = a[--i];
      *s++ = hexdigits[b >> 4];
      *s++ = hexdigits[b & 0xf];
   }
}

template<typename small>
string dump_hex(const small* a, index len) {
   len = normi(a, len);
   if (len == 0) return "0";
   if (len == 1 && a[0] < 10) return string(1, (char)('0'+a[0]));
   tape_alloc<byte> buf(sizeof(small) * len * 2 + 3);
   char* s = (char*)buf.p + 2;
   _dump_hex(s, (byte*)a, len * sizeof(small));
   *s = 0;
   char* q = (char*)buf.p + 2;
   while (*q == '0') ++q;
   q -= 2;
   q[0] = '0';
   q[1] = 'x';
   return q;
}

#ifdef CL_TEST_CODE
static void test_bignum_dump_hex() {
   uint16 a[] = { 1, 0, 0, 0 };
   string s = dump_hex(a, 1);
   classert(s == "1");

   s = dump_hex(a+1, 1);
   classert(s == "0");

   s = dump_hex(a, 4);
   classert(s == "1");

   a[1] = 0xff;
   a[2] = 0x8;
   a[3] = 0x55;
   s = dump_hex(a, 4);
   classert(s == "0x55000800ff0001");
}
#endif


#ifdef CL_TEST_CODE
void _CL_CALL test_bignum_util() {
   test_log2_star();
   test_bignum_norm();
   test_bignum_normu1();
   test_bignum_comp();
   test_bignum_neg();
   //test_bignum_tape_alloc();
   test_bignum_dump_hex();
   test_cmp_pow2();
   test_bignum_alloc_lal();
}
#endif


// ------ generate template functions -----------------------------------------

//#if _MSC_VER < 1300
#pragma inline_depth(0)

template<typename small>
void generate_bignum_utils() {
   small u;
   neg(&u, &u, 0);
   neg_on(&u, 0);
   normi(&u, 0);
   normu(&u, 0);
   compi_nn(&u, 0, &u, 0);
   compi(&u, 0, &u, 0);
   compu_nn(&u, 0, &u, 0);
   compu(&u, 0, &u, 0);
   dump_hex(&u, 0);
   cmp_pow2<small>(0, 0, 0);
   rand_fill<small>(0, 0);
   rand_alloc<small>(0);
   bignum_free<small>(0);
}

void _generate_bignum_utils() {
   classertm(0, "must not be called");
   generate_bignum_utils<uint8>();
   generate_bignum_utils<uint16>();
   generate_bignum_utils<uint32>();
#ifdef _M_X64
   generate_bignum_utils<uint64>();
#endif
}

#pragma inline_depth()
//#endif

}  // namespace bignum
