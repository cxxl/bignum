/*
 *  arbitrary length number arithmetic - limb operations on small's
 */

// $+HEADER$
// 
// Copyright 2015 Christoph Lueders
// 
// This file is part of the BIGNUM Library.
// 
// The BIGNUM Library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
// 
// The BIGNUM Library is distributed in the hope that it will be useful, but 
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public 
// License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with the BIGNUM Library.  If not, see <http://www.gnu.org/licenses/>.
// 
// $-HEADER$

// $Date: 2015-04-24 11:47:36 +0200 (Fr, 24 Apr 2015) $
// $Rev: 26592 $

#ifndef CL_BIGNUM_SMALL_H
#define CL_BIGNUM_SMALL_H 1

#include <intrin.h>

#ifndef CL_CLLIB_H
#include "cl/cllib.h"
#endif

#pragma pack(push, 8)  // arbitrary but fixed

namespace bignum {


// the big type is always double the size than the small type.  we can handle that with some templates.

template<typename T> struct make_big { /* no type */ };
template<> struct make_big<uint8> { typedef uint16 type; };
template<> struct make_big<uint16> { typedef uint32 type; };
template<> struct make_big<uint32> { typedef uint64 type; };


template<typename small>
struct base_ops {
   // basic arithmetic
   static small __forceinline addc(small& r, const small a, const small carry) {
      const make_big<small>::type l = (make_big<small>::type)a + carry;
      r = (small)l;
      const small of = l >> bits(small);
      caassert(of <= 1);
      return of;
   }
   static small __forceinline addc1(small& r, const small a) {
      return addc(r, a, 1);
   }
   // this adds 0xff..., UINT..._MAX to the number.
   // regarding overflow, that is different from subtracting 1!
   static small __forceinline addcm1(small& r, const small a) {
      return addc(r, a, -1);
   }
   static small __forceinline addc(small& r, const small a, const small b, const small carry) {
      const make_big<small>::type l = (make_big<small>::type)a + b + carry;
      r = (small)l;
      const small of = l >> bits(small);
      caassert(of <= 2);
      return of;
   }
   // this adds 0xff..., UINT..._MAX to the number.
   // regarding overflow, that is different from subtracting 1!
   static small __forceinline addcm1(small& r, const small a, const small carry) {
      return addc(r, a, -1, carry);
   }
   static small __forceinline addc(small& r, const small a, const small b, const small c, const small carry) {
      const make_big<small>::type l = (make_big<small>::type)a + b + c + carry;
      r = (small)l;
      const small of = l >> bits(small);
      caassert(of <= 3);
      return of;
   }
   static small __forceinline subc(small& r, const small a, const small borrow) {
      const make_big<small>::type l = (make_big<small>::type)a - borrow;
      r = (small)l;
      const small of = 0-(l >> bits(small));
      caassert(of <= 2);
      return of;
   }
   static small __forceinline subc(small& r, const small a, const small b, const small borrow) {
      const make_big<small>::type l = (make_big<small>::type)a - b - borrow;
      r = (small)l;
      const small of = 0-(l >> bits(small));
      caassert(of <= 2);
      return of;
   }
   // of:r = a + (b << sh1) + carry
   static small __forceinline addc_shl(small& r, const small a, const small b, const unsigned sh1, const unsigned sh2, const small carry) {
      caassert(sh1 + sh2 == bits(small));
      const make_big<small>::type l = (make_big<small>::type)a + ((make_big<small>::type)b << sh1) + carry;
      r = (small)l;
      const small of = l >> bits(small);
      return of;
   }
   static small __forceinline mul(small& r, const small a, const small b) {
      const make_big<small>::type l = (make_big<small>::type)a * b;
      r = (small)l;
      const small of = l >> bits(small);
      return of;
   }
   static small __forceinline muladdc(small& r, const small a, const small b, const small c, const small carry) {
      const make_big<small>::type l = (make_big<small>::type)a * b + c + carry;
      r = (small)l;
      const small of = l >> bits(small);
      return of;
   }
   static small __forceinline squaddonc(small* r, const small a, const small carry) {
      const make_big<small>::type p = (make_big<small>::type)a * a;
      const make_big<small>::type s = (make_big<small>::type)r[0] * 2 + carry;
      make_big<small>::type l = p + s;  // might overflow
      r[0] = (small)l;
      l = (l >> bits(small)) + ((make_big<small>::type)(l < p) << bits(small)) + (make_big<small>::type)r[1] * 2;
      r[1] = (small)l;
      return l >> bits(small);
   }
   static small __forceinline divc(small& q, const small a, const small b, const small carry) {
      const make_big<small>::type l = a + ((make_big<small>::type)carry << bits(small));
      q = l / b;
      const small r = l % b;
      return r;  // VC++ optimizes second DIV away
   }
   static small __forceinline divc(const small a, const small b, const small carry) {
      const make_big<small>::type l = a + ((make_big<small>::type)carry << bits(small));
      const small r = l % b;
      return r;
   }
   static bool __forceinline divco(small& q, small& r, const small a, const small b, const small carry) {
      const make_big<small>::type l = a + ((make_big<small>::type)carry << bits(small));
      const make_big<small>::type lq = l / b;
      q = (small)lq;
      r = l % b;
      return lq > (small)-1;  // return if overflow
   }

   // boolean operations
   static small __forceinline shl(small& r, const small a, const index count, const small carry) {
      r = (a << count) | carry;
      return a >> (bits(small) - count);
   }
   static small __forceinline shr(small& r, const small a, const index count, const small carry) {
      r = (a >> count) | carry;
      return a << (bits(small) - count);
   }
};


template<typename small>
struct ops { /* empty */ };

// specialisations:
template<> struct ops<uint32> : public base_ops<uint32> { };
template<> struct ops<uint16> : public base_ops<uint16> { };
template<> struct ops<uint8> : public base_ops<uint8> { };


#ifdef _M_X64
// real specialisation for uint64:
template<> struct ops<uint64> : public base_ops<uint64> {
   // basic arithmetic
   static uint64 __forceinline addc(uint64& r, const uint64 a, const uint64 carry) {
      const uint64 l1 = a + carry;
      const uint64 of = l1 < a;
      r = l1;
      caassert(of <= 1);
      return of;
   }
   static uint64 __forceinline addc1(uint64& r, const uint64 a) {
      const uint64 l1 = a + 1;
      const uint64 of = l1 == 0;
      r = l1;
      caassert(of <= 1);
      return of;
   }
   // this adds 0xffffffff'ffffffff, _UI64_MAX to the number.
   // regarding overflow, that is different from subtracting 1!
   static uint64 __forceinline addcm1(uint64& r, const uint64 a) {
      const uint64 l1 = a - 1;
      const uint64 of = a != 0;
      r = l1;
      caassert(of <= 1);
      return of;
   }
   static uint64 __forceinline addc(uint64& r, const uint64 a, const uint64 b, const uint64 carry) {
      const uint64 l1 = a + b;
      uint64 of = l1 < a;
      const uint64 l2 = l1 + carry;
      of += l2 < l1;
      r = l2;
      caassert(of <= 2);
      return of;
   }
   // this adds 0xffffffff'ffffffff, _UI64_MAX to the number.
   // regarding overflow, that is different from subtracting 1!
   static uint64 __forceinline addcm1(uint64& r, const uint64 a, const uint64 carry) {
      const uint64 l1 = a - 1;
      uint64 of = a != 0;
      const uint64 l2 = l1 + carry;
      of += l2 < l1;
      r = l2;
      caassert(of <= 2);
      return of;
   }
   static uint64 __forceinline addc(uint64& r, const uint64 a, const uint64 b, const uint64 c, const uint64 carry) {
      const uint64 l1 = a + b;
      uint64 of = l1 < a;
      const uint64 l2 = l1 + c;
      of += l2 < l1;
      const uint64 l3 = l2 + carry;
      of += l3 < l2;
      r = l3;
      caassert(of <= 3);
      return of;
   }
   static uint64 __forceinline subc(uint64& r, const uint64 a, const uint64 borrow) {
      const uint64 l1 = a - borrow;
      uint64 of = l1 > a;
      r = l1;
      caassert(of <= 1);
      return of;
   }
   static uint64 __forceinline subc(uint64& r, const uint64 a, const uint64 b, const uint64 borrow) {
      const uint64 l1 = a - b;
      uint64 of = l1 > a;
      const uint64 l2 = l1 - borrow;
      of += l2 > l1;
      r = l2;
      caassert(of <= 2);
      return of;
   }
   // of:r = a + (b << sh1) + carry
   static uint64 __forceinline addc_shl(uint64& r, const uint64 a, const uint64 b, const unsigned sh1, const unsigned sh2, const uint64 carry) {
      caassert(sh1 + sh2 == bits(uint64));
      caassert(sh2 < bits(uint64));
      const uint64 l1 = a + (b << sh1);
      uint64 of = b >> sh2;
      of += l1 < a;
      const uint64 l2 = l1 + carry;
      of += l2 < l1;
      r = l2;
      return of;
   }
   static uint64 __forceinline mul(uint64& r, const uint64 a, const uint64 b) {
      uint64 hiprod;
      r = _umul128(a, b, &hiprod);
      return hiprod;
   }
   static uint64 __forceinline muladdc(uint64& r, const uint64 a, const uint64 b, const uint64 c, const uint64 carry) {
      uint64 hiprod;
      const uint64 prod = _umul128(a, b, &hiprod);
      hiprod += addc(r, prod, c, carry);
      return hiprod;
   }
   static uint64 __forceinline squaddonc(uint64* r, const uint64 a, const uint64 carry) {
      uint64 hiprod;
      const uint64 prod = _umul128(a, a, &hiprod);
      uint64 of = addc(r[0], r[0], r[0], carry, prod);
      of = addc(r[1], r[1], r[1], of, hiprod);
      return of;
   }
   // divide carry:a / b; save floor quotient into q, return remainder
   static uint64 __forceinline divc(uint64& q, const uint64 a, const uint64 b, const uint64 carry) {
      uint64 r = divr_asmx64(a, carry, b, q);
      return r;
   }
   // divide carry:a / b; return just remainder, quotient not saved
   static uint64 __forceinline divc(const uint64 a, const uint64 b, const uint64 carry) {
      uint64 r = divr_asmx64(a, carry, b);
      return r;
   }
   // divide carry:a / b; return just remainder, quotient not saved
   static bool inline divco(uint64& q, uint64& r, const uint64 a, const uint64 b, const uint64 carry) {  // it will not inline, because of SEH
      __try {
         r = divr_asmx64(a, carry, b, q);
         return false;
      }
      __except(EXCEPTION_EXECUTE_HANDLER) {
         return true;
      }
   }
};
#endif


}  // namespace bignum

#pragma pack(pop)

#endif  // ndef CL_BIGNUM_SMALL_H
