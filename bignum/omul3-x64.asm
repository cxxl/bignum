; $+HEADER$
; 
; Copyright 2015 Christoph Lueders
; 
; This file is part of the BIGNUM Library.
; 
; The BIGNUM Library is free software: you can redistribute it and/or modify
; it under the terms of the GNU Lesser General Public License as published by
; the Free Software Foundation, either version 3 of the License, or (at your 
; option) any later version.
; 
; The BIGNUM Library is distributed in the hope that it will be useful, but 
; WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
; or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public 
; License for more details.
; 
; You should have received a copy of the GNU Lesser General Public License
; along with the BIGNUM Library.  If not, see <http://www.gnu.org/licenses/>.
; 
; $-HEADER$

; $Date: 2015-04-24 11:28:16 +0200 (Fr, 24 Apr 2015) $
; $Rev: 26590 $


EXTRN memset:PROC

_TEXT SEGMENT

; ------ MULTIPLICATION -------------------------------------------------------

   align 16

?omul3_asmx64@bignum@@YAXPEA_KPEB_K_K12@Z PROC   ; cl::omul3_asmx64

; 150  : void omul3_asmx64(small* r, const small* a, index alen, const small* b, index blen) {

; parameters:
; rcx = r
; rdx = a
; r8 = alen
; r9 = b
; [rsp+56] = blen (sp�ter in r13)

; rbx, rsi, rdi, rbp, r12 - r15 must be preserved by callee
; may be destroyed: rax, rcx, rdx, r8, r9, r10, r11
; http://msdn.microsoft.com/en-us/magazine/cc300794.aspx

   mov   r11, QWORD PTR [rsp+40]                ; r13 = blen

; no need to check for zero, omul_ls() does that already
;   test  r11, r11
;   jnz   SHORT not_zero
;
;   shl   r8, 3
;   xor   edx, edx
;   jmp   memset


not_zero:
   mov   QWORD PTR [rsp+8], rbx
   mov   QWORD PTR [rsp+16], r13
   mov   QWORD PTR [rsp+24], rsi
   mov   QWORD PTR [rsp+32], rdi
   push  r12
   push  r14
   push  r15
   push  rbp

; register allocation:
; --------------------
;   rax = scratch
;   rbx = a
;   rcx = r
;   rdx = scratch
;   rsi = scratch
;   rdi = bb
;   r8 = alen
;   r9 = b
;   r10 = i
;   r11 = of
;   r12 = j
;   r13 = blen
;   r14 = rm
;   r15 = of2
;   rbp = of3

   mov   r13, r11
                                                ; r9 = b
   mov   rdi, QWORD PTR [r9]                    ; rdi = bb = b[0]
   xor   r11d, r11d                             ; r11 = of

; 161  :    index i = alen % 8;

   mov   r10, r8                                ; r8 = alen
   mov   rbx, rdx                               ; rdx = a
   and   r10, 7

; 162  :    switch (i) {

   lea   rax, offset init_jump_table
   add   rax, QWORD PTR [rax+r10*8]
   jmp   rax


   align 16
init_loop:
   add   r10, 8                                 ; r10 = i

; 176  :       s(-8);

   mov   rax, QWORD PTR [rbx+r10*8-64]                               ; rax = bb = b[0]
                                                ; rbx = a
   mul   rdi               ; rdx:rax = rax * mem = b[0] * a[i]
   add   rax, r11                               ; r11 = of
   mov   QWORD PTR [rcx+r10*8-64], rax          ; rcx = r
   adc   rdx, 0
   mov   r11, rdx                               ; r11 = of

init_loop7:
; 177  : l7:   s(-7);
   mov   rax, QWORD PTR [rbx+r10*8-56]
   mul   rdi
   add   rax, r11
   mov   QWORD PTR [rcx+r10*8-56], rax
   adc   rdx, 0
   mov   r11, rdx

init_loop6:
; 178  : l6:   s(-6);
   mov   rax, QWORD PTR [rbx+r10*8-48]
   mul   rdi
   add   rax, r11
   mov   QWORD PTR [rcx+r10*8-48], rax
   adc   rdx, 0
   mov   r11, rdx

init_loop5:
; 179  : l5:   s(-5);
   mov   rax, QWORD PTR [rbx+r10*8-40]
   mul   rdi
   add   rax, r11
   mov   QWORD PTR [rcx+r10*8-40], rax
   adc   rdx, 0
   mov   r11, rdx

init_loop4:
; 180  : l4:   s(-4);
   mov   rax, QWORD PTR [rbx+r10*8-32]
   mul   rdi
   add   rax, r11
   mov   QWORD PTR [rcx+r10*8-32], rax
   adc   rdx, 0
   mov   r11, rdx

init_loop3:
; 181  : l3:   s(-3);
   mov   rax, QWORD PTR [rbx+r10*8-24]
   mul   rdi
   add   rax, r11
   mov   QWORD PTR [rcx+r10*8-24], rax
   adc   rdx, 0
   mov   r11, rdx

init_loop2:
; 182  : l2:   s(-2);
   mov   rax, QWORD PTR [rbx+r10*8-16]
   mul   rdi
   add   rax, r11
   mov   QWORD PTR [rcx+r10*8-16], rax
   adc   rdx, 0
   mov   r11, rdx

init_loop1:
; 183  : l1:   s(-1);
   mov   rax, QWORD PTR [rbx+r10*8-8]
   mul   rdi
   add   rax, r11
   mov   QWORD PTR [rcx+r10*8-8], rax
   adc   rdx, 0
   mov   r11, rdx

   cmp   r10, r8                                ; r8 = alen
   jb    init_loop


   mov   QWORD PTR [rcx+r10*8], r11             ; r11 = of
   mov   r12, 1                                 ; r12 = j
   jmp   outer_loop_cmp


; ------ outer loop
; r8 (== alen) must be > 0!

   align 16
outer_loop:

   mov   rdi, QWORD PTR [r9+r12*8]              ; rdi = bb

   dec   r8
   mov   r10, r8                                ; r8 = alen, r10 = i
   xor   r11d, r11d                             ; r11 = of
   and   r10, 7
   lea   r14, QWORD PTR [rcx+r12*8]             ; r14 = rm

   ; do first mul
   mov   rax, QWORD PTR [rbx]
   mul   rdi
   mov   rbp, rax
   mov   r15, rdx

   ; perform jump-in
   lea   rax, offset inner_jump_table
   add   rax, QWORD PTR [rax+r10*8]
   jmp   rax


inner_starter7:
   mov   r11, rbp
   mov   rbp, r15
   jmp   inner_go7

inner_starter6:
   jmp   inner_go6

inner_starter5:
   mov   r11, r15
   mov   r15, rbp
   jmp   inner_go5

inner_starter4:
   mov   r11, rbp
   mov   rbp, r15
   jmp   inner_go4

inner_starter3:
   jmp   inner_go3

inner_starter2:
   mov   r11, r15
   mov   r15, rbp
   jmp   inner_go2

inner_starter1:
   mov   r11, rbp
   mov   rbp, r15
   jmp   inner_go1


; ------ the inner loop without the jump-ins

   align 16
inner_loop:
   ;;IACA START_MARKER
   ;mov ebx, 111
   ;db 64h, 67h, 90h

   add   r10, 8                                  ; r10 = i

   mov   r11, r15
   mov   r15, rbp

   mov   rax, QWORD PTR [rbx+r10*8-56]
   mul   rdi
   mov   rsi, QWORD PTR [r14+r10*8-64]
   xor   ebp, ebp
   add   r15, rsi
   adc   r11, rax
   adc   rbp, rdx
   mov   QWORD PTR [r14+r10*8-64], r15

inner_go7:
   mov   rax, QWORD PTR [rbx+r10*8-48]
   mul   rdi
   mov   rsi, QWORD PTR [r14+r10*8-56]
   xor   r15d, r15d
   add   r11, rsi
   adc   rbp, rax
   adc   r15, rdx
   mov   QWORD PTR [r14+r10*8-56], r11

inner_go6:
   mov   rax, QWORD PTR [rbx+r10*8-40]
   mul   rdi
   mov   rsi, QWORD PTR [r14+r10*8-48]
   xor   r11d, r11d
   add   rbp, rsi
   adc   r15, rax
   adc   r11, rdx
   mov   QWORD PTR [r14+r10*8-48], rbp

inner_go5:
   mov   rax, QWORD PTR [rbx+r10*8-32]
   mul   rdi
   mov   rsi, QWORD PTR [r14+r10*8-40]
   xor   ebp, ebp
   add   r15, rsi
   adc   r11, rax
   adc   rbp, rdx
   mov   QWORD PTR [r14+r10*8-40], r15

inner_go4:
   mov   rax, QWORD PTR [rbx+r10*8-24]
   mul   rdi
   mov   rsi, QWORD PTR [r14+r10*8-32]
   xor   r15d, r15d
   add   r11, rsi
   adc   rbp, rax
   adc   r15, rdx
   mov   QWORD PTR [r14+r10*8-32], r11

inner_go3:
   mov   rax, QWORD PTR [rbx+r10*8-16]
   mul   rdi
   mov   rsi, QWORD PTR [r14+r10*8-24]
   xor   r11d, r11d
   add   rbp, rsi
   adc   r15, rax
   adc   r11, rdx
   mov   QWORD PTR [r14+r10*8-24], rbp

inner_go2:
   mov   rax, QWORD PTR [rbx+r10*8-8]
   mul   rdi
   mov   rsi, QWORD PTR [r14+r10*8-16]
   xor   ebp, ebp
   add   r15, rsi
   adc   r11, rax
   adc   rbp, rdx
   mov   QWORD PTR [r14+r10*8-16], r15

inner_go1:
   mov   rax, QWORD PTR [rbx+r10*8]
   mul   rdi
   mov   rsi, QWORD PTR [r14+r10*8-8]
   xor   r15d, r15d
   add   r11, rsi
   adc   rbp, rax
   adc   r15, rdx
   mov   QWORD PTR [r14+r10*8-8], r11

inner_cmp:
   cmp   r10, r8
   jb    inner_loop

   ;;IACA END_MARKER
   ;mov ebx, 222
   ;db 64h, 67h, 90h


; ------ we're done

   mov   rsi, QWORD PTR [r14+r10*8]
   add   rbp, rsi
   mov   QWORD PTR [r14+r10*8], rbp
   adc   r15, 0
   mov   QWORD PTR [r14+r10*8+8], r15

   inc   r8                            ; it was decremented prior to loop start
   inc   r12

outer_loop_cmp:
   cmp   r12, r13
   jb    outer_loop


outer_loop_end:
   pop   rbp
   pop   r15
   pop   r14
   pop   r12
   mov   rbx, QWORD PTR [rsp+8]
   mov   r13, QWORD PTR [rsp+16]
   mov   rsi, QWORD PTR [rsp+24]
   mov   rdi, QWORD PTR [rsp+32]

   ret   0


   align 16
init_jump_table:
   DQ init_loop - init_jump_table
   DQ init_loop1 - init_jump_table
   DQ init_loop2 - init_jump_table
   DQ init_loop3 - init_jump_table
   DQ init_loop4 - init_jump_table
   DQ init_loop5 - init_jump_table
   DQ init_loop6 - init_jump_table
   DQ init_loop7 - init_jump_table

   align 16
inner_jump_table:
   DQ inner_cmp - inner_jump_table
   DQ inner_starter1 - inner_jump_table
   DQ inner_starter2 - inner_jump_table
   DQ inner_starter3 - inner_jump_table
   DQ inner_starter4 - inner_jump_table
   DQ inner_starter5 - inner_jump_table
   DQ inner_starter6 - inner_jump_table
   DQ inner_starter7 - inner_jump_table

?omul3_asmx64@bignum@@YAXPEA_KPEB_K_K12@Z ENDP   ; cl::omul3_asmx64

_TEXT ENDS

END
