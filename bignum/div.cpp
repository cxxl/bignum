// $+HEADER$
// 
// Copyright 2015 Christoph Lueders
// 
// This file is part of the BIGNUM Library.
// 
// The BIGNUM Library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
// 
// The BIGNUM Library is distributed in the hope that it will be useful, but 
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public 
// License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with the BIGNUM Library.  If not, see <http://www.gnu.org/licenses/>.
// 
// $-HEADER$

// $Date: 2015-04-24 11:28:16 +0200 (Fr, 24 Apr 2015) $
// $Rev: 26590 $

/*
 *  arbitrary length number arithmetic - division
 */

#include "precomp.h"
#include "impl.h"
#include "small.h"
#include "cl/memzero.h"
#include "cl/int_math.h"
#include "../bignum/bignum_t.h"


// we must switch off "unreachable code" warning, since bool template parameters generate code paths that are never used
#pragma warning (disable:4702)

namespace bignum {

// ------ DIVISION -----------------------------------------------------------

// input must be non-negative and normalized
// calculate q = floor(a / b), r = a % b; a = q * b + r
// q xor r may be nullptr
// returns false for b == 0, true otherwise
// q MAY NOT overlap a or b; rr MAY overlap a or b

template<typename small>
void div_simple(small* q, index qmax, small* rr, index rmax, const small* a, index alen, const small* b, index blen) {
   caassert(q != nullptr || rr != nullptr);
   caassert(a != nullptr);
   caassert(b != nullptr);
   classert(alen == normu(a, alen));
   classert(blen == normu(b, blen));
   caassert(q == nullptr || qmax + blen >= alen);
   caassert(rr == nullptr || rmax >= blen);
   classert(compu1(b, blen, small(0)) != 0);   // b != 0
   classert(compu(a, alen, b, blen) >= 0);   // a >= b
   caassert(q != a && q != b);

   classert(bit_length(a, alen) >= bit_length(b, blen));
   bit_index s = bit_length(a, alen) - bit_length(b, blen);
   classert(q == nullptr || qmax >= (s + bits(small) - 1) / bits(small));
   index rlen = alen;
   index mlen = alen;
   tape_alloc<small> tmp(rlen + mlen);
   small* r = tmp.p;          // rest of the division, gets slowly reduced
   small* m = tmp.p + rlen;   // divisor that gets shifted around in the process

   if (q) zero(q, qmax);
   copy(r, a, alen);
   shl(m, mlen, b, blen, s);  // shift divisor so that its bit_length is the same as the dividend (copied to r)
   mlen = normu(m, mlen);
   classert(bit_length(r, rlen) == bit_length(m, mlen));

   while (true) {
      if (compu(r, rlen, m, mlen) >= 0) {  // if r > m, we can subtract m (the shifted b) from r and increase q (add a shifted 1)
         if (q) set_bit(q, qmax, s);
         subu_on(r, rlen, m, mlen);
         rlen = normu(r, rlen);
      }
      classert(compu(r, rlen, m, mlen) < 0);  // since we chose s properly, r must now be smaller than m
      if (s-- == 0) break;
      classert(!is_bit_set(m, mlen, 0));  // must be zero, since we shifted m that many bits
      shr_on(m, mlen, 1);  // shift down by 1
      mlen = normu1(m, mlen);
   }
   classert(compu(m, mlen, b, blen) == 0);  // m must be unshifted b in the end

   if (rr) {
      classert(rmax >= rlen);
      copy(rr, r, rlen);
      zero(rr, rmax, rlen);
   }

   classert(compu(r, rlen, b, blen) < 0);  // rest r must be smaller than divisor
#ifndef NDEBUG
   if (q) {
      index tlen = tmax(alen, qmax+blen);
      tape_alloc<small> tmp2(tlen);
      small* t = tmp2.p;
      mulu(t, q, qmax, b, blen);
      zero(t, tlen, qmax+blen);
      addu_on(t, tlen, r, rlen);
      classert(compu_nn(a, alen, t, tlen) == 0);
   }
#endif
}



// http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.14.5815&rep=rep1&type=pdf

// ------ Knuth DIVISION -----------------------------------------------------

// knuth, vol ii, 4.3.1, p. 272
// q = floor(u/v), r = u % v

#if 0
#define DOUT(a) DTOUT_PUTS(a)
#else
#define DOUT(a)
#endif

// q may not overlap, r may overlap

template<typename small>
void div_knuth(small* q, index qmax, small* r, index rmax, const small* uu, index ulen, const small* vv, index vlen) {
   caassert(q != nullptr || r != nullptr);
   caassert(uu != nullptr);
   caassert(vv != nullptr);
   classert(ulen == normu(uu, ulen));
   classert(vlen == normu(vv, vlen));
   caassert(q == nullptr || qmax + vlen >= ulen);
   caassert(r == nullptr || rmax >= vlen);
   classert(compu1(vv, vlen, small(0)) != 0);   // vv != 0
   classert(compu(uu, ulen, vv, vlen) >= 0);   // uu >= vv
   caassert(vlen >= 2);  // otherwise simple algo should have done it
   caassert(q != uu && q != vv);

   tape_alloc<small> tmp(ulen + 1 + vlen + vlen + 1);
   small* u = tmp.p;          // length ulen + 1
   small* t = u + ulen + 1;   // length vlen + 1
   small* v = t + vlen + 1;   // length vlen

   // step D1
   // d = 2^sh
   const index m = ulen - vlen;
   index j = m;
   const unsigned sh = bits(small) - bit_width(vv[vlen-1]);

   if (sh == 0) {
      copy(u, uu, ulen);
      u[ulen] = 0;
      v = const_cast<small*>(vv);

      // first quotient word
      if (compu_nn(u+j, vlen+1, v, vlen) >= 0) {
         if (q) q[j] = 1;
         subu_on(u+j, vlen+1, v, vlen);
      }
      else {
         if (q) q[j] = 0;
      }
      goto loop;
   }
   else {
      // build new 'normalized' values u and v
      shl(u, ulen+1, uu, ulen, sh);
      shl(v, vlen, vv, vlen, sh);
   }
   caassert(top_set(v[vlen-1]));

   do {
      // step D3
      small qhat;
      small rhat;
      DOUT((format("divco: %#x / %#x") << basic_bignum<small,false>(u+j+vlen-1, 2) << v[vlen-1]));
      if (ops<small>::divco(qhat, rhat, u[j+vlen-1], v[vlen-1], u[j+vlen])) {
         // we got overflow
         qhat = (small)-1;
         rhat = v[vlen-1];
         DOUT((format(" = overflow\n")));
      }
      else DOUT((format(" = %#x, rest %#x\n") << qhat << rhat));
      while (1) {
         small loprod;
         small hiprod = ops<small>::mul(loprod, qhat, v[vlen-2]);
         if (hiprod < rhat || hiprod == rhat && loprod <= u[j+vlen-2]) break;  // exit if qhat*v[n-2] <= rhat*b+u[j+n-2]
         --qhat;
         rhat += v[vlen-1];
         if (rhat < v[vlen-1]) break;  // exit if rhat >= b (i.e. overflow)
      }

      // step D4
      mul1_fast(t, v, vlen, qhat);  // t has length vlen+1
      DOUT((format("%#x * %#x = %#x\n%#x") << qhat << basic_bignum<small,false>(v, vlen) << basic_bignum<small,false>(t, vlen+1) << basic_bignum<small,false>(u+j, vlen+1)));
      small of = sub_fast(u+j, u+j, t, vlen+1);
      DOUT((format(" - %#x = %#x\n") << basic_bignum<small,false>(t, vlen+1) << basic_bignum<small,false>(u+j, vlen+1)));

      // step D5
      if (!of) {
         if (q) q[j] = qhat;
      }
      else {
         caassert(qhat > 0);
         // step D6
         if (q) q[j] = qhat - 1;
         addu_on(u+j, vlen+1, v, vlen);
      }
loop:
      caassert(u[j+vlen] == 0);  // we cleared that word
   } while (j-- > 0);

   // step D8
   if (r) shr(r, rmax, u, vlen, sh);
   if (q) zero(q, qmax, m+1);

#ifndef NDEBUG
   if (q && r) {
      classert(compu_nn(r, rmax, vv, vlen) < 0);
      index tlen = tmax(ulen, qmax+vlen);
      tape_alloc<small> tmp2(tlen);
      small* t = tmp2.p;
      mulu(t, q, qmax, vv, vlen);
      zero(t, tlen, qmax+vlen);
      addu_on(t, tlen, r, normu(r, rmax));
      classert(compu_nn(uu, ulen, t, tlen) == 0);
   }
#endif
}

#ifdef CL_TEST_CODE
static void test_knuth_div() {
#if 1
   {
      uint32 a[8];
      uint32 b[4];
      uint32 q[8];
      uint32 r[4];
      uint32 qc[8];
      uint32 rc[4];

      // div 2 by 2 words, no normalize needed
      memzero(a); a[0] = 0x1234; a[1] = 0xa3450000;
      memzero(b); b[0] = 0x89ab; b[1] = 0x82340000;
      memzero(qc); qc[0] = 0x1;
      memzero(rc); rc[0] = 0xffff8889; rc[1] = 0x2110ffff;
      div_knuth(q, NBEL(q), r, NBEL(r), a, normu(a, NBEL(a)), b, normu(b, NBEL(b)));
      classert(compu_nn(q, NBEL(q), qc, NBEL(qc)) == 0);
      classert(compu_nn(r, NBEL(r), rc, NBEL(rc)) == 0);

      // div 2 by 2 words, normalize needed
      memzero(a); a[0] = 0x1234; a[1] = 0x23450000;
      memzero(b); b[0] = 0x89ab; b[1] = 0x12340000;
      memzero(qc); qc[0] = 0x1;
      memzero(rc); rc[0] = 0xffff8889; rc[1] = 0x1110ffff;
      div_knuth(q, NBEL(q), r, NBEL(r), a, normu(a, NBEL(a)), b, normu(b, NBEL(b)));
      classert(compu_nn(q, NBEL(q), qc, NBEL(qc)) == 0);
      classert(compu_nn(r, NBEL(r), rc, NBEL(rc)) == 0);

      // div 2 by 2 words, max unbalanced
      memzero(a); a[0] = 0x1234; a[1] = 0xffffffff;
      memzero(b); b[0] = 0x89ab; b[1] = 0x80000000;
      memzero(qc); qc[0] = 0x1;
      memzero(rc); rc[0] = 0xffff8889; rc[1] = 0x7ffffffe;
      div_knuth(q, NBEL(q), r, NBEL(r), a, normu(a, NBEL(a)), b, normu(b, NBEL(b)));
      classert(compu_nn(q, NBEL(q), qc, NBEL(qc)) == 0);
      classert(compu_nn(r, NBEL(r), rc, NBEL(rc)) == 0);

      // div 6 by 3 words
      memzero(a); a[0] = 0x0234; a[1] = 0x1234; a[2] = 0x2234; a[3] = 0x3234; a[4] = 0x4234; a[5] = 0xfabc4512;
      memzero(b); b[0] = 0x89ab; b[1] = 0x55aa; b[2] = 0x12340000;
      memzero(qc); qc[0] = 0x77e69e33; qc[1] = 0xbdd6977b; qc[2] = 0xc638f8db; qc[3] = 0xd;
      memzero(rc); rc[0] = 0x7e4b0b23; rc[1] = 0x28ebd2b2; rc[2] = 0x119bb807;
      div_knuth(q, NBEL(q), r, NBEL(r), a, normu(a, NBEL(a)), b, normu(b, NBEL(b)));
      classert(compu_nn(q, NBEL(q), qc, NBEL(qc)) == 0);
      classert(compu_nn(r, NBEL(r), rc, NBEL(rc)) == 0);

      // div 6 by 3 words: qhat*v[n-2] <= rhat*b+u[j+n-2] case
      memzero(a); a[0] = 0xa773d9f1; a[1] = 0xfb9ac4fa; a[2] = 0x3e8e6749; a[3] = 0x388acffb; a[4] = 0x71a64ff9; a[5] = 0x609c47bb;
      memzero(b); b[0] = 0x66f27446; b[1] = 0x693465e5; b[2] = 0xc8c31dec;
      memzero(qc); qc[0] = 0xb399957e; qc[1] = 0x7c57fc32; qc[2] = 0x7b312270;
      memzero(rc); rc[0] = 0x6e9be17d; rc[1] = 0x35d26d46; rc[2] = 0xadf940a6;
      div_knuth(q, NBEL(q), r, NBEL(r), a, normu(a, NBEL(a)), b, normu(b, NBEL(b)));
      classert(compu_nn(q, NBEL(q), qc, NBEL(qc)) == 0);
      classert(compu_nn(r, NBEL(r), rc, NBEL(rc)) == 0);
   }
   {
      // div 6 by 3 bytes, overflow case
      uint8 a[8];
      uint8 b[4];
      uint8 q[8];
      uint8 r[4];
      uint8 qc[8];
      uint8 rc[4];
      memzero(a); a[0] = 0xad; a[1] = 0x57; a[2] = 0xb2; a[3] = 0xac; a[4] = 0x92; a[5] = 1;
      memzero(b); b[0] = 0xf2; b[1] = 0x23;
      memzero(qc); qc[0] = 0xfe; qc[1] = 0xd1; qc[2] = 0x33; qc[3] = 0xb;
      memzero(rc); rc[0] = 0x91; rc[1] = 0x1b;
      div_knuth(q, NBEL(q), r, NBEL(r), a, normu(a, NBEL(a)), b, normu(b, NBEL(b)));
      classert(compu_nn(q, NBEL(q), qc, NBEL(qc)) == 0);
      classert(compu_nn(r, NBEL(r), rc, NBEL(rc)) == 0);

      // div 6 by 3 bytes, add back case
      memzero(a); a[0] = 0xfb; a[1] = 0xd0; a[2] = 0x7d; a[3] = 0xb3; a[4] = 0x8c; a[5] = 0x05;
      memzero(b); b[0] = 0xd7; b[1] = 0x34; b[2] = 0x02;
      memzero(qc); qc[0] = 0xff; qc[1] = 0xe5; qc[2] = 0x83; qc[3] = 0x02;
      memzero(rc); rc[0] = 0xd2; rc[1] = 0xdb; rc[2] = 0x01;
      div_knuth(q, NBEL(q), r, NBEL(r), a, normu(a, NBEL(a)), b, normu(b, NBEL(b)));
      classert(compu_nn(q, NBEL(q), qc, NBEL(qc)) == 0);
      classert(compu_nn(r, NBEL(r), rc, NBEL(rc)) == 0);
   }
#if 0
   {
      uint8 a[8];
      uint8 b[4];
      uint8 q[8];
      uint8 r[4];
      while (1) {
         // div 6 by 3 words
         memzero(a); rand_fill(a, 6); a[5] &= ((uint8)1 << (make_rand.get<unsigned>() % bits(uint8))) - 1;
         memzero(b); rand_fill(b, 3); b[2] &= ((uint8)1 << (make_rand.get<unsigned>() % bits(uint8))) - 1;
         div_knuth(q, NBEL(q), r, NBEL(r), a, normu(a, NBEL(a)), b, normu(b, NBEL(b)));
      }
   }
#endif
#endif
#ifdef _M_X64
#if 1
   {
      uint64 a[8];
      uint64 b[4];
      uint64 q[8];
      uint64 r[4];
      uint64 qc[8];
      uint64 rc[4];

      // div 6 by 3 words
      memzero(a); a[0] = 0x0234; a[1] = 0x1234; a[2] = 0x2234; a[3] = 0x3234; a[4] = 0x4234; a[5] = 0xfabc451266667777;
      memzero(b); b[0] = 0x89ab; b[1] = 0x55aa; b[2] = 0x1234000098769876;
      memzero(qc); qc[0] = 0x443f69cdee3f56e3; qc[1] = 0xdcb3dfaffa604e69; qc[2] = 0xc638f86dffbaa700; qc[3] = 0xd;
      memzero(rc); rc[0] = 0x7a051ef1f6317d93; rc[1] = 0xf0e6c59af9cc4a9f; rc[2] = 0x0fbcc082cb8b4451;
      div_knuth(q, NBEL(q), r, NBEL(r), a, normu(a, NBEL(a)), b, normu(b, NBEL(b)));
      classert(compu_nn(q, NBEL(q), qc, NBEL(qc)) == 0);
      classert(compu_nn(r, NBEL(r), rc, NBEL(rc)) == 0);
   }
#endif
#if 0
   {
      uint64 qhat, rhat;
      bool of = ops<uint64>::divco(qhat, rhat, 0, 2, 3);
      classert(of);

      uint64 a[8];
      uint64 b[4];
      uint64 q[8];
      uint64 r[4];
      while (1) {
         // div 6 by 3 words
         memzero(a); rand_fill(a, 6); a[5] &= ((uint64)1 << (make_rand.get<unsigned>() % bits(uint64))) - 1;
         memzero(b); rand_fill(b, 3); b[2] &= ((uint64)1 << (make_rand.get<unsigned>() % bits(uint64))) - 1;
         div_knuth(q, NBEL(q), r, NBEL(r), a, normu(a, NBEL(a)), b, normu(b, NBEL(b)));
      }
   }
#endif
#endif
}
#endif



// ------ DIVISION MAIN ROUTINE ----------------------------------------------

// 'r' buffer must have length 'blen'
// q MAY NOT overlap a or b; rr MAY overlap a or b

template<typename small, bool hassign>
bool tdiv(small* q, index qmax, small* r, index rmax, const small* a, index alen, const small* b, index blen) {
   caassert(q != nullptr || r != nullptr);   // at least one must be non-nullptr
   caassert(a != nullptr);
   caassert(b != nullptr);
   classert(q == nullptr || qmax + (tnorm<small, hassign>(b, blen)) >= (tnorm<small, hassign>(a, alen)));  // q must be big enough
   classert(r == nullptr || rmax >= (tnorm<small, hassign>(b, blen)));  // r must be big enough
   caassert(q != a && q != b);

   const bool asf = is_negative<small, hassign>(a, alen);
   if (asf) neg_on(const_cast<small*>(a), alen);
   alen = normu(a, alen);
   const bool bsf = is_negative<small, hassign>(b, blen);
   if (bsf) neg_on(const_cast<small*>(b), blen);
   blen = normu(b, blen);
   bool ret = true;

   // handle special cases
   if (blen <= 1) {  // max 1 small
      if (blen == 0) {  // b == 0, zero divide
         ret = false;  // zero divide
         goto error_case;
      }
      else {  // exactly 1 small
         const small rr = div1(q, qmax, a, alen, b[0]);  // r may overlap
         if (r) {
            if (rmax > 0) r[0] = rr;
            zero(r, rmax, 1);
         }
      }
   }
   else {
      if (compu(a, alen, b, blen) < 0) {  // a < b, q = 0, r = a
error_case:
         if (q) zero(q, qmax);
         if (r) {
            copy(r, a, alen);
            zero(r, rmax, alen);
         }
      }
      else div_knuth(q, qmax, r, rmax, a, alen, b, blen);  // q may not overlap, r may overlap
   }

   if (asf) neg_on(const_cast<small*>(a), alen);
   if (bsf) neg_on(const_cast<small*>(b), blen);
   if ((unsigned)asf ^ (unsigned)bsf) neg_on(q, qmax);

   return ret;
}


#ifdef CL_TEST_CODE
static void test_div() {
   uint32 a[8];
   uint32 b[4];
   uint32 q[8];
   uint32 r[4];
   uint32 qc[8];
   uint32 rc[4];

   memzero(a); a[0] = 0x1234;
   memzero(b); b[0] = 0x89;
   memzero(qc); qc[0] = 0x22;
   memzero(rc); rc[0] = 0x2;
   bool ret = tdiv<uint32, false>(q, NBEL(q), r, NBEL(r), a, NBEL(a), b, NBEL(b));
   classert(ret);
   classert(compu_nn(q, NBEL(q), qc, NBEL(qc)) == 0);
   classert(compu_nn(r, NBEL(r), rc, NBEL(rc)) == 0);

   // test a % b == 0
   memzero(a); a[0] = 0x1234;
   memzero(b); b[0] = 20;
   memzero(qc); qc[0] = 233;
   memzero(rc);
   ret = tdiv<uint32, false>(q, NBEL(q), r, NBEL(r), a, NBEL(a), b, NBEL(b));
   classert(ret);
   classert(compu_nn(q, NBEL(q), qc, NBEL(qc)) == 0);
   classert(compu_nn(r, NBEL(r), rc, NBEL(rc)) == 0);

   // test a < b
   memzero(a); a[0] = 0x1233;
   memzero(b); b[0] = 0x1234;
   memzero(qc);
   memzero(rc); rc[0] = 0x1233;
   ret = tdiv<uint32, false>(q, NBEL(q), r, NBEL(r), a, NBEL(a), b, NBEL(b));
   classert(ret);
   classert(compu_nn(q, NBEL(q), qc, NBEL(qc)) == 0);
   classert(compu_nn(r, NBEL(r), rc, NBEL(rc)) == 0);

   // test bit_length(a) == bit_length(b)
   memzero(a); a[0] = 0x1234;
   memzero(b); b[0] = 0x1233;
   memzero(qc); qc[0] = 1;
   memzero(rc); rc[0] = 1;
   ret = tdiv<uint32, false>(q, NBEL(q), r, NBEL(r), a, NBEL(a), b, NBEL(b));
   classert(ret);
   classert(compu_nn(q, NBEL(q), qc, NBEL(qc)) == 0);
   classert(compu_nn(r, NBEL(r), rc, NBEL(rc)) == 0);

   // test div by zero
   memzero(a); a[0] = 0x1234;
   memzero(b);
   memzero(qc);
   memzero(rc); rc[0] = 0x1234;
   ret = tdiv<uint32, false>(q, NBEL(q), r, NBEL(r), a, NBEL(a), b, NBEL(b));
   classert(!ret);
   classert(compu_nn(q, NBEL(q), qc, NBEL(qc)) == 0);
   classert(compu_nn(r, NBEL(r), rc, NBEL(rc)) == 0);

   // test a < b, long
   memzero(a);
   a[1] = 0x1233;
   a[0] = 0;
   memzero(b);
   b[1] = 0x1234;
   b[0] = 0;
   memzero(qc);
   memzero(rc);
   rc[1] = 0x1233;
   rc[0] = 0;
   ret = tdiv<uint32, false>(q, NBEL(q), r, NBEL(r), a, NBEL(a), b, NBEL(b));
   classert(ret);
   classert(compu_nn(q, NBEL(q), qc, NBEL(qc)) == 0);
   classert(compu_nn(r, NBEL(r), rc, NBEL(rc)) == 0);

   // test bit_length(a) == bit_length(b), long
   memzero(a);
   a[1] = 0x1234;
   a[0] = 0x1234;
   memzero(b);
   b[1] = 0x1234;
   b[0] = 0x1233;
   memzero(qc);
   qc[0] = 1;
   memzero(rc);
   rc[0] = 1;
   ret = tdiv<uint32, false>(q, NBEL(q), r, NBEL(r), a, NBEL(a), b, NBEL(b));
   classert(ret);
   classert(compu_nn(q, NBEL(q), qc, NBEL(qc)) == 0);
   classert(compu_nn(r, NBEL(r), rc, NBEL(rc)) == 0);

   // test long dividend
   // a = 0x72f89a0160af7130f477df7d8e2411f274b683f98bfc5d9df594b086e4aca5bb = 52002905716636912718128038185571884816037536909212244845351431723385474557371
   // b = 0x298742d1 = 696730321
   // qc = 0x2c4bc24457d6ef11df31fcb8057a637acdda625fe4159830f17731bd6 = 74638499501497815132589928127402230304309573616521628093965831183318
   // rc = 0x1eabc005 = 514572293
   memzero(a);
   a[7] = 0x72f89a01;
   a[6] = 0x60af7130;
   a[5] = 0xf477df7d;
   a[4] = 0x8e2411f2;
   a[3] = 0x74b683f9;
   a[2] = 0x8bfc5d9d;
   a[1] = 0xf594b086;
   a[0] = 0xe4aca5bb;
   memzero(b);
   b[0] = 0x298742d1;
   memzero(qc);
   qc[7] = 0x2;
   qc[6] = 0xc4bc2445;
   qc[5] = 0x7d6ef11d;
   qc[4] = 0xf31fcb80;
   qc[3] = 0x57a637ac;
   qc[2] = 0xdda625fe;
   qc[1] = 0x4159830f;
   qc[0] = 0x17731bd6;
   memzero(rc);
   rc[0] = 0x1eabc005;
   ret = tdiv<uint32, false>(q, NBEL(q), r, NBEL(r), a, NBEL(a), b, NBEL(b));
   classert(ret);
   classert(compu_nn(q, NBEL(q), qc, NBEL(qc)) == 0);
   classert(compu_nn(r, NBEL(r), rc, NBEL(rc)) == 0);

   // test long dividend and long divisor
   // a = 0x72f89a0160af7130f477df7d8e2411f274b683f98bfc5d9df594b086e4aca5bb = 52002905716636912718128038185571884816037536909212244845351431723385474557371
   // b = 0x2d1345bf3549abc223405986abc0f111 = 59915328085966836406601336076034437393
   // qc = 0x28cf726789f8ffa1e4fea64c1beec4d6b = 867939930864149866137306180495579303275
   // rc = 0x16655713d24a70bceff7d8fa38d9c6a0 = 29769204030032617224174075255927195296
   memzero(a);
   a[7] = 0x72f89a01;
   a[6] = 0x60af7130;
   a[5] = 0xf477df7d;
   a[4] = 0x8e2411f2;
   a[3] = 0x74b683f9;
   a[2] = 0x8bfc5d9d;
   a[1] = 0xf594b086;
   a[0] = 0xe4aca5bb;
   memzero(b);
   b[3] = 0x2d1345bf;
   b[2] = 0x3549abc2;
   b[1] = 0x23405986;
   b[0] = 0xabc0f111;
   memzero(qc);
   qc[4] = 0x2;
   qc[3] = 0x8cf72678;
   qc[2] = 0x9f8ffa1e;
   qc[1] = 0x4fea64c1;
   qc[0] = 0xbeec4d6b;
   memzero(rc);
   rc[3] = 0x16655713;
   rc[2] = 0xd24a70bc;
   rc[1] = 0xeff7d8fa;
   rc[0] = 0x38d9c6a0;
   ret = tdiv<uint32, false>(q, NBEL(q), r, NBEL(r), a, NBEL(a), b, NBEL(b));
   classert(ret);
   classert(compu_nn(q, NBEL(q), qc, NBEL(qc)) == 0);
   classert(compu_nn(r, NBEL(r), rc, NBEL(rc)) == 0);
}
#endif  // def CL_TEST_CODE


// ------ DIVISION by constant small ------------------------------------------

// division of a const input number by a small constant
// length can be zero, numbers don't need to be normalised

template<typename small, small d>
void tdiv_small(small* q, const small* a, index alen) {
   caassert(q != nullptr);
   caassert(a != nullptr);
   caassert(d > 0);

   //tout_puts("div=" + dump_hex(a, alen) + "\n");
   small of = 0;
   index i = alen;
   while (i > 0) {
      --i;
      of = ops<small>::divc(q[i], a[i], d, of);
   }
}

template<typename small>
void div_by3(small* q, const small* a, index alen) {
   tdiv_small<small, 3>(q, a, alen);
}

#ifdef CL_TEST_CODE
static void test_div_by3() {
   uint16 a[] = { 0, 0, 0 };

   a[0] = 999;
   a[1] = 666;
   a[2] = 333;
   div_by3(a, a, 3);
   classert(a[0] == 333);
   classert(a[1] == 222);
   classert(a[2] == 111);

   a[0] = 998;
   a[1] = 666;
   a[2] = 334;
   div_by3(a, a, 3);
   classert(a[0] == 22178);
   classert(a[1] == 22067);
   classert(a[2] == 111);
}
#endif


// ------ DIVISION by 1 small ------------------------------------------------

// division of a const input number by a small
// length can be zero, numbers don't need to be normalised

template<typename small>
small div1(small* q, const small* a, index alen, small b) {
   caassert(q != nullptr);
   caassert(a != nullptr);
   caassert(b > 0);

   small of = 0;
   index i = alen;
   while (i > 0) {
      --i;
      of = ops<small>::divc(q[i], a[i], b, of);
   }
   return of;
}

template<typename small>
small div1(small* q, index qmax, const small* a, index alen, small b) {
   caassert(q == nullptr || qmax + 1 >= alen);
   caassert(a != nullptr);
   caassert(b > 0);

   small of = 0;
   index i = alen;
   if (q == nullptr) {
      while (i > 0) {
         --i;
         of = ops<small>::divc(a[i], b, of);
      }
   }
   else {
      if (qmax < alen) {
         caassert(i > 0);
         --i;
         of = ops<small>::divc(a[i], b, of);
      }
      while (i > 0) {
         --i;
         of = ops<small>::divc(q[i], a[i], b, of);
      }
      zero(q, qmax, alen);  // zero remaining upper words
   }
   return of;
}

#ifdef CL_TEST_CODE
static void test_div1() {
   {
      uint16 a[] = { 0, 0, 0 };
      uint16 q[NBEL(a)];

      a[2] = 0x1111;
      a[1] = 0x2222;
      a[0] = 0x3333;
      uint16 r = div1<uint16>(q, a, NBEL(a), 0xcafe);
      // 0x111122223333 / 0xcafe = 0x15860e19, rest 0x7c65
      classert(q[2] == 0);
      classert(q[1] == 0x1586);
      classert(q[0] == 0x0e19);
      classert(r == 0x7c65);
   }

#ifdef _M_X64
   {
      uint64 a[] = { 0, 0, 0 };
      uint64 q[NBEL(a)];

      a[2] = 0x1111111111111111;
      a[1] = 0x2222222222222222;
      a[0] = 0x3333333333333333;
      uint64 r = div1<uint64>(q, a, NBEL(a), 0xc001babe00caffee);
      // 0x111111111111111122222222222222223333333333333333 / 0xc001babe00caffee = 0x16c1379e1a165ca2366b618e4bec8a41, rest 0x2aa70464e849ebc5
      classert(q[2] == 0);
      classert(q[1] == 0x16c1379e1a165ca2);
      classert(q[0] == 0x366b618e4bec8a41);
      classert(r == 0x2aa70464e849ebc5);
   }
#endif
}
#endif


// ------ second attempt

/*
// this looks good: http://www.daniweb.com/software-development/cpp/threads/62148
// and this inspired my code: http://embeddedgurus.com/stack-overflow/category/efficient-cc/page/2/
static uint64 __forceinline divby3(const uint64 a) {
   const uint64 hiprod = _mulh(a, 0xaaaaaaaaaaaaaaab);
   return hiprod >> 1;
}
*/

template<typename small>
void div_by3_2(small* r, const small* a, index alen) {
   caassert(r != 0);
   caassert(a != 0);

   small rm = 0;
   index i = alen;
   static const small hi_const[] = { 0, (small)-1/3, (small)-1/3*2 };
   while (i > 0) {
      --i;
      small q = a[i] / 3;
      const small newr = a[i] - q*3;
      classert(rm < 3);
      q += hi_const[rm];
      rm += newr;
      const small is = rm >= 3;
      rm -= 3 * is;
      q += is;
      r[i] = q;
   }
}

#ifdef CL_TEST_CODE
static void test_small_div2() {
   uint16 a[] = { 0, 0, 0 };

   a[0] = 999;
   a[1] = 666;
   a[2] = 333;
   div_by3_2(a, a, 3);
   classert(a[0] == 333);
   classert(a[1] == 222);
   classert(a[2] == 111);

   a[0] = 998;
   a[1] = 666;
   a[2] = 334;
   div_by3_2(a, a, 3);
   classert(a[0] == 22178);
   classert(a[1] == 22067);
   classert(a[2] == 111);
}
#endif


template<typename small>
void div_by3_3(small* r, const small* a, index alen) {
   caassert(r != 0);
   caassert(a != 0);

   small rm = 0;
   index i = alen;
   const small hic = (small)-1/3;
   static const small q_add[] = {
      0, hic,   2*hic,   0,   // newr == 0
      0, hic,   2*hic+1, 0,   // newr == 1
      0, hic+1, 2*hic+1, 0,   // newr == 2
   };
   static const small rm_add[] = {
      0, 1,     2,       0,   // newr == 0
      1, 2,     0,       0,   // newr == 1
      2, 0,     1,       0,   // newr == 2
   };
   while (i > 0) {
      --i;
      small q = a[i] / 3;
      const small newr = a[i] - q*3;
      const unsigned idx = rm + newr * 4;
      classert(idx < NBEL(q_add));
      classert(idx % 4 != 3);
      q += q_add[idx];
      rm = rm_add[idx];
      classert(rm < 3);
      r[i] = q;
   }
}

#ifdef CL_TEST_CODE
static void test_small_div3() {
   uint16 a[] = { 0, 0, 0 };

   a[0] = 999;
   a[1] = 666;
   a[2] = 333;
   div_by3_3(a, a, 3);
   classert(a[0] == 333);
   classert(a[1] == 222);
   classert(a[2] == 111);

   a[0] = 998;
   a[1] = 666;
   a[2] = 334;
   div_by3_3(a, a, 3);
   classert(a[0] == 22178);
   classert(a[1] == 22067);
   classert(a[2] == 111);
}
#endif


template<typename small>
dword selftest_div() {
   const index nb = 2048 / bits(small);
   small a[nb];
   small a2[nb];
   small r[nb];
   zero(a, nb);
   a[0] = 3;

   index alen;
   unsigned pow = 1;
   const unsigned maxp = 100;
   while ((alen = normu(a, nb)) < nb / 2 && pow < maxp) {
      mulu(r, a, alen, a, alen);
      zero(r, nb, 2*alen);
      copy(a, r, nb);
      pow *= 2;
   }

   alen = normu(a, nb);
   copy(a2, a, alen);
   for (unsigned i=0; i<pow; ++i) {
      div_by3(a, a, alen);
      div_by3_2(a2, a2, alen);
      classert(compu(a, alen, a2, alen) == 0);
      alen = normu(a, alen);
   }

   classert(alen == 1);
   classert(a[0] == 1);
   return alen == 1 && a[0] == 1 ? 0 : 1;
}


#ifdef CL_TEST_CODE
static void test_div_asm() {
#ifdef _M_X64
   {
      uint64 r = divr_asmx64_64by32_rev(4294967291, 4294967291ui64 * 4294967279 + 4711);
      classert(r == 4711);
   }
#endif
}

void _CL_CALL test_bignum_div() {
   test_div_asm();
   test_div_by3();
   test_small_div2();
   test_small_div3();
   test_div1();
   test_knuth_div();
   test_div();

   selftest_div<uint8>();
   selftest_div<uint16>();
   selftest_div<uint32>();
#ifdef _M_X64
   selftest_div<uint64>();
#endif
}
#endif


template<typename small>
small div_by3_asm(small& r, const small a, const small carry) {
   const small b = 3;
   const make_big<small>::type l = a + ((make_big<small>::type)carry << bits(small));
   r = l / b;
   return l - r*b;
}


// ------ generate template functions -----------------------------------------

#pragma inline_depth(0)

template<typename small>
void generate_bignum_divs() {
   small u;
   div_by3(&u, &u, 0);
   div_by3_2(&u, &u, 0);
   div_by3_3(&u, &u, 0);
   div1<small>(&u, &u, 0, 1);
   div1<small>(&u, 0, &u, 0, 1);
   div_simple<small>(&u, 0, &u, 0, &u, 0, &u, 0);
   div_knuth<small>(&u, 0, &u, 0, &u, 0, &u, 0);
   divu<small>(nullptr, 0, nullptr, 0, nullptr, 0);
   modu<small>(nullptr, 0, nullptr, 0, nullptr, 0);
   tdiv<small, true>(nullptr, 0, nullptr, 0, nullptr, 0, nullptr, 0);
   selftest_div<small>();
}

void _generate_bignum_divs() {
   classertm(0, "must not be called");
   generate_bignum_divs<uint8>();
   generate_bignum_divs<uint16>();
   generate_bignum_divs<uint32>();
#ifdef _M_X64
   generate_bignum_divs<uint64>();
#endif
}

#pragma inline_depth()

}  // namespace bignum
