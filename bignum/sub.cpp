// $+HEADER$
// 
// Copyright 2015 Christoph Lueders
// 
// This file is part of the BIGNUM Library.
// 
// The BIGNUM Library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
// 
// The BIGNUM Library is distributed in the hope that it will be useful, but 
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public 
// License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with the BIGNUM Library.  If not, see <http://www.gnu.org/licenses/>.
// 
// $-HEADER$

// $Date: 2015-04-24 11:28:16 +0200 (Fr, 24 Apr 2015) $
// $Rev: 26590 $

/*
 *  arbitrary length number arithmetic - subtraction
 */

#include "precomp.h"
#include "impl.h"
#include "small.h"
#include "cl/int_math.h"
#include "cl/format.h"


// we must switch off "unreachable code" warning, since bool template parameters generate code paths that are never used
#pragma warning (disable:4702)

namespace bignum {


// ------ SUBTRACTION --------------------------------------------------------

// mother of all subs: slow, but simple
template<typename small, bool swap, bool hassign>
small sub_simple(small* r, index rmax, const small* a, index alen, const small* b, index blen) {
   caassert(r != nullptr);
   caassert(a != nullptr);
   caassert(b != nullptr);
   caassert(rmax >= alen);
   caassert((small)-1 > 0);  // must be unsigned type

   small of = 0;
   index i = 0;

   const bool asf = is_negative<small, hassign>(a, alen);
   const bool bsf = is_negative<small, hassign>(b, blen);
   while (i < rmax) {
      const small aa = i < alen ? a[i] : asf ? (small)-1 : 0;
      const small bb = i < blen ? b[i] : bsf ? (small)-1 : 0;
      of = !swap ? ops<small>::subc(r[i++], aa, bb, of) : ops<small>::subc(r[i++], bb, aa, of);
   }

   return of;
}


// subtraction of two const input numbers 'a' and 'b' into a pre-reserved result 'r'
// swap == false: 'b' is subtracted from 'a': a - b
// swap == true: 'a' is subtracted from 'b': b - a
// 'a' must be longer than or equal the size as 'b'
// the result will be padded up to 'rmax' and must be long enough to hold the result
// length can be zero, numbers don't need to be normalised

template<typename small, bool swap, bool hassign>
small tsub_ls(small* r, index rmax, const small* a, index alen, const small* b, index blen) {
   caassert(r != 0);
   caassert(a != 0);
   caassert(b != 0);
   caassert(alen >= blen);
   caassert(rmax >= alen);
   caassert((small)-1 > 0);  // must be unsigned type

   small of = 0;
   index i = 0;

   // we must get the signs now, since they may be overwritten by a subtraction in progress later
   const bool asf = is_negative<small, hassign>(a, alen);
   const bool bsf = is_negative<small, hassign>(b, blen);

#ifdef BN_SUB_USE_FAST
   // use the loop-unrolled or asm version
   of = !swap ? sub_fast(r, a, b, blen) : sub_fast(r, b, a, blen);
   i = blen;
#else
   // subtract the length where we have two values
   while (i < blen) {
      of = !swap ? ops<small>::subc(r[i], a[i], b[i], of) : ops<small>::subc(r[i], b[i], a[i], of);
      ++i;
   }
#endif
   caassert(of <= 1);

   // continue to work on 'a' only while propagating overflow and honouring 'b's sign
   caassert(i == blen);
   if (bsf) {
      while (i < alen) {
         of = !swap ? ops<small>::subc(r[i], a[i], -1, of) : ops<small>::subc(r[i], -1, a[i], of);
         ++i;
      }
   }
   else {
      if (!swap) {
         while (i < alen && of) {
            of = ops<small>::subc(r[i], a[i], 1);
            ++i;
         }
         // if we have no overflow, we can just copy
         while (i < alen)
            r[i++] = a[i];
      }
      else {
         while (i < alen && !of) {
            of = ops<small>::subc(r[i], 0, a[i]);
            ++i;
         }
         // if we have overflow once, it'll continue forever
         while (i < alen) {
            caassert(of);
            r[i++] = 0 - a[i] - 1;
         }
      }
   }

   // propagate overflow and honour signs
   if (i < rmax) {
      if (!swap) {
         if (asf == bsf) {
            if (of) one_in(r, rmax, i);
            else zero_in(r, rmax, i);
         }
         else {
            while (i < rmax) {
               const small aa = asf ? (small)-1 : 0;
               const small bb = bsf ? (small)-1 : 0;
               of = ops<small>::subc(r[i++], aa, bb, of);
            }
         }
      }
      else {
         if (asf == bsf) {
            if (of) one_in(r, rmax, i);
            else zero_in(r, rmax, i);
         }
         else {
            while (i < rmax) {
               const small aa = asf ? (small)-1 : 0;
               const small bb = bsf ? (small)-1 : 0;
               of = ops<small>::subc(r[i++], bb, aa, of);
            }
         }
      }
   }

   return of;
}


// manually unrolled version of the first sub loop on small's.
// returns the carry.
// length can be zero, numbers don't need to be normalised

template<typename small>
small __fastcall sub_unrolled(small* r, const small* a, const small* b, const index len) {
   caassert(r != 0);
   caassert(a != 0);
   caassert(b != 0);

   small of = 0;
   index i = 0;

#define s(o) of = ops<small>::subc(r[i+(o)], a[i+(o)], b[i+(o)], of)
   UNROLL8(i, len, 0, l, s);
#undef s

   return of;
}


// subtraction of one number in-place (destructively) from another
// if swap == false: a = a - b
// if swap == true:  a = b - a

template<typename small, bool swap, bool hassign>
small tsub_on(small* a, index alen, const small* b, index blen) {
   caassert(a != 0);
   caassert(b != 0);
   caassert(alen >= blen);
   caassert((small)-1 > 0);  // must be unsigned type

   small of = 0;
   index i = 0;

   /*
   // slow, but simple version:
   const bool bsf = is_negative<small, hassign>(b, blen);
   while (i < alen) {
      const small bb = i < blen ? b[i] : bsf ? (small)-1 : 0;
      of = !swap ? ops<small>::subc(a[i], a[i], bb, of) : ops<small>::subc(a[i], bb, a[i], of);
      ++i;
   }
   */

#ifdef BN_SUB_USE_FAST
   // use the loop-unrolled or asm version
   // improve: use sub_on_fast
   of = !swap ? sub_fast(a, a, b, blen) : sub_fast(a, b, a, blen);
   i = blen;
#else
   while (i < blen) {
      of = !swap ? ops<small>::subc(a[i], a[i], b[i], of) : ops<small>::subc(a[i], b[i], a[i], of);
      ++i;
   }
#endif

   const bool bsf = is_negative<small, hassign>(b, blen);
   if (bsf) {
      caassert(hassign);
      while (i < alen) {
         of = !swap ? ops<small>::subc(a[i], a[i], -1, of) : ops<small>::subc(a[i], -1, a[i], of);
         ++i;
      }
   }
   else {
      if (!swap) {
         while (of && i < alen) {
            of = ops<small>::subc(a[i], a[i], 1);
            ++i;
         }
      }
      else {
         while (i < alen && !of) {
            of = ops<small>::subc(a[i], 0, a[i]);
            ++i;
         }
         // once of is set, it will always be set
         while (i < alen) {
            caassert(of);
            a[i++] = 0 - a[i] - 1;
         }
      }
   }

   return of;
}

#ifdef CL_TEST_CODE
static void test_sub_on() {
   {
      uint16 a[] = { 2 };
      uint16 b[] = { 1 };
      uint16 r = subu_on(a, NBEL(a), b, NBEL(b));
      classert(r == 0);
   }
   {
      uint16 a[] = { 1 };
      uint16 b[] = { 2 };
      uint16 r = subu_on(a, NBEL(a), b, NBEL(b));
      classert(r == 1);
   }
   {
      uint16 a[] = { 1, 0 };
      uint16 b[] = { 2 };
      uint16 r = subu_on(a, NBEL(a), b, NBEL(b));
      classert(r == 1);
   }
}
#endif


// ------ ADD/SUB -------------------------------------------------------------


#ifdef CL_TEST_CODE
static void test_addsub() {
   const uint16 a[] = { 1, 2, 3, 0 };  // a > b
   const uint16 b[] = { 2, 3, 1, 0 };
   uint16 r1[4], r2[4];
   bool sign;

   // a + b
   addu(r1, 4, a, 4, b, 4);
   sign = add_or_subu(r2, 4, a, 4, false, b, 4, false);
   classert(sign == false);
   classert(compu_nn(r1, 4, r2, 4) == 0);
   classert(r2[3] == 0);  // no overflow

   // -a - b
   sign = add_or_subu(r2, 4, a, 4, true, b, 4, true);
   classert(sign == true);
   classert(compu_nn(r1, 4, r2, 4) == 0);
   classert(r2[3] == 0);  // no overflow

   // -b - a
   sign = add_or_subu(r2, 4, b, 4, true, a, 4, true);
   classert(sign == true);
   classert(compu_nn(r1, 4, r2, 4) == 0);
   classert(r2[3] == 0);  // no overflow

   // a - b
   subu(r1, 4, a, 4, b, 4);
   sign = add_or_subu(r2, 4, a, 4, false, b, 4, true);
   classert(sign == false);
   classert(compu_nn(r1, 4, r2, 4) == 0);
   classert(r2[3] == 0);  // no overflow

   // b - a
   subu(r1, 4, b, 4, a, 4);
   neg_on(r1, 4);
   sign = add_or_subu(r2, 4, b, 4, false, a, 4, true);
   classert(sign == true);
   classert(compu_nn(r1, 4, r2, 4) == 0);
   classert(r2[3] == 0);  // no overflow

   // -a + b  =  b - a
   subu(r1, 4, b, 4, a, 4);
   neg_on(r1, 4);
   sign = add_or_subu(r2, 4, a, 4, true, b, 4, false);
   classert(sign == true);
   classert(compu_nn(r1, 4, r2, 4) == 0);
   classert(r2[3] == 0);  // no overflow

   // -b + a  =  a - b
   subu(r1, 4, a, 4, b, 4);
   sign = add_or_subu(r2, 4, b, 4, true, a, 4, false);
   classert(sign == false);
   classert(compu_nn(r1, 4, r2, 4) == 0);
   classert(r2[3] == 0);  // no overflow
}

static void test_addsub_on() {
   uint16 a[] = { 1, 2, 3, 0 };
   uint16 b[] = { 2, 3, 1, 0 };
   uint16 r1[4], r2[4];
   bool sign;

   // a + b
   copy(r1, a, 4);
   addu_on(r1, 4, b, 4);
   copy(r2, a, 4);
   sign = add_or_subu_on(r2, 4, false, b, 4, false);
   classert(sign == false);
   classert(compu_nn(r1, 4, r2, 4) == 0);

   // -a - b
   copy(r1, a, 4);
   neg_on(r1, 4);
   subu_on(r1, 4, b, 4);
   neg_on(r1, 4);
   copy(r2, a, 4);
   sign = add_or_subu_on(r2, 4, true, b, 4, true);
   classert(sign == true);
   classert(compu_nn(r1, 4, r2, 4) == 0);

   // -b - a
   copy(r1, b, 4);
   neg_on(r1, 4);
   subu_on(r1, 4, a, 4);
   neg_on(r1, 4);
   copy(r2, b, 4);
   sign = add_or_subu_on(r2, 4, true, a, 4, true);
   classert(sign == true);
   classert(compu_nn(r1, 4, r2, 4) == 0);

   // a - b
   copy(r1, a, 4);
   subu_on(r1, 4, b, 4);
   copy(r2, a, 4);
   sign = add_or_subu_on(r2, 4, false, b, 4, true);
   classert(sign == false);
   classert(compu_nn(r1, 4, r2, 4) == 0);

   // b - a
   copy(r1, b, 4);
   subu_on(r1, 4, a, 4);
   neg_on(r1, 4);
   copy(r2, b, 4);
   sign = add_or_subu_on(r2, 4, false, a, 4, true);
   classert(sign == true);
   classert(compu_nn(r1, 4, r2, 4) == 0);

   // -a + b
   copy(r1, b, 4);
   subu_on(r1, 4, a, 4);
   neg_on(r1, 4);
   copy(r2, a, 4);
   sign = add_or_subu_on(r2, 4, true, b, 4, false);
   classert(sign == true);
   classert(compu_nn(r1, 4, r2, 4) == 0);

   // -b + a
   copy(r1, b, 4);
   neg_on(r1, 4);
   addu_on(r1, 4, a, 4);
   copy(r2, b, 4);
   sign = add_or_subu_on(r2, 4, true, a, 4, false);
   classert(sign == false);
   classert(compu_nn(r1, 4, r2, 4) == 0);
}
#endif


#ifdef CL_TEST_CODE
static void test_sub1(const int64 a, const int64 b, int rsign) {
   // use 16 bits, since it's easier to test
   uint16 aa[] = { 0, 0, 0, 0 };
   uint16 bb[] = { 0, 0, 0, 0 };
   uint16 rr[] = { 1, 2, 3, 4, 5 };

   const int64 r = a - b;
   classert(a > -(int64)0xffffffff && a < 0xffffffff);
   classert(b > -(int64)0xffffffff && b < 0xffffffff);
   classert(r > -(int64)0xffffffff && r < 0xffffffff);
   classert(rsign > 0 ? r > 0 : rsign < 0 ? r < 0 : r == 0);

   *(int64*)aa = a;
   *(int64*)bb = b;

   tsub_ls<uint16, false, true>(rr, 4, aa, 2, bb, 2);  // test with same size
   int64 rx = *(int64*)rr;
   classert(rx == r);
   classert(rr[4] == 5);  // untouched

   tsub_ls<uint16, false, true>(rr, 4, aa, 3, bb, 2);  // test with a longer than b
   rx = *(int64*)rr;
   classert(rx == r);
   classert(rr[4] == 5);  // untouched

   tsub_ls<uint16, true, true>(rr, 4, aa, 2, bb, 2);  // test with same size
   rx = *(int64*)rr;
   classert(rx == -r);
   classert(rr[4] == 5);  // untouched

   tsub_ls<uint16, true, true>(rr, 4, aa, 3, bb, 2);  // test with a longer than b
   rx = *(int64*)rr;
   classert(rx == -r);
   classert(rr[4] == 5);  // untouched
}

template<typename t1, typename t2>
static dword test_sub_fib2(index bytes) {
   dword ret = 0;
   const index l1 = bytes / sizeof(t1);
   t1* a1 = bignum_alloc<t1>(l1);
   zero(a1, l1);
   a1[0] = 1;
   t1* b1 = bignum_alloc<t1>(l1);
   zero(b1, l1);
   t1* r1 = bignum_alloc<t1>(l1);
   t1* s1 = bignum_alloc<t1>(l1);

   const index l2 = bytes / sizeof(t2);
   t2* a2 = bignum_alloc<t2>(l2);
   zero(a2, l2);
   a2[0] = 1;
   t2* b2 = bignum_alloc<t2>(l2);
   zero(b2, l2);
   t2* r2 = bignum_alloc<t2>(l2);
   t2* s2 = bignum_alloc<t2>(l2);

   int rounds = 0;
   do {
      // add up fibonacci numbers
      classert(memcmp(a1, a2, bytes) == 0);
      classert(memcmp(b1, b2, bytes) == 0);
      addu(r1, l1, a1, l1, b1, l1);  // r = a + b
      addu(r2, l2, a2, l2, b2, l2);
      classert(memcmp(r1, r2, bytes) == 0);
      // do some subtractions
      subu(s1, l1, r1, l1, a1, l1);  // s = r - a
      subu(s2, l2, r2, l2, a2, normu(a2, l2));
      classert(memcmp(s1, s2, bytes) == 0);
      classert(memcmp(s1, b1, bytes) == 0);
      classert(memcmp(s2, b2, bytes) == 0);
      // r = a + b
      memcpy(b1, a1, bytes);  // b = a
      memcpy(b2, a2, bytes);
      memcpy(a1, r1, bytes);  // a = r
      memcpy(a2, r2, bytes);
      // another subtraction
      subu(r1, l1, s1, l1, b1, l1);
      subu(r2, l2, s2, l2, b2, l2);
      classert(memcmp(r1, r2, bytes) == 0);
      ++rounds;
   } while (a1[l1-1] == 0 && a2[l2-1] == 0);
   classert(rounds >= bytes * 8);

   bignum_free(a1);
   bignum_free(b1);
   bignum_free(r1);
   bignum_free(s1);
   bignum_free(a2);
   bignum_free(b2);
   bignum_free(r2);
   bignum_free(s2);

   return ret;
}

template<typename t1, typename t2>
static dword test_sub_on_fib(index bytes) {
   dword ret = 0;
   const index l1 = bytes / sizeof(t1);
   t1* a1 = bignum_alloc<t1>(l1);
   zero(a1, l1);
   a1[0] = 1;
   t1* b1 = bignum_alloc<t1>(l1);
   zero(b1, l1);
   t1* r1 = bignum_alloc<t1>(l1);
   t1* s1 = bignum_alloc<t1>(l1);

   const index l2 = bytes / sizeof(t2);
   t2* a2 = bignum_alloc<t2>(l2);
   zero(a2, l2);
   a2[0] = 1;
   t2* b2 = bignum_alloc<t2>(l2);
   zero(b2, l2);
   t2* r2 = bignum_alloc<t2>(l2);
   t2* s2 = bignum_alloc<t2>(l2);

   int rounds = 0;
   do {
      // add up fibonacci numbers
      classert(memcmp(a1, a2, bytes) == 0);
      classert(memcmp(b1, b2, bytes) == 0);
      addu(r1, l1, a1, l1, b1, l1);  // r = a + b
      addu(r2, l2, a2, l2, b2, l2);
      classert(memcmp(r1, r2, bytes) == 0);
      // do some subtractions
      memcpy(s1, r1, bytes);
      memcpy(s2, r2, bytes);
      subu_on(s1, l1, a1, l1);  // s = r - a
      subu_on(s2, l2, a2, normu(a2, l2));
      classert(memcmp(s1, b1, bytes) == 0);
      classert(memcmp(s2, b2, bytes) == 0);
      // do some more subtractions
      memcpy(s1, r1, bytes);
      memcpy(s2, r2, bytes);
      subu_on_neg(s1, l1, a1, l1);  // s = a - r
      subu_on_neg(s2, l2, a2, normu(a2, l2));
      neg_on(s1, l1);
      neg_on(s2, l2);
      classert(memcmp(s1, b1, bytes) == 0);
      classert(memcmp(s2, b2, bytes) == 0);
      // r = a + b
      memcpy(b1, a1, bytes);  // b = a
      memcpy(b2, a2, bytes);
      memcpy(a1, r1, bytes);  // a = r
      memcpy(a2, r2, bytes);
      // another subtraction
      subu(r1, l1, s1, l1, b1, l1);
      subu(r2, l2, s2, l2, b2, l2);
      classert(memcmp(r1, r2, bytes) == 0);
      ++rounds;
   } while (a1[l1-1] == 0 && a2[l2-1] == 0);
   classert(rounds >= bytes * 8);

   bignum_free(a1);
   bignum_free(b1);
   bignum_free(r1);
   bignum_free(s1);
   bignum_free(a2);
   bignum_free(b2);
   bignum_free(r2);
   bignum_free(s2);

   return ret;
}

#if defined _M_X86 && defined BN_SUB_USE_ASMX86
static void test_sub_asmx86() {
   uint32 a[40];
   uint32 b[40];
   uint32 r1[41];
   uint32 r2[41];
   a[0] = 0x1f1f1f1f;
   for (unsigned i=1; i<NBEL(a)-1; ++i) a[i] = 0x22222200 + i;
   b[0] = 0x2f2f2f2f;
   for (unsigned i=1; i<NBEL(b)-1; ++i) b[i] = 0x11111100 + i;

   // a full 8-unrolled round
   memset(r1, 0xcc, sizeof(r1));
   memset(r2, 0xcc, sizeof(r2));
   uint32 c1 = sub_unrolled(r1+1, a+1, b+1, 16);
   uint32 c2 = sub_asmx86(r2+1, a+1, b+1, 16);
   classert(c1 == c2);
   classert(memcmp(r1, r2, sizeof(r1)) == 0);

   // just part of it
   memset(r1, 0xcc, sizeof(r1));
   memset(r2, 0xcc, sizeof(r2));
   c1 = sub_unrolled(r1+1, a+1, b+1, 12);
   c2 = sub_asmx86(r2+1, a+1, b+1, 12);
   classert(c1 == c2);
   classert(memcmp(r1, r2, sizeof(r1)) == 0);

   // one word uneven
   memset(r1, 0xcc, sizeof(r1));
   memset(r2, 0xcc, sizeof(r2));
   c1 = sub_unrolled(r1+1, a+1, b+1, 11);
   c2 = sub_asmx86(r2+1, a+1, b+1, 11);
   classert(c1 == c2);
   classert(memcmp(r1, r2, sizeof(r1)) == 0);

   // two full 8-unrolled rounds
   memset(r1, 0xcc, sizeof(r1));
   memset(r2, 0xcc, sizeof(r2));
   c1 = sub_unrolled(r1+1, a+1, b+1, 32);
   c2 = sub_asmx86(r2+1, a+1, b+1, 32);
   classert(c1 == c2);
   classert(memcmp(r1, r2, sizeof(r1)) == 0);

   // two full and some 8-unrolled rounds
   memset(r1, 0xcc, sizeof(r1));
   memset(r2, 0xcc, sizeof(r2));
   c1 = sub_unrolled(r1+1, a+1, b+1, 36);
   c2 = sub_asmx86(r2+1, a+1, b+1, 36);
   classert(c1 == c2);
   classert(memcmp(r1, r2, sizeof(r1)) == 0);
}
#endif

#if defined _M_X64 && defined BN_SUB_USE_ASMX64
static void test_sub_asmx64(uint64 (*sub_func)(uint64* r, const uint64* a, const uint64* b, const uint64 len)) {
   uint64 a[40];
   uint64 b[40];
   uint64 r1[41];
   uint64 r2[41];
   a[0] = 0x1f1f1f1f1f1f1f1f;
   for (unsigned i=1; i<NBEL(a)-1; ++i) a[i] = 0x2222222222222200 + i;
   b[0] = 0x2f2f2f2f2f2f2f2f;
   for (unsigned i=1; i<NBEL(b)-1; ++i) b[i] = 0x1111111111111100 + i;

   // a full 8-unrolled round
   memset(r1, 0xcc, sizeof(r1));
   memset(r2, 0xcc, sizeof(r2));
   uint64 c1 = sub_unrolled(r1+1, a+1, b+1, 16);
   uint64 c2 = sub_func(r2+1, a+1, b+1, 16);
   classert(c1 == c2);
   classert(memcmp(r1, r2, sizeof(r1)) == 0);

   // just part of it
   memset(r1, 0xcc, sizeof(r1));
   memset(r2, 0xcc, sizeof(r2));
   c1 = sub_unrolled(r1+1, a+1, b+1, 12);
   c2 = sub_func(r2+1, a+1, b+1, 12);
   classert(c1 == c2);
   classert(memcmp(r1, r2, sizeof(r1)) == 0);

   // one word uneven
   memset(r1, 0xcc, sizeof(r1));
   memset(r2, 0xcc, sizeof(r2));
   c1 = sub_unrolled(r1+1, a+1, b+1, 11);
   c2 = sub_func(r2+1, a+1, b+1, 11);
   classert(c1 == c2);
   classert(memcmp(r1, r2, sizeof(r1)) == 0);

   // two full 8-unrolled rounds
   memset(r1, 0xcc, sizeof(r1));
   memset(r2, 0xcc, sizeof(r2));
   c1 = sub_unrolled(r1+1, a+1, b+1, 32);
   c2 = sub_func(r2+1, a+1, b+1, 32);
   classert(c1 == c2);
   classert(memcmp(r1, r2, sizeof(r1)) == 0);

   // two full and some 8-unrolled rounds
   memset(r1, 0xcc, sizeof(r1));
   memset(r2, 0xcc, sizeof(r2));
   c1 = sub_unrolled(r1+1, a+1, b+1, 36);
   c2 = sub_func(r2+1, a+1, b+1, 36);
   classert(c1 == c2);
   classert(memcmp(r1, r2, sizeof(r1)) == 0);
}
#endif


template<typename small>
static void test_ops_sub() {
   small r;
   small of = ops<small>::subc(r, 0, 0, 0);
   classert(r == 0);
   classert(of == 0);

   of = ops<small>::subc(r, 14, 2, 0);
   classert(r == 12);
   classert(of == 0);

   of = ops<small>::subc(r, 14, 0, 1);
   classert(r == 13);
   classert(of == 0);

   of = ops<small>::subc(r, 0, 1, 0);
   classert(r == small(-1));
   classert(of == 1);

   of = ops<small>::subc(r, 0, 0, 1);
   classert(r == small(-1));
   classert(of == 1);

   of = ops<small>::subc(r, 0, -1, 0);
   classert(r == 1);
   classert(of == 1);

   of = ops<small>::subc(r, 0, -1, 1);
   classert(r == 0);
   classert(of == 1);
}

static void test_sub2() {
   // use 16 bits, since it's easier to test
   uint16 b[] = { 0, 0, 0 };
   uint16 a[] = { 0, 0, 0 };
   uint16 r[] = { 1, 2, 3, 4, 5, 6, 7 };

   // 0 - 0 == 0
   tsub_ls<uint16, false, true>(r, 2, a, 1, b, 1);
   classert(r[0] == 0);
   classert(r[1] == 0);
   classert(r[2] == 3);  // untouched

   // large vmax: 3 - 1 == 2
   a[0] = 3;
   b[0] = 1;
   tsub_ls<uint16, false, true>(r, 3, a, 1, b, 1);
   classert(r[0] == 2);
   classert(r[1] == 0);
   classert(r[2] == 0);
   classert(r[3] == 4);  // untouched

   // a longer than b: 0x10009 - 7 == 0x10002
   a[0] = 9;
   a[1] = 1;
   b[0] = 7;
   tsub_ls<uint16, false, true>(r, 3, a, 2, b, 1);
   classert(r[0] == 2);
   classert(r[1] == 1);
   classert(r[2] == 0);
   classert(r[3] == 4);  // untouched

   // underflow: 0x10007 - 9 == 0xfffe
   a[0] = 7;
   a[1] = 1;
   b[0] = 9;
   tsub_ls<uint16, false, true>(r, 3, a, 2, b, 1);
   classert(r[0] == 0xfffe);
   classert(r[1] == 0);
   classert(r[2] == 0);
   classert(r[3] == 4);  // untouched

   // sign flip: 9 - 0x10007 == -0x10002
   a[0] = 7;
   a[1] = 1;
   b[0] = 9;
   tsub_ls<uint16, true, true>(r, 3, a, 2, b, 1);
   classert(r[0] == (uint16)2);
   classert(r[1] == (uint16)-1);
   classert(r[2] == (uint16)-1);
   classert(r[3] == 4);  // untouched


   // ------ no overflow ------
   // --- result > 0 ---
   test_sub1(10, 7, 1);       // a > 0, b > 0
   test_sub1(10, -7, 1);      // a > 0, b < 0
   // a < 0, b > 0: doesn't exist
   test_sub1(-3, -7, 1);      // a < 0, b < 0

   // --- result < 0 ---
   test_sub1(3, 7, -1);        // a > 0, b > 0
   // a > 0, b < 0: doesn't exist
   test_sub1(-10, 7, -1);      // a < 0, b > 0
   test_sub1(-7, -4, -1);      // a < 0, b < 0

   // ------ overflow ------
   // --- result > 0
   test_sub1(0x10002, 0x8001, 1);         // a > 0, b > 0
   test_sub1(0x8001, -(int64)0x8002, 1);  // a > 0, b < 0
   // a < 0, b > 0: doesn't exist
   test_sub1(-5, -10, 1);                 // a < 0, b < 0

   // --- result < 0
   test_sub1(0x8001, 0x8002, -1);          // a > 0, b > 0
   // a > 0, b < 0: doesn't exist
   test_sub1(-(int64)0x8002, 0x8001, -1);  // a < 0, b > 0
   test_sub1(-(int64)0x10002, -5, -1);     // a < 0, b < 0
}
#endif


template<typename small, bool hassign>
static dword selftest_sub1(int64 p, int64 q) {
   small a[sizeof(uint64) / sizeof(small) + 1];
   small b[sizeof(uint64) / sizeof(small) + 1];
   small r[sizeof(uint64) / sizeof(small) + 1];
   // trash contents
   memset((void*)a, 0xcc, sizeof(a));
   memset((void*)b, 0xcc, sizeof(b));
   memset((void*)r, 0xcc, sizeof(r));
   *(int64*)a = p;
   *(int64*)b = q;
   const unsigned pbits = tmax<unsigned>((p < 0 ? bit_width(-p) : bit_width(p)) + (hassign ? 1 : 0), 1);
   const unsigned qbits = tmax<unsigned>((q < 0 ? bit_width(-q) : bit_width(q)) + (hassign ? 1 : 0), 1);
   const index rmax = (pbits + bits(small)) / bits(small);
   const index alen = (pbits + bits(small) - 1) / bits(small);
   const index blen = (qbits + bits(small) - 1) / bits(small);

   tsub_ls<small, false, hassign>(r, rmax, a, alen, b, blen);
   if (*(byte*)&r[rmax] != 0xcc) {
      classertm(0, "data after trashed");
      return 1;  // data after trashed
   }
   const uint64 mask = (1ui64 << (rmax * bits(small))) - 1;
   int64 rr = *(int64*)r;
   if (mask != 0) {
      rr &= mask;  // mask out unused parts
      if (hassign) {
         const uint64 sf = 1ui64 << (rmax * bits(small) - 1);
         if (rr & sf)  // signed result?
            rr |= ~mask;  // extend sign
      }
   }
   const int64 sum = p - q;
   if (rr != sum) {
      classertm(0, "result does not match");
      return 2;  // result mismatch
   }
   return 0;  // all cool
}

template<typename small>
dword selftest_sub() {
   const int64 max = (0ui64 - 1) >> 2;
   classert(max > 0);  // must be signed
   // fibonacci series
   int64 p = 0, q = 1;
   unsigned round = 0;
   while (q < max) {
      dword r;
      if ((r = selftest_sub1<small, true>(p, -p)) != 0)
         return round * 0x100 + r;
      if ((r = selftest_sub1<small, true>(q, p)) != 0)
         return round * 0x100 + 0x10 + r;
      if ((r = selftest_sub1<small, true>(q, -p)) != 0)
         return round * 0x100 + 0x20 + r;
      if ((r = selftest_sub1<small, true>(-q, p)) != 0)
         return round * 0x100 + 0x30 + r;
      if ((r = selftest_sub1<small, true>(-q, -p)) != 0)
         return round * 0x100 + 0x40 + r;
      if ((r = selftest_sub1<small, false>(q, p)) != 0)
         return round * 0x100 + 0x50 + r;
      // iterate
      const int64 t = p + q;
      p = q;
      q = t;
      ++round;
   }
   return 0;
}


#ifdef CL_TEST_CODE
// ------ sub test matrix -----------------------------------------------------

struct sub_tester {
   bool sign;
   byte a;
   byte b;
   byte r0;
   byte r1;
   byte of;
   bool need_ex;
};

static const sub_tester mm[] = {
   // unsigned tests, easy
   { false,   2,   1,    1,  0, 0, false },
   { false,   5,   5,    0,  0, 0, false },
   // signed tests, not so easy
   { true,    5,   5,    0,  0, 0, false },     // + + 0
   { true,   -5,  -5,    0,  0, 0, false },     // - - 0
   { true,    2,   1,    1,  0, 0, false },     // + + +
   { true,    1,   2,   -1, -1, 1, false },     // + + -
   { true,    2,  -1,    3,  0, 1, false },     // + - +
   { true,   -2,   1,   -3, -1, 0, false },     // - + -
   { true,   -2,  -1,   -1, -1, 1, false },     // - - -
   { true,   -2,  -3,    1,  0, 0, false },     // - - +
   { true, -128,  -1, -127, -1, 1, false },     // - - +
};

struct sub_tester_length {
   index alen;
   index blen;
   index rlen;
};

static const sub_tester_length ll[] = {
   { 1, 1, 2 },
   { 2, 1, 3 },
   { 1, 2, 3 },
   { 2, 2, 3 },
   { 3, 1, 4 },
   { 1, 3, 4 },

   { 1, 1, 1 },
   { 2, 1, 2 },
   { 1, 2, 2 },
   { 2, 2, 2 },
   { 3, 1, 3 },
   { 1, 3, 3 },
};

static void test_sub_matrix() {
   typedef int64 cint;
   const unsigned maxlen = sizeof(cint);
   byte of;
   byte a[maxlen];
   byte b[maxlen];
   byte r[maxlen];
   byte cr[maxlen];
   string s;
   strformat ss(s);
   bool do_print = false;

   for (unsigned j=0; j<NBEL(ll); ++j) {
      index alen = ll[j].alen;
      index blen = ll[j].blen;
      index rlen = ll[j].rlen;
      classert(alen <= maxlen);
      classert(blen <= maxlen);
      classert(rlen <= maxlen);
      if (do_print) DTOUT_PUTS(format("alen=%? blen=%? rlen=%?\n") << alen << blen << rlen);

      for (unsigned i=0; i<NBEL(mm); ++i) {
         const sub_tester& m = mm[i];
         //auto sub = m.sign ? subi<byte> : subu<byte>;
         auto sub = m.sign ? sub_simple<byte, false, true> : sub_simple<byte, false, false>;  // mother of all subs
         auto subon = m.sign ? subi_on<byte> : subu_on<byte>;
         auto comp = m.sign ? compi_nn<byte> : compu_nn<byte>;
         auto signx = m.sign ? tsignx<byte, true> : tsignx<byte, false>;
         bool asign, bsign, crsign, rsign;

         for (unsigned k=0; k<2; ++k) {
            s.clear();
            a[0] = m.a;
            signx(a, 1, maxlen);
            asign = m.sign ? is_negative<byte, true>(a, maxlen) : false;
            classert(asign == (*(cint*)&a < 0));
            b[0] = m.b;
            signx(b, 1, maxlen);
            bsign = m.sign ? is_negative<byte, true>(b, maxlen) : false;
            classert(bsign == (*(cint*)&b < 0));
            cr[0] = m.r0;
            cr[1] = m.r1;
            signx(cr, 2, maxlen);
            crsign = m.sign ? is_negative<byte, true>(cr, maxlen) : false;
            classert(crsign == (*(cint*)&cr < 0));
            char* sign_char = m.sign ? "d" : "u";
            string fmt = format("%?signed %%%? %? %%%?") <<  (m.sign ? "" : "un") << sign_char << (k == 0 ? "-" : "-=") << sign_char;
            string op = format(fmt.c_str()) << *(cint*)&a << *(cint*)&b;
            fmt = format("%?:\n  correct    %%%?, of=%%?\n") << op << sign_char;
            ss(fmt.c_str()) << *(cint*)&cr << m.of;
            if (do_print) DTOUT_PUTS(s);
            s.clear();
            if (op == "signed -2 - -1")
               NOP();
            if (k == 0) {
               // subtraction: r = a - b
               of = sub(r, rlen, a, alen, b, blen);
            }
            else {
               // subtraction: r = a, r -= b
               memcpy(r, a, sizeof(r));
               of = subon(r, rlen, b, blen);
            }
            rsign = m.sign ? is_negative<byte, true>(r, rlen) : false;
            if (!m.need_ex) {
               signx(r, rlen, maxlen);
               ss(m.sign ? "  short   => %d, of=%?\n" :  "  short   => %u, of=%?\n") << *(cint*)&r << of;
               if (do_print) DTOUT_PUTS(s);
               s.clear();
               classert(comp(r, maxlen, cr, maxlen) == 0);
               classert(of == m.of);
               classert((asign == bsign) == (rsign == (of != 0)));  // that's the rule
               classert(need_signx(asign, bsign, of, rsign) == false);
            }
            if (rlen < maxlen) {
               // calculate proper high word from inputs and 'of' return
               bool need_sx = need_signx(asign, bsign, of, rsign);
               signx(r, rlen, maxlen);
               cint old_r = *(cint*)&r;
               r[rlen] = sub_of_word(asign, bsign, of);
               rsign = m.sign ? is_negative<byte, true>(r, rlen+1) : false;
               signx(r, rlen+1, maxlen);
               ss(m.sign ? "  of_word => %d, of=%?\n" :  "  of_word => %u, of=%?\n") << *(cint*)&r << of;
               if (do_print) DTOUT_PUTS(s);
               s.clear();
               classert(comp(r, maxlen, cr, maxlen) == 0);
               classert(of == m.of);
               classert((asign == bsign) == (rsign == (of != 0)));  // that's the rule
               classert(need_signx(asign, bsign, of, rsign) == false);
               if (need_sx) classert(*(cint*)&r != old_r);  // make sure need_signx works
            }
         }
      }
      if (do_print) DTOUT_PUTS("\n");
   }
}


void _CL_CALL test_bignum_sub() {
   test_ops_sub<uint8>();
   test_ops_sub<uint16>();
   test_ops_sub<uint32>();

#ifdef _M_X64
   test_ops_sub<uint64>();
#endif
#if defined _M_X86 && defined BN_SUB_USE_ASMX86
   test_sub_asmx86();
#endif
#if defined _M_X64 && defined BN_SUB_USE_ASMX64
   test_sub_asmx64(sub8_asmx64);
   test_sub_asmx64(sub4_asmx64);
   test_sub_asmx64(sub2_asmx64);
#endif

   test_sub2();
   test_sub_on();

   // double fibonacci test
   dword rr = test_sub_fib2<uint16, uint32>(1024);
   classert(rr == 0);
   rr = test_sub_on_fib<uint16, uint32>(1024);
   classert(rr == 0);
#ifdef _M_X64
   rr = test_sub_fib2<uint32, uint64>(1024);
   classert(rr == 0);
   rr = test_sub_on_fib<uint32, uint64>(1024);
   classert(rr == 0);
#endif

   test_addsub();
   test_addsub_on();
   test_sub_matrix();

   selftest_sub<uint8>();
   selftest_sub<uint16>();
   selftest_sub<uint32>();
#ifdef _M_X64
   selftest_sub<uint64>();
#endif
}
#endif


//#if _MSC_VER < 1300
#pragma inline_depth(0)
   void _pull_bignum_sub_into_existance() {
      classertm(0, "must not be called");
      bool bol = false;
      {
         uint8 u;
         subi(&u, 0, &u, 0, &u, 0);
         subu(&u, 0, &u, 0, &u, 0);
         sub_unrolled(&u, &u, &u, 0);
         sub_fast(&u, &u, &u, 0);
         subi_on(&u, 0, &u, 0);
         subu_on(&u, 0, &u, 0);
         subi_on_neg(&u, 0, &u, 0);
         subu_on_neg(&u, 0, &u, 0);
         add_or_subu(&u, 0, &u, 0, true, &u, 0, true);
         add_or_subu_on(&u, 0, bol, &u, 0, true);
         selftest_sub<uint8>();
      }
      {
         uint16 u;
         subi(&u, 0, &u, 0, &u, 0);
         subu(&u, 0, &u, 0, &u, 0);
         sub_unrolled(&u, &u, &u, 0);
         sub_fast(&u, &u, &u, 0);
         subi_on(&u, 0, &u, 0);
         subu_on(&u, 0, &u, 0);
         subi_on_neg(&u, 0, &u, 0);
         subu_on_neg(&u, 0, &u, 0);
         add_or_subu(&u, 0, &u, 0, true, &u, 0, true);
         add_or_subu_on(&u, 0, bol, &u, 0, true);
         selftest_sub<uint16>();
      }
      {
         uint32 u;
         subi(&u, 0, &u, 0, &u, 0);
         subu(&u, 0, &u, 0, &u, 0);
         sub_unrolled(&u, &u, &u, 0);
         sub_fast(&u, &u, &u, 0);
         subi_on(&u, 0, &u, 0);
         subu_on(&u, 0, &u, 0);
         subi_on_neg(&u, 0, &u, 0);
         subu_on_neg(&u, 0, &u, 0);
         add_or_subu(&u, 0, &u, 0, true, &u, 0, true);
         add_or_subu_on(&u, 0, bol, &u, 0, true);
         selftest_sub<uint32>();
      }
#ifdef _M_X64
      {
         uint64 u;
         subi(&u, 0, &u, 0, &u, 0);
         subu(&u, 0, &u, 0, &u, 0);
         sub_unrolled(&u, &u, &u, 0);
         sub_fast(&u, &u, &u, 0);
         subi_on(&u, 0, &u, 0);
         subu_on(&u, 0, &u, 0);
         subi_on_neg(&u, 0, &u, 0);
         subu_on_neg(&u, 0, &u, 0);
         add_or_subu(&u, 0, &u, 0, true, &u, 0, true);
         add_or_subu_on(&u, 0, bol, &u, 0, true);
         selftest_sub<uint64>();
      }
#endif
   }
#pragma inline_depth()
//#endif

}  // namespace bignum
