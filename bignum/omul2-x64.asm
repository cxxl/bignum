; $+HEADER$
; 
; Copyright 2015 Christoph Lueders
; 
; This file is part of the BIGNUM Library.
; 
; The BIGNUM Library is free software: you can redistribute it and/or modify
; it under the terms of the GNU Lesser General Public License as published by
; the Free Software Foundation, either version 3 of the License, or (at your 
; option) any later version.
; 
; The BIGNUM Library is distributed in the hope that it will be useful, but 
; WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
; or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public 
; License for more details.
; 
; You should have received a copy of the GNU Lesser General Public License
; along with the BIGNUM Library.  If not, see <http://www.gnu.org/licenses/>.
; 
; $-HEADER$

; $Date: 2015-04-24 11:28:16 +0200 (Fr, 24 Apr 2015) $
; $Rev: 26590 $



EXTRN memset:PROC

_TEXT SEGMENT

stackframe = 11*8

; stack parameter
rbx_save = stackframe + 8     ; arg1 spill
rsi_save = stackframe + 16    ; arg2 spill
r12_save = stackframe + 24    ; arg3 spill
r14_save = stackframe + 32    ; arg4 spill
blen = stackframe + 40        ; arg5

; second bulk is saved before we do inner loops
rdi_save = 8
rbp_save = rdi_save + 8
r13_save = rbp_save + 8
r15_save = r13_save + 8

; parameters
r = r15_save + 8
a = r + 8
alen = a + 8
b = alen + 8

; local variables
jump = b + 8
remain = jump + 8


   align 16

?omul2_asmx64@bignum@@YAXPEA_KPEB_K_K12@Z PROC

; 306  : void omul2_asmx64(uint64* r, const uint64* a, uint64 alen, const uint64* b, uint64 blen) {

; rcx = r
; rdx = a
; r8 = alen
; r9 = b
; blen (sp�ter in r11)

; rbx, rsi, rdi, rbp, r12 - r15 must be preserved by callee
; may be destroyed: rax, rcx, rdx, r8, r9, r10, r11
; http://msdn.microsoft.com/en-us/magazine/cc300794.aspx
; even better:
; http://blogs.msdn.com/b/oldnewthing/archive/2004/01/14/58579.aspx

; 313  :    if (blen == 0) {

   mov   r11, QWORD PTR [blen-stackframe+rsp]  ; r11 = blen

; no need to check for zero, omul_ls() does that already
;   test  r11, r11
;   jnz   SHORT not_zero

;   shl   r8, 3
;   xor   edx, edx
;   jmp   memset


; ------ proper start

; --- register allocation:
; rcx = r      (incoming, volatile)
; rdx = a      (incoming, volatile)
; rsi = a + n
; r8 = alen    (incoming, volatile)
; r9 = b       (incoming, volatile)
; r10 = of     (volatile)
; r11 = blen   (incoming, volatile)
; r12 = bb
; r14 = a + alen

not_zero:
   sub   rsp, stackframe

   mov   QWORD PTR rsi_save[rsp], rsi
   mov   QWORD PTR r12_save[rsp], r12
   mov   QWORD PTR r14_save[rsp], r14

   ; save parameters, part 1
   mov   QWORD PTR r[rsp], rcx
   mov   QWORD PTR a[rsp], rdx

; 320  :    // unrolled inner loop on first element:
; 321  :    // init the first 'alen' elements in result with product without adding up old contents.
; 322  :    small of = 0;
; 323  :    const small bb = b[0];

   mov   rsi, rdx                      ; rsi = a
   lea   r14, QWORD PTR [rsi+r8*8]     ; r8 = alen, r14 = a + alen

   mov   r12, QWORD PTR [r9]           ; r9 = b, r12 = b[0] = bb
   xor   r10d, r10d                    ; r10 = of = 0

; 324  :    index i = alen % 8;
; 325  :    switch (i) {
;              ...
; 335  :    }

   mov   rdx, r8                       ; r8 = alen
   and   rdx, 7                        ; rdx = alen % 8
   lea   rax, offset init_table
   add   rax, QWORD PTR [rax+rdx*8]
   lea   rsi, QWORD PTR [rsi+rdx*8]    ; adjust for alen % 8
   lea   rcx, QWORD PTR [rcx+rdx*8]    ; adjust for alen % 8
   jmp   rax


; ------ init loop

; --- register allocation:
; rcx = r + 8*n
; rsi = a + 8*n
; r8 = alen
; r9 = b
; r10 = of
; r11 = blen
; r12 = bb
; r14 = a + alen

   align 16
init_loop:
; 338  : l0:while (i < alen) {

; 340  :       i += 8;
   add   rsi, 64
   add   rcx, 64

; 341  :       s(-8);
   mov   rax, QWORD PTR [rsi-64]
   mul   r12
   add   r10, rax
   mov   QWORD PTR [rcx-64], r10
   adc   rdx, 0
   mov   r10, rdx

; 342  : l7:   s(-7);
init_loop7:
   mov   rax, QWORD PTR [rsi-56]
   mul   r12
   add   r10, rax
   mov   QWORD PTR [rcx-56], r10
   adc   rdx, 0
   mov   r10, rdx

; 343  : l6:   s(-6);
init_loop6:
   mov   rax, QWORD PTR [rsi-48]
   mul   r12
   add   r10, rax
   mov   QWORD PTR [rcx-48], r10
   adc   rdx, 0
   mov   r10, rdx

; 344  : l5:   s(-5);
init_loop5:
   mov   rax, QWORD PTR [rsi-40]
   mul   r12
   add   r10, rax
   mov   QWORD PTR [rcx-40], r10
   adc   rdx, 0
   mov   r10, rdx

; 345  : l4:   s(-4);
init_loop4:
   mov   rax, QWORD PTR [rsi-32]
   mul   r12
   add   r10, rax
   mov   QWORD PTR [rcx-32], r10
   adc   rdx, 0
   mov   r10, rdx

; 346  : l3:   s(-3);
init_loop3:
   mov   rax, QWORD PTR [rsi-24]
   mul   r12
   add   r10, rax
   mov   QWORD PTR [rcx-24], r10
   adc   rdx, 0
   mov   r10, rdx

; 347  : l2:   s(-2);
init_loop2:
   mov   rax, QWORD PTR [rsi-16]
   mul   r12
   add   r10, rax
   mov   QWORD PTR [rcx-16], r10
   adc   rdx, 0
   mov   r10, rdx

; 348  : l1:   s(-1);
init_loop1:
   mov   rax, QWORD PTR [rsi-8]
   mul   r12
   add   r10, rax
   mov   QWORD PTR [rcx-8], r10
   adc   rdx, 0
   mov   r10, rdx

init_loop0:
   cmp   rsi, r14                      ; a+i < a+alen?
   jb    init_loop

   mov   QWORD PTR [rcx], r10    ; r[i] = of


; ------ more?  save the rest of the registers

; --- register allocation:
; rcx = r + 8*n
; rsi = a + 8*n
; r8 = alen
; r9 = b
; r10 = of
; r11 = blen
; r12 = bb
; r14 = a + alen

   cmp   r11, 1                        ; r11 = blen
   jbe   no_outer_loop

   ; save registers we need to preserve
   mov   QWORD PTR rbx_save[rsp], rbx
   mov   QWORD PTR rdi_save[rsp], rdi
   mov   QWORD PTR rbp_save[rsp], rbp
   mov   QWORD PTR r13_save[rsp], r13
   mov   QWORD PTR r15_save[rsp], r15

   ; save parameters, part 2
   mov   QWORD PTR alen[rsp], r8
   mov   QWORD PTR b[rsp], r9


; ------ interleaved outer loop prologue (executed only once)

; --- register allocation:
; r8 = alen       (incoming)
; r9 = b          (incoming)
; r11 = blen      (incoming)
; r14 = a + alen  (incoming)
; r15 = j

; 368  :    index j = 1;

   mov   r15, 1                        ; r15 = j = 1

; 370  :    while (j+1 < blen) {  // make sure last two accesses will fit

;   jmp   simple_loop

   cmp   r11, 2                        ; r11 = blen
   jbe   simple_loop

   ; precompute jump into il_inner_loop
   lea   rax, QWORD PTR [r8-1]         ; r8 = alen (we know it's > 0)
   xor   edx, edx
   mov   r10, 6
   div   r10
   mov   rax, offset il_inner_table
   add   rax, QWORD PTR [rax+rdx*8]
   mov   QWORD PTR jump[rsp], rax

   sub   r14, 8                        ; r14 is now a + alen - 1


; ------ il_outer loop

; --- register allocation:
; rsi = a + 6*n
; rdi = rm + 6*n
; r11 = blen      (incoming)
; r12 = bb0
; r13 = bb1
; r14 = a + alen - 1 (incoming)
; r15 = j         (incoming)

   align 16
il_outer_loop:
; 371  :       rm = r + j;
; 372  :       const small bb0 = b[j];
; 373  :       const small bb1 = b[j+1];
   mov   rsi, QWORD PTR a[rsp]         ; rsi = a
   mov   rdi, QWORD PTR r[rsp]         ; rdi = r
   mov   rbx, QWORD PTR b[rsp]         ; rbx = b
   lea   rdi, QWORD PTR [rdi+r15*8]    ; rdi = rm = r+j
   mov   r12, QWORD PTR [rbx+r15*8]    ; r12 = bb0 = b[j]
   mov   r13, QWORD PTR [rbx+r15*8+8]  ; r13 = bb1 = b[j+1]
   mov   rax, QWORD PTR [rsi]          ; rax = a[0]
   mov   rcx, QWORD PTR [rdi]          ; rcx = rm[0]

; 374  :       of = ops<small>::muladdc(rm[0], a[0], bb0, rm[0], 0);  // of:rm[0] = a[0] * b[0] + rm[0]  [max b^2-b]
; 376  :       small ofh = 0;

   mul   r12                           ; bb0 * a[0]
   xor   r8d, r8d                      ; of = 0
   xor   r10d, r10d                    ; ofh = 0
   add   rcx, rax
   mov   rax, QWORD PTR jump[rsp]
   mov   QWORD PTR [rdi], rcx
   adc   r10, rdx
   jmp   rax                           ; jump to right loop starter


; --- register allocation:
; rax = scratch
; rbx = rm[i+2]
; rcx = rm[i+1]
; rdx = scratch
; rsi = a + 6*n
; rdi = rm + 6*n
; rbp = free
; r8, r9, r10 = carries
; r11 = blen
; r12 = bb0
; r13 = bb1
; r14 = a + alen - 1
; r15 = j


; ------ loop starters
; they get the overflow in r10, with r8 == 0

il_inner_start5:
;  registers needed: r9:r10 and rm[i+1] add
   add   rsi, 40
   add   rdi, 40
   mov   rcx, QWORD PTR [rdi-32]       ; rbx = rm[i+1]
   xor   r9d, r9d
   add   r10, rcx
   adc   r9, 0
   jmp   il_inner_go5

il_inner_start4:
;  registers needed: r8:r9
   add   rsi, 32
   add   rdi, 32
   mov   r9, r10
   jmp   il_inner_go4

il_inner_start3:
;  registers needed: r10:r8 and rm[i+1] add
   add   rsi, 24
   add   rdi, 24
   mov   r8, QWORD PTR [rdi-16]        ; r8 = rm[i+1]
   add   r8, r10
   mov   r10, 0
   adc   r10, 0
   jmp   il_inner_go3

il_inner_start2:
;  registers needed: r9:r10
   add   rsi, 16
   add   rdi, 16
   xor   r9d, r9d
   jmp   il_inner_go2

il_inner_start1:
;  registers needed: r8:r9 and rm[i+1] add
   add   rsi, 8
   add   rdi, 8
   mov   r9, QWORD PTR [rdi]           ; r9 = rm[i+1]
   add   r9, r10
   adc   r8, 0
   jmp   il_inner_go1

il_inner_start0:
;  registers needed: r10:r8
   mov   r8, r10
   xor   r10d, r10d
   jmp   il_inner_loop_test


; ------ interleaved inner loop, 6-times unrolled

; 391  : #define s(o) do {  \
; 392  :          small p1l;  \
; 393  :          const small p1h = ops<small>::muladdc(p1l, a[i+(o)], bb1, rm[i+1+(o)], 0);  \
; 394  :          small p2l;  \
; 395  :          const small p2h = ops<small>::muladdc(p2l, a[i+1+(o)], bb0, of, 0);  \
; 396  :          ofh += ops<small>::addc(rm[i+1+(o)], p1l, p2l, 0);  \
; 397  :          ofh = ops<small>::addc(of, p1h, p2h, ofh);  \
; 398  :       } while(0)

   align 16
il_inner_loop:

; 414  :             i += 6;
   add   rdi, 48                       ; advance rm
   add   rsi, 48                       ; advance a

; 415  :             s(-6);
   mov   rax, QWORD PTR [rsi-48]       ; rax = a[i]
   mov   rcx, QWORD PTR [rdi-40]       ; rcx = rm[i+1]
   mov   rbx, QWORD PTR [rdi-32]       ; rbx = rm[i+2]
   mul   r13                           ; r13 = bb1
   xor   r9d, r9d

   add   r8, rcx
   adc   r10, rbx
   adc   r9, 0                         ; r9:r10:r8 = ofh:of + rm[i+1] + rm[i+2]

   add   r8, rax
   mov   rax, QWORD PTR [rsi-40]
   adc   r10, rdx
   adc   r9, 0                         ; r9:r10:r8 = a[i] * bb1 + ofh:of + rm[i+1] + rm[i+2]

   mul   r12                           ; r12 = bb0

   add   r8, rax
   mov   QWORD PTR [rdi-40], r8
   adc   r10, rdx
   adc   r9, 0

il_inner_go5:
;  registers needed: r9:r10 and rm[i+1] add
; 416  :             s(-5);
   mov   rax, QWORD PTR [rsi-40]       ; rax = a[i]
   mul   r13                           ; r13 = bb1 ; a[i] * bb1
   xor   r8d, r8d

   add   r10, rax
   mov   rax, QWORD PTR [rsi-32]
   adc   r9, rdx
   adc   r8, 0                         ; r8:r9:r10 = a[i] * bb1 + ofh:of

   mul   r12                           ; r12 = bb0

   add   r10, rax
   mov   QWORD PTR [rdi-32], r10       ; save rm[i+1]
   adc   r9, rdx
   adc   r8, 0                         ; r8:r9:r10 = a[i] * bb1 + a[i+1] * bb0 + ofh:of + rm[i+1]

il_inner_go4:
;  registers needed: r8:r9
; 417  :             s(-4);
   mov   rax, QWORD PTR [rsi-32]       ; rax = a[i]
   mov   rcx, QWORD PTR [rdi-24]       ; rcx = rm[i+1]
   mov   rbx, QWORD PTR [rdi-16]       ; rbx = rm[i+2]
   mul   r13
   xor   r10d, r10d

   add   r9, rcx
   adc   r8, rbx
   adc   r10, 0                        ; r10:r8:r9 = ofh:of + rm[i+1] + rm[i+2]

   add   r9, rax
   mov   rax, QWORD PTR [rsi-24]
   adc   r8, rdx
   adc   r10, 0                        ; r10:r8:r9 = a[i] * bb1 + ofh:of + rm[i+1]

   mul   r12                           ; a[i+1] * bb0

   add   r9, rax
   mov   QWORD PTR [rdi-24], r9        ; save rm[i+1]
   adc   r8, rdx
   adc   r10, 0                        ; r10:r8:r9 = a[i] * bb1 + a[i] * bb0 + ofh:of + rm[i+1]

il_inner_go3:
;  registers needed: r10:r8 and rm[i+1] add
; 418  :             s(-3);
   mov   rax, QWORD PTR [rsi-24]       ; rax = a[i]
   mul   r13                           ; r13 = bb1 ; a[i] * bb1
   xor   r9d, r9d

   add   r8, rax
   mov   rax, QWORD PTR [rsi-16]
   adc   r10, rdx
   adc   r9, 0                         ; r9:r10:r8 = a[i] * bb1 + ofh:of + rm[i+1]

   mul   r12                           ; r12 = bb0

   add   r8, rax
   mov   QWORD PTR [rdi-16], r8
   adc   r10, rdx
   adc   r9, 0

il_inner_go2:
;  registers needed: r9:r10
; 419  :             s(-2);
   mov   rax, QWORD PTR [rsi-16]
   mov   rcx, QWORD PTR [rdi-8]
   mov   rbx, QWORD PTR [rdi]
   mul   r13
   xor   r8d, r8d

   add   r10, rcx
   adc   r9, rbx
   adc   r8, 0

   add   r10, rax
   mov   rax, QWORD PTR [rsi-8]
   adc   r9, rdx
   adc   r8, 0

   mul   r12

   add   r10, rax
   mov   QWORD PTR [rdi-8], r10
   adc   r9, rdx
   adc   r8, 0

il_inner_go1:
;  registers needed: r8:r9 and rm[i+1] add
; 420  :             s(-1);
   mov   rax, QWORD PTR [rsi-8]
   mul   r13
   xor   r10d, r10d

   add   r9, rax
   mov   rax, QWORD PTR [rsi]
   adc   r8, rdx
   adc   r10, 0

   mul   r12

   add   r9, rax
   mov   QWORD PTR [rdi], r9
   adc   r8, rdx
   adc   r10, 0


il_inner_loop_test:
   cmp   rsi, r14                      ; a+i < alen-1?
   jb    il_inner_loop


il_inner_loop_end:
; --- register allocation:
; rsi = a + 6*n
; rdi = rm + 6*n
; r10:r8 = carries
; r11 = blen
; r12 = bb0
; r13 = bb1
; r14 = a + alen - 1
; r15 = j

; 428  :       // p1h:p1l = a[i] * b[j+1] + of
; 429  :       of = ops<small>::muladdc(rm[i+1], a[i], bb1, of, 0);  // [max b^2-b-2]
; 430  :       of = ops<small>::addc(rm[i+2], of, ofh, 0);  // [max b+1]
; 431  :       rm[i+3] = of;  // [max 1]
; 432  :       j += 2;

   mov   rax, QWORD PTR [rsi]          ; a[i]
   mul   r13
   xor   r9d, r9d
   add   r8, rax
   mov   QWORD PTR [rdi+8], r8         ; rm[i+1]
   adc   r10, rdx
   mov   QWORD PTR [rdi+16], r10       ; rm[i+2]

   add   r15, 2
   lea   rax, QWORD PTR[r15+1]         ; rax = (j += 2) + 1
   cmp   rax, r11                      ; j+1 < blen?
   jb    il_outer_loop
   cmp   r15, r11
   jae   exit_restore


; --- reload some registers for simple loop

   add   r14, 8                        ; r14 is no again a + alen


simple_loop:
; ------ simple inner loop prologue (executed only once)

; --- register allocation:
; rbx = b + j
; rcx = rm[i]
; rsi = a         (incoming)
; rdi = rm + 6*n
; r9, r10 = carries
; r11 = blen      (incoming)
; r12 = bb
; r14 = a + alen  (incoming)
; r15 = j         (incoming)

   mov   rbx, QWORD PTR b[rsp]         ; rbx = b
   lea   rbx, QWORD PTR [rbx+r15*8-8]  ; rbx = b+j

; 436  :    while (j < blen) {

   ; precompute jump into s_inner_loop
   mov   rdx, QWORD PTR alen[rsp]       ; rdx = alen
   and   rdx, 7
   mov   rax, offset s_inner_table
   add   rax, QWORD PTR [rax+rdx*8]
   mov   QWORD PTR jump[rsp], rax
   shl   rdx, 3
   mov   QWORD PTR remain[rsp], rdx    ; save alen % 8 for adjustment


   align 16
s_outer_loop:

; 437  :       rm = r + j;
; 438  :       const small bb = b[j];

   add   rbx, 8
   mov   r12, QWORD PTR [rbx]          ; r12 = b[j] = bb
   mov   rsi, QWORD PTR a[rsp]         ; rsi = a
   mov   rdi, QWORD PTR r[rsp]         ; rdi = r
   lea   rdi, QWORD PTR [rdi+r15*8]    ; rdi = rm = r + j
   mov   rdx, QWORD PTR remain[rsp]
   mov   rax, QWORD PTR jump[rsp]
   xor   r9d, r9d
   xor   r10d, r10d                    ; of = 0
   add   rsi, rdx                      ; adjust for alen % 8
   add   rdi, rdx                      ; adjust for alen % 8
   jmp   rax


s_starter7:
   mov   rax, QWORD PTR [rsi-56]
   jmp   s_inner_loop7

s_starter6:
   mov   rax, QWORD PTR [rsi-48]
   jmp   s_inner_loop6

s_starter5:
   mov   rax, QWORD PTR [rsi-40]
   jmp   s_inner_loop5

s_starter4:
   mov   rax, QWORD PTR [rsi-32]
   jmp   s_inner_loop4

s_starter3:
   mov   rax, QWORD PTR [rsi-24]
   jmp   s_inner_loop3

s_starter2:
   mov   rax, QWORD PTR [rsi-16]
   jmp   s_inner_loop2

s_starter1:
   mov   rax, QWORD PTR [rsi-8]
   jmp   s_inner_loop1


; 453  : #define s(o) of = ops<small>::muladdc(rm[i+(o)], a[i+(o)], bb, rm[i+(o)], of)

   align 16
s_inner_loop:

; 468  :             i += 8;
   add   rsi, 64
   add   rdi, 64

; 471  :             s(-8);
   mov   rax, QWORD PTR [rsi-64]       ; a[i]
   mul   r12                           ; r12 = bb
   mov   rcx, QWORD PTR [rdi-64]       ; rm[i]
   xor   r10d, r10d
   add   r9, rcx
   adc   r10, 0
   add   r9, rax
   mov   QWORD PTR [rdi-64], r9
   mov   rax, QWORD PTR [rsi-56]       ; a[i]
   adc   r10, rdx

s_inner_loop7:
; 472  :             s(-7);
   mul   r12                           ; r12 = bb
   mov   rcx, QWORD PTR [rdi-56]       ; rm[i]
   xor   r9d, r9d
   add   r10, rcx
   adc   r9, 0
   add   r10, rax
   mov   QWORD PTR [rdi-56], r10
   mov   rax, QWORD PTR [rsi-48]       ; a[i]
   adc   r9, rdx

s_inner_loop6:
; 471  :             s(-6);
   mul   r12                           ; r12 = bb
   mov   rcx, QWORD PTR [rdi-48]       ; rm[i]
   xor   r10d, r10d
   add   r9, rcx
   adc   r10, 0
   add   r9, rax
   mov   QWORD PTR [rdi-48], r9
   mov   rax, QWORD PTR [rsi-40]       ; a[i]
   adc   r10, rdx

s_inner_loop5:
; 472  :             s(-5);
   mul   r12                           ; r12 = bb
   mov   rcx, QWORD PTR [rdi-40]       ; rm[i]
   xor   r9d, r9d
   add   r10, rcx
   adc   r9, 0
   add   r10, rax
   mov   QWORD PTR [rdi-40], r10
   mov   rax, QWORD PTR [rsi-32]       ; a[i]
   adc   r9, rdx

s_inner_loop4:
; 473  :             s(-4);
   mul   r12                           ; r12 = bb
   mov   rcx, QWORD PTR [rdi-32]       ; rm[i]
   xor   r10d, r10d
   add   r9, rcx
   adc   r10, 0
   add   r9, rax
   mov   QWORD PTR [rdi-32], r9
   mov   rax, QWORD PTR [rsi-24]       ; a[i]
   adc   r10, rdx

s_inner_loop3:
; 474  :             s(-3);
   mul   r12                           ; r12 = bb
   mov   rcx, QWORD PTR [rdi-24]       ; rm[i]
   xor   r9d, r9d
   add   r10, rcx
   adc   r9, 0
   add   r10, rax
   mov   QWORD PTR [rdi-24], r10
   mov   rax, QWORD PTR [rsi-16]       ; a[i]
   adc   r9, rdx

s_inner_loop2:
; 475  :             s(-2);
   mul   r12                           ; r12 = bb
   mov   rcx, QWORD PTR [rdi-16]       ; rm[i]
   xor   r10d, r10d
   add   r9, rcx
   adc   r10, 0
   add   r9, rax
   mov   QWORD PTR [rdi-16], r9
   mov   rax, QWORD PTR [rsi-8]        ; a[i]
   adc   r10, rdx

s_inner_loop1:
; 476  :             s(-1);
   mul   r12                           ; r12 = bb
   mov   rcx, QWORD PTR [rdi-8]        ; rm[i]
   xor   r9d, r9d
   add   r10, rcx
   adc   r9, 0
   add   r10, rax
   mov   QWORD PTR [rdi-8], r10
   adc   r9, rdx

s_inner_loop_test:
   cmp   rsi, r14                      ; a+i < a+alen?
   jb    s_inner_loop

   mov   QWORD PTR [rdi], r9           ; of


s_outer_test:
   inc   r15
   cmp   r15, r11                      ; r15 = j, r11 = blen
   jb    s_outer_loop


exit_restore:
   mov   rbx, QWORD PTR rbx_save[rsp]
   mov   rdi, QWORD PTR rdi_save[rsp]
   mov   rbp, QWORD PTR rbp_save[rsp]
   mov   r13, QWORD PTR r13_save[rsp]
   mov   r15, QWORD PTR r15_save[rsp]

no_outer_loop:
   mov   rsi, QWORD PTR rsi_save[rsp]
   mov   r12, QWORD PTR r12_save[rsp]
   mov   r14, QWORD PTR r14_save[rsp]

   add   rsp, stackframe

both_zero:
   ret   0


   align 16
init_table:
   DQ init_loop0 - init_table
   DQ init_loop1 - init_table
   DQ init_loop2 - init_table
   DQ init_loop3 - init_table
   DQ init_loop4 - init_table
   DQ init_loop5 - init_table
   DQ init_loop6 - init_table
   DQ init_loop7 - init_table

il_inner_table:
   DQ il_inner_start0 - il_inner_table
   DQ il_inner_start1 - il_inner_table
   DQ il_inner_start2 - il_inner_table
   DQ il_inner_start3 - il_inner_table
   DQ il_inner_start4 - il_inner_table
   DQ il_inner_start5 - il_inner_table

s_inner_table:
   DQ s_inner_loop_test - s_inner_table
   DQ s_starter1 - s_inner_table
   DQ s_starter2 - s_inner_table
   DQ s_starter3 - s_inner_table
   DQ s_starter4 - s_inner_table
   DQ s_starter5 - s_inner_table
   DQ s_starter6 - s_inner_table
   DQ s_starter7 - s_inner_table

?omul2_asmx64@bignum@@YAXPEA_KPEB_K_K12@Z ENDP

_TEXT ENDS

END
