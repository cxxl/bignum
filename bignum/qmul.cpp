// $+HEADER$
// 
// Copyright 2015 Christoph Lueders
// 
// This file is part of the BIGNUM Library.
// 
// The BIGNUM Library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
// 
// The BIGNUM Library is distributed in the hope that it will be useful, but 
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public 
// License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with the BIGNUM Library.  If not, see <http://www.gnu.org/licenses/>.
// 
// $-HEADER$

// $Date: 2015-04-24 11:28:16 +0200 (Fr, 24 Apr 2015) $
// $Rev: 26590 $

/*
 *  arbitrary length number arithmetic - modular fft multiplication
 */

#include "precomp.h"
#include "cl/memzero.h"
#include "impl.h"
#include "small.h"
#include "fft.h"
#include "cl/format.h"


// could be interesting: http://www.mersenneforum.org/showthread.php?t=15588

// we must switch off "unreachable code" warning, since bool template parameters generate code paths that are never used
#pragma warning (disable:4702)

namespace bignum {

index _CL_CALL switch_qmul(index set) {
   const index r = qmul_thresh;
   qmul_thresh = tmax(set, qmul_min_len);  // must not be less than 2, otherwise it will never end recursion
   return r;
}

index _CL_CALL switch_qsqu(index set) {
   const index r = qsqu_thresh;
   qsqu_thresh = tmax(set, qmul_min_len);  // must not be less than 2, otherwise it will never end recursion
   return r;
}


// ------ QUICKMUL: MODULAR FFT MULTIPLICATION --------------------------------

template<typename small>
struct qmul_params {
};

template<>
struct qmul_params<uint8> {
   static const uint8 mod = 193;  // = 2^6 * 3
   static const unsigned mod_log = 7;
   // 5 is a generator and a (mod-1)-th root
   static const uint8 w = 125;  // 5^3
   static const index fft_depth = 6;
   static const index tpl_n = 0;
   template<int n> struct low_w { static const uint64 value = 1; };
   static __forceinline uint8 modmul(const uint8 u, const uint8 v) {
      return ((uint16)u * v) % mod;
   }
};

template<>
struct qmul_params<uint16> {
   static const uint16 mod = 40961;  // = 2^13 * 5
   static const unsigned mod_log = 15;
   // 3 is a generator and a (mod-1)-th root
   static const uint16 w = 243;  // 3^5
   static const index fft_depth = 13;
   static const index tpl_n = 0;
   template<int n> struct low_w { static const uint64 value = 1; };
   static __forceinline uint16 modmul(const uint16 u, const uint16 v) {
      return ((uint32)u * v) % mod;
   }
};

template<>
struct qmul_params<uint32> {
   static const uint32 mod = 3489660929;  // = 2^28 * 13
   static const unsigned mod_log = 31;
   // 3 is a generator and a (mod-1)-th root
   static const uint32 w = 1594323;  // 3^13
   static const index fft_depth = 28;
   static const index tpl_n = 0;
   template<int n> struct low_w { static const uint64 value = 1; };
   static __forceinline uint32 modmul(const uint32 u, const uint32 v) {
      return ((uint64)u * v) % mod;
   }
};

#ifdef _M_X64
#if 1
// true 64 bit
template<>
struct qmul_params<uint64> {
   static const uint64 mod = 10232178353385766913;  // == 2^57 * 71
   static const unsigned mod_log = 63;
   // 3 is a generator and a (mod-1)-th root
   static const uint64 w = 3419711604162223203;  // 3^71
   static const index fft_depth = 57;
   static const index tpl_n = 32;
   template<int n> struct low_w { static const uint64 value = 1; };
   template<> struct low_w<2> {
      static const uint64 value = 10232178353385766912;
      template<int n> struct low_w_pow { static const uint64 value = 1; };
   };
   template<> struct low_w<4> {
      static const uint64 value = 9336970564456853289;
      template<int n> struct low_w_pow { static const uint64 value = 1; };
      template<> struct low_w_pow<1> { static const uint64 value = 9336970564456853289; };
   };
   template<> struct low_w<8> {
      static const uint64 value = 2772096347440647993;
      template<int n> struct low_w_pow { static const uint64 value = 1; };
      template<> struct low_w_pow<1> { static const uint64 value = 2772096347440647993; };
      template<> struct low_w_pow<2> { static const uint64 value = 9336970564456853289; };
      template<> struct low_w_pow<3> { static const uint64 value = 6298661554675146767; };
   };
   template<> struct low_w<16> {
      static const uint64 value = 5030854302624130630;
      template<int n> struct low_w_pow { static const uint64 value = 1; };
      template<> struct low_w_pow<1> { static const uint64 value = 5030854302624130630; };
      template<> struct low_w_pow<2> { static const uint64 value = 2772096347440647993; };
      template<> struct low_w_pow<3> { static const uint64 value = 3592825278396647154; };
      template<> struct low_w_pow<4> { static const uint64 value = 9336970564456853289; };
      template<> struct low_w_pow<5> { static const uint64 value = 8726024001988674581; };
      template<> struct low_w_pow<6> { static const uint64 value = 6298661554675146767; };
      template<> struct low_w_pow<7> { static const uint64 value = 7133508746187551932; };
   };
   template<> struct low_w<32> {
      static const uint64 value = 2008881404560210166;
      template<int n> struct low_w_pow { static const uint64 value = 1; };
      template<> struct low_w_pow<1> { static const uint64 value = 2008881404560210166; };
      template<> struct low_w_pow<2> { static const uint64 value = 5030854302624130630; };
      template<> struct low_w_pow<3> { static const uint64 value = 8979380501225731084; };
      template<> struct low_w_pow<4> { static const uint64 value = 2772096347440647993; };
      template<> struct low_w_pow<5> { static const uint64 value = 5391665280075306233; };
      template<> struct low_w_pow<6> { static const uint64 value = 3592825278396647154; };
      template<> struct low_w_pow<7> { static const uint64 value = 6019538214791545918; };
      template<> struct low_w_pow<8> { static const uint64 value = 9336970564456853289; };
      template<> struct low_w_pow<9> { static const uint64 value = 749117769138701941; };
      template<> struct low_w_pow<10> { static const uint64 value = 8726024001988674581; };
      template<> struct low_w_pow<11> { static const uint64 value = 924799321563057856; };
      template<> struct low_w_pow<12> { static const uint64 value = 6298661554675146767; };
      template<> struct low_w_pow<13> { static const uint64 value = 1386459332737351937; };
      template<> struct low_w_pow<14> { static const uint64 value = 7133508746187551932; };
      template<> struct low_w_pow<15> { static const uint64 value = 2886800246580651752; };
   };
   static __forceinline uint64 modmul(const uint64 u, const uint64 v) {
      classert(u < mod);
      classert(v < mod);
#if 0
      const uint64 r = modmul_asmx64(u, v, mod);
#else
      const uint64 r = modmul2_asmx64(u, v);
#endif
      classert(r < mod);
      return r;
   }
};
#else
// untrue 64 bit
template<>
struct qmul_params<uint64> {
   static const uint64 mod = 3489660929;  // = 2^28 * 13
   static const unsigned mod_log = 31;
   // 3 is a generator and a (mod-1)-th root
   static const uint64 w = 1594323;  // 3^13
   static const index fft_depth = 28;
   static const index tpl_n = 16;
   template<int n> struct low_w { static const uint64 value = 1; };
   template<> struct low_w<2> { static const uint64 value = 1392672017; };
   template<> struct low_w<4> { static const uint64 value = 1178393024; };
   template<> struct low_w<8> { static const uint64 value = 2206155841; };
   template<> struct low_w<16> { static const uint64 value = 2106507513; };
   template<> struct low_w<32> { static const uint64 value = 1139259149; };
   static __forceinline uint64 modmul(const uint64 u, const uint64 v) {
      return (u * v) % mod;
   }
};
#endif
#endif

#pragma warning(push)
#pragma warning(disable:4305)
#pragma warning(disable:4309)
#pragma warning(disable:4706)  // assignment within conditional expression

template<typename small>
struct q_mul {
   static const small mod = qmul_params<small>::mod;
   static const unsigned mod_log = qmul_params<small>::mod_log;

   small* r;
   const small* a;
   const small* b;
   const index alen;
   const index blen;
   const bool square;

   index n;
   index w;
   index cut_bits;
   unsigned depth;

   static small* pre_w[qmul_params<small>::fft_depth];

#ifdef CL_TEST_CODE
   q_mul(bool) : r(0), a(0), b(0), alen(0), blen(0), square(false) { }
#endif
   q_mul(small* _r, const small* _a, const index _alen, const small* _b, const index _blen, const bool _square) :
      r(_r), a(_a), b(_b), alen(_alen), blen(_blen), square(_square) { }
   static __forceinline small modmul(const small u, const small v) {
      classert(u < mod);
      classert(v < mod);
      return qmul_params<small>::modmul(u, v);
   }
#ifdef CL_TEST_CODE
   static void test_modmul() {
      small r = modmul(2, 3);
      classert(r == 6);
      r = modmul(mod-1, mod-3);
      small r2 = modmul(mod-2, mod-2);
      r2 -= r;
      classert(r2 == 1);
      if (sizeof(small) == sizeof(uint64)) {
         small r = modmul(64, 10072300566614114305);
         classert(r == 1);
      }
   }
#endif

   static __forceinline small modadd(const small u, const small v) {
      classert(u < mod);
      classert(v < mod);
      small t = u + v;
      const small sub1 = t < u && mod_log+1 >= bits(small) ? mod : 0;
      const small sub2 = t >= mod ? mod : 0;
      t -= (mod_log+1 >= bits(small) ? sub1 : 0) + sub2;
      classert(t >= 0 && t < mod);
      return t;
   }
#ifdef CL_TEST_CODE
   static void test_modadd() {
      small r = modadd(1, 2);
      classert(r == 3);
      r = modadd(1, mod-1);
      classert(r == 0);
      r = modadd(1, mod-2);
      classert(r == mod-1);
      r = modadd(1, mod-2);
      classert(r == mod-1);
      r = modadd(mod-1, mod-1);
      classert(r == mod-2);
   }
#endif

   static __forceinline small modsub(const small u, const small v) {
      classert(u < mod);
      classert(v < mod);
      const small add = u < v ? mod : 0;
      const small t = u - v + add;
      classert(t >= 0 && t < mod);
      return t;
   }
#ifdef CL_TEST_CODE
   static void test_modsub() {
      small r = modsub(2, 1);
      classert(r == 1);
      r = modsub(mod-1, 1);
      classert(r == mod-2);
      r = modsub(1, 2);
      classert(r == mod-1);
   }
#endif

   // from http://comeoncodeon.wordpress.com/2011/10/09/modular-multiplicative-inverse/
   // This function returns the gcd of a and b followed by the pair x and y of equation ax + by = gcd(a,b)

#define signed_sub(a,b,as,bs,diff,diffs)           \
   do {                                            \
      if (as == bs) {                              \
         if (a < b) {                              \
            diff = (b) - (a);                      \
            diffs = !as;                           \
         }                                         \
         else {                                    \
            diff = (a) - (b);                      \
            diffs = as;                            \
         }                                         \
      }                                            \
      else {                                       \
         diff = a + b;                             \
         diffs = as;                               \
      }                                            \
   } while(0)

#ifdef CL_TEST_CODE
   static void test_qmul_signed_sub() {
      small d;
      bool sign;
      signed_sub(3, 2, false, false, d, sign);     // 3 - 2 = 1
      classert(sign == false && d == 1);
      signed_sub(3, 2, true, false, d, sign);      // -3 - 2 = -5
      classert(sign == true && d == 5);
      signed_sub(3, 2, false, true, d, sign);      // 3 - -2 = 5
      classert(sign == false && d == 5);
      signed_sub(3, 2, true, true, d, sign);       // -3 - -2 = -1
      classert(sign == true && d == 1);

      signed_sub(2, 3, false, false, d, sign);     // 2 - 3 = -1
      classert(sign == true && d == 1);
      signed_sub(2, 3, true, false, d, sign);      // -2 - 3 = -5
      classert(sign == true && d == 5);
      signed_sub(2, 3, false, true, d, sign);      // 2 - -3 = 5
      classert(sign == false && d == 5);
      signed_sub(2, 3, true, true, d, sign);       // -2 - -3 = 1
      classert(sign == false && d == 1);
   }
#endif

#define x (x_val[i])
#define y (y_val[i])
#define x_last (x_val[1-i])
#define y_last (y_val[1-i])
#define x_new x_last
#define y_new y_last

#define x_sign (x_valsign[i])
#define y_sign (y_valsign[i])
#define x_last_sign (x_valsign[1-i])
#define y_last_sign (y_valsign[1-i])
#define x_new_sign x_last_sign
#define y_new_sign y_last_sign

// #define EGCD_DEBUG 1
   static void egcd(small a, small b, small& gg, small& xx, bool& xx_sign, small& yy, bool& yy_sign) {
#ifdef EGCD_DEBUG
      int64 _x = 1, _y = 0, _x_last = 0, _y_last = 1;
#endif
      /* int x = 1, y = 0;     int xLast = 0, yLast = 1;     int q, r, m, n; */
      unsigned i = 1;  // index into arrays
      small x_val[2] = { 0, 1 };
      small y_val[2] = { 1, 0 };
      bool x_valsign[2] = { false, false };
      bool y_valsign[2] = { false, false };

#ifdef EGCD_DEBUG
      DTOUT_PUTS(format("me: x=%?%? y=%?%? xlast=%?%? ylast=%?%?\n") << (x_sign ? "-" : "") << x << (y_sign ? "-" : "") << y << (x_last_sign ? "-" : "") << x_last << (y_last_sign ? "-" : "") << y_last);
      DTOUT_PUTS(format("ok: x=%? y=%? xlast=%? ylast=%?\n") << _x << _y << _x_last << _y_last);
#endif

      while (a != 0) {
         const small q = b / a;    // quotient
         const small r = b % a;    // remainder

         /* x_new = x_last - q*x;
         y_new = y_last - q*y;
         x_last = x;
         y_last = y;
         x = x_new;
         y = y_new; */

#ifdef EGCD_DEBUG
         int64 _x_new = _x_last - (int64)q * _x;
         int64 _y_new = _y_last - (int64)q * _y;
         _x_last = _x;
         _y_last = _y;
         _x = _x_new;
         _y = _y_new;
#endif

         classert(int_log2(q) + int_log2(x) <= bits(small));
         const small qx = q*x;
         classert(int_log2(q) + int_log2(y) <= bits(small));
         const small qy = q*y;
         signed_sub(x_last, qx, x_last_sign, x_sign, x_new, x_new_sign);
         signed_sub(y_last, qy, y_last_sign, y_sign, y_new, y_new_sign);
         i = 1-i;                   // switch last and actual x's and y's
#ifdef EGCD_DEBUG
         DTOUT_PUTS(format("me: x=%?%? y=%?%? xlast=%?%? ylast=%?%?\n") << (x_sign ? "-" : "") << x << (y_sign ? "-" : "") << y << (x_last_sign ? "-" : "") << x_last << (y_last_sign ? "-" : "") << y_last);
         DTOUT_PUTS(format("ok: x=%? y=%? xlast=%? ylast=%?\n") << _x << _y << _x_last << _y_last);
#endif
         b = a;
         a = r;
      }
      gg = b;
      xx = x_last;
      yy = y_last;
      xx_sign = x_last_sign;
      yy_sign = y_last_sign;
   }

#undef x
#undef y
#undef x_last
#undef y_last
#undef x_sign
#undef y_sign
#undef x_last_sign
#undef y_last_sign
#undef signed_sub

   static small modinv(small a, small m) {
      small g, x, y;
      bool x_sign, y_sign;
      egcd(a, m, g, x, x_sign, y, y_sign);
      const small r = x_sign ? m - x : x;
      return r;
   }
#ifdef CL_TEST_CODE
   static void test_qmul_modinv() {
      small i;
      i = modinv(4, 13);
      classert(i == 10);
      if (sizeof(small) >= sizeof(uint16)) {
         i = modinv(10, 251);
         classert(i == 226);
      }
      if (sizeof(small) >= sizeof(uint32)) {
         i = modinv(10, 44497);
         classert(i == 31148);
      }
      if (sizeof(small) >= sizeof(uint32)) {
         i = modinv(32, 3489660929);
         classert(i == 3380609025);
      }
      if (sizeof(small) >= sizeof(uint64)) {
         i = modinv(10, 4000000063);
         classert(i == 1200000019);
      }
   }
#endif
   static bool set_param2(const index rlen, unsigned& depth, index& cut_bits) {
      classert(int_log2(mod) == mod_log);
      index bl = rlen * bits(small);
      depth = qmul_params<small>::fft_depth;
      index n = (index)1 << qmul_params<small>::fft_depth;
      classert(n != 0);
      ctassert(mod_log > qmul_params<small>::fft_depth);
      cut_bits = (mod_log - qmul_params<small>::fft_depth) / 2;
      if (bl > n * cut_bits) {
         // input numbers too big
         return false;
      }
      classert(n * (((small)1 << cut_bits) - 1) * (((small)1 << cut_bits) - 1) < mod);
      classert(depth + 2*cut_bits <= mod_log);
      // scale down fft size as far as possible
      while (n > 2) {
         const auto new_n = n/2;
         const auto new_depth = depth - 1;
         const auto new_cut_bits = (mod_log - new_depth + 1) / 2;
         if (bl > new_n * new_cut_bits) break;
         classert(new_n/2 * (((small)1 << new_cut_bits) - 1) * (((small)1 << new_cut_bits) - 1) < mod);
         classert(new_depth + 2*new_cut_bits - 1 <= mod_log);
         n = new_n;
         depth = new_depth;
         cut_bits = new_cut_bits;
      }
      classert(cut_bits > 0 && cut_bits < bits(small));
      classert(2*cut_bits < bits(small));
      classert(depth + 2*cut_bits <= bits(small));
      classert(n/2 * (((small)1 << cut_bits) - 1) * (((small)1 << cut_bits) - 1) < mod);
      return true;
   }
   bool set_param() {
      classert(int_log2(mod) == mod_log);
      if (!set_param2(alen+blen, depth, cut_bits))
         return false;
      unsigned d = qmul_params<small>::fft_depth;
      w = qmul_params<small>::w;
      while (d > depth) {
         --d;
         w = modmul(w, w);
      }
      n = (index)1 << depth;
#ifndef NDEBUG
      // test if w is really a primitive root
      {
         auto x = w;
         for (unsigned i=0; i<depth; ++i) {
            classert(x != 1);
            x = modmul(x, x);
         }
         classert(x == 1);
      }
#endif
      //DTOUT_PUTS(format("qmul: using fft depth %?, cut bits %?, root %?, modulus %?\n") << depth << cut_bits << w << mod);

      // precompute roots
      small w1 = w;
      for (unsigned i=depth; i>0; --i) {
         if (pre_w[i] == 0) {
            const index n = (index)1 << i;
            pre_w[i] = bignum_alloc<small>(n/2);
            small* pw = pre_w[i];
            small ww = 1;
            for (index j=0; j<n/2; ++j) {
               pw[j] = ww;
               ww = modmul(ww, w1);
               classert(ww != 1);
            }
            classert(modmul(ww, ww) == 1);
         }
         w1 = modmul(w1, w1);
      }

      return true;
   }
#ifdef CL_TEST_CODE
   void test_set_param() {
      if (sizeof(small) == sizeof(uint64)) {
         *(small*)&alen = 64;
         *(small*)&blen = 64;
         set_param();

         *(small*)&alen = 128;
         *(small*)&blen = 128;
         set_param();
      }
   }
#endif

   // ------ display fft parameters ---------------------------------------------
   static void print_params(const index bit, const bool square) {
      const index ll = (bit + bits(small) - 1) / bits(small);
      unsigned lg;
      index mm;
      index l;
      index k;
      smul_set_params<small>(square, ll, ll, lg, mm, l, k);

      string s;
      strformat ss(s);
      ss("input length of each factor is %? words (%'? bits)\n") << ll << bit;

      if (optimise) {
         ss("fft crossover table chooses size of %? (2^%?) coefficients\n") << mm << lg;
         ss("\n");
         tout_puts(s);
         lg = smul_optimise<limb>(ll, square, true, &k, iter_time);
         const bool ok = smul_set_params<small>(square, ll, ll, lg, mm, l, k);
         caassert(ok);
         s = "\n";
      }

      ss("outer ring size N is %? words (%'? bits)\n") << mm*l << mm*l*bits(small);
      ss("fft size is %? (2^%?) coefficients\n") << mm << lg;
      ss("input coefficent size is %? words (%'? bits)\n") << l << l * bits(small);
      ss("inner ring size K is %? words (%'? bits)\n") << k / bits(small) << k;
      const uint64 waste = 10000ui64 * mm * (k + bits(small)) / (4*bit) - 10000;
      ss("waste is %,.02?%%\n") << waste;
      tout_puts(s);
   }

   template<bool square>
   void qmul() {
      // allocate temp space
      tape_alloc<small> tmp((square ? 2 : 3) * n);
      small* vr = tmp.p;
      small* va = tmp.p + n;
      small* vb = square ? 0 : tmp.p + 2*n;

      // init vectors and evaluate
      init(va, a, alen);
      evaluate(va, 0, depth);
      if (!square) {
         init(vb, b, blen);
         evaluate(vb, 0, depth);
      }

      // multiply pointwise
      for (index i=0; i<n; ++i)
         vr[i] = modmul(va[i], square ? va[i] : vb[i]);

      // shuffle result
      shuffle(vr);

      // evaluate result
      evaluate(vr, 0, depth);

      // normalise result
      small inv = modinv(n, mod);
      classert(modmul(n, inv) == 1);
      for (index i=0; i<n; ++i)
         vr[i] = modmul(vr[i], inv);

      // reassemble
      reasm(vr);
   }

   static void evaluate(small* p, const index i, const index d) {
      const index n = (index)1 << d;
      classert(n > 1);
      const index half = n/2;
      if (qmul_params<small>::tpl_n > 0 && n == qmul_params<small>::tpl_n) {
         t_evaluate<qmul_params<small>::tpl_n>(p, i);
         return;
      }
      else if (half > 1) {
         evaluate(p, i, d-1);        // even part
         evaluate(p, i+half, d-1);   // odd part
      }
      // handle w^0
      const small t = p[i+half];
      p[i+half] = modsub(p[i], t);
      p[i] = modadd(p[i], t);
      // handle w^k, k>0
      const small* pw = pre_w[d];
      for (index k=1; k<half; ++k) {
         const small t = modmul(pw[k], p[i+half+k]);
         p[k+i+half] = modsub(p[i+k], t);
         p[k+i] = modadd(p[i+k], t);
      }
   }
#ifdef CL_TEST_CODE
   void test_evaluate() {
      if (mod == 193) {
         depth = 1;
         n = (index)1 << depth;
         small v[] = { 1, 2 };
         w = modmul(185, 185);
         w = modmul(w, w);
         w = modmul(w, w);
         w = modmul(w, w);
         classert(w == 192);
         evaluate(v, 0, depth);
         classert(v[0] == 3 && v[1] == 192);
      }
      if (mod == 193) {
         depth = 2;
         n = (index)1 << depth;
         small v[] = { 1, 2, 3, 4 };
         w = modmul(185, 185);
         w = modmul(w, w);
         w = modmul(w, w);
         classert(w == 112);
         evaluate(v, 0, depth);
         classert(v[0] == 10 && v[1] == 80 && v[2] == 189 && v[3] == 111);
      }
      if (mod == 193) {
         depth = 3;
         n = (index)1 << depth;
         small v[] = { 1, 2, 3, 4, 5, 6, 7, 8 };
         w = modmul(185, 185);
         w = modmul(w, w);
         classert(w == 43);
         evaluate(v, 0, depth);
         classert(v[0] == 36 && v[1] == 46 && v[2] == 127 && v[3] == 77 && v[4] == 177 && v[5] == 114 && v[6] == 58 && v[7] == 145);
      }
   }
#endif

#if 1
   template<index n, index cnt>
   struct t_evaluate_mul_loop {
      static __forceinline void loop(small* p, const index i) {
         const small ww = qmul_params<small>::low_w<n>::low_w_pow<n/2-cnt>::value;
         const index half = n/2;
         const index k = half - cnt;
         const small t = k == 0 ? p[i+half+k] : modmul(ww, p[i+half+k]);
         p[k+i+half] = modsub(p[i+k], t);
         p[k+i] = modadd(p[i+k], t);
         t_evaluate_mul_loop<n, cnt-1>::loop(p, i);
      }
   };
   template<index n>
   struct t_evaluate_mul_loop<n, 0> {
      static __forceinline void loop(small* p, const index i) {
      }
   };

   template<index n>
   static __forceinline void t_evaluate(small* p, const index i) {
      classert(n > 1);
      const index half = n/2;
      t_evaluate<n/2>(p, i);        // even part
      t_evaluate<n/2>(p, i+half);   // odd part
#if 0
      const small w = qmul_params<small>::low_w<n>::value;
      small ww = 1;
      for (index k=0; k<half; ++k) {
         const small t = modmul(ww, p[i+half+k]);
         p[k+i+half] = modsub(p[i+k], t);
         p[k+i] = modadd(p[i+k], t);
         ww = modmul(ww, w);
      }
#else
      t_evaluate_mul_loop<n,n/2>::loop(p, i);
#endif
   }
   template<>
   static __forceinline void t_evaluate<1>(small* p, const index i) { }
   template<>
   static __forceinline void t_evaluate<0>(small* p, const index i) { }
#ifdef CL_TEST_CODE
   void test_t_evaluate() {
      /*if (sizeof(small) == sizeof(uint64)) {
         // mod == 3489660929, 1392672017 is a 2^27-th root
         {
            t_evaluate<1,1>(0, 0);
         }
         {
            small p[2] = { 1, 2 };
            t_evaluate<2,3489660928>(p, 0);
            small p2[2] = { 1, 2 };
            evaluate(p2, 0, depth);
            NOP();
         }
      }*/
   }
#endif
#endif

   void shuffle(small* r) {
      caassert(1ui64 << depth == n);
      for (index i = 0; i < n; ++i) {
         const index j = bit_rev(i, depth);
         if (i < j) {
            small* a = r+i;
            small* b = r+j;
            const small t = *a;
            *a = *b;
            *b = t;
         }
      }
   }
#ifdef CL_TEST_CODE
   void test_shuffle() {
      depth = 3;
      n = (index)1 << depth;
      {
         small v[8] = { 0, 1, 2, 3, 4, 5, 6, 7 };
         shuffle(v);
         classert(v[0] == 0 && v[1] == 4 && v[2] == 2 && v[3] == 6 && v[4] == 1 && v[5] == 5 && v[6] == 3 && v[7] == 7);
      }
      {
         small v[10] = { 0xff, 0, 1, 2, 3, 4, 5, 6, 7, 0xee };
         shuffle(v+1);
         classert(v[0] == 0xff && v[1] == 0 && v[2] == 4 && v[3] == 2 && v[4] == 6 && v[5] == 1 && v[6] == 5 && v[7] == 3 && v[8] == 7 && v[9] == 0xee);
      }
   }
#endif

   void init(small* v, const small* p, const index len) {
      // zero all dest memory
      zero(v, n);
      // set the positions that we have data for
      const small mask = ((small)1 << cut_bits) - 1;
      index s = 0;  // source small index
      index i = 0;  // source cut_bit index
      small accu = 0;  // accumulator
      unsigned accu_bits = 0;  // bits in accumulator
      // cycle over whole input vector
      while (s < len) {
         classert(accu_bits < cut_bits);
         accu |= p[s] << accu_bits;
         classert(i < n);
         const small dig = accu & mask;
         v[bit_rev(i++, depth)] = dig;
         accu = p[s++] >> (cut_bits - accu_bits);
         accu_bits += bits(small) - cut_bits;
         while (accu_bits >= cut_bits) {
            if (i >= n) return;
            const small dig = accu & mask;
            v[bit_rev(i++, depth)] = dig;
            accu >>= cut_bits;
            accu_bits -= cut_bits;
         }
      }
      if (accu_bits > 0) {
         classert(accu_bits < cut_bits);
         classert(i < n);
         v[bit_rev(i, depth)] = accu;
      }
   }
#ifdef CL_TEST_CODE
   void test_init() {
      depth = 3;
      n = (index)1 << depth;
      {
         small v[8];
         byte p[] = { 0, 1, 2, 3, 4, 5, 6, 7 };
         cut_bits = 8;
         init(v, (small*)p, sizeof(p) / sizeof(small));
         classert(v[0] == 0 && v[1] == 4 && v[2] == 2 && v[3] == 6 && v[4] == 1 && v[5] == 5 && v[6] == 3 && v[7] == 7);
      }
      {
         small v[8];
#define S(d,n) (d * (1ui64 << (3*n)))
         uint64 p = S(0,0) + S(1,1) + S(2,2) + S(3,3) + S(4,4) + S(5,5) + S(6,6) + S(7,7);
#undef S
         cut_bits = 3;
         init(v, (small*)&p, (3*8+sizeof(small)*8-1) / sizeof(small) / 8);
         classert(v[0] == 0 && v[1] == 4 && v[2] == 2 && v[3] == 6 && v[4] == 1 && v[5] == 5 && v[6] == 3 && v[7] == 7);
      }
      {
         small v[8];
#define S(d,n) (d * (1ui64 << (4*n)))
         uint64 p = S(0,0) + S(1,1) + S(2,2) + S(3,3);
#undef S
         cut_bits = 4;
         init(v, (small*)&p, (4*4+sizeof(small)*8-1) / sizeof(small) / 8);
         classert(v[0] == 0 && v[1] == 0 && v[2] == 2 && v[3] == 0 && v[4] == 1 && v[5] == 0 && v[6] == 3 && v[7] == 0);
      }
   }
#endif

   void reasm(const small *v) {
      // handle first element
      small accu = v[0];
      small hiaccu = 0;
      unsigned shift_bits = cut_bits;
      index s = 0;
      // handle all others
      for (index i=n-1; i>0; --i) {
         if (shift_bits == 0) {  // must check, since CPU might not shift at all, if shift bits are >= register width
            const small t = accu + v[i];
            if (t < accu) {
               ++hiaccu;
               classert(hiaccu != 0);  // no overflow
            }
            accu = t;
         }
         else {
            small t = accu + (v[i] << shift_bits);
            if (t < accu) {
               ++hiaccu;
               classert(hiaccu != 0);  // no overflow
            }
            accu = t;
            t = hiaccu + (v[i] >> (bits(small) - shift_bits));
            classert(t >= hiaccu);  // no overflow
            hiaccu = t;
         }
         shift_bits += cut_bits;
         if (shift_bits >= bits(small)) {
            r[s++] = accu;
            if (s >= alen+blen) break;
            accu = hiaccu;
            hiaccu = 0;
            shift_bits -= bits(small);
         }
      }
      if (s < alen+blen) {
         r[s++] = accu;
         if (s < alen+blen) {
            r[s++] = hiaccu;
            zero(r, alen+blen, s);
         }
      }
   }
#ifdef CL_TEST_CODE
   void test_reasm() {
      depth = 3;
      n = (index)1 << depth;
      const unsigned aalen = (2+sizeof(small)-1)/sizeof(small);
      const unsigned bblen = (2+sizeof(small)-1)/sizeof(small);
      *(small*)&alen = aalen;
      *(small*)&blen = bblen;
      union{
         small rr[aalen+bblen];
         uint64 r64;
      };
      r = rr;
      r64 = 0;
      memzero(rr);
      {
         cut_bits = 4;
         small v[] = { 0x11, 0, 0, 0, 0, 0x44, 0x33, 0x22 };
         reasm(v);
         const uint64 rc = 0x11 + (0x22<<4) + (0x33<<8) + (0x44<<12);
         classert(r64 == rc);
      }
   }
#endif

#ifdef CL_TEST_CODE
   static void test_all() {
      test_qmul_signed_sub();
      test_qmul_modinv();
      test_modadd();
      test_modsub();
      test_modmul();
      q_mul q_muler(false);
      q_muler.test_shuffle();
      q_muler.test_init();
      q_muler.test_reasm();
      q_muler.test_set_param();
#if 0
      q_muler.test_t_evaluate();
#endif
      q_muler.test_evaluate();

      //DTOUT_PUTS(format("qmul lltest: sizeof(small)=%?\n") << sizeof(small));
      auto old = switch_qsqu(always_on);
      selftest_lltest<small>(607, qsqu<small>);
      switch_qsqu(old);
   }
#endif
};

#pragma warning(pop)

template<typename small>
bool qmul_set_params(const index rlen, unsigned& depth, index& cut_bits) {
   return q_mul<small>::set_param2(rlen, depth, cut_bits);
}

template<typename small>
void qmul_print_params(const index bit) {
   string s;
   strformat ss(s);

   unsigned depth;
   index cut_bits;
   const index ll = (bit + bits(small) - 1) / bits(small);
   ss("length of each input is %'? words (%'? bits)\n") << ll << bit;
   if (!qmul_set_params<small>(2*ll, depth, cut_bits)) {
      ss("cannot use qmul() for this length!\n");
      return;
   }
   ss("fft size is %'? (2^%?) coefficients\n") << ((index)1 << depth) << depth;
   ss("cut_bits are %? bits\n") << cut_bits;
   uint64 muls = ((uint64)1 << depth) * depth;
   ss("that makes %'? total modular muls\n") << muls;
   tout_puts(s);
}

template<typename small>
small f_modmul(const small u, const small v) {
   return qmul_params<small>::modmul(u, v);
}

template<typename small>
small f_modadd(const small u, const small v) {
   return q_mul<small>::modadd(u, v);
}

template<typename small>
small f_modsub(const small u, const small v) {
   return q_mul<small>::modsub(u, v);
}


// unsigned multiplication of two const input numbers 'a' and 'b' into a pre-reserved result 'r'.
// 'r' must be long enough to hold the 'alen+blen' long result.  there is no padding of the result.
// length must be >= kmul_min_len, numbers don't need to be normalised

template<typename small, bool square>
void qmul_tsq(small* r, const small* a, index alen, const small* b, index blen) {
   caassert(!square || a == b);
   caassert(!square || alen == blen);

   caassert(r != nullptr);
   caassert(a != nullptr);
   caassert(b != nullptr);
   //caassert(alen >= blen);
   caassert((small)-1 > 0);  // must be unsigned type

   if (!use_qmul_tsq<square>(alen, blen)) {
      smul_tsq<small, square>(r, a, alen, square ? a : b, square ? alen : blen);
      return;
   }
   caassert(alen >= qmul_min_len);
   caassert(blen >= qmul_min_len);

   q_mul<small> q_muler(r, a, alen, b, blen, square);
   if (!q_muler.set_param()) {
      smul_tsq<small, square>(r, a, alen, square ? a : b, square ? alen : blen);
      return;
   }
   q_muler.qmul<square>();
}



#ifdef CL_TEST_CODE
static void test_qmul() {
   const index old = switch_qmul(always_on);
   {
      uint32 a[] = { 0x10, 0x11, 0x12, 0x13 };
      uint32 b[] = { 0x20, 0x21, 0x22, 0x23 };
      uint32 r[NBEL(a)+NBEL(b)];
      uint32 rc[NBEL(a)+NBEL(b)];

      omul(rc, a, NBEL(a), b, NBEL(a));
      qmul(r, a, NBEL(a), b, NBEL(a));

      if (memcmp(r, rc, sizeof(r)) != 0) {
         DTOUT_PUTS(format("is    =%?\n") << dump_hex(r, NBEL(r)));
         DTOUT_PUTS(format("should=%?\n") << dump_hex(rc, NBEL(r)));
         classertm(0, "qmul bug!");
      }
   }
   {
      uint32 a[] = { 0xffffff10, 0xffffff11, 0xffffff12, 0xffffff13 };
      uint32 b[] = { 0xffffff20, 0xffffff21, 0xffffff22, 0xffffff23 };
      uint32 r[NBEL(a)+NBEL(b)];
      uint32 rc[NBEL(a)+NBEL(b)];

      omul(rc, a, NBEL(a), b, NBEL(a));
      qmul(r, a, NBEL(a), b, NBEL(a));

      if (memcmp(r, rc, sizeof(r)) != 0) {
         DTOUT_PUTS(format("is    =%?\n") << dump_hex(r, NBEL(r)));
         DTOUT_PUTS(format("should=%?\n") << dump_hex(rc, NBEL(r)));
         classertm(0, "qmul bug!");
      }
   }
   {
      uint32 a[] = { 0xcfbd, 0x61ec, 0xdd8e, 0x68a3 };
      uint32 b[] = { 0xf6bf, 0x840b, 0x8ad0, 0x11f3, 1};
      uint32 r[NBEL(a)+NBEL(b)];
      uint32 rc[NBEL(a)+NBEL(b)];

      omul(rc, a, NBEL(a), b, NBEL(b));
      qmul(r, a, NBEL(a), b, NBEL(b));

      if (memcmp(r, rc, sizeof(r)) != 0) {
         DTOUT_PUTS(format("is    =%?\n") << dump_hex(r, 8));
         DTOUT_PUTS(format("should=%?\n") << dump_hex(rc, 8));
         classertm(0, "qmul bug!");
      }
   }
   // test different lengths
   {
      uint32 a[] = { 0xffffff10, 0xffffff11, 0xffffff12, 0xffffff13 };
      uint32 b[] = { 0xffffff20, 0xffffff21 };
      uint32 r[NBEL(a)+NBEL(b)];
      uint32 rc[NBEL(a)+NBEL(b)];

      omul(rc, a, NBEL(a), b, NBEL(b));
      qmul(r, a, NBEL(a), b, NBEL(b));

      if (memcmp(r, rc, sizeof(r)) != 0) {
         DTOUT_PUTS(format("is    =%?\n") << dump_hex(r, NBEL(rc)));
         DTOUT_PUTS(format("should=%?\n") << dump_hex(rc, NBEL(rc)));
         classertm(0, "qmul bug!");
      }
   }
#ifdef _M_X64
   {
      uint64 a[] = { 0x546b4c02, 0};
      uint64 r[2*NBEL(a)];
      uint64 rc[2*NBEL(a)];

      omul(rc, a, NBEL(a), a, NBEL(a));
      qmul(r, a, NBEL(a), a, NBEL(a));

      if (memcmp(r, rc, sizeof(r)) != 0) {
         DTOUT_PUTS(format("is    =%?\n") << dump_hex(r, NBEL(rc)));
         DTOUT_PUTS(format("should=%?\n") << dump_hex(rc, NBEL(rc)));
         classertm(0, "qmul bug!");
      }
   }
#endif
   switch_qmul(old);
}

static void test_qsqu() {
   const index old = switch_qsqu(always_on);
   {
      uint32 a[] = { 0x2702, 0x221f, 0xad42, 0xa94f };
      uint32 r[2*NBEL(a)];
      uint32 rc[2*NBEL(a)];

      osqu(rc, a, NBEL(a));
      qsqu(r, a, NBEL(a));

      if (memcmp(r, rc, sizeof(r)) != 0) {
         DTOUT_PUTS(format("is    =%?\n") << dump_hex(r, 8));
         DTOUT_PUTS(format("should=%?\n") << dump_hex(rc, 8));
         classertm(0, "qsqu bug!");
      }
   }
   {
      uint16 a[] = { 0x10, 0x11, 0x12, 0x13 };
      uint16 r[2*NBEL(a)];
      qsqu(r, a, NBEL(a));
      string s = dump_hex(r, NBEL(r));
      classert(s == "0x16902ac03ca04c4036102200100");
   }
   {
      uint16 a[] = { 0x10, 0x11 };
      uint16 r[2*NBEL(a)];
      qsqu(r, a, NBEL(a));
      string s = dump_hex(r, NBEL(r));
      classert(s == "0x12102200100");
   }
   {
      uint16 a[] = { 0x10, 0x11, 0xff, 0xfe, 1, 2, 3, 4, 5, 6, 0x55, 0xaa };
      uint16 r[2*NBEL(a)];
      qsqu(r, a, NBEL(a));
      string s = dump_hex(r, NBEL(r));
      classert(s == "0x70e470e424310aa008c606e004ef02f55249fb90cbf5367e1d5c0f5a0b1b06defea7fa671fdd419e210102200100");
   }
   {
      // this is iteration 7 of LL-test of M521
      uint32 a[] = { 0xf03d3002, 0x1bd696d9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
      uint32 r[2*NBEL(a)];
      uint32 rc[2*NBEL(a)];
      qsqu(r, a, NBEL(a));
      osqu(rc, a, NBEL(a));
      string s = dump_hex(r, NBEL(r));
      string sc = dump_hex(rc, NBEL(rc));
      classert(s == sc);
   }
   {
      // this is something else, but failed as well
      uint32 a[] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0xf03d3002, 0x1bd696d9 };
      uint32 r[2*NBEL(a)];
      uint32 rc[2*NBEL(a)];
      qsqu(r, a, NBEL(a));
      osqu(rc, a, NBEL(a));
      string s = dump_hex(r, NBEL(r));
      string sc = dump_hex(rc, NBEL(rc));
      classert(s == sc);
   }
   switch_qsqu(old);
}
#endif


#ifdef CL_TEST_CODE
void _CL_CALL test_bignum_qmul() {
   q_mul<uint16>::test_all();
   q_mul<uint32>::test_all();
#ifdef _M_X64
   q_mul<uint64>::test_all();
#endif
   test_qmul();
   test_qsqu();
}
#endif


// ------ generate template functions -----------------------------------------

//#if _MSC_VER < 1300
#pragma inline_depth(0)

uint8* q_mul<uint8>::pre_w[qmul_params<uint8>::fft_depth];
uint16* q_mul<uint16>::pre_w[qmul_params<uint16>::fft_depth];
uint32* q_mul<uint32>::pre_w[qmul_params<uint32>::fft_depth];
#ifdef _M_X64
uint64* q_mul<uint64>::pre_w[qmul_params<uint64>::fft_depth];
#endif

uint64 dummy_global1;

template<typename small>
void generate_bignum_qmuls() {
   qmul<small>(0, 0, 0, 0, 0);
   qsqu<small>(0, 0, 0);
   qmul_print_params<small>(0);
   dummy_global1 = f_modmul<small>(0, 0);
   dummy_global1 = f_modadd<small>(0, 0);
   dummy_global1 = f_modsub<small>(0, 0);
   // static index dummy_global = kmul_mem<small, true>(*(index*)0, 0) + kmul_mem<small, false>(*(index*)0, 0);
}

void _generate_bignum_qmuls() {
   classertm(0, "must not be called");
   generate_bignum_qmuls<uint8>();
   generate_bignum_qmuls<uint16>();
   generate_bignum_qmuls<uint32>();
#ifdef _M_X64
   generate_bignum_qmuls<uint64>();
#endif
}

#pragma inline_depth()
//#endif

}  // namespace bignum
