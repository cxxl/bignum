// $+HEADER$
// 
// Copyright 2015 Christoph Lueders
// 
// This file is part of the BIGNUM Library.
// 
// The BIGNUM Library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
// 
// The BIGNUM Library is distributed in the hope that it will be useful, but 
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public 
// License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with the BIGNUM Library.  If not, see <http://www.gnu.org/licenses/>.
// 
// $-HEADER$

// $Date: 2015-04-24 11:28:16 +0200 (Fr, 24 Apr 2015) $
// $Rev: 26590 $

/*
 *  arbitrary length number arithmetic - processor specific base_crossover_data data
 */

#include "precomp.h"
#include "impl.h"
#include "smul.h"
#include "cl/format.h"
#include "cl/cpu.h"
#include "cl/timefunc.h"
#include "cl/str_trim.h"
#include "cl/str_conv.h"
#include "cl/osver.h"


// we must switch off "unreachable code" warning, since bool template parameters generate code paths that are never used
#pragma warning (disable:4702)
// warning C4065: switch statement contains 'default' but no 'case' labels
#pragma warning (disable:4065)

namespace bignum {


// ------ default init values

// must be inited before smul() can be used
#pragma warning(disable: 4075)
#pragma init_seg(".CRT$XCM_BIGNUM_K")

// omul

#ifdef _M_X64
uint64 (*add_ptr)(uint64* r, const uint64* a, const uint64* b, const uint64 len) = add8_asmx64;
uint64 (*sub_ptr)(uint64* r, const uint64* a, const uint64* b, const uint64 len) = sub8_asmx64;
void (*omul_ptr)(uint64* r, const uint64* a, const index alen, const uint64* b, const index blen) = omul4_asmx64;
void (*osqu_ptr)(uint64* r, const uint64* a, const index alen) = osqu4_asmx64;
#endif


// kmul

#ifdef _M_X64
#define KMUL_THRESH 28  // measured with -MKKK -a7 on ZIR
#define KSQU_THRESH 52  // measured with -SKKK -a7 on ZIR
#else
// AMD Athlon 64 3500+ had a kmul_thresh of 40 when a Core2 Duo T7300 had one of 36
// Intel Core Duo T2450 @ 2.00 GHz had a kmul_thresh of 32 when a Core2 Duo T7300 had one of 36
#define KMUL_THRESH 32  // measured with -MKK -a7 on ZIR
#define KSQU_THRESH 28  // measured with -SKK -a7 on ZIR
#endif

index kmul_thresh_init = KMUL_THRESH;
index kmul_thresh = kmul_thresh_init;
index ksqu_thresh_init = KSQU_THRESH;
index ksqu_thresh = ksqu_thresh_init;


// t3mul

#ifdef _M_X64
#define T3MUL_THRESH 152  // measured with -MTT -a7 on ZIR
#define T3SQU_THRESH 184  // measured with -STT -a7 on ZIR
#else
#define T3MUL_THRESH 144  // measured with -MTT -a7 on ZIR
#define T3SQU_THRESH 96  // measured with -STT -a7 on ZIR
#endif

index t3mul_thresh_init = T3MUL_THRESH;
index t3mul_thresh = t3mul_thresh_init;
index t3squ_thresh_init = T3SQU_THRESH;
index t3squ_thresh = t3squ_thresh_init;


// qmul

#define QMUL_THRESH always_off
#define QSQU_THRESH always_off

index qmul_thresh_init = QMUL_THRESH;
index qmul_thresh = qmul_thresh_init;
index qsqu_thresh_init = QSQU_THRESH;
index qsqu_thresh = qsqu_thresh_init;


// smul/ssqu

#ifdef _M_X64
#define SMUL_THRESH 2465  // measured on ZIR with -MSSSS
#define SSQU_THRESH 2241   // measured on ZIR with -SSS
#else
#define SMUL_THRESH 2699  // measured on ZIR with -MS
#define SSQU_THRESH 1521  // measured on ZIR with -SS
#endif

index smul_thresh_init = SMUL_THRESH;
index smul_thresh = smul_thresh_init;
index ssqu_thresh_init = SSQU_THRESH;
index ssqu_thresh = smul_thresh_init;

static index smul_init_vals[] = {
   2*1120,  // 6
   2*2496,  // 7
   2*6016,  // 8
   2*16128,  // 9
   2*294400,  // 10
   2*818176,  // 11
   2*4323328,  // 12
   2*13103104,  // 13
   2*36478976,  // 14
   2*145440768,  // 15
   0 };
choose_crossover_data smul_calib("smul", 6, smul_init_vals);
static index ssqu_init_vals[] = {
   2*1120,  // 6
   2*2496,  // 7
   2*5504,  // 8
   2*16128,  // 9
   2*48640,  // 10
   2*97280,  // 11
   0 };
choose_crossover_data ssqu_calib("ssqu", 6, ssqu_init_vals);


// new smul_mod/ssqu_mod calibration

smul_calib_item _smul_mod_lg_packed[] = {
#ifdef _M_X64
   { 0, nullptr },  // 0
   { 0, nullptr },  // 1
   { 0, nullptr },  // 2
   { 0, nullptr },  // 3
   { 352, nullptr },  // 4
   { 480, nullptr },  // 5
   { 960, nullptr },  // 6
   { 2432, "7676" },  // 7 (starting 1920)
   { 5376, "87" },  // 8 (starting 4864)
   { 12800, "988998" },  // 9 (starting 9728)
   { 34816, "aa9999aaaa99" },  // 10 (starting 22528)
   { 139264, "bbbbaaaaaaaabbbbbbbbcacacbcbcbcbcbcbbaaa" },  // 11 (starting 57344)
   { 196608, "ccccccccbabbbbbbbbbbbbbb" },  // 12 (starting 98304)
#else
   { 0, nullptr },  // 0
   { 0, nullptr },  // 1
   { 0, nullptr },  // 2
   { 0, nullptr },  // 3
   { 400, "40" },  // 4 (starting 368)
   { 480, nullptr },  // 5
   { 1088, "65" },  // 6 (starting 960)
   { 2688, "767676" },  // 7 (starting 1920)
   { 6400, "877887" },  // 8 (starting 4864)
   { 17408, "998888999988" },  // 9 (starting 11264)
   { 40960, "aaaa99999999" },  // 10 (starting 28672)
#endif
   { -1, 0 }
};
smul_calib_data smul_mod_lg(_smul_mod_lg_packed);

smul_calib_item _ssqu_mod_lg_packed[] = {
#ifdef _M_X64
   { 0, nullptr },  // 0
   { 0, nullptr },  // 1
   { 0, nullptr },  // 2
   { 0, nullptr },  // 3
   { 352, nullptr },  // 4
   { 480, nullptr },  // 5
   { 960, nullptr },  // 6
   { 2432, nullptr },  // 7
   { 5376, "87" },  // 8 (starting 4864)
   { 11264, "988" },  // 9 (starting 9728)
   { 34816, "aa9999aaaa99" },  // 10 (starting 22528)
   { 106496, "bbbbaaaaaaaabbbbbbbbcaca" },  // 11 (starting 57344)
   { 196608, "ccccccccbbbbbbbbbbbbbbbb" },  // 12 (starting 98304)
#else
   { 0, nullptr },  // 0
   { 0, nullptr },  // 1
   { 0, nullptr },  // 2
   { 0, nullptr },  // 3
   { 208, nullptr },  // 4
   { 288, nullptr },  // 5
   { 704, nullptr },  // 6
   { 1664, "76" },  // 7 (starting 1408)
   { 4352, "877887" },  // 8 (starting 2816)
   { 13312, "9988889999a8" },  // 9 (starting 7168)
   { 36864, "aaaa99999999aaaaaaaa9999" },  // 10 (starting 12288)
#endif
   { -1, 0 }
};
smul_calib_data ssqu_mod_lg(_ssqu_mod_lg_packed);


// ------ set proper thresholds for CPUs

#define _UNPACK(a, ...) a, __VA_ARGS__
#define FFT_PARM(kthn,kth,t3thn,t3th,thn,th,modn,modl,modpp,muln,col,copp) { kthn = kth; t3thn = t3th; thn = th; static const index modx[] = {_UNPACK modpp}; modn.set_init(modl, modx); static const index cox[] = {_UNPACK copp}; muln.set_init(col, cox); }
#define MUL_PARMS(kth,t3th,th,modl,modpp,col,copp) FFT_PARM(kmul_thresh_init, kth, t3mul_thresh_init, t3th, smul_thresh_init, th, smul_mod_calib, modl, modpp, smul_calib, col, copp)
#define SQU_PARMS(kth,t3th,th,modl,modpp,col,copp) FFT_PARM(ksqu_thresh_init, kth, t3squ_thresh_init, t3th, ssqu_thresh_init, th, ssqu_mod_calib, modl, modpp, ssqu_calib, col, copp)

// table contains the highest value that should be used with the respective fft size

bool _CL_CALL set_thresholds() {
   const unsigned man = cpuid.is_intel() ? 1 : cpuid.is_amd() ? 2 : 0;
   const unsigned id = (cpuid.get_family() << 8) | cpuid.get_model();
   const unsigned both = (man << 16) | id;
   bool ret = true;

   switch (both) {
   default:
      ret = false;
      // fall-thru
#ifdef _M_X64
//   case 0x1063a:  // Intel(R) Core(TM) i7-3770 CPU @ 3.40GHz, ID: 06_3ah (ZIR)
//      MUL_PARMS(30, 160, 2230, 4, (     400, 416, 576, 896, 1280, 2560, 5120, 10240, 0), 2, (2*72, 2*124, 2*202, 2*398, 2*1040, 2*2240,  2*3968, 2*12480, 2*32256, 2*80896, 2*292864, 0));
//      SQU_PARMS(50, 160, 1674, 3, (544, 272, 288, 448, 768, 1280, 2560, 5120, 10240, 0), 2, (2*72,  2*80, 2*182, 2*398,  2*856, 2*1848,  2*3968, 2*10304, 2*32256, 2*80896, 2*292864, 0));
//      break;
//   case 0x10617:  // Intel(R) Core(TM)2 Quad CPU Q9550 @ 2.83GHz, ID: 06_17h (ZIR)
//      MUL_PARMS(25, 124, 2193, 4, (     368, 384, 512, 768, 1280, 2560, 5120, 10240, 0), 2, (2*72, 2*108, 2*200, 2*432,  2*864, 2*1984,  2*4992, 2*10752, 2*26112, 2*80896, 2*292864, 0));
//      SQU_PARMS(45, 124, 1665, 4, (     224, 288, 384, 640, 1280, 2560, 5120, 10240, 0), 2, (2*88, 2*184, 2*336, 2*736, 2*1600, 2*3968, 2*12032, 2*34304, 2*97280, 0));
//      break;
//   case 0x1062a:  // Intel(R) Core(TM) i5-2500 CPU @ 3.30GHz, ID: 06_2ah (sandy bridge, karsten)
//      MUL_PARMS(30,  96, 2961, 4, (     368, 448, 576, 896, 1280, 2560, 5120, 10240, 0), 2, (2*72, 2*108, 2*200, 2*432,  2*864, 2*1984,  2*4992, 2*10752, 2*26112, 2*80896, 2*292864, 0));
//      SQU_PARMS(50,  96, 1674, 4, (     240, 256, 448, 640, 1280, 2560, 5120, 10240, 0), 2, (2*72,  2*80, 2*168, 2*320,  2*672, 2*1408,  2*3968, 2*10496, 2*40448, 2*80896, 2*292864, 0));
//      break;
//   case 0x21006:  // AMD Athlon(tm) II X2 215 Processor, ID: 10_06h (mario)
//      MUL_PARMS(30, 200, 2561, 3, (296, 352, 448, 576, 896, 0                         ), 2, (2*72, 2*108, 2*200, 2*432,  2*864, 2*1984,  2*4992, 2*10752, 2*26112, 2*80896, 2*292864, 0));
//      SQU_PARMS(50, 200, 1537, 3, (544, 288, 320, 448, 0                              ), 2, (2*72,  2*80, 2*168, 2*320,  2*672, 2*1408,  2*3968, 2*10496, 2*40448, 2*80896, 2*292864, 0));
//      break;
#else
//   case 0x10617:  // Intel(R) Core(TM)2 Quad CPU Q9550 @ 2.83GHz, ID: 06_17h (ZIR)
//      MUL_PARMS(40, 224, 2587, 4, (     224, 288, 448, 640, 0), 2, (2*118, 2*174, 2*234, 2*508, 2*1094, 2*2137, 2*6717, 2*14404, 2*41107, 2*171729, 2*592870, 0));
//      SQU_PARMS(74, 256, 1801, 3, (664, 208, 288, 384, 640, 0), 2, ( 2*72,  2*80, 2*168, 2*320,  2*672, 2*1408, 2*3968, 2*10496, 2*40448,  2*80896, 2*292864, 0));
//      break;
//   case 0x1060f:  // Intel(R) Core(TM)2 Duo CPU T7300 @ 2.00GHz, ID: 06_0fh (FERMAT)
//      MUL_PARMS(40, 160, 2587, 4, (     352, 384, 512, 768, 0), 2, (2*118, 2*174, 2*234, 2*508, 2*1094, 2*2137, 2*6717, 2*14404, 2*41107, 2*171729, 2*592870, 0));
//      SQU_PARMS(80, 320, 1801, 3, (664, 208, 288, 384, 640, 0), 2, ( 2*72,  2*80, 2*168, 2*320,  2*672, 2*1408, 2*3968, 2*10496, 2*40448,  2*80896, 2*292864, 0));
//      break;
#endif
   }

   kmul_thresh = kmul_thresh_init;
   t3mul_thresh = t3mul_thresh_init;
   ksqu_thresh = ksqu_thresh_init;

   smul_thresh = smul_thresh_init;
   t3squ_thresh = t3squ_thresh_init;
   ssqu_thresh = ssqu_thresh_init;

   return ret;
}



// ------ manage value[lg size]

base_crossover_data::base_crossover_data(const string _name, const index _low_value, const index _high_value, const unsigned start, const index* vals) :
   name(_name), low_value(_low_value), high_value(_high_value)
{
   set_init(start, vals);
}

void base_crossover_data::set_init(const unsigned start, const index* vals) {
   set_vec(init_thresh, start, vals);
   init();
}

void base_crossover_data::clear() {
   thresh.clear();
   thresh.resize(bits(index), high_value);
}

void base_crossover_data::set_vec(vector<index>& th, const unsigned start, const index* vals) {
   th.clear();
   th.resize(bits(index), high_value);
   // count values
   unsigned len = 0;
   while (vals[len]) ++len;
   // copy provided values
   unsigned i = 0;
   while (i < start) {
      th[i] = low_value;
      ++i;
   }
   while (i < th.size() && i < start + len) {
      th[i] = vals[i - start];
      ++i;
   }
   // set high values
   if (high_value == -1) {  // otherwise high_value will be set for the high values
      const index last = th[i-1];
      while (i < th.size()) {
         th[i] = last;
         ++i;
      }
   }
}

void base_crossover_data::init() {
   thresh = init_thresh;
}

vector<index> base_crossover_data::set(const vector<index>& vals) {
   const vector<index> old = thresh;
   thresh = vals;
   return old;
}

string base_crossover_data::setting_string() const {
   string s;
   strformat ss(s);
   index old = low_value;
   for (size_t i=0; i<thresh.size(); ++i) {
      if (thresh[i] == low_value) continue;
      if (thresh[i] == high_value) break;
      if (thresh[i] != old) {
         if (!s.empty()) s += ", ";
         ss("[%?] = %?") << i << thresh[i];
         old = thresh[i];
      }
   }
   if (s.empty()) s = "never";
   return name + ": " + s;
}

string base_crossover_data::setting_string_short() const {
   string s;
   strformat ss(s);
   size_t i = 0;
   while (i < thresh.size() && thresh[i] == low_value) ++i;
   ss("[%?..] = ") << i;
   index old = low_value;
   while (i < thresh.size() && thresh[i] != high_value) {
      if (thresh[i] != old) {
         if (old != low_value) s += ", ";
         ss("%?") << thresh[i];
         old = thresh[i];
      }
      ++i;
   }
   if (s.empty()) s = "never";
   return name + ": " + s;
}


// ------ mod_crossover_data

vector<index> mod_crossover_data::set_val(const index val) {
   const vector<index> old = thresh;
   thresh.clear();
   thresh.resize(bits(index), val);
   return old;
}


// ------ choose_crossover_data

unsigned choose_crossover_data::choose(const index rlen) {
   index i = 2;  // can never be less than 2
   while (thresh[i] > 0) {
      // keep on looping as long as table entry is smaller or equal than rlen
      if (thresh[i] >= rlen) break;
      ++i;
   }
   return i;
}


// ------ change thresholds

index _CL_CALL switch_smul(index set) {
   const index r = smul_thresh;
   smul_thresh = tmax(set, (index)32);
   return r;
}

index _CL_CALL switch_ssqu(index set) {
   const index r = ssqu_thresh;
   ssqu_thresh = tmax(set, (index)32);
   return r;
}


// ------ new calib for smul_mod

void smul_calib_data::init_packed(const index *packed_data) {
   size_to_lg.clear();
   packed_lg_sizes.clear();
   while (*packed_data != (index)-1) {
      packed_lg_sizes.push_back(smul_calib_item2(*packed_data));
      ++packed_data;
   }
}

void smul_calib_data::init_packed_items(const smul_calib_item* items) {
   size_to_lg.clear();
   packed_lg_sizes.clear();
   while (items->full != (index)-1) {
      smul_calib_item2 ci;
      ci.start = ci.full = items->full;
      const index pow2 = (index)1 << packed_lg_sizes.size();
      if (items->before != 0) {
         for (index i=0; items->before[i] != 0; ++i) {
            caassert(ci.start >= pow2);
            ci.start -= pow2;
            unsigned u = 0;
            CL_IF_DEBUG(bool r =,) cton(36, items->before[i], u);
            classert(r);
            ci.between.push_back(u);
         }
      }
      packed_lg_sizes.push_back(ci);
      ++items;
   }
}

string smul_calib_data::print(const string& name) const {
   string s;
   strformat ss(s);
   ss("const index %?_step = %?;\n") << name << stepsz;
   size_t i = 0;
   for (; i<size_to_lg.size(); ++i)
      if (size_to_lg[i] != 0) break;
   ss("const index %?_start = %?;\n") << name << i*stepsz;
   ss("const byte %?_data[] = {\n") << name;
   const size_t pl = 32;
   if (i % pl != 0) s += "  ";
   for (; i<size_to_lg.size(); ++i) {
      if (i % pl == 0) s += "  ";
      ss("%2?,") << size_to_lg[i];
      if (i % pl == pl-1) s += "\n";
   }
   s += "-1";
   if (i % pl != 0) s += "\n";
   ss("};\n");
   return s;
}

unsigned smul_calib_data::get_lg(const index sz) const {
   classert(sz != 0);
   // first check full array
   if (!size_to_lg.empty()) {
      if ((sz & (stepsz - 1)) != 0) return 0;
      return size_to_lg[size_to_lg.size() < sz/stepsz ? size_to_lg.size()-1 : sz/stepsz];
   }
   // then check packed array
   if (!packed_lg_sizes.empty()) {
      // find max power of 2 in sz
      unsigned lg = trail_zero_cnt(sz);
      // cycle from top through packed_lg_sizes and look for first entry which is <= sz
      if (lg >= (unsigned)packed_lg_sizes.size()) lg = (unsigned)packed_lg_sizes.size() - 1;
      while (lg != 0) {
         const auto& pls = packed_lg_sizes[lg];
         if (pls.full == 0) break;  // so low that we must use classical algorithm
         if (sz >= pls.start) {
            if (sz >= pls.full) return lg;
            const index pow2 = (index)1 << lg;
            caassert((sz - pls.start) % pow2 == 0);
            classert((pls.full - pls.start) / pow2 == pls.between.size());
            return pls.between[(sz - pls.start) / pow2];
         }
         --lg;
      }
      return 0;
   }
   return 0;
}

string smul_calib_data::pack_table(const string& name) {
   string s;
   strformat ss(s);

   index i = 0;

   // skip zeros at the beginning
   while (i < size_to_lg.size()) {
      if (size_to_lg[i] != 0) break;
      ++i;
   }
   if (i >= size_to_lg.size()) return "no data";  // only zeros
   const index start_thresh = i*stepsz;  // first size where smul_mod() is faster than t3mul()
   packed_lg_sizes.clear();

   byte max_lg = 0;
   for (auto j = i; j<size_to_lg.size(); ++j)
      max_lg = tmax(max_lg, size_to_lg[j]);
   unsigned last_lg = -1;
   unsigned lg = int_log2(stepsz);
   ss("smul_calib_item %?_packed[] = {\n") << name;
   // cycle through all logs found in the table
   bool found;
   do {
      found = false;
      const index inc = (index)1 << lg;
      i = (start_thresh + inc - 1)/inc*inc/stepsz;
      index first = 0, last = 0;
      while (i < size_to_lg.size()) {
         if (size_to_lg[i] == lg) {
            if (!found) {
               found = true;
               first = last = i*stepsz;
            }
         }
         else if (found && size_to_lg[i] < lg) {  // something else was found after the first match
            last = i*stepsz + inc;
         }
         i = (i*stepsz + inc) / stepsz;
      }
      if (found) {
         for (unsigned j=last_lg+1; j<lg; ++j)
            ss("   { 0, nullptr },  // %?\n") << j;
         ss("   { %?, ") << last;
         if (packed_lg_sizes.size() <= lg) packed_lg_sizes.resize(lg+1);
         packed_lg_sizes[lg] = first;
         if (first != last) {
            s += "\"";
            for (auto j=first; j<last; j+=(index)1<<lg)
               ss("%z") << size_to_lg[j/stepsz];
            s += "\"";
         }
         else s += "nullptr";
         ss(" },  // %?") << lg;
         if (first != last) ss(" (starting %?)") << first;
         s += "\n";
         last_lg = lg;
      }
      ++lg;
   } while (lg <= max_lg);
   ss("   { -1, nullptr }\n};\n");
   return s;
}

#ifdef CL_TEST_CODE
static void test_pack_table() {
   smul_calib_data d;
   byte a[] = {
      0, 0, 0, 0, 0, 0, 0, 0,    // off 0
      2, 1, 2, 1, 2, 1, 2, 1,    // off 16
      3, 1, 2, 1, 3, 1, 2, 1,    // off 32
      3, 1, 2, 1, 3, 1, 2, 1,    // off 48
      4, 1, 2, 1, 3, 1, 2, 1,    // off 64
      4, 1, 2, 1, 3, 1, 2, 1,    // off 80
      4, 1, 2, 1, 3, 1, 2, 1,    // off 96
      4, 1, 2, 1, 3, 1, 2, 1,    // off 112
      5, 1, 2, 1, 3, 1, 2, 1,    // off 128
      -1,
   };
   d.init(2, 0, a);
   d.pack_table("x");
   classert(d.packed_lg_sizes[0].full == 0);
   classert(d.packed_lg_sizes[1].full == 18);
   classert(d.packed_lg_sizes[2].full == 16);
   classert(d.packed_lg_sizes[3].full == 32);
   classert(d.packed_lg_sizes[4].full == 64);
   classert(d.packed_lg_sizes[5].full == 128);

   smul_calib_item items[] = {
      { 0, 0 },  // 0
      { 0, 0 },  // 1
      { 0, 0 },  // 2
      { 0, 0 },  // 3
      { 336, 0 },  // 4
      { 480, 0 },  // 5
      { 960, 0 },  // 6
      { 2432, 0 },  // 7
      { 5376, "87" },  // 8
      { 11264, "988" },  // 9
      { 34816, "aa9999aaaa99" },  // 10
      { 106496, "bbbbaaaaaaaabbbbbbbbcaca" },  // 11
      { -1, 0 }
   };
   d.init_packed_items(items);
   classert(d.get_lg(328) == 0);
   classert(d.get_lg(336) == 4);
   classert(d.get_lg(352) == 4);
   classert(d.get_lg(448) == 4);
   classert(d.get_lg(480) == 5);
   classert(d.get_lg(4608) == 7);
   classert(d.get_lg(4864) == 8);
   classert(d.get_lg(5120) == 7);
   classert(d.get_lg(5376) == 8);
}
#endif

/*string smul_calib_data::print_packed(const string& name) const {
   string s;
   strformat ss(s);
   ss("index %?_packed[] = {\n") << name;
   for (size_t i=0; i<packed_lg_sizes.size(); ++i) {
      ss("   %?,  // %?\n") << (packed_lg_sizes[i] == 0 ? 1 : packed_lg_sizes[i]) << i;
   }
   ss("   0\n};\n");
   return s;
}*/

string smul_calib_data::print_short(const string& name) const {
   if (!size_to_lg.empty()) return format("%?: individual data") << name;
   string s;
   strformat ss(s);
   size_t i = 0;
   while (i < packed_lg_sizes.size() && packed_lg_sizes[i].full == 0) ++i;
   auto first = i;
   while (i < packed_lg_sizes.size()) {
      if (i != first) s += ", ";
      ss("%?") << packed_lg_sizes[i].full;
      ++i;
   }
   if (s.empty()) return format("%?: never use") << name;
   return format("%?[%?..] = %?") << name << first << s;
}


// ------ status --------------------------------------------------------------

string _CL_CALL omul_routine_name() {
   return
#ifdef _M_X64
      omul_ptr == omul_plain<uint64> ? "omul_plain" :
      omul_ptr == omul_unrolled<uint64> ? "omul_unrolled" :
#if defined _M_X64 && defined BN_OMUL_USE_ASMX64
      omul_ptr == omul_asmx64 ? "omul_asmx64" :
#endif
#if defined _M_X64 && defined BN_OMUL2_USE_ASMX64
      omul_ptr == omul2_asmx64 ? "omul2_asmx64" :
#endif
#if defined _M_X64 && defined BN_OMUL3_USE_ASMX64
      omul_ptr == omul3_asmx64 ? "omul3_asmx64" :
#endif
#if defined _M_X64 && defined BN_OMUL4_USE_ASMX64
      omul_ptr == omul4_asmx64 ? "omul4_asmx64" :
#endif
      "???";
#else
      "omul_unrolled";
#endif
}

string _CL_CALL osqu_routine_name() {
   return
#ifdef _M_X64
      osqu_ptr == osqu_plain<uint64> ? "osqu_plain" :
#if defined _M_X64 && defined BN_OSQU4_USE_ASMX64
      osqu_ptr == osqu4_asmx64 ? "osqu4_asmx64" :
#endif
#if defined _M_X64 && defined BN_OMUL_USE_ASMX64
      osqu_ptr == osqu_omul_asmx64 ? "omul_asmx64" :
#endif
#if defined _M_X64 && defined BN_OMUL2_USE_ASMX64
      osqu_ptr == osqu_omul2_asmx64 ? "omul2_asmx64" :
#endif
#if defined _M_X64 && defined BN_OMUL3_USE_ASMX64
      osqu_ptr == osqu_omul3_asmx64 ? "omul3_asmx64" :
#endif
#if defined _M_X64 && defined BN_OMUL4_USE_ASMX64
      osqu_ptr == osqu_omul4_asmx64 ? "omul4_asmx64" :
#endif
      "???";
#else
      "osqu_plain";
#endif
}

string cp_str(index i) {
   if (i == always_off) return "off";
   return format("%?, %? bits") << i << i * bits(limb);
}

/*string string_priority_class() {
   const DWORD c = GetPriorityClass(GetCurrentProcess());
   const int p = GetThreadPriority(GetCurrentThread());
   const int* class_prio = nullptr;
   switch (c) {
      case IDLE_PRIORITY_CLASS:         class_prio = [1, 2, 3, 4, 5, 6, 15]; break;     
      case BELOW_NORMAL_PRIORITY_CLASS: class_prio = []; break;     
      case NORMAL_PRIORITY_CLASS:       class_prio = []; break;     
      case ABOVE_NORMAL_PRIORITY_CLASS: class_prio = []; break;     
      case HIGH_PRIORITY_CLASS:         class_prio = []; break;     
      case REALTIME_PRIORITY_CLASS:     class_prio = []; break;     
   }
   return format("%#x") << c;
}*/

string string_priority_class() {
   const DWORD c = GetPriorityClass(GetCurrentProcess());
   switch (c) {
      case IDLE_PRIORITY_CLASS:              return "IDLE_PRIORITY_CLASS";
      case BELOW_NORMAL_PRIORITY_CLASS:      return "BELOW_NORMAL_PRIORITY_CLASS";
      case NORMAL_PRIORITY_CLASS:            return "NORMAL_PRIORITY_CLASS";
      case ABOVE_NORMAL_PRIORITY_CLASS:      return "ABOVE_NORMAL_PRIORITY_CLASS";
      case HIGH_PRIORITY_CLASS:              return "HIGH_PRIORITY_CLASS";
      case REALTIME_PRIORITY_CLASS:          return "REALTIME_PRIORITY_CLASS";
   }
   return format("%#x") << c;
}

string string_thread_priority() {
   const int p = GetThreadPriority(GetCurrentThread());
   switch (p) {
      case THREAD_PRIORITY_IDLE:             return "THREAD_PRIORITY_IDLE";
      case THREAD_PRIORITY_LOWEST:           return "THREAD_PRIORITY_LOWEST";
      case THREAD_PRIORITY_BELOW_NORMAL:     return "THREAD_PRIORITY_BELOW_NORMAL";
      case THREAD_PRIORITY_NORMAL:           return "THREAD_PRIORITY_NORMAL";
      case THREAD_PRIORITY_ABOVE_NORMAL:     return "THREAD_PRIORITY_ABOVE_NORMAL";
      case THREAD_PRIORITY_HIGHEST:          return "THREAD_PRIORITY_HIGHEST";
      case THREAD_PRIORITY_TIME_CRITICAL:    return "THREAD_PRIORITY_TIME_CRITICAL";
   }
   return format("%d") << p;
}

string _CL_CALL system_info() {
   string s;
   strformat ss(s);
   ss("Worker \"%?\", directory \"%?\"\n") << worker_name << working_directory;
   ss("Command line: %?\n") << command_line;
   ss("Start time: %?") << start_datetime;
   if (run_proc != -1) ss("; running on processor %?") << run_proc;
   s += "\n";
   ss("Time source: %?") << cyc.time_source();
   ss("; priority %?, %?\n") << string_priority_class() << string_thread_priority();
   string feats;
#ifdef HT_DETECT
   if (cpuid.is_htt_active()) feats += "HT active, ";
#endif
   if (!feats.empty()) feats = " (" + feats.substr(0, feats.size()-2) + ")";
   ss("CPU: %?, ID: %?, %#,.3? GHz%?, %? cores\n") << ltrimstr(cpuid.brand_string()) << cpuid.cpuid() << clock_rate/1000000 << feats << osver.procs();
   string s2;
#ifdef _M_X64
   s2 = format("omul_ptr: %?") << omul_routine_name();
   ss("%-35?  osqu_ptr: %?\n") << s2 << osqu_routine_name();
#endif
   s2 = format("kmul: %?") << cp_str(kmul_thresh);
   ss("%-35?  ksqu: %?\n") << s2 << cp_str(ksqu_thresh);
   s2 = format("t3mul: %?") << cp_str(t3mul_thresh);
   ss("%-35?  t3squ: %?\n") << s2 << cp_str(t3squ_thresh);
   s2 = format("smul: %?") << cp_str(smul_thresh);
   ss("%-35?  ssqu: %?\n") << s2 << cp_str(ssqu_thresh);
   ss("%?\n") << smul_mod_lg.print_short("smul_mod");
   ss("%?\n") << smul_calib.setting_string_short();
   ss("%?\n") << ssqu_mod_lg.print_short("ssqu_mod");
   ss("%?\n") << ssqu_calib.setting_string_short();
   s2 = format("qmul: %?") << cp_str(qmul_thresh);
   ss("%-35?  qsqu: %?\n") << s2 << cp_str(qsqu_thresh);
   s2 = format("dkss_mul: %?") << cp_str(dkss_mul_thresh);
   ss("%-35?  dkss_squ: %?\n") << s2 << cp_str(dkss_squ_thresh);
   //ss("index has %? bits\n") << bits(index);
   //ss("%? bit process\n")  << (osver.is_x64() ? 64 : 32);
   return s;
}


#ifdef CL_TEST_CODE
void _CL_CALL test_calibrate() {
   test_pack_table();
}
#endif

}  // namespace bignum
