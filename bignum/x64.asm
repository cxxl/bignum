; $+HEADER$
; 
; Copyright 2015 Christoph Lueders
; 
; This file is part of the BIGNUM Library.
; 
; The BIGNUM Library is free software: you can redistribute it and/or modify
; it under the terms of the GNU Lesser General Public License as published by
; the Free Software Foundation, either version 3 of the License, or (at your 
; option) any later version.
; 
; The BIGNUM Library is distributed in the hope that it will be useful, but 
; WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
; or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public 
; License for more details.
; 
; You should have received a copy of the GNU Lesser General Public License
; along with the BIGNUM Library.  If not, see <http://www.gnu.org/licenses/>.
; 
; $-HEADER$

; $Date: 2015-04-24 11:28:16 +0200 (Fr, 24 Apr 2015) $
; $Rev: 26590 $


_TEXT SEGMENT

; ------ DIVISION -------------------------------------------------------------

   align 16

; uint64 __fastcall divq_asmx64(uint64 a1, uint64 a0, uint64 b, uint64& r)

?divq_asmx64@bignum@@YA_K_K00AEA_K@Z PROC

; rcx = a1
; rdx = a0
; r8 = b
; r9 = &r

; may be destroyed: rax, rcx, rdx, r8, r9, r10, r11

   mov   rax, rdx                      ; a0
   mov   rdx, rcx                      ; a1
   div   r8
   mov   [r9], rdx                     ; return remainder per reference
   ret   0                             ; return quotient in rax

?divq_asmx64@bignum@@YA_K_K00AEA_K@Z ENDP



   align 16

; uint64 __fastcall divr_asmx64(uint64 a0, uint64 a1, uint64 b, uint64& q)

?divr_asmx64@bignum@@YA_K_K00AEA_K@Z PROC

; rcx = a0
; rdx = a1
; r8 = b
; r9 = &q

; may be destroyed: rax, rcx, rdx, r8, r9, r10, r11

   mov   rax, rcx                      ; a0
   div   r8
   mov   [r9], rax                     ; return quotient per reference
   mov   rax, rdx                      ; return remainder in rax
   ret   0

?divr_asmx64@bignum@@YA_K_K00AEA_K@Z ENDP



   align 16

; uint64 __fastcall divr_asmx64(uint64 a0, uint64 a1, uint64 b)

?divr_asmx64@bignum@@YA_K_K00@Z PROC

; rcx = a0
; rdx = a1
; r8 = b

; may be destroyed: rax, rcx, rdx, r8, r9, r10, r11

   mov   rax, rcx                      ; a0
   div   r8
   mov   rax, rdx                      ; return remainder in rax
   ret   0

?divr_asmx64@bignum@@YA_K_K00@Z ENDP



; ------ 64/32=32 MODULAR DIVISION --------------------------------------------

   align 16

; uint32 __fastcall divr_asmx64_64by32_rev(uint32 b, uint64 a)

?divr_asmx64_64by32_rev@bignum@@YA_K_K0@Z PROC

; rcx = b
; rdx = a

; may be destroyed: rax, rcx, rdx, r8, r9, r10, r11

   mov   eax, edx                      ; low part dividend
   shr   rdx, 32                       ; high part dividend
   div   ecx
   mov   eax, edx                      ; return remainder in rax
   ret   0

?divr_asmx64_64by32_rev@bignum@@YA_K_K0@Z ENDP



; ------ MODULAR MULTIPLICATION -----------------------------------------------

   align 16

; uint64 __fastcall modmul_asmx64(uint64 u, uint64 v, uint64 m)

?modmul_asmx64@bignum@@YA_K_K00@Z PROC

; rcx = u
; rdx = v
; r8 = m

; may be destroyed: rax, rcx, rdx, r8, r9, r10, r11

   mov   rax, rdx
   mul   rcx
   div   r8
   mov   rax, rdx
   ret   0

?modmul_asmx64@bignum@@YA_K_K00@Z ENDP



; ------ MODULAR MULTIPLICATION -----------------------------------------------

   align 16

; uint64 __fastcall modmul2_asmx64(uint64 u, uint64 v)

?modmul2_asmx64@bignum@@YA_K_K0@Z PROC

; rcx = u
; rdx = v

; may be destroyed: rax, rcx, rdx, r8, r9, r10, r11

; result is in r11:r10:r9:XXX

   mov   rax, rdx
   mul   rcx

   ; c:\> magic.py 10232178353385766913 128
   ; M=0x73615a240e6c2b43b1cb15614023c06d (153366700584084941264278763015088881773), s=62, a=False
   ; m1=73615a240e6c2b43, m0=b1cb15614023c06d
   ; p1=rdx, p0=rax

   mov   r8, rax                       ; save p0
   mov   rcx, rdx                      ; save p1

   mov   r11, 0b1cb15614023c06dh       ; m0
   mul   r11                           ; p0*m0
   mov   r9, rdx

   mov   rax, 073615a240e6c2b43h       ; m1
   mul   r8                            ; p0*m1
   add   r9, rax
   mov   r10, rdx
   adc   r10, 0

   mov   rax, 073615a240e6c2b43h       ; m1
   mul   rcx                           ; p1*m1
   add   r10, rax
   mov   r11, rdx
   adc   r11, 0

   mov   rax, 0b1cb15614023c06dh       ; m0
   mul   rcx                           ; p1*m0
   add   r9, rax
   adc   r10, rdx
   adc   r11, 0

   shl   r11, 2
   shr   r10, 62
   or    r10, r11

   mov   rax, 10232178353385766913
   mul   r10

   sub   r8, rax
;   sbb   rcx, rdx

   mov   rax, r8
   ret   0

?modmul2_asmx64@bignum@@YA_K_K0@Z ENDP



; ------ JUNK 2-SOURCE MEMCPY -------------------------------------------------

; Intel i7-3770 Ivy Bridge 4-core 3.4GHz CPU:
; it's slower if I collect all MOVs in big blocks together, so it can burst them to/from the same memory location
; it's slower if i remove the complicated addressing in favour of "add rdx/r8/rcx, 8"

   align 16

?threeop_asmx64@bignum@@YAKPEA_KPEB_K1_K@Z PROC   ; cl::threeop_asmx64, COMDAT

; uint64 __fastcall threeop_asmx64(uint64* r, const uint64* a, const uint64* b, const uint64 len) {

; rcx = r
; rdx = a
; r8 = b
; r9 = len

; rbx, rsi, rdi, rbp, r12 - r15 must be preserved by callee
; may be destroyed: rax, rcx, rdx, r8, r9, r10, r11
; http://msdn.microsoft.com/en-us/magazine/cc300794.aspx

; rax = accu
; rcx = r
; rdx = a
; r8 = b
; r9 = len
; r10 = accu #2
; r11 = i

; 223  :    const index llen = len/2;
; 224  :    switch (llen % 8) {

  mov r11, r9
  and r11, 7
  lea r10, offset threeop_jump_table
  add r10, QWORD PTR [r10+r11*8]
  jmp r10


  align 16
threeop_loop:
  add r11, 8

  mov rax, QWORD PTR [rdx+r11*8-64]
  mov r10, QWORD PTR [r8+r11*8-64]
  mov QWORD PTR [rcx+r11*8-64], rax

threeop_l7:
  mov rax, QWORD PTR [rdx+r11*8-56]
  mov r10, QWORD PTR [r8+r11*8-56]
  mov QWORD PTR [rcx+r11*8-56], rax

threeop_l6:
  mov rax, QWORD PTR [rdx+r11*8-48]         ; rax = a[-6]
  mov r10, QWORD PTR [r8+r11*8-48]          ; r10 = b[-6]
  mov QWORD PTR [rcx+r11*8-48], rax

threeop_l5:
  mov rax, QWORD PTR [rdx+r11*8-40]
  mov r10, QWORD PTR [r8+r11*8-40]
  mov QWORD PTR [rcx+r11*8-40], rax

threeop_l4:
  mov rax, QWORD PTR [rdx+r11*8-32]
  mov r10, QWORD PTR [r8+r11*8-32]
  mov QWORD PTR [rcx+r11*8-32], rax

threeop_l3:
  mov rax, QWORD PTR [rdx+r11*8-24]
  mov r10, QWORD PTR [r8+r11*8-24]
  mov QWORD PTR [rcx+r11*8-24], rax

threeop_l2:
  mov rax, QWORD PTR [rdx+r11*8-16]
  mov r10, QWORD PTR [r8+r11*8-16]
  mov QWORD PTR [rcx+r11*8-16], rax

threeop_l1:
  mov rax, QWORD PTR [rdx+r11*8-8]
  mov r10, QWORD PTR [r8+r11*8-8]
  mov QWORD PTR [rcx+r11*8-8], rax

threeop_l0:
  cmp r11, r9
  jb threeop_loop

  ret 0


  align 16
threeop_jump_table:
  DQ  threeop_l0 - threeop_jump_table
  DQ  threeop_l1 - threeop_jump_table
  DQ  threeop_l2 - threeop_jump_table
  DQ  threeop_l3 - threeop_jump_table
  DQ  threeop_l4 - threeop_jump_table
  DQ  threeop_l5 - threeop_jump_table
  DQ  threeop_l6 - threeop_jump_table
  DQ  threeop_l7 - threeop_jump_table

?threeop_asmx64@bignum@@YAKPEA_KPEB_K1_K@Z ENDP   ; cl::threeop_asmx64



; ------ JUNK 2-SOURCE MEMCPY with SSE2 ---------------------------------------

   align 16

?threeop2_asmx64@bignum@@YAKPEA_KPEB_K1_K@Z PROC   ; cl::threeop2_asmx64, COMDAT

; uint64 __fastcall threeop2_asmx64(uint64* r, const uint64* a, const uint64* b, const uint64 len) {

; rcx = r
; rdx = a
; r8 = b
; r9 = len

; rbx, rsi, rdi, rbp, r12 - r15 must be preserved by callee
; may be destroyed: rax, rcx, rdx, r8, r9, r10, r11
; http://msdn.microsoft.com/en-us/magazine/cc300794.aspx

; rax = accu
; rcx = r
; rdx = a
; r8 = b
; r9 = len
; r10 = accu #2
; r11 = i

; 223  :    const index llen = len/2;
; 224  :    switch (llen % 8) {

  mov r11, r9
  and r11, 7
  lea r10, offset threeop2_jump_table
  add r10, QWORD PTR [r10+r11*8]
  jmp r10


  align 16
threeop2_loop:
  add r11, 8

  movdqa xmm0, XMMWORD PTR [rdx+r11*8-64]
  movdqa xmm1, XMMWORD PTR [r8+r11*8-64]
  movdqa XMMWORD PTR [rcx+r11*8-64], xmm0

threeop2_l7:
threeop2_l6:
  movdqa xmm0, XMMWORD PTR [rdx+r11*8-48]
  movdqa xmm1, XMMWORD PTR [r8+r11*8-48]
  movdqa XMMWORD PTR [rcx+r11*8-48], xmm0

threeop2_l5:
threeop2_l4:
  movdqa xmm0, XMMWORD PTR [rdx+r11*8-32]
  movdqa xmm1, XMMWORD PTR [r8+r11*8-32]
  movdqa XMMWORD PTR [rcx+r11*8-32], xmm0

threeop2_l3:
threeop2_l2:
  movdqa xmm0, XMMWORD PTR [rdx+r11*8-16]
  movdqa xmm1, XMMWORD PTR [r8+r11*8-16]
  movdqa XMMWORD PTR [rcx+r11*8-16], xmm0

threeop2_l1:
threeop2_l0:
  cmp r11, r9
  jb threeop2_loop

  ret 0


  align 16
threeop2_jump_table:
  DQ  threeop2_l0 - threeop2_jump_table
  DQ  threeop2_l1 - threeop2_jump_table
  DQ  threeop2_l2 - threeop2_jump_table
  DQ  threeop2_l3 - threeop2_jump_table
  DQ  threeop2_l4 - threeop2_jump_table
  DQ  threeop2_l5 - threeop2_jump_table
  DQ  threeop2_l6 - threeop2_jump_table
  DQ  threeop2_l7 - threeop2_jump_table

?threeop2_asmx64@bignum@@YAKPEA_KPEB_K1_K@Z ENDP   ; cl::threeop2_asmx64



; ------ JUNK 2-SOURCE MEMCPY with AVX ----------------------------------------

   align 16

?threeop3_asmx64@bignum@@YAKPEA_KPEB_K1_K@Z PROC   ; cl::threeop3_asmx64, COMDAT

; uint64 __fastcall threeop3_asmx64(uint64* r, const uint64* a, const uint64* b, const uint64 len) {

; rcx = r
; rdx = a
; r8 = b
; r9 = len

; rbx, rsi, rdi, rbp, r12 - r15 must be preserved by callee
; may be destroyed: rax, rcx, rdx, r8, r9, r10, r11
; http://msdn.microsoft.com/en-us/magazine/cc300794.aspx

; rax = accu
; rcx = r
; rdx = a
; r8 = b
; r9 = len
; r10 = accu #2
; r11 = i

; 223  :    const index llen = len/2;
; 224  :    switch (llen % 8) {

  mov r11, r9
  and r11, 7
  lea r10, offset threeop3_jump_table
  add r10, QWORD PTR [r10+r11*8]
  jmp r10


  align 16
threeop3_loop:
  add r11, 8

  vmovdqa ymm0, YMMWORD PTR [rdx+r11*8-64]
  vmovdqa ymm1, YMMWORD PTR [r8+r11*8-64]
  vmovdqa YMMWORD PTR [rcx+r11*8-64], ymm0

threeop3_l7:
threeop3_l6:
threeop3_l5:
threeop3_l4:
  vmovdqa ymm0, YMMWORD PTR [rdx+r11*8-32]
  vmovdqa ymm1, YMMWORD PTR [r8+r11*8-32]
  vmovdqa YMMWORD PTR [rcx+r11*8-32], ymm0

threeop3_l3:
threeop3_l2:
threeop3_l1:
threeop3_l0:
  cmp r11, r9
  jb threeop3_loop

  ret 0


  align 16
threeop3_jump_table:
  DQ  threeop3_l0 - threeop3_jump_table
  DQ  threeop3_l1 - threeop3_jump_table
  DQ  threeop3_l2 - threeop3_jump_table
  DQ  threeop3_l3 - threeop3_jump_table
  DQ  threeop3_l4 - threeop3_jump_table
  DQ  threeop3_l5 - threeop3_jump_table
  DQ  threeop3_l6 - threeop3_jump_table
  DQ  threeop3_l7 - threeop3_jump_table

?threeop3_asmx64@bignum@@YAKPEA_KPEB_K1_K@Z ENDP   ; cl::threeop3_asmx64


_TEXT ENDS

END
