; $+HEADER$
; 
; Copyright 2015 Christoph Lueders
; 
; This file is part of the BIGNUM Library.
; 
; The BIGNUM Library is free software: you can redistribute it and/or modify
; it under the terms of the GNU Lesser General Public License as published by
; the Free Software Foundation, either version 3 of the License, or (at your 
; option) any later version.
; 
; The BIGNUM Library is distributed in the hope that it will be useful, but 
; WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
; or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public 
; License for more details.
; 
; You should have received a copy of the GNU Lesser General Public License
; along with the BIGNUM Library.  If not, see <http://www.gnu.org/licenses/>.
; 
; $-HEADER$

; $Date: 2015-04-24 11:28:16 +0200 (Fr, 24 Apr 2015) $
; $Rev: 26590 $



_TEXT SEGMENT

stackframe = 9*8

inner_jumpin = stackframe - 72
alen$ = stackframe - 64
j$1$ = stackframe - 56
r14_save = stackframe - 40
r15_save = stackframe - 32
rdi_save = stackframe - 24
rsi_save = stackframe - 16
rbx_save = stackframe - 8

rbp_save = stackframe + 8     ; shadow space
r12_save = stackframe + 16    ; shadow space
r13_save = stackframe + 24    ; shadow space
;b$ = stackframe + 32          ; shadow space
;blen$ = stackframe + 40       ; parameter on stack


   align 16

?osqu4_asmx64@bignum@@YAXPEA_KPEB_K_K@Z PROC

; 343  : void osqu4_asm(small* r, const small* a, index alen) {

; parameters:
; rcx = r
; rdx = a
; r8 = alen

; rbx, rsi, rdi, rbp, r12 - r15 must be preserved by callee
; may be destroyed: rax, rcx, rdx, r8, r9, r10, r11
; http://msdn.microsoft.com/en-us/magazine/cc300794.aspx

; 350  :    if (alen == 0) {

   test  r8, r8                           ; r8 = alen
   jz    ret_exit

   sub   rsp, stackframe

   mov   QWORD PTR r15_save[rsp], r15
   mov   QWORD PTR rdi_save[rsp], rdi
   mov   QWORD PTR rsi_save[rsp], rsi
   mov   QWORD PTR rbx_save[rsp], rbx

   mov   QWORD PTR r14_save[rsp], r14
   mov   QWORD PTR rbp_save[rsp], rbp
   mov   QWORD PTR r12_save[rsp], r12
   mov   QWORD PTR r13_save[rsp], r13

   mov   alen$[rsp], r8                   ; r8 ~ alen
   mov   rsi, r8                          ; rsi = alen
   mov   r10, rdx
   mov   r15, rcx

; 357  :    // inner loop on first element:
; 358  :    // init the first 'alen' elements in result with product without adding up old memory contents.
; 359  :    small of1 = 0;
; 360  :    index i = 1;
; 361  :    const small bb = a[0];

   mov   rdi, rsi                         ; rsi ~ alen
   dec   rdi
   and   edi, 7
   mov   rax, init_jump_table
   add   rax, QWORD PTR [rax+rdi*8]
   inc   edi
   xor   ecx, ecx                         ; rcx ~ of1 : zero input carry
   mov   QWORD PTR [r15], rcx             ; r[0] = 0
   mov   r8, QWORD PTR [r10]              ; r10 ~ a
   jmp   rax


;        of1 = ops<small>::muladdc(r[i+(o)], a[i+(o)], bb, 0, of1)

   align 16
init_loop:
   mov   rax, QWORD PTR [r10+rdi*8]
   mul   r8
   add   rax, rcx
   mov   rcx, rdx
   adc   rcx, 0
   mov   QWORD PTR [r15+rdi*8], rax
   add   rdi, 8

init7:
   mov   rax, QWORD PTR [r10+rdi*8-56]
   mul   r8
   add   rax, rcx
   mov   rcx, rdx
   adc   rcx, 0
   mov   QWORD PTR [r15+rdi*8-56], rax

init6:
   mov   rax, QWORD PTR [r10+rdi*8-48]
   mul   r8
   add   rax, rcx
   mov   rcx, rdx
   adc   rcx, 0
   mov   QWORD PTR [r15+rdi*8-48], rax

init5:
   mov   rax, QWORD PTR [r10+rdi*8-40]
   mul   r8
   add   rax, rcx
   mov   rcx, rdx
   adc   rcx, 0
   mov   QWORD PTR [r15+rdi*8-40], rax

init4:
   mov   rax, QWORD PTR [r10+rdi*8-32]
   mul   r8
   add   rax, rcx
   mov   rcx, rdx
   adc   rcx, 0
   mov   QWORD PTR [r15+rdi*8-32], rax

init3:
   mov   rax, QWORD PTR [r10+rdi*8-24]
   mul   r8
   add   rax, rcx
   mov   rcx, rdx
   adc   rcx, 0
   mov   QWORD PTR [r15+rdi*8-24], rax

init2:
   mov   rax, QWORD PTR [r10+rdi*8-16]
   mul   r8
   add   rax, rcx
   mov   rcx, rdx
   adc   rcx, 0
   mov   QWORD PTR [r15+rdi*8-16], rax

init1:
   mov   rax, QWORD PTR [r10+rdi*8-8]
   mul   r8
   add   rax, rcx
   mov   rcx, rdx
   adc   rcx, 0
   mov   QWORD PTR [r15+rdi*8-8], rax

init0:
   cmp   rdi, rsi
   jb    init_loop

; 367  :    r[i] = of1;  // save overflow so the next add in the inner loop works ok.

   mov   QWORD PTR [r15+rdi*8], rcx


; prepare for the main inner loop

   lea   r11, QWORD PTR [rsi-6]           ; rsi ~ alen
   mov   rax,0AAAAAAAAAAAAAAABh
   mul   r11
   shr   rdx, 1                           ; rdx = (alen-6) / 3

   lea   rcx,[rdx+rdx*2]                  ; rcx = 3*edx
   mov   rax, r11
   sub   rax, rcx                         ; rax = (alen-6) % 3

   mov   rdx, inner_jump_table
   add   rdx, QWORD PTR [rdx+rax*8]
   mov   inner_jumpin[rsp], rdx



; 369  :    // the main outer loop
; 370  :    index j = 1;

   mov   ebp, 1

   align 16
outer_main_loop:
; 372  :    while (j+4 < alen) {  // make sure three accesses will fit

; rbx : of1    (yet to be loaded)
; rsi : alen
; rbp : j
; rbp : of2    (yet to be loaded)
; r8  : bb2    (constant, yet to be loaded)
; r9  : bb1    (constant, yet to be loaded)
; r10 : a
; r13 : bb0    (constant, yet to be loaded)
; r14 : of3    (yet to be loaded)
; r15 : r

   mov   QWORD PTR j$1$[rsp], rbp
   lea   rax, QWORD PTR [rbp+5]           ; rax = j + 5
   cmp   rax, rsi                         ; rsi ~ alen
   ja    outer_leftover_check

; 374  :       const small bb0 = b[j];
; 375  :       const small bb1 = b[j+1];
; 376  :       const small bb2 = b[j+2];

   mov   r13, QWORD PTR [r10+rbp*8]       ; r13 ~ bb0
   mov   r9, QWORD PTR [r10+rbp*8+8]      ; r9 ~ bb1
   mov   r8, QWORD PTR [r10+rbp*8+16]     ; r8 ~ bb2
   mov   r11, rbp                         ; r11 ~ i, rbp ~ j : i = j

; 373  :       rm = r + j;

   lea   r12, QWORD PTR [r15+rbp*8]       ; r12 = rm = r+j
   mov   rcx, QWORD PTR [r12+r11*8+8]     ; rcx = rm[i+1] = r[j+i+1]

; 377  :       small of2 = 0;
; 378  :       small of3 = 0;

   xor   r14d, r14d                       ; r14 ~ of3

; 381  :       of1 = ops<small>::muladdc(rm[1], a[i+1], bb0, rm[1], 0);  // of1:rm[1] = a[i+1] * b[0] + rm[1]

   mov   rax, QWORD PTR [r10+r11*8+8]     ; rax = a[i+1]
   mul   r13                              ; a[i+1] * bb0

   add   rcx, rax                         ; kein input carry, da of1 = 0
   mov   rbx, rdx
   adc   rbx, 0                           ; rbx ~ output of1
   mov   QWORD PTR [r12+r11*8+8], rcx     ; rm[i+1]

; 381  :       of1 = ops<small>::muladdc(rm[2], a[i+2], bb0, rm[2], 0);  // of1:rm[2] = a[i+2] * b[0] + rm[2]

   mov   rax, QWORD PTR [r10+r11*8+16]    ; rax = a[i+2]
   mov   rcx, QWORD PTR [r12+r11*8+16]    ; rm[i+2]
   mul   r13                              ; a[i+2] * bb0

   add   rcx, rbx                         ; rbx ~ input of1
   mov   rbx, rdx
   adc   rbx, 0
   add   rcx, rax                         ; rcx ~ output rm[i+2]
   adc   rbx, 0                           ; rbx ~ output of1
   mov   QWORD PTR [r12+r11*8+16], rcx    ; rm[i+2]

; 383  :       of2 = ops<small>::muladdc(mx, a[i+2], bb1, rm[3], 0);  // of1:mx = a[i+2] * b[1] + rm[3]

   mov   rax, QWORD PTR [r10+r11*8+16]    ; a[i+2]
   mov   rcx, QWORD PTR [r12+r11*8+24]    ; rcx = rm[i+3]
   mul   r9                               ; a[i+2] * bb1

   add   rcx, rax                         ; kein input carry, da input of2 = 0
   mov   rbp, rdx
   adc   rbp, 0                           ; rbp ~ output of2
                                          ; rcx ~ mx

;      :       of1 = ops<small>::muladdc(rm[3], a[i+3], bb0, mx, of1);  // of2:rm[3] = a[i+3] * b[0] + mx + of1

   mov   rax, QWORD PTR [r10+r11*8+24]    ; a[i+3]
   mul   r13                              ; a[i+3] * bb0

   add   rcx, rbx                         ; rbx ~ input of1
   mov   rbx, rdx
   adc   rbx, 0
   add   rcx, rax                         ; rcx ~ mx
   adc   rbx, 0                           ; rbx ~ output of1
   mov   QWORD PTR [r12+r11*8+24], rcx    ; rm[i+3]

; 383  :       of2 = ops<small>::muladdc(mx, a[i+3], bb1, rm[4], of2);  // of1:mx = a[i+2] * b[1] + rm[3] + of2

   mov   rax, QWORD PTR [r10+r11*8+24]    ; a[i+3]
   mov   rcx, QWORD PTR [r12+r11*8+32]    ; rcx = rm[i+4]
   mul   r9                               ; a[i+3] * bb1

   add   rcx, rbp                         ; rbp ~ input of2
   mov   rbp, rdx
   adc   rbp, 0
   add   rcx, rax                         ; rcx ~ mx
   adc   rbp, 0                           ; rbp ~ output of2

;      :       of1 = ops<small>::muladdc(rm[4], a[i+4], bb0, mx, of1);  // of2:rm[3] = a[i+3] * b[0] + mx + of1

   mov   rax, QWORD PTR [r10+r11*8+32]    ; a[i+4]
   mul   r13                              ; a[i+4] * bb0

   add   rcx, rbx                         ; rbx ~ input of1
   mov   rbx, rdx
   adc   rbx, 0
   add   rcx, rax                         ; rcx ~ mx
   adc   rbx, 0                           ; rbx ~ output of1
   mov   QWORD PTR [r12+r11*8+32], rcx    ; rm[i+4]

   mov   rax, inner_jumpin[rsp]
   jmp   rax

   align 16
start_inner2:
   add   r11, 2+3
   mov   rsi, QWORD PTR [r10+r11*8-16]
   mov   r15, QWORD PTR [r10+r11*8-8]
   jmp   inner2

   align 16
start_inner1:
   add   r11, 1+3
   mov   r15, QWORD PTR [r10+r11*8-8]
   mov   rdi, QWORD PTR [r10+r11*8]
   jmp   inner1

   align 16
start_inner0:
   add   r11, 3
   mov   rdi, QWORD PTR [r10+r11*8]
   mov   rsi, QWORD PTR [r10+r11*8+8]
   jmp   inner0



   align 16
inner_loop:

;          of1 = ops<small>::muladdc(mx, a[i+(o)], bb2, rm[i+2+(o)], of1);
;          of2 = ops<small>::muladdc(mx, a[i+1+(o)], bb1, mx, of2);
;          of3 = ops<small>::muladdc(rm[i+2+(o)], a[i+2+(o)], bb0, mx, of3);

; register allocation in this loop:
; rax = scratch
; rbx = of1
; rcx = mx
; rdx = scratch
; rbp = of2
; rsi = memory a[]
; rdi = memory a[]
; r8  = bb2
; r9  = bb1
; r10 = a
; r11 = i
; r12 = rm
; r13 = bb0
; r14 = of3
; r15 = memory a[]

   ; r15 is free
   mov   rax, rdi                         ; QWORD PTR [r10+r11*8-24]
   ; rdi is now free
   mul   r8                               ; a[i] * bb2
   add   r11, 3
   mov   rcx, QWORD PTR [r12+r11*8-8]

   add   rcx, rbx                         ; rbx ~ input of1
   mov   rbx, rdx                         ; rbx ~ output of1
   adc   rbx, 0
   add   rcx, rax                         ; rcx ~ output mx
   adc   rbx, 0

   mov   rax, rsi                         ; QWORD PTR [r10+r11*8-16]
   mul   r9                               ; a[i+1] * bb1

   add   rcx, rbp                         ; rbp ~ input of2
   mov   rbp, rdx                         ; rbp ~ output of2
   adc   rbp, 0
   add   rcx, rax                         ; rcx ~ output mx
   adc   rbp, 0

   mov   rax, QWORD PTR [r10+r11*8-8]
   mov   r15, rax
   mul   r13                              ; a[i+2] * bb0

   add   rcx, r14                         ; r14 ~ input of3
   mov   r14, rdx                         ; r14 ~ output of3
   adc   r14, 0
   add   rcx, rax                         ; rcx ~ output mx
   adc   r14, 0
   mov   QWORD PTR [r12+r11*8-8], rcx

inner2:
   ; rdi is free
   mov   rax, rsi                         ; QWORD PTR [r10+r11*8-16]
   ; rsi is now free
   mul   r8                               ; a[i] * bb2
   mov   rcx, QWORD PTR [r12+r11*8]

   add   rcx, rbx                         ; rbx ~ input of1
   mov   rbx, rdx                         ; rbx ~ output of1
   adc   rbx, 0
   add   rcx, rax                         ; rcx ~ output mx
   adc   rbx, 0

   mov   rax, r15                         ; QWORD PTR [r10+r11*8-8]
   mul   r9                               ; a[i+1] * bb1

   add   rcx, rbp                         ; rbp ~ input of2
   mov   rbp, rdx                         ; rbp ~ output of2
   adc   rbp, 0
   add   rcx, rax                         ; rcx ~ output mx
   adc   rbp, 0

   mov   rax, QWORD PTR [r10+r11*8]
   mov   rdi, rax
   mul   r13                              ; a[i+2] * bb0

   add   rcx, r14                         ; r14 ~ input of3
   mov   r14, rdx                         ; r14 ~ output of3
   adc   r14, 0
   add   rcx, rax                         ; rcx ~ output mx
   adc   r14, 0
   mov   QWORD PTR [r12+r11*8], rcx

inner1:
   ; rsi is free
   mov   rax, r15                         ; QWORD PTR [r10+r11*8-8]
   ; r15 is now free
   mul   r8                               ; a[i] * bb2
   mov   rcx, QWORD PTR [r12+r11*8+8]

   add   rcx, rbx                         ; rbx ~ input of1
   mov   rbx, rdx                         ; rbx ~ output of1
   adc   rbx, 0
   add   rcx, rax                         ; rcx ~ output mx
   adc   rbx, 0

   mov   rax, rdi                         ; QWORD PTR [r10+r11*8]
   mul   r9                               ; a[i+1] * bb1

   add   rcx, rbp                         ; rbp ~ input of2
   mov   rbp, rdx                         ; rbp ~ output of2
   adc   rbp, 0
   add   rcx, rax                         ; rcx ~ output mx
   adc   rbp, 0

   mov   rax, QWORD PTR [r10+r11*8+8]
   mov   rsi, rax
   mul   r13                              ; a[i+2] * bb0

   add   rcx, r14                         ; r14 ~ input of3
   mov   r14, rdx                         ; r14 ~ output of3
   adc   r14, 0
   add   rcx, rax                         ; rcx ~ output mx
   adc   r14, 0
   mov   QWORD PTR [r12+r11*8+8], rcx

inner0:
   mov   rdx, alen$[rsp]                  ; rdx = alen
   lea   rax, QWORD PTR [r11+2]           ; r11 ~ i
   cmp   rax, rdx
   jb    inner_loop



;      of1 = ops<small>::muladdc(mx, a[i], bb2, of3, of1);  // of1:mx = a[i] * b[2] + of1 + of3
;      of2 = ops<small>::muladdc(rm[i+2], a[i+1], bb1, mx, of2);  // of2:rm[i+1] = a[i+1] * b[1] + mx + of2

   ; reload r15 with r = rm - j
   mov   rax, QWORD PTR j$1$[rsp]
   neg   rax
   lea   r15, QWORD PTR [r12+rax*8]       ; r12 ~ rm, r15 ~ r

   mov   rax, QWORD PTR [r10+r11*8]       ; a[i]
   mul   r8                               ; a[i] * bb2

   mov   rcx, rbx                         ; rbx ~ input of1
   add   rcx, r14                         ; r14 ~ input of3
   mov   rbx, rdx
   adc   rbx, 0
   add   rcx, rax                         ; rcx ~ mx
   adc   rbx, 0                           ; rbx ~ output of1

   mov   rax, QWORD PTR [r10+r11*8+8]     ; a[i+1]
   mul   r9                               ; a[i+1] * bb1

   add   rcx, rbp                         ; rbp ~ input of2
   mov   rbp, rdx
   adc   rbp, 0
   add   rcx, rax
   adc   rbp, 0                           ; rbp ~ output of2
   mov   QWORD PTR [r12+r11*8+16], rcx

;      of1 = ops<small>::muladdc(rm[i+3], a[i+1], bb2, of1, of2);  // of1:rm[i+2] = a[i+1] * b[2] + of1 + of2
;      rm[i+4] = of1;

   mov   rax, QWORD PTR [r10+r11*8+8]     ; a[i+1]
   mul   r8                               ; a[i+1] * bb2

   add   rbp, rbx
   mov   rbx, rdx
   adc   rbx, 0
   add   rbp, rax
   adc   rbx, 0
   mov   QWORD PTR [r12+r11*8+24], rbp
   mov   QWORD PTR [r12+r11*8+32], rbx

; 401  :       j += 3;

   mov   rbp, QWORD PTR j$1$[rsp]
   add   rbp, 3
   mov   rsi, QWORD PTR alen$[rsp]

   jmp   outer_main_loop



; 404  :    // lumpensammler: multiply what's still left
; 405  :    while (j < alen - 1) {

   align 16
outer_leftover_loop:

; 406  :       rm = r + j;
; 407  :       const small bb = b[j];

   mov   rbx, QWORD PTR [r10+rbp*8]       ; rbx = bb, r10 ~ a

   lea   r9, QWORD PTR [rbp+1]            ; r9 = i = j+1
   lea   r11, QWORD PTR [r15+rbp*8]       ; r11 ~ rm
   xor   ecx, ecx                         ; rcx ~ carry
   jmp   short leftover_check

   align 16
leftover_loop:
   mov   rax, QWORD PTR [r10+r9*8]        ; rax = a[i]
   mul   rbx                              ; rbx ~ bb
   mov   r8, QWORD PTR [r11+r9*8]         ; r8 = rm[i]

   add   r8, rcx
   mov   rcx, rdx
   adc   rcx, 0
   add   r8, rax
   adc   rcx, 0                           ; rcx ~ of
   mov   QWORD PTR [r11+r9*8], r8

   inc   r9
leftover_check:
   cmp   r9, rsi
   jb    leftover_loop

; 414  :       rm[i] = of1;
; 415  :       ++j;
; 416  :    }

   mov   QWORD PTR [r11+r9*8], rcx
   inc   rbp

outer_leftover_check:
   cmp   rbp, rsi                         ; j < alen?
   jb    outer_leftover_loop



; square the diagonal and multiply the result to far by 2

; rbx : of1
; rcx : scratch for memory
; rsi : alen
; r9  : addressing
; r10 : a
; r11 : i      (yet to be loaded)
; r14 : of2    (yet to be loaded)
; r15 : r

   xor   r11d, r11d                       ; r11 ~ i
   xor   ebx, ebx                         ; of1
   xor   r14d, r14d                       ; of2

   align 16
diag_loop:
   mov   rax, QWORD PTR [r10+r11*8]       ; a[i]
   mul   rax                              ; a[i] * a[i]
   lea   r9, QWORD PTR [r11*8]

   mov   rcx, QWORD PTR [r15+r9*2]        ; r[2*i]
   mov   r8, QWORD PTR [r15+r9*2+8]       ; r[2*i+1]

   shl   rcx, 1                           ; shift left r8:rcx by 1
   rcl   r8, 1
   setc  bl

   add   rax, r14
   adc   rdx, 0
   setc  r14b

   add   rax, rcx
   adc   rdx, r8
   adc   r14d, ebx

   mov   QWORD PTR [r15+r9*2], rax        ; r[2*i]
   mov   QWORD PTR [r15+r9*2+8], rdx      ; r[2*i+1]

   inc   r11
   cmp   r11, rsi                         ; i < alen?
   jb    diag_loop

   ; r14 must be zero here!



no_leftover:
   mov   r14, QWORD PTR r14_save[rsp]
   mov   rbp, QWORD PTR rbp_save[rsp]
   mov   r12, QWORD PTR r12_save[rsp]
   mov   r13, QWORD PTR r13_save[rsp]

pop_exit:
   mov   r15, QWORD PTR r15_save[rsp]
   mov   rdi, QWORD PTR rdi_save[rsp]
   mov   rsi, QWORD PTR rsi_save[rsp]
   mov   rbx, QWORD PTR rbx_save[rsp]

   add   rsp, stackframe

ret_exit:
   ret   0


   align 16
init_jump_table:
   DQ    init0 - init_jump_table
   DQ    init1 - init_jump_table
   DQ    init2 - init_jump_table
   DQ    init3 - init_jump_table
   DQ    init4 - init_jump_table
   DQ    init5 - init_jump_table
   DQ    init6 - init_jump_table
   DQ    init7 - init_jump_table

   align 16
inner_jump_table:
   DQ    start_inner0 - inner_jump_table
   DQ    start_inner1 - inner_jump_table
   DQ    start_inner2 - inner_jump_table

?osqu4_asmx64@bignum@@YAXPEA_KPEB_K_K@Z ENDP

_TEXT ENDS
END
