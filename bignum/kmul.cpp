// $+HEADER$
// 
// Copyright 2015 Christoph Lueders
// 
// This file is part of the BIGNUM Library.
// 
// The BIGNUM Library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
// 
// The BIGNUM Library is distributed in the hope that it will be useful, but 
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public 
// License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with the BIGNUM Library.  If not, see <http://www.gnu.org/licenses/>.
// 
// $-HEADER$

// $Date: 2015-04-24 11:28:16 +0200 (Fr, 24 Apr 2015) $
// $Rev: 26590 $

/*
 *  arbitrary length number arithmetic - karatsuba multiplication
 */

#include "precomp.h"
#include "impl.h"
#include "small.h"
#include "cl/format.h"

// we must switch off "unreachable code" warning, since bool template parameters generate code paths that are never used
#pragma warning (disable:4702)

namespace bignum {

#define USE_KARA 1

index _CL_CALL switch_kmul(index set) {
   const index r = kmul_thresh;
   ctassert(kmul_min_len >= 2);  // must not be less than 2, otherwise it will never end recursion
   kmul_thresh = tmax(set, kmul_min_len);
   return r;
}

index _CL_CALL switch_ksqu(index set) {
   const index r = ksqu_thresh;
   ctassert(kmul_min_len >= 2);  // must not be less than 2, otherwise it will never end recursion
   ksqu_thresh = tmax(set, kmul_min_len);
   return r;
}


// ------ calc mem for KARATSUBA MUL ------------------------------------------

template<typename small, bool square>
index kmul_mem(const index alen, const index blen) {
   if (!use_qkmul<square>(alen, blen)) return 0;

   const index llen = blen / 2;  // low part length
   const index ahlen = alen - llen;  // a high part length
   const index bhlen = blen - llen;  // b high part length

   return ((square ? 3 : 4)*ahlen
#ifndef USE_KARA
      +1
#endif
      ) * sizeof(small) + tmax(kmul_mem<small, square>(llen, llen), kmul_mem<small, square>(ahlen, bhlen), kmul_mem<small, square>(ahlen, ahlen));
}


#ifdef USE_KARA
// ------ helper routines for kmul() ------------------------------------------

// AAAAA:BBBBB:CCCCC:DDDDD  EEEEE:FFFFF
// \----r2---/ \----r0---/  \----p----/
//        llen  llen: llen         llen

// we have A:B:C:D (r2:r0) and E:F (p) and want to compute B:C += A:B + C:D + E:F
// B =  B + A + C + E =  (B + C) + A + E
// C =  C + B + D + F =  (B + C) + D + F
// this works in 5 ops and 6 reads and 2 writes of length n, compared to 3 ops and 6 reads and 3 writes of length 2*n

template<typename small>
static void kara_add(small* r, index rlen, small* p, index plen, index llen) {
   caassert(llen > 0);
   caassert(plen >= 2*llen);
   caassert(rlen >= 4*llen);
   caassert(rlen >= llen+plen);
   caassert(rlen-2*llen <= plen);

   small* a = r + 3*llen;  // length rlen-3*llen
   small* b = r + 2*llen;  // length llen
   small* c = r + llen;    // length llen
   small* d = r;           // length llen
   small* e = p + llen;    // length plen-llen
   small* f = p;           // length llen

   small sum_bc;
   index i = 0;
   small of_b = 0, of_c = 0;
   while (i < llen) {
      small of_bc = ops<small>::addc(sum_bc, b[i], c[i]);
      of_b = ops<small>::addc(b[i], a[i], e[i], sum_bc, of_b) + of_bc;
      of_c = ops<small>::addc(c[i], d[i], f[i], sum_bc, of_c) + of_bc;
      ++i;
   }
   // parts B, C, D and F are done.  high parts A and E might still have data
   const index rhlen = rlen - 3*llen;
   while (i < rhlen) {
      of_b = ops<small>::addc(b[i], b[i], a[i], e[i], of_b);
      ++i;
   }
   // parts A, B, C, D and F are done.  high part E might still have data
   const index phlen = plen - llen;
   while (i < phlen) {
      of_b = ops<small>::addc(b[i], b[i], e[i], of_b);
      ++i;
   }
   // propagate of_b
   while (of_b) {
      classert(i < rlen-2*llen);
      of_b = ops<small>::addc(b[i], b[i], of_b);
      ++i;
   }
   // propagate of_c
   i = llen;
   while (of_c) {
      classert(i < rlen-llen);
      of_c = ops<small>::addc(c[i], c[i], of_c);
      ++i;
   }
}

#ifdef CL_TEST_CODE
static void test_kara_add() {
   {
      uint16 r[] = { 0x1111, 0x2222, 0x3333, 0x4444 };
      uint16 p[] = { 0x5555, 0x6666 };
      uint16 r2[NBEL(r)];
      uint16 p2[NBEL(p)];
      index llen = 1;

      copy(p2, p, NBEL(p));
      copy(r2, r, NBEL(r));
      addu_on(p2, NBEL(p2), r, 2*llen);  // ps += r0
      addu_on(p2, NBEL(p2), r+2*llen, NBEL(r)-2*llen);  // ps += r2
      // add the final temp into the result
      addu_on(r2+llen, NBEL(r2)-llen, p2, NBEL(p2));

      kara_add(r, NBEL(r), p, NBEL(p), llen);

      classert(compu(r, NBEL(r), r2, NBEL(r2)) == 0);
   }
   {
      // alen = 4, blen = 4: llen = 2, bhlen = 2, ahlen = 4
      uint16 r[] = { 0xf111, 0xf211, 0xf122, 0xf222, 0xf133, 0xf233, 0xf144, 0x0244 };
      uint16 p[] = { 0xf155, 0xf255, 0xf166, 0xf266, 0 };
      uint16 r2[NBEL(r)];
      uint16 p2[NBEL(p)];
      index llen = 2;

      copy(p2, p, NBEL(p));
      copy(r2, r, NBEL(r));
      addu_on(p2, NBEL(p2), r, 2*llen);  // ps += r0
      addu_on(p2, NBEL(p2), r+2*llen, NBEL(r)-2*llen);  // ps += r2
      // add the final temp into the result
      addu_on(r2+llen, NBEL(r2)-llen, p2, NBEL(p2));

      kara_add(r, NBEL(r), p, NBEL(p), llen);

      classert(compu(r, NBEL(r), r2, NBEL(r2)) == 0);
   }
   {
      // alen = 5, blen = 4: llen = 2, bhlen = 2, ahlen = 3
      uint16 r[] = { 0xf111, 0xf211, 0xf122, 0xf222, 0xf133, 0xf233, 0xf144, 0xf244, 0x0155 };
      uint16 p[] = { 0xf166, 0xf266, 0xf177, 0xf277, 0xf188, 0xf288, 0 };
      uint16 r2[NBEL(r)];
      uint16 p2[NBEL(p)];
      index llen = 2;

      copy(p2, p, NBEL(p));
      copy(r2, r, NBEL(r));
      addu_on(p2, NBEL(p2), r, 2*llen);  // ps += r0
      addu_on(p2, NBEL(p2), r+2*llen, NBEL(r)-2*llen);  // ps += r2
      // add the final temp into the result
      addu_on(r2+llen, NBEL(r2)-llen, p2, NBEL(p2));

      kara_add(r, NBEL(r), p, NBEL(p), llen);

      classert(compu(r, NBEL(r), r2, NBEL(r2)) == 0);
   }
   {
      // alen = 6, blen = 5: llen = 2, bhlen = 3, ahlen = 4
      uint16 r[] = { 0xf111, 0xf211, 0xf122, 0xf222, 0xf133, 0xf233, 0xf144, 0xf244, 0xf155, 0xf255, 0x0166 };
      uint16 p[] = { 0xf166, 0xf266, 0xf177, 0xf277, 0xf188, 0xf288, 0xf199, 0xf299, 0 };
      uint16 r2[NBEL(r)];
      uint16 p2[NBEL(p)];
      index llen = 2;

      copy(p2, p, NBEL(p));
      copy(r2, r, NBEL(r));
      addu_on(p2, NBEL(p2), r, 2*llen);  // ps += r0
      addu_on(p2, NBEL(p2), r+2*llen, NBEL(r)-2*llen);  // ps += r2
      // add the final temp into the result
      addu_on(r2+llen, NBEL(r2)-llen, p2, NBEL(p2));

      kara_add(r, NBEL(r), p, NBEL(p), llen);

      classert(compu(r, NBEL(r), r2, NBEL(r2)) == 0);
   }
}
#endif


// AAAAA:BBBBB:CCCCC:DDDDD  EEEEE:FFFFF
// \----r2---/ \----r0---/  \----p----/
//        llen  llen: llen         llen

// we have A:B:C:D (r2:r0) and E:F (p) and want to compute B:C += A:B + C:D - E:F
// B =  B + A + C - E =  (B + C) + A - E
// C =  C + B + D - F =  (B + C) + D - F
// this works in 5 ops and 6 reads and 2 writes of length n, compared to 3 ops and 6 reads and 3 writes of length 2*n

template<typename small>
static void kara_sub(small* r, index rlen, small* p, index plen, index llen) {
   caassert(llen > 0);
   caassert(plen >= 2*llen);
   caassert(rlen >= 4*llen);
   caassert(rlen >= llen+plen);
   caassert(rlen-2*llen <= plen);

   small* a = r + 3*llen;  // length rlen-3*llen
   small* b = r + 2*llen;  // length llen
   small* c = r + llen;    // length llen
   small* d = r;           // length llen
   small* e = p + llen;    // length plen-llen
   small* f = p;           // length llen

   register small mx;
   small sum_bc;
   index i = 0;
   small of_bp = 0, of_bm = 0, of_cp = 0, of_cm = 0, of_bc = 0;
#define s(o) \
   do { \
      of_bc = ops<small>::addc(sum_bc, b[i+(o)], c[i+(o)], of_bc);  \
      of_bp = ops<small>::addc(mx, a[i+(o)], sum_bc, of_bp);  \
      of_bm = ops<small>::subc(b[i+(o)], mx, e[i+(o)], of_bm);  \
      of_cp = ops<small>::addc(mx, d[i+(o)], sum_bc, of_cp);  \
      of_cm = ops<small>::subc(c[i+(o)], mx, f[i+(o)], of_cm);  \
   } while(0)
   UNROLL4(i, llen, 0, ladd, s);
#undef s
   of_bp += of_bc;
   of_cp += of_bc;

   // parts B, C, D and F are done.  high parts A and E might still have data
   const index rhlen = rlen - 3*llen;  // should be quite short (probably 1 word)
   while (i < rhlen) {
      of_bp = ops<small>::addc(mx, b[i], a[i], of_bp);
      of_bm = ops<small>::subc(b[i], mx, e[i], of_bm);
      ++i;
   }

   // parts A, B, C, D and F are done.  high part E might still have data
   const index phlen = plen - llen;
#define s(o) \
   do { \
      of_bp = ops<small>::addc(mx, b[i+(o)], of_bp);  \
      of_bm = ops<small>::subc(b[i+(o)], mx, e[i+(o)], of_bm);  \
   } while(0)
   UNROLL4(i, phlen, 0, padd, s);
#undef s

   // propagate of_bX
   if (of_bp >= of_bm) {
      of_bp -= of_bm;
      while (of_bp) {
         classert(i < rlen-2*llen);
         of_bp = ops<small>::addc(b[i], b[i], of_bp);
         ++i;
      }
   }
   else {
      of_bm -= of_bp;
      while (of_bm) {
         classert(i < rlen-2*llen);
         of_bm = ops<small>::subc(b[i], b[i], of_bm);
         ++i;
      }
   }

   // propagate of_cX
   i = llen;
   if (of_cp >= of_cm) {
      of_cp -= of_cm;
      while (of_cp) {
         classert(i < rlen-llen);
         of_cp = ops<small>::addc(c[i], c[i], of_cp);
         ++i;
      }
   }
   else {
      of_cm -= of_cp;
      while (of_cm) {
         classert(i < rlen-llen);
         of_cm = ops<small>::subc(c[i], c[i], of_cm);
         ++i;
      }
   }
}

#ifdef CL_TEST_CODE
#ifdef KMUL_CHECK_DUMP
static bool kmul_dump = false;
#endif
static void test_kara_sub() {
   {
      uint16 r[] = { 0x1111, 0x2222, 0x3333, 0x4444 };
      uint16 p[] = { 0x5555, 0x6666, 0 };
      uint16 r2[NBEL(r)];
      uint16 p2[NBEL(p)];
      index llen = 1;

      copy(p2, p, NBEL(p));
      copy(r2, r, NBEL(r));
      subu_on_neg(p2, NBEL(p2), r, 2*llen);  // ps = r0 - ps
      addu_on(p2, NBEL(p2), r+2*llen, NBEL(r)-2*llen);  // ps += r2
      // add the final temp into the result
      addu_on(r2+llen, NBEL(r2)-llen, p2, NBEL(p2));

      kara_sub(r, NBEL(r), p, NBEL(p), llen);

      classert(compu(r, NBEL(r), r2, NBEL(r2)) == 0);
   }
   {
      // alen = 4, blen = 4: llen = 2, bhlen = 2, ahlen = 4
      uint16 r[] = { 0xf111, 0xf211, 0xf122, 0xf222, 0xf133, 0xf233, 0xf144, 0x0244 };
      uint16 p[] = { 0xf155, 0xf255, 0xf166, 0xf266, 0 };
      uint16 r2[NBEL(r)];
      uint16 p2[NBEL(p)];
      index llen = 2;

      copy(p2, p, NBEL(p));
      copy(r2, r, NBEL(r));
      subu_on_neg(p2, NBEL(p2), r, 2*llen);  // ps = r0 - ps
      addu_on(p2, NBEL(p2), r+2*llen, NBEL(r)-2*llen);  // ps += r2
      // add the final temp into the result
      addu_on(r2+llen, NBEL(r2)-llen, p2, NBEL(p2));

      kara_sub(r, NBEL(r), p, NBEL(p), llen);

      classert(compu(r, NBEL(r), r2, NBEL(r2)) == 0);
   }
   {
      // alen = 5, blen = 4: llen = 2, bhlen = 2, ahlen = 3
      uint16 r[] = { 0xf111, 0xf211, 0xf122, 0xf222, 0xf133, 0xf233, 0xf144, 0xf244, 0x0155 };
      uint16 p[] = { 0xf166, 0xf266, 0xf177, 0xf277, 0xf188, 0x0288, 0 };
      uint16 r2[NBEL(r)];
      uint16 p2[NBEL(p)];
      index llen = 2;

      copy(p2, p, NBEL(p));
      copy(r2, r, NBEL(r));
      subu_on_neg(p2, NBEL(p2), r, 2*llen);  // ps = r0 - ps
      addu_on(p2, NBEL(p2), r+2*llen, NBEL(r)-2*llen);  // ps += r2
      // add the final temp into the result
      addu_on(r2+llen, NBEL(r2)-llen, p2, NBEL(p2));

      kara_sub(r, NBEL(r), p, NBEL(p), llen);

      classert(compu(r, NBEL(r), r2, NBEL(r2)) == 0);
   }
   {
      // alen = 6, blen = 5: llen = 2, bhlen = 3, ahlen = 4
      uint16 r[] = { 0xf111, 0xf211, 0xf122, 0xf222, 0xf133, 0xf233, 0xf144, 0xf244, 0xf155, 0xf255, 0x0166 };
      uint16 p[] = { 0xf166, 0xf266, 0xf177, 0xf277, 0xf188, 0xf288, 0xf199, 0x0299, 0 };
      uint16 r2[NBEL(r)];
      uint16 p2[NBEL(p)];
      index llen = 2;

      copy(p2, p, NBEL(p));
      copy(r2, r, NBEL(r));
      subu_on_neg(p2, NBEL(p2), r, 2*llen);  // ps = r0 - ps
      addu_on(p2, NBEL(p2), r+2*llen, NBEL(r)-2*llen);  // ps += r2
      // add the final temp into the result
      addu_on(r2+llen, NBEL(r2)-llen, p2, NBEL(p2));

      kara_sub(r, NBEL(r), p, NBEL(p), llen);

      classert(compu(r, NBEL(r), r2, NBEL(r2)) == 0);
   }

#ifdef KMUL_CHECK_DUMP
   {
      uint16 a[] = { 0x64f5, 0x35ee, 0x574c, 0xb61f, 0x9a31, 0xe1fd, 0x8a42, 0xe429, 0x7b75, 0x38a2, 0x9b0d, 0x22ed, 0x9fdc, 0xa883, 0xd4e3, 0xda42, 0xe244, 0x0b9b,
         0x27b8, 0xe100, 0xa543, 0x903b, 0x8213, 0xb956, 0x5a8f, 0x3ee3, 0x4370, 0x866f, 0xb86e, 0xaf6c, 0x0821, 0x8256, 0x20e5, 0x3c82, 0xce07, 0x9ba6, 0x890d,
         0x047c, 0xfe32, 0x76d3, 0xeaab, 0x39a7, 0xbf81, 0xd5bc, 0x0b08, 0xb843, 0x10c3, 0x1 };
      uint16 r[2*NBEL(a)];
      kmul_dump = true;
      ksqu(r, a, NBEL(a));
   }
#endif

   {
      uint16 r[] = { 0x5279, 0x515c, 0x0b45, 0xe8dd, 0x546a, 0x4be4, 0x8904, 0x0445, 0xb7bf, 0x4505, 0x8c0c, 0x3d83, 0x67ad, 0xeb6f, 0xa781, 0x6600, 0xa566, 0x604d,
         0xb68a, 0xa9ba, 0x7854, 0xaf59, 0x6786, 0x3d2a, 0x41fc, 0x8bf5, 0x9300, 0x4325, 0x13d9, 0xb4e7, 0x59f2, 0x057b, 0xa203, 0xfe7c, 0x52da, 0x1f6b, 0xddb6,
         0xa9c4, 0xc802, 0xaf96, 0x5faa, 0x33e6, 0x05ec, 0x72c4, 0xfcea, 0xedb2, 0x253b, 0x862e, 0xdbe1, 0xfda2, 0x00e6, 0x53cb, 0xe0fc, 0xd727, 0x2853, 0x9c32,
         0xf07a, 0x84bb, 0xda2e, 0x0995, 0x2419, 0x8bd4, 0xc82b, 0xfe10, 0x2cf9, 0xa82e, 0xa113, 0x60a2, 0x8c3b, 0xe3e2, 0xc71f, 0x94f9, 0x9e69, 0x1f5f, 0xf39a,
         0x241f, 0x503a, 0x8ca2, 0x1fe5, 0xbc63, 0x393e, 0xe7fc, 0x2052, 0xcaa5, 0x116d, 0xbb5a, 0x21d3, 0xefb6, 0x5024, 0x2ed7, 0x0712, 0xb236, 0x7d30, 0x22a0,
         0x0001, 0x0000 };
      uint16 p[] = { 0x20a4, 0xb930, 0x3d0e, 0xff15, 0x440f, 0x628d, 0x8c89, 0xcd8b, 0xa6ee, 0xa7ea, 0xaa9e, 0x5ccb, 0xc137, 0xda44, 0x1fa1, 0xf899, 0xd0f3, 0xe053,
         0xc1d6, 0x0cc8, 0xdf6d, 0x2b2f, 0xce87, 0xea2d, 0xb1d5, 0x8f6d, 0xf81b, 0xca3a, 0x1aab, 0xe8c1, 0x2e39, 0xfb4c, 0x7ecb, 0x1c74, 0xe040, 0xed61, 0x731e,
         0xa0b1, 0xe180, 0x5c8b, 0xe1de, 0x41b0, 0xbc3f, 0x3b47, 0x4f90, 0xd752, 0x9a49, 0x862c, 0x0000 };
      index llen = 24;
      uint16 r2[NBEL(r)];
      uint16 p2[NBEL(p)];

      copy(p2, p, NBEL(p));
      copy(r2, r, NBEL(r));
      subu_on_neg(p2, NBEL(p2), r, 2*llen);  // ps = r0 - ps
      addu_on(p2, NBEL(p2), r+2*llen, NBEL(r)-2*llen);  // ps += r2
      // add the final temp into the result
      addu_on(r2+llen, NBEL(r2)-llen, p2, NBEL(p2));

      kara_sub(r, NBEL(r), p, NBEL(p), llen);

      // r[33] = 0x2054 != r2[33] = 0x2053
      // r[53] = 0x2ff1 != r2[53] = 0x2ff0
      // r[66] = 0xf1d3 != r2[66] = 0xf1d2
      // r[68] = 0xb6c7 != r2[68] = 0xb6c6
      //for (unsigned j=0; j<NBEL(r); ++j)
      //   if (r[j] != r2[j]) tout_puts(format("r[%?] = 0x%04x != r2[%?] = 0x%04x\n") << j << r[j] << j << r2[j]);
      classert(compu(r, NBEL(r), r2, NBEL(r2)) == 0);
   }
}
#endif
#endif


// ------ KARATSUBA MULTIPLICATION --------------------------------------------

// unsigned multiplication of two const input numbers 'a' and 'b' into a pre-reserved result 'r'.
// 'a' must be longer than or equal the size of 'b'.
// 'r' must be long enough to hold the 'alen+blen' long result.  there is no padding of the result.
// length must be >= kmul_min_len, numbers don't need to be normalised

// let's call 'a' composed of a1*2^n+a0 and 'b' composed of b1*2^n+b0.
// then we look for r2 = a1*b1, r1 = a1*b0 + a0*b1, r0 = a0*b0.
// if we have r0 and r2, we can calculate r1 as (a1 - a0)(b1 - b0) + r0 + r2.

template<typename small, bool square>
void kmul_ls(small* r, const small* a, index alen, const small* b, index blen) {
   caassert(!square || a == b);
   caassert(!square || alen == blen);

   caassert(r != nullptr);
   caassert(a != nullptr);
   caassert(b != nullptr);
   caassert(alen >= blen);
   caassert((small)-1 > 0);  // must be unsigned type

   if (!use_qkmul<square>(alen, blen)) {
      qomul<small, square>(r, a, alen, square ? a : b, square ? alen : blen);
      return;
   }
   caassert(alen >= kmul_min_len);
   caassert(blen >= kmul_min_len);

   const index llen = blen / 2;  // low part length
   caassert(llen > 0);
   const index ahlen = alen - llen;  // a high part length
   caassert(ahlen > 0);
   const index bhlen = square ? ahlen : blen - llen;  // b high part length
   caassert(bhlen > 0);

   // unbalanced mul lengths
   if (!square && blen < ahlen
#ifndef USE_KARA
       + 1
#endif
      ) {
      //tout_puts(format("unbalanced kmul: %?/%?\n") << alen << blen);
      omul(r, a, alen, b, blen);  // quick and dirty fix
      return;
   }

   // compute r0 = a0 * b0
   // this will lie in 'r' on index 0..2*llen-1
   kmul_ls<small, square>(r, a, llen, square ? a : b, llen);

   // compute r2 = a1 * b1
   // this will lie in 'r' on index 2*llen..alen+blen-1
   kmul_ls<small, square>(r+2*llen, a+llen, ahlen, square ? a+llen : b+llen, square ? ahlen : bhlen);

   // allocate some work space for factors and third mul
   caassert(ahlen >= llen);
   tape_alloc<small> tmp((square ? 3 : 4)*ahlen
#ifndef USE_KARA
      + 1
#endif
      );  // |a1 - a0| will fit in ahlen as well as (a1 - a0)(b1 - b0) + r0 + r2
   small* sa = tmp.p;
   small* sb = square ? 0 : tmp.p + ahlen;

   // subtract values for later multiplication
   // improve: do compare-and-sub in one?
   const bool asign = compu_nn(a+llen, ahlen, a, llen) < 0;  // asign set if a1 < a0
   if (asign) {
      classert(compu_nn(a, llen, a+llen, ahlen) > 0);
      subu(sa, ahlen, a, llen, a+llen, ahlen);  // a0 - a1 > 0
   }
   else {
      classert(compu_nn(a+llen, ahlen, a, llen) >= 0);
      subu(sa, ahlen, a+llen, ahlen, a, llen);  // a1 - a0 >= 0
   }

   const bool bsign = square ? asign : compu_nn(b+llen, bhlen, b, llen) < 0;  // bsign set if b1 < b0
   if (!square) {
      if (bsign) {
         classert(compu_nn(b, llen, b+llen, bhlen) > 0);
         subu(sb, ahlen, b, llen, b+llen, bhlen);  // b0 - b1 > 0
      }
      else {
         classert(compu_nn(b+llen, bhlen, b, llen) >= 0);
         subu(sb, ahlen, b+llen, bhlen, b, llen);  // b1 - b0 >= 0
      }
   }

   // multiply both halves
   small* ps = tmp.p + (square ? 1 : 2)*ahlen;
   const index plen = 2*ahlen
#ifndef USE_KARA
      + 1
#endif
      ;
   qkmul<small, square>(ps, sa, ahlen, square ? sa : sb, ahlen);
#ifndef USE_KARA
   ps[plen-1] = 0;
#endif

#ifdef KMUL_CHECK_DUMP
   if (kmul_dump) {
      tout_puts("r[] = { ");
      for (unsigned j=0; j<alen+blen; ++j)
         tout_puts(format("0x%04x, ") << r[j]);
      tout_puts("};\n");
      tout_puts("p[] = { ");
      for (unsigned j=0; j<plen; ++j)
         tout_puts(format("0x%04x, ") << ps[j]);
      tout_puts("};\n");
      tout_puts(format("llen = %?\n") << llen);
      kmul_dump = false;
   }
#endif
#ifdef USE_KARA
   if (square || asign == bsign) kara_sub(r, alen+blen, ps, plen, llen);
   else kara_add(r, alen+blen, ps, plen, llen);
#else
   if (square || asign == bsign) {
      // add r2 + r0 - ps as middle result
      subu_on_neg(ps, plen, r, 2*llen);  // ps = r0 - ps
   }
   else {
      // add r0 + r2 + ps as middle result
      addu_on(ps, plen, r, 2*llen);  // ps += r0
      // improve: maybe ps[plen-1] is always == 0 here?
   }
   addu_on(ps, plen, r + 2*llen, ahlen + bhlen);  // ps += r2
   // add the final temp into the result
   addu_on(r+llen, ahlen + blen, ps, plen);
#endif
}


#ifdef CL_TEST_CODE
static void test_kmul() {
   const index old = switch_kmul(always_on);
   {
      uint16 a[] = { 0x10, 0x11, 0x12, 0x13 };
      uint16 b[] = { 0x20, 0x21, 0x22, 0x23 };
      uint16 r[] = { 0, 0, 0, 0, 0, 0, 0, 0 };

      kmul(r, a, 4, b, 4);

      uint16 rc[] = { 0x0200, 0x0430, 0x0691, 0x0924, 0x072a, 0x04fc, 0x0299, 0x0000 };
      if (memcmp(r, rc, sizeof(r)) != 0) {
         DTOUT_PUTS(format("is    =%?\n") << dump_hex(r, NBEL(r)));
         DTOUT_PUTS(format("should=%?\n") << dump_hex(rc, NBEL(r)));
         classertm(0, "kmul bug!");
      }
   }
   {
      uint16 a[] = { 0xff10, 0xff11, 0xff12, 0xff13 };
      uint16 b[] = { 0xff20, 0xff21, 0xff22, 0xff23 };
      uint16 r[] = { 0, 0, 0, 0, 0, 0, 0, 0 };

      kmul(r, a, 4, b, 4);

      uint16 rc[] = { 0xd200, 0xa060, 0x6cf5, 0x37be, 0x63fc, 0x959e, 0xc907, 0xfe38 };
      if (memcmp(r, rc, sizeof(r)) != 0) {
         DTOUT_PUTS(format("is    =%?\n") << dump_hex(r, NBEL(r)));
         DTOUT_PUTS(format("should=%?\n") << dump_hex(rc, NBEL(r)));
         classertm(0, "kmul bug!");
      }
   }
   {
      uint16 a[] = { 0x2702, 0x221f, 0xad42, 0xa94f };
      uint16 r[] = { 0, 0, 0, 0, 0, 0, 0, 0 };
      ksqu(r, a, 4);

      uint16 rc[] = { 0, 0, 0, 0, 0, 0, 0, 0 };
      osqu(rc, a, 4);
      if (memcmp(r, rc, sizeof(r)) != 0) {
         DTOUT_PUTS(format("is    =%?\n") << dump_hex(r, 8));
         DTOUT_PUTS(format("should=%?\n") << dump_hex(rc, 8));
         classertm(0, "kmul bug!");
      }
   }
   {
      uint16 b[] = { 0xcfbd, 0x61ec, 0xdd8e, 0x68a3 };
      uint16 c[] = { 0xf6bf, 0x840b, 0x8ad0, 0x11f3, 1};
      uint16 r[] = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
      kmul(r, b, 4, c, 5);

      uint16 rc[] = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
      omul(rc, b, 4, c, 5);
      if (memcmp(r, rc, sizeof(r)) != 0) {
         DTOUT_PUTS(format("is    =%?\n") << dump_hex(r, 8));
         DTOUT_PUTS(format("should=%?\n") << dump_hex(rc, 8));
         classertm(0, "kmul bug!");
      }
   }
   // test different lengths
   {
      uint16 a[] = { 0xff10, 0xff11 };
      uint16 b[] = { 0xff20 };
      uint16 r[NBEL(a)+NBEL(b)];

      kmul(r, a, NBEL(a), b, NBEL(b));

      uint16 rc[NBEL(a)+NBEL(b)] = { 0xd200, 0xcf50, 0xfe32 };
      if (memcmp(r, rc, sizeof(r)) != 0) {
         DTOUT_PUTS(format("is    =%?\n") << dump_hex(r, NBEL(rc)));
         DTOUT_PUTS(format("should=%?\n") << dump_hex(rc, NBEL(rc)));
         classertm(0, "kmul bug!");
      }
   }
   // test very different lengths
   {
      uint16 a[] = { 0xff10, 0xff11, 0xff12, 0xff13 };
      uint16 b[] = { 0xff20 };
      uint16 r[NBEL(a)+NBEL(b)];

      kmul(r, a, NBEL(a), b, NBEL(b));

      uint16 rc[NBEL(a)+NBEL(b)] = { 0xd200, 0xcf50, 0xce72, 0xcd93, 0xfe34 };
      if (memcmp(r, rc, sizeof(r)) != 0) {
         DTOUT_PUTS(format("is    =%?\n") << dump_hex(r, NBEL(rc)));
         DTOUT_PUTS(format("should=%?\n") << dump_hex(rc, NBEL(rc)));
         classertm(0, "kmul bug!");
      }
   }
   switch_kmul(old);
}

static void test_ksqu() {
   const index old = switch_ksqu(always_on);
   {
      uint16 a[] = { 0x10, 0x11, 0x12, 0x13 };
      uint16 r[2*NBEL(a)];
      ksqu(r, a, NBEL(a));
      string s = dump_hex(r, NBEL(r));
      classert(s == "0x16902ac03ca04c4036102200100");
   }
   {
      uint16 a[] = { 0x10, 0x11 };
      uint16 r[2*NBEL(a)];
      ksqu(r, a, NBEL(a));
      string s = dump_hex(r, NBEL(r));
      classert(s == "0x12102200100");
   }
   {
      uint16 a[] = { 0x10, 0x11, 0xff, 0xfe, 1, 2, 3, 4, 5, 6, 0x55, 0xaa };
      uint16 r[2*NBEL(a)];
      ksqu(r, a, NBEL(a));
      string s = dump_hex(r, NBEL(r));
      classert(s == "0x70e470e424310aa008c606e004ef02f55249fb90cbf5367e1d5c0f5a0b1b06defea7fa671fdd419e210102200100");
   }
   switch_ksqu(old);
}
#endif


#ifdef CL_TEST_CODE
void _CL_CALL test_bignum_kmul() {
#ifdef USE_KARA
   test_kara_add();
   test_kara_sub();
#endif
   test_kmul();
   test_ksqu();
}
#endif


// ------ generate template functions -----------------------------------------

index dummy_global;

//#if _MSC_VER < 1300
#pragma inline_depth(0)

template<typename small>
void generate_bignum_kmuls() {
   kmul<small>(0, 0, 0, 0, 0);
   ksqu<small>(0, 0, 0);
   dummy_global = kmul_mem<small, true>(*(index*)0, 0) + kmul_mem<small, false>(*(index*)0, 0);
}

void _generate_bignum_kmuls() {
   classertm(0, "must not be called");
   generate_bignum_kmuls<uint8>();
   generate_bignum_kmuls<uint16>();
   generate_bignum_kmuls<uint32>();
#ifdef _M_X64
   generate_bignum_kmuls<uint64>();
#endif
}

#pragma inline_depth()
//#endif

}  // namespace bignum
