/*
 *  arbitrary length number arithmetic - base_crossover_data settings
 */

// $+HEADER$
// 
// Copyright 2015 Christoph Lueders
// 
// This file is part of the BIGNUM Library.
// 
// The BIGNUM Library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
// 
// The BIGNUM Library is distributed in the hope that it will be useful, but 
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public 
// License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with the BIGNUM Library.  If not, see <http://www.gnu.org/licenses/>.
// 
// $-HEADER$

// $Date: 2015-04-24 11:47:36 +0200 (Fr, 24 Apr 2015) $
// $Rev: 26592 $

#ifndef CL_BIGNUM_CALIBRATE_H
#define CL_BIGNUM_CALIBRATE_H 1

#ifndef CL_CLLIB_H
#include "cl/cllib.h"
#endif

#pragma pack(push, 8)  // arbitrary but fixed

namespace bignum {

struct base_crossover_data {
   const string name;
   const index low_value;
   const index high_value;
   vector<index> init_thresh;
   vector<index> thresh;

   base_crossover_data(const string _name, const index _low_value, const index _high_value, const unsigned start, const index* vals);
   void clear();
   void init();
   vector<index> set(const vector<index>& vals);
   void set_vec(vector<index>& th, const unsigned start, const index* vals);
   void set_init(const unsigned start, const index* vals);
   string setting_string() const;
   string setting_string_short() const;
};

struct mod_crossover_data : public base_crossover_data {
   mod_crossover_data(const string _name, const unsigned start, const index* vals) : base_crossover_data(_name, -1, -1, start, vals) { }
   vector<index> set_val(index val);
   bool use(const index klen, const unsigned lg) {
      return klen >= thresh[lg];
   }
};

struct choose_crossover_data : public base_crossover_data {
   choose_crossover_data(const string _name, const unsigned start, const index* vals) : base_crossover_data(_name, 1, 0, start, vals) { }
   unsigned choose(const index rlen);
};

extern choose_crossover_data smul_calib;
extern choose_crossover_data ssqu_calib;

static inline unsigned choose_lg(const index rlen, const bool square) {
   return (square ? ssqu_calib : smul_calib).choose(rlen);
}

}  // namespace bignum

#pragma pack(pop)

#endif  // ndef CL_BIGNUM_CALIBRATE_H
