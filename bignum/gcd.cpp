// $+HEADER$
// 
// Copyright 2015 Christoph Lueders
// 
// This file is part of the BIGNUM Library.
// 
// The BIGNUM Library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
// 
// The BIGNUM Library is distributed in the hope that it will be useful, but 
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public 
// License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with the BIGNUM Library.  If not, see <http://www.gnu.org/licenses/>.
// 
// $-HEADER$

// $Date: 2015-04-24 11:28:16 +0200 (Fr, 24 Apr 2015) $
// $Rev: 26590 $

/*
 *  arbitrary length number arithmetic - (E)GCD
 */

#include "precomp.h"
#include "bignum_t.h"
#include "cl/int_math.h"


// we must switch off "unreachable code" warning, since bool template parameters generate code paths that are never used
#pragma warning (disable:4702)

namespace bignum {

   template<typename T>
   static void gcd_signed_sub(const T& a, const T& b, const bool as, const bool bs, T& diff, bool& diffs) {
      if (as == bs) {
         if (a < b) {
            diff = b - a;
            diffs = !as;
         }
         else {
            diff = a - b;
            diffs = as;
         }
      }
      else {
         diff = a + b;
         diffs = as;
      }
   }

#ifdef CL_TEST_CODE
   static void test_gcd_signed_sub() {
      unsigned d;
      bool sign;
      gcd_signed_sub(3u, 2u, false, false, d, sign);     // 3 - 2 = 1
      classert(sign == false && d == 1);
      gcd_signed_sub(3u, 2u, true, false, d, sign);      // -3 - 2 = -5
      classert(sign == true && d == 5);
      gcd_signed_sub(3u, 2u, false, true, d, sign);      // 3 - -2 = 5
      classert(sign == false && d == 5);
      gcd_signed_sub(3u, 2u, true, true, d, sign);       // -3 - -2 = -1
      classert(sign == true && d == 1);

      gcd_signed_sub(2u, 3u, false, false, d, sign);     // 2 - 3 = -1
      classert(sign == true && d == 1);
      gcd_signed_sub(2u, 3u, true, false, d, sign);      // -2 - 3 = -5
      classert(sign == true && d == 5);
      gcd_signed_sub(2u, 3u, false, true, d, sign);      // 2 - -3 = 5
      classert(sign == false && d == 5);
      gcd_signed_sub(2u, 3u, true, true, d, sign);       // -2 - -3 = 1
      classert(sign == false && d == 1);
   }
#endif



#define x (x_val[i])
#define y (y_val[i])
#define x_last (x_val[1-i])
#define y_last (y_val[1-i])
#define x_new x_last
#define y_new y_last

#define x_sign (x_valsign[i])
#define y_sign (y_valsign[i])
#define x_last_sign (x_valsign[1-i])
#define y_last_sign (y_valsign[1-i])
#define x_new_sign x_last_sign
#define y_new_sign y_last_sign

// #define EGCD_DEBUG 1
   template<typename T>
   void egcd_with_sign(T a, T b, T& gg, T& xx, bool& xx_sign, T& yy, bool& yy_sign) {
#ifdef EGCD_DEBUG
      int64 _x = 1, _y = 0, _x_last = 0, _y_last = 1;
#endif
      /* int x = 1, y = 0;     int xLast = 0, yLast = 1;     int q, r, m, n; */
      unsigned i = 1;  // index into arrays
      T x_val[2] = { 0, 1 };
      T y_val[2] = { 1, 0 };
      bool x_valsign[2] = { false, false };
      bool y_valsign[2] = { false, false };

#ifdef EGCD_DEBUG
      DTOUT_PUTS(format("me: x=%?%? y=%?%? xlast=%?%? ylast=%?%?\n") << (x_sign ? "-" : "") << x << (y_sign ? "-" : "") << y << (x_last_sign ? "-" : "") << x_last << (y_last_sign ? "-" : "") << y_last);
      DTOUT_PUTS(format("ok: x=%? y=%? xlast=%? ylast=%?\n") << _x << _y << _x_last << _y_last);
#endif

      while (a != 0) {
         const T q = b / a;    // quotient
         const T r = b % a;    // remainder

         /* x_new = x_last - q*x;
         y_new = y_last - q*y;
         x_last = x;
         y_last = y;
         x = x_new;
         y = y_new; */

#ifdef EGCD_DEBUG
         int64 _x_new = _x_last - (int64)q * _x;
         int64 _y_new = _y_last - (int64)q * _y;
         _x_last = _x;
         _y_last = _y;
         _x = _x_new;
         _y = _y_new;
#endif

         classert(!cl_limits<T>::has_limits || int_log2(q) + int_log2(x) <= bits(T));
         const T qx = q*x;
         classert(!cl_limits<T>::has_limits || int_log2(q) + int_log2(y) <= bits(T));
         const T qy = q*y;
         gcd_signed_sub(x_last, qx, x_last_sign, x_sign, x_new, x_new_sign);
         gcd_signed_sub(y_last, qy, y_last_sign, y_sign, y_new, y_new_sign);
         i = 1-i;                   // switch last and actual x's and y's
#ifdef EGCD_DEBUG
         DTOUT_PUTS(format("me: x=%?%? y=%?%? xlast=%?%? ylast=%?%?\n") << (x_sign ? "-" : "") << x << (y_sign ? "-" : "") << y << (x_last_sign ? "-" : "") << x_last << (y_last_sign ? "-" : "") << y_last);
         DTOUT_PUTS(format("ok: x=%? y=%? xlast=%? ylast=%?\n") << _x << _y << _x_last << _y_last);
#endif
         b = a;
         a = r;
      }
      gg = b;
      xx = x_last;
      yy = y_last;
      xx_sign = x_last_sign;
      yy_sign = y_last_sign;
   }

#undef x
#undef y
#undef x_last
#undef y_last
#undef x_sign
#undef y_sign
#undef x_last_sign
#undef y_last_sign
#undef signed_sub



   template<typename T>
   T mod_inv(T a, T m) {
      T g, x, y;
      bool x_sign, y_sign;
      egcd_with_sign(a, m, g, x, x_sign, y, y_sign);
      const T r = x_sign ? m - x : x;
      return r;
   }

#ifdef CL_TEST_CODE
   template<typename T>
   static void test_modinv2() {
      T i;
      i = mod_inv(4, 13);
      classert(i == 10);
      if (!cl_limits<T>::has_limits || sizeof(T) >= sizeof(uint16)) {
         i = mod_inv<T>(10, 251);
         classert(i == 226);
         classert((bignum_t(10) * bignum_t(i)) % 251 == 1);
      }
      if (!cl_limits<T>::has_limits || sizeof(T) >= sizeof(uint32)) {
         i = mod_inv<T>(10, 44497);
         classert(i == 31148);
         classert((bignum_t(10) * bignum_t(i)) % 44497 == 1);
      }
      if (!cl_limits<T>::has_limits || sizeof(T) >= sizeof(uint32)) {
         i = mod_inv<T>(32, 1489660919);
         classert(i == 1163797593);
         classert((bignum_t(32) * bignum_t(i)) % 1489660919 == 1);
      }
      if (!cl_limits<T>::has_limits || sizeof(T) >= sizeof(uint64)) {
         i = mod_inv<T>(32, 3489660929);
         classert(i == 3380609025);
         classert((bignum_t(32) * bignum_t(i)) % 3489660929 == 1);
      }
      if (!cl_limits<T>::has_limits || sizeof(T) >= sizeof(uint64)) {
         i = mod_inv<T>(10, 4000000063);
         classert(i == 1200000019);
         classert((bignum_t(10) * bignum_t(i)) % 4000000063 == 1);
      }
   }

   void test_mod_inv() {
      test_modinv2<int>();
      test_modinv2<unsigned>();
      test_modinv2<uint64>();
      test_modinv2<bignum_t>();
      test_modinv2<ubignum_t>();
   }
#endif


#ifdef CL_TEST_CODE
void _CL_CALL test_bignum_gcd() {
   test_gcd_signed_sub();
   test_mod_inv();
}
#endif


// ------ generate template functions -----------------------------------------

#pragma inline_depth(0)

template<typename small>
void generate_bignum_gcd() {
   small u = 0;
   mod_inv(u, u);
}

void _generate_bignum_gcd() {
   classertm(0, "must not be called");
   generate_bignum_gcd<int8>();
   generate_bignum_gcd<int16>();
   generate_bignum_gcd<int32>();
   generate_bignum_gcd<int64>();
   generate_bignum_gcd<uint8>();
   generate_bignum_gcd<uint16>();
   generate_bignum_gcd<uint32>();
   generate_bignum_gcd<uint64>();
   generate_bignum_gcd<bignum_t>();
   generate_bignum_gcd<ubignum_t>();
   generate_bignum_gcd<basic_bignum<uint8>>();
   generate_bignum_gcd<basic_bignum<uint16>>();
   generate_bignum_gcd<basic_bignum<uint32>>();
   generate_bignum_gcd<basic_bignum<uint8,false>>();
   generate_bignum_gcd<basic_bignum<uint16,false>>();
   generate_bignum_gcd<basic_bignum<uint32,false>>();
}

#pragma inline_depth()

}  // namespace bignum
