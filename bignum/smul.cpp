// $+HEADER$
// 
// Copyright 2015 Christoph Lueders
// 
// This file is part of the BIGNUM Library.
// 
// The BIGNUM Library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
// 
// The BIGNUM Library is distributed in the hope that it will be useful, but 
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public 
// License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with the BIGNUM Library.  If not, see <http://www.gnu.org/licenses/>.
// 
// $-HEADER$

// $Date: 2015-04-24 11:28:16 +0200 (Fr, 24 Apr 2015) $
// $Rev: 26590 $

// $dissect$ smul-support.txt
/*
 *  arbitrary length number arithmetic - sch�nhage-strassen fft
 */

#include "precomp.h"

#include "impl.h"
#include "small.h"
#include "fft.h"
#include "smul.h"
#include "cl/format.h"
#include "cl/strutil.h"
#ifdef CL_TEST_CODE
#include "cl/lcm.h"
#endif

#ifdef SMUL_PROFILE
#include "cl/hrtime.h"
#define PROF(a) a
#else
#define PROF(a) NOP()
#endif


//#define SMUL_PROFILE 1
bool do_profiling = false;


// we must switch off "unreachable code" warning, since bool template parameters generate code paths that are never used
#pragma warning (disable:4702)

namespace bignum {

vector<smul_params> smul_opt;
vector<smul_params> ssqu_opt;


#ifdef CL_TEST_CODE
static void test_smul_choose_lg() {
   unsigned i;
   i = smul_calib.choose(1);
   classert(i > 0);
   i = smul_calib.choose(2);
   classert(i > 0);
   i = smul_calib.choose(smul_calib.thresh[7] - 1);
   classert(i == 7);
   unsigned j = (unsigned)smul_calib.thresh.size() - 1;
   while (smul_calib.thresh[j] == 0) --j;
   ++j;
   i = smul_calib.choose(smul_calib.thresh[j - 1] + 1);
   classert(i == j);
}
#endif



// ------ calc mem for SCH�NHAGE-STRASSEN FFT MUL ----------------------------

template<typename small>
index smul_mem2(const index ll, const index mm, const index kk, bool square, bool negacyclic) {
   auto smulmem = [=] () {
      return square ? smul_mem<small, true>(kk, kk, negacyclic) : smul_mem<small, false>(kk, kk, negacyclic);
   };
   auto t3mulmem = [=] () {
      return square ? t3mul_mem<small, true>(kk, kk) : t3mul_mem<small, false>(kk, kk);
   };
   return (kk + mm*kk + (!square || negacyclic ? mm*kk : 0)) * sizeof(small) +
      (kk < ll ? smulmem() : 2*kk * sizeof(small) + t3mulmem());
}

template<typename small, bool square>
index smul_mem(const index alen, const index blen, bool negacyclic) {
   caassert(!square || alen == blen);

   if (!use_qsmul<square>(alen, blen)) return t3mul_mem<small, square>(alen, blen);

   // compute fft parameters:
   unsigned m;  // fft depth
   index mm; // mm == 2^m, number of pieces of each number and number of lower elements
   index l;  // smalls per piece
   index k;  // exponent of modulo ring 2^k+1
   smul_set_params<small>(square, alen, blen, m, mm, l, k);
   const index kk = k / bits(small) + 1;  // smalls per lower element

   return smul_mem2<small>(blen, mm, kk, square, negacyclic);
}


// $dissect$ smul-core.txt
// ------ choose smul params --------------------------------------------------

// lg: fft depth  (DA: m)
// mm: mm == 2^lg, number of pieces of each number and number of lower elements  (DA: n)
// l:  small's per piece  (DA: s bit)
// k:  exponent of modulo ring 2^k+1

template<typename small>
bool smul_set_params2(const index ll, const unsigned lg, index& mm, index& l, index& k) {
   if (lg >= bits(index)) return false;  // too many parts

   // fft size
   mm = (index)1 << lg;
   // words per part
   l = (ll + mm - 1) / mm;
   // outer ring size in bits
   //N = mm * l * bits(small);
   //if (l*bits(small) >= (2*lg - 1) * mm) return false;  // bad fft size

   // inner ring size in bits
   // the "-1" is because we have max 2*l/2 convolution sums != 0, but it gets eaten up my the FFT length of 2^m+1
   // i think 2 extra bits would do, but it doesn't make any difference, since it's rounded up to words anyway
   const index k1 = 2*l*bits(small) + lg + 3;  // sch� wants 3 extra bits, just give them to him
   // for negacyclic convolution the ring size has to be a multiple of both fft size and word size
   // for acyclic/cyclic convolution the ring size has to be a multiple of both half the fft size and word size
   const index a = max(mm/2, bits(small));
   classert(is_pow2(a));
   k = (k1 + a - 1) & ~(a - 1);

   classert(is_pow2(mm));  // must be power of 2 for the fft evaluation divide and conquer
   caassert(k % bits(small) == 0);  // inner ring size must be a multiple of word size
   caassert(l*bits(small) < k);  // part length must be strictly smaller than inner ring size
   caassert(2*l*bits(small) + lg <= k);  // there must be no overflow when parts are added up in convolution
   caassert(2*k >= mm);  // we need mm roots of unity and 2^k+1 gives us 2*k roots
   caassert(2*k/mm*mm == 2*k);  // #parts divides k, otherwise roots don't properly square up for next recursion
   caassert(mm*k >= ll * bits(small));  // part length must be big enough so all of the output is covered

   return true;
}


// lg: fft depth
// mm: mm == 2^lg, number of pieces of each number and number of lower elements
// l:  smalls per piece
// k:  exponent of modulo ring 2^k+1

template<typename small>
bool smul_set_params(const bool square, const index alen, const index blen, unsigned& lg, index& mm, index& l, index& k) {
   caassert(!square || alen == blen);

   const index ll = 2*tmax(alen, blen);
   vector<smul_params>& opt = qsmul_opt(square);
   for (size_t i=0; i<opt.size(); ++i) {
      const smul_params& o = opt[i];
      if (o.sz == ll) {
         lg = o.lg;
         mm = (index)1 << lg;
         k = o.k;
         l = (ll + mm - 1) / mm;
         return true;
      }
   }
   lg = choose_lg(ll, square);
   const bool ok = smul_set_params2<small>(ll, lg, mm, l, k);
   caassert(ok);
   return ok;
}

// $dissect$ smul-support.txt
// ------ display fft parameters ---------------------------------------------

template<typename small>
void smul_print_params(const index bit, const unsigned fft_lg, const bool square, const bool optimise, uint64* iter_time) {
   const index ll = (bit + bits(small) - 1) / bits(small);
   unsigned lg;
   index mm;
   index l;
   index k;
   smul_set_params<small>(square, ll, ll, lg, mm, l, k);

   string s;
   strformat ss(s);
   ss("input length of each factor is %'? words (%'? bits)\n") << ll << bit;

   if (fft_lg > 1) {
      lg = fft_lg;
      const bool ok = smul_set_params2<small>(2*ll, lg, mm, l, k);
      caassert(ok);
   }
   else if (optimise) {
      ss("fft crossover table chooses size of %'? (2^%?) coefficients\n") << mm << lg;
      ss("\n");
      tout_puts(s);
      lg = smul_optimise<small>(ll, square, true, &k, iter_time);
      const bool ok = smul_set_params<small>(square, ll, ll, lg, mm, l, k);
      caassert(ok);
      s = "\n";
   }

   ss("outer ring size N is %'? words (%'? bits)\n") << mm*l << mm*l*bits(small);
   ss("fft size is %'? (2^%?) coefficients\n") << mm << lg;
   ss("input coefficent size is %'? words (%'? bits)\n") << l << l * bits(small);
   ss("inner ring size K is %'? words (%'? bits)\n") << k / bits(small) << k;
   const int64 waste = 10000ui64 * mm * (k + bits(small)) / (4*bit) - 10000;
   ss("waste is %,.02?%%\n") << waste;
   ss("primitive 2^%?-th root is 2^%?\n") << lg << 2*k/mm;

   const index kk = k / bits(small) + 1;  // smalls per lower element
   auto mem = smul_mem2<small>(ll, mm, kk, square, false);
   ss("temporary memory needed is %?b\n") << scaled_count(mem, SCT_MAX4);
   tout_puts(s);
}


// $dissect$ smul-core.txt
// ------ SMUL MOD 2^N+1 ------------------------------------------------------

// ------ choose smul params based on given ring size -------------------------

// n:  ring size in bits!  (DA: N)
// lg: fft depth  (DA: m)
// mm: mm == 2^lg, number of pieces of each number and number of lower elements  (DA: n)
// l:  length in small's of the input coefficients  (DA: s bit)
// k:  exponent of modulo ring 2^k+1

template<typename small>
void smul_mod_set_params2(const index n, const unsigned& lg, index& mm, index& l, index& k) {
   caassert(n % bits(small) == 0);
   caassert(lg < bits(small));

   mm = (index)1 << lg;
   classert((n & (mm * bits(small) - 1)) == 0);  // input can be cut this way into coeffs
   classert(lg >= 2);

   l = n / bits(small) / mm;  // l = length in small's of the input coefficients

   // inner ring size in bits
   // the "-1" is because we have max 2*l/2 convolution sums != 0, but it gets eaten up my the FFT length of 2^m+1
   // i think 2 extra bits would do, but it doesn't make any difference, since it's rounded up to words anyway
   const index k1 = 2*l*bits(small) + lg + 3;  // sch� wants 3 extra bits, just give them to him
   // for negacyclic convolution the ring size has to be a multiple of both fft size and word size
   const index a = max(mm, bits(small));  // both are powers of 2
   classert(is_pow2(a));
   k = (k1 + a - 1) & ~(a - 1);

   classert(is_pow2(mm));  // must be power of 2 for the fft evaluation divide and conquer
   caassert(l < k);  // part length must be strictly smaller than inner ring size
   caassert(2*l + lg <= k);  // there must be no overflow when parts are added up in convolution
   caassert(2*k >= mm);  // we need mm roots of unity and 2^k+1 gives us 2*k roots
   caassert(2*k/mm*mm == 2*k);  // #parts divides k, otherwise roots don't properly square up for next recursion
   caassert(mm*k >= n);  // part length must be big enough so all of the output is covered
}

template<typename small>
bool smul_mod_params(const index n, const bool square, unsigned& lg, index& mm, index& l, index& k) {
   caassert(n % bits(small) == 0);
   const index ll = n / bits(small);
   smul_calib_data& cdata = square ? ssqu_mod_lg : smul_mod_lg;
   lg = cdata.get_lg(ll);
   if (lg == 0) return false;  // can't use this size or it doesn't pay off speed-wise
   smul_mod_set_params2<small>(n, lg, mm, l, k);
   return true;
}


// $dissect$ smul-support.txt
// ------ display mod fft parameters -----------------------------------------

template<typename small>
void smul_mod_print_params2(index n, const bool square, const bool optimise) {
   const index ll = (n + bits(small) - 1) / bits(small);
   n = ll * bits(small);  // sanitise input, n should be divisible

   string s;
   strformat ss(s);
   ss("outer ring size N is %? words (%'? bits)\n") << ll << n;

   unsigned lg;
   index mm;
   index l;
   index k;
   if (!smul_mod_params<small>(n, square, lg, mm, l, k)) {
      ss("no suitable parameters could be found!\n");
      tout_puts(s);
      return;
   }

   ss("fft size is %'? (2^%?) coefficients\n") << mm << lg;
   ss("input coefficent size is %'? words (%'? bits)\n") << l << l * bits(small);
   ss("inner ring size K is %'? words (%'? bits)\n") << k / bits(small) << k;
   const uint64 waste = 10000ui64 * mm * (k + bits(small)) / (2*n) - 10000;
   ss("waste is %,.02?%%\n") << waste;
   ss("primitive 2^%?-th root is 2^%?\n") << lg << 2*k/mm;
   tout_puts(s);
}


// $dissect$ smul-core.txt
// ------ the smul_mod itself -------------------------------------------------

template<typename small, bool square>
void smul_mod(small* r, const small* a, const small* b, const index len, const index n) {
   caassert(len == n / bits(small) + 1);  // len must be big enough to hold 2^n
   caassert(!square || a == b);

   unsigned lg;  // fft depth
   index mm; // mm == 2^lg, number of pieces of each number and number of lower elements
   index l;  // smalls per piece
   index k;  // inner ring size
   if (!smul_mod_params<small>(n, square, lg, mm, l, k)) {
      qt3mul_mod<small, square>(r, a, b, len, n);
      return;
   }

   // call the main routine
   qsmul2<small, square>(r, len, a, len, b, len, lg, mm, l, k, true);
   classert(cmp_pow2(r, len, n) <= 0);
}

#ifdef CL_TEST_CODE
static void test_smul_mod() {
   auto save = smul_mod_lg;
   smul_mod_lg.always_on();
   {
      uint8 a[] = { 0xf1, 0xf2, 0xf3, 0xf4, 0 };  // 0xf4f3f2f1
      uint8 b[NBEL(a)] = { 0xf2, 0xf3, 0xf4, 0xf5, 0 };  // 0xf5f4f3f2
      uint8 r[NBEL(a)];
      // 0xf4f3f2f1 * 0xf5f4f3f2 = 0xeb57d96f50ea6ad2
      // 0xeb57d96f50ea6ad2 % (2**32+1) = 0x65929164
      smul_mod<uint8, false>(r, a, b, NBEL(a), 32);
      string s = dump_hex(r, NBEL(r));
      classert(s == "0x65929164");
   }
   {
      uint8 a[] = { 1, 2, 3, 4, 0 };  // 0x4030201
      uint8 b[NBEL(a)] = { 2, 3, 4, 5, 0 };  // 0x5040302
      uint8 r[NBEL(a)];
      // 0x4030201 * 0x5040302 = 0x141f221e100702
      // 0x141f221e100702 % (2**32+1) = 0x1dfbe7e0
      smul_mod<uint8, false>(r, a, b, NBEL(a), 32);
      string s = dump_hex(r, NBEL(r));
      classert(s == "0x1dfbe7e0");
   }
   smul_mod_lg = save;
}
#endif


// $dissect$ smul-support.txt
// ------ bit reversal --------------------------------------------------------

#ifdef CL_TEST_CODE
static void test_bit_rev() {
   classert(bit_rev(1u, 3) == 4);
   classert(bit_rev(1u, 4) == 8);
   classert(bit_rev(1u, 5) == 16);
   classert(bit_rev(1u, 32) == 0x80000000u);
   for (int i=0; i<100; ++i) {
      const unsigned b = lib_rand.get(63) + 1;
      const uint64 rnd = (lib_rand.get() ^ (lib_rand.get() << (b > 19 ? b - 19 : 0)));  // try to make sure that n has bits at both ends (lib_rand generates some 19 bits)
      const uint64 n = rnd & ((1ui64 << b) - 1);
      classert(bit_rev(bit_rev(n, b), b) == n);
   }
}
#endif


// $dissect$ smul-core.txt
// recurse either again into smul() or use conventional mul with modulo
// all three numbers a, b, and (!) r have length of len smalls
template<typename small, bool square>
static void smul_modmul(small *r, const small* a, const small* b, const index len, const index k, const index org_len) {
   caassert(len * bits(small) >= k + 1);  // len must be big enough to hold 2^k
   caassert(!square || a == b);

   if (len < org_len) smul_mod<small, square>(r, a, b, len, k);
   else qt3mul_mod<small, square>(r, a, b, len, k);
   classert(cmp_pow2(r, len, k) <= 0);
}

#ifdef CL_TEST_CODE
static void test_smul_modmul() {
   uint16 a[] = { 1, 2, 0 };
   uint16 b[] = { 3, 4, 0 };
   uint16 r[NBEL(a)];
   smul_modmul<uint16, false>(r, a, b, NBEL(a), (NBEL(a)-1) * bits(a[0]), 4);
   string s = dump_hex(r, NBEL(r));
   classert(s == "0x9fffb");
}
#endif


// ------ SCH�NHAGE-STRASSEN FFT MUL -----------------------------------------

#pragma warning(push)
#pragma warning(disable:4701)

// init inner coefficients from input number - cyclic convolution
template<typename small>
static void smul_init_cyclic(small* v, const small* a, index len, const index l, const unsigned lg, const index mm, const index kk) {
   caassert(mm > 0);
   caassert(1ui64 << lg == mm);
   caassert(kk > l);

   index i = 0;  // source index
   index ii = 0;  // source offset
   const small* s;  // source pointer
   index j = 0;  // dest index
   small* d;  // dest pointer
   const index mm1 = mm - 1;
   const index maxi = tmin(mm1, len / l);

   // copy input values that are there in full
   while (i < maxi) {
      j = bit_rev(i, lg);  // dest index
      s = a + ii;  // source pointer
      d = v + j*kk;  // dest pointer
      caassert(ii+l <= len);  // main case: full input value there
      copy(d, s, l);
      caassert(kk > l);
      zero(d+l, kk-l);
      ++i;
      ii += l;
   }

   // highest coefficient has one more word that could hold the 1 of 2^n
   if (i == mm1 && ii+l+1 <= len) {
      j = bit_rev(i, lg);  // dest index
      s = a + ii;  // source pointer
      d = v + j*kk;  // dest pointer
      caassert(ii+l+1 <= len);  // main case: full input value there
      copy(d, s, l+1);
      caassert(kk >= l+1);
      zero(d+l+1, kk-l-1);
      ++i;
   }
   // copy the one part that may be there only partly
   else if (ii <= len) {  // parts of input value there
      j = bit_rev(i, lg);  // dest index
      s = a + ii;  // source pointer
      d = v + j*kk;  // dest pointer
      caassert(len >= ii);
      const index have = len - ii;
      caassert(have >= 0);
      copy(d, s, have);
      caassert(kk > have);
      zero(d+have, kk-have);
      ++i;
   }

   // the other inputs are zero
   while (i < mm) {
      j = bit_rev(i, lg);  // dest index
      d = v + j*kk;
      zero(d, kk);  // input value not there altogether
      ++i;
   }
}

// init inner coefficients from input number - negacyclic convolution
template<typename small>
static void weigh_copy(small* r, const index rlen, const small* a, const index alen, const index k, const index shift_count) {
   tape_alloc<small> tmp(rlen);
   small* t = tmp.p;
   copy(t, a, alen);
   zero(t, rlen, alen);
   smul_mul_pow2(r, t, rlen, k, shift_count);
}

template<typename small>
static void smul_init_negacyclic(small* v, const small* a, index len, const index l, const unsigned lg, const index mm, const index kk, const index k) {
   caassert(mm > 0);
   caassert(1ui64 << lg == mm);
   caassert(kk > l);
   caassert((kk - 1) * bits(small) == k);

   index i = 0;  // source index
   index ii = 0;  // source offset
   const small* s;  // source pointer
   index j = 0;  // dest index
   small* d;  // dest pointer
   const index mm1 = mm - 1;
   const index maxi = tmin(mm1, len / l);
   index sh = 0;
   const index ww = k / mm;  // not 2*k!

   // copy input values that are there in full
   while (i < maxi) {
      j = bit_rev(i, lg);  // dest index
      s = a + ii;  // source pointer
      d = v + j*kk;  // dest pointer
      caassert(ii+l <= len);  // main case: full input value there
      weigh_copy(d, kk, s, l, k, sh);
      ++i;
      ii += l;
      sh += ww;
   }

   // highest coefficient has one more word that could hold the 1 of 2^n
   if (i == mm1 && ii+l+1 <= len) {
      j = bit_rev(i, lg);  // dest index
      s = a + ii;  // source pointer
      d = v + j*kk;  // dest pointer
      caassert(ii+l+1 <= len);  // main case: full input value there
      weigh_copy(d, kk, s, l+1, k, sh);
      ++i;
      sh += ww;
   }
   // copy the one part that may be there only partly
   else if (ii <= len) {  // parts of input value there
      j = bit_rev(i, lg);  // dest index
      s = a + ii;  // source pointer
      d = v + j*kk;  // dest pointer
      caassert(len >= ii);
      const index have = len - ii;
      caassert(have >= 0);
      weigh_copy(d, kk, s, have, k, sh);
      ++i;
      sh += ww;
   }

   // the other inputs are zero
   while (i < mm) {
      j = bit_rev(i, lg);  // dest index
      d = v + j*kk;
      zero(d, kk);  // input value not there altogether
      ++i;
   }
}

#pragma warning(pop)

#ifdef CL_TEST_CODE
static void test_smul_init() {
   {
      uint8 a[] = { 1, 2, 3, 4 };
      uint8 r[4*NBEL(a)];
      smul_init_cyclic(r, a, NBEL(a), 1, 2, 4, 4);
      string s = dump_hex(r, NBEL(r));
      classert(s == "0x4000000020000000300000001");
   }
   {
      uint8 a[] = { 1, 2, 3 };
      uint8 r[16];
      smul_init_cyclic(r, a, NBEL(a), 1, 2, 4, 4);
      string s = dump_hex(r, NBEL(r));
      classert(s == "0x20000000300000001");
   }
   {
      uint8 a[] = { 1, 2, 3, 4 };
      uint8 r[2*NBEL(a)];
      smul_init_cyclic(r, a, NBEL(a), 2, 1, 2, 4);
      string s = dump_hex(r, NBEL(r));
      classert(s == "0x40300000201");
   }
   {
      uint8 a[] = { 1, 2, 3 };
      uint8 r[8];
      smul_init_cyclic(r, a, NBEL(a), 2, 1, 2, 4);
      string s = dump_hex(r, NBEL(r));
      classert(s == "0x300000201");
   }
   {
      // test the 2^n case
      uint8 a[] = { 1, 2, 3, 4, 1 };
      uint8 r[4*(NBEL(a)-1)];
      smul_init_cyclic(r, a, NBEL(a), 1, 2, 4, 4);
      string s = dump_hex(r, NBEL(r));
      classert(s == "0x104000000020000000300000001");
   }
}
#endif


/*template<typename small>
static void smul_shuffle_negacyclic(small* r, const unsigned lg, const index mm, const index kk, const index k) {
   caassert(1ui64 << lg == mm);
   caassert((kk - 1) * bits(small) == k);

   tape_alloc<small> tmp(kk);
   small* t = tmp.p;
   const index ww = 2*k / mm;
   for (index i = 0; i < mm; ++i) {
      const index j = bit_rev(i, lg);
      if (i != j) {
         if (i < j) {
            small* a = r+i*kk;
            small* b = r+j*kk;
            smul_mul_pow2(t, a, kk, k, (2*k-i*ww) % (2*k));
            smul_mul_pow2(a, b, kk, k, (2*k-j*ww) % (2*k));
            copy(b, t, kk);
         }
      }
      else {
         small* a = r+i*kk;
         smul_mul_pow2(t, a, kk, k, (2*k-i*ww) % (2*k));
         copy(a, t, kk);
      }
   }
}*/


// $dissect$ smul-mulpow2.txt
#pragma warning(push)
#pragma warning(disable:4146)
#pragma warning(disable:4293)  // warning C4293: '<<' : shift count negative or too big, undefined behavior


// use v2len?

// ------ helper classes

template<typename small>
struct get_unshifted {
   static small get0(const small* v, const unsigned sh1) {
      caassert(sh1 == 0);
      return v[0];
   }
   static small get(const small* v, const index i, const unsigned sh1, const unsigned sh2) {
      caassert(i > 0);
      caassert(sh1 == 0);
      return v[i];
   }
};

template<typename small, unsigned tsh1>
struct get_bytealign {
   static small get0(const small* v, const unsigned sh1) {
      caassert(sh1 == tsh1);
      return v[0] << tsh1;
   }
   static small get(const small* v, const index i, const unsigned sh1, const unsigned sh2) {
      caassert(i > 0);
      caassert(sh1 == tsh1);
      caassert(sh1 + sh2 == bits(small));
      const small* v2 = (const small*) ((const byte*)v - tsh1/bits(byte));
      return v2[i];
   }
};

template<typename small>
struct get_shifted {
   static small get0(const small* v, const unsigned sh1) {
      caassert(sh1 < bits(small));
      return v[0] << sh1;
   }
   static small get(const small* v, const index i, const unsigned sh1, const unsigned sh2) {
      caassert(i > 0);
      caassert(sh1 + sh2 == bits(small));
      return v[i] << sh1 | v[i-1] >> sh2;
   }
};

#ifdef CL_TEST_CODE
static void test_smul_mul_pow2_getter() {
   {
      uint8 a[] = { 1, 2, 0xff, 4 };
      uint8 v = get_unshifted<uint8>::get0(a, 0);
      classert(v == 1);
      v = get_unshifted<uint8>::get(a, 1, 0, 0);
      classert(v == 2);
      v = get_unshifted<uint8>::get(a, 3, 0, 0);
      classert(v == 4);

      v = get_shifted<uint8>::get0(a, 1);
      classert(v == 2);
      v = get_shifted<uint8>::get(a, 1, 1, 7);
      classert(v == 4);
      v = get_shifted<uint8>::get(a, 3, 1, 7);
      classert(v == 9);
   }
   {
      uint32 a[] = { 1, 2, 0xffffffff, 4};
      uint32 v = get_unshifted<uint32>::get0(a, 0);
      classert(v == 1);

      v = get_shifted<uint32>::get(a, 2, 8, 24);
      classert(v == 0xffffff00);
      v = get_shifted<uint32>::get(a, 3, 8, 24);
      classert(v == 0x4ff);

      v = get_bytealign<uint32, 8>::get(a, 2, 8, 24);
      classert(v == 0xffffff00);
      v = get_bytealign<uint32, 8>::get(a, 3, 8, 24);
      classert(v == 0x4ff);
   }
}
#endif

#define SMUL_UNROLL 1

// ------ the worker loops

template<typename small, typename getter>
static void __forceinline left_positive(small* r, const small* v, index& i, index j, const index klen, const unsigned sh1, const unsigned sh2) {
   const ptrdiff_t d = (v+i) - (r+j);
   small* p = const_cast<small*>(v) - d;
   classert(p+i == r+j);
   const index maxi = i + (klen - j);

   // access v[0]
   caassert(j < klen);
   p[i] = getter::get0(v, sh1);
   ++i;
   // do the rest
#ifndef SMUL_UNROLL
   while (i < maxi) {
      caassert(i < klen);
      p[i] = getter::get(v, i, sh1, sh2);
      ++i;
   }
#else
   caassert(i <= maxi);
   const index u = (maxi - i) % 8;
   i += u;
   switch (u) {
      case 7: goto l7;
      case 6: goto l6;
      case 5: goto l5;
      case 4: goto l4;
      case 3: goto l3;
      case 2: goto l2;
      case 1: goto l1;
      case 0: goto l0;
      default: caassert(0);
   }
#define s(o) p[i+(o)] = getter::get(v, i+(o), sh1, sh2)
l0:while (i < maxi) {
      caassert(i < klen);
      caassert((maxi - i) % 8 == 0);
      i += 8;
      s(-8);
l7:   s(-7);
l6:   s(-6);
l5:   s(-5);
l4:   s(-4);
l3:   s(-3);
l2:   s(-2);
l1:   s(-1);
   }
#undef s
#endif
}

template<typename small, typename getter>
static small __forceinline left_negative(small* r, const small* v, index i, index j, const index klen, const unsigned sh1, const unsigned sh2) {
   const ptrdiff_t d = (v+i) - (r+j);
   small* p = const_cast<small*>(v) - d;
   classert(p+i == r+j);

   small of = 0;
   while (!of && i < klen) {
      caassert(i < klen);
      of = ops<small>::subc(p[i], 0, getter::get(v, i, sh1, sh2));
      ++i;
   }
   // if of is on once, it will stay on, since 0-positive is always < 0
   // furthermore, since -a = ~a + 1, is -a -1 = ~a.
#ifndef SMUL_UNROLL
   while (i < klen) {
      caassert(i < klen);
      p[i] = ~getter::get(v, i, sh1, sh2);
      ++i;
   }
#else
   caassert(i <= klen);
   const index u = (klen - i) % 8;
   i += u;
   switch (u) {
      case 7: goto l7;
      case 6: goto l6;
      case 5: goto l5;
      case 4: goto l4;
      case 3: goto l3;
      case 2: goto l2;
      case 1: goto l1;
      case 0: goto l0;
      default: caassert(0);
   }
#define s(o) p[i+(o)] = ~getter::get(v, i+(o), sh1, sh2)
l0:while (i < klen) {
      caassert(i < klen);
      caassert((klen - i) % 8 == 0);
      i += 8;
      s(-8);
l7:   s(-7);
l6:   s(-6);
l5:   s(-5);
l4:   s(-4);
l3:   s(-3);
l2:   s(-2);
l1:   s(-1);
   }
#undef s
#endif
   caassert(i == klen);
   return of;
}

template<typename small, typename getter>
static small __forceinline right_negative(small* r, const small* v, index& i, index j, const index klen, const unsigned sh1, const unsigned sh2) {
   const ptrdiff_t d = (v+i) - (r+j);
   small* p = const_cast<small*>(v) - d;
   classert(p+i == r+j);
   const index maxi = i + (klen - j);

   small of = 0;
   // access v[0]
   caassert(j < klen);
   of = ops<small>::subc(p[i], 0, getter::get0(v, sh1), of);
   ++i;
   // do the rest
   while (!of && i < maxi) {
      caassert(i < klen);
      of = ops<small>::subc(p[i], 0, getter::get(v, i, sh1, sh2));
      ++i;
   }
#ifndef SMUL_UNROLL
   while (i < maxi) {
      caassert(i < klen);
      p[i] = ~getter::get(v, i, sh1, sh2);
      ++i;
   }
#else
   caassert(i <= maxi);
   const index u = (maxi - i) % 8;
   i += u;
   switch (u) {
      case 7: goto l7;
      case 6: goto l6;
      case 5: goto l5;
      case 4: goto l4;
      case 3: goto l3;
      case 2: goto l2;
      case 1: goto l1;
      case 0: goto l0;
      default: caassert(0);
   }
#define s(o) p[i+(o)] = ~getter::get(v, i+(o), sh1, sh2)
l0:while (i < maxi) {
      caassert(i < klen);
      caassert((maxi - i) % 8 == 0);
      i += 8;
      s(-8);
l7:   s(-7);
l6:   s(-6);
l5:   s(-5);
l4:   s(-4);
l3:   s(-3);
l2:   s(-2);
l1:   s(-1);
   }
#undef s
#endif
   return of;
}

template<typename small, typename getter>
static small __forceinline right_positive(small* r, const small* v, index i, index j, const index klen, const unsigned sh1, const unsigned sh2, small of) {
   const ptrdiff_t d = (v+i) - (r+j);
   small* p = const_cast<small*>(v) - d;
   classert(p+i == r+j);

   while (of && i < klen) {
      caassert(i < klen);
      of = ops<small>::addc1(p[i], getter::get(v, i, sh1, sh2));
      ++i;
   }
#ifndef SMUL_UNROLL
   while (i < klen) {
      caassert(i < klen);
      p[i] = getter::get(v, i, sh1, sh2);
      ++i;
   }
#else
   caassert(i <= klen);
   const index u = (klen - i) % 8;
   i += u;
   switch (u) {
      case 7: goto l7;
      case 6: goto l6;
      case 5: goto l5;
      case 4: goto l4;
      case 3: goto l3;
      case 2: goto l2;
      case 1: goto l1;
      case 0: goto l0;
      default: caassert(0);
   }
#define s(o) p[i+(o)] = getter::get(v, i+(o), sh1, sh2)
l0:while (i < klen) {
      caassert(i < klen);
      caassert((klen - i) % 8 == 0);
      i += 8;
      s(-8);
l7:   s(-7);
l6:   s(-6);
l5:   s(-5);
l4:   s(-4);
l3:   s(-3);
l2:   s(-2);
l1:   s(-1);
   }
#undef s
#endif
   caassert(i == klen);
   return of;
}

// ------ left and right shift worker routines

template<typename small, typename getter>
static void shift_left(small* r, const small* v, const index off, const index klen, const unsigned sh1, const unsigned sh2) {
   caassert(off < klen);

   index i = 0;
   // first round: init the values positive
   left_positive<small, getter>(r, v, i, off, klen, sh1, sh2);
   // second round: init the values negative, propagate carry
   small of = left_negative<small, getter>(r, v, i, 0, klen, sh1, sh2);
   // handle top input word
   // negative case continues, propagate carry
   index j = off;
   caassert(j < klen);
   of = ops<small>::subc(r[j], r[j], getter::get(v, klen, sh1, sh2), of);
   ++j;
   if (j == klen)
      r[klen] = -of;
   else
      r[klen] = -subu_on(r+j, klen-j, &of, 1);  // '-' to save proper -1 (sub returns 1 in case of overflow)
}

template<typename small, typename getter>
static void shift_right(small* r, const small* v, const index off, const index klen, const unsigned sh1, const unsigned sh2) {
   caassert(off >= klen);
   caassert(off < 2*klen);

   index i = 0;
   // first round: init the values negative, propagate carry
   small of = right_negative<small, getter>(r, v, i, off - klen, klen, sh1, sh2);
   // second round: init the values positive, propagate carry
   of = right_positive<small, getter>(r, v, i, 0, klen, sh1, sh2, of);
   // handle top input word
   // positive case continues, propagate carry
   index j = off - klen;
   caassert(j < klen);
   of = ops<small>::addc(r[j], r[j], getter::get(v, klen, sh1, sh2), of);
   ++j;
   if (j == klen)
      r[klen] = of;
   else
      r[klen] = addu_on(r+j, klen-j, &of, 1);
}

// ------ the cyclic shift itself
// r has to have length kk

template<typename small>
void smul_mul_pow2(small* r, const small* v, const index kk, const index k, const index shift_count) {
   caassert(k % bits(small) == 0);
   caassert(k / bits(small) == kk - 1);
   caassert(shift_count < 2*k);
   caassert(cmp_pow2(v, kk, k) <= 0);

   const index sh1 = shift_count % bits(small);
   const index sh2 = bits(small) - sh1;
   const index off = shift_count / bits(small);
   const index klen = kk - 1;

   if (off < klen) {  // init first positive, then negative; shift count < k
      caassert(shift_count < k);
      switch (sh1) {
         case  0: shift_left<small, get_unshifted<small> >(r, v, off, klen, sh1, sh2); break;
         case  8: shift_left<small, get_bytealign<small, 8> >(r, v, off, klen, sh1, sh2); break;
         case 16: shift_left<small, get_bytealign<small,16> >(r, v, off, klen, sh1, sh2); break;
         case 24: shift_left<small, get_bytealign<small,24> >(r, v, off, klen, sh1, sh2); break;
         case 32: shift_left<small, get_bytealign<small,32> >(r, v, off, klen, sh1, sh2); break;
         case 40: shift_left<small, get_bytealign<small,40> >(r, v, off, klen, sh1, sh2); break;
         case 48: shift_left<small, get_bytealign<small,48> >(r, v, off, klen, sh1, sh2); break;
         case 56: shift_left<small, get_bytealign<small,56> >(r, v, off, klen, sh1, sh2); break;
         default: shift_left<small, get_shifted<small> >(r, v, off, klen, sh1, sh2); break;
      }
   }
   else {  // init first negative, then positive; shift count >= k
      caassert(shift_count >= k);
      switch (sh1) {
         case  0: shift_right<small, get_unshifted<small> >(r, v, off, klen, sh1, sh2); break;
         case  8: shift_right<small, get_bytealign<small, 8> >(r, v, off, klen, sh1, sh2); break;
         case 16: shift_right<small, get_bytealign<small,16> >(r, v, off, klen, sh1, sh2); break;
         case 24: shift_right<small, get_bytealign<small,24> >(r, v, off, klen, sh1, sh2); break;
         case 32: shift_right<small, get_bytealign<small,32> >(r, v, off, klen, sh1, sh2); break;
         case 40: shift_right<small, get_bytealign<small,40> >(r, v, off, klen, sh1, sh2); break;
         case 48: shift_right<small, get_bytealign<small,48> >(r, v, off, klen, sh1, sh2); break;
         case 56: shift_right<small, get_bytealign<small,56> >(r, v, off, klen, sh1, sh2); break;
         default: shift_right<small, get_shifted<small> >(r, v, off, klen, sh1, sh2); break;
      }
   }

   const small one = 1;
   switch (r[klen]) {
   case 0:
      break;  // all is fine
   case (small)-1:
      r[klen] = 0;
      addu_on(r, kk, &one, 1);
      break;
   case 1:
      r[klen] = 0;
      subu_on(r, kk, &one, 1);
      if (r[klen] != 0) {  // it was 2^k
         zero(r, klen);
         r[klen] = 1;
      }
      break;
   default:
      classertm(0, "must not happen!");
   }

   classert(cmp_pow2(r, kk, k) <= 0);
}

#pragma warning(pop)

#ifdef CL_TEST_CODE
static void test_smul_mul_pow2() {
   {
      // 1 << 1 (mod 2^8+1) == 2
      uint8 v[] = { 1, 0 };
      uint8 r[NBEL(v)];
      smul_mul_pow2(r, v, NBEL(v), 8*sizeof(v)-8, 1);
      string s = dump_hex(r, NBEL(r));
      classert(s == "2");
   }
   {
      // 1 << 8 (mod 2^16+1) == 256
      uint8 v[] = { 1, 0, 0 };
      uint8 r[NBEL(v)];
      smul_mul_pow2(r, v, NBEL(v), 8*sizeof(v)-8, 8);
      string s = dump_hex(r, NBEL(r));
      classert(s == "0x100");
   }
   {
      // 1 << 15 (mod 2^16+1) == 32768
      uint8 v[] = { 1, 0, 0 };
      uint8 r[NBEL(v)];
      smul_mul_pow2(r, v, NBEL(v), 8*sizeof(v)-8, 15);
      string s = dump_hex(r, NBEL(r));
      classert(s == "0x8000");
   }

   {
      // 1 << 24 (mod 2^16+1) == 65281
      uint8 v[] = { 1, 0, 0 };
      uint8 r[NBEL(v)];
      smul_mul_pow2(r, v, NBEL(v), 8*sizeof(v)-8, 24);
      string s = dump_hex(r, NBEL(r));
      classert(s == "0xff01");
   }
   {
      // 1 << 25 (mod 2^16+1) == 65025
      uint8 v[] = { 1, 0, 0 };
      uint8 r[NBEL(v)];
      smul_mul_pow2(r, v, NBEL(v), 8*sizeof(v)-8, 25);
      string s = dump_hex(r, NBEL(r));
      classert(s == "0xfe01");
   }

   {
      // 1 << 16 (mod 2^16+1) == 65536
      uint8 v[] = { 1, 0, 0 };
      uint8 r[NBEL(v)];
      smul_mul_pow2(r, v, NBEL(v), 8*sizeof(v)-8, 16);
      string s = dump_hex(r, NBEL(r));
      classert(s == "0x10000");
   }
   {
      // 2^16 << 1 (mod 2^16+1) == 65535
      uint8 v[] = { 0, 0, 1 };
      uint8 r[NBEL(v)];
      smul_mul_pow2(r, v, NBEL(v), 8*sizeof(v)-8, 1);
      string s = dump_hex(r, NBEL(r));
      classert(s == "0xffff");
   }
   {
      // 2^16 << 16 (mod 2^16+1) == 1
      uint8 v[] = { 0, 0, 1 };
      uint8 r[NBEL(v)];
      smul_mul_pow2(r, v, NBEL(v), 8*sizeof(v)-8, 16);
      string s = dump_hex(r, NBEL(r));
      classert(s == "1");
   }
   {
      // 2^15 << 1 (mod 2^16+1) == 2^16
      uint8 v[] = { 0, 0x80, 0 };
      uint8 r[NBEL(v)];
      smul_mul_pow2(r, v, NBEL(v), 8*sizeof(v)-8, 1);
      string s = dump_hex(r, NBEL(r));
      classert(s == "0x10000");
   }

   {
      const unsigned p = 16;
      const uint64 m = (1ui64 << p) + 1;
      for (unsigned i = 0; i < 10000; ++i) {
         const unsigned sh = lib_rand.get() % (2*p);
         const uint64 v = lib_rand_big.get() % m;
         const uint64 r1 = (v << sh) % m;
         uint8 v2[3];
         v2[0] = v % 256;
         v2[1] = (v >> 8) % 256;
         v2[2] = v >> 16;
         uint8 r2[NBEL(v2)];
         smul_mul_pow2(r2, v2, NBEL(v2), 8*sizeof(v2)-8, sh);
         uint64 rr2 = r2[0] | r2[1] << 8 | r2[2] << 16;
         classert(rr2 == r1);
      }
   }
}
#endif


// $dissect$ smul-core.txt
template<typename small>
void smul_addsub_mul(small* v1, small* v2, const index k, const index kk, const index shift_count) {
   tape_alloc<small> tmp(kk);
   small* t = tmp.p;
   const small one = 1;  // improve: make global

   // calculate t = w*v2
   smul_mul_pow2(t, v2, kk, k, shift_count);
   //DTOUT_PUTS(format("v1=%? v2=%? ") << dump_hex(v1, kk) << dump_hex(v2, kk));
   //DTOUT_PUTS(format("sh=%?: t=%?, ") << shift_count << dump_hex(t, kk));
#if 0
   const index kkl = normu(t, kk);
   if (subu(v2, kk, v1, kk, t, kkl) != 0) {  // v2 = v1-t
      // add 2^k+1
      addu_on(v2, kk, &one, 1);
      addu_on(v2+kk-1, 1, &one, 1);
   }
   addu_on(v1, kk, t, kkl);  // v1 = v1+t
#else
   if (addsubu(v1, v2, v1, t, kk) & 2) {
      // sub has returned a carry: add 2^k+1
      addu_on(v2, kk, &one, 1);
      addu_on(v2+kk-1, 1, &one, 1);
   }
#endif
   mod_pow2p1_aligned_on(v1, kk, k);
   //DTOUT_PUTS(format("v1=v1+t=%? v2=v1-t=%?\n") << dump_hex(v1, kk) << dump_hex(v2, kk));
}

template<typename small>
void smul_eval(small* v, const unsigned lg, const index mm, const index k, const index kk, const bool eval) {
   caassert(mm > 1);
   caassert(1ui64 << lg == mm);
   classert(is_pow2(mm));
   classert(k / bits(small) + 1 == kk);
   caassert(mm % 2 == 0);

   const index mhi = mm/2*kk;
   if (mm > 2) {
      smul_eval<small>(v, lg-1, mm/2, k, kk, eval);
      smul_eval<small>(v+mhi, lg-1, mm/2, k, kk, eval);
   }
   // set w to primitive mm'th root of unity in 2^k+1
   // i.e. w^mm == 1, w^(mm/2) == -1, w^(mm/2) == 2^k
   // assume w == 2^ww, then (2^ww)^(mm/2) == 2^k, 2^(ww*mm/2) == 2^k
   // ergo ww*mm/2 == k, ww == k/mm*2, w = 2^(k/mm*2)
   const index ww = (2*k) >> lg;  // shift count
   caassert(ww * mm == 2*k);  // (2^ww)^mm == 1
   const index ww2 = eval ? ww : 0-ww;

   // cycle through all coefficients
   sindex sh = 0;
   index ii = 0;
   for (index i=0; i<mm/2; ++i, ii+=kk) {
      small* v1 = v+ii;  // v[i]
      small* v2 = v+ii+mhi;  // v[i+mm/2]
      smul_addsub_mul(v1, v2, k, kk, sh);
      // next mm'th root when evaluating, previous when interpolating
      sh += ww2;
      if (sh < 0) sh += 2*k;  // make sure it's always non-negative
   }
}

#ifdef CL_TEST_CODE
static void test_smul_eval() {
   {
      uint8 v[] = { 2, 0, 1, 0 };
      smul_eval<uint8>(v, 1, 2, 8, 2, true);
      string s = dump_hex(v, NBEL(v));
      classert(s == "0x10003");

      smul_eval<uint8>(v, 1, 2, 8, 2, false);
      s = dump_hex(v, NBEL(v));
      classert(s == "0x20004");
   }
   {
      uint8 v[] = { 4, 0, 2, 0, 3, 0, 1, 0 };
      smul_eval<uint8>(v, 2, 4, 8, 2, true);
      string s = dump_hex(v, NBEL(v));
      classert(s == "0xe300020022000a");

      fft_shuffle(v, 2, 2);
      s = dump_hex(v, NBEL(v));
      classert(s == "0xe300220002000a");

      smul_eval<uint8>(v, 2, 4, 8, 2, false);
      s = dump_hex(v, NBEL(v));
      classert(s == "0x40008000c0010");
   }
   {
      uint8 v[] = { 4, 0, 0, 2, 0, 0, 3, 0, 0, 1, 0, 0 };
      smul_eval<uint8>(v, 2, 4, 16, 3, true);
      string s = dump_hex(v, NBEL(v));
      classert(s == "0xfe0300000200020200000a");

      fft_shuffle(v, 2, 3);
      s = dump_hex(v, NBEL(v));
      classert(s == "0xfe0300020200000200000a");

      smul_eval<uint8>(v, 2, 4, 16, 3, false);
      s = dump_hex(v, NBEL(v));
      classert(s == "0x400000800000c000010");
   }
   {
      uint8 v[] = { 8, 0, 4, 0, 6, 0, 2, 0, 7, 0, 3, 0, 5, 0, 1, 0 };
      smul_eval<uint8>(v, 3, 8, 8, 2, true);
      string s = dump_hex(v, NBEL(v));
      classert(s == "0xb600c50035000400d4004400530024");

      fft_shuffle(v, 3, 2);
      s = dump_hex(v, NBEL(v));
      classert(s == "0xb600d40035005300c5004400040024");

      smul_eval<uint8>(v, 3, 8, 8, 2, false);
      s = dump_hex(v, NBEL(v));
      classert(s == "0x80010001800200028003000380040");
   }
}
#endif

template<typename small>
static void correct_negative(small* t, const index kk, const index l, const index i1) {
   caassert(kk > 1);
   if (compu_nn(t+2*l, kk-2*l, (const small*)&i1, sizeof(index)/sizeof(small)) > 0) {  // byteorder
      const small one = 1;
      subu_on(t, kk, &one, 1);
      --t[kk-1];
   }
}

template<typename small>
static void smul_sumup(small* r, const index rlen, small* rr, const index l, const unsigned lg, const index mm, const index k, const index kk, const bool negacyclic) {
   classert(mm > 0);
   classert(1ui64 << lg == mm);

   tape_alloc<small> tmp(kk);
   small* t = tmp.p;
   sindex sh = 2*k - lg;
   const sindex ww = negacyclic ? k / mm : 0;  // not 2*k!

   index i1 = 1;
   index ii = 0;
   index jj = 0;
   index cnt = 0;  // smalls already written into r

   const index rm = rlen;
   if (rm >= kk) {
      // handle first element
      smul_mul_pow2(r, rr, kk, k, sh);
      if (negacyclic) correct_negative(r, kk, l, i1);
      ++i1;
      ii += l;
      jj += kk;
      cnt += kk;
      sh -= ww;
      if (sh < 0) sh += 2*k;

      // handle all elements that are addable in full
      const index imax = rm - kk;
      while (ii <= imax) {
         smul_mul_pow2(t, rr+jj, kk, k, sh);
         if (negacyclic) correct_negative(t, kk, l, i1);

         // we already have 'cnt' smalls in output buffer and need to add up smalls at offset 'ii'
         caassert(cnt > ii);
         const index over = cnt - ii;
         caassert(over <= kk);
         addi(r + ii, kk, t, kk, r + ii, over);
         cnt = ii + kk;

         ++i1;
         ii += l;
         jj += kk;
         sh -= ww;
         if (sh < 0) sh += 2*k;
      }
   }

   // handle what is left
   zero(r, rlen, cnt);
   const index maxjj = mm * kk;
   while (jj < maxjj && ii < rlen) {
      smul_mul_pow2(t, rr+jj, kk, k, sh);
      if (negacyclic) correct_negative(t, kk, l, i1);

      // at least part of it is available
      const index todo = tmin(rlen - ii, kk);
      addi_on(r + ii, todo, t, todo);

      ++i1;
      ii += l;
      jj += kk;
      sh -= ww;
      if (sh < 0) sh += 2*k;
   }
}


// external wrapper
template<typename small, bool square>
void smul_ls(small* r, const small* a, const index alen, const small* b, const index blen) {
   caassert(!square || a == b);
   caassert(!square || alen == blen);

   if (!use_qsmul<square>(alen, blen)) {
      qt3mul_ls<small, square>(r, a, alen, b, blen);
      return;
   }

   // compute fft parameters:
   unsigned lg;  // fft depth
   index mm; // mm == 2^lg, number of pieces of each number and number of lower elements
   index l;  // smalls per piece
   index k;  // exponent of modulo ring 2^k+1
   smul_set_params<small>(square, alen, blen, lg, mm, l, k);

   // call the main routine
   qsmul2<small, square>(r, alen+blen, a, alen, b, blen, lg, mm, l, k, false);
}

#ifdef CL_TEST_CODE
static void test_smul() {
   const index old = switch_smul(always_on);
   {
      uint8 a[] = { 1, 2, 3, 4 };
      uint8 b[] = { 5, 6, 7, 8 };

      uint8 r[NBEL(a)+NBEL(b)];
      smul(r, a, NBEL(a), b, NBEL(b));
      string s = dump_hex(r, NBEL(r));
      classert(s == "0x20343d3c221005");
   }
#ifdef _M_X64
   {
      uint64 a[] = { 1, 2, 3, 4 };
      uint64 b[] = { 5, 6, 7, 8 };

      uint64 r[NBEL(a)+NBEL(b)];
      smul(r, a, NBEL(a), b, NBEL(b));
      string s = dump_hex(r, NBEL(r));
      classert(s == "0x200000000000000034000000000000003d000000000000003c000000000000002200000000000000100000000000000005");
   }
#endif
   {
      uint8 a[] = { 0xff, 0xfe, 0xfd, 0xfc };
      uint8 b[] = { 0xfb, 0xfa, 0xf9, 0xf8 };

      uint8 r[NBEL(a)+NBEL(b)];
      smul(r, a, NBEL(a), b, NBEL(b));
      string s = dump_hex(r, NBEL(r));
      classert(s == "0xf60d1a1c26150a05");
   }
   {
      uint16 a[] = { 0xffff, 0xfffe, 0xfffd };
      uint16 b[] = { 0xfffb, 0xfffa, 0xfff9 };

      /*uint16 r2[NBEL(a)+NBEL(b)];
      omul(r2, a, NBEL(a), b, NBEL(b));
      string s2 = dump_hex(r2, NBEL(r2));
      classert(s2 == "0xfff80006000a0015000a0005");*/

      uint16 r[NBEL(a)+NBEL(b)];
      smul(r, a, NBEL(a), b, NBEL(b));
      string s = dump_hex(r, NBEL(r));
      classert(s == "0xfff80006000a0015000a0005");
   }
   switch_smul(old);
}
#endif


// main worker routine
template<typename small, bool square>
void qsmul2(small* r, const index rlen, const small* a, const index alen, const small* b, const index blen, const unsigned lg, const index mm, const index l, const index k, const bool negacyclic) {
   classert(1ui64 << lg == mm);
   classert(is_pow2(mm));  // must be power of 2 for the fft evaluation divide and conquer
   caassert(l*bits(small) < k);  // part length must be smaller than inner ring size
   caassert(2*l*bits(small) + lg <= k);  // there must be no overflow when parts are added up in convolution
   caassert(2*k >= mm);  // we need mm roots of unity and 2^k+1 gives us 2*k roots
   caassert(2*k/mm*mm == 2*k);  // #parts divides k, otherwise roots don't properly square up for next recursion
   caassert(mm*l >= rlen-1);  // part length must be big enough so all of the output is covered

   caassert(!square || a == b);
   caassert(!square || alen == blen);

   const index kk = k / bits(small) + 1;  // smalls per inner element

   // alloc buffer space
   tape_alloc<small> tmp(kk + mm*kk + (!square || negacyclic ? mm*kk : 0));
   small* rr = tmp.p;
   small* va = tmp.p + kk;
   small* vb = !square || negacyclic ? tmp.p + kk + mm*kk : 0;

   PROF(uint64 start = 0);
   PROF(uint64 diff = 0);
   PROF(uint64 nsec = 0);
   PROF(const bool doprof = do_profiling && (l << lg) > 672);

   // init inner coefficients
   PROF(if (doprof) { start = hrtime.get(); } );
   if (negacyclic) {
      smul_init_negacyclic(va, a, alen, l, lg, mm, kk, k);
      if (!square) smul_init_negacyclic(vb, b, blen, l, lg, mm, kk, k);
   }
   else {
      smul_init_cyclic(va, a, alen, l, lg, mm, kk);
      if (!square) smul_init_cyclic(vb, b, blen, l, lg, mm, kk);
   }
   PROF(if (doprof) { diff = hrtime.get() - start; uint64 nsec = !doprof ? 0 : hrtime_to_nsec64(diff); if (doprof) tout_puts(format("smul%? %? init time: %'? nsec\n") << (negacyclic ? "_mod" : "") << (l << lg) << nsec); } );

   // ffts to evaluate
   PROF(if (doprof) { start = hrtime.get(); } );
   smul_eval<small>(va, lg, mm, k, kk, true);
   if (!square) smul_eval<small>(vb, lg, mm, k, kk, true);
   PROF(if (doprof) { diff = hrtime.get() - start; nsec = hrtime_to_nsec64(diff); tout_puts(format("smul%? %? eval time: %'? nsec\n") << (negacyclic ? "_mod" : "") << (l << lg) << nsec); } );

   // pointwise multiplications
   PROF(if (doprof) { start = hrtime.get(); } );
   index ii = 0;
   for (index i = 0; i < mm; ++i) {
      // rr overwrites va array
      smul_modmul<small, square>(rr+ii, va+ii, square ? va+ii : vb+ii, kk, k, blen);  // mul is done mod 2^k+1
      ii += kk;
   }
   PROF(if (doprof) { diff = hrtime.get() - start; nsec = hrtime_to_nsec64(diff); tout_puts(format("smul%? %? mul time: %'? nsec\n") << (negacyclic ? "_mod" : "") << (l << lg) << nsec); } );

   // shuffle results
   PROF(if (doprof) { start = hrtime.get(); } );
   fft_shuffle(rr, lg, kk);
   PROF(if (doprof) { diff = hrtime.get() - start; nsec = hrtime_to_nsec64(diff); tout_puts(format("smul%? %? shuffle time: %'? nsec\n") << (negacyclic ? "_mod" : "") << (l << lg) << nsec); } );

   // fft to interpolate
   PROF(if (doprof) { start = hrtime.get(); } );
   smul_eval<small>(rr, lg, mm, k, kk, false);
   PROF(if (doprof) { diff = hrtime.get() - start; nsec = hrtime_to_nsec64(diff); tout_puts(format("smul%? %? r-eval time: %'? nsec\n") << (negacyclic ? "_mod" : "") << (l << lg) << nsec); } );

   // normalise & sum up result
   PROF(if (doprof) { start = hrtime.get(); } );
   if (negacyclic) {
      // use vb as temp storage
      smul_sumup(vb, mm*l-l+kk, rr, l, lg, mm, k, kk, true);
      mod_pow2p1_aligned_on(vb, mm*l-l+kk, mm*l*bits(small));
      copy(r, vb, rlen);
   }
   else
      smul_sumup(r, rlen, rr, l, lg, mm, k, kk, false);
   PROF(if (doprof) { diff = hrtime.get() - start; nsec = hrtime_to_nsec64(diff); tout_puts(format("smul%? %? sumup time: %'? nsec\n") << (negacyclic ? "_mod" : "") << (l << lg) << nsec); } );
}

#ifdef CL_TEST_CODE
static void test_smul2() {
   {
      uint8 a[] = { 1, 2, 3, 4 };
      uint8 b[] = { 5, 6, 7, 8 };

      /*uint8 r2[NBEL(a)+NBEL(b)];
      omul(r2, a, NBEL(a), b, NBEL(b));
      string s2 = dump_hex(r2, NBEL(r2));
      classert(s2 == "0x20343d3c221005");*/

      uint8 r[NBEL(a)+NBEL(b)];
      smul2(r, NBEL(r), a, NBEL(a), b, NBEL(b), 3, 8, 1, 24, false);
      string s = dump_hex(r, NBEL(r));
      classert(s == "0x20343d3c221005");
   }
#ifdef _M_X64
   {
      uint64 a[] = { 1, 2, 3, 4 };
      uint64 b[] = { 5, 6, 7, 8 };

      /*uint64 r2[NBEL(a)+NBEL(b)];
      omul(r2, a, NBEL(a), b, NBEL(b));
      string s2 = dump_hex(r2, NBEL(r2));
      classert(s2 == "0x200000000000000034000000000000003d000000000000003c000000000000002200000000000000100000000000000005");*/

      uint64 r[NBEL(a)+NBEL(b)];
      smul2(r, NBEL(r), a, NBEL(a), b, NBEL(b), 3, 8, 1, 192, false);
      string s = dump_hex(r, NBEL(r));
      classert(s == "0x200000000000000034000000000000003d000000000000003c000000000000002200000000000000100000000000000005");
   }
#endif
   {
      uint8 a[] = { 0xff, 0xfe, 0xfd, 0xfc };
      uint8 b[] = { 0xfb, 0xfa, 0xf9, 0xf8 };

      /*uint8 r2[NBEL(a)+NBEL(b)];
      omul(r2, a, NBEL(a), b, NBEL(b));
      string s2 = dump_hex(r2, NBEL(r2));
      classert(s2 == "0xf60d1a1c26150a05");*/

      uint8 r[NBEL(a)+NBEL(b)];
      smul2(r, NBEL(r), a, NBEL(a), b, NBEL(b), 3, 8, 1, 192, false);
      string s = dump_hex(r, NBEL(r));
      classert(s == "0xf60d1a1c26150a05");
   }
}
#endif


// $dissect$ smul-support.txt
// ------ estimate smul() time ------------------------------------------------

// I used these timings:

// I:\lltest\lltest# x64\Release\lltest64.exe -BBLL:smul(
// This is lltest v6.0 (x64) (r22489M, 2013-01-11 10:36:26) by chris@cfos.de
//
// worker "0", directory "I:\lltest\lltest\x64\Release\"
// it is now 2013-01-11 11:54:03.338
// CPU: Intel(R) Core(TM) i7-3770 CPU @ 3.40GHz, ID: 06_3ah, 3.410 GHz
// omul_ptr = omul4_asmx64;             osqu_ptr = osqu4_asmx64
// kmul_thresh = 30, 1920 bits;         ksqu_thresh = 50, 3200 bits
// t3mul_thresh = 160, 10240 bits;      t3squ_thresh = 160, 10240 bits
// smul_thresh = 2721, 174144 bits;     ssqu_thresh = 2705, 173120 bits
// ssqu: [2..] = 144, 296, 624, 1184, 2112, 4736, 14080, 32256, 97280, 194560
// ssqu_mod: [4..] = 384, 416, 640, 1024, 1536, 2560, 5120, 10240
// time source: thread cycles
//
// null1_ticks = 290
// null2_ticks = 5.25
//
// benchmarking smul(n,n):
//  size/w  mem/b rounds    total      ns/w      cyc/w   cyc/O
//       1     32  35.8M   0.4977     13.89      47.36   47.36
//       2     64    28M   0.4975      8.87      30.24   30.24
//       4    128  19.7M   0.4976      6.28      21.43   10.71
//       8    256  7.48M   0.4939      8.25      28.12    5.76
//      16    512  2.31M   0.4962     13.42      45.78    5.72
//      32     1K   665K   0.4977     23.38      79.73    6.85
//      64     2K   199K   0.4904     38.48     131.13    8.45
//     128     4K  63.7K   0.4873     59.71     203.58   10.35
//     256     8K    20K   0.4615     90.14     307.32   12.80
//     512    16K  7.21K   0.4958    134.30     457.89   16.04
//      1K    32K  2.53K   0.4971    191.82     654.01   19.68
//      2K    64K    936   0.4959    258.72     881.37   23.16
//      4K   128K    398   0.4968    304.74    1038.92   24.14
//      8K   256K    181   0.4956    334.27    1139.61   23.68
//     16K   512K     80   0.4925    375.78    1281.06   24.03
//     32K     1M     36   0.4953    419.89    1431.53   24.42
//     64K     2M     16   0.4926    469.83    1601.75   25.02
//    128K     4M      6   0.4515    574.13    1957.17   28.16
//    256K     8M      3   0.4942    628.49    2142.63   28.54

static double smul_cyc_per_u64[] = {
     47.36,   //    1 = 2^0
     30.24,   //    2 = 2^1
     21.43,   //    4 = 2^2
     28.12,   //    8 = 2^3
     45.78,   //   16 = 2^4
     79.73,   //   32 = 2^5
    131.13,   //   64 = 2^6
    203.58,   //  128 = 2^7
    307.32,   //  256 = 2^8
    457.89,   //  512 = 2^9
    654.01,   //   1K = 2^10
    881.37,   //   2K = 2^11
   1038.92,   //   4K = 2^12
   1139.61,   //   8K = 2^13
   1281.06,   //  16K = 2^14
   1431.53,   //  32K = 2^15
   1601.75,   //  64K = 2^16
   1957.17,   // 128K = 2^17
   2142.63,   // 256K = 2^18
};

uint64 _CL_CALL estimate_smul_time(index len) {
   return estimate_time(len, smul_cyc_per_u64, NBEL(smul_cyc_per_u64), 1);
}


#ifdef CL_TEST_CODE
void _CL_CALL test_bignum_smul() {
   test_bit_rev();
   test_smul_modmul();
   test_smul_choose_lg();
   test_smul_mul_pow2_getter();
   test_smul_mul_pow2();
   test_smul_init();
   test_smul_eval();
   test_smul2();
   test_smul_mod();
   test_smul();

   /*uint64 a[3000];
   uint64 b[3000];
   uint64 r[6000];
   smul(r, a, NBEL(a), b, NBEL(b));*/
}
#endif


// ------ generate template functions -----------------------------------------

//#if _MSC_VER < 1300
#pragma inline_depth(0)

template<typename small>
void generate_bignum_smuls() {
   smul_print_params<small>(0, 0, false, false);
   index i;
   unsigned u;
   smul_set_params<small>(false, 0, 0, u, i, i, i);
   smul_init_cyclic<small>(0, 0, 0, 0, 0, 0, 0);
   smul_mul_pow2<small>(0, 0, 0, 0, 0);
   smul_addsub_mul<small>(0, 0, 0, 0, 0);
   smul_modmul<small, false>(0, 0, 0, 0, 0, 0);
   smul_modmul<small, true>(0, 0, 0, 0, 0, 0);
   smul_ls<small, false>(0, 0, 0, 0, 0);
   smul_ls<small, true>(0, 0, 0, 0, 0);
   smul<small>(0, 0, 0, 0, 0);
   smul_mem<small, false>(0, 0);
   smul_mem<small, true>(0, 0);
   smul_mod_params<small>(0, false, u, i, i, i);
   smul_mod_print_params2<small>(0, false, false);
}

void _generate_bignum_smuls() {
   classertm(0, "must not be called");
   generate_bignum_smuls<uint8>();
   generate_bignum_smuls<uint16>();
   generate_bignum_smuls<uint32>();
#ifdef _M_X64
   generate_bignum_smuls<uint64>();
#endif
}

#pragma inline_depth()
//#endif

}  // namespace bignum
