; $+HEADER$
; 
; Copyright 2015 Christoph Lueders
; 
; This file is part of the BIGNUM Library.
; 
; The BIGNUM Library is free software: you can redistribute it and/or modify
; it under the terms of the GNU Lesser General Public License as published by
; the Free Software Foundation, either version 3 of the License, or (at your 
; option) any later version.
; 
; The BIGNUM Library is distributed in the hope that it will be useful, but 
; WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
; or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public 
; License for more details.
; 
; You should have received a copy of the GNU Lesser General Public License
; along with the BIGNUM Library.  If not, see <http://www.gnu.org/licenses/>.
; 
; $-HEADER$

; $Date: 2015-04-24 11:28:16 +0200 (Fr, 24 Apr 2015) $
; $Rev: 26590 $


_TEXT SEGMENT

;r$ = 16
;a$ = 24
;alen$ = 32

?div_by3_asmx64@bignum@@YAXPEA_KPEB_K_K@Z PROC      ; void __cdecl cl::div_by3_asmx64(unsigned __int64 *,unsigned __int64 const *,unsigned __int64)

; 75   : void div_by3_2(small* r, const small* a, index alen) {

   sub   rsp, 8

; 79   :    small rm = 0;

   xor   r10d, r10d
   mov   r9, rdx

; 80   :    index i = alen;
; 81   :    static const small hi_const[] = { 0, (small)-1/3, (small)-1/3*2 };
; 82   :    while (i > 0) {

   test  r8, r8
   jz    SHORT exit

   mov   QWORD PTR [rsp+16], rbx
   mov   QWORD PTR [rsp], rdi
   lea   r11, QWORD PTR [rcx+r8*8]

; 80   :    index i = alen;
; 81   :    static const small hi_const[] = { 0, (small)-1/3, (small)-1/3*2 };
; 82   :    while (i > 0) {

   lea   rdi, OFFSET hi_const_table
   sub   r9, rcx
   mov   rbx, 0aaaaaaaaaaaaaaabH

   align 16
div_loop:

; 83   :       --i;
; 84   :       small q = a[i] / 3;

   mov   rcx, QWORD PTR [r9+r11-8]
   mov   rax, rbx
   mul   rcx

   ; do some unrelated things here
   sub   r11, 8

   shr   rdx, 1

; 85   :       const small newr = a[i] - q*3;

   lea   rax, QWORD PTR [rdx+rdx*2]

; 86   :       classert(rm < 3);
; 87   :       q += hi_const[rm];

   add   rdx, QWORD PTR [rdi+r10*8]
   sub   rcx, rax

; 88   :       rm += newr;

   add   r10, rcx

; 89   :       const small is = rm >= 3;

   xor   ecx, ecx
   cmp   r10, 3
   setae cl

; 90   :       rm -= 3 * is;

   lea   rax, QWORD PTR [rcx+rcx*2]
   sub   r10, rax
   dec   r8

; 91   :       q += is;

   lea   rax, QWORD PTR [rcx+rdx]

; 92   :       r[i] = q;

   mov   QWORD PTR [r11], rax
   jne   SHORT div_loop
   mov   rdi, QWORD PTR [rsp]
   mov   rbx, QWORD PTR [rsp+16]
exit:

; 93   :    }
; 94   : }

   add   rsp, 8
   ret   0
?div_by3_asmx64@bignum@@YAXPEA_KPEB_K_K@Z ENDP      ; void __cdecl cl::div_by3_asmx64(unsigned __int64 *,unsigned __int64 const *,unsigned __int64)


   align 16
hi_const_table:
   DQ 05555555555555555H
   DQ 0aaaaaaaaaaaaaaaaH


_TEXT ENDS

END
