// $+HEADER$
// 
// Copyright 2015 Christoph Lueders
// 
// This file is part of the BIGNUM Library.
// 
// The BIGNUM Library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
// 
// The BIGNUM Library is distributed in the hope that it will be useful, but 
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public 
// License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with the BIGNUM Library.  If not, see <http://www.gnu.org/licenses/>.
// 
// $-HEADER$

// $Date: 2015-04-25 13:01:03 +0200 (Sa, 25 Apr 2015) $
// $Rev: 26600 $

/*
 *  prime testing and factoring
 */

#include "precomp.h"
#include "impl.h"
#include "prime.h"
#include "bignum_t.h"
#include "cl/int_math.h"
#include "cl/memzero.h"
#include "cl/cpu.h"
#include "cl/format.h"
//#include "cl/hrtime.h"
#include "cl/str_conv.h"

namespace bignum {

   // http://stackoverflow.com/questions/4643647/fast-prime-factorization-module

   // from WP: http://en.wikipedia.org/wiki/Miller%E2%80%93Rabin_primality_test#Deterministic_variants_of_the_test

   /*
   For example, Pomerance, Selfridge and Wagstaff[8] and Jaeschke[9] have verified that

   if n < 1,373,653, it is enough to test a = 2 and 3;
   if n < 9,080,191, it is enough to test a = 31 and 73;
   if n < 4,759,123,141, it is enough to test a = 2, 7, and 61;
   if n < 1,122,004,669,633, it is enough to test a = 2, 13, 23, and 1662803;
   if n < 2,152,302,898,747, it is enough to test a = 2, 3, 5, 7, and 11;
   if n < 3,474,749,660,383, it is enough to test a = 2, 3, 5, 7, 11, and 13;
   if n < 341,550,071,728,321, it is enough to test a = 2, 3, 5, 7, 11, 13, and 17.
   */


   // http://stackoverflow.com/questions/3918968/brute-force-single-threaded-prime-factorization

   /*
   If we want to encode all the primes in order, we can just encode their differences.
   Again, the expected difference is 1/ln(N). Also, they're all even, which saves an extra bit.
   And they are never zero, which makes extension to a multibyte encoding free. So using one
   byte per prime, differences up to 512 can be stored in one byte, which gets us up to
   303,371,455,241 (> 2^38) according to that Wikipedia article.

   it's 203 MB
   */


   // ------ primality test through trial factoring by 2, 3, 5, 7 and then a 210-cycle

   template<class C>
   bool _CL_CALL trial_div1_t(const C n, C& factor) {
      classert(n > 2);
      classert(n % 2 != 0);
      if (n == 3 || n == 5 || n == 7) return true;
      if (n % 3 == 0) {
         factor = 3;
         return false;
      }
      if (n % 5 == 0) {
         factor = 5;
         return false;
      }
      if (n % 7 == 0) {
         factor = 7;
         return false;
      }
      if (n < 11*11) return true;
      return trial_div_range<C, def_mod<C>>(n, 11, int_sqrt(n), factor);
   }

   template<class C, bool (*mod)(const C&, const C&)>
   bool _CL_CALL trial_div_range(const C n, const C start, const C end, C& factor) {
      caassert(n % 2 != 0);
      caassert(n % 3 != 0);
      caassert(n % 5 != 0);
      caassert(n % 7 != 0);
      caassert(n > 7);
      caassert(start >= 11);
      caassert(start % 2 == 1);
      caassert(start <= end);
      caassert(start % 210 == 11);

      // a total of 48 tests are done for a 210-cycle
      // in 32 bit mode, each trial-div needs 15 bytes code, 6 opcodes
      // in 64 bit mode, each trial-div needs 17 bytes code, 6 opcodes
      C f = start;
      C end2 = cl_limits<C>::has_limits ? tmin<C>(end, numeric_limits<C>::max() - 210 + 1) : end;  // avoid overflow of 'f'
      bignum_lal_clear();
      while (f <= end2) {
         if (!mod(n, f)) {    // 0
has_factor:
            if (n == f) return true;
            factor = f;
            return false;
         }

         f += 2; if (!mod(n, f)) goto has_factor;  // 2
         f += 4; if (!mod(n, f)) goto has_factor;  // 6
         f += 2; if (!mod(n, f)) goto has_factor;  // 8
         f += 4; if (!mod(n, f)) goto has_factor;  // 12
         f += 6; if (!mod(n, f)) goto has_factor;  // 18
         f += 2; if (!mod(n, f)) goto has_factor;  // 20
         f += 6; if (!mod(n, f)) goto has_factor;  // 26
         f += 4; if (!mod(n, f)) goto has_factor;  // 30
         f += 2; if (!mod(n, f)) goto has_factor;  // 32
         f += 4; if (!mod(n, f)) goto has_factor;  // 36
         f += 6; if (!mod(n, f)) goto has_factor;  // 42
         f += 6; if (!mod(n, f)) goto has_factor;  // 48
         f += 2; if (!mod(n, f)) goto has_factor;  // 50
         f += 6; if (!mod(n, f)) goto has_factor;  // 56
         f += 4; if (!mod(n, f)) goto has_factor;  // 60
         f += 2; if (!mod(n, f)) goto has_factor;  // 62
         f += 6; if (!mod(n, f)) goto has_factor;  // 68
         f += 4; if (!mod(n, f)) goto has_factor;  // 72
         f += 6; if (!mod(n, f)) goto has_factor;  // 78
         f += 8; if (!mod(n, f)) goto has_factor;  // 86
         f += 4; if (!mod(n, f)) goto has_factor;  // 90
         f += 2; if (!mod(n, f)) goto has_factor;  // 92
         f += 4; if (!mod(n, f)) goto has_factor;  // 96
         f += 2; if (!mod(n, f)) goto has_factor;  // 98
         f += 4; if (!mod(n, f)) goto has_factor;  // 102
         f += 8; if (!mod(n, f)) goto has_factor;  // 110
         f += 6; if (!mod(n, f)) goto has_factor;  // 116
         f += 4; if (!mod(n, f)) goto has_factor;  // 120
         f += 6; if (!mod(n, f)) goto has_factor;  // 126
         f += 2; if (!mod(n, f)) goto has_factor;  // 128
         f += 4; if (!mod(n, f)) goto has_factor;  // 132
         f += 6; if (!mod(n, f)) goto has_factor;  // 138
         f += 2; if (!mod(n, f)) goto has_factor;  // 140
         f += 6; if (!mod(n, f)) goto has_factor;  // 146
         f += 6; if (!mod(n, f)) goto has_factor;  // 152
         f += 4; if (!mod(n, f)) goto has_factor;  // 156
         f += 2; if (!mod(n, f)) goto has_factor;  // 158
         f += 4; if (!mod(n, f)) goto has_factor;  // 162
         f += 6; if (!mod(n, f)) goto has_factor;  // 168
         f += 2; if (!mod(n, f)) goto has_factor;  // 170
         f += 6; if (!mod(n, f)) goto has_factor;  // 176
         f += 4; if (!mod(n, f)) goto has_factor;  // 180
         f += 2; if (!mod(n, f)) goto has_factor;  // 182
         f += 4; if (!mod(n, f)) goto has_factor;  // 186
         f += 2; if (!mod(n, f)) goto has_factor;  // 188
         f += 10;if (!mod(n, f)) goto has_factor;  // 198
         f += 2; if (!mod(n, f)) goto has_factor;  // 200
         f += 10;
      }

      if (cl_limits<C>::has_limits) {
         // simple loop to handle wrap-around
         caassert(f > 1);
         while (f <= end) {
            if (n % f == 0) goto has_factor;
            f += 2;  // after wrap-around, f could be 0 or 1
            if (f < 2) break;
         }
      }
      return true;
   }

#ifdef CL_TEST_CODE
   template<class C>
   static void _CL_CALL _test_trial_div1() {
      C f;
      bool b = trial_div1_t<C>(3, f);
      classert(b);
      b = trial_div1_t<C>(5, f);
      classert(b);
      b = trial_div1_t<C>(7, f);
      classert(b);
      b = trial_div1_t<C>(9, f);
      classert(!b);
      classert(f == 3);
      b = trial_div1_t<C>(53, f);
      classert(b);
      b = trial_div1_t<C>(1001, f);
      classert(!b);
      classert(f == 7);
      b = trial_div1_t<C>(65521, f);
      classert(b);
      b = trial_div1_t<C>(251*257, f);
      classert(!b);
      classert(f == 251);
   }

#pragma warning(push)
#pragma warning(disable:4307)  // integral constant overflow
   template<class C>
   static void _CL_CALL _test_trial_div_t2(bool(_CL_CALL *prime_func)(const C, C&)) {
      // make test values larger than 128^2
      C f;
      bool b = prime_func(19997, f);  // larger than (64*2)^2
      classert(b);

      b = prime_func(19993*19997, f);
      classert(!b);
      classert(f == 19993);

      if (sizeof(C) >= sizeof(uint64)) {
         b = prime_func((C)130087*(C)130099, f);  // outside of 8k prime sieve
         classert(!b);
         classert(f == 130087);

         b = prime_func((C)2097223*(C)2097229, f);  // outside of 128k prime sieve
         classert(!b);
         classert(f == 2097223);
      }
   }
#pragma warning(pop)

   template<class C>
   static void _CL_CALL _test_trial_div1_t() {
      _test_trial_div1<C>();
      _test_trial_div_t2<C>(trial_div1_t<C>);
   }
   static void _CL_CALL test_trial_div1() {
      _test_trial_div1_t<uint32>();
      _test_trial_div1_t<uint64>();
   }
#endif  //def CL_TEST_CODE



   prime_sieve<limb> *small_siv = nullptr;

   void _CL_CALL build_small_siv(size_t max) {
      if (small_siv && small_siv->max > max) return;
      delete small_siv;
      small_siv = new prime_sieve<limb>(max);
   }


   cycle_sieve<limb> *cycle_siv = nullptr;

   void _CL_CALL build_cycle_siv() {
      delete cycle_siv;
      cycle_siv = new cycle_sieve<limb>();
      cycle_siv->init_cycle(4, *small_siv);
   }


   template<typename T>
   static bool def_mod(const T& a, const T& b) {
      return a % b != 0;
   }

#ifdef _M_X64
   static bool mod_64by32(const uint64& a, const uint64& b) {
      caassert(b < 0x100000000);
      uint64 r = divr_asmx64_64by32_rev(b, a);
      caassert(r < 0x100000000);
      return r != 0;
   }

   static bool mod_128by64(const ubignum_t& a, const ubignum_t& b) {
      caassert(a.len() == 2);
      caassert(b.len() == 1);
      caassert(b <= 0xffffffffffffffff);
      uint64 r = divr_asmx64(a.ptr[0], a.ptr[1], b.ptr[0]);
      caassert(r <= 0xffffffffffffffff);
      return r != 0;
   }
#endif

   template<class C, bool (*mod)(const C&, const C&)>
   bool _CL_CALL trial_div2_t(const C n, C& factor, const C start, const C end) {
      classert(n > 2);
      classert(n % 2 != 0);
      classert(small_siv != nullptr);
      classert(start >= 3);

      C f = start;
      if (f < small_siv->max) {
         f = f / small_siv->div * small_siv->div + 1;  // even numbers are not in sieve
         classert(f % 2 == 1);

         // test for small numbers.  make sure we don't trial-div by more than n
         // (upcoming loop only checks for <= end once every 2*bits(small_siv->buf) numbers
         if (end < 2*bits(*small_siv->buf)) {
            while (f <= end) {
               if (small_siv->_is_prime(f) && !mod(n, f)) {
                  factor = f;
                  return false;
               }
               f += 2;
            }
            return true;
         }

         // test low factors from mask words of sieve
         auto end_buf = small_siv->buf + (size_t)t2min(small_siv->max, end + 1 + small_siv->div - 1) / small_siv->div;  // end pointer
         auto p = small_siv->buf + (size_t)t2min(small_siv->max, f) / small_siv->div;  // run pointer
         classert(f >= (p - small_siv->buf) * small_siv->div + 1);
         bignum_lal_clear();
         while (p < end_buf) {
            auto mask = ~*p++;
            C next_f = f + small_siv->div;
            unsigned idx;
            while (asm_bsf(mask, idx)) {
               f += 2*idx;
               if (!mod(n, f)) {
                  factor = f;
                  return false;
               }
               mask >>= idx;  // avoid idx + 1 == sizeof(mask)*8, in which case nothing happens
               mask >>= 1;
               f += 2;
            }
            f = next_f;
         }
         if (f > end) return true;
      }

      // continue regular cycle trial division
      classert(f >= 11);
      f = (f - 11) / 210 * 210 + 11;
      return trial_div_range<C, mod>(n, f, end, factor);
   }

#ifdef CL_TEST_CODE
   template<class C>
   static bool _CL_CALL trial_div2_t_adaptor(const C n, C& factor) {
      return trial_div2_t<C, def_mod<C>>(n, factor, (C)3, int_sqrt(n));
   }
   template<class C>
   static void _CL_CALL _test_trial_div2_t() {
      _test_trial_div_t2<C>(trial_div2_t_adaptor<C>);
   }
   static void _CL_CALL test_trial_div2() {
      _test_trial_div2_t<uint32>();
      _test_trial_div2_t<uint64>();
   }
#endif  //def CL_TEST_CODE


#pragma warning(push)
#pragma warning(disable:4293)  // warning C4293: '>>' : shift count negative or too big, undefined behavior

   template<class C>
   bool _CL_CALL is_prime(const C n, C& factor, bool force_factor, C start) {
      if (n < 2) {  // junk input
         factor = 1;
         return false;
      }
      if (n == 2) return true;
      if ((n & 1) == 0) {
         factor = 2;
         return false;
      }
      // now we know n > 2 and n is odd
      // build and use sieve
      if (small_siv == nullptr) build_small_siv();
      if (n < small_siv->max) {
         if (small_siv->_is_prime(n)) return true;
         if (!force_factor) {
            factor = 0;
            return false;  // composite, but we don't have a factor
         }
      }

      if (start < 3) start = 3;
      if (n <= numeric_limits<uint32>::max()) {
         uint32 f;
         uint32 end = int_sqrt((uint32)n);
         bool b = trial_div2_t<uint32, def_mod<uint32>>(n, f, start, end);
         factor = (C)f;
         return b;
      }
      if (n <= numeric_limits<uint64>::max()) {
         uint64 f;
         uint64 end = int_sqrt((uint64)n);
#ifdef _M_X64
         // if n / f < 2^32, we can still use small 64/32=32 DIV
         // make sure we're behind any rounding of sieve or 210-cyle range
         uint64 end1 = tmin(((uint64)n >> 32) + 1 + fmax(210, small_siv->div), end);
         bool b = trial_div2_t<uint64, def_mod<uint64>>(n, f, start, end1);
         if (!b) {
            factor = (C)f;
            return false;
         }
         if (end1 < end) {
            classert(end1 > 1);
            classert(n / end1 <= numeric_limits<uint32>::max());
            b = trial_div2_t<uint64, mod_64by32>(n, f, end1+1, end);
            factor = (C)f;
         }
#else
         bool b = trial_div2_t<uint64, def_mod<uint64>>(n, f, start, end);
         factor = (C)f;
#endif
         return b;
      }
      if (!cl_limits<C>::has_limits) {
         ubignum_t f;
         ubignum_t end = int_sqrt(n);
#ifdef _M_X64
         if (end < numeric_limits<uint64>::max()) {
            // if n / f < 2^64, we can still use small 128/64=64 DIV
            // make sure we're behind any rounding of sieve or 210-cyle range
            ubignum_t end1 = (n >> 64) + 1 + fmax(210, small_siv->div);
            if (end < end1) end1 = end;
            bool b = trial_div2_t<ubignum_t, def_mod<ubignum_t>>(n, f, start, end1);
            if (!b) {
               factor = (C)f;
               return false;
            }
            if (end1 < end) {
               classert(end1 > 1);
               classert(n / end1 <= numeric_limits<uint64>::max());
               b = trial_div2_t<ubignum_t, mod_128by64>(n, f, end1+1, end);
               factor = (C)f;
            }
            return b;
         }
         else
#endif
         {
            bool b = trial_div2_t<ubignum_t, def_mod<ubignum_t>>(n, f, start, end);
            factor = (C)f;
            return b;
         }
      }
      return false;
   }
#pragma warning(pop)


#if 0
   // ------ new style --------------------------------------------------------

   struct CC1 {
      limb* ptr;
      uint64 low_val() const { return ptr[0]; }
      bool greater(limb b, index len) const { return compu_nn(ptr, len, &b, 1) > 0; }
      bool greater(const CC1& b, index len) const { return compu_nn(ptr, len, b.ptr, len) > 0; }
      bool less(limb b, index len) const { return compu_nn(ptr, len, &b, 1) < 0; }
      bool less(const CC1& b, index len) const { return compu_nn(ptr, len, b.ptr, len) < 0; }
      bool has_factor(limb b, index len, CC1& tmp) const {
         tdiv<limb, false>(nullptr, 0, tmp.ptr, len, ptr, len, &b, 1);
         return normu(tmp.ptr, len) == 0;
      }
      bool has_factor(const CC1& b, index len, CC1& tmp) const {
         tdiv<limb, false>(nullptr, 0, tmp.ptr, len, ptr, len, b.ptr, len);
         return normu(tmp.ptr, len) == 0;
      }
      void copy(const CC1& b, index len) { ::copy(ptr, b.ptr, len); }
      void and_low(limb b) { ptr[0] &= b; }
      void add_low(limb b) { ptr[0] += b; }
   };

   struct CC {
      uint64 a;
      uint64 low_val() const { return a; }
      bool greater(uint64 b, index) const { return a > b; }
      bool greater(const CC& b, index) const { return a > b.a; }
      bool less(uint64 b, index) const { return a < b; }
      bool less(const CC& b, index) const { return a < b.a; }
      bool has_factor(uint64 b, index, CC&) const { return a % b == 0; }
      bool has_factor(const CC& b, index, CC&) const { return a % b.a == 0; }
      void copy(const CC& b, index) { a = b.a; }
      void and_low(limb b) { a &= b; }
      void add_low(limb b) { a += b; }
   };

   bool _CL_CALL trial_div3_t(const index len, const CC n, CC& factor, const CC start, const CC end, CC f, CC tmp) {
      classert(n.greater(2, len));
      classert(!n.has_factor(2, len, tmp));
      classert(small_siv != nullptr);
      classert(start.greater(2, len));

      f.copy(start, len);
      if (f.less(small_siv->max, len)) {
         classert(small_siv->max < numeric_limits<decltype(f.low_val())>::max());
         // align f to the sieve
         f.and_low(~(limb)small_siv->div);
         f.add_low(1);  // even numbers are not in sieve
         classert(!f.has_factor(2, len, tmp));

         // test for small numbers.  make sure we don't trial-div by more than n
         // (upcoming loop only checks for <= r once every 2*bits(small_siv->buf) numbers
         if (end.less(2*bits(*small_siv->buf), len)) {
            while (!f.greater(end, len)) {
               if (small_siv->_is_prime(f.low_val()) && n.has_factor(f, len, tmp)) {
                  factor = f;
                  return false;
               }
               f.add_low(2);
            }
            return true;
         }

         // test low factors from mask words of sieve
         auto end_buf = small_siv->buf + (size_t)t2min(small_siv->max, r + 1 + small_siv->div - 1) / small_siv->div;  // end pointer
         auto p = small_siv->buf + (size_t)t2min(small_siv->max, f) / small_siv->div;  // run pointer
         classert(f >= (p - small_siv->buf) * small_siv->div + 1);
         bignum_lal_clear();
         while (p < end_buf) {
            auto mask = ~*p++;
            C next_f = f + small_siv->div;
            unsigned idx;
            while (asm_bsf(mask, idx)) {
               f += 2*idx;
               if (!(n % f)) {
                  factor = f;
                  return false;
               }
               mask >>= idx;  // avoid idx + 1 == sizeof(mask)*8, in which case nothing happens
               mask >>= 1;
               f += 2;
            }
            f = next_f;
         }
         if (f > r) return true;
      }

      // continue regular cycle trial division
      classert(f >= 11);
      f = (f - 11) / 210 * 210 + 11;
      return trial_div_range(n, f, r, factor);
   }
#endif


   // ------ time estimate ----------------------------------------------------

   static double _is_prime_eta_cnt(double n, uint64 siv_max) {
      double sqr = sqrt(n);
      if (n < siv_max) return 1;
      double start = t2min(siv_max, sqr);
      double tests = start / log(start);
      if (sqr > start) tests += (sqr - start) * 48 / 210;
      return tests;
   }

   uint64 _CL_CALL _is_prime_eta(const bignum_t& _n) {  // in msec
      if (_n < 9 || (_n & 1) == 0) return 1;
      double n = _n.to_dbl();
      double x2 = log(n), y2;
      // timings done with -S32
      if (_n <= numeric_limits<uint64>::max()) {
         static double x0 = log(75161927681.0);
         static double y0 = log(0.000198);
         static double x1 = log(15564440312192434177.0);
         static double y1 = log(2.338227);
         static double a = (y1 - y0) / (x1 - x0);  // steigung
         y2 = a * (x2 - x0) + y0;
      }
      else {
         static double x0 = log(35740566642812256257.0);
         static double y0 = log(22.926336);
         static double x1 = log(46043073207979040833537.0);
         static double y1 = log(1583.741944);
         static double a = (y1 - y0) / (x1 - x0);  // steigung
         y2 = a * (x2 - x0) + y0;
      }
      double r = exp(y2) * 3400000000.0 / clock_rate;
      double sf = _is_prime_eta_cnt(n, small_siv ? small_siv->max : 0) / _is_prime_eta_cnt(n, 0x100000000);
      return r * 1000 * sf;
   }


   // ------ sieving, odd bit-sieve -------------------------------------------

   template<typename T>
   prime_sieve<T>::prime_sieve(size_t to) {
      buf = nullptr;
      max = 0;
      if (to > 2) sieve(to);
   }

   template<typename T>
   void prime_sieve<T>::dealloc() {
      free(buf);
      buf = nullptr;
      max = 0;
   }

   template<typename T>
   bool prime_sieve<T>::alloc(size_t m) {
      dealloc();
      bytes = m / div * sizeof(T);  // make sure right amount is allocated, even if max is (unsigned)-1
      if (bytes / sizeof(T) * div < m) bytes += sizeof(T);
      max = m;
      buf = (T*)malloc(bytes);
      return buf != nullptr;
   }

   template<typename T>
   bool prime_sieve<T>::is_set(size_t idx) const {
      classert(idx % 2 != 0);
      classert(idx <= max);
      size_t i = idx / div;
      size_t b = (idx % div) / 2;
      return _is_bit_set(buf[i], b);
      //return asm_bt((const limb*)buf, idx/2);  // too slow on IBR :( !!
   }

   template<typename T>
   void prime_sieve<T>::set(size_t idx) {
      classert(idx % 2 != 0);
      classert(idx <= max);
      size_t i = idx / div;
      size_t b = (idx % div) / 2;
      _set_bit(buf[i], b);
   }

   template<typename T>
   bool prime_sieve<T>::_is_prime(size_t idx) const {
      return !is_set(idx);
   }

   template<typename T>
   bool prime_sieve<T>::is_prime(size_t idx) const {
      if (idx == 2) return true;
      if (idx % 2 == 0) return false;
      return _is_prime(idx);
   }

   template<typename T>
   void prime_sieve<T>::sieve(size_t to) {
      classert(to > 2);
      alloc(to);
      memzero(buf, bytes);
      set(1);  // not a prime
      size_t sqr = int_sqrt(to) + 1;
      size_t f = 3;
      // start with conventional loop
      for (; f <= tmin(sqr, div); f += 2) {
         if (!is_set(f)) {
            for (size_t j=f*f; j<to; j+=2*f)
               set(j);
         }
      }
      if (f > sqr) return;
      caassert(f == div + 1);
      // continue with faster loop that reads mask words of sieve
      T *end_buf = buf + tmin<size_t>(max, sqr + 1 + div - 1) / div;  // end pointer
      T *p = buf + f/div;  // run pointer
      f -= 2;  // try to save one opcode
      while (p < end_buf) {
         auto mask = ~*p++;
         size_t next_f = f + div;
         unsigned idx;
         while (asm_bsf(mask, idx)) {
            f += 2*idx + 2;
            for (size_t j=f*f; j<to; j+=2*f)
               set(j);
            mask >>= idx + 1;
         }
         f = next_f;
      }
   }

   template<typename T>
   size_t prime_sieve<T>::count_primes() const {
      size_t cnt = 0;
      for (size_t i=1; i<max; i+=2) {
         if (_is_prime(i)) ++cnt;
      }
      return cnt;
   }

#ifdef CL_TEST_CODE
   template<class C>
   static void _CL_CALL _test_sieve() {
      prime_sieve<C> s;
      s.alloc(200);
      memzero(s.buf, s.bytes);
      for (size_t i=1; i<s.max; i+=2)
         classert(!s.is_set(i));

      s.set(63);
      classert(s.is_set(63));
      classert(!s.is_set(61));
      classert(!s.is_set(65));

      s.set(129);
      classert(s.is_set(129));
      classert(!s.is_set(127));
      classert(!s.is_set(131));

      s.sieve(1000);
      size_t cnt = s.count_primes() + 1;  // +1 for 2
      classert(cnt == 168);
      classert(!s.is_set(967));
      classert(!s.is_set(971));
      classert(!s.is_set(977));
      classert(!s.is_set(983));
      classert(!s.is_set(991));
      classert(!s.is_set(997));
      classert(s.is_set(969));
      classert(s.is_set(973));
      classert(s.is_set(979));
      classert(s.is_set(985));
      classert(s.is_set(993));
      classert(s.is_set(999));

      s.sieve(1024*1024*2);
      classert(!s.is_set(2097083));
      classert(!s.is_set(2097091));
      classert(!s.is_set(2097097));
      classert(!s.is_set(2097131));
      classert(!s.is_set(2097133));
      classert(!s.is_set(2097143));
      classert(s.is_set(2097081));
      classert(s.is_set(2097089));
      classert(s.is_set(2097095));
      classert(s.is_set(2097129));
      classert(s.is_set(2097135));
      classert(s.is_set(2097141));
      cnt = s.count_primes() + 1;  // +1 for 2
      classert(cnt == 155611);
   }
   static void _CL_CALL test_sieve() {
      _test_sieve<uint32>();
#ifdef _M_X64
      _test_sieve<uint64>();
#endif
   }
#endif  //def CL_TEST_CODE


   // ------ cycle_sieve ------------------------------------------------------

   template<typename T>
   template<class C>
   void cycle_sieve<T>::sieve_part(const prime_sieve<C>& siv, size_t from, size_t to, size_t highp) {
      classert(from > 2);
      classert(to > from);
      classert(&siv != nullptr);
      classert(from % 2 == 0);
      alloc(to-from);
      memzero(buf, bytes);
      for (size_t f = 3; f <= highp; f += 2) {
         if (siv.is_prime(f)) {
            size_t j = f >= from ? f - from : f - from + (from - f + 2*f - 1) / (2*f) * 2*f;
            for (; j<to-from; j+=2*f) {
               //CLDEBUG(size_t n = j + from);
               //DTOUT_PUTS(format("%? ") << n);
               set(j);
            }
         }
      }
   }

   // prime products:
   // 2: 2*3 = 6, 1 b, 2/6=33%
   // 3: 2*3*5 = 30, 4 b, 8/30=27%
   // 4: 2*3*5*7 = 210, 27 b, 48/210=22%
   // 5: 2*3*5*7*11 = 2310, 288 b, 480/2310=20%
   // 6: 2*3*5*7*11*13 = 30030, 3753 b, 5760/30030=19%
   // 7: 2*3*5*7*11*13*17 = 510510, 62.3 kb, 92160/510510=18%
   // 8: 2*3*5*7*11*13*17*19 = 9699690, 1.16 mb, 1658880/9699690=17%
   // 9: 2*3*5*7*11*13*17*19*23 = 223092870, 13.9 mb, 36495360/223092870=16%
   // 10: 2*3*5*7*11*13*17*19*23*27 = 6023507493, 376 mb

   template<typename T>
   template<class C>
   void cycle_sieve<T>::init_cycle(unsigned prime_cnt, const prime_sieve<C>& siv) {
      classert(prime_cnt > 1);  // must include 2 and one more
      if (sizeof(size_t) == sizeof(uint32)) classert(prime_cnt <= 9);
      --prime_cnt;
      prod = 2;
      highp = 0;
      size_t i = 3;
      for (; prime_cnt > 0 && i < siv.max; i += 2) {
         if (siv._is_prime(i)) {
            classert(prod <= numeric_limits<size_t>::max() / i);
            prod *= i;
            --prime_cnt;
            highp = i;
         }
      }
      sieve_part(siv, highp+1, prod+highp+1, highp);
      start = highp+1;
      //DTOUT_PUTS(format("%?/%?=%?%%\n") << count_primes() << prod << 100*count_primes()/prod);
   }

#ifdef CL_TEST_CODE
   template<class C>
   static void _CL_CALL _test_cycle_sieve() {
      cycle_sieve<C> s;
      s.sieve_part(*small_siv, 4, 4+6, 3);
      classert(s.is_prime(5-4));
      classert(s.is_prime(7-4));
      classert(!s.is_prime(9-4));
      size_t cnt = s.count_primes();
      classert(cnt == 2);

      s.sieve_part(*small_siv, 6, 6+30, 5);
      classert(s.is_prime(7-6));
      classert(!s.is_prime(9-6));
      classert(s.is_prime(11-6));
      classert(s.is_prime(13-6));
      classert(!s.is_prime(15-6));
      classert(s.is_prime(17-6));
      classert(s.is_prime(19-6));
      classert(!s.is_prime(21-6));
      classert(s.is_prime(23-6));
      classert(!s.is_prime(25-6));
      classert(!s.is_prime(27-6));
      classert(s.is_prime(29-6));
      classert(s.is_prime(31-6));
      classert(!s.is_prime(33-6));
      classert(!s.is_prime(35-6));
      cnt = s.count_primes();
      classert(cnt == 8);
   }
   static void _CL_CALL test_cycle_sieve() {
      _test_cycle_sieve<uint32>();
#ifdef _M_X64
      _test_cycle_sieve<uint64>();
#endif
   }
#endif  //def CL_TEST_CODE



   // ------ factorize --------------------------------------------------------

   template<typename N, typename P>
   bool factorize(N n, vector<pair<N, P>>& factors) {
      factors.clear();
      if (n < 1) return false;

      auto inc_pow = [&factors](N& p) {
         if (factors.empty()) factors.push_back(make_pair(p, (index)1));
         else {
            auto& last = factors.back();
            if (last.first == p) ++last.second;
            else factors.push_back(make_pair(p, (index)1));
         }
      };
      N f = 2;
      while (true) {
         if (is_prime(n, f, true, f)) {
            inc_pow(n);
            break;
         }
         inc_pow(f);
         n /= f;
      }
      return true;
   }

#ifdef CL_TEST_CODE
   template<typename T>
   static void _test_bignum_factorize() {
      T t;
      vector<pair<T, index>> fs;
      bool b;

      t = 13;
      b = factorize(t, fs);
      classert(b);
      classert(fs.size() == 1);
      classert(fs[0] == make_pair((T)13, (index)1));

      t = 121;
      b = factorize(t, fs);
      classert(b);
      classert(fs.size() == 1);
      classert(fs[0] == make_pair((T)11, (index)2));

      t = 12;
      b = factorize(t, fs);
      classert(b);
      classert(fs.size() == 2);
      classert(fs[0] == make_pair((T)2, (index)2));
      classert(fs[1] == make_pair((T)3, (index)1));

      t = 1001;
      b = factorize(t, fs);
      classert(b);
      classert(fs.size() == 3);
      classert(fs[0] == make_pair((T)7, (index)1));
      classert(fs[1] == make_pair((T)11, (index)1));
      classert(fs[2] == make_pair((T)13, (index)1));

      t = 64969u * 64997u;
      b = factorize(t, fs);
      classert(b);
      classert(fs.size() == 2);
      classert(fs[0] == make_pair((T)64969, (index)1));
      classert(fs[1] == make_pair((T)64997, (index)1));

      t = 64951ui64 * 64969u * 64997u;
      b = factorize(t, fs);
      classert(b);
      classert(fs.size() == 3);
      classert(fs[0] == make_pair((T)64951, (index)1));
      classert(fs[1] == make_pair((T)64969, (index)1));
      classert(fs[2] == make_pair((T)64997, (index)1));
   }

   static void test_bignum_factorize() {
      _test_bignum_factorize<uint64>();
      _test_bignum_factorize<ubignum_t>();
   }
#endif


   // ------ lucas primality test ------------------------------------------------

   // also n-1 test
   // see crandall/pomerance, ch. 4.1, p. 173
   // see http://en.wikipedia.org/wiki/Lucas_primality_test
   lucas_ret _CL_CALL lucas_prime_test(const ubignum_t& n) {
      if (n < 2) return lucas_error;  // junk input
      if (n == 2) return lucas_is_prime;  // is prime

      ubignum_t nm1 = n - 1;
      vector<pair<ubignum_t, index>> fs;
      factorize(nm1, fs);

      ubignum_t a = 2;
      // limit due to http://en.wikipedia.org/wiki/Eric_Bach
      ubignum_t lim = 2 * n.bit_length() * n.bit_length();
      while (a <= lim) {
         if (a.mod_pow(nm1, n) != 1) return lucas_is_composite;  // is composite
         size_t i = 0;
         for (; i<fs.size(); ++i) {
            auto& par = fs[i];
            auto& q = par.first;
            classert(nm1 % q == 0);
            if (a.mod_pow(nm1 / q, n) == 1) break;  // not prime or bad a
         }
         if (i == fs.size()) return lucas_is_prime;  // is prime
         ++a;
      }
      return lucas_unknown;  // unknown status
   }

#ifdef CL_TEST_CODE
   static void test_lucas_prime_test() {
      ubignum_t n = s_to<ubignum_t>("13576803638250229989377");  // prime
      lucas_ret r = lucas_prime_test(n);
      classert(r == lucas_is_prime);

      n = s_to<ubignum_t>("12986507827891524337665");  // composite
      r = lucas_prime_test(n);
      classert(r == lucas_is_composite);
   }
#endif  //def CL_TEST_CODE


   // ------ end stuff -----------------------------------------------------------

#ifdef CL_TEST_CODE
   void _CL_CALL test_bignum_prime() {
      test_trial_div1();
      test_sieve();
      build_small_siv();  // is needed
      test_cycle_sieve();
      test_trial_div2();
      test_bignum_factorize();
      test_lucas_prime_test();
   }
#endif  //def CL_TEST_CODE


#pragma inline_depth(0)

   template<typename small>
   static void generate_bignum_prime_test() {
      small f;
      is_prime<small>(0, f);
      vector<pair<small, index>> fs;
      factorize<small>(0, fs);
      is_prime_eta(f);
   }

   template<typename small>
   static void generate_bignum_prime_sieve() {
      prime_sieve<small> s;
      s.sieve(0);
      s.is_prime(0);
      s.count_primes();
   }

   void _generate_bignum_primes() {
      classertm(0, "must not be called");
      generate_bignum_prime_test<uint32>();
      generate_bignum_prime_test<uint64>();
      generate_bignum_prime_test<ubignum_t>();

      lucas_prime_test(0);

      generate_bignum_prime_sieve<uint32>();
#ifdef _M_X64
      generate_bignum_prime_sieve<uint64>();
#endif
   }

#pragma inline_depth()

}  // namespace bignum
