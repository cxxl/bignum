// $+HEADER$
// 
// Copyright 2015 Christoph Lueders
// 
// This file is part of the BIGNUM Library.
// 
// The BIGNUM Library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
// 
// The BIGNUM Library is distributed in the hope that it will be useful, but 
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public 
// License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with the BIGNUM Library.  If not, see <http://www.gnu.org/licenses/>.
// 
// $-HEADER$

// $Date: 2015-04-25 13:01:03 +0200 (Sa, 25 Apr 2015) $
// $Rev: 26600 $

/*
 *  arbitrary length number arithmetic - benchmarking
 */

#include "precomp.h"
#include "impl.h"
#include "gmp_wrap.h"
#include "smul.h"
#include "dkss.h"
#include "cl/format.h"
#include "cl/hrtime.h"
#include "cl/memzero.h"
#include "cl/match.h"
#include "cl/cpu.h"
#include "cl/stat_avgdev.h"
#include "cl/tracer.h"
//#include "cl/uaw.h"
#include "cl/widechar.h"
#include "cl/int_math.h"
#include "cl/proc_mgmt.h"
#include <map>


// we must switch off "unreachable code" warning, since bool template parameters generate code paths that are never used
#pragma warning (disable:4702)

volatile bool break_signalled = false;

namespace bignum {

using namespace std;

thread_cycles cyc;


// ------ PROFILING -----------------------------------------------------------

#ifdef PROFILE
map<string, unsigned> profile_count;

void _CL_CALL profile_params2(const char* name, index alen, index blen) {
   const string s = format("%?(%?, %?)") << name << alen << blen;
   ++profile_count[s];
}

void _CL_CALL profile_clear() {
   profile_count.clear();
}

void _CL_CALL print_profiling() {
   for (map<string, unsigned>::const_iterator i = profile_count.begin(); i != profile_count.end(); ++i) {
      const auto& name = i->first;
      const auto count = i->second;
      tout_puts(format("%8? %?\n") << count << name);
   }
}
#endif


// ------ BENCHMARK -----------------------------------------------------------

class bn_benchmark {
public:
   bn_benchmark() { }
   virtual ~bn_benchmark() { }
   virtual void setup(index len) = 0;
   virtual void destroy() = 0;
   virtual void benchmark() = 0;
   virtual uint64 estimate(uint64 len) = 0;
   virtual uint64 big_o(uint64 len) = 0;
   virtual uint64 mem_usage() = 0;
   virtual string name() = 0;
   virtual bool do_size(index sz, uint64 free) = 0;
};

class null_benchmark : public bn_benchmark {
   bool done;
public:
   null_benchmark() {
      done = false;
   }
   virtual void setup(index) { }
   virtual void destroy() { }
   virtual void benchmark() { }
   virtual uint64 estimate(uint64) { return 10; }
   virtual uint64 big_o(uint64 len) { return 1; }
   virtual uint64 mem_usage() { return 0; }
   virtual bool do_size(index sz, uint64 free) {
      const bool d = done;
      done = true;
      return !d;
   }
};

class null1_benchmark : public null_benchmark {
   bool done;
public:
   virtual string name() {
      return "null1";
   }
};

class null2_benchmark : public null_benchmark {
   bool done;
public:
   virtual string name() {
      return "null2";
   }
};

template<typename small>
class unary_benchmark : public bn_benchmark {
protected:
   small* a;
   small* r;
   index alen;
   index rlen;
public:
   unary_benchmark() {
      a = 0;
      r = 0;
   }
   virtual void destroy() {
      bignum_free(a); a = 0;
      bignum_free(r); r = 0;
   }
   virtual uint64 mem_usage() {
      return (rlen + alen) * sizeof(small);
   }
};

template<typename small>
class binary_benchmark : public bn_benchmark {
protected:
   small* a;
   small* b;
   small* r;
   index alen;
   index blen;
   index rlen;
public:
   binary_benchmark() {
      a = nullptr;
      b = nullptr;
      r = nullptr;
   }
   virtual void destroy() {
      bignum_free(a); a = nullptr;
      bignum_free(b); b = nullptr;
      bignum_free(r); r = nullptr;
   }
   virtual uint64 mem_usage() {
      return (rlen + alen + blen) * sizeof(small);
   }
   virtual bool do_size(index sz, uint64 free) {
      return 3 * sz * sizeof(small) < free;
   }
};

template<typename small>
class trinary_benchmark : public bn_benchmark {
protected:
   small* a;
   small* b;
   small* r1;
   small* r2;
   index alen;
   index blen;
   index r1len;
   index r2len;
public:
   trinary_benchmark() {
      a = nullptr;
      b = nullptr;
      r1 = nullptr;
      r2 = nullptr;
   }
   virtual void destroy() {
      bignum_free(a); a = nullptr;
      bignum_free(b); b = nullptr;
      bignum_free(r1); r1 = nullptr;
      bignum_free(r2); r2 = nullptr;
   }
   virtual uint64 mem_usage() {
      return (r1len + r2len + alen + blen) * sizeof(small);
   }
   virtual bool do_size(index sz, uint64 free) {
      return 4 * sz * sizeof(small) < free;
   }
};


// ------ regular benchmarks start here ---------------------------------------

template<typename small>
class memcpy_benchmark : public unary_benchmark<small> {
public:
   virtual void setup(index len) {
      alen = len;
      rlen = len;
      destroy();
      a = bignum_alloc<small>(alen);
      r = bignum_alloc<small>(rlen);
   }
   virtual uint64 estimate(uint64 len) {
      return len;
   }
   virtual uint64 big_o(uint64 len) {
      return len;
   }
   virtual void benchmark() {
      memcpy(r, a, alen * sizeof(small));
   }
   virtual string name() {
      return "memcpy(n)";
   }
   virtual bool do_size(index sz, uint64 free) {
      return 2 * sz * sizeof(small) < free;
   }
};

template<typename small>
class memcpy_unaligned_benchmark : public unary_benchmark<small> {
public:
   virtual void setup(index len) {
      alen = len;
      rlen = len;
      destroy();
      a = bignum_alloc<small>(alen);
      r = bignum_alloc<small>(rlen+1);
   }
   virtual uint64 estimate(uint64 len) {
      return len;
   }
   virtual uint64 big_o(uint64 len) {
      return len;
   }
   virtual void benchmark() {
      memcpy(r+1, a, alen * sizeof(small));
   }
   virtual string name() {
      return "memcpy_unaligned(n)";
   }
   virtual bool do_size(index sz, uint64 free) {
      return 2 * sz * sizeof(small) < free;
   }
};

template<typename small>
class copy_in_benchmark : public unary_benchmark<small> {
public:
   virtual void setup(index len) {
      alen = len;
      rlen = len;
      destroy();
      a = bignum_alloc<small>(alen);
      r = bignum_alloc<small>(rlen);
   }
   virtual uint64 estimate(uint64 len) {
      return len;
   }
   virtual uint64 big_o(uint64 len) {
      return len;
   }
   virtual void benchmark() {
      copy_in(r, a, alen);
   }
   virtual string name() {
      return "copy_in(n)";
   }
   virtual bool do_size(index sz, uint64 free) {
      return 2 * sz * sizeof(small) < free;
   }
};


template<typename small>
class transpose_odd_benchmark : public unary_benchmark<small> {
public:
   index m;
   index n;
   virtual void setup(index len) {
      alen = len;
      index x = len/3;
      n = 32;
      m = x / n;
      classert(m*n*3 <= len);
      rlen = 0;
      destroy();
      a = bignum_alloc<small>(alen);
      r = nullptr;
   }
   virtual uint64 estimate(uint64 len) {
      return len;
   }
   virtual uint64 big_o(uint64 len) {
      return len;
   }
   virtual void benchmark() {
      transpose_in_place_odd(a, m, 32, 3);
   }
   virtual string name() {
      return "transpose_odd(n,32,3)";
   }
   virtual bool do_size(index sz, uint64 free) {
      return sz >= 3*32 && sz * sizeof(small) < free;
   }
};

template<typename small>
class transpose_pow2_benchmark : public transpose_odd_benchmark<small> {
public:
   virtual void benchmark() {
      transpose_in_place_pow2(a, m, 32, 3);
   }
   virtual string name() {
      return "transpose_pow2(n,32,3)";
   }
};


// ------ div by small

template<typename small>
class div_small_benchmark : public unary_benchmark<small> {
public:
   virtual void setup(index len) {
      destroy();
      alen = len;
      rlen = len;
      a = rand_alloc<small>(alen);
      r = bignum_alloc<small>(rlen);
   }
   virtual uint64 estimate(uint64 len) {
      return len;
   }
   virtual uint64 big_o(uint64 len) {
      return len;
   }
   virtual bool do_size(index sz, uint64 free) {
      return 2 * sz * sizeof(small) < free;
   }
};

template<typename small>
class divby3_benchmark : public div_small_benchmark<small> {
public:
   virtual void benchmark() {
      div_by3(r, a, alen);
   }
   virtual string name() {
      return "div_by3(n)";
   }
};

template<typename small>
class divby3_2_benchmark : public div_small_benchmark<small> {
public:
   virtual void benchmark() {
      div_by3_2(r, a, alen);
   }
   virtual string name() {
      return "div_by3_2(n)";
   }
};

template<typename small>
class divby3_3_benchmark : public div_small_benchmark<small> {
public:
   virtual void benchmark() {
      div_by3_3(r, a, alen);
   }
   virtual string name() {
      return "div_by3_3(n)";
   }
};

template<typename small>
class divby3_fast_benchmark : public div_small_benchmark<small> {
public:
   virtual void benchmark() {
      div_by3_fast(r, a, alen);
   }
   virtual string name() {
      return "div_by3_fast(n)";
   }
};

template<typename small>
class div1_benchmark : public div_small_benchmark<small> {
public:
   virtual void benchmark() {
      const small v = (small)0xA23F75038CFBDD97ui64;  // some number.  high bits in all uint sizes are set
      div1(r, rlen, a, alen, v);
   }
   virtual string name() {
      return "div1(n)";
   }
};


// ------ longer div

template<typename small>
class div_benchmark : public trinary_benchmark<small> {
public:
   virtual void setup(index len) {
      destroy();
      alen = len;
      blen = 2;
      r1len = alen - blen + 1;
      r2len = blen;
      a = rand_alloc<small>(alen);
      a[blen-1] |= (small)3 << (bits(small) - 2);  // set both top bits
      b = rand_alloc<small>(blen);
      b[blen-1] |= (small)1 << (bits(small) - 1);  // set top bit
      b[blen-1] &= ~((small)1 << (bits(small) - 2));  // clear top-1 bit
      classert(compu_nn(a, alen, b, blen) > 0);  // a > b!
      r1 = bignum_alloc<small>(r1len);
      r2 = bignum_alloc<small>(r2len);
   }
   virtual uint64 estimate(uint64 len) {
      return len * blen;
   }
   virtual uint64 big_o(uint64 len) {
      return len * blen;
   }
   virtual bool do_size(index sz, uint64 free) {  // is called before setup()
      return sz >= 2 && 2 * sz * sizeof(small) < free;
   }
};

template<typename small>
class div_simple_2_benchmark : public div_benchmark<small> {
public:
   virtual void benchmark() {
      div_simple(r1, r1len, r2, r2len, a, alen, b, blen);
   }
   virtual string name() {
      return "div_simple(n,2)";
   }
};

template<typename small>
class div_knuth_2_benchmark : public div_benchmark<small> {
public:
   virtual void benchmark() {
      div_knuth(r1, r1len, r2, r2len, a, alen, b, blen);
   }
   virtual string name() {
      return "div_knuth(n,2)";
   }
};


// ------ add/sub

template<typename small>
class adder_benchmark : public binary_benchmark<small> {
public:
   virtual void setup(index len) {
      alen = len;
      blen = len;
      rlen = len;
      destroy();
      a = rand_alloc<small>(alen);
      b = rand_alloc<small>(blen);
      r = bignum_alloc<small>(rlen);
   }
   virtual uint64 estimate(uint64 len) {
      return len + 1;
   }
   virtual uint64 big_o(uint64 len) {
      return len;
   }
};

template<typename small>
class doubleadder_benchmark : public trinary_benchmark<small> {
public:
   virtual void setup(index len) {
      alen = len;
      blen = len;
      r1len = len;
      r2len = len;
      destroy();
      a = rand_alloc<small>(alen);
      b = rand_alloc<small>(blen);
      r1 = bignum_alloc<small>(r1len);
      r2 = bignum_alloc<small>(r2len);
   }
   virtual uint64 estimate(uint64 len) {
      return len + 1;
   }
   virtual uint64 big_o(uint64 len) {
      return len;
   }
};

template<typename small>
class addi_benchmark : public adder_benchmark<small> {
public:
   virtual void benchmark() {
      addi<small>(r, rlen, a, alen, b, blen);
   }
   virtual string name() {
      return "addi(n,n)";
   }
};

template<typename small>
class addu_benchmark : public adder_benchmark<small> {
public:
   virtual void benchmark() {
      addu<small>(r, rlen, a, alen, b, blen);
   }
   virtual string name() {
      return "addu(n,n)";
   }
};

template<typename small>
class addu_on_benchmark : public adder_benchmark<small> {
public:
   virtual void benchmark() {
      addu_on<small>(r, rlen, a, alen);
   }
   virtual string name() {
      return "addu_on(n,n)";
   }
};

template<typename small>
class add_fast_benchmark : public adder_benchmark<small> {
public:
   virtual void benchmark() {
      add_fast<small>(r, a, b, blen);
   }
   virtual string name() {
      return "add_fast(n)";
   }
};

template<typename small>
class adder2_benchmark : public adder_benchmark<small> {
public:
   virtual bool do_size(index sz, uint64 free) {
      return sz >= 2 && (2*sz + sz/2) * sizeof(small) < free;
   }
   virtual uint64 mem_usage() {
      return (rlen + alen + blen/2) * sizeof(small);
   }
};

template<typename small>
class addi2_benchmark : public adder2_benchmark<small> {
public:
   virtual void benchmark() {
      addi<small>(r, rlen, a, alen, b, blen/2);
   }
   virtual string name() {
      return "addi(n,n/2)";
   }
};

template<typename small>
class addu2_benchmark : public adder2_benchmark<small> {
public:
   virtual void benchmark() {
      addu<small>(r, rlen, a, alen, b, blen/2);
   }
   virtual string name() {
      return "addu(n,n/2)";
   }
};

#if defined _M_X64 && defined BN_ADD_USE_ASMX64
template<typename small>
class add8_asmx64_benchmark : public adder_benchmark<small> {
public:
   virtual void benchmark() {
      classert(alen == rlen);
      classert(blen == rlen);
      add8_asmx64((uint64*)r, (uint64*)a, (uint64*)b, rlen);
   }
   virtual string name() {
      return "add8_asmx64(n,n)";
   }
};

template<typename small>
class add4_asmx64_benchmark : public adder_benchmark<small> {
public:
   virtual void benchmark() {
      classert(alen == rlen);
      classert(blen == rlen);
      add4_asmx64((uint64*)r, (uint64*)a, (uint64*)b, rlen);
   }
   virtual string name() {
      return "add4_asmx64(n,n)";
   }
};
#endif

#if defined _M_X64 && defined BN_ADDSUB_USE_ASMX64
template<typename small>
class addsub_asmx64_benchmark : public doubleadder_benchmark<small> {
public:
   virtual void benchmark() {
      classert(alen == r1len);
      classert(blen == r1len);
      classert(r1len == r2len);
      addsub_asmx64((uint64*)r1, (uint64*)r2, (uint64*)a, (uint64*)b, r1len);
   }
   virtual string name() {
      return "addsub_asmx64(n)";
   }
};
#endif

#if defined _M_X64
template<typename small>
class addsub2_asmx64_benchmark : public doubleadder_benchmark<small> {
public:
   virtual void benchmark() {
      classert(alen == r1len);
      classert(blen == r1len);
      classert(r1len == r2len);
      addsub2_asmx64((uint64*)r1, (uint64*)r2, (uint64*)a, (uint64*)b, r1len);
   }
   virtual string name() {
      return "addsub2_asmx64(n)";
   }
};
#endif

#if defined _M_X64
template<typename small>
class addsub3_asmx64_benchmark : public doubleadder_benchmark<small> {
public:
   virtual void benchmark() {
      classert(alen == r1len);
      classert(blen == r1len);
      classert(r1len == r2len);
      addsub3_asmx64((uint64*)r1, (uint64*)r2, (uint64*)a, (uint64*)b, r1len);
   }
   virtual string name() {
      return "addsub3_asmx64(n)";
   }
};
#endif

#if defined _M_X64
template<typename small>
class threeop_benchmark : public adder_benchmark<small> {
public:
   virtual void benchmark() {
      classert(alen == rlen);
      classert(blen == rlen);
      threeop_asmx64((uint64*)r, (uint64*)a, (uint64*)b, rlen);
   }
   virtual string name() {
      return "threeop_asmx64(n,n)";
   }
};

template<typename small>
class threeop2_benchmark : public adder_benchmark<small> {
public:
   virtual void benchmark() {
      classert(alen == rlen);
      classert(blen == rlen);
      threeop2_asmx64((uint64*)r, (uint64*)a, (uint64*)b, rlen);
   }
   virtual string name() {
      return "threeop2_asmx64(n,n)";
   }
};

template<typename small>
class threeop3_benchmark : public adder_benchmark<small> {
public:
   virtual void benchmark() {
      classert(alen == rlen);
      classert(blen == rlen);
      threeop3_asmx64((uint64*)r, (uint64*)a, (uint64*)b, rlen);
   }
   virtual string name() {
      return "threeop3_asmx64(n,n)";
   }
};
#endif


template<typename small>
class sub_fast_benchmark : public adder_benchmark<small> {
public:
   virtual void benchmark() {
      sub_fast<small>(r, a, b, blen);
   }
   virtual string name() {
      return "sub_fast(n)";
   }
};

template<typename small>
class subi_benchmark : public adder_benchmark<small> {
public:
   virtual void benchmark() {
      subi<small>(r, rlen, a, alen, b, blen);
   }
   virtual string name() {
      return "subi(n,n)";
   }
};

template<typename small>
class subu_benchmark : public adder_benchmark<small> {
public:
   virtual void benchmark() {
      subu<small>(r, rlen, a, alen, b, blen);
   }
   virtual string name() {
      return "subu(n,n)";
   }
};

#if defined _M_X64 && defined BN_SUB_USE_ASMX64
template<typename small>
class sub2_asmx64_benchmark : public adder_benchmark<small> {
public:
   virtual void benchmark() {
      classert(alen == rlen);
      classert(blen == rlen);
      sub2_asmx64((uint64*)r, (uint64*)a, (uint64*)b, rlen);
   }
   virtual string name() {
      return "sub2_asmx64(n,n)";
   }
};

template<typename small>
class sub4_asmx64_benchmark : public adder_benchmark<small> {
public:
   virtual void benchmark() {
      classert(alen == rlen);
      classert(blen == rlen);
      sub4_asmx64((uint64*)r, (uint64*)a, (uint64*)b, rlen);
   }
   virtual string name() {
      return "sub4_asmx64(n,n)";
   }
};

template<typename small>
class sub8_asmx64_benchmark : public adder_benchmark<small> {
public:
   virtual void benchmark() {
      classert(alen == rlen);
      classert(blen == rlen);
      sub8_asmx64((uint64*)r, (uint64*)a, (uint64*)b, rlen);
   }
   virtual string name() {
      return "sub8_asmx64(n,n)";
   }
};
#endif


// ------ logic

template<typename small>
class shl_benchmark : public adder_benchmark<small> {
public:
   virtual void benchmark() {
      shl<small>(r, rlen, a, alen, 1);
   }
   virtual string name() {
      return "shl(n,1)";
   }
};

template<typename small>
class shl_on_benchmark : public adder_benchmark<small> {
public:
   virtual void benchmark() {
      shl_on<small>(a, alen, 1);
   }
   virtual string name() {
      return "shl_on(n,1)";
   }
};

template<typename small>
class shr_benchmark : public adder_benchmark<small> {
public:
   virtual void benchmark() {
      shr<small>(r, rlen, a, alen, 1);
   }
   virtual string name() {
      return "shr(n,1)";
   }
};

template<typename small>
class shr_on_benchmark : public adder_benchmark<small> {
public:
   virtual void benchmark() {
      shr_on<small>(a, alen, 1);
   }
   virtual string name() {
      return "shr_on(n,1)";
   }
};

template<typename small>
class smul_addsub_mul1_benchmark : public adder_benchmark<small> {
public:
   virtual void benchmark() {
      smul_addsub_mul(a, b, (alen-1)*bits(small), alen, bits(small));
   }
   virtual string name() {
      return format("smul_addsub_mul(n,%?)") << bits(small);
   }
   virtual bool do_size(index sz, uint64 free) {
      return sz > 1 && adder_benchmark<small>::do_size(sz, free);
   }
};

template<typename small>
class smul_addsub_mul2_benchmark : public adder_benchmark<small> {
public:
   virtual void benchmark() {
      smul_addsub_mul(a, b, (alen-1)*bits(small), alen, 1);
   }
   virtual string name() {
      return "smul_addsub_mul(n,1)";
   }
   virtual bool do_size(index sz, uint64 free) {
      return sz > 1 && adder_benchmark<small>::do_size(sz, free);
   }
};

template<typename small>
class smul_addsub_mul3_benchmark : public adder_benchmark<small> {
public:
   virtual void benchmark() {
      smul_addsub_mul(a, b, (alen-1)*bits(small), alen, (alen-1)*bits(small));
   }
   virtual string name() {
      return "smul_addsub_mul(n,n)";
   }
   virtual bool do_size(index sz, uint64 free) {
      return sz > 1 && adder_benchmark<small>::do_size(sz, free);
   }
};


// ------ mul

template<typename small>
class mul_benchmark : public bn_benchmark {
protected:
   small* a;
   small* b;
   small* r;
   index alen;
   index blen;
   index rlen;
public:
   mul_benchmark() {
      a = nullptr;
      b = nullptr;
      r = nullptr;
   }
   virtual void destroy() {
      bignum_free(a); a = nullptr;
      bignum_free(b); b = nullptr;
      bignum_free(r); r = nullptr;
   }
   virtual uint64 mem_usage() {
      return (rlen + alen + blen) * sizeof(small);
   }
   virtual bool do_size(index sz, uint64 free) {
      return 4 * sz * sizeof(small) < free;
   }
   virtual void setup(index len) {
      destroy();
      alen = len;
      blen = len;
      rlen = 2*len;
      a = rand_alloc<small>(alen);
      b = rand_alloc<small>(blen);
      r = bignum_alloc<small>(rlen);
   }
   virtual uint64 estimate(uint64 len) {
      return len * len;
   }
   virtual uint64 big_o(uint64 len) {
      return len * len;
   }
};

template<typename small>
class muli_benchmark : public mul_benchmark<small> {
public:
   virtual void benchmark() {
      muli<small>(r, a, alen, b, blen);
   }
   virtual string name() {
      return "muli(n,n)";
   }
};

template<typename small>
class omul_benchmark : public mul_benchmark<small> {
public:
   virtual void benchmark() {
      omul_ls<small>(r, a, alen, b, blen);
   }
   virtual string name() {
      return "omul(n,n)";
   }
};

#if defined _M_X64 && defined BN_OMUL_USE_ASMX64
class omul_asm_benchmark : public mul_benchmark<uint64> {
public:
   virtual void benchmark() {
      omul_asmx64(r, a, alen, b, blen);
   }
   virtual string name() {
      return "omul_asmx64(n,n)";
   }
};
#endif

#if defined _M_X64 && defined BN_OMUL2_USE_ASMX64
class omul2_asm_benchmark : public mul_benchmark<uint64> {
public:
   virtual void benchmark() {
      omul2_asmx64(r, a, alen, b, blen);
   }
   virtual string name() {
      return "omul2_asmx64(n,n)";
   }
};
#endif

#if defined _M_X64 && defined BN_OMUL3_USE_ASMX64
class omul3_asm_benchmark : public mul_benchmark<uint64> {
public:
   virtual void benchmark() {
      omul3_asmx64(r, a, alen, b, blen);
   }
   virtual string name() {
      return "omul3_asmx64(n,n)";
   }
};
#endif

#if defined _M_X64 && defined BN_OMUL4_USE_ASMX64
class omul4_asm_benchmark : public mul_benchmark<uint64> {
public:
   virtual void benchmark() {
      omul4_asmx64(r, a, alen, b, blen);
   }
   virtual string name() {
      return "omul4_asmx64(n,n)";
   }
};
#endif

template<typename small>
class omul_plain_benchmark : public mul_benchmark<small> {
public:
   virtual void benchmark() {
      omul_plain<small>(r, a, alen, b, blen);
   }
   virtual string name() {
      return "omul_plain(n,n)";
   }
};

template<typename small>
class omul_interleaved_benchmark : public mul_benchmark<small> {
public:
   virtual void benchmark() {
      omul_interleaved<small>(r, a, alen, b, blen);
   }
   virtual string name() {
      return "omul_interleaved(n,n)";
   }
};

template<typename small>
class omul_interleaved2_benchmark : public mul_benchmark<small> {
public:
   virtual void benchmark() {
      omul_interleaved2<small>(r, a, alen, b, blen);
   }
   virtual string name() {
      return "omul_interleaved2(n,n)";
   }
};

template<typename small>
class omul_interleaved3_benchmark : public mul_benchmark<small> {
public:
   virtual void benchmark() {
      omul_interleaved3<small>(r, a, alen, b, blen);
   }
   virtual string name() {
      return "omul_interleaved3(n,n)";
   }
};

template<typename small>
class omul_unrolled_benchmark : public mul_benchmark<small> {
public:
   virtual void benchmark() {
      omul_unrolled<small>(r, a, alen, b, blen);
   }
   virtual string name() {
      return "omul_unrolled(n,n)";
   }
};

template<typename small>
class omul_interleaved_unrolled_benchmark : public mul_benchmark<small> {
public:
   virtual void benchmark() {
      omul_interleaved_unrolled<small>(r, a, alen, b, blen);
   }
   virtual string name() {
      return "omul_interleaved_unrolled(n,n)";
   }
};

template<typename small>
class omul_interleaved2_unrolled_benchmark : public mul_benchmark<small> {
public:
   virtual void benchmark() {
      omul_interleaved2_unrolled<small>(r, a, alen, b, blen);
   }
   virtual string name() {
      return "omul_interleaved2_unrolled(n,n)";
   }
};

template<typename small>
class kmul_benchmark : public mul_benchmark<small> {
   index old_thresh;
public:
   kmul_benchmark() : old_thresh(0) { }
   virtual void benchmark() {
      kmul<small>(r, a, alen, b, blen);
   }
   virtual string name() {
      return "kmul(n,n)";
   }
   virtual uint64 estimate(uint64 len) {
      return ceil(26 * pow(len, log2_3));
   }
   virtual uint64 big_o(uint64 len) {
      return ceil(pow(len, log2_3));
   }
   virtual bool do_size(index sz, uint64 free) {
      return sz >= kmul_min_len;
   }
   virtual void setup(index len) {
      //tout_puts(" (setup) ");
      mul_benchmark<small>::setup(len);
      if (len < kmul_thresh) {
         old_thresh = switch_kmul(len);
         classert(kmul_thresh == len);
      }
   }
   virtual void destroy() {
      //tout_puts(" (destroy) ");
      mul_benchmark<small>::destroy();
      if (old_thresh != 0) {
         switch_kmul(old_thresh);
         old_thresh = 0;
      }
   }
};

template<typename small>
class t3mul_benchmark : public mul_benchmark<small> {
   index old_thresh;
public:
   t3mul_benchmark() : old_thresh(0) { }
   virtual void benchmark() {
      t3mul<small>(r, a, alen, b, blen);
   }
   virtual string name() {
      return "t3mul(n,n)";
   }
   virtual uint64 estimate(uint64 len) {
      return ceil(47 * pow(len, log3_5));
   }
   virtual uint64 big_o(uint64 len) {
      return ceil(pow(len, log3_5));
   }
   virtual bool do_size(index sz, uint64 free) {
      return sz >= t3mul_min_len;
   }
   virtual void setup(index len) {
      //tout_puts(" (setup) ");
      mul_benchmark<small>::setup(len);
      if (len < t3mul_thresh) {
         old_thresh = switch_t3mul(len);
         classert(t3mul_thresh == len);
      }
   }
   virtual void destroy() {
      //tout_puts(" (destroy) ");
      mul_benchmark<small>::destroy();
      if (old_thresh != 0) {
         switch_t3mul(old_thresh);
         old_thresh = 0;
      }
   }
};

template<typename small>
class smul_benchmark : public mul_benchmark<small> {
public:
   virtual void benchmark() {
      smul<small>(r, a, alen, b, blen);
   }
   virtual string name() {
      return "smul(n,n)";
   }
   virtual uint64 estimate(uint64 len) {
      return ceil(40 * len * xlog2(len) * xlog2(xlog2(len)));
   }
   virtual uint64 big_o(uint64 len) {
      return ceil(len * xlog2(len) * xlog2(xlog2(len)));
   }
};

template<typename small>
class qmul_benchmark : public mul_benchmark<small> {
   index old_thresh;
public:
   qmul_benchmark() : old_thresh(0) { }
   virtual bool do_size(index sz, uint64 free) {
      return sz > qmul_min_len;
   }
   virtual void benchmark() {
      qmul<small>(r, a, alen, b, blen);
   }
   virtual string name() {
      return "qmul(n,n)";
   }
   virtual uint64 estimate(uint64 len) {
      return ceil(40 * len * xlog2(len));
   }
   virtual uint64 big_o(uint64 len) {
      return ceil(len * xlog2(len));
   }
   virtual void setup(index len) {
      //tout_puts(" (setup) ");
      mul_benchmark<small>::setup(len);
      if (len < qmul_thresh) {
         old_thresh = switch_qmul(len);
         classert(qmul_thresh == len);
      }
   }
   virtual void destroy() {
      //tout_puts(" (destroy) ");
      mul_benchmark<small>::destroy();
      if (old_thresh != 0) {
         switch_qmul(old_thresh);
         old_thresh = 0;
      }
   }
};

template<typename small>
class dkss_mul_benchmark : public mul_benchmark<small> {
public:
   dkss_mul_benchmark() { }
   virtual void benchmark() {
      q_dkss_mul1<small, false>(r, a, alen, b, blen);
   }
   virtual string name() {
      return "dkss_mul(n,n)";
   }
   virtual uint64 estimate(uint64 len) {
      return ceil(40 * len * xlog2(len) * pow(2.0, xlog2_star(len)));
   }
   virtual uint64 big_o(uint64 len) {
      return ceil(len * xlog2(len) * pow(2.0, xlog2_star(len)));
   }
};

template<typename small>
class mulu_benchmark : public mul_benchmark<small> {
public:
   virtual void benchmark() {
      mulu<small>(r, a, alen, b, blen);
   }
   virtual string name() {
      return "mulu(n,n)";
   }
   virtual uint64 estimate(uint64 len) {
      return ceil(40 * len * xlog2(len) * xlog2(xlog2(len)));
   }
   virtual uint64 big_o(uint64 len) {
      return ceil(len * xlog2(len) * xlog2(xlog2(len)));
   }
};

extern uint64 dummy_global1;

template<typename small>
class modmul_benchmark : public bn_benchmark {
   small u, v;
   unsigned cnt;
public:
   modmul_benchmark() {
      u = make_rand.get<small>() & (((small)1 << (bits(small)-1)) - 1);
      v = make_rand.get<small>() & (((small)1 << (bits(small)-1)) - 1);
      cnt = 0;
   }
   virtual bool do_size(index sz, uint64 free) {
      return cnt++ == 0;  // do any size, but just once
   }
   virtual void setup(index len) { }
   virtual void destroy() { }
   virtual uint64 mem_usage() { return 0; }
   virtual void benchmark() {
      dummy_global1 = f_modmul<small>(u, v);
   }
   virtual string name() {
      return "modmul()";
   }
   virtual uint64 estimate(uint64 len) {
      return 30;
   }
   virtual uint64 big_o(uint64 len) {
      return 1;
   }
};

template<typename small>
class modadd_benchmark : public bn_benchmark {
   small u, v;
   unsigned cnt;
public:
   modadd_benchmark() {
      u = make_rand.get<small>() & (((small)1 << (bits(small)-1)) - 1);
      v = make_rand.get<small>() & (((small)1 << (bits(small)-1)) - 1);
      cnt = 0;
   }
   virtual bool do_size(index sz, uint64 free) {
      return cnt++ == 0;  // do any size, but just once
   }
   virtual void setup(index len) { }
   virtual void destroy() { }
   virtual uint64 mem_usage() { return 0; }
   virtual void benchmark() {
      dummy_global1 = f_modadd<small>(u, v);
   }
   virtual string name() {
      return "modadd()";
   }
   virtual uint64 estimate(uint64 len) {
      return 30;
   }
   virtual uint64 big_o(uint64 len) {
      return 1;
   }
};

template<typename small>
class modsub_benchmark : public bn_benchmark {
   small u, v;
   unsigned cnt;
public:
   modsub_benchmark() {
      u = make_rand.get<small>() & (((small)1 << (bits(small)-1)) - 1);
      v = make_rand.get<small>() & (((small)1 << (bits(small)-1)) - 1);
      cnt = 0;
   }
   virtual bool do_size(index sz, uint64 free) {
      return cnt++ == 0;  // do any size, but just once
   }
   virtual void setup(index len) { }
   virtual void destroy() { }
   virtual uint64 mem_usage() { return 0; }
   virtual void benchmark() {
      dummy_global1 = f_modsub<small>(u, v);
   }
   virtual string name() {
      return "modsub()";
   }
   virtual uint64 estimate(uint64 len) {
      return 30;
   }
   virtual uint64 big_o(uint64 len) {
      return 1;
   }
};



// ------ squ

template<typename small>
class squ_benchmark : public bn_benchmark {
protected:
   small* a;
   small* r;
   index alen;
   index rlen;
public:
   squ_benchmark() {
      a = 0;
      r = 0;
   }
   virtual void destroy() {
      bignum_free(a); a = 0;
      bignum_free(r); r = 0;
   }
   virtual uint64 mem_usage() {
      return (rlen + alen) * sizeof(small);
   }
   virtual bool do_size(index sz, uint64 free) {
      return 3 * sz * sizeof(small) < free;
   }
   virtual void setup(index len) {
      alen = len;
      rlen = 2*len;
      destroy();
      a = rand_alloc<small>(alen);
      r = bignum_alloc<small>(rlen);
   }
   virtual uint64 estimate(uint64 len) {
      return len * len;
   }
   virtual uint64 big_o(uint64 len) {
      return len * len;
   }
};

template<typename small>
class osqu_plain_benchmark : public squ_benchmark<small> {
public:
   virtual void benchmark() {
      osqu_plain<small>(r, a, alen);
   }
   virtual string name() {
      return "osqu_plain(n)";
   }
};

template<typename small>
class osqu_benchmark : public squ_benchmark<small> {
public:
   virtual void benchmark() {
      osqu<small>(r, a, alen);
   }
   virtual string name() {
      return "osqu(n)";
   }
};

#if defined _M_X64 && defined BN_OSQU4_USE_ASMX64
class osqu4_asm_benchmark : public squ_benchmark<uint64> {
public:
   virtual void benchmark() {
      osqu4_asmx64(r, a, alen);
   }
   virtual string name() {
      return "osqu4_asmx64(n)";
   }
};
#endif

template<typename small>
class ksqu_benchmark : public squ_benchmark<small> {
public:
   virtual void benchmark() {
      ksqu<small>(r, a, alen);
   }
   virtual string name() {
      return "ksqu(n,n)";
   }
   virtual uint64 estimate(uint64 len) {
      return ceil(26 * pow(len, log2_3));
   }
   virtual uint64 big_o(uint64 len) {
      return ceil(pow(len, log2_3));
   }
};

template<typename small>
class t3squ_benchmark : public squ_benchmark<small> {
public:
   virtual void benchmark() {
      t3squ<small>(r, a, alen);
   }
   virtual string name() {
      return "t3squ(n,n)";
   }
   virtual uint64 estimate(uint64 len) {
      return ceil(47 * pow(len, log3_5));
   }
   virtual uint64 big_o(uint64 len) {
      return ceil(pow(len, log3_5));
   }
};

template<typename small>
class ssqu_benchmark : public squ_benchmark<small> {
public:
   virtual void benchmark() {
      ssqu(r, a, alen);
   }
   virtual string name() {
      return "ssqu(n,n)";
   }
   virtual uint64 estimate(uint64 len) {
      return ceil(40 * len * xlog2(len) * xlog2(xlog2(len)));
   }
   virtual uint64 big_o(uint64 len) {
      return ceil(len * xlog2(len) * xlog2(xlog2(len)));
   }
};

template<typename small>
class qsqu_benchmark : public squ_benchmark<small> {
   index old_thresh;
public:
   qsqu_benchmark() : old_thresh(0) { }
   virtual bool do_size(index sz, uint64 free) {
      return sz > qmul_min_len;
   }
   virtual void benchmark() {
      qsqu(r, a, alen);
   }
   virtual string name() {
      return "qsqu(n,n)";
   }
   virtual uint64 estimate(uint64 len) {
      return ceil(40 * len * xlog2(len) * xlog2(xlog2(len)));
   }
   virtual uint64 big_o(uint64 len) {
      return ceil(len * xlog2(len) * xlog2(xlog2(len)));
   }
   virtual void setup(index len) {
      //tout_puts(" (setup) ");
      squ_benchmark<small>::setup(len);
      if (len < qmul_thresh) {
         old_thresh = switch_qsqu(len);
         classert(qsqu_thresh == len);
      }
   }
   virtual void destroy() {
      //tout_puts(" (destroy) ");
      squ_benchmark<small>::destroy();
      if (old_thresh != 0) {
         switch_qsqu(old_thresh);
         old_thresh = 0;
      }
   }
};

template<typename small>
class squu_benchmark : public ssqu_benchmark<small> {
public:
   squu_benchmark() { }
   virtual void benchmark() {
      qsqu(r, a, alen);
   }
   virtual string name() {
      return "squu(n,n)";
   }
};


#ifdef USE_GMP
// ------ GMP tests -----------------------------------------------------------

template<typename small>
class gmp_mul_benchmark : public smul_benchmark<small> {
public:
   gmp_mul_benchmark() { }
   virtual void benchmark() {
      gmp_mul<small>(r, a, alen, b, blen);
   }
   virtual string name() {
      return "gmp_mul(n,n)";
   }
};

template<typename small>
class gmp_mul_n_benchmark : public smul_benchmark<small> {
public:
   gmp_mul_n_benchmark() { }
   virtual void benchmark() {
      gmp_mul_n<small>(r, a, b, blen);
   }
   virtual string name() {
      return "gmp_mul_n(n,n)";
   }
};

template<typename small>
class gmp_sqr_benchmark : public ssqu_benchmark<small> {
public:
   gmp_sqr_benchmark() { }
   virtual void benchmark() {
      gmp_sqr<small>(r, a, alen);
   }
   virtual string name() {
      return "gmp_sqr(n)";
   }
};

template<typename small>
class gmp_mul_basecase_benchmark : public mul_benchmark<small> {
public:
   gmp_mul_basecase_benchmark() { }
   virtual void benchmark() {
      gmp_mul_basecase<small>(r, a, alen, b, blen);
   }
   virtual string name() {
      return "gmp_mul_basecase(n,n)";
   }
};
#endif


bench_param::bench_param() {
   all_sizes = 0;
   factor = 0.0;
   step_sz = 0.0;
   dkss_steps = 0;
   qmul_steps = 0;
   start_sz = 1;
   end_sz = UINT_MAX;
   half_sized = 0;
   run_long = 0;
   add_test = 0;
   sub_test = 0;
   mul_test = 0;
   squ_test = 0;
   div_test = 0;
   logic_test = 0;
   smul_test = 0;
   list_only = 0;
   fft_len = 0;
   verbose = 0;
   realtime = false;
}

bool bench_param::any_selected() const {
   return add_test || sub_test || mul_test || squ_test || div_test || logic_test || smul_test || !wildcards.empty();
}

void bench_param::inc_all() {
   ++add_test;
   ++sub_test;
   ++mul_test;
   ++squ_test;
   ++div_test;
   ++logic_test;
   ++smul_test;
}


template<class C>
void check_bench(const bench_param& bp, vector<bn_benchmark*>& benchs, const bool cond) {
   C* c = new C;
   auto is_listed = [&]() {
      const string name = c->name();
      for (size_t i=0; i<bp.wildcards.size(); ++i) {
         const string& wild = bp.wildcards[i];
         //tout_puts(format("%? %?\n") << name << wild);
         if (matche(wild.c_str(), name.c_str()) == MATCH_VALID) return true;
      }
      return false;
   };
   if (cond || is_listed()) benchs.push_back(c);
   else delete c;
}


void time_source_check() {
   tout_puts(system_info() + "\n");
   DWORD_PTR proc;
   DWORD_PTR sys;
   if (GetProcessAffinityMask(GetCurrentProcess(), &proc, &sys) && !is_pow2(proc))
      tout_puts("*** Set processor affinity to get more exact results!\n");
}


template<typename small>
static void calc_dkss_sizes(const bool precise, vector<index>& sizes, index start_sz, index end_sz, const bool verbose) {
   dkss_pc_bits_roundup = !precise;
   index d = t2max(start_sz, 2);
   dkss_parm<small> p;
   end_sz = tmin(end_sz, p.max_len);
   while (d <= end_sz) {
      dkss_set_mmu<small>(p, d, d);
      sizes.push_back(d);
      if (verbose) tout.puts(format("DKSS step start: %?\n") << d);
      // binary search for rising edge
      index h = (index)1 << (int_ceil_log2(d) - 1);
      index e = d+2*h;
      dkss_parm<small> q;
      while (1) {
         dkss_set_mmu<small>(q, e, e);
         if (h == 0) {
            if (!p.is_same(q, precise)) --e;  // e is last length with same params
            break;
         }
         if (p.is_same(q, precise)) {
            // same, increase
            e += h;
         }
         else {
            // different, decrease
            e -= h;
         }
         h /= 2;
      }
      dkss_set_mmu<small>(q, e, e);
      classert(p.is_same(q, precise));
      dkss_set_mmu<small>(q, e+1, e+1);
      classert(!p.is_same(q, precise));
      if (sizes.back() != e && e <= end_sz) {
         sizes.push_back(e);
         if (verbose) tout.puts(format("DKSS step end: %?\n") << e);
      }
      d = e+1;
   }
   if (verbose) tout.puts(format("Total %? DKSS steps\n") << sizes.size());
}

#ifdef CL_TEST_CODE
static void test_calc_dkss_sizes() {
   vector<index> s;
   calc_dkss_sizes<uint32>(true, s, 2, 2000, false);
}
#endif


struct qmul_p {
   unsigned depth;
   index cut_bits;
   bool operator==(const qmul_p& b) const {
      return depth == b.depth && cut_bits == b.cut_bits;
   }
   bool operator!=(const qmul_p& b) const {
      return !operator==(b);
   }
};

template<typename small>
static void calc_qmul_sizes(vector<index>& sizes, index start_sz, index end_sz, bool verbose) {
   index d = t2max(start_sz, qmul_min_len);
   qmul_p p;
   if (!qmul_set_params<small>(2*d, p.depth, p.cut_bits)) return;
   while (d <= end_sz) {
      if (!qmul_set_params<small>(2*d, p.depth, p.cut_bits)) return;
      sizes.push_back(d);
      if (verbose) tout.puts(format("QMUL step start: %?\n") << d);
      // binary search for rising edge
      index h = (index)1 << (int_ceil_log2(d) - 1);
      index e = d+2*h;
      qmul_p q;
      while (1) {
         if (!qmul_set_params<small>(2*e, q.depth, q.cut_bits)) return;
         if (h == 0) {
            if (p != q) --e;  // e is last length with same params
            break;
         }
         if (p == q) {
            // same, increase
            e += h;
         }
         else {
            // different, decrease
            e -= h;
         }
         h /= 2;
      }
      if (!qmul_set_params<small>(2*e, q.depth, q.cut_bits)) return;
      classert(p == q);
      if (!qmul_set_params<small>(2*(e+1), q.depth, q.cut_bits)) return;
      classert(p != q);
      if (sizes.back() != e && e <= end_sz) {
         sizes.push_back(e);
         if (verbose) tout.puts(format("QMUL step end: %?\n") << e);
      }
      d = e+1;
   }
}

#ifdef CL_TEST_CODE
static void test_calc_qmul_sizes() {
   vector<index> s;
   calc_qmul_sizes<uint32>(s, 2, 2000, false);
}
#endif

// #define DBG_PRINT 1

template<typename small>
static void benchmark(bench_param& bp) {
   set_prio_class prio_cls(bp.realtime ? REALTIME_PRIORITY_CLASS : HIGH_PRIORITY_CLASS);
   set_thread_prio th_prio(bp.realtime ? THREAD_PRIORITY_TIME_CRITICAL : THREAD_PRIORITY_HIGHEST);

   const index use_max_mem = physical_mem(true) / 8 * 7;
   string s;
   strformat ss(s);

   time_source_check();

   vector<index> sizes;
   bool end_set = bp.end_sz != UINT_MAX;
   bp.end_sz = tmin(bp.end_sz, use_max_mem / sizeof(small));
   if (bp.factor == 0.0 && bp.step_sz == 0.0 && bp.dkss_steps == 0 && bp.qmul_steps == 0) {
      if (bp.all_sizes > 1) {
         bp.start_sz = 1;
         bp.factor = 2;
      }
      else {
         bp.start_sz = 8;
         bp.factor = 4;
      }
   }
   if (bp.start_sz < 1.0) bp.start_sz = 1.0;
   if (bp.factor > 1.0) {
      //tout_puts(format("do sizes from %? to %? factor step %?\n") << bp.start_sz << bp.end_sz << bp.factor);
      unsigned i = 1;
      double d = bp.start_sz;
      while (d <= bp.end_sz) {
         //tout_puts(format("d = %.10?\n") << d);
         sizes.push_back((index)d);
         d = tmax<double>(ceil(bp.start_sz * pow(bp.factor, (double)i)), d+1);
         i = ceil(log(d / bp.start_sz) / log(bp.factor)) + 1;
      }
   }
   else if (bp.step_sz > 0.0) {
      //tout_puts(format("do sizes from %? to %? step %?\n") << bp.start_sz << bp.end_sz << bp.step_sz);
      double d = bp.start_sz;
      while (d <= bp.end_sz) {
         //tout_puts(format("d = %.10?\n") << d);
         sizes.push_back((index)d);
         d = ceil(tmax<double>(d+bp.step_sz, d+1));
      }
   }
   else if (bp.dkss_steps) {
      calc_dkss_sizes<small>(bp.dkss_steps > 1, sizes, bp.start_sz, bp.end_sz, bp.verbose > 0);
   }
   else if (bp.qmul_steps) {
      calc_qmul_sizes<small>(sizes, bp.start_sz, bp.end_sz, bp.verbose > 0);
   }
   else {
      tout_puts("factor must be > 1.0 or step must be > 0.0!\n");
      return;
   }
   vector<bn_benchmark*> benchs;
   check_bench<null1_benchmark>(bp, benchs, true);
   check_bench<null2_benchmark>(bp, benchs, true);
   check_bench<memcpy_benchmark<small>>(bp, benchs, bp.add_test > 1 || bp.sub_test > 1);
   check_bench<memcpy_unaligned_benchmark<small>>(bp, benchs, bp.add_test > 1 || bp.sub_test > 1);
   check_bench<copy_in_benchmark<small>>(bp, benchs, bp.add_test > 1 || bp.sub_test > 1);
#if defined _M_X64
   if (sizeof(small) == sizeof(uint64)) {
      check_bench<threeop_benchmark<small>>(bp, benchs, bp.add_test > 1 || bp.sub_test > 1);
      check_bench<threeop2_benchmark<small>>(bp, benchs, bp.add_test > 1 || bp.sub_test > 1);
      check_bench<threeop3_benchmark<small>>(bp, benchs, bp.add_test > 1 || bp.sub_test > 1);
   }
#endif
   check_bench<transpose_odd_benchmark<small>>(bp, benchs, bp.add_test > 1 || bp.sub_test > 1);
   check_bench<transpose_pow2_benchmark<small>>(bp, benchs, bp.add_test > 1 || bp.sub_test > 1);

   check_bench<addu_benchmark<small>>(bp, benchs, bp.add_test > 0);
   check_bench<addu_on_benchmark<small>>(bp, benchs, bp.add_test > 0);
   check_bench<addi_benchmark<small>>(bp, benchs, bp.add_test > 1);
   check_bench<add_fast_benchmark<small>>(bp, benchs, bp.add_test > 1);
   check_bench<addi2_benchmark<small>>(bp, benchs, bp.add_test > 1);
   check_bench<addu2_benchmark<small>>(bp, benchs, bp.add_test > 1);
#if defined _M_X64 && defined BN_ADD_USE_ASMX64
   if (sizeof(small) == sizeof(uint64)) {
      check_bench<add4_asmx64_benchmark<small>>(bp, benchs, bp.add_test > 1);
      check_bench<add8_asmx64_benchmark<small>>(bp, benchs, bp.add_test > 1);
   }
#endif

   check_bench<subu_benchmark<small>>(bp, benchs, bp.sub_test > 0);
   check_bench<subi_benchmark<small>>(bp, benchs, bp.sub_test > 1);
   check_bench<sub_fast_benchmark<small>>(bp, benchs, bp.sub_test > 1);
#if defined _M_X64 && defined BN_SUB_USE_ASMX64
   if (sizeof(small) == sizeof(uint64)) {
      check_bench<sub2_asmx64_benchmark<small>>(bp, benchs, bp.sub_test > 1);
      check_bench<sub4_asmx64_benchmark<small>>(bp, benchs, bp.sub_test > 1);
      check_bench<sub8_asmx64_benchmark<small>>(bp, benchs, bp.sub_test > 1);
   }
#endif

#if defined _M_X64
   if (sizeof(small) == sizeof(uint64)) {
#if defined BN_ADDSUB_USE_ASMX64
      check_bench<addsub_asmx64_benchmark<small>>(bp, benchs, bp.add_test > 0 || bp.sub_test > 0);
#endif
      check_bench<addsub2_asmx64_benchmark<small>>(bp, benchs, bp.add_test > 1 || bp.sub_test > 1);
      check_bench<addsub3_asmx64_benchmark<small>>(bp, benchs, bp.add_test > 1 || bp.sub_test > 1);
   }
#endif

   check_bench<shl_benchmark<small>>(bp, benchs, bp.logic_test > 0);
   check_bench<shl_on_benchmark<small>>(bp, benchs, bp.logic_test > 0);
   check_bench<shr_benchmark<small>>(bp, benchs, bp.logic_test > 0);
   check_bench<shr_on_benchmark<small>>(bp, benchs, bp.logic_test > 0);

   check_bench<divby3_benchmark<small>>(bp, benchs, bp.div_test > 1);
   check_bench<divby3_2_benchmark<small>>(bp, benchs, bp.div_test > 1);
   check_bench<divby3_3_benchmark<small>>(bp, benchs, bp.div_test > 1);
   check_bench<divby3_fast_benchmark<small>>(bp, benchs, bp.div_test > 0);
   check_bench<div1_benchmark<small>>(bp, benchs, bp.div_test > 0);
   check_bench<div_simple_2_benchmark<small>>(bp, benchs, bp.div_test > 1);
   check_bench<div_knuth_2_benchmark<small>>(bp, benchs, bp.div_test > 0);

   check_bench<smul_addsub_mul1_benchmark<small>>(bp, benchs, bp.smul_test > 0);
   check_bench<smul_addsub_mul2_benchmark<small>>(bp, benchs, bp.smul_test > 0);
   check_bench<smul_addsub_mul3_benchmark<small>>(bp, benchs, bp.smul_test > 0);

#if defined _M_X64 && defined BN_OMUL_USE_ASMX64
   if (sizeof(small) == sizeof(uint64))
      check_bench<omul_asm_benchmark>(bp, benchs, bp.mul_test > 1);
#endif
#if defined _M_X64 && defined BN_OMUL2_USE_ASMX64
   if (sizeof(small) == sizeof(uint64))
      check_bench<omul2_asm_benchmark>(bp, benchs, bp.mul_test > 1);
#endif
#if defined _M_X64 && defined BN_OMUL3_USE_ASMX64
   if (sizeof(small) == sizeof(uint64))
      check_bench<omul3_asm_benchmark>(bp, benchs, bp.mul_test > 1);
#endif
#if defined _M_X64 && defined BN_OMUL4_USE_ASMX64
   if (sizeof(small) == sizeof(uint64))
      check_bench<omul4_asm_benchmark>(bp, benchs, bp.mul_test > 1);
#endif
   check_bench<omul_plain_benchmark<small>>(bp, benchs, bp.mul_test > 2);
   check_bench<omul_unrolled_benchmark<small>>(bp, benchs, bp.mul_test > 2);
   check_bench<omul_interleaved_benchmark<small>>(bp, benchs, bp.mul_test > 2);
   check_bench<omul_interleaved_unrolled_benchmark<small>>(bp, benchs, bp.mul_test > 2);
   check_bench<omul_interleaved2_benchmark<small>>(bp, benchs, bp.mul_test > 2);
   check_bench<omul_interleaved2_unrolled_benchmark<small>>(bp, benchs, bp.mul_test > 2);
   check_bench<omul_interleaved3_benchmark<small>>(bp, benchs, bp.mul_test > 2);

   check_bench<omul_benchmark<small>>(bp, benchs, bp.mul_test > 0);
   check_bench<kmul_benchmark<small>>(bp, benchs, bp.mul_test > 0);
   check_bench<t3mul_benchmark<small>>(bp, benchs, bp.mul_test > 0);
   check_bench<smul_benchmark<small>>(bp, benchs, bp.mul_test > 0);
   check_bench<qmul_benchmark<small>>(bp, benchs, bp.mul_test > 0);
   check_bench<dkss_mul_benchmark<small>>(bp, benchs, bp.mul_test > 0);
   check_bench<mulu_benchmark<small>>(bp, benchs, bp.mul_test > 2);
   check_bench<modmul_benchmark<small>>(bp, benchs, bp.mul_test > 2);
   check_bench<modadd_benchmark<small>>(bp, benchs, bp.mul_test > 2);
   check_bench<modsub_benchmark<small>>(bp, benchs, bp.mul_test > 2);

#if defined _M_X64 && defined BN_OSQU4_USE_ASMX64
   if (sizeof(small) == sizeof(uint64))
      check_bench<osqu4_asm_benchmark>(bp, benchs, bp.squ_test > 1);
#endif

   check_bench<osqu_plain_benchmark<small>>(bp, benchs, bp.squ_test > 2);
   check_bench<osqu_benchmark<small>>(bp, benchs, bp.squ_test > 0);
   check_bench<ksqu_benchmark<small>>(bp, benchs, bp.squ_test > 0);
   check_bench<t3squ_benchmark<small>>(bp, benchs, bp.squ_test > 0);
   check_bench<ssqu_benchmark<small>>(bp, benchs, bp.squ_test > 0);
   check_bench<qsqu_benchmark<small>>(bp, benchs, bp.squ_test > 0);
   check_bench<squu_benchmark<small>>(bp, benchs, bp.squ_test > 0);

#ifdef USE_GMP
   check_bench<gmp_mul_benchmark<small>>(bp, benchs, bp.mul_test > 2);
   check_bench<gmp_mul_n_benchmark<small>>(bp, benchs, bp.mul_test > 2);
   check_bench<gmp_sqr_benchmark<small>>(bp, benchs, bp.mul_test > 2);
   check_bench<gmp_mul_basecase_benchmark<small>>(bp, benchs, bp.mul_test > 2);
#endif

   uint64 null1_ticks = 0;
   uint64 null2_ticks = 0;
   uint64 null2_rounds = 1;
   for (size_t j=0; j<benchs.size(); ++j) {
      auto& b = *benchs[j];
      const bool is_null1 = b.name() == "null1";
      const bool is_null2 = b.name() == "null2";
      const bool is_null = is_null1 || is_null2;
      if (!is_null) {
         tout_puts(format("benchmarking %?:\n") << b.name());
         s = " size/w  mem/b rounds     secs      cyc/w    cyc/O    dev/O     b/s    size/w        cyc/r\n";
         tout_puts(s);
         s.clear();
      }
      double c = 1000 CLDEBUG(/ 5);
      const unsigned runs = pow(2, bp.run_long + 1) + 1;  // 3, 5, 9, 17, ...
      const uint64 min_cycles = (uint64)tmin(pow(4.0, bp.run_long + 1.0), 4000000000.0) * 500*1000*1000ui64 / runs;
      bool loop = true;
      for (size_t i=0; loop && i<sizes.size(); ++i) {
         const auto sz = sizes[i];
         if (!b.do_size(sz, use_max_mem)) continue;
         b.setup(sz);

         const uint64 est = tmax<uint64>(c * b.big_o(sz), 1);  // estimated cycles
         if (est > min_cycles * 2 && !end_set) {  // it would take too long
            //tout_puts(format("min_cylces=%'? est=%'? sz=%'? big_o(sz)=%'? c=%,.02?\n") << min_cycles << est << sz << b.big_o(sz) << (uint64)(c*100));
            b.destroy();
            break;
         }

#ifdef DBG_PRINT
         tout_puts(format("sz=%? ") << sz);
#endif
         if (!is_null) {
            ss(" %6?") << scaled_count(sz, SCT_BIGGEST_UNIT|SCT_NO_SPACE|SCT_MAX3);
            ss(" %6?") << scaled_count(b.mem_usage(), SCT_BIGGEST_UNIT|SCT_NO_SPACE|SCT_MAX3);
            tout_puts(s);
            s.clear();
         }

         uint64 rounds = is_null1 ? 0 :
            is_null2 ? 100*1000*1000 :
            tmax<uint64>(1000000 / est / runs, 1);
#ifdef DBG_PRINT
         tout_puts(format("rounds=%? ") << rounds);
#endif
         uint64 total_nsec = 0;
         uint64 ticks;
         uint64 best_ticks = _UI64_MAX;
         stat_avgdev<double> stat;
         unsigned run = 0;
         unsigned pass = 1;
#ifndef DBG_PRINT
         tout_puts(" ");
#endif
         //tout_puts(format(" (kmul_thresh=%?) ") << kmul_thresh);
         while (1) {
            Sleep(0);
#ifndef DBG_PRINT
            tout_puts(".");
#endif
            uint64 diff = hrtime.get();
            ticks = cyc.get_cycles();
            for (uint64 k = rounds; k > 0; --k) b.benchmark();
            ticks = cyc.get_cycles() - ticks;
#ifdef DBG_PRINT
            //tout_puts(format("ticks=%? ") << ticks);
#endif
            diff = hrtime.get() - diff;
            if (!is_null1) ticks -= null1_ticks;
            if (!is_null) ticks -= null2_ticks * rounds / null2_rounds;
            const uint64 nsec = hrtime_to_nsec64(diff);
#ifdef DBG_PRINT
            //tout_puts(format("nsec=%? ") << nsec);
#endif
            if (stat.has_data() || is_null || (ticks > min_cycles*7/10 && (ticks <= min_cycles*2 || end_set))) {  // 70% of the time is enough, but not more than 200%
               if (run > 0) {  // always ignore first run
#ifdef DBG_PRINT
                  tout_puts(format("ticks=%? nsec=%? ") << ticks << nsec);
#endif
                  total_nsec += nsec;
                  best_ticks = ticks;
                  stat.update(ticks);
               }
               if (++run >= runs) break;
            }
            else if (pass > 10 && !stat.has_data()) {
               c = (double)ticks / rounds / b.big_o(sz);
               break;  // no dice!
            }
            if (!is_null1) {
               if (min_cycles >= 10 * ticks) rounds *= 10;
               else rounds = tmax<uint64>(tmin((rounds * min_cycles + ticks - 1) / ticks, min_cycles), 1);
            }
            ++pass;
         }
#ifndef DBG_PRINT
         for (unsigned i = 0; i<pass; ++i) tout_puts("\b \b");
         tout_puts("\b");
#endif
         if (is_null1) {
            null1_ticks = stat.get_avg();
            ss("null1_ticks = %? avg, dev %#?") << null1_ticks << stat.get_dev();
         }
         else if (is_null2) {
            null2_ticks = stat.get_avg();
            null2_rounds = rounds;
            ss("null2_ticks = %#,.2? avg, dev %#,.3?\n") << 100 * null2_ticks / null2_rounds << 1000 * (uint64)(stat.get_dev() / null2_rounds);
         }
         else if (!stat.has_data()) {
            s = "     --";
            loop = false;
         }
         else  {
            ss(" %6?") << scaled_count(rounds, SCT_BIGGEST_UNIT|SCT_NO_SPACE|SCT_MAX3|SCT_DEC);
            ss(" %,8.4?") << total_nsec / 100000;
            const uint64 cycles = stat.get_avg() * 100 / rounds / sz;
            ss(" %,10.2?") << cycles;
            double o = (double)rounds * b.big_o(sz);
            c = stat.get_avg() / o;
            ss(" %,8.2?") << (uint64)(c * 100);
            ss(" %,8.3?") << (uint64)(stat.get_dev() * 1000 / o);
            ss(" %7?") << scaled_count(1000000000.0 * sz * sizeof(small) * rounds * (run-1) / total_nsec, SCT_BIGGEST_UNIT|SCT_NO_SPACE|SCT_MAX3);
            ss(" %9?") << sz;
            ss(" %12?") << (uint64)((stat.get_avg() + rounds/2) / rounds);
         }
         s += "\n";
         tout_puts(s);
         s.clear();

         b.destroy();
      }
   }
}

void _CL_CALL bignum_benchmark(bench_param& bp) {
   benchmark<limb>(bp);
}

void _CL_CALL bignum_benchmark_half(bench_param& bp) {
   benchmark<halflimb>(bp);
}


// ------ CALIBRATION ---------------------------------------------------------

template<typename small>
static void xmul_calibration(const unsigned fullness, const int what, bool square) {
   set_prio_class prio_cls(REALTIME_PRIORITY_CLASS);
   set_thread_prio th_prio(THREAD_PRIORITY_TIME_CRITICAL);
   tout_puts(system_info() + "\n");

   index& xmul_thresh = what == 2 ? (square ? ksqu_thresh : kmul_thresh) : (square ? t3squ_thresh : t3mul_thresh);
   void (*xmul)(small* r, const small* a, index alen, const small* b, index blen) = what == 2 ? kmul<small> : t3mul<small>;
   void (*xsqu)(small* r, const small* a, index alen) = what == 2 ? ksqu<small> : t3squ<small>;
   const string xmul_name = string(what == 2 ? "k" : "t") + (square ? "squ" : "mul");

   index start_i, step_i, max_i;
   double fact_i = 1.0;
   double pw;
   const index sz_krange[] = { 100, 109, 119, 129, 141, 154, 168, 183, 0 };
   const index sz_trange[] = { 2000, 2294, 2632, 3019, 3464, 3974, 4559, 5230, 0 };
   //const index sz_long[] = { 10000, 13000, 16000, 19000, 22000, 25000, 28000, 0 };
   //const index sz_superlong[] = { 100000, 130000, 160000, 190000, 220000, 250000, 280000, 0 };
   const index* sz;
   unsigned uncert;
   unsigned rfact = fullness >= 3 ? 3 : fullness >= 2 ? 2 : 1;

   if (what == 2) {  // kmul
#ifdef _M_X64
      start_i = 12;
      step_i = 4;
      max_i = square ? 160 : 50;
      fact_i = 1.08;
#else
      start_i = 8;
      step_i = 2;
      max_i = 56;
#endif
      pw = log2_3;
      sz = sz_krange;
      // higher threshold values are better for kmul, since omul needs no extra memory at all!
      uncert = 995;
#ifndef NDEBUG
      max_i *= 2;
      step_i = 2;
#endif
   }
   else {  // t3mul
#ifdef _M_X64
      start_i = 16;
      max_i = 2000;
      step_i = 8;
      fact_i = 1.08;
#else
      start_i = 64;
      max_i = 500;
      step_i = fullness >= 3 ? 8 : fullness >= 2 ? 16 : 32;
#endif
      pw = log3_5;
      sz = sz_trange;
      // lower threshold values are better for t3mul, since it uses less memory
      uncert = 1002;
#ifndef NDEBUG
      max_i *= 8;
      step_i = 64;
#endif
   }

   map<index, uint64> twtimes;

   unsigned j = 0;
   for (; sz[j]; ++j) {
      const index l = sz[j];
      const uint64 rounds = tmax<uint64>(rfact * 1000000.0 CLDEBUG(/ 5) / pow(l, pw), 1);
      small* p = rand_alloc<small>(l);
      small* q = rand_alloc<small>(l);
      small* r = bignum_alloc<small>(2*l);

      // measure
      map<index, uint64> times;
      uint64 tmn = _UI64_MAX;
      unsigned dots = 0;
      for (index i=start_i; i<max_i; ) {
         xmul_thresh = i;
         uint64 mn = 0;
         tout_puts(".");
         ++dots;
         const unsigned max_k = 5;
         unsigned k = 0;
         for (; k<max_k+1; ++k) {
            const uint64 start = cyc.get_cycles();
            if (square) {
               for (uint64 k=0; k<rounds; ++k)
                  xsqu(r, p, l);
            }
            else {
               for (uint64 k=0; k<rounds; ++k)
                  xmul(r, p, l, q, l);
            }
            const uint64 diff = cyc.get_cycles() - start;
            if (k > 0) mn += diff;
         }
         mn /= (max_k * rounds);
         tmn = tmin(tmn, mn);
         times[i] = mn;
         i = (tmax<index>(ceil(i * fact_i), i+1) + step_i - 1) / step_i * step_i;
      }
      while (dots-- > 0) tout_puts("\b \b");

      // print
      tout_puts(" words thresh   rounds      cyc/w       %\n");
      for (map<index, uint64>::const_iterator i = times.begin(); i != times.end(); ++i) {
         const index thresh = i->first;
         const uint64 ticks = i->second;
         tout_puts(format("%6?  %5?   %6? %,10.2? %,10.2?\n")
            << l << thresh << scaled_count(rounds, SCT_BIGGEST_UNIT|SCT_NO_SPACE|SCT_MAX3|SCT_DEC) << 100 * ticks / l << 10000ui64 * ticks / tmn);
         twtimes[thresh] += 10000ui64 * ticks / tmn;
      }
      tout_puts("\n");

      bignum_free(p);
      bignum_free(q);
      bignum_free(r);
   }

   // print totals
   index min_thresh = 0;
   uint64 min_val = _UI64_MAX;
   tout_puts("------ thresh ------------------------- % --\n");
   for (map<index, uint64>::const_iterator i = twtimes.begin(); i != twtimes.end(); ++i) {
      const index thresh = i->first;
      const uint64 perc = i->second / j;
      tout_puts(format("        %5?                     %,10.2?\n")
         << thresh << perc);
      if (perc * uncert / 1000 <= min_val) {
         if (perc < min_val) min_val = perc;
         min_thresh = thresh;
      }
   }
   tout_puts(format("\nbest %?_thresh = %?\n") << xmul_name << min_thresh);
   xmul_thresh = min_thresh;
}

void _CL_CALL xmul_calibrate(unsigned fullness, int what, bool square) {
   xmul_calibration<limb>(fullness, what, square);
}


// ------ SCH�NHAGE-STRASSEN smul_mod() CALIBRATION ---------------------------

template<typename small>
static void smul_mod_calibration(const unsigned fullness, const bool square) {
   set_prio_class prio(REALTIME_PRIORITY_CLASS);
   time_source_check();

   map<index, uint64> twtimes;

   const index max_sz = 64 / bits(small) * (fullness >= 3 ? 200000 : fullness >= 2 ? 20000: 6000);  // -SM is just over optimise code for M47
   bool print_head = true;
   const unsigned min_lg = 3;
   unsigned start_lg = min_lg;
   const unsigned stepsz = 1 << start_lg;
   index sz = (200 + stepsz - 1) & ~(stepsz - 1);
   unsigned lg_range = 8;
   bool test_t3mul = true;
   unsigned line = 0;
   double t3_fuzzy = 0.995;
   vector<index> endsz_lg(bits(index), numeric_limits<index>::max());
   index endsz_t3 = numeric_limits<index>::max();
   vector<byte> fake_value(bits(index));

   smul_calib_data& cdata = square ? ssqu_mod_lg : smul_mod_lg;
   cdata.init(stepsz);  // clear all

   while (sz < max_sz) {
      const uint64 rounds = tmax<uint64>(200000.0 CLDEBUG(/5) / CL_IF_X64(1, 2) / sz / log2(sz), 1);
      const unsigned runs = fullness >= 2 ? bound<unsigned>(100000 / sz, 2, 5) : 2;
      auto tz = trail_zero_cnt(sz);

      // measure
      vector<uint64> times(bits(small));
      uint64 tmn = _UI64_MAX;
      unsigned lgmn = 0;
      uint64 t3time = _UI64_MAX;

      for (unsigned pass=0; pass<runs; ++pass) {
         small* p = rand_alloc<small>(sz);
         small* q = rand_alloc<small>(sz);
         small* r = bignum_alloc<small>(2*sz);

         if (test_t3mul) {
            const uint64 start = cyc.get_cycles();
            if (square) {
               for (uint64 rnd=0; rnd<rounds; ++rnd)
                  t3squ_mod(r, p, sz, (sz-1)*bits(small));
            }
            else {
               for (uint64 rnd=0; rnd<rounds; ++rnd)
                  t3mul_mod(r, p, q, sz, (sz-1)*bits(small));
            }
            const uint64 diff = cyc.get_cycles() - start;
            t3time = tmin(t3time, diff / rounds);
         }

         uint64 last_ops = _UI64_MAX/2;
         for (unsigned lg=start_lg; lg<tmin(tz+1, start_lg + lg_range); ++lg) {
            // check if power of 2 can be used to cut number into coeffs
            classert((((index)1 << lg) - 1 & sz) == 0);  // this power of 2 is in sz
            index mm = 0;
            index l = 0;
            index k = 0;
            smul_mod_set_params2<small>(sz*bits(small), lg, mm, l, k);
            // tame super-slow high powers of 2
            const uint64 ops = (k / bits(small) + 1) * mm;
            if (sz/mm <= 4 || ops > 2*last_ops) break;
            last_ops = ops;

            uint64 time = pass == 0 ? _UI64_MAX : times[lg];
            const uint64 start = cyc.get_cycles();
            if (square) {
               for (uint64 rnd=0; rnd<rounds; ++rnd)
                  ssqu2(r, sz, p, sz, lg, mm, l, k, true);
            }
            else {
               for (uint64 rnd=0; rnd<rounds; ++rnd)
                  smul2(r, sz, p, sz, q, sz, lg, mm, l, k, true);
            }
            const uint64 diff = cyc.get_cycles() - start;
            time = tmin(time, diff / rounds);
            times[lg] = time;
            if (time < tmn) {
               tmn = time;
               lgmn = lg;
            }
         }

         bignum_free(p);
         bignum_free(q);
         bignum_free(r);
      }

      // print results
      if (print_head || line > 40) {
         tout_puts("\n               ");
         if (test_t3mul) tout_puts("  t3mul");
         for (unsigned lg=start_lg; lg<start_lg + lg_range; ++lg) tout_puts(format("%7?") << lg);
         tout_puts("\n");
         line = 2;
         print_head = false;
      }

      const string rstr = format("[%?]") << scaled_count(rounds*runs, SCT_MAX4|SCT_NO_SPACE);
      tout_puts(format("%-6? %7?:") << rstr << sz);
      bool t3_faster = false;
      if (test_t3mul) {
         t3_faster = t3time * t3_fuzzy <= times[lgmn];
         const string s = format("%?%?") << (t3_faster ? "*" : "") << (t3time + sz/2) / sz;
         tout_puts(format("%7?") << s);
      }
      for (unsigned lg=start_lg; lg<start_lg + lg_range; ++lg) {
         const uint64 nsec = times[lg];
         if (nsec == 0) break;
         const string s = (lg == lgmn ? !t3_faster ? "*" : "+" : "") + (string)(format("%?") << (nsec + sz/2) / sz);
         tout_puts(format("%7?") << s);
      }
      tout_puts("\n");
      ++line;
      auto idx = t3_faster ? 0 : lgmn;
      cdata.set_lg(sz, idx);

      // output tailoring
      if (test_t3mul) {
         if (tz > start_lg && t3_faster)  // t3mul was faster and there were higher powers of 2 available
            endsz_t3 = 2*sz;  // two times sz when t3mul was fastest the last time
         if (sz > endsz_t3) {
            test_t3mul = false;
            print_head = true;
         }
      }
      if (tz > start_lg && lgmn < tz) {
         endsz_lg[lgmn] = 2*sz;
         for (index j=lgmn; j>0; --j)
            endsz_lg[j-1] = tmin(2*sz, endsz_lg[j-1]);  // if lower logs are yet unset, set them
      }
      while (sz > endsz_lg[start_lg]) {
         // search last entry that had exactly the number of trailing zeros as the column we're about to switch off
         index j = sz - 1;
         while (trail_zero_cnt(j) != start_lg)
            --j;
         fake_value[start_lg] = cdata.get_lg(j);
         ++start_lg;
         print_head = true;
      }

      auto const a2 = (index)1 << min_lg;
      auto j = sz + a2;
      const index a = (index)1 << start_lg;
      sz = (sz + a) & ~(a - 1);
      while (j < sz) {
         cdata.set_lg(j, fake_value[trail_zero_cnt(j)]);
         j += a2;
      }
   }

   const char* name = square ? "_ssqu_mod_lg" : "_smul_mod_lg";
   //tout_puts("\n" + cdata.print(name));
   tout_puts("\n" + cdata.pack_table(name));
   //tout_puts("\n" + cdata.print_packed(name));
   //cdata.size_to_lg.clear();
}

void _CL_CALL qsmul_mod_calibrate(unsigned fullness, const bool square) {
   smul_mod_calibration<limb>(fullness, square);
}


// ------ SCH�NHAGE-STRASSEN smul() CALIBRATION -------------------------------

template<typename small>
void warmup() {
   const unsigned run_msecs = 2000;
   index sz = 32;

   tout_puts("warmup... ");
   uint64 rounds = tmax<uint64>(200000.0 CLDEBUG(/5) / CL_IF_X64(1, 2) / sz / log2(sz), 1);
   small* p = rand_alloc<small>(sz);
   small* q = rand_alloc<small>(sz);
   small* r = bignum_alloc<small>(2*sz);

   uint64 all_start = hrtime.get();
   while (!break_signalled) {
      //tout_puts(format("[%?] ") << rounds);
      uint64 start = hrtime.get();
      for (uint64 rnd=0; rnd<rounds; ++rnd)
         t3mul(r, p, sz, q, sz);
      uint64 stop = hrtime.get();
      if (hrtime.to_msec(stop - all_start) > run_msecs) break;
      if (hrtime.to_msec(stop - start) < run_msecs / 8) rounds *= 2;
   }

   bignum_free(p);
   bignum_free(q);
   bignum_free(r);
   tout_puts("done.\n");
}

static BOOL WINAPI ctrl_break_smul_calib(DWORD ctrl_type) {
   switch (ctrl_type) {
   case CTRL_C_EVENT:
      break_signalled = true;
      return TRUE;
   }
   return FALSE;
}

template<typename small>
static void smul_calibration(const unsigned fullness, const bool square, const bench_param& bp) {
   set_prio_class prio(bp.realtime ? REALTIME_PRIORITY_CLASS : HIGH_PRIORITY_CLASS);
   time_source_check();

   index& xmul_thresh = square ? ssqu_thresh : smul_thresh;
   const char* xmul_name = square ? "ssqu" : "smul";

   file_trace ffout;
   tracer sout;
   file_trace fsout;
   tracer lout;
   file_trace flout;
   if (bp.list_only) {
      ffout.set_time(false, false);
      ffout.set_name(str2wstr(xmul_name) + L"-output.txt");
      //uremove(ffout.get_name());
      tout.add(&ffout);
      ffout.puts("\n" + system_info() + "\n");

      fsout.set_time(false, false);
      fsout.set_name(str2wstr(xmul_name) + L"-speed.txt");
      //uremove(fsout.get_name());
      sout.add(&fsout);
      sout.puts(format("\n%% %?\n") << start_datetime);

      flout.set_time(false, false);
      flout.set_name(str2wstr(xmul_name) + L"-fft.txt");
      //uremove(flout.get_name());
      lout.add(&flout);
      lout.puts(format("\n%% %?\n") << start_datetime);
   }

   map<index, uint64> twtimes;

   index max_sz = 0;
   double fact = 1.0;
   switch (fullness) {
   case 0:
   case 1:
      max_sz = 100*1000;                      // 0.1m words
      fact = 1.1;
      break;
   case 2:
      fact = 1.01;
      max_sz = 1000*1000;                     // 1m words
      break;
   case 3:
      max_sz = 200*1000*1000 / bits(small);   // 3.125m 64-bit words
      break;
   case 4:
      max_sz = 1000*1000*1000 / bits(small);  // 15.625m 64-bit words
      break;
   case 5:
   default:
      max_sz = numeric_limits<index>::max() / sizeof(small);
      break;
   }
   max_sz = t2max(max_sz, bp.end_sz);
   const index use_max_mem = physical_mem(true) / 8 * 7;
   max_sz = t2min(max_sz, use_max_mem / sizeof(small));

   index sz = t2max(32, bp.start_sz);
   bool print_head = true;
   unsigned start_lg = t2max(2, bp.fft_len);
   unsigned lg_range = 8;
   bool test_t3mul = sz < 2*(square ? t3squ_thresh : t3mul_thresh);
   index thresh = 0;
   map<unsigned, index> last_use;
   unsigned highest_lg = 0;
   const double t3_uncert = 0.995;
   vector<uint64> old_time(bits(small));
   vector<uint64> old_ring(bits(small));
   unsigned line = 0;
   uint64 old_out_time = 0;
   unsigned old_out_lgmn = 0;
   size_t old_sout_sz = 0;
   size_t old_lout_sz = 0;

   tout_puts(format("Benchmarking up to %'? words (%'? bits, %?B)") 
      << max_sz << max_sz * bits(small) << scaled_count(max_sz * sizeof(small), SCT_MAX4|SCT_BIGGEST_UNIT));
   if (fact != 1.0) tout_puts(format(" in factor %#.6f steps") << fact);
   tout_puts("\n");
   SetConsoleCtrlHandler(ctrl_break_smul_calib, TRUE);
   warmup<small>();

   while (true) {
      const uint64 rounds = tmax<uint64>(200000.0 CLDEBUG(/5) / CL_IF_X64(1, 2) / sz / log2(sz), 1);
      const unsigned runs = fullness >= 2 ? bound<unsigned>(500 / int_sqrt(sz), 1, 5) : 2;

      // measure
      vector<uint64> times(bits(small));
      vector<bool> did_calc(bits(small));
      uint64 tmn = _UI64_MAX;
      unsigned lgmn = 0;
      uint64 t3time = _UI64_MAX;
      index min_ring = numeric_limits<index>::max();
      bool do_ring = true;
      bool param_ok = false;

      for (unsigned pass=0; pass<runs; ++pass) {
         small* p = rand_alloc<small>(sz);
         small* q = rand_alloc<small>(sz);
         small* r = bignum_alloc<small>(2*sz);

         if (test_t3mul) {
            const uint64 start = cyc.get_cycles();
            if (square) {
               for (uint64 rnd=0; rnd<rounds; ++rnd)
                  t3squ(r, p, sz);
            }
            else {
               for (uint64 rnd=0; rnd<rounds; ++rnd)
                  t3mul(r, p, sz, q, sz);
            }
            const uint64 diff = cyc.get_cycles() - start;
            t3time = tmin(t3time, diff / rounds);
         }

         uint64 last_ops = _UI64_MAX/2;
         for (unsigned lg=start_lg; lg<start_lg + lg_range; ++lg) {
            index mm = 0;
            index l = 0;
            index k = 0;
            if (!smul_set_params2<small>(2*sz, lg, mm, l, k)) continue;
            const index kk = k / bits(small) + 1;  // smalls per lower element
            auto mem = smul_mem2<small>(sz, mm, kk, square, false);
            if (mem >= use_max_mem) break;

            const uint64 ops = kk * mm;
            if (sz/mm <= 4 || ops > 2*last_ops) break;
            last_ops = ops;
            const index ring = mm * l;
            caassert(ring >= sz);
            min_ring = tmin(min_ring, ring);
            param_ok = true;

            uint64 time = pass == 0 ? _UI64_MAX : old_time[lg];
            if (pass == 0 && ring == old_ring[lg]) do_ring = false;
            if (do_ring) {
               Sleep(0);
               const uint64 start = cyc.get_cycles();
               if (square) {
                  for (uint64 rnd=0; rnd<rounds; ++rnd)
                     ssqu2(r, 2*sz, p, sz, lg, mm, l, k, false);
               }
               else {
                  for (uint64 rnd=0; rnd<rounds; ++rnd)
                     smul2(r, 2*sz, p, sz, q, sz, lg, mm, l, k, false);
               }
               const uint64 diff = cyc.get_cycles() - start;
               time = tmin(time, diff / rounds);
               old_ring[lg] = ring;
               old_time[lg] = time;
               did_calc[lg] = true;
            }
            else {
               time = old_time[lg];
               did_calc[lg] = false;
            }
            times[lg] = time;
            if (time < tmn) {
               tmn = time;
               lgmn = lg;
            }
         }

         bignum_free(p);
         bignum_free(q);
         bignum_free(r);

         if (!do_ring) break;
      }
      min_ring = (min_ring + 1) / 2;

      // print results
      if (bp.list_only) {
         //const uint64 t = test_t3mul && t3time < times[lgmn] ? t3time : times[lgmn];
         const uint64 t = times[lgmn];
         if (t != old_out_time) {
            if (old_out_time != 0) sout.puts(format("%? %?\n") << sz-1 << old_out_time);
            sout.puts(format("%? %?\n") << sz << t);
            old_out_time = t;
            old_sout_sz = sz;
         }
         if (lgmn != old_out_lgmn) {
            if (old_out_lgmn != 0) lout.puts(format("%? %?\n") << sz-1 << old_out_lgmn);
            lout.puts(format("%? %?\n") << sz << lgmn);
            old_out_lgmn = lgmn;
            old_lout_sz = sz;
         }
      }

      if (print_head || line > 40) {
         tout_puts("\n               ");
         if (test_t3mul) tout_puts(" t3mul  ");
         for (unsigned lg=start_lg; lg<start_lg + lg_range; ++lg) tout_puts(format("%7?") << lg);
         tout_puts("\n");
         line = 2;
         print_head = false;
      }

      const string rstr = format("[%?]") << scaled_count(rounds*runs, SCT_MAX4|SCT_NO_SPACE);
      tout_puts(format("%-6? %7?:") << rstr << sz);
      if (test_t3mul) {
         const string s = format("%?%?") << (t3time < times[lgmn] ? "*" : "") << (t3time + sz/2) / sz;
         tout_puts(format("%6?  ") << s);
      }
      for (unsigned lg=start_lg; lg<start_lg + lg_range; ++lg) {
         const uint64 nsec = times[lg];
         if (nsec == 0) break;
         const string s = (lg == lgmn ? "*" : did_calc[lg] ? "." : "") + (string)(format("%?") << (nsec + sz/2) / sz);
         tout_puts(format("%7?") << s);
      }
      tout_puts("\n");
      ++line;

      last_use[lgmn] = min_ring;
      highest_lg = tmax(highest_lg, lgmn);

      if (sz >= max_sz || !param_ok || break_signalled) {
         if (bp.list_only) {
            if (sz != old_sout_sz) {
               const uint64 t = times[lgmn];
               sout.puts(format("%? %?\n\n") << sz << t);
            }
            if (sz != old_lout_sz) 
               lout.puts(format("%? %?\n\n") << sz << lgmn);
         }
         break;
      }

      if (lgmn > start_lg + 3) {
         print_head = true;
         start_lg = lgmn - 3;
      }
      if (test_t3mul) {
         if (t3time * t3_uncert <= tmn) thresh = sz;
         if (t3time > tmn*3/2) {
            test_t3mul = false;
            print_head = true;
            ++lg_range;
         }
      }

      sz = tmax<index>(min_ring + 1, sz + 1, ceil(sz * fact));
   }
   SetConsoleCtrlHandler(NULL, FALSE);

   xmul_thresh = thresh;
   tout_puts(format("\nbest %?_thresh = %?\n") << xmul_name << thresh);
   unsigned i = 0;
   while (i < highest_lg) {
      if (last_use[i] > 0) break;
      ++i;
   }
   while (i+1 < highest_lg) {
      index sz = last_use[i];
      if (sz == 0) break;
      tout_puts(format("   2*%?,  // %?\n") << sz << i);
      ++i;
   }

   if (bp.list_only) tout.remove(&ffout);
}

void _CL_CALL qsmul_calibrate(unsigned fullness, const bool square, const bench_param& bp) {
   smul_calibration<limb>(fullness, square, bp);
}

template<typename T>
unsigned zero_bit_count(T k) {
   unsigned cnt = 0;
   while (k > 0) {
      if ((k & 1) != 0) break;
      ++cnt;
      k >>= 1;
   }
   return cnt;
}

template<typename small>
unsigned smul_optimise(const index sz, const bool square, const bool print, index* kret, uint64* iter_time) {
   uint64 tmn = _UI64_MAX;
   unsigned lgmn = 0;
   uint64 last_ops = _UI64_MAX;
   const uint64 rounds = est_mul_rounds(sz, 300);
   index kmn = 0;
   const double fuzz = 0.95;
   vector<smul_params>& opt = qsmul_opt(square);

   unsigned lg_guess;
   index mm;
   index l;
   index k;
   smul_set_params<small>(square, sz, sz, lg_guess, mm, l, k);

   if (print) tout_puts(format("optimising fft %? for length %?, %? rounds\n") << (square ? "square" : "mul") << sz << rounds);
   small* p = rand_alloc<small>(sz);
   small* q = rand_alloc<small>(sz);
   small* r = bignum_alloc<small>(2*sz);

   set_prio_class prio(ABOVE_NORMAL_PRIORITY_CLASS);
   for (unsigned lg=lg_guess-2; lg<bits(small); ++lg) {
      if (!smul_set_params2<small>(2*sz, lg, mm, l, k)) continue;
      const index k_org = k;
      uint64 best_ops = _UI64_MAX;
      uint64 min_k = _UI64_MAX;
      for (unsigned tries=0; tries<5; ++tries) {
         const uint64 ops = k / bits(small) * mm;
         if (sz/mm <= 4) continue;
         if (ops > 2*last_ops) break;
         best_ops = tmin(best_ops, ops);

         const uint64 start = cyc.get_cycles();
         if (square) {
            for (uint64 pass=0; pass<rounds; ++pass)
               ssqu2(r, 2*sz, p, sz, lg, mm, l, k, false);
         }
         else {
            for (uint64 pass=0; pass<rounds; ++pass)
               smul2(r, 2*sz, p, sz, q, sz, lg, mm, l, k, false);
         }
         const uint64 diff = cyc.get_cycles() - start;
         const uint64 time = diff / rounds;
         const index bitlen = sz * bits(small);
         if (print) tout_puts(format("fft size %? (2^%?), K=%? words (%'? bits), cyc/bit=%,.2?\n") << mm << lg << k/bits(small) << k << 100 * (time + bitlen/2) / bitlen);
         if (time < tmn) {
            tmn = time;
            lgmn = lg;
            kmn = k;
         }
         if (time * fuzz > min_k) break;
         min_k = tmin(time, min_k);

         // try to get a k which contains a higher power of 2
         const index a = (index)1 << zero_bit_count(k);
         const index k1 = k + a;
         classert(k1 > k);
         classert(zero_bit_count(k1) > zero_bit_count(k));
         if (k1 > k_org*3/2) break;  // too big
         k = k1;
      }
      if (best_ops < _UI64_MAX) {
         last_ops = best_ops;
         if (print) tout_puts("\n");
      }
      if (min_k * fuzz > tmn) break;
   }
   prio.reset();

   if (print) tout_puts(format("selected fft size %? (2^%?), K=%? words (%'? bits), iteration time %,.03? msec\n")
      << (1ui64 << lgmn) << lgmn << kmn/bits(small) << kmn << tmn*1000*1000/clock_rate);
   opt.clear();
   opt.push_back(smul_params(2*sz, lgmn, kmn));

   bignum_free(p);
   bignum_free(q);
   bignum_free(r);

   if (kret != 0) *kret = kmn;
   if (iter_time != 0) *iter_time = tmn*1000000000.0/clock_rate;
   return lgmn;
}


// ------ ALIGNMENT TEST ------------------------------------------------------

template<typename small>
void talign_test() {
   time_source_check();

   const uint64 min_cycles = 0.5 * 4000000000ui64;  // 0.5 sec on a 4GHz machine
   const index l = kmul_thresh;
   const index max = 64 / sizeof(small);

   tape_alloc<small> tmp(4*l+2*max);
   small* p = (small*)((uint_ptr)(tmp.p + max - 1) & ~63);
   small* q = p + l;
   small* r = q + l;

   uint64 rounds = tmax<uint64>(1000 CLDEBUG(/ 5) / l*l, 1);
   unsigned pass = 1;
   uint64 diff0;
   while (1) {
      tout_puts(".");
      const uint64 start = cyc.get_cycles();
      for (uint64 k=0; k<rounds; ++k)
         omul_ls(r, p, l, q, l);
      const uint64 diff = cyc.get_cycles() - start;
      diff0 = diff;
      if (diff > min_cycles) break;
      rounds = tmin(rounds * 4/3 * min_cycles / diff, clock_rate * min_cycles / 1000000000);
      ++pass;
   }
   for (unsigned j=0; j<pass; ++j) tout_puts("\b \b");
   tout_puts(format("%? rounds:\n") << rounds);

   tout_puts("word off     perc\n");
   for (index i=0; i<max; ++i) {
      tout_puts(format("     %3? ") << i);
      const uint64 start = cyc.get_cycles();
      for (uint64 k=0; k<rounds; ++k)
         omul_ls(r+i, p+i, l, q+i, l);
      const uint64 diff = cyc.get_cycles() - start;
      tout_puts(format(" %,6.2?%%\n") << 10000ui64 * diff / diff0);
   }
}

void _CL_CALL align_test() {
   talign_test<limb>();
}


#ifdef CL_TEST_CODE
void _CL_CALL test_bignum_benchmark() {
   test_calc_dkss_sizes();
   test_calc_qmul_sizes();
}
#endif


//#if _MSC_VER < 1300
#pragma inline_depth(0)
template<typename small>
void pull_benchmark_into_existance() {
   smul_optimise<small>(0, false, false);
}
void _pull_bignum_into_existance() {
   classertm(0, "must not be called");
   pull_benchmark_into_existance<uint8>();
   pull_benchmark_into_existance<uint16>();
   pull_benchmark_into_existance<uint32>();
#ifdef _M_X64
   pull_benchmark_into_existance<uint64>();
#endif
}
#pragma inline_depth()
//#endif

}  // namespace bignum
