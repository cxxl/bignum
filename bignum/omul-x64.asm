; $+HEADER$
; 
; Copyright 2015 Christoph Lueders
; 
; This file is part of the BIGNUM Library.
; 
; The BIGNUM Library is free software: you can redistribute it and/or modify
; it under the terms of the GNU Lesser General Public License as published by
; the Free Software Foundation, either version 3 of the License, or (at your 
; option) any later version.
; 
; The BIGNUM Library is distributed in the hope that it will be useful, but 
; WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
; or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public 
; License for more details.
; 
; You should have received a copy of the GNU Lesser General Public License
; along with the BIGNUM Library.  If not, see <http://www.gnu.org/licenses/>.
; 
; $-HEADER$

; $Date: 2015-04-24 11:28:16 +0200 (Fr, 24 Apr 2015) $
; $Rev: 26590 $


EXTRN __ImageBase:BYTE

_TEXT SEGMENT

; ------ MULTIPLICATION -------------------------------------------------------

   align 16

?omul_asmx64@bignum@@YAXPEA_KPEB_K_K12@Z PROC   ; cl::omul_asmx64<unsigned __int64>, COMDAT

; 150  : void omul_asmx64(small* r, const small* a, index alen, const small* b, index blen) {

; parameters:
; rcx = r
; rdx = a
; r8 = alen
; r9 = b
; [rsp+56] = blen (sp�ter in r13)

; rbx, rsi, rdi, rbp, r12 - r15 must be preserved by callee
; may be destroyed: rax, rcx, rdx, r8, r9, r10, r11
; http://msdn.microsoft.com/en-us/magazine/cc300794.aspx

   mov   QWORD PTR [rsp+8], rbx
   mov   QWORD PTR [rsp+16], r13
   mov   QWORD PTR [rsp+24], rsi
   mov   QWORD PTR [rsp+32], rdi
   push  r12
   push  r14

; register allocation:
; --------------------
;   rax = scratch
;   rbx = a
;   rcx = r
;   rdx = scratch
;   rsi = scratch
;   rdi = bb
;   r8 = alen
;   r9 = b
;   r10 = i
;   r11 = of
;   r12 = j
;   r13 = blen
;   r14 = rm

; not used: rbp, r15


; 157  :    // unrolled inner loop on first element:
; 158  :    // init the first 'alen' elements in result with product without adding up old contents.
; 159  :    small of = 0;
; 160  :    small bb = b[0];

                                                ; r9 = b
   mov   rdi, QWORD PTR [r9]                    ; rdi = bb = b[0]
   xor   r11d, r11d                             ; r11 = of
   mov   r13, QWORD PTR [rsp+56]                ; r13 = blen

; 161  :    index i = alen % 8;

   mov   r10, r8                                ; r8 = alen
   mov   rbx, rdx                               ; rdx = a
   and   r10, 7

; 162  :    switch (i) {

   lea   rax, offset mul_init_loop_jump_table
   add   rax, QWORD PTR [rax+r10*8]
   jmp   rax


   align 16
mul_init_loop:

; 163  :       case 7: goto l7;
; 164  :       case 6: goto l6;
; 165  :       case 5: goto l5;
; 166  :       case 4: goto l4;
; 167  :       case 3: goto l3;
; 168  :       case 2: goto l2;
; 169  :       case 1: goto l1;
; 170  :    }
; 171  :
; 172  : #define s(o) of = ops<small>::muladdc(r[i+(o)], a[i+(o)], bb, 0, of)
; 173  :    while (i < alen) {

; 175  :       i += 8;

   add   r10, 8                                 ; r10 = i

; 176  :       s(-8);

   mov   rax, QWORD PTR [rbx+r10*8-64]          ; rax = bb = b[0]
                                                ; rbx = a
   mul   rdi                                    ; rdx:rax = rax * mem = b[0] * a[i]
   add   rax, r11                               ; r11 = of
   mov   QWORD PTR [rcx+r10*8-64], rax          ; rcx = r
   adc   rdx, 0
   mov   r11, rdx                               ; r11 = of

mul_init_loop7:
; 177  : l7:   s(-7);
   mov   rax, QWORD PTR [rbx+r10*8-56]
   mul   rdi
   add   rax, r11
   mov   QWORD PTR [rcx+r10*8-56], rax
   adc   rdx, 0
   mov   r11, rdx

mul_init_loop6:
; 178  : l6:   s(-6);
   mov   rax, QWORD PTR [rbx+r10*8-48]
   mul   rdi
   add   rax, r11
   mov   QWORD PTR [rcx+r10*8-48], rax
   adc   rdx, 0
   mov   r11, rdx

mul_init_loop5:
; 179  : l5:   s(-5);
   mov   rax, QWORD PTR [rbx+r10*8-40]
   mul   rdi
   add   rax, r11
   mov   QWORD PTR [rcx+r10*8-40], rax
   adc   rdx, 0
   mov   r11, rdx

mul_init_loop4:
; 180  : l4:   s(-4);
   mov   rax, QWORD PTR [rbx+r10*8-32]
   mul   rdi
   add   rax, r11
   mov   QWORD PTR [rcx+r10*8-32], rax
   adc   rdx, 0
   mov   r11, rdx

mul_init_loop3:
; 181  : l3:   s(-3);
   mov   rax, QWORD PTR [rbx+r10*8-24]
   mul   rdi
   add   rax, r11
   mov   QWORD PTR [rcx+r10*8-24], rax
   adc   rdx, 0
   mov   r11, rdx

mul_init_loop2:
; 182  : l2:   s(-2);
   mov   rax, QWORD PTR [rbx+r10*8-16]
   mul   rdi
   add   rax, r11
   mov   QWORD PTR [rcx+r10*8-16], rax
   adc   rdx, 0
   mov   r11, rdx

mul_init_loop1:
; 183  : l1:   s(-1);
   mov   rax, QWORD PTR [rbx+r10*8-8]
   mul   rdi
   add   rax, r11
   mov   QWORD PTR [rcx+r10*8-8], rax
   adc   rdx, 0
   mov   r11, rdx

; 184  :    }

mul_init_loop_cmp:
   cmp   r10, r8                                ; r8 = alen
   jb    mul_init_loop


; 185  : #undef s
; 186  :
; 187  :    r[i] = of;  // save overflow so the next add in the inner loop works ok.
; 188  :
; 189  :    // now the main loop
; 190  :    for (index j=1; j<blen; ++j) {

   mov   QWORD PTR [rcx+r10*8], r11             ; r11 = of
   mov   r12, 1                                 ; r12 = j

   jmp mul_outer_loop_cmp


   align 16
mul_outer_loop:

; 191  :       small* rm = r + j;
; 192  :       bb = b[j];

   mov   rdi, QWORD PTR [r9+r12*8]              ; rdi = bb

; 193  :       of = 0;
; 194  :       i = alen % 8;

   mov   r10, r8                                ; r8 = alen, r10 = i
   xor   r11d, r11d                             ; r11 = of
   and   r10, 7
   lea   r14, QWORD PTR [rcx+r12*8]             ; r14 = rm

; 195  :       switch (i) {

   lea   rax, offset mul_inner_loop_jump_table
   add   rax, QWORD PTR [rax+r10*8]
   jmp   rax


   align 16
mul_inner_loop:

; 196  :       case 7: goto il7;
; 197  :       case 6: goto il6;
; 198  :       case 5: goto il5;
; 199  :       case 4: goto il4;
; 200  :       case 3: goto il3;
; 201  :       case 2: goto il2;
; 202  :       case 1: goto il1;
; 203  :       }
; 204  :
; 205  : #define s(o) of = ops<small>::muladdc(rm[i+(o)], a[i+(o)], bb, rm[i+(o)], of)
; 206  :       while (i < alen) {

; 207  :          caassert((alen - i) % 8 == 0);
; 208  :          i += 8;
; 209  :          s(-8);

   ;;IACA START_MARKER
   ;mov ebx, 111
   ;db 64h, 67h, 90h

   add   r10, 8                                  ; r10 = i

                                                 ; r14 = rm
   mov   rax, QWORD PTR [rbx+r10*8-64]           ; rdi = bb = b[j]
   mul   rdi                                     ; rdx:rax = b[j] * a[i]
   add   rax, r11
   mov   r11, rdx
   adc   r11, 0
   add   QWORD PTR [r14+r10*8-64], rax           ; rm[i]
   adc   r11, 0

mul_inner_loop7:
; 210  : il7:     s(-7);
   mov   rax, QWORD PTR [rbx+r10*8-56]
   mul   rdi
   add   rax, r11
   mov   r11, rdx
   adc   r11, 0
   add   QWORD PTR [r14+r10*8-56], rax
   adc   r11, 0

mul_inner_loop6:
; 211  : il6:     s(-6);
   mov   rax, QWORD PTR [rbx+r10*8-48]
   mul   rdi
   add   rax, r11
   mov   r11, rdx
   adc   r11, 0
   adc   rdx, 0
   add   QWORD PTR [r14+r10*8-48], rax
   adc   r11, 0

mul_inner_loop5:
; 212  : il5:     s(-5);
   mov   rax, QWORD PTR [rbx+r10*8-40]
   mul   rdi
   add   rax, r11
   mov   r11, rdx
   adc   r11, 0
   add   QWORD PTR [r14+r10*8-40], rax
   adc   r11, 0

mul_inner_loop4:
; 213  : il4:     s(-4);
   mov   rax, QWORD PTR [rbx+r10*8-32]
   mul   rdi
   add   rax, r11
   mov   r11, rdx
   adc   r11, 0
   add   QWORD PTR [r14+r10*8-32], rax
   adc   r11, 0

mul_inner_loop3:
; 214  : il3:     s(-3);
   mov   rax, QWORD PTR [rbx+r10*8-24]
   mul   rdi
   add   rax, r11
   mov   r11, rdx
   adc   r11, 0
   add   QWORD PTR [r14+r10*8-24], rax
   adc   r11, 0

mul_inner_loop2:
; 215  : il2:     s(-2);
   mov   rax, QWORD PTR [rbx+r10*8-16]
   mul   rdi
   add   rax, r11
   mov   r11, rdx
   adc   r11, 0
   add   QWORD PTR [r14+r10*8-16], rax
   adc   r11, 0

mul_inner_loop1:
; 216  : il1:     s(-1);
   mov   rax, QWORD PTR [rbx+r10*8-8]
   mul   rdi
   add   rax, r11
   mov   r11, rdx
   adc   r11, 0
   add   QWORD PTR [r14+r10*8-8], rax
   adc   r11, 0

; 217  :       }

mul_inner_loop_cmp:
   cmp   r10, r8
   jae   mul_inner_loop_done

   ;;IACA END_MARKER
   ;mov ebx, 222
   ;db 64h, 67h, 90h


; ------ the inner loop without the jump-ins

   align 16
mul_inner_loop_long:

   add   r10, 8                                  ; r10 = i

                                                 ; r14 = rm
   mov   rax, QWORD PTR [rbx+r10*8-64]           ; rdi = bb = b[j]
   mul   rdi                                     ; rdx:rax = b[j] * a[i]
   mov   rsi, QWORD PTR [r14+r10*8-64]           ; rsi = rm[i]
   add   rax, rsi
   adc   rdx, 0
   add   rax, r11
   mov   QWORD PTR [r14+r10*8-64], rax
   mov   rax, QWORD PTR [rbx+r10*8-56]
   adc   rdx, 0
   mov   r11, rdx

   mul   rdi
   mov   rsi, QWORD PTR [r14+r10*8-56]
   add   rax, rsi
   adc   rdx, 0
   add   rax, r11
   mov   QWORD PTR [r14+r10*8-56], rax
   mov   rax, QWORD PTR [rbx+r10*8-48]
   adc   rdx, 0
   mov   r11, rdx

   mul   rdi
   mov   rsi, QWORD PTR [r14+r10*8-48]
   add   rax, rsi
   adc   rdx, 0
   add   rax, r11
   mov   QWORD PTR [r14+r10*8-48], rax
   mov   rax, QWORD PTR [rbx+r10*8-40]
   adc   rdx, 0
   mov   r11, rdx

   mul   rdi
   mov   rsi, QWORD PTR [r14+r10*8-40]
   add   rax, rsi
   adc   rdx, 0
   add   rax, r11
   mov   QWORD PTR [r14+r10*8-40], rax
   mov   rax, QWORD PTR [rbx+r10*8-32]
   adc   rdx, 0
   mov   r11, rdx

   mul   rdi
   mov   rsi, QWORD PTR [r14+r10*8-32]
   add   rax, rsi
   adc   rdx, 0
   add   rax, r11
   mov   QWORD PTR [r14+r10*8-32], rax
   mov   rax, QWORD PTR [rbx+r10*8-24]
   adc   rdx, 0
   mov   r11, rdx

   mul   rdi
   mov   rsi, QWORD PTR [r14+r10*8-24]
   add   rax, rsi
   adc   rdx, 0
   add   rax, r11
   mov   QWORD PTR [r14+r10*8-24], rax
   mov   rax, QWORD PTR [rbx+r10*8-16]
   adc   rdx, 0
   mov   r11, rdx

   mul   rdi
   mov   rsi, QWORD PTR [r14+r10*8-16]
   add   rax, rsi
   adc   rdx, 0
   add   rax, r11
   mov   QWORD PTR [r14+r10*8-16], rax
   mov   rax, QWORD PTR [rbx+r10*8-8]
   adc   rdx, 0
   mov   r11, rdx

   mul   rdi
   mov   rsi, QWORD PTR [r14+r10*8-8]
   add   rax, rsi
   adc   rdx, 0
   add   rax, r11
   mov   QWORD PTR [r14+r10*8-8], rax
   adc   rdx, 0
   mov   r11, rdx

   cmp   r10, r8
   jb    mul_inner_loop_long


; ------ we're done

mul_inner_loop_done:
; 218  : #undef s
; 219  :
; 220  :       rm[i] = of;  // save overflow so the next add in the inner loop works ok.

   mov   QWORD PTR [r14+r10*8], r11
   inc   r12

mul_outer_loop_cmp:
   cmp   r12, r13
   jb    mul_outer_loop


mul_outer_loop_end:

; 221  :    }
; 222  : }

   pop   r14
   pop   r12
   mov   rbx, QWORD PTR [rsp+8]
   mov   r13, QWORD PTR [rsp+16]
   mov   rsi, QWORD PTR [rsp+24]
   mov   rdi, QWORD PTR [rsp+32]

   ret   0


   align 16
mul_init_loop_jump_table:
   DQ mul_init_loop_cmp - mul_init_loop_jump_table
   DQ mul_init_loop1 - mul_init_loop_jump_table
   DQ mul_init_loop2 - mul_init_loop_jump_table
   DQ mul_init_loop3 - mul_init_loop_jump_table
   DQ mul_init_loop4 - mul_init_loop_jump_table
   DQ mul_init_loop5 - mul_init_loop_jump_table
   DQ mul_init_loop6 - mul_init_loop_jump_table
   DQ mul_init_loop7 - mul_init_loop_jump_table

   align 16
mul_inner_loop_jump_table:
   DQ mul_inner_loop_cmp - mul_inner_loop_jump_table
   DQ mul_inner_loop1 - mul_inner_loop_jump_table
   DQ mul_inner_loop2 - mul_inner_loop_jump_table
   DQ mul_inner_loop3 - mul_inner_loop_jump_table
   DQ mul_inner_loop4 - mul_inner_loop_jump_table
   DQ mul_inner_loop5 - mul_inner_loop_jump_table
   DQ mul_inner_loop6 - mul_inner_loop_jump_table
   DQ mul_inner_loop7 - mul_inner_loop_jump_table

?omul_asmx64@bignum@@YAXPEA_KPEB_K_K12@Z ENDP   ; cl::omul_asmx64<unsigned __int64>

_TEXT ENDS

END
