/*
 *  arbitrary length number arithmetic - C++ long int class
 */

// $+HEADER$
// 
// Copyright 2015 Christoph Lueders
// 
// This file is part of the BIGNUM Library.
// 
// The BIGNUM Library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
// 
// The BIGNUM Library is distributed in the hope that it will be useful, but 
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public 
// License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with the BIGNUM Library.  If not, see <http://www.gnu.org/licenses/>.
// 
// $-HEADER$

// $Date: 2015-04-24 11:47:36 +0200 (Fr, 24 Apr 2015) $
// $Rev: 26592 $

#ifndef CL_BIGNUM_BIGNUM_T_H
#define CL_BIGNUM_BIGNUM_T_H 1

#ifndef CL_CLLIB_H
#include "cl/cllib.h"
#endif

#include "impl.h"
#include "gcd.h"
#include "cl/cllimits.h"
#include "cl/format.h"
#include "cl/trans_type.h"

#pragma pack(push, 8)  // arbitrary but fixed

#pragma warning(push)
#pragma warning(disable:4189)  // warning C4189: 'rsign' : local variable is initialized but not referenced

namespace bignum {

   using namespace cl;

   template<typename T> T zero_stop_sub(const T a, const T b) {
      return a < b ? 0 : a - b;
   }

   struct bignum_len {
      index len;
      bignum_len(index _len) : len(_len) {}
   };

   // if is_signed == numeric_limits<T>::is_signed sign extension will go smoothly
   // if is_signed == true && numeric_limits<T>::is_signed == false, we might need another word for proper sign
   // if is_signed == false && numeric_limits<T>::is_signed == true, we're headed for trouble anyway!
   template<typename small, typename T, bool is_signed=false>
   struct bignum_sx {
      static const bool needs_alloc = sizeof(T) < sizeof(small) || (is_signed && !numeric_limits<T>::is_signed);
      static const index _type_len = (sizeof(T) + sizeof(small) - 1) / sizeof(small);  // small's needed to store T
      static const index _sx_len = is_signed && !numeric_limits<T>::is_signed && sizeof(T) >= sizeof(small) ? 1 : 0;
      static const index _len = _type_len + _sx_len;
      small buf[_type_len + _sx_len];
      void _set(const T& t) {
         if (!is_signed && numeric_limits<T>::is_signed) classert(t >= 0);
         if (!is_signed) {
            classert(_sx_len == 0);
            if (sizeof(T) <= sizeof(small)) buf[0] = t;
            else *(T*)&buf = t;
         }
         else {
            if (sizeof(T) < sizeof(small)) {
               classert(_sx_len == 0);
               buf[0] = (make_signed<small>::type)t;
            }
            else {
               *(T*)&buf = t;
               if (!numeric_limits<T>::is_signed) {
                  classert(_sx_len == 1);
                  buf[_type_len] = t < 0 ? (small)-1 : 0;
               }
               else classert(_sx_len == 0);
            }
         }
      }
      bignum_sx(const T& t) {
         _set(t);
      }
      index len() const {
         return _len;
      }
   };


   struct bignum_base {
#ifndef NDEBUG
      static size_t obj_cnt;  // count object instances
#endif
   };

   template<typename small, bool is_signed=true>
   struct basic_bignum : public bignum_base {
      small* ptr;  // pointer to number, starting with data.  refcnt etc is before that
      typedef small small_type;
      typedef basic_bignum (basic_bignum::*worker_func)(const small* bptr, index blen) const;
      typedef void (basic_bignum::*worker_on_func)(const small* bptr, index blen);
      const static small one = 1;

      // ------ con/destruction -----------------------------------------------
      basic_bignum() {  // empty ctor sets len to zero, hence value is zero
         ptr = bignum_alloc<small>(1);  // len == 0
         CLDEBUG(++obj_cnt);
      }
      basic_bignum(bignum_len l) {
         ptr = bignum_alloc<small>(l.len);
         CLDEBUG(++obj_cnt);
      }
      template<typename T> void _set(const T& t) {
         classert(numeric_limits<T>::is_integer);
         if (!is_signed && numeric_limits<T>::is_signed) classert(t >= 0);  // setting an unsigned bignum to a negative number is trouble
         const index l = type_len<T>() + (sizeof(T) >= sizeof(small) && is_signed && !numeric_limits<T>::is_signed ? 1 : 0);
         classert(maxlen() >= l);
         if (sizeof(T) < sizeof(small) || (sizeof(T) == sizeof(small) && !(is_signed && !numeric_limits<T>::is_signed))) {  // handle smaller built-in integer types
            classert(l == 1);
            *ptr = t;
            len() = 1;
         }
         else { // handle larger built-in integer types
            memcpy(ptr, &t, sizeof(T));
            if (is_signed && !numeric_limits<T>::is_signed) {
               classert(l == type_len<T>() + 1);
               ptr[l-1] = !numeric_limits<T>::is_signed || t >= 0 ? 0 : (small)-1;
            }
            else
               classert(l == type_len<T>());
            len() = l > 1 ? tnorm<small, is_signed>(ptr, l) : 1;
         }
      }
      template<typename T> basic_bignum(const T& t) {  // ctor for built-in integer types
         const index l = type_len<T>() + (sizeof(T) >= sizeof(small) && is_signed && !numeric_limits<T>::is_signed ? 1 : 0);
         ptr = bignum_alloc<small>(l + 1);  // reserve one more word so add's will not realloc so often
         _set(t);
         classert(refcnt() == 1);
         CLDEBUG(++obj_cnt);
      }
      basic_bignum(const basic_bignum& a) {  // copy ctor
         ptr = a.ptr;
         inc_ref();
         CLDEBUG(++obj_cnt);
      }
      // helper to copy contents from (maybe) other sized and signed external type
      template<typename ext_small> void _set_ext(const ext_small* a, const index alen, const bool ext_signed) {
         const index l = (alen * sizeof(ext_small) + sizeof(small) - 1) / sizeof(small);
         //    small  ext_small  sx needed
         //   signed     signed  false
         //   signed   unsigned  maybe (if top_set(a))
         // unsigned     signed  false (no use anyway if ext_small is negative)
         // unsigned   unsigned  false
         const bool need_sx = is_signed && !ext_signed;
         ptr = bignum_alloc<small>(l + (need_sx ? 1 : 0));
         if (l > 0) {
            if (sizeof(small) > sizeof(ext_small)) {
               // the following memcpy will not entirely fill our buffer, pad with proper sign
               ptr[l-1] = is_signed && is_negative<ext_small>(a, alen, ext_signed) ? (small)-1 : 0;
            }
            memcpy(ptr, a, alen * sizeof(ext_small));
            if (is_signed && !ext_signed) ptr[l] = 0;  // zero extend, since the other was unsigned
            len() = tnorm<small, is_signed>(ptr, l + (need_sx ? 1 : 0));
         }
      }
      template<typename ext_small, bool ext_signed> basic_bignum(const basic_bignum<ext_small, ext_signed>& a) {  // copy ctor for other signedness type
         _set_ext(a.ptr, a.len(), ext_signed);
         CLDEBUG(++obj_cnt);
      }
      template<typename ext_small> basic_bignum(const ext_small* a, const index alen, const bool ext_signed=is_signed) {  // copy ctor for pointer, length pair
         _set_ext(a, alen, ext_signed);
         CLDEBUG(++obj_cnt);
      }
      ~basic_bignum() {
         dec_ref();
         CLDEBUG(--obj_cnt);
      }

      // ------ assignment ----------------------------------------------------
      basic_bignum& operator=(const basic_bignum& a) {  // assignment op
         if (this == &a) return *this;  // ignore a = a
         dec_ref();
         ptr = a.ptr;
         inc_ref();
         return *this;
      }
      template<typename ext_small, bool ext_signed> basic_bignum& operator=(const basic_bignum<ext_small, ext_signed>& a) {  // assignment op for other type
         dec_ref();
         _set_ext(a.ptr, a.len(), ext_signed);
         return *this;
      }
      template<typename T> basic_bignum& operator=(const T& t) {  // assignment op for built-in integer types
         const index l = type_len<T>() + (sizeof(T) >= sizeof(small) && is_signed && !numeric_limits<T>::is_signed ? 1 : 0);
         uniq_ref_lossy(l + 1);  // reserve one more word so add's will not realloc so often
         _set(t);
         return *this;
      }

      // ------ conversion ----------------------------------------------------
      template<typename T> operator T() const {
         classert(numeric_limits<T>::is_integer);
         if (len() == 0) return 0;
         if (sizeof(T) <= sizeof(small)) return ptr[0];
         if (len() >= type_len<T>()) return *(T*)ptr;
         return *(T*)ptr & (((T)1 << len() * bits(small)) - 1);
      }
      operator bool() const {
         return tnorm<small, false>(ptr, len()) > 0;
      }
      operator double() const {
         return to_dbl();
      }
      template<typename ext_small, bool ext_signed> operator basic_bignum<ext_small, ext_signed>() const {  // conversion op for other type
         basic_bignum<ext_small, ext_signed> n(*this);
         return n;
      }

      // ------ comparison ----------------------------------------------------
      int _comp_func(const basic_bignum& t) const {
         return tcomp<small, is_signed, false>(ptr, len(), t.ptr, t.len());
      }
      template<typename T> int _comp_func(const T& t) const {
         classert(numeric_limits<T>::is_integer);
         if (needs_alloc<T>()) {
            bignum_sx<small, T, is_signed> t2(t);
            return tcomp<small, is_signed, false>(ptr, len(), t2.buf, t2.len());
         }
         else return tcomp<small, is_signed, false>(ptr, len(), (const small*)&t, type_len<T>());
      }

      // ------ addition ------------------------------------------------------
      void _addon_func(const small* bptr, index blen) {
         index mx = tmax(len(), blen);
         if (refcnt() == 1 && maxlen() >= mx) {
            tsignx<small, is_signed>(ptr, len(), mx);  // make sure upper yet unused words are properly inited
            const bool asign = is_negative<small, is_signed>(ptr, len());
            const bool bsign = is_negative<small, is_signed>(bptr, blen);
            small of = tadd_on<small, is_signed>(ptr, mx, bptr, blen);
            const bool rsign = is_negative<small, is_signed>(ptr, len());
            if (need_signx(asign, bsign, of, rsign)) {
               if (maxlen() <= mx) reserve(mx+1);
               ptr[mx++] = add_of_word(asign, bsign, of);
            }
         }
         else {  // allocate new storage
            ++mx;  // leave space for overflow
            small* ptr2 = bignum_alloc<small>(mx);
            tadd<small, is_signed>(ptr2, mx, ptr, len(), bptr, blen);
            dec_ref();
            ptr = ptr2;
         }
         len() = tnorm<small, is_signed>(ptr, mx);
      }
      basic_bignum& operator+=(const basic_bignum& b) {
         _addon_func(b.ptr, b.len());
         return *this;
      }
      template<typename T> basic_bignum& operator+=(const T& b) {
         return signx_type_on<&basic_bignum::_addon_func>(b);
      }

      basic_bignum _add_func(const small* bptr, index blen) const {
         index mx = tmax(len(), blen) + 1;  // reserve one more word so add will not realloc
         basic_bignum n = basic_bignum(bignum_len(mx));
         tadd<small, is_signed>(n.ptr, mx, ptr, len(), bptr, blen);
         n.len() = tnorm<small, is_signed>(n.ptr, mx);
         return n;
      }
      /*basic_bignum operator+(const basic_bignum& b) const {
         return _add_func(b.ptr, b.len());
      }
      template<typename T> basic_bignum operator+(const T& b) const {
         return signx_type<&basic_bignum::_add_func>(b);
      }*/

      // ------ subtraction ---------------------------------------------------
      void _subon_func(const small* bptr, index blen) {
         index mx = tmax(len(), blen);
         if (refcnt() == 1 && maxlen() >= mx) {
            tsignx<small, is_signed>(ptr, len(), mx);  // make sure upper yet unused words are properly inited
            const bool asign = is_negative<small, is_signed>(ptr, len());
            const bool bsign = is_negative<small, is_signed>(bptr, blen);
            small of = tsub_on<small, false, is_signed>(ptr, mx, bptr, blen);
            classert(is_signed || of == 0);  // otherwise overflow has happened, which is not healthy!
            const bool rsign = is_negative<small, is_signed>(ptr, len());
            if (need_signx(asign, bsign, of, rsign)) {
               if (maxlen() <= mx) reserve(mx+1);
               ptr[mx++] = sub_of_word(asign, bsign, of);
            }
         }
         else {  // allocate new storage
            ++mx;  // leave space for overflow
            small* ptr2 = bignum_alloc<small>(mx);
            tsub<small, false, is_signed>(ptr2, mx, ptr, len(), bptr, blen);
            dec_ref();
            ptr = ptr2;
         }
         len() = tnorm<small, is_signed>(ptr, mx);
      }
      basic_bignum& operator-=(const basic_bignum& b) {
         _subon_func(b.ptr, b.len());
         return *this;
      }
      template<typename T> basic_bignum& operator-=(const T& b) {
         return signx_type_on<&basic_bignum::_subon_func>(b);
      }

      template<bool swap>
      basic_bignum _sub_func(const small* bptr, index blen) const {
         index mx = tmax(len(), blen) + 1;  // reserve one more word so sub will not realloc
         basic_bignum n = basic_bignum(bignum_len(mx));
         small of = tsub<small, swap, is_signed>(n.ptr, mx, ptr, len(), bptr, blen);
         classert(is_signed || of == 0);  // otherwise overflow has happened, which is not healthy!
         n.len() = tnorm<small, is_signed>(n.ptr, mx);
         return n;
      }
      /*basic_bignum operator-(const basic_bignum& b) const {
         return _sub_func(b.ptr, b.len());
      }
      template<typename T> basic_bignum operator-(const T& b) const {
         return signx_type<&basic_bignum::_sub_func>(b);
      }*/

      // ------ unaries -------------------------------------------------------
      basic_bignum operator-() {
         index l = len();
         bool asign = is_negative<small, is_signed>(ptr, l);
         basic_bignum n = basic_bignum(bignum_len(l + 1));
         neg(n.ptr, ptr, l);
         bool rsign = is_negative<small, is_signed>(n.ptr, l);
         if (is_signed && asign == rsign)
            n.ptr[l++] = asign == true ? 0 : (small)-1;
         n.len() = l;
         return n;
      }

      basic_bignum& operator+() {
         return *this;
      }

      // ------ inc/decrement -------------------------------------------------
      basic_bignum& operator++() {  // prefix increment (++x)
         _addon_func(&one, 1);
         return *this;
      }
      basic_bignum operator++(int) {  // postfix increment (x++)
         basic_bignum n = *this;
         _addon_func(&one, 1);
         return n;
      }

      basic_bignum& operator--() {  // prefix decrement (--x)
         _subon_func(&one, 1);
         return *this;
      }
      basic_bignum operator--(int) {  // postfix decrement (x--)
         basic_bignum n = *this;
         _subon_func(&one, 1);
         return n;
      }

      // ------ multiplication ------------------------------------------------
      void _mulon_func(const small* bptr, index blen) {
         index mx = len() + blen;
         small* nptr = bignum_alloc<small>(mx);
         tmul<small, is_signed>(nptr, ptr, len(), bptr, blen);
         dec_ref();
         ptr = nptr;
         len() = tnorm<small, is_signed>(ptr, mx);
      }
      basic_bignum& operator*=(const basic_bignum& b) {
         _mulon_func(b.ptr, b.len());
         return *this;
      }
      template<typename T> basic_bignum& operator*=(const T& b) {
         return signx_type_on<&basic_bignum::_mulon_func>(b);
      }

      basic_bignum _mul_func(const small* bptr, index blen) const {
         index mx = len() + blen;
         basic_bignum n = basic_bignum(bignum_len(mx));
         tmul<small, is_signed>(n.ptr, ptr, len(), bptr, blen);
         n.len() = tnorm<small, is_signed>(n.ptr, mx);
         return n;
      }
      /*basic_bignum operator*(const basic_bignum& b) const {
         return _mul_func(b.ptr, b.len());
      }
      template<typename T> basic_bignum operator*(const T& b) const {
         return signx_type<&basic_bignum::_mul_func>(b);
      }*/

      // ------ division ------------------------------------------------------
      void _divon_func(const small* bptr, index blen) {
         blen = tnorm<small, is_signed>(bptr, blen);
         index mx = len() - blen + 1;
         small* nptr = bignum_alloc<small>(mx);
         tdiv<small, is_signed>(nptr, mx, nullptr, 0, ptr, len(), bptr, blen);
         dec_ref();
         ptr = nptr;
         len() = tnorm<small, is_signed>(ptr, mx);
      }
      basic_bignum& operator/=(const basic_bignum& b) {
         _divon_func(b.ptr, b.len());
         return *this;
      }
      template<typename T> basic_bignum& operator/=(const T& b) {
         return signx_type_on<&basic_bignum::_divon_func>(b);
      }

      static basic_bignum __div_func(const small* aptr, index alen, const small* bptr, index blen) {
         blen = tnorm<small, is_signed>(bptr, blen);
         index mx = (alen > blen ? alen - blen : 0) + 1;
         basic_bignum n = basic_bignum(bignum_len(mx));
         tdiv<small, is_signed>(n.ptr, mx, nullptr, 0, aptr, alen, bptr, blen);
         n.len() = tnorm<small, is_signed>(n.ptr, mx);
         return n;
      }
      basic_bignum _div_func(const small* bptr, index blen) const {
         return __div_func(ptr, len(), bptr, blen);
      }
      basic_bignum _divinv_func(const small* bptr, index blen) const {
         return __div_func(bptr, blen, ptr, len());
      }
      /*basic_bignum operator/(const basic_bignum& b) const {
         return _div_func(b.ptr, b.len());
      }
      template<typename T> basic_bignum operator/(const T& b) const {
         return signx_type<&basic_bignum::_div_func>(b);
      }*/

      // ------ modulo division -----------------------------------------------
      void _modon_func(const small* bptr, index blen) {
         small* nptr = bignum_alloc<small>(blen);
         tdiv<small, is_signed>(nullptr, 0, nptr, blen, ptr, len(), bptr, blen);
         dec_ref();
         ptr = nptr;
         len() = tnorm<small, is_signed>(ptr, blen);
      }
      basic_bignum& operator%=(const basic_bignum& b) {
         _modon_func(b.ptr, b.len());
         return *this;
      }
      template<typename T> basic_bignum& operator%=(const T& b) {
         return signx_type_on<&basic_bignum::_modon_func>(b);
      }

      basic_bignum __mod_func(const small* aptr, index alen, const small* bptr, index blen) const {
         basic_bignum n = basic_bignum(bignum_len(blen));
         tdiv<small, is_signed>(nullptr, 0, n.ptr, blen, aptr, alen, bptr, blen);
         n.len() = tnorm<small, is_signed>(n.ptr, blen);
         return n;
      }
      basic_bignum _mod_func(const small* bptr, index blen) const {
         return __mod_func(ptr, len(), bptr, blen);
      }
      basic_bignum _modinv_func(const small* bptr, index blen) const {
         return __mod_func(bptr, blen, ptr, len());
      }
      /*basic_bignum operator%(const basic_bignum& b) const {
         return _mod_func(b.ptr, b.len());
      }
      template<typename T> basic_bignum operator%(const T& b) const {
         return signx_type<&basic_bignum::_mod_func>(b);
      }*/

      // ------ bitwise and ---------------------------------------------------
      void _andon_func(const small* bptr, index blen) {
         index l = tmin(len(), blen);
         if (refcnt() == 1) and_on(ptr, l, bptr, l);
         else {  // allocate new storage
            small* ptr2 = bignum_alloc<small>(l);
            and_ls(ptr2, l, ptr, l, bptr, l);
            dec_ref();
            ptr = ptr2;
         }
         len() = tnorm<small, is_signed>(ptr, l);
      }
      basic_bignum& operator&=(const basic_bignum& b) {
         _andon_func(b.ptr, b.len());
         return *this;
      }
      template<typename T> basic_bignum& operator&=(const T& b) {
         return signx_type_on<&basic_bignum::_andon_func>(b);
      }

      basic_bignum _and_func(const small* bptr, index blen) const {
         index l = tmin(len(), blen);
         basic_bignum n = basic_bignum(bignum_len(l));
         and_ls(n.ptr, l, ptr, l, bptr, l);
         n.len() = tnorm<small, is_signed>(n.ptr, l);
         return n;
      }
      /*basic_bignum operator&(const basic_bignum& b) const {
         return _and_func(b.ptr, b.len());
      }
      template<typename T> basic_bignum operator&(const T& b) const {
         return signx_type<&basic_bignum::_and_func>(b);
      }*/

      // ------ bitwise or ----------------------------------------------------
      void _oron_func(const small* bptr, index blen) {
         index l = tmax(len(), blen);
         if (refcnt() == 1 && maxlen() >= l) {
            tsignx<small, false>(ptr, len(), l);  // make sure upper yet unused words are properly inited
            or_on(ptr, l, bptr, blen);
         }
         else {  // allocate new storage
            small* ptr2 = bignum_alloc<small>(l);
            or(ptr2, l, ptr, len(), bptr, blen);
            dec_ref();
            ptr = ptr2;
         }
         len() = tnorm<small, is_signed>(ptr, l);
      }
      basic_bignum& operator|=(const basic_bignum& b) {
         _oron_func(b.ptr, b.len());
         return *this;
      }
      template<typename T> basic_bignum& operator|=(const T& b) {
         return signx_type_on<&basic_bignum::_oron_func>(b);
      }

      basic_bignum _or_func(const small* bptr, index blen) const {
         index l = tmax(len(), blen);
         basic_bignum n = basic_bignum(bignum_len(l));
         or(n.ptr, l, ptr, len(), bptr, blen);
         n.len() = tnorm<small, is_signed>(n.ptr, l);
         return n;
      }
      /*basic_bignum operator|(const basic_bignum& b) const {
         return _or_func(b.ptr, b.len());
      }
      template<typename T> basic_bignum operator|(const T& b) const {
         return signx_type<&basic_bignum::_or_func>(b);
      }*/

      // ------ shift left ----------------------------------------------------
      basic_bignum& _shl_on(const bit_index b) {
         index mx = len() + (b + bits(small) - 1) / bits(small);
         if (refcnt() == 1 && maxlen() >= mx) {
            zero_in(ptr, mx, len());
            shl_on(ptr, mx, b);
         }
         else {  // allocate new storage
            small* ptr2 = bignum_alloc<small>(mx);
            shl(ptr2, mx, ptr, len(), b);
            dec_ref();
            ptr = ptr2;
         }
         len() = tnorm<small, is_signed>(ptr, mx);
         return *this;
      }
      template<typename T> basic_bignum& operator<<=(const T& b) {
         caassert(b / bits(small) < numeric_limits<index>::max());
         return _shl_on(b);
      }

      basic_bignum _shl(const bit_index b) const {
         index mx = len() + (b + bits(small) - 1) / bits(small);
         basic_bignum n = basic_bignum(bignum_len(mx));
         shl<small>(n.ptr, mx, ptr, len(), b);
         n.len() = tnorm<small, is_signed>(n.ptr, mx);
         return n;
      }
      template<typename T> basic_bignum operator<<(const T& b) const {
         caassert(b / bits(small) < numeric_limits<index>::max());
         return _shl(b);
      }

      // ------ shift right ---------------------------------------------------
      basic_bignum& _shr_on(const bit_index b) {
         index l = zero_stop_sub(len(), (index)(b / bits(small)));
         if (refcnt() == 1) shr_on(ptr, len(), b);
         else {  // allocate new storage
            small* ptr2 = bignum_alloc<small>(l);
            shr(ptr2, l, ptr, len(), b);
            dec_ref();
            ptr = ptr2;
         }
         len() = tnorm<small, is_signed>(ptr, l);
         return *this;
      }
      template<typename T> basic_bignum& operator>>=(const T& b) {
         caassert(b / bits(small) < numeric_limits<index>::max());
         return _shr_on(b);
      }

      basic_bignum _shr(const bit_index b) const {
         index l = zero_stop_sub(len(), (index)(b / bits(small)));
         basic_bignum n = basic_bignum(bignum_len(l));
         shr<small>(n.ptr, l, ptr, len(), b);
         n.len() = tnorm<small, is_signed>(n.ptr, l);
         return n;
      }
      template<typename T> basic_bignum operator>>(const T& b) const {
         caassert(b / bits(small) < numeric_limits<index>::max());
         return _shr(b);
      }

      // ------ exponentiation ------------------------------------------------
      static basic_bignum pow2(const bit_index exp) {
         caassert(exp >= 0);
         const index mx = (1 + exp + bits(small) - 1) / bits(small) + (is_signed ? 1 : 0);
         caassert(mx > 0);
         basic_bignum n = basic_bignum(bignum_len(mx));
         zero(n.ptr, mx, 0);
         n.ptr[exp / bits(small)] = (small)1 << (exp % bits(small));
         n.len() = tnorm<small, is_signed>(n.ptr, mx);
         return n;
      }

      basic_bignum squ() const {
         return _mul_func(ptr, len());
      }

      // see http://stackoverflow.com/questions/5246856/how-did-python-implement-the-built-in-function-pow
      // or http://en.wikipedia.org/wiki/Modular_exponentiation
      // or d:\bin\pylib\modular.py

      basic_bignum pow(const basic_bignum& exp) const {
         basic_bignum n = 1;
         if (exp > 0) {
            basic_bignum base = *this;
            bit_index s = exp.bit_length();
            bit_index i = 0;
            while (1) {
               if (exp.is_bit_set(i)) n *= base;
               ++i;
               if (i >= s) break;
               base *= base;
            }
         }
         return n;
      }
      basic_bignum mod_pow(const basic_bignum& exp, const basic_bignum& mod) const {
         basic_bignum n = 1;
         if (exp > 0) {
            basic_bignum base = *this;
            bit_index s = exp.bit_length();
            bit_index i = 0;
            while (1) {
               if (exp.is_bit_set(i)) {
                  n *= base;
                  n %= mod;
               }
               ++i;
               if (i >= s) break;
               base *= base;
               base %= mod;
            }
         }
         return n;
      }

      // ------ misc ----------------------------------------------------------
      bit_index bit_length() const {
         return bignum::bit_length(ptr, len());
      }

      index word_length() const {
         return tnorm<small, is_signed>(ptr, len());
      }

      bool is_bit_set(bit_index i) const {
         return bignum::is_bit_set(ptr, len(), i);
      }

      bool is_pow2() const {
         classert(!(is_negative<small, is_signed>(ptr, len())));
         const index l = normu(ptr, len());
         if (l == 0) return false;
         return ::is_pow2(ptr[l-1]) && normu(ptr, l-1) == 0;
      }

      basic_bignum low_bits(bit_index b) const {
         basic_bignum m = ((bignum_t)1 << b) - 1;  // quick and dirty
         m &= *this;
         return m;
      }

      basic_bignum mod_inv(const basic_bignum& mod) const {
         return ::bignum::mod_inv(*this, mod);
      }

      double to_dbl() const {
         bool sign = is_negative<small, is_signed>(ptr, len());
         classert(!sign);
         index l = normu(ptr, len());
         if (l <= sizeof(uint64) / sizeof(small)) return (uint64)*this;
         bit_index i = bignum::bit_length(ptr, l);
         uint64 top64 = *(uint64*)&ptr[l-sizeof(uint64)/sizeof(small)];
         if (i % bits(small) == 0)
            return (double)top64 * ::pow(2.0, (double)(i-bits(uint64)));
         else {
            unsigned sh = bits(small) - (i % bits(small));
            return (double)((top64 << sh) | ptr[l-sizeof(uint64)/sizeof(small)-1] >> (bits(small)-sh)) * ::pow(2.0, (double)(i-bits(uint64)));
         }
         /*if (sign) {
            if (l == 1) return sign ? -(double)(0-ptr[0]) : ptr[0];
            return 0.0;
         }*/
      }

      template<typename ext_small>
      void copy_to(ext_small* r, const index rlen) const {
         const size_t rby = rlen * sizeof(ext_small);
         classert(rby * 8 >= bit_length());
         const size_t mn = tmin(rby, len() * sizeof(small));
         memcpy(r, ptr, mn);
         if (rby > mn) memzero((char*)r + mn, rby - mn);
      }

      // ------ utility -------------------------------------------------------
      template<typename T> static index type_len() {
         return (sizeof(T) + sizeof(small) - 1) / sizeof(small);
      }
      template<typename T> static bool needs_alloc() {
         return bignum_sx<small, T, is_signed>::needs_alloc;
      }
      template<worker_on_func func, typename T> void signx_type_on_caller(const T b) {  // otherwise compiler creates two local variables with same value
         (this->*func)((const small*)&b, type_len<T>());
      }
      template<worker_on_func func, typename T> basic_bignum& signx_type_on(const T b) {
         classert(numeric_limits<T>::is_integer);
         if (needs_alloc<T>()) {  // handle smaller built-in integer types
            bignum_sx<small, T, is_signed> t2(b);
            (this->*func)(t2.buf, t2.len());
         }
         else  // handle larger built-in integer types
            signx_type_on_caller<func, T>(b);
         return *this;
      }
      template<worker_func func, typename T> basic_bignum signx_type_caller(const T b) const {  // otherwise compiler creates two local variables with same value
         return (this->*func)((const small*)&b, type_len<T>());
      }
      template<worker_func func, typename T> basic_bignum signx_type(const T b) const {
         classert(numeric_limits<T>::is_integer);
         if (needs_alloc<T>()) {  // handle smaller built-in integer types
            bignum_sx<small, T, is_signed> t2(b);
            return (this->*func)(t2.buf, t2.len());
         }
         else  // handle larger built-in integer types
            return signx_type_caller<func, T>(b);
      }

      bignum_head& head() const {
         caassert(ptr != nullptr);
         bignum_head* h = (bignum_head*)((char*)ptr - bignum_aligned_head_sz);
         return *h;
      }
      index& refcnt() const {
         return head().refcnt;
      }
      index& len() const {
         return head().len;
      }
      index& maxlen() const {
         return head().maxlen;
      }
      void dec_ref() {
         if (--refcnt() == 0) bignum_free(ptr);
         ptr = nullptr;
      }
      void inc_ref() {
         ++refcnt();
      }

      void reserve(size_t l) {  // make sure buffer is large enough; reallocate if needed and preserve contents
         if (maxlen() >= l) return;
         small* ptr2 = bignum_alloc<small>(l);
         index old_len = len();
         copy(ptr2, ptr, old_len);
         dec_ref();
         ptr = ptr2;
         len() = old_len;
         classert(maxlen() >= l);
         classert(maxlen() >= old_len);
      }
      void reserve_lossy(size_t l) {  // make sure buffer is large enough; reallocate if needed, don't preserve contents
         if (maxlen() >= l) return;
         bignum_free(ptr);
         ptr = bignum_alloc<small>(l);
         classert(maxlen() >= l);
      }
      void uniq_ref_lossy(size_t l) {  // make sure we own the buffer & it's large enough, don't preserve contents
         if (refcnt() == 1) reserve_lossy(l);
         else {
            dec_ref();
            ptr = bignum_alloc<small>(l);
         }
         classert(refcnt() == 1);
         classert(maxlen() >= l);
      }
   };

   // comparison
   template<typename small, bool is_signed> bool operator==(const basic_bignum<small, is_signed>& l, const basic_bignum<small, is_signed>& r) { return l._comp_func(r) == 0; }
   template<typename small, bool is_signed> bool operator!=(const basic_bignum<small, is_signed>& l, const basic_bignum<small, is_signed>& r) { return l._comp_func(r) != 0; }
   template<typename small, bool is_signed> bool operator< (const basic_bignum<small, is_signed>& l, const basic_bignum<small, is_signed>& r) { return l._comp_func(r) <  0; }
   template<typename small, bool is_signed> bool operator<=(const basic_bignum<small, is_signed>& l, const basic_bignum<small, is_signed>& r) { return l._comp_func(r) <= 0; }
   template<typename small, bool is_signed> bool operator> (const basic_bignum<small, is_signed>& l, const basic_bignum<small, is_signed>& r) { return l._comp_func(r) >  0; }
   template<typename small, bool is_signed> bool operator>=(const basic_bignum<small, is_signed>& l, const basic_bignum<small, is_signed>& r) { return l._comp_func(r) >= 0; }

   template<typename small, typename T, bool is_signed> bool operator==(const basic_bignum<small, is_signed>& l, const T& r) { return l._comp_func(r) == 0; }
   template<typename small, typename T, bool is_signed> bool operator!=(const basic_bignum<small, is_signed>& l, const T& r) { return l._comp_func(r) != 0; }
   template<typename small, typename T, bool is_signed> bool operator< (const basic_bignum<small, is_signed>& l, const T& r) { return l._comp_func(r) <  0; }
   template<typename small, typename T, bool is_signed> bool operator<=(const basic_bignum<small, is_signed>& l, const T& r) { return l._comp_func(r) <= 0; }
   template<typename small, typename T, bool is_signed> bool operator> (const basic_bignum<small, is_signed>& l, const T& r) { return l._comp_func(r) >  0; }
   template<typename small, typename T, bool is_signed> bool operator>=(const basic_bignum<small, is_signed>& l, const T& r) { return l._comp_func(r) >= 0; }

   template<typename small, typename T, bool is_signed> bool operator==(const T& l, const basic_bignum<small, is_signed>& r) { return r._comp_func(l) == 0; }
   template<typename small, typename T, bool is_signed> bool operator!=(const T& l, const basic_bignum<small, is_signed>& r) { return r._comp_func(l) != 0; }
   template<typename small, typename T, bool is_signed> bool operator< (const T& l, const basic_bignum<small, is_signed>& r) { return r._comp_func(l) >  0; }
   template<typename small, typename T, bool is_signed> bool operator<=(const T& l, const basic_bignum<small, is_signed>& r) { return r._comp_func(l) >= 0; }
   template<typename small, typename T, bool is_signed> bool operator> (const T& l, const basic_bignum<small, is_signed>& r) { return r._comp_func(l) <  0; }
   template<typename small, typename T, bool is_signed> bool operator>=(const T& l, const basic_bignum<small, is_signed>& r) { return r._comp_func(l) <= 0; }

   // +
   template<typename small, bool is_signed> basic_bignum<small, is_signed> operator+(const basic_bignum<small, is_signed>& a, const basic_bignum<small, is_signed>& b) {
      return a._add_func(b.ptr, b.len());
   }
   template<typename small, bool is_signed, typename T> basic_bignum<small, is_signed> operator+(const basic_bignum<small, is_signed>& a, const T& b) {
      return a.signx_type<&basic_bignum<small, is_signed>::_add_func>(b);
   }
   template<typename small, bool is_signed, typename T> basic_bignum<small, is_signed> operator+(const T& a, const basic_bignum<small, is_signed>& b) {
      return b.signx_type<&basic_bignum<small, is_signed>::_add_func>(a);
   }

   // -
   template<typename small, bool is_signed> basic_bignum<small, is_signed> operator-(const basic_bignum<small, is_signed>& a, const basic_bignum<small, is_signed>& b) {
      return a._sub_func<false>(b.ptr, b.len());
   }
   template<typename small, bool is_signed, typename T> basic_bignum<small, is_signed> operator-(const basic_bignum<small, is_signed>& a, const T& b) {
      return a.signx_type<&basic_bignum<small, is_signed>::_sub_func<false>>(b);
   }
   template<typename small, bool is_signed, typename T> basic_bignum<small, is_signed> operator-(const T& a, const basic_bignum<small, is_signed>& b) {
      return b.signx_type<&basic_bignum<small, is_signed>::_sub_func<true>>(a);
   }

   // *
   template<typename small, bool is_signed> basic_bignum<small, is_signed> operator*(const basic_bignum<small, is_signed>& a, const basic_bignum<small, is_signed>& b) {
      return a._mul_func(b.ptr, b.len());
   }
   template<typename small, bool is_signed, typename T> basic_bignum<small, is_signed> operator*(const basic_bignum<small, is_signed>& a, const T& b) {
      return a.signx_type<&basic_bignum<small, is_signed>::_mul_func>(b);
   }
   template<typename small, bool is_signed, typename T> basic_bignum<small, is_signed> operator*(const T& a, const basic_bignum<small, is_signed>& b) {
      return b.signx_type<&basic_bignum<small, is_signed>::_mul_func>(a);
   }

   // /
   template<typename small, bool is_signed> basic_bignum<small, is_signed> operator/(const basic_bignum<small, is_signed>& a, const basic_bignum<small, is_signed>& b) {
      return a._div_func(b.ptr, b.len());
   }
   template<typename small, bool is_signed, typename T> basic_bignum<small, is_signed> operator/(const basic_bignum<small, is_signed>& a, const T& b) {
      return a.signx_type<&basic_bignum<small, is_signed>::_div_func>(b);
   }
   template<typename small, bool is_signed, typename T> basic_bignum<small, is_signed> operator/(const T& a, const basic_bignum<small, is_signed>& b) {
      return b.signx_type<&basic_bignum<small, is_signed>::_divinv_func>(a);
   }

   // %
   template<typename small, bool is_signed> basic_bignum<small, is_signed> operator%(const basic_bignum<small, is_signed>& a, const basic_bignum<small, is_signed>& b) {
      return a._mod_func(b.ptr, b.len());
   }
   template<typename small, bool is_signed, typename T> basic_bignum<small, is_signed> operator%(const basic_bignum<small, is_signed>& a, const T& b) {
      return a.signx_type<&basic_bignum<small, is_signed>::_mod_func>(b);
   }
   template<typename small, bool is_signed, typename T> basic_bignum<small, is_signed> operator%(const T& a, const basic_bignum<small, is_signed>& b) {
      return b.signx_type<&basic_bignum<small, is_signed>::_modinv_func>(a);
   }

   // &
   template<typename small, bool is_signed> basic_bignum<small, is_signed> operator&(const basic_bignum<small, is_signed>& a, const basic_bignum<small, is_signed>& b) {
      return a._and_func(b.ptr, b.len());
   }
   template<typename small, bool is_signed, typename T> basic_bignum<small, is_signed> operator&(const basic_bignum<small, is_signed>& a, const T& b) {
      return a.signx_type<&basic_bignum<small, is_signed>::_and_func>(b);
   }
   template<typename small, bool is_signed, typename T> basic_bignum<small, is_signed> operator&(const T& a, const basic_bignum<small, is_signed>& b) {
      return b.signx_type<&basic_bignum<small, is_signed>::_and_func>(a);
   }

   // |
   template<typename small, bool is_signed> basic_bignum<small, is_signed> operator|(const basic_bignum<small, is_signed>& a, const basic_bignum<small, is_signed>& b) {
      return a._or_func(b.ptr, b.len());
   }
   template<typename small, bool is_signed, typename T> basic_bignum<small, is_signed> operator|(const basic_bignum<small, is_signed>& a, const T& b) {
      return a.signx_type<&basic_bignum<small, is_signed>::_or_func>(b);
   }
   template<typename small, bool is_signed, typename T> basic_bignum<small, is_signed> operator|(const T& a, const basic_bignum<small, is_signed>& b) {
      return b.signx_type<&basic_bignum<small, is_signed>::_or_func>(a);
   }

}  // namespace bignum

typedef bignum::basic_bignum<bignum::limb> bignum_t;
typedef bignum::basic_bignum<bignum::limb, false> ubignum_t;


namespace cl {

   // ------ format -----------------------------------------------------------

   template<class C, bool is_signed> CL_DECLSPEC_NOINLINE format_base<C>& _CL_CALL operator<<(format_base<C>& f, const bignum::basic_bignum<bignum::limb, is_signed>& b);
   template<class C, typename small, bool is_signed> CL_DECLSPEC_NOINLINE format_base<C>& _CL_CALL operator<<(format_base<C>& f, const bignum::basic_bignum<small, is_signed>& b) {
      return f << (bignum_t)b;
   }


   // ------ trans_type -------------------------------------------------------

   template<typename T> struct cl_make_unsigned<bignum::basic_bignum<T>> { typedef bignum::basic_bignum<T> type; };
   template<typename T> struct cl_make_unsigned<bignum::basic_bignum<T, false>> { typedef bignum::basic_bignum<T, false> type; };

   template<typename T> inline size_t cl_sizeof(bignum::basic_bignum<T> t) { return (t.bit_length() + 7) / 8; }
   template<typename T> inline size_t cl_sizeof(bignum::basic_bignum<T, false> t) { return (t.bit_length() + 7) / 8; }

   template<typename T> struct cl_limits<bignum::basic_bignum<T>> { static const bool has_limits = false; };
   template<typename T> struct cl_limits<bignum::basic_bignum<T, false>> { static const bool has_limits = false; };

}  // namespace cl

#pragma warning(pop)
#pragma pack(pop)

#endif  // ndef CL_BIGNUM_BIGNUM_T_H
