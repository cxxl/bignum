; $+HEADER$
; 
; Copyright 2015 Christoph Lueders
; 
; This file is part of the BIGNUM Library.
; 
; The BIGNUM Library is free software: you can redistribute it and/or modify
; it under the terms of the GNU Lesser General Public License as published by
; the Free Software Foundation, either version 3 of the License, or (at your 
; option) any later version.
; 
; The BIGNUM Library is distributed in the hope that it will be useful, but 
; WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
; or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public 
; License for more details.
; 
; You should have received a copy of the GNU Lesser General Public License
; along with the BIGNUM Library.  If not, see <http://www.gnu.org/licenses/>.
; 
; $-HEADER$

; $Date: 2015-04-24 11:28:16 +0200 (Fr, 24 Apr 2015) $
; $Rev: 26590 $


_TEXT SEGMENT

; ------ ADDITION/SUBTRACTION IN ONE ------------------------------------------

stackframe = 7*8

counter = stackframe - 56
plen = stackframe - 48
rlen = stackframe - 40
rbx_save = stackframe - 32
rsi_save = stackframe - 24
rdi_save = stackframe - 16
rbp_save = stackframe - 8

r12_save = stackframe + 8     ; shadow space
r13_save = stackframe + 16    ; shadow space
r14_save = stackframe + 24    ; shadow space
r15_save = stackframe + 32    ; shadow space
llen$ = stackframe + 40       ; parameter on stack

   align 16

?karasub_asmx64@bignum@@YA_KPEA_K0PEB_K1_K@Z PROC

; uint64 __cdecl addsub_asmx64(uint64* sum, uint64* diff, const uint64* a, const uint64* b, const uint64 len) {
; void __cdecl kara_sub(uint64* r, uint64 rlen, uint64* p, uint64 plen, uint64 llen)

; parameters:
; rcx = sum
; rdx = diff
; r8 = a
; r9 = b
; [rsp+40] = len

; rbx, rsi, rdi, rbp, r12 - r15 must be preserved by callee
; may be destroyed: rax, rcx, rdx, r8, r9, r10, r11
; http://msdn.microsoft.com/en-us/magazine/cc300794.aspx

   sub   rsp, stackframe

   mov   QWORD PTR rbx_save[rsp], rbx
   mov   QWORD PTR rsi_save[rsp], rsi
   mov   QWORD PTR rdi_save[rsp], rdi
   mov   QWORD PTR rbp_save[rsp], rbp
   mov   QWORD PTR r12_save[rsp], r12
   mov   QWORD PTR r13_save[rsp], r13
   mov   QWORD PTR r14_save[rsp], r14
   mov   QWORD PTR r15_save[rsp], r15

   mov   QWORD PTR rlen[rsp], rdx
   mov   QWORD PTR plen[rsp], r9          ; r9 ~ plen
   mov   rdx, r10                         ; rdx = r10 ~ llen
   mov   rdi, rcx                         ; rdi = rcx ~ r
   lea   rsi, QWORD PTR[rdx+2*rdx]
   add   rsi, rdi                         ; rsi = a
   mov   rbp, r8                          ; rbp = r8 ~ p


; prepare for init loop

   mov   r9, rdx
   xor   eax, eax
   xor   ebx, ebx
   xor   ecx, ecx
   jmp   short init_add_loop_start

init_add_loop:
   add   rdi, 8
   add   rsi, 8
   add   rbp, 8

init_add_loop_start:
   mov   r8, QWORD PTR [rdi+2*rdx]        ; b[0]

   bt    eax, 0                           ; load bc carry
   adc   r8, QWORD PTR [rdi+rdx]          ; c[0]
   setc  al                               ; save bc carry

   bt    ebx, 0                           ; load b add carry
   mov   r12, QWORD PTR [rsi]             ; a[0]
   adc   r12, r8
   setc  bl                               ; save b add carry

   bt    ebx, 8                           ; load b sub carry
   sbb   r12, QWORD PTR [rbp+rdx]         ; e[0]
   setc  bh                               ; save b sub carry

   bt    ecx, 0                           ; load c add carry
   adc   r8, QWORD PTR [rdi]              ; d[0]
   setc  cl                               ; save c add carry

   bt    ecx, 8                           ; load c sub carry
   sbb   r8, QWORD PTR [rbp]              ; f[0]
   setc  ch                               ; save c sub carry

   dec   r9
   test  r9, 3
   jnz   init_add_loop


; prepare for unrolled loop

   shr   r9, 2
   mov   QWORD PTR counter[rsp], r9
   jmp   short full_add_start

;  B =  B + A + C - E =  (B + C) + A - E
;  C =  C + B + D - F =  (B + C) + D - F

;  small* a = r + 3*llen;  // length rlen-3*llen
;  small* b = r + 2*llen;  // length llen
;  small* c = r + llen;    // length llen
;  small* d = r;           // length llen
;  small* e = p + llen;    // length plen-llen
;  small* f = p;           // length llen

;  small of_bp = 0, of_bm = 0, of_cp = 0, of_cm = 0, of_bc = 0;
;  while (i < llen) {
;     small of_bc = ops<small>::addc(sum_bc, b[i], c[i], of_bc);
;     of_bp = ops<small>::addc(mx, a[i], sum_bc, of_bp);
;     of_bm = ops<small>::subc(b[i], mx, e[i], of_bm);
;     of_cp = ops<small>::addc(mx, d[i], sum_bc, of_cp);
;     of_cm = ops<small>::subc(c[i], mx, f[i], of_cm);
;     ++i;
;  }

; rax = of_bc
; rbx = of_bm:of_bp
; rcx = of_cm:of_cp
; rdx = llen
; rsi = a
; rdi = d = r
; rbp = f = p
; r8  = scratch
; r9  = scratch
; r10 = scratch
; r11 = scratch
; r12 = scratch
; r13 = scratch
; r14 = scratch
; r15 = scratch

full_add_loop:
   add   rdi, 4*8
   add   rsi, 4*8
   add   rbp, 4*8

full_add_start:
   mov   r8, QWORD PTR [rdi+2*rdx]        ; b[0]
   mov   r9, QWORD PTR [rdi+2*rdx+8]      ; b[1]
   mov   r10, QWORD PTR [rdi+2*rdx+16]    ; b[2]
   mov   r11, QWORD PTR [rdi+2*rdx+24]    ; b[3]

   bt    eax, 0                           ; load bc carry
   adc   r8, QWORD PTR [rdi+rdx]          ; c[0]
   adc   r9, QWORD PTR [rdi+rdx+8]        ; c[1]
   adc   r10, QWORD PTR [rdi+rdx+16]      ; c[2]
   adc   r11, QWORD PTR [rdi+rdx+24]      ; c[3]
   setc  al                               ; save bc carry

   bt    ebx, 0                           ; load b add carry
   mov   r12, QWORD PTR [rsi]             ; a[0]
   mov   r13, QWORD PTR [rsi+8]           ; a[1]
   mov   r14, QWORD PTR [rsi+16]          ; a[2]
   mov   r15, QWORD PTR [rsi+24]          ; a[3]
   adc   r12, r8
   adc   r13, r9
   adc   r14, r10
   adc   r15, r11
   setc  bl                               ; save b add carry

   bt    ebx, 8                           ; load b sub carry
   sbb   r12, QWORD PTR [rbp+rdx]         ; e[0]
   sbb   r13, QWORD PTR [rbp+rdx+8]       ; e[1]
   sbb   r14, QWORD PTR [rbp+rdx+16]      ; e[2]
   sbb   r15, QWORD PTR [rbp+rdx+24]      ; e[3]
   setc  bh                               ; save b sub carry

   bt    ecx, 0                           ; load c add carry
   adc   r8, QWORD PTR [rdi]              ; d[0]
   adc   r9, QWORD PTR [rdi+8]            ; d[1]
   adc   r10, QWORD PTR [rdi+16]          ; d[2]
   adc   r11, QWORD PTR [rdi+24]          ; d[3]
   setc  cl                               ; save c add carry

   bt    ecx, 8                           ; load c sub carry
   sbb   r8, QWORD PTR [rbp]              ; f[0]
   sbb   r9, QWORD PTR [rbp+8]            ; f[1]
   sbb   r10, QWORD PTR [rbp+16]          ; f[2]
   sbb   r11, QWORD PTR [rbp+24]          ; f[3]
   setc  ch                               ; save c sub carry

   dec   QWORD PTR counter[rsp]
   jnz   full_add_loop






   mov   rbx, QWORD PTR rbx_save[rsp]
   mov   rsi, QWORD PTR rsi_save[rsp]
   mov   rdi, QWORD PTR rdi_save[rsp]
   mov   rbp, QWORD PTR rbp_save[rsp]
   mov   r12, QWORD PTR r12_save[rsp]
   mov   r13, QWORD PTR r13_save[rsp]
   mov   r14, QWORD PTR r14_save[rsp]
   mov   r15, QWORD PTR r15_save[rsp]

   add   rsp, stackframe

   ret   0

?karasub_asmx64@bignum@@YA_KPEA_K0PEB_K1_K@Z ENDP

_TEXT ENDS
END
