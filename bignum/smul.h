/*
 *  arbitrary length number arithmetic - fft functions helper header
 */

// $+HEADER$
// 
// Copyright 2015 Christoph Lueders
// 
// This file is part of the BIGNUM Library.
// 
// The BIGNUM Library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
// 
// The BIGNUM Library is distributed in the hope that it will be useful, but 
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public 
// License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with the BIGNUM Library.  If not, see <http://www.gnu.org/licenses/>.
// 
// $-HEADER$

// $Date: 2015-04-24 11:47:36 +0200 (Fr, 24 Apr 2015) $
// $Rev: 26592 $

// $dissect$ smul-support.txt
#ifndef CL_BIGNUM_SMUL_H
#define CL_BIGNUM_SMUL_H 1

#include "cl/int_math.h"

#pragma pack(push, 8)  // arbitrary but fixed

namespace bignum {

using namespace cl;
using namespace std;

// ------ SMUL crossover points ----------------------------------------------

struct smul_calib_item {
   index full;
   const char* before;
};

struct smul_calib_item2 {
   index start;
   index full;
   vector<byte> between;
   smul_calib_item2() : start(0), full(0) { }
   smul_calib_item2(const index _full) {
      start = full = _full;
   }
};

struct smul_calib_data {
   // table data
   index stepsz;
   vector<byte> size_to_lg;
   vector<byte> init_size_to_lg;
   // packed data
   vector<smul_calib_item2> packed_lg_sizes;
   vector<smul_calib_item2> init_packed_lg_sizes;

   smul_calib_data() {
      stepsz = 1;
   }
   smul_calib_data(const index step, const index start, const byte *data) {
      init(step, start, data);
      save_init();
   }
   smul_calib_data(const index *packed_data) {
      init_packed(packed_data);
      save_init();
   }
   smul_calib_data(const smul_calib_item *items) {
      init_packed_items(items);
      save_init();
   }
   void save_init() {
      init_size_to_lg = size_to_lg;
      init_packed_lg_sizes = packed_lg_sizes;
   }
   void reset() {
      size_to_lg = init_size_to_lg;
      packed_lg_sizes = init_packed_lg_sizes;
   }
   void init(const index _stepsz) {
      stepsz = _stepsz;
      clear_all();
   }
   void init(const index _stepsz, const index start, const byte *data) {
      init(_stepsz);
      classert((start & (_stepsz - 1)) == 0);
      size_to_lg.resize(start / stepsz);
      while (*data != (byte)-1) {
         size_to_lg.push_back(*data);
         ++data;
      }
   }
   void set_lg(const index sz, const byte lg) {
      classert((sz & (stepsz - 1)) == 0);
      if (size_to_lg.size() <= sz/stepsz) size_to_lg.resize(sz/stepsz + 1);
      size_to_lg[sz/stepsz] = lg;
      classert(get_lg(sz) == lg);
   }
   unsigned get_lg(const index sz) const;
   string print(const string& name) const;
   string pack_table(const string& name);
   void init_packed(const index* packed_data);
   void init_packed_items(const smul_calib_item* items);
   void clear_all() {
      size_to_lg.clear();
      packed_lg_sizes.clear();
   }
   void always_on() {
      clear_all();
      set_packed_lg(4, 2);
   }
   void set_packed_lg(const index sz, const unsigned lg) {
      if (packed_lg_sizes.size() <= lg) packed_lg_sizes.resize(lg+1);
      packed_lg_sizes[lg].full = sz;
   }
   string print_short(const string& name) const;
};

extern smul_calib_data smul_mod_lg;
extern smul_calib_data ssqu_mod_lg;

}  // namespace bignum

#pragma pack(pop)

#endif  // ndef CL_BIGNUM_SMUL_H
