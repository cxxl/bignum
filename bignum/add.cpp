// $+HEADER$
// 
// Copyright 2015 Christoph Lueders
// 
// This file is part of the BIGNUM Library.
// 
// The BIGNUM Library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
// 
// The BIGNUM Library is distributed in the hope that it will be useful, but 
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public 
// License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with the BIGNUM Library.  If not, see <http://www.gnu.org/licenses/>.
// 
// $-HEADER$

// $Date: 2015-04-24 11:28:16 +0200 (Fr, 24 Apr 2015) $
// $Rev: 26590 $

/*
 *  arbitrary length number arithmetic - addition
 */

#include "precomp.h"
#include "impl.h"
#include "small.h"
#include "cl/int_math.h"
#include "cl/format.h"


// we must switch off "unreachable code" warning, since bool template parameters generate code paths that are never used
#pragma warning (disable:4702)

namespace bignum {


// ------ ADDITION -----------------------------------------------------------

// mother of all adds: slow, but simple
template<typename small, bool hassign>
small add_simple(small* r, const index rmax, const small* a, const index alen, const small* b, const index blen) {
   caassert(r != nullptr);
   caassert(a != nullptr);
   caassert(b != nullptr);
   caassert(rmax >= alen);
   caassert((small)-1 > 0);  // must be unsigned type

   small of = 0;
   index i = 0;

   const bool asf = is_negative<small, hassign>(a, alen);
   const bool bsf = is_negative<small, hassign>(b, blen);
   while (i < rmax) {
      const small aa = i < alen ? a[i] : asf ? (small)-1 : 0;
      const small bb = i < blen ? b[i] : bsf ? (small)-1 : 0;
      of = ops<small>::addc(r[i], aa, bb, of);
      ++i;
   }

   return of;
}

// addition of two const input numbers 'a' and 'b' into a pre-reserved result 'r'
// 'a' must be longer than or equal the size as 'b'
// the result will be padded up to 'rmax' and must be long enough to hold the result

// can be used like tadd_ls(a, alen, a, alen, a, alen)
// length can be zero, numbers don't need to be normalised
// returns 'of' (0 or 1), which has the same meaning as in the add loop.
// 'of' DOES NOT indicate if a sign extension is needed nor the word for extension!

template<typename small, bool hassign>
small tadd_ls(small* r, index rmax, const small* a, index alen, const small* b, index blen) {
   caassert(r != nullptr);
   caassert(a != nullptr);
   caassert(b != nullptr);
   caassert(alen >= blen);
   caassert(rmax >= alen);
   caassert((small)-1 > 0);  // must be unsigned type

   small of = 0;
   index i = 0;

   // we must get the signs now, since they may be overwritten by an addition in progress later
   const bool asf = is_negative<small, hassign>(a, alen);
   const bool bsf = is_negative<small, hassign>(b, blen);

#ifdef BN_ADD_USE_FAST
   // use the loop-unrolled or asm version
   of = add_fast(r, a, b, blen);
   i = blen;
#else
   // sum up the length where we have two values
   while (i < blen) {
      of = ops<small>::addc(r[i], a[i], b[i], of);
      ++i;
   }
#endif
   caassert(of <= 1);

   // continue to work on 'a' only while propagating overflow and honouring 'b's sign
   caassert(i == blen);
   if (bsf) {
      caassert(hassign);
      while (i < alen) {
         of = ops<small>::addcm1(r[i], a[i], of);
         ++i;
      }
   }
   else {
      while (of && i < alen) {
         of = ops<small>::addc1(r[i], a[i]);
         ++i;
      }
      while (i < alen)
         r[i++] = a[i];
   }

   // propagate overflow and honour signs
   if (i < rmax) {
      caassert(i == alen);
      if (asf != bsf) {
         caassert(hassign);
         if (of == 0) one_in(r, rmax, i);  // no sign flip: result is still negative
         else zero_in(r, rmax, i);
      }
      else if (asf == false) {  // implies bsf == false
         caassert(bsf == false);
         r[i++] = of;
         zero_in(r, rmax, i);
         of = 0;
      }
      else {  // asf == true && bsf == true
         caassert(asf == true && bsf == true);
         caassert(hassign);
         r[i++] = (small)-2 + of;  // 0xffff + 0xffff + (0 oder 1) == 0x1fffe oder 0x1ffff
         one_in(r, rmax, i);
         of = 1;
      }
   }

   return of;
}


// manually unrolled version of the first add loop on small's.
// returns the carry.
// length can be zero, numbers don't need to be normalised

template<typename small>
small __fastcall add_unrolled(small* r, const small* a, const small* b, const index len) {
   caassert(r != 0);
   caassert(a != 0);
   caassert(b != 0);

   small of = 0;
   index i = 0;

#define s(o) of = ops<small>::addc(r[i+(o)], a[i+(o)], b[i+(o)], of)
   UNROLL8(i, len, 0, l, s);
#undef s

   return of;
}


// addition of one number in-place (destructively) on another
// same rules as for tadd_ls()

template<typename small, bool hassign>
small tadd_on(small* a, index alen, const small* b, index blen) {
   caassert(a != nullptr);
   caassert(b != nullptr);
   caassert(alen >= blen);
   caassert((small)-1 > 0);  // must be unsigned type

   small of = 0;
   index i = 0;

   /*
   // slow, but simple version:
   const bool bsf = is_negative<small, hassign>(b, blen);
   while (i < alen) {
      const small bb = i < blen ? b[i] : bsf ? (small)-1 : 0;
      of = ops<small>::addc(a[i], a[i], bb, of);
      ++i;
   }
   */

#ifdef BN_ADD_USE_FAST
   // use the loop-unrolled or asm version
   of = add_fast(a, a, b, blen);
   i = blen;
#else
   while (i < blen) {
      of = ops<small>::addc(a[i], a[i], b[i], of);
      ++i;
   }
#endif

   const bool bsf = is_negative<small, hassign>(b, blen);
   if (bsf) {
      caassert(hassign);
      while (i < alen) {
         of = ops<small>::addcm1(a[i], a[i], of);
         ++i;
      }
   }
   else {
      while (of && i < alen) {
         of = ops<small>::addc1(a[i], a[i]);
         ++i;
      }
   }

   return of;
}

#ifdef CL_TEST_CODE
static void test_add_on() {
   {
      uint16 a[] = { 2 };
      uint16 b[] = { 1 };
      uint16 r = addu_on(a, NBEL(a), b, NBEL(b));
      classert(r == 0);
   }
   {
      uint16 a[] = { 32768 };
      uint16 b[] = { 32768 };
      uint16 r = addu_on(a, NBEL(a), b, NBEL(b));
      classert(r == 1);
   }
}
#endif


// ------ ADDITION OF SHIFTED NUMBER ------------------------------------------

template<typename small>
small addu_on_shl(small* a, index alen, const small* b, const index blen, const bit_index shift_count) {
   caassert(a != nullptr);
   caassert(b != nullptr);
   const index off = shift_count / bits(small);
   caassert(alen >= blen + off);
   //classert(bits(small) * alen >= bit_length(b, blen) + shift_count);
   caassert((small)-1 > 0);  // must be unsigned type

   const unsigned sh1 = shift_count % bits(small);
   a += off;
   alen -= off;
   // improve: handle byte-aligned adds as well
   if (sh1 == 0)
      return addu_on(a, alen, b, blen);

   small of = 0;
   index i = 0;
   const unsigned sh2 = bits(small) - sh1;

   while (i < blen) {
      classert(i < alen);
      of = ops<small>::addc_shl(a[i], a[i], b[i], sh1, sh2, of);
      ++i;
   }
   if (of && i < alen) {
      // 'of' can contain more than one bit here!
      of = ops<small>::addc(a[i], a[i], of);
      ++i;
      // here it's max 1 bit
      while (of && i < alen) {
         caassert(of == 1);
         of = ops<small>::addc1(a[i], a[i]);
         ++i;
      }
   }
   classert(i <= alen);

   return of;
}

#ifdef CL_TEST_CODE
void test_addu_on_shl() {
   {
      // not shifted at all
      uint8 a[] = { 2, 3, 5, 7 };
      uint8 b[] = { 11, 13 };
      addu_on_shl(a, NBEL(a), b, NBEL(b), 0);
      uint8 rc[] = { 2+11, 3+13, 5, 7 };
      classert(memcmp(a, rc, sizeof(a)) == 0);
   }
   {
      // shifted by one small
      uint8 a[] = { 2, 3, 5, 7 };
      uint8 b[] = { 11, 13 };
      addu_on_shl(a, NBEL(a), b, NBEL(b), bits(uint8));
      uint8 rc[] = { 2, 3+11, 5+13, 7 };
      classert(memcmp(a, rc, sizeof(a)) == 0);
   }
   {
      // shifted by 1
      uint8 a[] = { 2, 3, 5, 7 };
      uint8 b[] = { 11, 13 };
      addu_on_shl(a, NBEL(a), b, NBEL(b), 1);
      uint8 rc[] = { 2+11*2, 3+13*2, 5, 7 };
      classert(memcmp(a, rc, sizeof(a)) == 0);
   }
   {
      // shifted by 7
      uint8 a[] = { 2, 3, 5, 7 };
      uint8 b[] = { 11, 13 };
      addu_on_shl(a, NBEL(a), b, NBEL(b), 7);
      uint8 rc[] = { 2+0x80, 3+11/2+0x80, 5+13/2, 7 };
      classert(memcmp(a, rc, sizeof(a)) == 0);
   }
   {
      // shifted by 12
      uint8 a[] = { 2, 3, 5, 7 };
      uint8 b[] = { 11, 13 };
      addu_on_shl(a, NBEL(a), b, NBEL(b), 12);
      uint8 rc[] = { 2, 3+11*16, 5+13*16, 7 };
      classert(memcmp(a, rc, sizeof(a)) == 0);
   }
#ifdef _M_X64
   {
      // not shifted at all
      uint64 a[] = { 2, 3, 5, 7 };
      uint64 b[] = { 11, 13 };
      addu_on_shl(a, NBEL(a), b, NBEL(b), 0);
      uint64 rc[] = { 2+11, 3+13, 5, 7 };
      classert(memcmp(a, rc, sizeof(a)) == 0);
   }
   {
      // shifted by one small
      uint64 a[] = { 2, 3, 5, 7 };
      uint64 b[] = { 11, 13 };
      addu_on_shl(a, NBEL(a), b, NBEL(b), bits(uint64));
      uint64 rc[] = { 2, 3+11, 5+13, 7 };
      classert(memcmp(a, rc, sizeof(a)) == 0);
   }
   {
      // shifted by 1
      uint64 a[] = { 2, 3, 5, 7 };
      uint64 b[] = { 11, 13 };
      addu_on_shl(a, NBEL(a), b, NBEL(b), 1);
      uint64 rc[] = { 2+11*2, 3+13*2, 5, 7 };
      classert(memcmp(a, rc, sizeof(a)) == 0);
   }
   {
      // shifted by 63
      uint64 a[] = { 2, 3, 5, 7 };
      uint64 b[] = { 11, 13 };
      addu_on_shl(a, NBEL(a), b, NBEL(b), 63);
      uint64 rc[] = { 2+0x8000000000000000, 3+11/2+0x8000000000000000, 5+13/2, 7 };
      classert(memcmp(a, rc, sizeof(a)) == 0);
   }
   {
      // shifted by 68
      uint64 a[] = { 2, 3, 5, 7 };
      uint64 b[] = { 11, 13 };
      addu_on_shl(a, NBEL(a), b, NBEL(b), 68);
      uint64 rc[] = { 2, 3+11*16, 5+13*16, 7 };
      classert(memcmp(a, rc, sizeof(a)) == 0);
   }
#endif
}
#endif


// ------ SELFTEST ------------------------------------------------------------

template<typename small, bool hassign>
static dword selftest_add1(int64 p, int64 q) {
   small a[sizeof(uint64) / sizeof(small) + 1];
   small b[sizeof(uint64) / sizeof(small) + 1];
   small r[sizeof(uint64) / sizeof(small) + 1];
   // trash contents
   memset((void*)a, 0xcc, sizeof(a));
   memset((void*)b, 0xcc, sizeof(b));
   memset((void*)r, 0xcc, sizeof(r));
   *(int64*)a = p;
   *(int64*)b = q;
   const unsigned pbits = tmax<unsigned>((p < 0 ? bit_width(-p) : bit_width(p)) + (hassign ? 1 : 0), 1);
   const unsigned qbits = tmax<unsigned>((q < 0 ? bit_width(-q) : bit_width(q)) + (hassign ? 1 : 0), 1);
   const index rmax = (pbits + bits(small)) / bits(small);
   const index alen = (pbits + bits(small) - 1) / bits(small);
   const index blen = (qbits + bits(small) - 1) / bits(small);

   tadd_ls<small, hassign>(r, rmax, a, alen, b, blen);
   if (*(byte*)&r[rmax] != 0xcc) {
      classertm(0, "data after trashed");
      return 1;  // data after trashed
   }
   const uint64 mask = (1ui64 << (rmax * bits(small))) - 1;
   int64 rr = *(int64*)r;
   if (mask != 0) {
      rr &= mask;  // mask out unused parts
      if (hassign) {
         const uint64 sf = 1ui64 << (rmax * bits(small) - 1);
         if (rr & sf)  // signed result?
            rr |= ~mask;  // extend sign
      }
   }
   const int64 sum = p + q;
   if (rr != sum) {
      classertm(0, "result does not match");
      return 2;  // result mismatch
   }
   return 0;  // all cool
}

template<typename small>
dword selftest_add() {
   const int64 max = (0ui64 - 1) >> 2;
   classert(max > 0);  // must be signed
   // fibonacci series
   int64 p = 0, q = 1;
   unsigned round = 0;
   while (q < max) {
      dword r;
      if ((r = selftest_add1<small, true>(p, -p)) != 0)
         return round * 0x100 + r;
      if ((r = selftest_add1<small, true>(q, p)) != 0)
         return round * 0x100 + 0x10 + r;
      if ((r = selftest_add1<small, true>(q, -p)) != 0)
         return round * 0x100 + 0x20 + r;
      if ((r = selftest_add1<small, true>(-q, p)) != 0)
         return round * 0x100 + 0x30 + r;
      if ((r = selftest_add1<small, true>(-q, -p)) != 0)
         return round * 0x100 + 0x40 + r;
      if ((r = selftest_add1<small, false>(q, p)) != 0)
         return round * 0x100 + 0x50 + r;
      // iterate
      const int64 t = p + q;
      p = q;
      q = t;
      ++round;
   }
   return 0;
}

#ifdef CL_TEST_CODE
static void test_add1(const int64 a, const int64 b, int rsign) {
   // use 16 bits, since it's easier to test
   uint16 aa[] = { 0, 0, 0, 0 };
   uint16 bb[] = { 0, 0, 0, 0 };
   uint16 rr[] = { 1, 2, 3, 4, 5 };

   const int64 r = a + b;
   classert(a > -(int64)0xffffffff && a < 0xffffffff);
   classert(b > -(int64)0xffffffff && b < 0xffffffff);
   classert(r > -(int64)0xffffffff && r < 0xffffffff);
   classert(rsign > 0 ? r > 0 : rsign < 0 ? r < 0 : r == 0);

   *(int64*)aa = a;
   *(int64*)bb = b;

   addi_ls<uint16>(rr, 4, aa, 2, bb, 2);  // test with same size
   int64 rx = *(int64*)rr;
   classert(rx == r);
   classert(rr[4] == 5);  // untouched

   addi_ls<uint16>(rr, 4, aa, 3, bb, 2);  // test with a longer than b
   rx = *(int64*)rr;
   classert(rx == r);
   classert(rr[4] == 5);  // untouched
}

template<typename t1, typename t2>
static dword test_add_fib2(index bytes) {
   dword ret = 0;
   const index l1 = bytes / sizeof(t1);
   t1* a1 = bignum_alloc<t1>(l1);
   zero(a1, l1);
   a1[0] = 1;
   t1* b1 = bignum_alloc<t1>(l1);
   zero(b1, l1);
   t1* r1 = bignum_alloc<t1>(l1);

   const index l2 = bytes / sizeof(t2);
   t2* a2 = bignum_alloc<t2>(l2);
   zero(a2, l2);
   a2[0] = 1;
   t2* b2 = bignum_alloc<t2>(l2);
   zero(b2, l2);
   t2* r2 = bignum_alloc<t2>(l2);

   int rounds = 0;
   do {
      classert(memcmp(a1, a2, bytes) == 0);
      classert(memcmp(b1, b2, bytes) == 0);
      addu(r1, l1, a1, l1, b1, l1);
      addu(r2, l2, a2, l2, b2, normu(b2, l2));  // make sure we test alen != blen as well
      for (unsigned i=0; i<bytes; ++i) {
         if (((byte*)r1)[i] != ((byte*)r2)[i]) {
            classertm(0, "results don't match!");
            ret = 1;
            goto out;
         }
      }
      // r = a + b
      memcpy(b1, a1, bytes);  // b = a
      memcpy(b2, a2, bytes);
      memcpy(a1, r1, bytes);  // a = r
      memcpy(a2, r2, bytes);
      ++rounds;
   } while (a1[l1-1] == 0 && a2[l2-1] == 0);
   classert(rounds >= bytes * 8);

out:
   bignum_free(a1);
   bignum_free(b1);
   bignum_free(r1);
   bignum_free(a2);
   bignum_free(b2);
   bignum_free(r2);

   return ret;
}

template<typename t1, typename t2>
static dword test_add_on_fib(index bytes) {
   dword ret = 0;
   const index l1 = bytes / sizeof(t1);
   t1* a1 = bignum_alloc<t1>(l1);
   zero(a1, l1);
   a1[0] = 1;
   t1* b1 = bignum_alloc<t1>(l1);
   zero(b1, l1);
   t1* r1 = bignum_alloc<t1>(l1);

   const index l2 = bytes / sizeof(t2);
   t2* a2 = bignum_alloc<t2>(l2);
   zero(a2, l2);
   a2[0] = 1;
   t2* b2 = bignum_alloc<t2>(l2);
   zero(b2, l2);
   t2* r2 = bignum_alloc<t2>(l2);

   int rounds = 0;
   do {
      classert(memcmp(a1, a2, bytes) == 0);
      classert(memcmp(b1, b2, bytes) == 0);
      memcpy(r1, a1, bytes);
      memcpy(r2, a2, bytes);
      addu_on(r1, l1, b1, l1);
      addu_on(r2, l2, b2, normu(b2, l2));  // make sure we test alen != blen as well
      for (unsigned i=0; i<bytes; ++i) {
         if (((byte*)r1)[i] != ((byte*)r2)[i]) {
            classertm(0, "results don't match!");
            ret = 1;
            goto out;
         }
      }
      // r = a + b
      memcpy(b1, a1, bytes);  // b = a
      memcpy(b2, a2, bytes);
      memcpy(a1, r1, bytes);  // a = r
      memcpy(a2, r2, bytes);
      ++rounds;
   } while (a1[l1-1] == 0 && a2[l2-1] == 0);
   classert(rounds >= bytes * 8);

out:
   bignum_free(a1);
   bignum_free(b1);
   bignum_free(r1);
   bignum_free(a2);
   bignum_free(b2);
   bignum_free(r2);

   return ret;
}

#if defined _M_X86 && defined BN_ADD_USE_ASMX86
static void test_add_asmx86() {
   uint32 a[40];
   uint32 b[40];
   uint32 r1[41];
   uint32 r2[41];
   a[0] = 0x1f1f1f1f;
   for (unsigned i=1; i<NBEL(a)-1; ++i) a[i] = 0x11111100 + i;
   b[0] = 0x2f2f2f2f;
   for (unsigned i=1; i<NBEL(b)-1; ++i) b[i] = 0x22222200 + i;

   // a full 8-unrolled round
   memset(r1, 0xcc, sizeof(r1));
   memset(r2, 0xcc, sizeof(r2));
   uint32 c1 = add_unrolled(r1+1, a+1, b+1, 16);
   uint32 c2 = add_asmx86(r2+1, a+1, b+1, 16);
   classert(c1 == c2);
   classert(memcmp(r1, r2, sizeof(r1)) == 0);

   // just part of it
   memset(r1, 0xcc, sizeof(r1));
   memset(r2, 0xcc, sizeof(r2));
   c1 = add_unrolled(r1+1, a+1, b+1, 12);
   c2 = add_asmx86(r2+1, a+1, b+1, 12);
   classert(c1 == c2);
   classert(memcmp(r1, r2, sizeof(r1)) == 0);

   // one word uneven
   memset(r1, 0xcc, sizeof(r1));
   memset(r2, 0xcc, sizeof(r2));
   c1 = add_unrolled(r1+1, a+1, b+1, 11);
   c2 = add_asmx86(r2+1, a+1, b+1, 11);
   classert(c1 == c2);
   classert(memcmp(r1, r2, sizeof(r1)) == 0);

   // two full 8-unrolled rounds
   memset(r1, 0xcc, sizeof(r1));
   memset(r2, 0xcc, sizeof(r2));
   c1 = add_unrolled(r1+1, a+1, b+1, 32);
   c2 = add_asmx86(r2+1, a+1, b+1, 32);
   classert(c1 == c2);
   classert(memcmp(r1, r2, sizeof(r1)) == 0);

   // two full and some 8-unrolled rounds
   memset(r1, 0xcc, sizeof(r1));
   memset(r2, 0xcc, sizeof(r2));
   c1 = add_unrolled(r1+1, a+1, b+1, 36);
   c2 = add_asmx86(r2+1, a+1, b+1, 36);
   classert(c1 == c2);
   classert(memcmp(r1, r2, sizeof(r1)) == 0);
}
#endif

#if defined _M_X64 && defined BN_ADD_USE_ASMX64
static void test_add_asmx64(uint64 (*add_func)(uint64* r, const uint64* a, const uint64* b, const uint64 len)) {
   uint64 a[40];
   uint64 b[40];
   uint64 r1[41];
   uint64 r2[41];
   a[0] = 0x1f1f1f1f1f1f1f1f;
   for (unsigned i=1; i<NBEL(a)-1; ++i) a[i] = 0xf111111111111100 + i;
   b[0] = 0x2f2f2f2f2f2f2f2f;
   for (unsigned i=1; i<NBEL(b)-1; ++i) b[i] = 0xf222222222222200 + i;

   // a full 8-unrolled round
   memset(r1, 0xcc, sizeof(r1));
   memset(r2, 0xcc, sizeof(r2));
   uint64 c1 = add_unrolled(r1+1, a+1, b+1, 16);
   uint64 c2 = add_func(r2+1, a+1, b+1, 16);
   classert(c1 == c2);
   classert(memcmp(r1, r2, sizeof(r1)) == 0);

   // just part of it
   memset(r1, 0xcc, sizeof(r1));
   memset(r2, 0xcc, sizeof(r2));
   c1 = add_unrolled(r1+1, a+1, b+1, 12);
   c2 = add_func(r2+1, a+1, b+1, 12);
   classert(c1 == c2);
   classert(memcmp(r1, r2, sizeof(r1)) == 0);

   // one word uneven
   memset(r1, 0xcc, sizeof(r1));
   memset(r2, 0xcc, sizeof(r2));
   c1 = add_unrolled(r1+1, a+1, b+1, 11);
   c2 = add_func(r2+1, a+1, b+1, 11);
   classert(c1 == c2);
   classert(memcmp(r1, r2, sizeof(r1)) == 0);

   // two full 8-unrolled rounds
   memset(r1, 0xcc, sizeof(r1));
   memset(r2, 0xcc, sizeof(r2));
   c1 = add_unrolled(r1+1, a+1, b+1, 32);
   c2 = add_func(r2+1, a+1, b+1, 32);
   classert(c1 == c2);
   classert(memcmp(r1, r2, sizeof(r1)) == 0);

   // two full and some 8-unrolled rounds
   memset(r1, 0xcc, sizeof(r1));
   memset(r2, 0xcc, sizeof(r2));
   c1 = add_unrolled(r1+1, a+1, b+1, 36);
   c2 = add_func(r2+1, a+1, b+1, 36);
   classert(c1 == c2);
   classert(memcmp(r1, r2, sizeof(r1)) == 0);
}
#endif


template<typename small>
static void test_ops_add() {
   small r;
   small of = ops<small>::addc(r, 0, 0, 0);
   classert(r == 0);
   classert(of == 0);

   of = ops<small>::addc(r, 5, 8, 0);
   classert(r == 13);
   classert(of == 0);

   of = ops<small>::addc(r, 5, -1, 0);
   classert(r == 4);
   classert(of == 1);

   of = ops<small>::addc(r, -1, -1, 0);
   classert(r == small(-2));
   classert(of == 1);

   of = ops<small>::addc(r, -1, 0, 0);
   classert(r == small(-1));
   classert(of == 0);

   of = ops<small>::addc(r, -1, 0, 1);
   classert(r == 0);
   classert(of == 1);

   of = ops<small>::addc(r, -1, -1, 1);
   classert(r == small(-1));
   classert(of == 1);

   // addc1()
   of = ops<small>::addc1(r, 5);
   classert(r == 6);
   classert(of == 0);

   of = ops<small>::addc1(r, (small)-1);
   classert(r == 0);
   classert(of == 1);

   // addcm1()
   of = ops<small>::addcm1(r, 5);
   classert(r == 4);
   classert(of == 1);

   of = ops<small>::addcm1(r, 0);
   classert(r == (small)-1);
   classert(of == 0);

   of = ops<small>::addcm1(r, 5, 2);
   classert(r == 6);
   classert(of == 1);

   of = ops<small>::addcm1(r, 1, 1);
   classert(r == 1);
   classert(of == 1);

   of = ops<small>::addcm1(r, 1, 0);
   classert(r == 0);
   classert(of == 1);

   of = ops<small>::addcm1(r, 0, 1);
   classert(r == 0);
   classert(of == 1);

   of = ops<small>::addcm1(r, 0, 0);
   classert(r == (small)-1);
   classert(of == 0);
}

static void _CL_CALL test_add2() {
   // use 16 bits, since it's easier to test
   uint16 b[] = { 0, 0, 0 };
   uint16 a[] = { 0, 0, 0 };
   uint16 r[] = { 1, 2, 3, 4, 5, 6, 7 };

   // 0 + 0 == 0
   addi_ls<uint16>(r, 2, a, 1, b, 1);
   classert(r[0] == 0);
   classert(r[1] == 0);
   classert(r[2] == 3);  // untouched

   // large rmax: 2 + 1 == 3
   a[0] = 2;
   b[0] = 1;
   addi_ls<uint16>(r, 3, a, 1, b, 1);
   classert(r[0] == 3);
   classert(r[1] == 0);
   classert(r[2] == 0);
   classert(r[3] == 4);  // untouched

   // a longer than b: 0x10002 + 7 == 0x10009
   a[0] = 2;
   a[1] = 1;
   b[0] = 7;
   addi_ls<uint16>(r, 3, a, 2, b, 1);
   classert(r[0] == 9);
   classert(r[1] == 1);
   classert(r[2] == 0);
   classert(r[3] == 4);  // untouched

   // overflow: 0x8222 + 0x8111 == 0x10333
   a[0] = 0x8222;
   a[1] = 0;
   b[0] = 0x8111;
   b[1] = 0;
   addi_ls<uint16>(r, 3, a, 2, b, 2);
   classert(r[0] == 0x333);
   classert(r[1] == 1);
   classert(r[2] == 0);
   classert(r[3] == 4);  // untouched

   // negative input: 7 + -2 == 5
   a[0] = 7;
   b[0] = -2;
   r[2] = 3;
   addi_ls<uint16>(r, 2, a, 1, b, 1);
   classert(r[0] == 5);
   classert(r[1] == 0);
   classert(r[2] == 3);  // untouched

   // negative input: -2 + 7 == 5
   a[0] = -2;
   b[0] = 7;
   addi_ls<uint16>(r, 2, a, 1, b, 1);
   classert(r[0] == 5);
   classert(r[1] == 0);
   classert(r[2] == 3);  // untouched

   // negative output: -10 + 7 == -3
   a[0] = -10;
   b[0] = 7;
   addi_ls<uint16>(r, 2, a, 1, b, 1);
   classert(r[0] == (uint16)-3);
   classert(r[1] == (uint16)-1);
   classert(r[2] == 3);  // untouched

   // negative output: 7 + -10 == -3
   a[0] = 7;
   b[0] = -10;
   addi_ls<uint16>(r, 2, a, 1, b, 1);
   classert(r[0] == (uint16)-3);
   classert(r[1] == (uint16)-1);
   classert(r[2] == 3);  // untouched

   // negative input, a extended: -2 + 7 == 5
   a[0] = -2;
   a[1] = -1;
   b[0] = 7;
   addi_ls<uint16>(r, 3, a, 2, b, 1);
   classert(r[0] == 5);
   classert(r[1] == 0);
   classert(r[2] == 0);
   classert(r[3] == 4);  // untouched

   // negative input, a extended: 7 + -2 == 5
   a[0] = 7;
   a[1] = 0;
   b[0] = -2;
   addi_ls<uint16>(r, 3, a, 2, b, 1);
   classert(r[0] == 5);
   classert(r[1] == 0);
   classert(r[2] == 0);
   classert(r[3] == 4);  // untouched

   // both negative, a extended: -7 + -2 == -9
   a[0] = -7;
   a[1] = -1;
   b[0] = -2;
   addi_ls<uint16>(r, 3, a, 2, b, 1);
   classert(r[0] == (uint16)-9);
   classert(r[1] == (uint16)-1);
   classert(r[2] == (uint16)-1);
   classert(r[3] == 4);  // untouched

   // negative output, a extended: -10 + 7 == -3
   a[0] = -10;
   a[1] = -1;
   b[0] = 7;
   addi_ls<uint16>(r, 3, a, 2, b, 1);
   classert(r[0] == (uint16)-3);
   classert(r[1] == (uint16)-1);
   classert(r[2] == (uint16)-1);
   classert(r[3] == 4);  // untouched

   // negative output, a extended: 7 + -10 == -3
   a[0] = 7;
   a[1] = 0;
   b[0] = -10;
   addi_ls<uint16>(r, 3, a, 2, b, 1);
   classert(r[0] == (uint16)-3);
   classert(r[1] == (uint16)-1);
   classert(r[2] == (uint16)-1);
   classert(r[3] == 4);  // untouched

   // negative output, a extended: -7 + -10 == -17
   a[0] = -7;
   a[1] = -1;
   b[0] = -10;
   addi_ls<uint16>(r, 3, a, 2, b, 1);
   classert(r[0] == (uint16)-17);
   classert(r[1] == (uint16)-1);
   classert(r[2] == (uint16)-1);
   classert(r[3] == 4);  // untouched

   // negative output, a extended: 7 + -10 == -3
   a[0] = 7;
   a[1] = 0;
   b[0] = -10;
   addi_ls<uint16>(r, 3, a, 2, b, 1);
   classert(r[0] == (uint16)-3);
   classert(r[1] == (uint16)-1);
   classert(r[2] == (uint16)-1);
   classert(r[3] == 4);  // untouched

   // negative input, both extended: 7 + -2 == 5
   a[0] = 7;
   a[1] = 0;
   b[0] = -2;
   b[1] = -1;
   r[3] = 4;
   addi_ls<uint16>(r, 3, a, 2, b, 2);
   classert(r[0] == 5);
   classert(r[1] == 0);
   classert(r[2] == 0);
   classert(r[3] == 4);  // untouched

   // negative input, both extended: 7 + -2 == 5
   a[0] = -2;
   a[1] = -1;
   b[0] = 7;
   b[1] = 0;
   r[3] = 4;
   addi_ls<uint16>(r, 3, a, 2, b, 2);
   classert(r[0] == 5);
   classert(r[1] == 0);
   classert(r[2] == 0);
   classert(r[3] == 4);  // untouched

   // negative output, both extended: -10 + 7 == -3
   a[0] = -10;
   a[1] = -1;
   b[0] = 7;
   b[1] = 0;
   r[3] = 4;
   addi_ls<uint16>(r, 3, a, 2, b, 2);
   classert(r[0] == (uint16)-3);
   classert(r[1] == (uint16)-1);
   classert(r[2] == (uint16)-1);
   classert(r[3] == 4);  // untouched

   // negative output, both extended: -10 + 7 == -3
   a[0] = 7;
   a[1] = 0;
   b[0] = -10;
   b[1] = -1;
   r[3] = 4;
   addi_ls<uint16>(r, 3, a, 2, b, 2);
   classert(r[0] == (uint16)-3);
   classert(r[1] == (uint16)-1);
   classert(r[2] == (uint16)-1);
   classert(r[3] == 4);  // untouched

   // output overflow must not trash: 0x8002 + 0x8001 == 3
   a[0] = 0x8002;
   b[0] = 0x8001;
   r[1] = 2;
   addi_ls<uint16>(r, 1, a, 1, b, 1);
   classert(r[0] == 3);
   classert(r[1] == 2);  // untouched

   // output overflow must not trash: -1 + 0x8002 == 0x8001
   a[0] = -1;
   b[0] = 0x8002;
   r[1] = 2;
   addi_ls<uint16>(r, 1, a, 1, b, 1);
   classert(r[0] == 0x8001);
   classert(r[1] == 2);  // untouched

   // both numbers have no sign bit, but the result has its highest bit in the first small set
   a[0] = 0x452f;
   b[0] = 0x6ff1;
   r[2] = 3;
   addi_ls<uint16>(r, 2, a, 1, b, 1);
   classert(r[0] == 0xb520);
   classert(r[1] == 0);
   classert(r[2] == 3);  // untouched


   // ------ no overflow ------
   // --- result > 0 ---
   test_add1(10, 7, 1);      // a > 0, b > 0
   test_add1(10, -7, 1);      // a > 0, b < 0
   test_add1(-3, 7, 1);       // a < 0, b > 0
   // a < 0, b < 0: doesn't exist

   // --- result < 0 ---
   // a > 0, b > 0: doesn't exist
   test_add1(3, -7, -1);      // a > 0, b < 0
   test_add1(-10, 7, -1);     // a < 0, b > 0
   test_add1(-4, -7, -1);    // a < 0, b < 0

   // ------ overflow ------
   // --- result > 0
   test_add1(0x8001, 0x8002, 1);    // a > 0, b > 0
   test_add1(0x10001, -(int64)0x8001, 1);  // a > 0, b < 0
   test_add1(-2, 0x8002, 1);               // a < 0, b > 0
   // a < 0, b < 0: doesn't exist

   // --- result < 0
   // a > 0, b > 0: doesn't exist
   test_add1(3, -5, -1);   // a > 0, b < 0
   test_add1(-5, 3, -1);   // a < 0, b > 0
   test_add1(-3, -5, -1);  // a < 0, b < 0
}

static void _CL_CALL test_add3() {
   uint8 t[] = { 0x9c, 0xc7, 1, 0 };
   uint8 r[] = { 0xf6, 0xff, 0xff, 0 };
   addi(r, 4, t, 4, r, 3);
   string s = dump_hex(r, 4);
   classert(s == "0x1c792");
}


// ------ add test matrix -----------------------------------------------------

struct add_tester {
   bool sign;
   byte a;
   byte b;
   byte r0;
   byte r1;
   byte of;
   bool need_ex;
};

static const add_tester mm[] = {
   // unsigned tests, easy
   { false,   1,   2,    3,  0, 0, false },
   { false, 127, 127,  254,  0, 0, false },
   { false, 255, 255,  254,  1, 0, true  },
   // signed tests, not so easy
   { true,    1,   2,    3,  0, 0, false },     // + + +
   { true,  127, 127,  254,  0, 0, true  },     // + + -
   { true,   -1,   2,    1,  0, 1, false },     // - + +
   { true,    2,  -1,    1,  0, 1, false },     // + - +
   { true,   -2,   1,   -1, -1, 0, false },     // + - -
   { true,    1,  -2,   -1, -1, 0, false },     // - + -
   { true,   -1,  -1,   -2, -1, 1, false },     // - - -
   { true, -127,-127, -254, -1, 1, true  },     // - - +
};

struct add_tester_length {
   index alen;
   index blen;
   index rlen;
};

static const add_tester_length ll[] = {
   { 1, 1, 2 },
   { 2, 1, 3 },
   { 2, 2, 3 },
   { 3, 1, 4 },

   { 1, 1, 1 },
   { 2, 1, 2 },
   { 2, 2, 2 },
   { 3, 1, 3 },
};

static void test_add_matrix() {
   typedef int64 cint;
   const unsigned maxlen = sizeof(cint);
   byte of;
   byte a[maxlen];
   byte b[maxlen];
   byte r[maxlen];
   byte cr[maxlen];
   string s;
   strformat ss(s);
   bool do_print = false;

   for (unsigned j=0; j<NBEL(ll); ++j) {
      index alen = ll[j].alen;
      index blen = ll[j].blen;
      index rlen = ll[j].rlen;
      classert(alen <= maxlen);
      classert(blen <= maxlen);
      classert(rlen <= maxlen);
      if (do_print) DTOUT_PUTS(format("alen=%? blen=%? rlen=%?\n") << alen << blen << rlen);

      for (unsigned i=0; i<NBEL(mm); ++i) {
         const add_tester& m = mm[i];
         auto add = m.sign ? addi<byte> : addu<byte>;
         //auto add = m.sign ? add_simple<byte, true> : add_simple<byte, false>;
         auto addon = m.sign ? addi_on<byte> : addu_on<byte>;
         auto comp = m.sign ? compi_nn<byte> : compu_nn<byte>;
         auto signx = m.sign ? tsignx<byte, true> : tsignx<byte, false>;
         bool asign, bsign, crsign, rsign;

         for (unsigned k=0; k<2; ++k) {
            s.clear();
            a[0] = m.a;
            signx(a, 1, maxlen);
            asign = m.sign ? is_negative<byte, true>(a, maxlen) : false;
            classert(asign == (*(cint*)&a < 0));
            b[0] = m.b;
            signx(b, 1, maxlen);
            bsign = m.sign ? is_negative<byte, true>(b, maxlen) : false;
            classert(bsign == (*(cint*)&b < 0));
            cr[0] = m.r0;
            cr[1] = m.r1;
            signx(cr, 2, maxlen);
            crsign = m.sign ? is_negative<byte, true>(cr, maxlen) : false;
            classert(crsign == (*(cint*)&cr < 0));
            char* sign_char = m.sign ? "d" : "u";
            string fmt = format("%?signed %%%? %? %%%?:\n  correct    %%%?, of=%%?\n") <<  (m.sign ? "" : "un") << sign_char << (k == 0 ? "+" : "+=") << sign_char << sign_char;
            ss(fmt.c_str()) << *(cint*)&a << *(cint*)&b << *(cint*)&cr << m.of;
            if (do_print) DTOUT_PUTS(s);
            s.clear();
            if (s == "1 + -2")
               NOP();
            if (k == 0) {
               // addition: r = a + b
               of = add(r, rlen, a, alen, b, blen);
            }
            else {
               // addition: r = a, r += b
               memcpy(r, a, sizeof(r));
               of = addon(r, rlen, b, blen);
            }
            rsign = m.sign ? is_negative<byte, true>(r, rlen) : false;
            if (!m.need_ex) {
               signx(r, rlen, maxlen);
               ss(m.sign ? "  short   => %d, of=%?\n" :  "  short   => %u, of=%?\n") << *(cint*)&r << of;
               if (do_print) DTOUT_PUTS(s);
               s.clear();
               classert(comp(r, maxlen, cr, maxlen) == 0);
               classert(of == m.of);
               classert((asign == bsign) == (rsign == (of != 0)));  // that's the rule
               classert(need_signx(asign, bsign, of, rsign) == false);
            }
            if (rlen < maxlen) {
               // calculate proper high word from inputs and 'of' return
               bool need_sx = need_signx(asign, bsign, of, rsign);
               signx(r, rlen, maxlen);
               cint old_r = *(cint*)&r;
               r[rlen] = add_of_word(asign, bsign, of);
               rsign = m.sign ? is_negative<byte, true>(r, rlen+1) : false;
               signx(r, rlen+1, maxlen);
               ss(m.sign ? "  of_word => %d, of=%?\n" :  "  of_word => %u, of=%?\n") << *(cint*)&r << of;
               if (do_print) DTOUT_PUTS(s);
               s.clear();
               classert(comp(r, maxlen, cr, maxlen) == 0);
               classert(of == m.of);
               classert((asign == bsign) == (rsign == (of != 0)));  // that's the rule
               classert(need_signx(asign, bsign, of, rsign) == false);
               if (need_sx) classert(*(cint*)&r != old_r);  // make sure need_signx works
            }
         }
      }
      if (do_print) DTOUT_PUTS("\n");
   }
}
#endif


// ------ estimate add() time -------------------------------------------------

// use these timings:

// CPU: Intel(R) Core(TM)2 Quad CPU Q9550 @ 2.83GHz, CPUID: 06_17h
// clock speed is about 2.833 GHz
// small has 64 bits
//
// benchmarking addu(n,n):
//  size/u  mem/b rounds    total      ns/u      cyc/u   cyc/O
//       1     24    87M   1.0103     11.60      32.88   32.88
//       2     48  86.9M   0.9995      5.74      16.27   16.27
//       4     96  71.3M   0.9969      3.49       9.89    9.89
//       8    192  56.9M   0.9976      2.18       6.20    6.20
//      16    384  42.1M   1.0016      1.48       4.21    4.21
//      32    768  21.2M   0.8033      1.17       3.34    3.34
//      64   1.5K  13.3M   0.8682      1.01       2.88    2.88
//     128     3K  7.69M   0.9129      0.92       2.62    2.62
//     256     6K  4.21M   0.9585      0.88       2.51    2.51
//     512    12K  2.09M   0.9997      0.93       2.64    2.64
//      1K    24K  1.04M   0.9800      0.91       2.59    2.59
//      2K    48K   363K   1.0014      1.34       3.80    3.80
//      4K    96K   169K   1.0006      1.44       4.08    4.08
//      8K   192K    84K   0.9885      1.43       4.07    4.07
//     16K   384K    42K   0.9883      1.43       4.06    4.06
//     32K   768K    21K   0.9015      1.31       3.71    3.71
//     64K   1.5M  10.3K   0.9987      1.47       4.16    4.16
//    128K     3M     5K   0.8483      1.29       3.66    3.66
//    256K     6M     2K   0.8116      1.54       4.38    4.38
//    512K    12M    321   0.9847      5.85      16.57   16.57
//      1M    24M    162   1.0033      5.90      16.73   16.73
//      2M    48M     81   1.0027      5.90      16.72   16.72
//      4M    96M     40   0.9783      5.83      16.52   16.52
//      8M   192M     20   0.9779      5.82      16.51   16.51
//     16M   384M     10   0.9785      5.83      16.52   16.52
//     32M   768M      5   0.9770      5.82      16.49   16.49
//     64M   1.5G      3   1.1740      5.83      16.52   16.52

static double add_cyc_per_u64[] = {
   32.88,   //    1 = 2^0
   16.27,   //    2 = 2^1
    9.89,   //    4 = 2^2
    6.20,   //    8 = 2^3
    4.21,   //   16 = 2^4
    3.34,   //   32 = 2^5
    2.88,   //   64 = 2^6
    2.62,   //  128 = 2^7
    2.51,   //  256 = 2^8
    2.64,   //  512 = 2^9
    2.59,   //   1K = 2^10
    3.80,   //   2K = 2^11
    4.08,   //   4K = 2^12
    4.07,   //   8K = 2^13
    4.06,   //  16K = 2^14
    3.71,   //  32K = 2^15
    4.16,   //  64K = 2^16
    3.66,   // 128K = 2^17
    4.38,   // 256K = 2^18
   16.57,   // 512K = 2^19
   16.73,   //   1M = 2^20
   16.72,   //   2M = 2^21
   16.52,   //   4M = 2^22
   16.51,   //   8M = 2^23
   16.52,   //  16M = 2^24
   16.49,   //  32M = 2^25
   16.52    //  64M = 2^26
};

// improve: does not handle logarithmic time properly
uint64 _CL_CALL estimate_time(const index len, const double* timings, const unsigned nb, const double exp) {
   const unsigned lg = int_log2(len);  // computes floor(log2(x))
   const index use_lg = lg >= nb - 1 ? nb - 1 : lg;
   const index base_len = (index)1 << use_lg;
   const double q = (double)len / base_len;
   if (use_lg < nb - 2) {
      // interpolate
      const double low = timings[use_lg];
      const double hi = timings[use_lg+1];
      const double qv = hi / low;
      const double r = ((qv - 1) * (q - 1) + 1) * low;
      return r * len;
   }
   else {
      // extrapolate
      const double pq = pow(q, exp);
      return timings[use_lg] * pq * base_len;
   }
}

uint64 _CL_CALL estimate_add_time(const index len) {
   return estimate_time(len, add_cyc_per_u64, NBEL(add_cyc_per_u64), 1.0);
}

#ifdef CL_TEST_CODE
static void test_estimate_add_time() {
   uint64 c = estimate_add_time(1);
   classert(c == 32);
   c = estimate_add_time(2);
   classert(c == 32);
   c = estimate_add_time(16384);
   classert(c == 66519);
   c = estimate_add_time(64);
   classert(c == 184);
   c = estimate_add_time(96);
   classert(c == 264);
   c = estimate_add_time(128);
   classert(c == 335);
   c = estimate_add_time(128*1024*1024);
   classert(c == 2217276866);
}
#endif


#ifdef CL_TEST_CODE
void _CL_CALL test_bignum_add() {
   test_ops_add<uint8>();
   test_ops_add<uint16>();
   test_ops_add<uint32>();

#ifdef _M_X64
   test_ops_add<uint64>();
   {
      uint64 r;
      uint64 of = ops<uint64>::addc(r, 0xee255ee9f138bbe1, 0xc4416c831592e186, 0xc4416c831592e186);
      classert(r == 0x76a837f01c5e7eed);
      classert(of == 2);
   }
#if defined BN_ADD_USE_ASMX64
   test_add_asmx64(add8_asmx64);
   test_add_asmx64(add4_asmx64);
#endif
#endif

#if defined _M_X86 && defined BN_ADD_USE_ASMX86
   test_add_asmx86();
#endif

   test_add2();
   test_add3();
   test_add_on();
   test_addu_on_shl();

   // double fibonacci test
   dword rr = test_add_fib2<uint16, uint32>(1024);
   classert(rr == 0);
   rr = test_add_on_fib<uint16, uint32>(1024);
   classert(rr == 0);
#ifdef _M_X64
   rr = test_add_fib2<uint32, uint64>(1024);
   classert(rr == 0);
   rr = test_add_on_fib<uint32, uint64>(1024);
   classert(rr == 0);
#endif

   selftest_add<uint8>();
   selftest_add<uint16>();
   selftest_add<uint32>();
#ifdef _M_X64
   selftest_add<uint64>();
#endif

   test_estimate_add_time();
   test_add_matrix();
}
#endif


// ------ generate template functions -----------------------------------------

#pragma inline_depth(0)

template<typename small>
void generate_bignum_add() {
   small u;
   addi(&u, 0, &u, 0, &u, 0);
   addu(&u, 0, &u, 0, &u, 0);
   add_unrolled(&u, &u, &u, 0);
   addi_on(&u, 0, &u, 0);
   addu_on(&u, 0, &u, 0);
   addu_on_shl(&u, 0, &u, 0, 0);
   selftest_add<small>();
}

void _generate_bignum_add() {
   classertm(0, "must not be called");
   generate_bignum_add<uint8>();
   generate_bignum_add<uint16>();
   generate_bignum_add<uint32>();
#ifdef _M_X64
   generate_bignum_add<uint64>();
#endif
}

#pragma inline_depth()

}  // namespace bignum
