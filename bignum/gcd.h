/*
 *  arbitrary length number arithmetic - (E)GCD
 */

// $+HEADER$
// 
// Copyright 2015 Christoph Lueders
// 
// This file is part of the BIGNUM Library.
// 
// The BIGNUM Library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
// 
// The BIGNUM Library is distributed in the hope that it will be useful, but 
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public 
// License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with the BIGNUM Library.  If not, see <http://www.gnu.org/licenses/>.
// 
// $-HEADER$

// $Date: 2015-04-24 11:47:36 +0200 (Fr, 24 Apr 2015) $
// $Rev: 26592 $

#ifndef CL_BIGNUM_GCD_H
#define CL_BIGNUM_GCD_H 1

#ifndef CL_CLLIB_H
#include "cl/cllib.h"
#endif

#pragma pack(push, 8)  // arbitrary but fixed

namespace bignum {

   using namespace cl;

   template<typename T> void egcd_with_sign(T a, T b, T& gg, T& xx, bool& xx_sign, T& yy, bool& yy_sign);
   template<typename T> T mod_inv(T a, T m);

}  // namespace cl

#pragma pack(pop)

#endif  // ndef CL_BIGNUM_GCD_H
