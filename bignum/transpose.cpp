// $+HEADER$
// 
// Copyright 2015 Christoph Lueders
// 
// This file is part of the BIGNUM Library.
// 
// The BIGNUM Library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
// 
// The BIGNUM Library is distributed in the hope that it will be useful, but 
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public 
// License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with the BIGNUM Library.  If not, see <http://www.gnu.org/licenses/>.
// 
// $-HEADER$

// $Date: 2015-04-24 11:28:16 +0200 (Fr, 24 Apr 2015) $
// $Rev: 26590 $

/*
 *  arbitrary length number arithmetic - transpose of a matrix
 */

#include "precomp.h"
#include "impl.h"
#include "cl/int_math.h"


// we must switch off "unreachable code" warning, since bool template parameters generate code paths that are never used
#pragma warning (disable:4702)

namespace bignum {

// ------ MATRIX TRANSPOSE ---------------------------------------------------

static inline index src_pos(const index i, const index m, const index n) {
   index x = i / n;
   index y = i % n;
   classert(i == x*n + y);
   return y*m + x;
}

static inline index src_pos_pow2(const index i, const unsigned mlog, const unsigned nlog) {
   index x = i >> nlog;
   index y = i & (((index)1 << nlog) - 1);
   classert(i == x*((index)1 << nlog) + y);
   index ret = (y << mlog) + x;
   classert(ret == src_pos(i, (index)1 << mlog, (index)1 << nlog));
   return ret;
}

// transpose a matrix with 'm' columns and 'n' rows.
// inspired by http://stackoverflow.com/questions/9227747/in-place-transposition-of-a-matrix

template<bool ispow2>
void _CL_CALL mem_transpose_in_place(void* _a, const index m, const index n, const size_t sz) {
   if (m <= 1 || n <= 1) return;  // it's ok as it is
   char* a = (char*)_a;
   classert(a != nullptr);
   classert(sz > 0);

   const index mn = m*n;
   const index vis_len = (mn + bits(limb) - 1) / bits(limb);
   tape_alloc<limb> _visited(vis_len);
   limb* visited = _visited.p;
   zero(visited, vis_len);
   tape_alloc<byte> _tmp(sz);
   void* tmp = _tmp.p;
   classert(!ispow2 || is_pow2(m));
   unsigned mlog = ispow2 ? int_log2(m) : 0;
   classert(!ispow2 || is_pow2(n));
   unsigned nlog = ispow2 ? int_log2(n) : 0;

   for (index i=0; i<mn; ++i) {
      if (is_bit_set(visited, i)) continue;
      index j = ispow2 ? src_pos_pow2(i, mlog, nlog) : src_pos(i, m, n);  // who wants my place?
      if (i != j) {
         memcpy(tmp, a+i*sz, sz);
         index cycle = i;
         do {
            memcpy(a+cycle*sz, a+j*sz, sz);
            set_bit(visited, cycle);
            cycle = j;
            j = ispow2 ? src_pos_pow2(cycle, mlog, nlog) : src_pos(cycle, m, n);
         } while (j != i);
         memcpy(a+cycle*sz, tmp, sz);
         set_bit(visited, cycle);
      }
   }
}


#ifdef CL_TEST_CODE
void _CL_CALL test_bignum_transpose() {
   {
      // 1x1 matrix - no change needed
      uint8 a[] = { 1 };
      transpose_in_place_odd(a, 1, 1);
      classert(dump_poly_hex(a, NBEL(a), 1) == "[1]");
   }
   {
      // 2x2 matrix
      uint8 a[] = { 1, 2, 3, 4 };
      transpose_in_place_odd(a, 2, 2);
      classert(dump_poly_hex(a, NBEL(a), 1) == "[1, 3, 2, 4]");
   }
   {
      // 4x2 matrix
      uint8 a[] = { 1, 2, 3, 4, 5, 6, 7, 8 };
      transpose_in_place_odd(a, 4, 2);
      classert(dump_poly_hex(a, NBEL(a), 1) == "[1, 5, 2, 6, 3, 7, 4, 8]");
   }
   {
      // 2x4 matrix
      uint8 a[] = { 1, 2, 3, 4, 5, 6, 7, 8 };
      transpose_in_place_odd(a, 2, 4);
      classert(dump_poly_hex(a, NBEL(a), 1) == "[1, 3, 5, 7, 2, 4, 6, 8]");
   }
   {
      // 3x5 matrix
      uint8 a[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0xa, 0xb, 0xc, 0xd, 0xe, 0xf };
      transpose_in_place_odd(a, 3, 5);
      classert(dump_poly_hex(a, NBEL(a), 1) == "[1, 4, 7, 0xa, 0xd, 2, 5, 8, 0xb, 0xe, 3, 6, 9, 0xc, 0xf]");
   }
   {
      // 3x5 matrix
      uint32 a[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0xa, 0xb, 0xc, 0xd, 0xe, 0xf };
      transpose_in_place_odd(a, 3, 5);
      classert(dump_poly_hex(a, NBEL(a), 1) == "[1, 4, 7, 0xa, 0xd, 2, 5, 8, 0xb, 0xe, 3, 6, 9, 0xc, 0xf]");
   }

   {
      // 2x2 matrix
      uint8 a[] = { 1, 2, 3, 4 };
      transpose_in_place_pow2(a, 2, 2);
      classert(dump_poly_hex(a, NBEL(a), 1) == "[1, 3, 2, 4]");
   }
   {
      // 4x2 matrix
      uint8 a[] = { 1, 2, 3, 4, 5, 6, 7, 8 };
      transpose_in_place_pow2(a, 4, 2);
      classert(dump_poly_hex(a, NBEL(a), 1) == "[1, 5, 2, 6, 3, 7, 4, 8]");
   }
   {
      // 2x4 matrix
      uint8 a[] = { 1, 2, 3, 4, 5, 6, 7, 8 };
      transpose_in_place_pow2(a, 2, 4);
      classert(dump_poly_hex(a, NBEL(a), 1) == "[1, 3, 5, 7, 2, 4, 6, 8]");
   }
}
#endif


// ------ generate template functions -----------------------------------------

#pragma inline_depth(0)

template<typename small>
void generate_bignum_transpose() {
   small a;
   transpose_in_place_odd(&a, 1, 1);
   transpose_in_place_pow2(&a, 1, 1);
}

void _generate_bignum_transpose() {
   classertm(0, "must not be called");
   generate_bignum_transpose<uint8>();
   generate_bignum_transpose<uint16>();
   generate_bignum_transpose<uint32>();
#ifdef _M_X64
   generate_bignum_transpose<uint64>();
#endif
}

#pragma inline_depth()

}  // namespace bignum
