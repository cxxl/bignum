// $+HEADER$
// 
// Copyright 2015 Christoph Lueders
// 
// This file is part of the BIGNUM Library.
// 
// The BIGNUM Library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
// 
// The BIGNUM Library is distributed in the hope that it will be useful, but 
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public 
// License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with the BIGNUM Library.  If not, see <http://www.gnu.org/licenses/>.
// 
// $-HEADER$

// $Date: 2015-04-24 11:47:36 +0200 (Fr, 24 Apr 2015) $
// $Rev: 26592 $

#ifndef BIGNUM_PRO_FILER_H
#define BIGNUM_PRO_FILER_H 1

#include "cl/cpu.h"

namespace bignum {

extern thread_cycles cyc;

using namespace std;
using namespace cl;

struct pro_filer_data {
   string name;
   uint64 cycle_sum;
   uint64 start_cyc;
   uint64 call_count;
   pro_filer_data(const string& _name) {
      name = _name;
      clear_counts();
   }
   pro_filer_data(const pro_filer_data& p) {
      name = p.name;
      cycle_sum = p.cycle_sum;
      start_cyc = p.start_cyc;
      call_count = p.call_count;
   }
   void clear_counts() {      
      cycle_sum = 0;
      start_cyc = 0;
      call_count = 0;
   }
};

struct pro_filer {
   vector<pro_filer_data> data;
   size_t total_id;
   uint64 cycle_adjust;
   uint64 cumulated_adjust;
   bool active;

   uint64 get_cyc() const {
      return cyc.get_cycles() - cumulated_adjust;
   }
   pro_filer();
   size_t register_counter(const string& name);
   void start(size_t idx) {
      if (!active) return;
      pro_filer_data& d = data[idx];
      caassert(d.start_cyc == 0);  // not yet used
      d.start_cyc = get_cyc();
   }
   void stop(size_t idx) {
      if (!active) return;
      pro_filer_data& d = data[idx];
      caassert(d.start_cyc != 0);  // in use
      cumulated_adjust += cycle_adjust;  // add one start/stop adjustment
      ++d.call_count;
      d.cycle_sum += get_cyc() - d.start_cyc;
      d.start_cyc = 0;  // mark not used
   }
   bool is_active() const {
      return active;
   }
   void all_start();
   void all_stop();
   void clear();
   void calibrate();
   pro_filer_data* find(const string& name) const;
   string output() const;
};

}  // namespace bignum

#endif  // ndef BIGNUM_PRO_FILER_H
