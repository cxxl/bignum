// $+HEADER$
// 
// Copyright 2015 Christoph Lueders
// 
// This file is part of the BIGNUM Library.
// 
// The BIGNUM Library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
// 
// The BIGNUM Library is distributed in the hope that it will be useful, but 
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public 
// License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with the BIGNUM Library.  If not, see <http://www.gnu.org/licenses/>.
// 
// $-HEADER$

// $Date: 2015-04-24 11:28:16 +0200 (Fr, 24 Apr 2015) $
// $Rev: 26590 $

/*
 *  arbitrary length number arithmetic - (self)test
 */

#include "precomp.h"
#include "cl/format.h"
#include "impl.h"


// we must switch off "unreachable code" warning, since bool template parameters generate code paths that are never used
#pragma warning (disable:4702)

namespace bignum {


// ------ SELFTEST ------------------------------------------------------------

template<typename small>
static dword selftest() {
   dword r;
   if ((r = selftest_add<small>()) != 0) return 0x10000 + r;
   if ((r = selftest_sub<small>()) != 0) return 0x20000 + r;
   if ((r = selftest_mul<small>()) != 0) return 0x30000 + r;
   if ((r = selftest_div<small>()) != 0) return 0x40000 + r;

   const unsigned maxp = CL_IF_X64(1279, 607);
   if ((r = selftest_lltest<small>(maxp)) != 0) return 0x80000 + r;

   return 0;
}

dword _CL_CALL bignum_selftest() {
   dword r;
   if ((r = selftest<uint32>()) != 0) return 0x5000000 + r;
   return 0;
}


// ------ LUCAS-LEHMER PRIMALITY TEST -----------------------------------------

// test if 2^p-1 is prime

template<typename small>
lucas_lehmer<small>::lucas_lehmer(void (*_squfunc)(small* r, const small* a, const index alen)) : squfunc(_squfunc) {}

template<typename small>
bool lucas_lehmer<small>::test(const index _p, bool& finished) {
   p = _p;
   //DTOUT_PUTS(format("lltest: M%?, sizeof(small)=%?\n") << p << sizeof(small));
   const index l = (p + bits(small) - 1) / bits(small);
   tape_alloc<small> u(l);
   zero(u.p, l);
   u[0] = 4;
   index i = 2;
   return run_iterations(p, i, u.p, l, finished);
}

template<typename small>
bool lucas_lehmer<small>::run_iterations(const index _p, index i, small* inbuf, const index ulen, bool& finished) {
   p = _p;
   caassert(p > 2);  // doesn't work on p == 2
   caassert(i < p);
   const index l = (p + bits(small) - 1) / bits(small);
   caassert(ulen <= l);
   tape_alloc<small> tmpu(2*l);
   small* u = tmpu.p;
   tape_alloc<small> tmpv(2*l);
   small* v = tmpv.p;
   copy(u, inbuf, ulen);
   zero(u, l, ulen);
   const small two = 2;
   finished = false;
   for (; i<p; ++i) {
      if (!callback(i, u, l)) return false;
      squfunc(v, u, l);   // v = u^2
      subu_on(v, 2*l, &two, 1);  // v -= 2
      mod_pow2m1_on(v, 2*l, p);  // v %= (2^p-1)
      swap(u, v);                // u = v
      //DTOUT_PUTS(dump_hex(u, l) + "\n");
   }
   finished = true;
   done(u, l);
   return normu(u, l) == 0;
}

template<typename small>
bool lucas_lehmer_test(const index p) {
   lucas_lehmer<small> ll;
   bool finished;
   return ll.test(p, finished);
}

const unsigned mersenne_prime_exps[] = {
   3, 5, 7, 13, 17, 19, 31, 61, 89, 107, 127, 521, 607,
   1279, 2203, 2281, 3217, 4253, 4423, 9689, 9941, 11213, 19937, 21701, 23209, 44497,
   86243, 110503, 132049, 216091, 756839, 859433, 1257787, 1398269, 2976221, 3021377,
   6972593, 13466917, 20996011, 24036583, 25964951, 30402457, 32582657, 37156667,
   42643801, 43112609, 57885161,
   0
};


template<typename small>
dword _CL_CALL selftest_lltest(const unsigned maxp, void (*squfunc)(small* r, const small* a, const index alen)) {
   lucas_lehmer<small> ll(squfunc);
   bool finished;
   for (dword i=0; ; ++i) {
      const unsigned p = mersenne_prime_exps[i];
      if (p == 0) break;
      if (p <= maxp && !ll.test(p, finished)) {
         classertm(0, ((string)(format("lucas-lehmer test of M%? failed, sizeof(small)=%?\n") << p << sizeof(small))).c_str());
         return i;
      }
   }
   return 0;
}


#ifdef CL_TEST_CODE
void _CL_CALL test_bignum_add();
void _CL_CALL test_bignum_sub();
void _CL_CALL test_bignum_addsub();
void _CL_CALL test_bignum_util();
void _CL_CALL test_bignum_omul();
void _CL_CALL test_bignum_logic();
void _CL_CALL test_bignum_kmul();
void _CL_CALL test_bignum_div();
void _CL_CALL test_bignum_t3mul();
void _CL_CALL test_bignum_smul();
void _CL_CALL test_bignum_gcd();
void _CL_CALL test_bignum_qmul();
void _CL_CALL test_bignum_prime();
void _CL_CALL test_bignum_t();
void _CL_CALL test_bignum_format();
void _CL_CALL test_bignum_all_ll();
void _CL_CALL test_bignum_dkss();
void _CL_CALL test_bignum_benchmark();
void _CL_CALL test_bignum_transpose();
#ifdef USE_GMP
void _CL_CALL test_bignum_gmp_wrap();
#endif

void _CL_CALL test_bignum() {
   test_bignum_add();
   test_bignum_sub();
   test_bignum_addsub();
   test_bignum_util();
   test_bignum_logic();
   test_bignum_transpose();
   test_bignum_omul();
   test_bignum_kmul();
   test_bignum_div();
   test_bignum_t3mul();
   test_bignum_smul();
   test_bignum_gcd();
   test_bignum_qmul();
   test_bignum_prime();
   test_bignum_t();
   test_bignum_format();
   test_bignum_all_ll();
   test_bignum_dkss();
   test_bignum_benchmark();
#ifdef USE_GMP
   test_bignum_gmp_wrap();
#endif

   selftest<uint16>();
   selftest<uint32>();
   selftest<uint8>();
#ifdef _M_X64
   selftest<uint64>();
#endif

   tout_puts("test_bignum() passed.\n");
}
#endif


//#if _MSC_VER < 1300
#pragma inline_depth(0)
   void _pull_bignum_test_into_existance() {
      classertm(0, "must not be called");

      lucas_lehmer_test<uint8>(0);
      lucas_lehmer_test<uint16>(0);
      lucas_lehmer_test<uint32>(0);
#ifdef _M_X64
      lucas_lehmer_test<uint64>(0);
#endif
      selftest_lltest<uint16>(0);
      selftest_lltest<uint32>(0);
      selftest_lltest<uint8>(0);
#ifdef _M_X64
      selftest_lltest<uint64>(0);
#endif
   }
#pragma inline_depth()
//#endif

}  // namespace bignum
