// $+HEADER$
// 
// Copyright 2015 Christoph Lueders
// 
// This file is part of the BIGNUM Library.
// 
// The BIGNUM Library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
// 
// The BIGNUM Library is distributed in the hope that it will be useful, but 
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public 
// License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with the BIGNUM Library.  If not, see <http://www.gnu.org/licenses/>.
// 
// $-HEADER$

// $Date: 2015-04-24 11:28:16 +0200 (Fr, 24 Apr 2015) $
// $Rev: 26590 $

#ifdef USE_GMP

#include "gmp_wrap.h"

#pragma warning(push)
#pragma warning(disable: 4127)  // warning C4127: conditional expression is constant
#include "../mpir-2.6.0/gmp.h"
#pragma warning(pop)

// make sure the right library flavour is included
#ifdef _WIN64
# ifdef _DEBUG
#  pragma comment(lib, "../mpir-2.6.0/lib/x64/Debug/mpir.lib")
# else
#  pragma comment(lib, "../mpir-2.6.0/lib/x64/Release/mpir.lib")
# endif
#else
# ifdef _DEBUG
#  pragma comment(lib, "../mpir-2.6.0/lib/Debug/mpir.lib")
# else
#  pragma comment(lib, "../mpir-2.6.0/lib/Release/mpir.lib")
# endif
#endif


namespace bignum {

#define size_to_limbs(s) ((mp_size_t)(sizeof(small) != sizeof(mp_limb_t) ? ((s) * sizeof(small) + sizeof(mp_limb_t) - 1) / sizeof(mp_limb_t) : (s)))

template<typename small>
void gmp_mul(small* r, const small* a, index alen, const small* b, index blen) {
   mp_limb_t *rp = (mp_limb_t *)r;   
   const mp_limb_t *s1p = (mp_limb_t *)a;
   mp_size_t s1n = size_to_limbs(alen);
   const mp_limb_t *s2p = (mp_limb_t *)b;
   mp_size_t s2n = size_to_limbs(blen);
   mpn_mul(rp, s1p, s1n, s2p, s2n);
}

template<typename small>
void gmp_mul_n(small* r, const small* a, const small* b, index len) {
   mp_limb_t *rp = (mp_limb_t *)r;   
   const mp_limb_t *s1p = (mp_limb_t *)a;
   const mp_limb_t *s2p = (mp_limb_t *)b;
   mp_size_t n = size_to_limbs(len);
   mpn_mul_n(rp, s1p, s2p, n);
}

template<typename small>
void gmp_sqr(small* r, const small* a, index len) {
   mp_limb_t *rp = (mp_limb_t *)r;   
   const mp_limb_t *s1p = (mp_limb_t *)a;
   mp_size_t n = size_to_limbs(len);
   mpn_sqr(rp, s1p, n);
}

extern "C" {
#define mpn_mul_basecase __MPN(mul_basecase)
__GMP_DECLSPEC mp_limb_t mpn_mul_basecase __GMP_PROTO ((mp_ptr, mp_srcptr, mp_size_t, mp_srcptr, mp_size_t));
}

template<typename small>
void gmp_mul_basecase(small* r, const small* a, index alen, const small* b, index blen) {
   mp_limb_t *rp = (mp_limb_t *)r;   
   const mp_limb_t *s1p = (mp_limb_t *)a;
   mp_size_t s1n = size_to_limbs(alen);
   const mp_limb_t *s2p = (mp_limb_t *)b;
   mp_size_t s2n = size_to_limbs(blen);
   mpn_mul_basecase(rp, s1p, s1n, s2p, s2n);
}


#ifdef CL_TEST_CODE
template<typename small>
static void test_gmp_mul(index len) {
   small* a = rand_alloc<small>(len);
   small* b = rand_alloc<small>(len);
   small* r = bignum_alloc<small>(2*len);
   small* r2 = bignum_alloc<small>(2*len);
   gmp_mul(r, a, len, b, len);
   mulu(r2, a, len, b, len);
   int cmp = compu_nn(r, 2*len, r2, 2*len);
   classert(cmp == 0);
   bignum_free<small>(a);
   bignum_free<small>(b);
   bignum_free<small>(r);
   bignum_free<small>(r2);
}

template<typename small>
static void test_gmp_mul_n(index len) {
   small* a = rand_alloc<small>(len);
   small* b = rand_alloc<small>(len);
   small* r = bignum_alloc<small>(2*len);
   small* r2 = bignum_alloc<small>(2*len);
   gmp_mul_n(r, a, b, len);
   mulu(r2, a, len, b, len);
   int cmp = compu_nn(r, 2*len, r2, 2*len);
   classert(cmp == 0);
   bignum_free<small>(a);
   bignum_free<small>(b);
   bignum_free<small>(r);
   bignum_free<small>(r2);
}

template<typename small>
static void test_gmp_sqr(index len) {
   small* a = rand_alloc<small>(len);
   small* r = bignum_alloc<small>(2*len);
   small* r2 = bignum_alloc<small>(2*len);
   gmp_sqr(r, a, len);
   squareu(r2, a, len);
   int cmp = compu_nn(r, 2*len, r2, 2*len);
   classert(cmp == 0);
   bignum_free<small>(a);
   bignum_free<small>(r);
   bignum_free<small>(r2);
}

template<typename small>
static void test_gmp_mul_basecase(index len) {
   small* a = rand_alloc<small>(len);
   small* b = rand_alloc<small>(len);
   small* r = bignum_alloc<small>(2*len);
   small* r2 = bignum_alloc<small>(2*len);
   gmp_mul_basecase(r, a, len, b, len);
   mulu(r2, a, len, b, len);
   int cmp = compu_nn(r, 2*len, r2, 2*len);
   classert(cmp == 0);
   bignum_free<small>(a);
   bignum_free<small>(b);
   bignum_free<small>(r);
   bignum_free<small>(r2);
}


template<typename small>
static void test_gmp_wrap2() {
   test_gmp_mul<small>(1);
   test_gmp_mul<small>(10);
   test_gmp_mul<small>(1000);
   test_gmp_mul<small>(100000);
   test_gmp_mul_n<small>(10000);
   test_gmp_sqr<small>(10000);
   test_gmp_mul_basecase<small>(20);
   test_gmp_mul_basecase<small>(19);
}

void _CL_CALL test_bignum_gmp_wrap() {
   test_gmp_wrap2<limb>();
}
#endif


// ------ generate template functions -----------------------------------------

#pragma inline_depth(0)

template<typename small>
void generate_bignum_gmp_wrap() {
   small u;
   gmp_mul(&u, &u, 0, &u, 0);
   gmp_mul_n(&u, &u, &u, 0);
   gmp_sqr(&u, &u, 0);
   gmp_mul_basecase(&u, &u, 0, &u, 0);
}

void _generate_bignum_gmp_wrap() {
   classertm(0, "must not be called");
   generate_bignum_gmp_wrap<uint8>();
   generate_bignum_gmp_wrap<uint16>();
   generate_bignum_gmp_wrap<uint32>();
#ifdef _M_X64
   generate_bignum_gmp_wrap<uint64>();
#endif
}

#pragma inline_depth()

}  // namespace bignum

#endif  // def USE_GMP
