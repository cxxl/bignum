/*
 *  arbitrary length number arithmetic
 */

// $+HEADER$
// 
// Copyright 2015 Christoph Lueders
// 
// This file is part of the BIGNUM Library.
// 
// The BIGNUM Library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
// 
// The BIGNUM Library is distributed in the hope that it will be useful, but 
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public 
// License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with the BIGNUM Library.  If not, see <http://www.gnu.org/licenses/>.
// 
// $-HEADER$

// $Date: 2015-04-24 11:47:36 +0200 (Fr, 24 Apr 2015) $
// $Rev: 26592 $

#ifndef CL_BIGNUM_IMPL_H
#define CL_BIGNUM_IMPL_H 1

#include <random>
#ifndef CL_CLLIB_H
#include "cl/cllib.h"
#endif
#include "cl/cpu.h"

#pragma pack(push, 8)  // arbitrary but fixed

namespace bignum {

   using namespace cl;

   // compile time floor integer log2
   template<uint64 v> struct ct_int_log2 {
      static const unsigned result = ct_int_log2<v / 2>::result + 1;
   };
   template<> struct ct_int_log2<1> { static const unsigned result = 0; };
   template<> struct ct_int_log2<0> { };  // undefined


#define bits(a) (sizeof(a) * 8)
#define log_bits(a) ct_int_log2<sizeof(a) * 8>::result
#define top_set(a) ((a >> (bits(a)-1)) != 0)


// let's use the native register size for x64.  it can only be faster.

#ifdef _M_X64
   typedef uint64 index;
   typedef uint64 bit_index;
   typedef int64 sindex;
   typedef uint64 limb;
   typedef uint32 halflimb;
#else
   typedef unsigned index;
   typedef uint64 bit_index;
   typedef int sindex;
   typedef uint32 limb;
   typedef uint16 halflimb;
#endif

   template<typename small>
   index bits_to_small(const index len) {
      return (len + bits(small) - 1) / bits(small);
   }

}


#include "calibrate.h"


namespace bignum {

// ------ math ----------------------------------------------------------------

double _CL_CALL log2(double x);
double _CL_CALL xlog2(double x);
double _CL_CALL slog(double base, double z);
double _CL_CALL log2_star(double x);
double _CL_CALL xlog2_star(double x);


// ------ estimations ---------------------------------------------------------

extern const uint64 clock_rate;

double _CL_CALL est_mul_cycles(const index ll);
double _CL_CALL est_mul_time(const index ll);
uint64 _CL_CALL est_mul_rounds(const index ll, const uint64 msec=1000);


// ------ status --------------------------------------------------------------

extern wstring worker_name;
extern wstring working_directory;
extern wstring command_line;
extern string start_datetime;
extern uint64 start_cycles;
extern int run_proc;
extern thread_cycles cyc;

string _CL_CALL system_info();


// ------ PROFILING -----------------------------------------------------------

//#define PROFILE 1

#ifdef PROFILE
   void _CL_CALL profile_params2(const char* name, index alen, index blen);
   void _CL_CALL print_profiling();
   void _CL_CALL profile_clear();
#define PROFILE_PARAMS2(name, alen, blen) profile_params2(name, alen, blen)
#define PROFILE_PRINT() print_profiling()
#define PROFILE_CLEAR() profile_clear()
#else
#define PROFILE_PARAMS2(name, alen, blen) NOP()
#define PROFILE_PRINT() NOP()
#define PROFILE_CLEAR() NOP()
#endif


// control which extern .asm or unrolled routines should be used.

#define BN_ADD_USE_FAST 1
//#define BN_ADD_USE_ASMX86 1
#define BN_ADD_USE_ASMX64 1

#define BN_SUB_USE_FAST 1
//#define BN_SUB_USE_ASMX86 1
#define BN_SUB_USE_ASMX64 1

#define BN_MUL1_USE_FAST 1
//#define BN_MUL1_USE_ASMX86 1
//#define BN_MUL1_USE_ASMX64 1

//#define BN_OMUL_USE_ASMX86 1
#define BN_OMUL_USE_ASMX64 1
// BN_OMUL2_USE_ASMX64 takes precedence over BN_OMUL_USE_ASMX64
#define BN_OMUL2_USE_ASMX64 1
// BN_OMUL3_USE_ASMX64 takes precedence over BN_OMUL2_USE_ASMX64 and BN_OMUL_USE_ASMX64
#define BN_OMUL3_USE_ASMX64 1
// BN_OMUL4_USE_ASMX64 takes precedence over BN_OMUL3_USE_ASMX64, BN_OMUL2_USE_ASMX64 and BN_OMUL_USE_ASMX64
#define BN_OMUL4_USE_ASMX64 1

#define BN_OSQU4_USE_ASMX64 1

#define BN_ADDSUB_USE_ASMX64 1
// #define BN_ADDSUB2_USE_ASMX64 1  // too slow
// #define BN_ADDSUB3_USE_ASMX64 1  // too complicated

// C++ version is faster than .asm
// #define BN_DIV_BY3_USE_ASMX64 1


// extern .asm routines' prototypes

#ifdef _M_X86
#ifdef BN_ADD_USE_ASMX86
   uint32 __fastcall add_asmx86(uint32* r, const uint32* a, const uint32* b, const uint32 len);
#endif
#ifdef BN_SUB_USE_ASMX86
   uint32 __fastcall sub_asmx86(uint32* r, const uint32* a, const uint32* b, const uint32 len);
#endif
#ifdef BN_MUL1_USE_ASMX86
   void __fastcall mul1_asmx86(uint32* r, const uint32* a, const uint32 len, const uint32 b);
#endif
#ifdef BN_OMUL_USE_ASMX86
   void __fastcall omul_asmx86(uint32* r, const uint32* a, const uint32 alen, const uint32* b, const uint32 blen);
#endif
#endif

#ifdef _M_X64
#ifdef BN_ADD_USE_ASMX64
   extern uint64 (*add_ptr)(uint64* r, const uint64* a, const uint64* b, const uint64 len);
   uint64 __cdecl add4_asmx64(uint64* r, const uint64* a, const uint64* b, const uint64 len);
   uint64 __cdecl add8_asmx64(uint64* r, const uint64* a, const uint64* b, const uint64 len);
#endif
#ifdef BN_SUB_USE_ASMX64
   extern uint64 (*sub_ptr)(uint64* r, const uint64* a, const uint64* b, const uint64 len);
   uint64 __cdecl sub2_asmx64(uint64* r, const uint64* a, const uint64* b, const uint64 len);
   uint64 __cdecl sub4_asmx64(uint64* r, const uint64* a, const uint64* b, const uint64 len);
   uint64 __cdecl sub8_asmx64(uint64* r, const uint64* a, const uint64* b, const uint64 len);
#endif
#ifdef BN_MUL1_USE_ASMX64
   void __cdecl mul1_asmx64(uint64* r, const uint64* a, const uint64 len, const uint64 b);
#endif
#ifdef BN_OMUL_USE_ASMX64
   void __cdecl omul_asmx64(uint64* r, const uint64* a, const uint64 alen, const uint64* b, const uint64 blen);
#endif
#ifdef BN_OMUL2_USE_ASMX64
   void __cdecl omul2_asmx64(uint64* r, const uint64* a, const uint64 alen, const uint64* b, const uint64 blen);
#endif
#ifdef BN_OMUL3_USE_ASMX64
   void __cdecl omul3_asmx64(uint64* r, const uint64* a, const uint64 alen, const uint64* b, const uint64 blen);
#endif
#ifdef BN_OMUL4_USE_ASMX64
   void __cdecl omul4_asmx64(uint64* r, const uint64* a, const uint64 alen, const uint64* b, const uint64 blen);
#endif
#ifdef BN_OSQU4_USE_ASMX64
   void __cdecl osqu4_asmx64(uint64* r, const uint64* a, const uint64 alen);
#endif
#ifdef BN_DIV_BY3_USE_ASMX64
   void __cdecl div_by3_asmx64(uint64* q, const uint64* a, uint64 alen);
#endif
#ifdef BN_ADDSUB_USE_ASMX64
   uint64 __cdecl addsub_asmx64(uint64* sum, uint64* diff, const uint64* a, const uint64* b, const uint64 len);
#endif
   // this is yet just a try - too slow!
   uint64 __cdecl addsub2_asmx64(uint64* sum, uint64* diff, const uint64* a, const uint64* b, const uint64 len);
   uint64 __cdecl addsub3_asmx64(uint64* sum, uint64* diff, const uint64* a, const uint64* b, const uint64 len);
   uint32 __cdecl threeop_asmx64(uint64* r, const uint64* a, const uint64* b, const uint64 len);
   uint32 __cdecl threeop2_asmx64(uint64* r, const uint64* a, const uint64* b, const uint64 len);
   uint32 __cdecl threeop3_asmx64(uint64* r, const uint64* a, const uint64* b, const uint64 len);
   uint64 __cdecl modmul_asmx64(uint64 u, uint64 v, uint64 m);
   uint64 __cdecl modmul2_asmx64(uint64 u, uint64 v);
   uint64 __cdecl divq_asmx64(uint64 a1, uint64 a0, uint64 b, uint64& r);
   uint64 __cdecl divr_asmx64(uint64 a0, uint64 a1, uint64 b, uint64& q);
   uint64 __cdecl divr_asmx64(uint64 a0, uint64 a1, uint64 b);
   uint64 __cdecl divr_asmx64_64by32_rev(uint64 b, uint64 a);
#endif


// ------ LOOP UNROLLING -----------------------------------------------------

#define UNROLL0(i, len, uf, lab, s)       \
            while (i+(uf) < (len)) {      \
               s(0);                      \
               ++i;                       \
            }

#define UNROLL2(i, len, uf, lab, s)       \
            {                             \
               const auto _unroll_init = ((len)-(uf)-i) % 2;  \
               i += _unroll_init;         \
               switch (_unroll_init) {    \
                  case 1: goto lab ## 1;  \
                  case 0: goto lab ## 0;  \
                  default: caassert(0);   \
               }                          \
            }                             \
lab ## 0:   while (i+(uf) < (len)) {      \
               caassert(((len) - (uf) - i) % 2 == 0);  \
               i += 2;                    \
               s(-2);                     \
lab ## 1:      s(-1);                     \
            }

#define UNROLL3(i, len, uf, lab, s)       \
            {                             \
               const auto _unroll_init = ((len)-(uf)-i) % 3;  \
               i += _unroll_init;         \
               switch (_unroll_init) {    \
                  case 2: goto lab ## 2;  \
                  case 1: goto lab ## 1;  \
                  case 0: goto lab ## 0;  \
                  default: caassert(0);   \
               }                          \
            }                             \
lab ## 0:   while (i+(uf) < (len)) {      \
               caassert(((len) - (uf) - i) % 3 == 0);  \
               i += 3;                    \
               s(-3);                     \
lab ## 2:      s(-2);                     \
lab ## 1:      s(-1);                     \
            }

#define UNROLL4(i, len, uf, lab, s)       \
            {                             \
               const auto _unroll_init = ((len)-(uf)-i) % 4;  \
               i += _unroll_init;         \
               switch (_unroll_init) {    \
                  case 3: goto lab ## 3;  \
                  case 2: goto lab ## 2;  \
                  case 1: goto lab ## 1;  \
                  case 0: goto lab ## 0;  \
                  default: caassert(0);   \
               }                          \
            }                             \
lab ## 0:   while (i+(uf) < (len)) {      \
               caassert(((len) - (uf) - i) % 4 == 0);  \
               i += 4;                    \
               s(-4);                     \
lab ## 3:      s(-3);                     \
lab ## 2:      s(-2);                     \
lab ## 1:      s(-1);                     \
            }

#define UNROLL8(i, len, uf, lab, s)       \
            {                             \
               const auto _unroll_init = ((len)-(uf)-i) % 8;  \
               i += _unroll_init;         \
               switch (_unroll_init) {    \
                  case 7: goto lab ## 7;  \
                  case 6: goto lab ## 6;  \
                  case 5: goto lab ## 5;  \
                  case 4: goto lab ## 4;  \
                  case 3: goto lab ## 3;  \
                  case 2: goto lab ## 2;  \
                  case 1: goto lab ## 1;  \
                  case 0: goto lab ## 0;  \
                  default: caassert(0);   \
               }                          \
            }                             \
lab ## 0:   while (i+(uf) < (len)) {      \
               caassert(((len) - (uf) - i) % 8 == 0);  \
               i += 8;                    \
               s(-8);                     \
lab ## 7:      s(-7);                     \
lab ## 6:      s(-6);                     \
lab ## 5:      s(-5);                     \
lab ## 4:      s(-4);                     \
lab ## 3:      s(-3);                     \
lab ## 2:      s(-2);                     \
lab ## 1:      s(-1);                     \
            }


// ------ ADDITION -----------------------------------------------------------

// addition of two const input numbers 'a' and 'b' into a pre-reserved result 'r'
// 'a' must be longer than or equal the size as 'b'
// the result will be padded up to 'vmax' and must be long enough to hold the result

// can be used like tadd_ls(a, alen, a, alen, a, alen)
// length can be zero, numbers don't need to be normalised
// returns 'of' (0 or 1), which has the same meaning as in the add loop.
// 'of' DOES NOT indicate if a sign extension is needed nor the word for extension!

template<typename small, bool hassign>
small tadd_ls(small* r, index rmax, const small* a, index alen, const small* b, index blen);

// don't care which operant is longer...
template<typename small, bool hassign>
inline small tadd(small* r, index rmax, const small* a, index alen, const small* b, index blen) {
   if (alen >= blen) return tadd_ls<small, hassign>(r, rmax, a, alen, b, blen);
   else return tadd_ls<small, hassign>(r, rmax, b, blen, a, alen);
}

// honour sign bits
template<typename small>
inline small addi_ls(small* r, index rmax, const small* a, index alen, const small* b, index blen) {
   return tadd_ls<small, true>(r, rmax, a, alen, b, blen);
}

template<typename small>
inline small addi(small* r, index rmax, const small* a, index alen, const small* b, index blen) {
   return tadd<small, true>(r, rmax, a, alen, b, blen);
}

// ignore sign bits - unsigned version
template<typename small>
inline small addu_ls(small* r, index rmax, const small* a, index alen, const small* b, index blen) {
   return tadd_ls<small, false>(r, rmax, a, alen, b, blen);
}

template<typename small>
inline small addu(small* r, index rmax, const small* a, index alen, const small* b, index blen) {
   return tadd<small, false>(r, rmax, a, alen, b, blen);
}


// in-place versions
// addition of one number in-place (destructively) on another

template<typename small, bool hassign>
small tadd_on(small* a, index alen, const small* b, index blen);

template<typename small>
inline small addi_on(small* a, index alen, const small* b, index blen) {
   return tadd_on<small, true>(a, alen, b, blen);
}

template<typename small>
inline small addu_on(small* a, index alen, const small* b, index blen) {
   return tadd_on<small, false>(a, alen, b, blen);
}


// addition of shifted number

template<typename small>
small addu_on_shl(small* a, index alen, const small* b, const index blen, const bit_index shift_count);


// calculate proper high word after addition from addends and 'of' return value

template<typename small>
inline small add_of_word(const bool asign, const bool bsign, small& of) {
   caassert(of <= 1);
   small r;
   if (asign || bsign) {
      if (asign && bsign) {
         classert(of == 1);
         r = -1;
      }
      else r = of - 1;
   }
   else {
      r = of;
      of = 0;
   }
   return r;
}

template<typename small /*, bool hassign*/>
inline bool need_signx(const bool asign, const bool bsign, const small of, const bool rsign) {
   //caassert(hassign || !asign);
   //caassert(hassign || !bsign);
   return (asign == bsign) != (rsign == (of != 0));
}


// manually unrolled version of the first add loop on small's.
// returns the carry.
// length can be zero, numbers don't need to be normalised

template<typename small>
small __fastcall add_unrolled(small* r, const small* a, const small* b, const index len);


// fast version of inner add loop.  is used by addi/u().
// for easy use of unrolled or asm versions of add
template<typename small>
inline small add_fast(small* r, const small* a, const small* b, index len) {
   caassert(r != 0);
   caassert(a != 0);
   caassert(b != 0);

#if defined _M_X86 && defined BN_ADD_USE_ASMX86
   if (sizeof(small) == sizeof(uint32)) return add_asmx86((uint32*)r, (uint32*)a, (uint32*)b, len);
   else
#elif defined _M_X64 && defined BN_ADD_USE_ASMX64
   if (sizeof(small) == sizeof(uint64)) return add_ptr((uint64*)r, (uint64*)a, (uint64*)b, len);
   else
#endif
   return add_unrolled(r, a, b, len);
}

// estimate times
uint64 _CL_CALL estimate_time(const index len, const double* timings, const unsigned nb, const double exp);

// estimate add() time
uint64 _CL_CALL estimate_add_time(const index len);

template<typename small>
dword selftest_add();


// ------ SUBTRACTION --------------------------------------------------------

// subtraction of two const input numbers 'a' and 'b' into a pre-reserved result 'r'
// swap == false: 'b' is subtracted from 'a': a - b
// swap == true: 'a' is subtracted from 'b': b - a
// 'a' must be longer than or equal the size as 'b'
// the result will be padded up to 'vmax' and must be long enough to hold the result
// length can be zero, numbers don't need to be normalised

template<typename small, bool swap, bool hassign>
small tsub_ls(small* r, index rmax, const small* a, index alen, const small* b, index blen);

// don't care which operant is longer...
template<typename small, bool swap, bool hassign>
small tsub(small* r, index rmax, const small* a, index alen, const small* b, index blen) {
   if (alen >= blen) return tsub_ls<small, swap, hassign>(r, rmax, a, alen, b, blen);
   else return tsub_ls<small, !swap, hassign>(r, rmax, b, blen, a, alen);
}

// honour sign bits
template<typename small, bool swap>
inline small subi_ls(small* r, index rmax, const small* a, index alen, const small* b, index blen) {
   return tsub_ls<small, swap, true>(r, rmax, a, alen, b, blen);
}

template<typename small>
inline small subi(small* r, index rmax, const small* a, index alen, const small* b, index blen) {
   return tsub<small, false, true>(r, rmax, a, alen, b, blen);
}

// ignore sign bits - unsigned version
template<typename small, bool swap>
inline small subu_ls(small* r, index rmax, const small* a, index alen, const small* b, index blen) {
   return tsub_ls<small, swap, false>(r, rmax, a, alen, b, blen);
}

template<typename small>
inline small subu(small* r, index rmax, const small* a, index alen, const small* b, index blen) {
   return tsub<small, false, false>(r, rmax, a, alen, b, blen);
}


// in-place versions
// subtraction of one number in-place (destructively) from another
// returns 1 if overflow, 0 otherwise

template<typename small, bool swap, bool hassign>
small tsub_on(small* a, index alen, const small* b, index blen);

// a = a - b
template<typename small>
inline small subi_on(small* a, index alen, const small* b, index blen) {
   return tsub_on<small, false, true>(a, alen, b, blen);
}

template<typename small>
inline small subu_on(small* a, index alen, const small* b, index blen) {
   return tsub_on<small, false, false>(a, alen, b, blen);
}

// a = b - a
template<typename small>
inline small subi_on_neg(small* a, index alen, const small* b, index blen) {
   return tsub_on<small, true, true>(a, alen, b, blen);
}

template<typename small>
inline small subu_on_neg(small* a, index alen, const small* b, index blen) {
   return tsub_on<small, true, false>(a, alen, b, blen);
}


// calculate proper high word after addition from addends and 'of' return value

template<typename small>
inline small sub_of_word(const bool asign, const bool bsign, small& of) {
   caassert(of <= 1);
   small r;
   if (asign == false && bsign == false) {
      r = 0 - of;
   }
   else if (asign == false && bsign == true) {
      caassert(of == 1);
      r = 0;
   }
   else if (asign == true && bsign == false) {
      caassert(of == 0);
      r = -1;
   }
   else {
      r = 0 - of;
   }
   return r;
}


// manually unrolled version of the first sub loop on small's.
// returns the carry.

template<typename small>
small __fastcall sub_unrolled(small* r, const small* a, const small* b, const index len);


// fast version of inner sub loop.  is used by subi/u().
// for easy use of unrolled or asm versions of sub
template<typename small>
inline small sub_fast(small* r, const small* a, const small* b, const index len) {
   caassert(r != nullptr);
   caassert(a != nullptr);
   caassert(b != nullptr);

#if defined _M_X86 && defined BN_SUB_USE_ASMX86
   if (sizeof(small) == sizeof(uint32)) return sub_asmx86((uint32*)r, (uint32*)a, (uint32*)b, len);
   else
#elif defined _M_X64 && defined BN_SUB_USE_ASMX64
   if (sizeof(small) == sizeof(uint64)) return sub_ptr((uint64*)r, (uint64*)a, (uint64*)b, len);
   else
#endif
   return sub_unrolled(r, a, b, len);
}


template<typename small>
dword selftest_sub();


// ------ ADD/SUB -------------------------------------------------------------

// this will calculate the sum or difference of two input values.
// sum or difference are decided through extra supplied sign bits.
// the result is >= 0 (the absolute value of the operation) and the returned sign bit reflects if the result would have been negative.

template<typename small>
__forceinline bool add_or_subu(small* r, index rmax, const small* a, index alen, bool asign, const small* b, index blen, bool bsign) {
   if (asign == bsign) {
      // a + b
      // or -a - b
      addu(r, rmax, a, alen, b, blen);
      return asign;
   }
   else {
      // a - b
      // or b - a
      const int comp = compu_nn(a, alen, b, blen);
#if 0
      if (bsign == false && comp > 0) {  // calc -a+b, a > b
         // we want b-a, but a > b, so result would be negative.  calc a-b and return sign.
         subu(r, rmax, a, alen, b, blen);
         return true;
      }
      else if (bsign == false && comp <= 0) {  // calc -a+b, a <= b
         // we want b-a and a <= b, so result is non-negative.
         subu(r, rmax, b, blen, a, alen);
         return false;
      }
      else if (bsign == true && comp >= 0) {  // calc a-b, a >= b
         // we want a-b and a >= b, so result is non-negative.
         subu(r, rmax, a, alen, b, blen);
         return false;
      }
      else if (bsign == true && comp < 0) {  // calc a-b, a < b
         // we want a-b, but a < b, so result would be negative.  calc b-a and return sign.
         subu(r, rmax, b, blen, a, alen);
         return true;
      }
      else {
         classertm(0, "never happens!");
         return false;
      }
#else
      // optimised version, but less obvious
      if (comp == 0) {
         zero(r, rmax);
         return false;
      }
      if (comp > 0) {  // calc -a+b, a > b  or  calc a-b, a > b
         // we want b-a, but a > b, so result would be negative.  calc a-b and return sign.
         // or, we want a-b and a > b, so result is positive.
         subu(r, rmax, a, alen, b, blen);
         return asign;
      }
      else {  // calc -a+b, a < b  or  calc a-b, a < b
         // we want b-a and a < b, so result is positive.
         // or, we want a-b, but a < b, so result would be negative.  calc b-a and return sign.
         subu(r, rmax, b, blen, a, alen);
         return bsign;
      }
#endif
   }
}

template<typename small>
__forceinline bool add_or_subu_on(small* a, index alen, bool asign, const small* b, index blen, bool bsign) {
   if (asign == bsign) {
      // -a - b (asign == true)
      // or a + b (asign == false)
      addu_on(a, alen, b, blen);
      return asign;
   }
   else {
      const int comp = compu_nn(a, alen, b, blen);
      // optimised version, see above
      if (comp == 0) {
         zero(a, alen);
         return false;
      }
      if (comp > 0) {  // calc -a+b, a > b  or  calc a-b, a > b
         // we want b-a, but a > b, so result would be negative.  calc a-b and return sign.
         // or, we want a-b and a > b, so result is positive.
         subu_on(a, alen, b, blen);
         return asign;
      }
      else {  // calc -a+b, a < b  or  calc a-b, a < b
         // we want b-a and a < b, so result is positive.
         // or, we want a-b, but a < b, so result would be negative.  calc b-a and return sign.
         subu_on_neg(a, alen, b, blen);  // a = b - a
         return bsign;
      }
   }
}



// ------ ADDITION/SUBTRACTION IN ONE ----------------------------------------

template<typename small, bool hassign>
small taddsub(small* sum, small* diff, const small* a, const small* b, index len);

// ignore sign bits - unsigned version
template<typename small>
inline small addsubu(small* sum, small* diff, const small* a, const small* b, index len) {
   caassert(sum != 0);
   caassert(diff != 0);
   caassert(a != 0);
   caassert(b != 0);

#if defined _M_X64 && defined BN_ADDSUB3_USE_ASMX64
   if (sizeof(small) == sizeof(uint64)) {
      return addsub3_asmx64((uint64*)sum, (uint64*)diff, (uint64*)a, (uint64*)b, len);
#elif defined _M_X64 && defined BN_ADDSUB2_USE_ASMX64
   if (sizeof(small) == sizeof(uint64)) {
      return addsub2_asmx64((uint64*)sum, (uint64*)diff, (uint64*)a, (uint64*)b, len);
#elif defined _M_X64 && defined BN_ADDSUB_USE_ASMX64
   if (sizeof(small) == sizeof(uint64)) {
#if !defined NDEBUG && 0
      tape_alloc<small> tmp(4*len);
      small cdiff = 0;
      small csum = 0;
      if (len > 0) {
         copy(tmp+2*len, a, len);
         copy(tmp+3*len, b, len);
         cdiff = subu(tmp.p, len, a, len, b, len);
         csum = addu(tmp.p+len, len, a, len, b, len);
      }
      const small c = addsub_asmx64((uint64*)sum, (uint64*)diff, (uint64*)a, (uint64*)b, len);
      if (len > 0) {
         classert(cdiff == (c >> 1));
         classert(memcmp(diff, tmp.p, len*sizeof(small)) == 0);
         classert(csum == (c & 1));
         classert(memcmp(sum, tmp.p+len, len*sizeof(small)) == 0);
      }
      return c;
#else
      return addsub_asmx64((uint64*)sum, (uint64*)diff, (uint64*)a, (uint64*)b, len);
#endif
   }
   else
#endif
   return taddsub<small, false>(sum, diff, a, b, len);
}


// ------ MULTIPLICATION MAIN ROUTINE -----------------------------------------

// multiplication of two const input numbers 'a' and 'b' into a pre-reserved result 'r'.
// 'r' must be long enough to hold the 'alen+blen' long result.  there is no additional padding.
// length can be zero, numbers don't need to be normalised

template<typename small, bool hassign>
inline void tmul(small* r, const small* a, index alen, const small* b, index blen);


// honour sign bits
template<typename small>
inline void muli_ls(small* r, const small* a, index alen, const small* b, index blen) {
   tmul<small, true>(r, a, alen, b, blen);
}

template<typename small>
inline void muli(small* r, const small* a, index alen, const small* b, index blen) {
   tmul<small, true>(r, a, alen, b, blen);
}

// ignore sign bits - unsigned version
template<typename small>
inline void mulu_ls(small* r, const small* a, index alen, const small* b, index blen) {
   tmul<small, false>(r, a, alen, b, blen);
}

template<typename small>
inline void mulu(small* r, const small* a, index alen, const small* b, index blen) {
   tmul<small, false>(r, a, alen, b, blen);
}


template<typename small>
index mul_mem(const index alen, const index blen) {
   return smul_mem<small, false>(alen, blen);
}

template<typename small>
index squ_mem(const index len) {
   return smul_mem<small, true>(len, len);
}


// ------ MULTIPLICATION with 1 small -----------------------------------------

// manually unrolled version of the inner mul loop on small's.
// length can be zero, numbers don't need to be normalised

template<typename small>
void mul1_unrolled(small* r, const small* a, index len, small b);


// fast version of inner mul loop.  is used by omul().
// for easy use of unrolled or asm versions of add
template<typename small>
inline void mul1_fast(small* r, const small* a, index len, const small b) {
   caassert(r != nullptr);
   caassert(a != nullptr);

#if defined _M_X86 && defined BN_MUL1_USE_ASMX86
   if (sizeof(small) == sizeof(uint32)) mul1_asmx86((uint32*)r, (uint32*)a, len, b);
   else
#elif defined _M_X64 && defined BN_MUL1_USE_ASMX64
   if (sizeof(small) == sizeof(uint64)) mul1_asmx64((uint64*)r, (uint64*)a, len, b);
   else
#endif
   mul1_unrolled(r, a, len, b);
}


// ------ ORDINARY MULTIPLICATION ---------------------------------------------

typedef void (*mulfunc16)(uint16* r, const uint16* a, const index alen, const uint16* b, const index blen);
typedef void (*mulfunc64)(uint64* r, const uint64* a, const index alen, const uint64* b, const index blen);
typedef void (*squfunc64)(uint64* r, const uint64* a, const index alen);
#ifdef _M_X64
extern void (*omul_ptr)(uint64* r, const uint64* a, const index alen, const uint64* b, const index blen);
extern void (*osqu_ptr)(uint64* r, const uint64* a, const index alen);
#endif

// mother of all muls: simple, not used in production
// length can be zero, numbers don't need to be normalised
template<typename small>
void omul_plain(small* r, const small* a, index alen, const small* b, index blen);

// ordinary multiplication with unrolled loops
// length can be zero, numbers don't need to be normalised
template<typename small>
void omul_unrolled(small* r, const small* a, index alen, const small* b, index blen);

// interleaved ordinary multiplication
// length can be zero, numbers don't need to be normalised
template<typename small>
void omul_interleaved(small* r, const small* a, index alen, const small* b, index blen);

// interleaved ordinary multiplication with unrolled loops
// length can be zero, numbers don't need to be normalised
template<typename small>
void omul_interleaved_unrolled(small* r, const small* a, index alen, const small* b, index blen);

// interleaved ordinary multiplication, 2nd version
// length can be zero, numbers don't need to be normalised
template<typename small>
void omul_interleaved2(small* r, const small* a, index alen, const small* b, index blen);

// interleaved ordinary multiplication, 2nd version, with unrolled loops
// length can be zero, numbers don't need to be normalised
template<typename small>
void omul_interleaved2_unrolled(small* r, const small* a, index alen, const small* b, index blen);

// interleaved ordinary multiplication, 3rd version
// length can be zero, numbers don't need to be normalised
template<typename small>
void omul_interleaved3(small* r, const small* a, index alen, const small* b, index blen);

// multiplication of two const input numbers 'a' and 'b' into a pre-reserved result 'r'.
// both input numbers must be non-negative.
// 'a' must be longer than or equal the size of 'b'.
// 'r' must be long enough to hold the 'alen+blen' long result.  there is no padding.
// length can be zero, numbers don't need to be normalised

template<typename small>
void omul_ls(small* r, const small* a, index alen, const small* b, index blen) {
   caassert(r != 0);
   caassert(a != 0);
   caassert(b != 0);
   caassert(alen >= blen);

   PROFILE_PARAMS2("omul_ls", alen, blen);

   if (blen == 0) {
      zero(r, alen+blen);
      return;
   }

/*#if defined _M_X86 && defined BN_OMUL_USE_ASMX86
   if (sizeof(small) == sizeof(uint32)) omul_asmx86((uint32*)r, (uint32*)a, alen, (uint32*)b, blen);
   else
#elif defined _M_X64 && defined BN_OMUL3_USE_ASMX64
   if (sizeof(small) == sizeof(uint64)) omul3_asmx64((uint64*)r, (uint64*)a, alen, (uint64*)b, blen);
   else
#elif defined _M_X64 && defined BN_OMUL2_USE_ASMX64
   if (sizeof(small) == sizeof(uint64)) omul2_asmx64((uint64*)r, (uint64*)a, alen, (uint64*)b, blen);
   else
#elif defined _M_X64 && defined BN_OMUL_USE_ASMX64
   if (sizeof(small) == sizeof(uint64)) omul_asmx64((uint64*)r, (uint64*)a, alen, (uint64*)b, blen);
   else
#endif
   omul_unrolled(r, a, alen, b, blen);*/
#ifdef _M_X64
   if (sizeof(small) == sizeof(uint64)) omul_ptr((uint64*)r, (uint64*)a, alen, (uint64*)b, blen);
   else
#endif
   omul_unrolled(r, a, alen, b, blen);
}

// don't care which operant is longer...
template<typename small>
inline void omul(small* r, const small* a, index alen, const small* b, index blen) {
   if (alen >= blen) omul_ls<small>(r, a, alen, b, blen);
   else omul_ls<small>(r, b, blen, a, alen);
}

index omul_mem(const index alen, const index blen);

template<typename small>
dword selftest_mul();

// ------ square

template<typename small>
void osqu_plain(small* r, const small* a, index alen);

template<typename small>
void osqu(small* r, const small* a, index alen) {
#ifdef _M_X64
   if (sizeof(small) == sizeof(uint64)) osqu_ptr((uint64*)r, (uint64*)a, alen);
   else
#endif
   osqu_plain(r, a, alen);
}

template<typename small, bool square>
inline void qomul(small* r, const small* a, index alen, const small* b, index blen) {
   caassert(!square || a == b);
   caassert(!square || alen == blen);
   if (square) osqu<small>(r, a, alen);
   else omul_ls<small>(r, a, alen, b, blen);
}


// alternative squaring by multiplication
void osqu_omul_asmx64(uint64* r, const uint64* a, index alen);
void osqu_omul2_asmx64(uint64* r, const uint64* a, index alen);
void osqu_omul3_asmx64(uint64* r, const uint64* a, index alen);
void osqu_omul4_asmx64(uint64* r, const uint64* a, index alen);


// ------ KARATSUBA MULTIPLICATION --------------------------------------------

// unsigned multiplication of two const input numbers 'a' and 'b' into a pre-reserved result 'r'.
// 'a' must be longer than or equal the size of 'b'.
// 'r' must be long enough to hold the 'alen+blen' long result.  there is no padding.
// length can be zero, numbers don't need to be normalised

// let's call 'a' composed of a1*2^n+a0 and 'b' composed of b1*2^n+b0.
// then we look for r2 = a1*b1, r1 = a1*b0 + a0*b1, r0 = a0*b0.
// if we have r0 and r2, we can calculate r1 as (a1 + a0)(b1 + b0) - r2 - r0.

template<typename small, bool square>
void kmul_ls(small* r, const small* a, index alen, const small* b, index blen);

const index kmul_min_len = 4;
const double log2_3 = 1.584962501;
extern index kmul_thresh;
extern index kmul_thresh_init;
extern index ksqu_thresh;
extern index ksqu_thresh_init;

__forceinline bool use_kmul(const index alen, const index blen) {
   caassert(alen >= blen);
   return blen >= kmul_thresh;
}

__forceinline bool use_ksqu(const index alen) {
   return alen >= ksqu_thresh;
}

template<bool square>
__forceinline bool use_qkmul(const index alen, const index blen) {
   caassert(!square || alen == blen);
   return square ? use_ksqu(alen) : use_kmul(alen, blen);
}

const index always_on = 0;
const index always_off = (index)-1;

index _CL_CALL switch_kmul(index set);
index _CL_CALL switch_ksqu(index set);

// don't care which operant is longer...
template<typename small, bool square>
inline void qkmul(small* r, const small* a, index alen, const small* b, index blen) {
   caassert(!square || a == b);
   caassert(!square || alen == blen);
   if (alen >= blen) kmul_ls<small, square>(r, a, alen, b, blen);
   else kmul_ls<small, square>(r, b, blen, a, alen);
}

template<typename small>
inline void kmul(small* r, const small* a, index alen, const small* b, index blen) {
   qkmul<small, false>(r, a, alen, b, blen);
}

template<typename small>
inline void ksqu(small* r, const small* a, index alen) {
   qkmul<small, true>(r, a, alen, a, alen);
}

// calc mem for KARATSUBA MUL
template<typename small, bool square>
index kmul_mem(const index alen, const index blen);


// ------ TOOM-COOK 3 MULTIPLICATION ------------------------------------------

// unsigned multiplication of two const input numbers 'a' and 'b' into a pre-reserved result 'r'.
// 'a' must be longer than or equal the size of 'b'.
// 'r' must be long enough to hold the 'alen+blen' long result.  there is no padding.
// length MUST NOT be zero, numbers don't need to be normalised

template<typename small, bool square>
void qt3mul_ls(small* r, const small* a, index alen, const small* b, index blen);

const index t3mul_min_len = 11;  // 3 works, 4 makes ahlen==0, (5 might become 4 after normu1), 5, 7, and 10 will not assert 2*hlen >= llen + 1
const double log3_5 = 1.46497352;
extern index t3mul_thresh;
extern index t3mul_thresh_init;
extern index t3squ_thresh;
extern index t3squ_thresh_init;

template<bool square>
inline bool use_qt3mul(const index alen, const index blen) {
   caassert(!square || alen == blen);
   return square ? use_t3squ(alen) : use_t3mul(alen, blen);
}

inline bool use_t3mul(const index alen, const index blen) {
   caassert(alen >= blen);
   return blen >= t3mul_thresh;
}

inline bool use_t3squ(const index len) {
   return len >= t3squ_thresh;
}

index _CL_CALL switch_t3mul(index set);
index _CL_CALL switch_t3squ(index set);

// don't care which operant is longer...
template<typename small, bool square>
inline void qt3mul(small* r, const small* a, index alen, const small* b, index blen) {
   caassert(!square || a == b);
   caassert(!square || alen == blen);
   if (alen >= blen) qt3mul_ls<small, square>(r, a, alen, b, blen);
   else qt3mul_ls<small, square>(r, b, blen, a, alen);
}

template<typename small>
inline void t3mul(small* r, const small* a, index alen, const small* b, index blen) {
   qt3mul<small, false>(r, a, alen, b, blen);
}

template<typename small>
inline void t3squ(small* r, const small* a, index alen) {
   qt3mul_ls<small, true>(r, a, alen, a, alen);
}

// calc mem for TOOM-COOK 3 MUL
template<typename small, bool square>
index t3mul_mem(const index alen, const index blen);

// estimate t3mul() time
uint64 _CL_CALL estimate_t3mul_time(index len);

// multiply mod 2^n+1
template<typename small, bool square>
void qt3mul_mod(small* r, const small* a, const small* b, const index len, const index n);

template<typename small>
void t3mul_mod(small* r, const small* a, const small* b, const index len, const index n) {
   qt3mul_mod<small, false>(r, a, b, len, n);
}

template<typename small>
void t3squ_mod(small* r, const small* a, const index len, const index n) {
   qt3mul_mod<small, true>(r, a, a, len, n);
}

template<typename small>
dword _CL_CALL selftest_t3mul();


// ------ QUICKMUL: MODULAR FFT MULTIPLICATION --------------------------------

// unsigned multiplication of two const input numbers 'a' and 'b' into a pre-reserved result 'r'.
// 'r' must be long enough to hold the 'alen+blen' long result.  there is no padding.
// length can be zero, numbers don't need to be normalised

template<typename small, bool square>
void qmul_tsq(small* r, const small* a, index alen, const small* b, index blen);

const index qmul_min_len = 2;
extern index qmul_thresh;
extern index qmul_thresh_init;
extern index qsqu_thresh;
extern index qsqu_thresh_init;

__forceinline bool use_qmul(const index alen, const index blen) {
   return alen >= qmul_thresh && blen >= qmul_thresh;
}

__forceinline bool use_qsqu(const index alen) {
   return alen >= qsqu_thresh;
}

template<bool square>
__forceinline bool use_qmul_tsq(const index alen, const index blen) {
   caassert(!square || alen == blen);
   return square ? use_qsqu(alen) : use_qmul(alen, blen);
}

index _CL_CALL switch_qmul(index set);
index _CL_CALL switch_qsqu(index set);

template<typename small>
inline void qmul(small* r, const small* a, index alen, const small* b, index blen) {
   qmul_tsq<small, false>(r, a, alen, b, blen);
}

template<typename small>
inline void qsqu(small* r, const small* a, index alen) {
   qmul_tsq<small, true>(r, a, alen, a, alen);
}

// calc mem for QUICKMUL
template<typename small, bool square>
index qmul_mem(const index alen, const index blen);

template<typename small>
bool qmul_set_params(const index rlen, unsigned& depth, index& cut_bits);

template<typename small>
void qmul_print_params(const index bit);

template<typename small>
small f_modmul(const small u, const small v);
template<typename small>
small f_modadd(const small u, const small v);
template<typename small>
small f_modsub(const small u, const small v);



// ------ SCH�NHAGE-STRASSEN FFT MUL -----------------------------------------

template<typename small>
void smul_print_params(const index bit, const unsigned fft_lg, const bool square, const bool optimise, uint64* iter_time=0);

template<typename small>
bool smul_set_params(const bool square, const index alen, const index blen, unsigned& lg, index& mm, index& l, index& k);

template<typename small>
bool smul_set_params2(const index ll, const unsigned lg, index& mm, index& l, index& k);

template<typename small>
void smul_mul_pow2(small* r, const small* a, const index alen, const index k, const index shift_count);

template<typename small>
void smul_addsub_mul(small* v1, small* v2, const index k, const index kk, const index shift_count);

// main worker routine - template parameter square
template<typename small, bool square>
void qsmul2(small* r, const index rlen, const small* a, const index alen, const small* b, const index blen, const unsigned m, const index mm, const index l, const index k, const bool negacyclic);

// main worker routine - mul version
template<typename small>
void smul2(small* r, const index rlen, const small* a, const index alen, const small* b, const index blen, const unsigned m, const index mm, const index l, const index k, const bool negacyclic) {
   qsmul2<small, false>(r, rlen, a, alen, b, blen, m, mm, l, k, negacyclic);
}

// main worker routine - square version
template<typename small>
void ssqu2(small* r, const index rlen, const small* a, const index alen, const unsigned m, const index mm, const index l, const index k, const bool negacyclic) {
   qsmul2<small, true>(r, rlen, a, alen, a, alen, m, mm, l, k, negacyclic);
}

// external wrapper
template<typename small, bool square>
void smul_ls(small* r, const small* a, const index alen, const small* b, const index blen);

// don't care which operant is longer...
template<typename small>
inline void smul(small* r, const small* a, index alen, const small* b, index blen) {
   if (alen >= blen) smul_ls<small, false>(r, a, alen, b, blen);
   else smul_ls<small, false>(r, b, blen, a, alen);
}

template<typename small, bool square>
inline void smul_tsq(small* r, const small* a, index alen, const small* b, index blen) {
   if (alen >= blen || square) smul_ls<small, square>(r, a, alen, b, blen);
   else smul_ls<small, square>(r, b, blen, a, alen);
}

template<typename small>
inline void ssqu(small* r, const small* a, index alen) {
   smul_ls<small, true>(r, a, alen, a, alen);
}

extern index smul_thresh;
extern index smul_thresh_init;

inline bool use_smul(const index alen, const index blen) {
   caassert(alen >= blen);
   return blen >= smul_thresh;
}

extern index ssqu_thresh;
extern index ssqu_thresh_init;

inline bool use_ssqu(const index len) {
   return len >= ssqu_thresh;
}

template<bool square>
inline bool use_qsmul(const index alen, const index blen) {
   caassert(alen >= blen);
   return square ? use_ssqu(alen) : use_smul(alen, blen);
}

index _CL_CALL switch_smul(index set);
index _CL_CALL switch_ssqu(index set);

template<typename small>
index smul_mem2(const index ll, const index mm, const index kk, bool square=false, bool negacyclic=false);
template<typename small, bool square>
index smul_mem(const index alen, const index blen, bool negacyclic);

template<typename small, bool square>
index smul_mem(const index alen, const index blen) {
   return smul_mem<small, square>(alen, blen, false);
}

uint64 _CL_CALL estimate_smul_time(index len);

template<typename small>
unsigned smul_optimise(const index sz, const bool square, const bool print, index* kret=0, uint64* iter_time=0);

// ------ SMUL mod 2^K+1

// ------ choose smul params based on given ring size
template<typename small>
void smul_mod_set_params2(const index n, const unsigned& lg, index& mm, index& l, index& k);

template<typename small>
bool smul_mod_params(const index n, const bool square, unsigned& lg, index& mm, index& l, index& k);

template<typename small>
void smul_mod_print_params2(index n, const bool square, const bool optimise);

// ------ save optimised parameters
struct smul_params {
   index sz;
   unsigned lg;
   index k;
   smul_params(const index _sz, const unsigned _lg, const index _k) : sz(_sz), lg(_lg), k(_k) { }
};

extern vector<smul_params> smul_opt;
extern vector<smul_params> ssqu_opt;

inline vector<smul_params>& qsmul_opt(bool square) {
   return square ? ssqu_opt : smul_opt;
}


// ------ DKSS FFT MUL --------------------------------------------------------

template<typename small, bool square>
void q_dkss_mul(small* r, const small* a, const index alen, const small* b, const index blen);

template<typename small, bool square>
void q_dkss_mul1(small* r, const small* a, const index alen, const small* b, const index blen);

extern index dkss_mul_thresh;
extern index dkss_mul_thresh_init;

inline bool use_dkss_mul(const index alen, const index blen) {
   caassert(alen >= blen);
   return blen >= dkss_mul_thresh;
}

const index dkss_mul_min_len = 2;
extern index dkss_squ_thresh;
extern index dkss_squ_thresh_init;

inline bool use_dkss_squ(const index len) {
   return len >= dkss_squ_thresh;
}

template<bool square>
inline bool q_use_dkss_mul(const index alen, const index blen) {
   caassert(alen >= blen);
   return square ? use_dkss_squ(alen) : use_dkss_mul(alen, blen);
}

template<typename small>
void dkss_mul(small* r, const small* a, const index alen, const small* b, const index blen) {
   q_dkss_mul<small, false>(r, a, alen, b, blen);
}

template<typename small>
void dkss_squ(small* r, const small* a, const index alen) {
   q_dkss_mul<small, true>(r, a, alen, a, alen);
}

index _CL_CALL switch_dkss_mul(index set);
index _CL_CALL switch_dkss_squ(index set);

template<typename small, bool square>
index q_dkss_mul_mem(const index alen, const index blen);


// ------ SQUARING ------------------------------------------------------------

// squaring of a const input number 'a' into a pre-reserved result 'r'.
// 'r' must be long enough to hold the '2*alen' long result.  there is no padding.
// length can be zero, numbers don't need to be normalised

template<typename small, bool hassign>
void tsquare(small* r, const small* a, index alen);


// honour sign bits
template<typename small>
inline void squarei(small* r, const small* a, index alen) {
   tsquare<small, true>(r, a, alen);
}

// ignore sign bits - unsigned version
template<typename small>
inline void squareu(small* r, const small* a, index alen) {
   tsquare<small, false>(r, a, alen);
}


// ------ DIVISION -----------------------------------------------------------

template<typename small>
void div_simple(small* q, index qmax, small* rr, index rmax, const small* a, index alen, const small* b, index blen);

template<typename small>
void div_knuth(small* q, index qmax, small* r, index rmax, const small* a, index alen, const small* b, index blen);

template<typename small, bool hassign>
bool tdiv(small* q, index qmax, small* r, index rmax, const small* a, index alen, const small* b, index blen);

template<typename small>
static bool divu(small* q, index qmax, const small* a, index alen, const small* b, index blen) {
   return tdiv<small, false>(q, qmax, nullptr, 0, a, alen, b, blen);
}

// r may overlap a or b
template<typename small>
static bool modu(small* r, index rmax, const small* a, index alen, const small* b, index blen) {
   return tdiv<small, false>(nullptr, 0, r, rmax, a, alen, b, blen);
}


// ------ DIVISION by constant small ------------------------------------------

// division of a const input number by a small constant
// length can be zero, numbers don't need to be normalised

template<typename small>
void div_by3(small* q, const small* a, index alen);

template<typename small>
void div_by3_2(small* q, const small* a, index alen);

template<typename small>
void div_by3_3(small* q, const small* a, index alen);

template<typename small>
void div_by3_fast(small* q, const small* a, index alen) {
#if defined _M_X64 && defined BN_DIV_BY3_USE_ASMX64
   if (sizeof(small) == sizeof(uint64)) div_by3_asmx64((uint64*)q, (uint64*)a, alen);
   else
#endif
   div_by3_3(q, a, alen);
}


// ------ DIVISION by 1 small ------------------------------------------------

// division of a const input number by a small
// length can be zero, numbers don't need to be normalised

template<typename small>
small div1(small* q, const small* a, index alen, small b);

template<typename small>
small div1(small* q, index qmax, const small* a, index alen, small b);

template<typename small>
dword selftest_div();


// ------ NORMALISE -----------------------------------------------------------

template<typename small, bool hassign>
index tnorm(const small* p, index len);

template<typename small>
inline index normi(const small* p, index len) {
   return tnorm<small, true>(p, len);
}

template<typename small>
inline index normu(const small* p, index len) {
   return tnorm<small, false>(p, len);
}

// cut off the uppermost small or not.  len MUST NOT be zero.
template<typename small>
index normu1(const small* p, index len) {
   caassert(p != nullptr);
   caassert(len > 0);

   return p[len-1] == 0 ? len - 1 : len;
}


// ------ NEGATION ------------------------------------------------------------

template<typename small>
void neg(small* r, const small* a, index len);

// in-place
template<typename small>
void neg_on(small* r, index len) {
   // improve: code real in-place variant
   neg<small>(r, r, len);
}


// ------ COMPARISON ----------------------------------------------------------

template<typename small, bool hassign, bool isnormal>
int tcomp(const small* a, index alen, const small* b, index blen);


template<typename small>
inline int compi_nn(const small* a, index alen, const small* b, index blen) {
   return tcomp<small, true, false>(a, alen, b, blen);
}

template<typename small>
inline int compi1_nn(const small* a, index alen, const small b) {
   return tcomp<small, true, false>(a, alen, &b, 1);
}

template<typename small>
inline int compi(const small* a, index alen, const small* b, index blen) {
   return tcomp<small, true, true>(a, alen, b, blen);
}

template<typename small>
inline int compu_nn(const small* a, index alen, const small* b, index blen) {
   return tcomp<small, false, false>(a, alen, b, blen);
}

template<typename small>
inline int compu1_nn(const small* a, index alen, const small b) {
   return tcomp<small, false, false>(a, alen, &b, 1);
}

template<typename small>
inline int compu(const small* a, index alen, const small* b, index blen) {
   return tcomp<small, false, true>(a, alen, b, blen);
}

template<typename small>
inline int compu1(const small* a, index alen, const small b) {
   return tcomp<small, false, true>(a, alen, &b, b == 0 ? 0 : 1);
}


// ------ SHIFT RIGHT ---------------------------------------------------------

// shift value to the right, i.e. 110110 >> 1 yields 11011.
// shr() should be ok if numbers overlap

template<typename small>
void shr(small* r, const index rmax, const small* a, const index alen, const bit_index shift_bits);

template<typename small>
void shr_on(small* a, const index alen, const bit_index shift_bits);


// ------ SHIFT LEFT ----------------------------------------------------------

// shift value to the left, i.e. 110110 << 1 yields 1101100.
// shl() must not be used if numbers overlap!

template<typename small>
void shl(small* r, const index rmax, const small* a, const index alen, const bit_index shift_bits);

template<typename small>
void shl_on(small* a, const index alen, const bit_index shift_bits);

template<typename small>
void shl1_on(small* a, const index alen) {
   // add_fast is faster than shl for now
   // improve: provide fast shl(x, 1) routine
   addu_on(a, alen, a, alen);
   //shl_on(a, alen, 1);
}


// ------ BIT ACCESS ----------------------------------------------------------

template<typename small>
bool _is_bit_set(small a, index bit_nb) {
   caassert(bit_nb < bits(a));
   const small mask = (small)1 << bit_nb;
   return (a & mask) != 0;
}

template<typename small>
bool is_bit_set(const small* a, bit_index bit_nb) {
   caassert(a != nullptr);
   caassert(bit_nb / bits(small) < numeric_limits<index>::max());
   const index off = bit_nb / bits(small);
   const index bit = bit_nb % bits(small);
   return _is_bit_set(a[off], bit);
}

template<typename small>
bool is_bit_set(const small* a, index alen, bit_index bit_nb) {
   caassert(a != nullptr);
   caassert(bit_nb / bits(small) < numeric_limits<index>::max());
   const index off = bit_nb / bits(small);
   if (off >= alen) return false;
   const index bit = bit_nb % bits(small);
   return _is_bit_set(a[off], bit);
}


template<typename small>
void _set_bit(small &a, index bit_nb) {
   caassert(bit_nb < bits(a));
   const small mask = (small)1 << bit_nb;
   a |= mask;
}

template<typename small>
void set_bit(small* a, bit_index bit_nb) {
   caassert(a != nullptr);
   caassert(bit_nb / bits(small) < numeric_limits<index>::max());
   const index off = bit_nb / bits(small);
   const index bit = bit_nb % bits(small);
   _set_bit(a[off], bit);
}

template<typename small>
void set_bit(small* a, index alen, bit_index bit_nb) {
   caassert(a != nullptr);
   caassert(bit_nb / bits(small) < numeric_limits<index>::max());
   const index off = bit_nb / bits(small);
   classert(off < alen);
   if (off >= alen) return;  // safety
   const index bit = bit_nb % bits(small);
   _set_bit(a[off], bit);
}


template<typename small>
void _clear_bit(small &a, index bit_nb) {
   caassert(bit_nb < bits(a));
   const small mask = (small)1 << bit_nb;
   a &= ~mask;
}

template<typename small>
void clear_bit(small* a, bit_index bit_nb) {
   caassert(a != nullptr);
   caassert(bit_nb / bits(small) < numeric_limits<index>::max());
   const index off = bit_nb / bits(small);
   const index bit = bit_nb % bits(small);
   _clear_bit(a[off], bit);
}

template<typename small>
void clear_bit(small* a, index alen, bit_index bit_nb) {
   caassert(a != nullptr);
   caassert(bit_nb / bits(small) < numeric_limits<index>::max());
   const index off = bit_nb / bits(small);
   classert(off < alen);
   if (off >= alen) return;  // safety
   const index bit = bit_nb % bits(small);
   _clear_bit(a[off], bit);
}


template<typename small>
bit_index bit_length(const small* a, index alen) {
   caassert(a != nullptr);
   alen = normu(a, alen);
   if (alen == 0) return 0;
   const small w = a[alen-1];
   caassert(w != 0);
   unsigned idx;
   bool b = asm_bsr(w, idx);
   caassert(b);
   return (bit_index)(alen - 1) * bits(small) + idx + 1;
}


// ------ BITWISE AND ---------------------------------------------------------

template<typename small>
void and_ls(small* r, index rmax, const small* a, index alen, const small* b, index blen);

// don't care which operant is longer...
template<typename small>
inline void and(small* r, index rmax, const small* a, index alen, const small* b, index blen) {
   if (alen >= blen) and_ls<small>(r, rmax, a, blen, b, blen);
   else and_ls<small>(r, rmax, b, alen, a, alen);
}

template<typename small>
inline void and_on(small* r, index rmax, const small* a, index alen) {
   // improve: code real in-place variant
   and_ls<small>(r, rmax, r, rmax, a, tmin(alen, rmax));
}


// ------ BITWISE OR ----------------------------------------------------------

template<typename small>
void or_ls(small* r, index rmax, const small* a, index alen, const small* b, index blen);

// don't care which operant is longer...
template<typename small>
inline void or(small* r, index rmax, const small* a, index alen, const small* b, index blen) {
   if (alen >= blen) or_ls<small>(r, rmax, a, alen, b, blen);
   else or_ls<small>(r, rmax, b, blen, a, alen);
}

template<typename small>
inline void or_on(small* r, index rmax, const small* a, index alen) {
   // improve: code real in-place variant
   caassert(rmax >= alen);
   or_ls<small>(r, rmax, r, rmax, a, alen);
}


// ------ SET ALL ONES --------------------------------------------------------

// create a value of 2^n-1, that is n 1-bits.

template<typename small>
void set_all_ones(small* r, const index rmax, const bit_index n);


// ------ MODULO 2^n-1 --------------------------------------------------------

template<typename small>
void mod_pow2m1_on(small* r, const index len, const bit_index n);


// ------ MODULO 2^n+1 --------------------------------------------------------

template<typename small>
void mod_pow2p1_aligned_on(small* r, index rlen, const bit_index n);


// ------ compare v with 2^k --------------------------------------------------

template<typename small>
int cmp_pow2(const small* v, index len, const bit_index k);


// ------ MATRIX TRANSPOSE ---------------------------------------------------

template<bool ispow2>
void _CL_CALL mem_transpose_in_place(void* _a, const index m, const index n, const size_t sz);

template<typename small>
void transpose_in_place_odd(small* a, const index m, const index n, const index psz=1) {
   mem_transpose_in_place<false>(a, m, n, sizeof(small)*psz);
}

template<typename small>
void transpose_in_place_pow2(small* a, const index m, const index n, const index psz=1) {
   mem_transpose_in_place<true>(a, m, n, sizeof(small)*psz);
}


// ------ ALLOCATION ----------------------------------------------------------

extern size_t alloc_cnt;

template<typename small>
small* bignum_alloc(index sz);

template<typename small>
void bignum_free(small* p);

void bignum_lal_clear();

struct rand_maker {
   mt19937_64 generator;
   template<typename small> small get() {
      uniform_int_distribution<small> distribution;
      return distribution(generator);
   }
   void seed(unsigned long v) {
      generator.seed(v);
   }
};
extern rand_maker make_rand;

template<typename small>
void _CL_CALL rand_fill(small* p, index len);

template<typename small>
small* _CL_CALL rand_alloc(index len);

#ifdef _M_X64
const size_t bignum_align = 32;  // for AVX
#else
const size_t bignum_align = 4;
#endif


// numbers are stored little-endian (intel-style) with their least significant unit first.
// the most significant unit has its top bit as sign bit.
// i.e. with 16-bit unsigned as small one must store [0x8000, 0] in order to represent 0x8000, because just [0x8000] would be interpreted as -0x7fff

// for allocation each number is preceded with this header.
// if we want to pad the 'v' array for alignment reasons, the padding is in front of the header.
// save the number of padding 'small's to front_pad, so we can recover the proper pointer for deallocation.

struct bignum_head {
   index refcnt;
   //index front_pad;
   index maxlen;
   index len;
   //small v[1];  // arbitrary long
};
const size_t bignum_aligned_head_sz = (sizeof(bignum_head) + bignum_align - 1) / bignum_align * bignum_align;


// ------ TAPE ALLOCATION -----------------------------------------------------

struct tape {
   size_t len;
   size_t used;
   byte* p;
   byte* end;

   tape(const size_t _len);
   tape(bool);  // just for init
   ~tape();
   size_t free() const {
      return len - used;
   }
   string print_layout() const;
};

struct tape_mgr {
   static const size_t init_max_total_sz = 64*1024;
   vector<tape*> tapes;
   size_t tape_idx;
   size_t total_sz;
   size_t max_total_sz;

   tape_mgr();
   ~tape_mgr();
   byte* alloc(const size_t sz, byte*& old);
   void unroll(byte* old);
   void free_unused_tapes();
   void free_all_tapes();
   void free_last_tape();
   void set_max_total_sz(size_t new_sz);
   void reset_max_total_sz();
   void print_tapes(const tracer& t) const;
};
extern tape_mgr tapemgr;

template<typename small>
struct tape_alloc {
   byte* old;
   small* p;

   tape_alloc(const index len) {  // in small's
      p = (small*)tapemgr.alloc(len * sizeof(small), old);
   }
   ~tape_alloc() {
      tapemgr.unroll(old);
   }
   operator small*() const {
      return p;
   }
};

template<typename small>
struct tape_alloc_maybe {
   byte* old;
   small* p;

   tape_alloc_maybe(bool condition, const index len) {  // in small's
      if (condition) p = (small*)tapemgr.alloc(len * sizeof(small), old);
      else {
         old = nullptr;
         p = nullptr;
      }
   }
   ~tape_alloc_maybe() {
      if (old != nullptr) tapemgr.unroll(old);
   }
   operator small*() const {
      return p;
   }
};


// ------ physical memory -----------------------------------------------------

size_t _CL_CALL physical_mem(bool available=true);  // total = false


// ------ UTILITY -------------------------------------------------------------

template<typename small, bool hassign>
bool is_negative(const small* a, const index len) {
   return hassign && len > 0 && top_set(a[len-1]);
}

template<typename small>
bool is_negative(const small* a, const index len, const bool hassign) {
   return hassign && len > 0 && top_set(a[len-1]);
}


template<typename small, bool hassign>
void tsignx(small* r, index len, index nlen) {
   caassert(r != 0);
#if 0
   if (len < nlen) {
      bool rsign = is_negative<small, hassign>(r, len);
      while (len < nlen)
         r[len++] = rsign ? (small)-1 : 0;
   }
#else
   if (len < nlen) {
      bool rsign = is_negative<small, hassign>(r, len);
      memset(r+len, rsign ? 0xff : 0, (nlen - len) * sizeof(small));
   }
#endif
}


template<typename small>
void zero(small* r, const index len, index i=0) {
   tsignx<small, false>(r, i, len);
}

template<typename small>
__forceinline small* zero_in(small* r, const index len, index i=0) {
   caassert(r != nullptr);
   while (i < len)
      r[i++] = 0;
   return r + len;
}


template<typename small>
void one(small* r, const index len, index i=0) {
   caassert(r != 0);
#if 0
   while (i < len)
      r[i++] = -1;
#else
   if (i < len)
      memset(r+i, 0xff, (len - i) * sizeof(small));
#endif
}

template<typename small>
__forceinline void one_in(small* r, const index len, index i=0) {
   caassert(r != 0);
   while (i < len)
      r[i++] = (small)-1;
}


template<typename small>
void copy(small* r, const small* a, const index len, index i=0) {
   caassert(r != 0);
   caassert(a != 0);
#if 0
   while (i < len)
      r[i++] = a[i];
#else
   if (i < len)
      memcpy(r+i, a+i, (len - i) * sizeof(small));
#endif
}


template<typename small>
__forceinline void copy_in(small* r, const small* a, const index len, index i=0) {
   caassert(r != 0);
   caassert(a != 0);
   while (i < len)
      r[i++] = a[i];
}


template<typename small>
void copy_rev(small* r, const small* a, const index len) {
   caassert(r != 0);
   caassert(a != 0);
   index i = len;
   while (1) {
      if (i == 0) return;
      --i;
      r[i] = a[i];
   }
}

template<typename small>
bool is_zero(const small* r, const index len, index i=0) {
   caassert(r != 0);
   while (i < len)
      if (r[i++] != 0) return false;
   return true;
}


// ------ memswap -------------------------------------------------------------

template<typename small>
__forceinline void swap_in(small *a, small *b, const index len) {
   for (index l=0; l<len; ++l) {
      const small t = a[l];
      a[l] = b[l];
      b[l] = t;
   }
}


// ------ HEX DUMP ------------------------------------------------------------

template<typename small>
string dump_hex(const small* a, index len);
template<typename small>
string dump_poly_hex(const small* a, const index sz, const index len);  // dump polynomial with 'sz' coeffs, each 'len' long


// ------ SELFTEST ------------------------------------------------------------

dword _CL_CALL bignum_selftest();

#ifdef CL_TEST_CODE
void _CL_CALL test_bignum();
#endif


// ------ LUCAS-LEHMER PRIMALITY TEST -----------------------------------------

// test if 2^p-1 is prime

template<typename small>
bool lucas_lehmer_test(const index p);

template<typename small>
class lucas_lehmer {
protected:
   index p;
   void (*squfunc)(small* r, const small* a, const index alen);
public:
   lucas_lehmer(void (*squfunc)(small* r, const small* a, const index alen) = squareu<small>);
   bool test(const index p, bool& finished);
   virtual bool run_iterations(const index p, index i, small* u, const index ulen, bool& finished);
   virtual bool callback(const index i, const small* v, const index len) { return true; }
   virtual void done(const small* v, const index len) { }
};

extern const unsigned mersenne_prime_exps[];

template<typename small>
dword _CL_CALL selftest_lltest(const unsigned maxp, void (*squfunc)(small* r, const small* a, const index alen) = squareu<small>);


// ------ BENCHMARK -----------------------------------------------------------

struct bench_param {
   unsigned all_sizes;
   double factor;
   double step_sz;
   unsigned dkss_steps;
   unsigned qmul_steps;
   index start_sz;
   index end_sz;
   unsigned half_sized;
   unsigned run_long;
   unsigned add_test;
   unsigned sub_test;
   unsigned mul_test;
   unsigned squ_test;
   unsigned div_test;
   unsigned logic_test;
   unsigned smul_test;
   unsigned list_only;
   unsigned fft_len;
   unsigned verbose;
   bool realtime;
   vector<string> wildcards;
   bench_param();
   bool any_selected() const;
   void inc_all();
};

void _CL_CALL bignum_benchmark(bench_param& bp);
void _CL_CALL bignum_benchmark_half(bench_param& bp);


// ------ CALIBRATION ---------------------------------------------------------

void _CL_CALL xmul_calibrate(unsigned fullness, int what, const bool square);
void _CL_CALL qsmul_calibrate(unsigned fullness, const bool square, const bench_param& bp);
void _CL_CALL qsmul_mod_calibrate(unsigned fullness, const bool square);
void _CL_CALL align_test() ;
bool _CL_CALL set_thresholds();


}  // namespace bignum

#pragma pack(pop)

// make sure the right library flavour is included
#ifdef NTDDK
# ifdef _WIN64
#  ifdef _DEBUG
#   pragma comment(lib, "bignum-x64-dbg-ddk")
#  else
#   pragma comment(lib, "bignum-x64-rel-ddk")
#  endif
# else
#  ifdef _DEBUG
#   pragma comment(lib, "bignum-x86-dbg-ddk")
#  else
#   pragma comment(lib, "bignum-x86-rel-ddk")
#  endif
# endif
#elif defined _MT
# ifdef _USRDLL
#  ifdef _WIN64
#   if defined _DEBUG  // ifdef:_DEBUG
#    pragma comment(lib, "bignum-x64-dbg-dll")
#   else
#    pragma comment(lib, "bignum-x64-rel-dll")
#   endif
#  else
#   if defined _DEBUG  // ifdef:_DEBUG
#    pragma comment(lib, "bignum-x86-dbg-dll")
#   else
#    pragma comment(lib, "bignum-x86-rel-dll")
#   endif
#  endif
# else
#  ifdef _WIN64
#   ifdef _DEBUG
#    pragma comment(lib, "bignum-x64-dbg")
#   else
#    pragma comment(lib, "bignum-x64-rel")
#   endif
#  else
#   ifdef _DEBUG
#    pragma comment(lib, "bignum-x86-dbg")
#   else
#    pragma comment(lib, "bignum-x86-rel")
#   endif
#  endif
# endif
#else
# error single-thread no longer supported!
#endif

#endif  // ndef CL_BIGNUM_IMPL_H
