// $+HEADER$
// 
// Copyright 2015 Christoph Lueders
// 
// This file is part of the BIGNUM Library.
// 
// The BIGNUM Library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
// 
// The BIGNUM Library is distributed in the hope that it will be useful, but 
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public 
// License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with the BIGNUM Library.  If not, see <http://www.gnu.org/licenses/>.
// 
// $-HEADER$

// $Date: 2015-04-24 11:28:16 +0200 (Fr, 24 Apr 2015) $
// $Rev: 26590 $

/*
 *  arbitrary length number arithmetic - ordinary multiplication
 */

#include "precomp.h"
#include "impl.h"
#include "small.h"
#ifndef NDEBUG
#include "cl/format.h"
#endif


// we must switch off "unreachable code" warning, since bool template parameters generate code paths that are never used
#pragma warning (disable:4702)

namespace bignum {

index omul_mem(const index, const index) {
   return 0;
}


// ------ MULTIPLICATION MAIN ROUTINE -----------------------------------------

// multiplication of two const input numbers 'a' and 'b' into a pre-reserved result 'r'.
// 'a' must be longer than or equal the size of 'b'.
// 'r' must be long enough to hold the 'alen+blen' long result.  there is no padding.
// length can be zero, numbers don't need to be normalised

template<typename small, bool hassign>
void tmul(small* r, const small* a, index alen, const small* b, index blen) {
   caassert(r != nullptr);
   caassert(a != nullptr);
   caassert(b != nullptr);

   const index rlen = alen + blen;
   const bool asf = is_negative<small, hassign>(a, alen);
   if (asf) neg_on<small>(const_cast<small*>(a), alen);
   alen = normu(a, alen);
   const bool bsf = is_negative<small, hassign>(b, blen);
   if (bsf) neg_on<small>(const_cast<small*>(b), blen);
   blen = normu(b, blen);

   if (alen >= blen) smul_ls<small, false>(r, a, alen, b, blen);
   else smul_ls<small, false>(r, b, blen, a, alen);
   zero_in(r, rlen, alen + blen);

   if (asf) neg_on<small>(const_cast<small*>(a), alen);
   if (bsf) neg_on<small>(const_cast<small*>(b), blen);
   if ((unsigned)asf ^ (unsigned)bsf) neg_on<small>(r, rlen);
}


// ------ SQUARING MAIN ROUTINE -----------------------------------------------

template<typename small, bool hassign>
void tsquare(small* r, const small* a, index alen) {
   caassert(r != nullptr);
   caassert(a != nullptr);

   // improve: do we really need the neg() here?
   const bool asf = is_negative<small, hassign>(a, alen);
   if (asf) neg_on<small>(const_cast<small*>(a), alen);

   ssqu(r, a, alen);

   if (asf) neg_on<small>(const_cast<small*>(a), alen);
}


// ------ ORDINARY MULTIPLICATION ---------------------------------------------

// unsigned multiplication of two const input numbers 'a' and 'b' into a pre-reserved result 'r'.
// 'a' must be longer than or equal the size of 'b'.
// 'r' must be long enough to hold the 'alen+blen' long result.  there is no padding.
// length can be zero, numbers don't need to be normalised

template<typename small>
void omul_plain(small* r, const small* a, index alen, const small* b, index blen) {
   caassert(r != 0);
   caassert(a != 0);
   caassert(b != 0);
   caassert(alen >= blen);
   caassert((small)-1 > 0);  // must be unsigned type

   if (blen == 0) {
      zero(r, alen+blen);
      return;
   }

   // unrolled inner loop on first element:
   // init the first 'alen' elements in result with product without adding up old contents.
   small of = 0;
   index i = 0;
   const small bb = b[0];
   while (i < alen) {
      of = ops<small>::muladdc(r[i], a[i], bb, 0, of);
      ++i;
   }
   r[i] = of;  // save overflow so the next add in the inner loop works ok.

   // now the main loop
   for (index j=1; j<blen; ++j) {
      small* rm = r + j;
      const small bb = b[j];
      i = 0;
      of = 0;
      while (i < alen) {
         of = ops<small>::muladdc(rm[i], a[i], bb, rm[i], of);  // r[m] already inited
         ++i;
      }
      rm[i] = of;  // save overflow to the next element
   }
}


// interleaved ordinary multiplication
// length can be zero, numbers don't need to be normalised

template<typename small>
void omul_interleaved(small* r, const small* a, index alen, const small* b, index blen) {
   caassert(r != 0);
   caassert(a != 0);
   caassert(b != 0);
   caassert(alen >= blen);
   caassert((small)-1 > 0);  // must be unsigned type

   if (blen == 0) {
      zero(r, alen+blen);
      return;
   }
   caassert(alen > 0);
   caassert(blen > 0);

   // unrolled inner loop on first element:
   // init the first 'alen' elements in result with product without adding up old memory contents.
   small of = 0;
   index i = 0;
   const small bb = b[0];
   while (i < alen) {
      of = ops<small>::muladdc(r[i], a[i], bb, 0, of);
      ++i;
   }
   r[i] = of;  // save overflow so the next add in the inner loop works ok.

   // the main outer loop
   index j = 1;
   small* rm;
   while (j+2 < blen) {  // make sure all three last accesses will fit
      rm = r + j;
      const small bb0 = b[j];
      const small bb1 = b[j+1];
      of = ops<small>::muladdc(rm[0], a[0], bb0, rm[0], 0);  // of:rm[0] = a[0] * b[0] + rm[0]  [max b^2-b]
      // the main inner loop
      i = 0;
      small ofh = 0;
      while (i+1 < alen) {
         // p1h:p1l = a[i] * b[j+1] + rm[i+1]  [max b^2-b]
         small p1l;
         const small p1h = ops<small>::muladdc(p1l, a[i], bb1, rm[i+1], 0);
         // p2h:p2l = a[i+1] * b[j] + of  [max b^2-b]
         small p2l;
         const small p2h = ops<small>::muladdc(p2l, a[i+1], bb0, of, 0);
         // write low sum back to memory
         ofh += ops<small>::addc(rm[i+1], p1l, p2l);  // ofh can at most increase by 1
         // ofh:of = p1h + p2h + ofh  [max 2b]
         ofh = ops<small>::addc(of, p1h, p2h, ofh);
         ++i;
      }
      caassert(j + i + 3 < alen + blen);
      // p1h:p1l = a[i] * b[j+1] + of
      of = ops<small>::muladdc(rm[i+1], a[i], bb1, of, 0);  // [max b^2-b-2]
      of = ops<small>::addc(rm[i+2], of, ofh);  // [max b+1]
      rm[i+3] = of;  // [max 1]
      j += 2;
   }

   // lumpensammler: multiply what's still left
   while (j < blen) {
      rm = r + j;
      const small bb = b[j];
      i = 0;
      of = 0;
      while (i < alen) {
         of = ops<small>::muladdc(rm[i], a[i], bb, rm[i], of);
         ++i;
      }
      rm[i] = of;
      ++j;
   }
}


// interleaved ordinary multiplication, 2nd version
// length can be zero, numbers don't need to be normalised

template<typename small>
void omul_interleaved2(small* r, const small* a, index alen, const small* b, index blen) {
   caassert(r != 0);
   caassert(a != 0);
   caassert(b != 0);
   caassert(alen >= blen);
   caassert((small)-1 > 0);  // must be unsigned type

   if (blen == 0) {
      zero(r, alen+blen);
      return;
   }
   caassert(alen > 0);
   caassert(blen > 0);

   // unrolled inner loop on first element:
   // init the first 'alen' elements in result with product without adding up old memory contents.
   small of1 = 0;
   index i = 0;
   const small bb = b[0];
   while (i < alen) {
      of1 = ops<small>::muladdc(r[i], a[i], bb, 0, of1);
      ++i;
   }
   r[i] = of1;  // save overflow so the next add in the inner loop works ok.

   // the main outer loop
   index j = 1;
   small* rm;
   while (j+1 < blen) {  // make sure two accesses will fit
      rm = r + j;
      const small bb0 = b[j];
      const small bb1 = b[j+1];
      of1 = ops<small>::muladdc(rm[0], a[0], bb0, rm[0], 0);  // of1:rm[0] = a[0] * b[0] + rm[0]
      // the main inner loop
      i = 0;
      small of2 = 0;
      while (i+1 < alen) {
         // a[i] * b[j+1] + rm[i+1] + of1
         register small mx;
         of1 = ops<small>::muladdc(mx, a[i], bb1, rm[i+1], of1);
         // a[i+1] * b[j] + mx + of2
         of2 = ops<small>::muladdc(rm[i+1], a[i+1], bb0, mx, of2);
         ++i;
      }
      caassert(j + 1 + i + 1 < alen + blen);
      // a[i] * b[j+1] + of1 + of2
      of1 = ops<small>::muladdc(rm[i+1], a[i], bb1, of1, of2);
      rm[i+2] = of1;
      j += 2;
   }

   // lumpensammler: multiply what's still left
   while (j < blen) {
      rm = r + j;
      const small bb = b[j];
      i = 0;
      of1 = 0;
      while (i < alen) {
         of1 = ops<small>::muladdc(rm[i], a[i], bb, rm[i], of1);
         ++i;
      }
      rm[i] = of1;
      ++j;
   }
}

// benchmark on Intel(R) Core(TM) i7-3770 CPU @ 3.40GHz, HT, all idle:
// non-unrolled:        32     1K  77.1K   0.0987     39.99     136.37    4.26
// 2-times unrolled:    32     1K  80.2K   0.0990     38.54     131.45    4.10
// 4-times unrolled:    32     1K  82.4K   0.0991     37.56     128.09    4.00
//                      32     1K  83.4K   0.0995     37.30     127.19    3.97
// 4-times unrolled w/init & leftover loop unrolled:
//                      32     1K  86.6K   0.0989     35.70     121.73    3.80

// HT, all working:
// non-unrolled:        32     1K  42.2K   0.0984     72.79     248.22    7.75
// 2-times unrolled:    32     1K  43.8K   0.0991     70.70     241.10    7.53
// 4-times unrolled:    32     1K  45.3K   0.0982     67.69     230.83    7.21

template<typename small>
void omul_interleaved2_unrolled(small* r, const small* a, index alen, const small* b, index blen) {
   caassert(r != 0);
   caassert(a != 0);
   caassert(b != 0);
   caassert(alen >= blen);
   caassert((small)-1 > 0);  // must be unsigned type

   if (blen == 0) {
      zero(r, alen+blen);
      return;
   }
   caassert(alen > 0);
   caassert(blen > 0);

   // unrolled inner loop on first element:
   // init the first 'alen' elements in result with product without adding up old memory contents.
   small of1 = 0;
   index i = 0;
   const small bb = b[0];

#define s(o) of1 = ops<small>::muladdc(r[i+(o)], a[i+(o)], bb, 0, of1)
   UNROLL8(i, alen, 0, init, s);
#undef s

   r[i] = of1;  // save overflow so the next add in the inner loop works ok.

   // the main outer loop
   index j = 1;
   small* rm;
   while (j+1 < blen) {  // make sure two accesses will fit
      rm = r + j;
      const small bb0 = b[j];
      const small bb1 = b[j+1];
      of1 = ops<small>::muladdc(rm[0], a[0], bb0, rm[0], 0);  // of1:rm[0] = a[0] * b[0] + rm[0]
      // the main inner loop
      small of2 = 0;
      register small mx;

      i = 0;
#define s(o) \
      do { \
         of1 = ops<small>::muladdc(mx, a[i+(o)], bb1, rm[i+1+(o)], of1); \
         of2 = ops<small>::muladdc(rm[i+1+(o)], a[i+1+(o)], bb0, mx, of2); \
      } \
      while(0)
      UNROLL4(i, alen, 1, inner, s);
#undef s

      caassert(j + 1 + i + 1 < alen + blen);
      // a[i] * b[j+1] + of1 + of2
      of2 = ops<small>::muladdc(rm[i+1], a[i], bb1, of1, of2);
      rm[i+2] = of2;
      j += 2;
   }

   // lumpensammler: multiply what's still left
   while (j < blen) {
      rm = r + j;
      const small bb = b[j];
      of1 = 0;

      i = 0;
#define s(o) of1 = ops<small>::muladdc(rm[i+(o)], a[i+(o)], bb, rm[i+(o)], of1)
      UNROLL8(i, alen, 0, leftover, s);
#undef s

      rm[i] = of1;
      ++j;
   }
}


template<typename small>
void omul_interleaved3(small* r, const small* a, index alen, const small* b, index blen) {
   caassert(r != 0);
   caassert(a != 0);
   caassert(b != 0);
   caassert(alen >= blen);
   caassert((small)-1 > 0);  // must be unsigned type

   if (blen == 0) {
      zero(r, alen+blen);
      return;
   }
   caassert(alen > 0);
   caassert(blen > 0);

   // inner loop on first element:
   // init the first 'alen' elements in result with product without adding up old memory contents.
   small of1 = 0;
   index i = 0;
   const small bb = b[0];

#define s(o) of1 = ops<small>::muladdc(r[i+(o)], a[i+(o)], bb, 0, of1)
   UNROLL8(i, alen, 0, init, s);
#undef s

   r[i] = of1;  // save overflow so the next add in the inner loop works ok.

   // the main outer loop
   index j = 1;
   small* rm;
   while (j+2 < blen) {  // make sure three accesses will fit
      rm = r + j;
      const small bb0 = b[j];
      const small bb1 = b[j+1];
      const small bb2 = b[j+2];
      small of2 = 0;
      small of3 = 0;
      register small mx;

      of1 = ops<small>::muladdc(rm[0], a[0], bb0, rm[0], 0);  // of1:rm[0] = a[0] * b[0] + rm[0]

      of1 = ops<small>::muladdc(mx, a[0], bb1, rm[1], of1);  // of1:mx = a[0] * b[1] + rm[1] + of1
      of2 = ops<small>::muladdc(rm[1], a[1], bb0, mx, 0);  // of2:rm[1] = a[1] * b[0] + mx

      // the main inner loop
      i = 0;
#define s(o) \
      do { \
         of1 = ops<small>::muladdc(mx, a[i+(o)], bb2, rm[i+2+(o)], of1); \
         of2 = ops<small>::muladdc(mx, a[i+1+(o)], bb1, mx, of2); \
         of3 = ops<small>::muladdc(rm[i+2+(o)], a[i+2+(o)], bb0, mx, of3); \
      } while(0)
      UNROLL4(i, alen, 2, inner, s);
#undef s

      of1 = ops<small>::muladdc(mx, a[i], bb2, of3, of1);  // of1:mx = a[i] * b[2] + of1 + of3
      of2 = ops<small>::muladdc(rm[i+2], a[i+1], bb1, mx, of2);  // of2:rm[i+1] = a[i+1] * b[1] + mx + of2

      of1 = ops<small>::muladdc(rm[i+3], a[i+1], bb2, of1, of2);  // of1:rm[i+2] = a[i+1] * b[2] + of1 + of2
      rm[i+4] = of1;
      j += 3;
   }

   // lumpensammler: multiply what's still left
   while (j < blen) {
      rm = r + j;
      const small bb = b[j];
      of1 = 0;

      i = 0;
#define s(o) of1 = ops<small>::muladdc(rm[i+(o)], a[i+(o)], bb, rm[i+(o)], of1)
      UNROLL8(i, alen, 0, leftover, s);
#undef s

      rm[i] = of1;
      ++j;
   }
}


#ifdef CL_TEST_CODE
static void test_omul(mulfunc16 mul) {
   {
      // simple case with 3*3 elements
      uint16 a[] = { 2, 3, 5 };
      uint16 b[] = { 7, 11, 13 };
      uint16 r[] = { 0xcd, 0xcd, 0xcd, 0xcd, 0xcd, 0xcd };
      uint16 r2[] = { 0xcd, 0xcd, 0xcd, 0xcd, 0xcd, 0xcd };

      mul(r, a, NBEL(a), b, NBEL(b));
      omul_ls(r2, a, NBEL(a), b, NBEL(b));
      classert(compu_nn(r, NBEL(r), r2, NBEL(r2)) == 0);
   }
   {
      // simple case with 4*4 elements
      uint16 a[] = { 2, 3, 5, 7 };
      uint16 b[] = { 11, 13, 17, 19 };
      uint16 r[] = { 0xcd, 0xcd, 0xcd, 0xcd, 0xcd, 0xcd, 0xcd, 0xcd };
      uint16 r2[] = { 0xcd, 0xcd, 0xcd, 0xcd, 0xcd, 0xcd, 0xcd, 0xcd };

      mul(r, a, NBEL(a), b, NBEL(b));
      omul_ls(r2, a, NBEL(a), b, NBEL(b));
      classert(compu_nn(r, NBEL(r), r2, NBEL(r2)) == 0);
   }
   {
      // simple case with 5*5 elements
      uint16 a[] = { 1, 2, 3, 5, 7 };
      uint16 b[] = { 11, 13, 17, 19, 23 };
      uint16 r[] = { 0xcd, 0xcd, 0xcd, 0xcd, 0xcd, 0xcd, 0xcd, 0xcd, 0xcd, 0xcd };
      uint16 r2[] = { 0xcd, 0xcd, 0xcd, 0xcd, 0xcd, 0xcd, 0xcd, 0xcd, 0xcd, 0xcd };

      mul(r, a, NBEL(a), b, NBEL(b));
      omul_ls(r2, a, NBEL(a), b, NBEL(b));
      classert(compu_nn(r, NBEL(r), r2, NBEL(r2)) == 0);
   }
   {
      // force overflows
      uint16 a[] = { 0xffff-2, 0xffff-3, 0xffff-5, 0xffff-7 };
      uint16 b[] = { 0xffff-11, 0xffff-13, 0xffff-17, 0xffff-19 };
      uint16 r[] = { 0xcd, 0xcd, 0xcd, 0xcd, 0xcd, 0xcd, 0xcd, 0xcd };
      uint16 r2[] = { 0xcd, 0xcd, 0xcd, 0xcd, 0xcd, 0xcd, 0xcd, 0xcd };

      mul(r, a, NBEL(a), b, NBEL(b));
      omul_ls(r2, a, NBEL(a), b, NBEL(b));
      classert(compu_nn(r, NBEL(r), r2, NBEL(r2)) == 0);
   }
   {
      // long case
      const unsigned sz = 18;
      uint16 a[sz];
      for (index i=0; i<sz; ++i) a[i] = i+1;
      uint16 b[sz];
      for (index i=0; i<sz; ++i) b[i] = 0xffff-i;
      uint16 r[2*sz];
      for (index i=0; i<2*sz; ++i) r[i] = 0xcd;
      uint16 r2[2*sz];
      for (index i=0; i<2*sz; ++i) r2[i] = 0xcd;

      mul(r, a, sz, b, sz);
      omul_ls(r2, a, sz, b, sz);
      classert(compu_nn(r, 2*sz, r2, 2*sz) == 0);
   }
   {
      // long case, odd number of elements
      const unsigned sz = 19;
      uint16 a[sz];
      for (index i=0; i<sz; ++i) a[i] = i+1;
      uint16 b[sz];
      for (index i=0; i<sz; ++i) b[i] = 0xffff-i;
      uint16 r[2*sz];
      for (index i=0; i<2*sz; ++i) r[i] = 0xcd;
      uint16 r2[2*sz];
      for (index i=0; i<2*sz; ++i) r2[i] = 0xcd;

      mul(r, a, sz, b, sz);
      omul_ls(r2, a, sz, b, sz);
      classert(compu_nn(r, 2*sz, r2, 2*sz) == 0);
   }
   {
      // long case, even number of elements
      const unsigned sz = 20;
      uint16 a[sz];
      for (index i=0; i<sz; ++i) a[i] = i+1;
      uint16 b[sz];
      for (index i=0; i<sz; ++i) b[i] = 0xffff-i;
      uint16 r[2*sz];
      for (index i=0; i<2*sz; ++i) r[i] = 0xcd;
      uint16 r2[2*sz];
      for (index i=0; i<2*sz; ++i) r2[i] = 0xcd;

      mul(r, a, sz, b, sz);
      omul_ls(r2, a, sz, b, sz);
      classert(compu_nn(r, 2*sz, r2, 2*sz) == 0);
   }
   {
      // test zero factor
      uint16 a[] = { 1, 2, 3, 5, 7 };
      uint16 r[NBEL(a)];

      mul(r, a, NBEL(a), a, 0);
      string s = dump_hex(r, NBEL(r));
      classert(s == "0");
   }
}
#endif


template<typename small>
void omul_interleaved_unrolled(small* r, const small* a, index alen, const small* b, index blen) {
   caassert(r != 0);
   caassert(a != 0);
   caassert(b != 0);
   caassert(alen >= blen);
   caassert((small)-1 > 0);  // must be unsigned type

   if (blen == 0) {
      zero(r, alen);
      return;
   }
   caassert(alen > 0);
   caassert(blen > 0);

   // unrolled inner loop on first element:
   // init the first 'alen' elements in result with product without adding up old contents.
   small of = 0;
   const small bb = b[0];
   index i = 0;

#define s(o) of = ops<small>::muladdc(r[i+(o)], a[i+(o)], bb, 0, of)
   UNROLL8(i, alen, 0, init, s);
#undef s

   r[i] = of;  // save overflow so the next add in the inner loop works ok.

   // the main outer loop
   index j = 1;
   small* rm;
   while (j+1 < blen) {  // make sure the last two accesses will fit
      rm = r + j;
      const small bb0 = b[j];
      const small bb1 = b[j+1];
      of = ops<small>::muladdc(rm[0], a[0], bb0, rm[0], 0);  // of:rm[0] = a[0] * b[0] + rm[0]  [max b^2-b]
      // the main inner loop
      small ofh = 0;

      i = 0;
#define s(o) do {  \
         small p1l;  \
         const small p1h = ops<small>::muladdc(p1l, a[i+(o)], bb1, rm[i+1+(o)], 0);  \
         small p2l;  \
         const small p2h = ops<small>::muladdc(p2l, a[i+1+(o)], bb0, of, 0);  \
         ofh += ops<small>::addc(rm[i+1+(o)], p1l, p2l);  \
         ofh = ops<small>::addc(of, p1h, p2h, ofh);  \
      } while(0)
      UNROLL4(i, alen, 1, inner, s);
#undef s

      caassert(j + i + 2 < alen + blen);
      // p1h:p1l = a[i] * b[j+1] + of
      of = ops<small>::muladdc(rm[i+1], a[i], bb1, of, 0);  // [max b^2-b-2]
      of = ops<small>::addc(rm[i+2], of, ofh);  // [max b+1]
      // check the maths out here!  why can of not be > 0?
      classert(of == 0);
      j += 2;
   }

   // simple loop: multiply what's still left
   while (j < blen) {
      rm = r + j;
      const small bb = b[j];
      of = 0;

      i = 0;
#define s(o) of = ops<small>::muladdc(rm[i+(o)], a[i+(o)], bb, rm[i+(o)], of)
      UNROLL8(i, alen, 0, leftover, s);
#undef s

      rm[i] = of;  // save overflow so the next add in the inner loop works ok.
      ++j;
   }
}

// ordinary multiplication with unrolled loops
// length can be zero, numbers don't need to be normalised

template<typename small>
void omul_unrolled(small* r, const small* a, index alen, const small* b, index blen) {
   caassert(r != 0);
   caassert(a != 0);
   caassert(b != 0);
   caassert(alen >= blen);
   caassert((small)-1 > 0);  // must be unsigned type

   if (blen == 0) {
      zero(r, alen);
      return;
   }
   caassert(alen > 0);
   caassert(blen > 0);

   // unrolled inner loop on first element:
   // init the first 'alen' elements in result with product without adding up old contents.
   small of = 0;
   small bb = b[0];
   index i = 0;

#define s(o) of = ops<small>::muladdc(r[i+(o)], a[i+(o)], bb, 0, of)
   UNROLL8(i, alen, 0, init, s);
#undef s

   r[i] = of;  // save overflow so the next add in the inner loop works ok.

   // now the main loop
   for (index j=1; j<blen; ++j) {
      small* rm = r + j;
      bb = b[j];
      of = 0;

      i = 0;
#define s(o) of = ops<small>::muladdc(rm[i+(o)], a[i+(o)], bb, rm[i+(o)], of)
      UNROLL8(i, alen, 0, inner, s);
#undef s

      rm[i] = of;  // save overflow so the next add in the inner loop works ok.
   }
}


#if defined _M_X64 && CL_TEST_CODE
static void test_mul_asmx64(mulfunc64 mul, const char* bugtext) {
   uint64 a[21];
   uint64 b[21];
   uint64 r1[41];
   uint64 r2[41];
   a[0] = 0xf1f1f1f1f1f1f1f1;
   for (unsigned i=1; i<NBEL(a)-1; ++i) a[i] = 0xf111111111111100 + i;
   b[0] = 0xf2f2f2f2f2f2f2f2;
   for (unsigned i=1; i<NBEL(b)-1; ++i) b[i] = 0xf222222222222200 + i;

   // a full 8-unrolled round
   memset(r1, 0xcc, sizeof(r1));
   memset(r2, 0xcc, sizeof(r2));
   omul_unrolled(r1+1, a+1, 8, b+1, 8);
   mul(r2+1, a+1, 8, b+1, 8);
   classert(memcmp(r1, r2, sizeof(r1)) == 0);

   // just part of it
   memset(r1, 0xcc, sizeof(r1));
   memset(r2, 0xcc, sizeof(r2));
   omul_unrolled(r1+1, a+1, 4, b+1, 4);
   mul(r2+1, a+1, 4, b+1, 4);
   classert(memcmp(r1, r2, sizeof(r1)) == 0);

   // one word uneven
   memset(r1, 0xcc, sizeof(r1));
   memset(r2, 0xcc, sizeof(r2));
   omul_unrolled(r1+1, a+1, 5, b+1, 5);
   mul(r2+1, a+1, 5, b+1, 5);
   classert(memcmp(r1, r2, sizeof(r1)) == 0);

   // two full 8-unrolled rounds
   memset(r1, 0xcc, sizeof(r1));
   memset(r2, 0xcc, sizeof(r2));
   omul_unrolled(r1+1, a+1, 16, b+1, 16);
   mul(r2+1, a+1, 16, b+1, 16);
   classert(memcmp(r1, r2, sizeof(r1)) == 0);

   // two full and some 8-unrolled rounds
   memset(r1, 0xcc, sizeof(r1));
   memset(r2, 0xcc, sizeof(r2));
   omul_unrolled(r1+1, a+1, 20, b+1, 20);
   mul(r2+1, a+1, 20, b+1, 20);
   classert(memcmp(r1, r2, sizeof(r1)) == 0);


   for (unsigned k=0; k<NBEL(a); ++k) a[k] = 0xaaaaaaaaaaaaaa00 + k;
   for (unsigned k=0; k<NBEL(b); ++k) b[k] = 0xbbbbbbbbbbbbbb00 + k;

   //for (unsigned k=0; k<NBEL(r1); ++k) r1[k] = 0xcdcdcdcdcdcdcdcd;
   //omul2_asmx64(r1, a, 7, b, 3);

   for (index j=1; j<=7; ++j) {
      for (index i=j; i<=17; ++i) {
         classert(i < NBEL(a));
         classert(j < NBEL(b));
         classert(i >= j);
         classert(i > 0);
         classert(j > 0);
         for (unsigned k=0; k<NBEL(r1); ++k) r1[k] = 0xcdcdcdcdcdcdcdcd;
         for (unsigned k=0; k<NBEL(r2); ++k) r2[k] = 0xcdcdcdcdcdcdcdcd;
         omul_unrolled(r1, a, i, b, j);
         mul(r2, a, i, b, j);
         if (memcmp(r1, r2, (i + j) * sizeof(uint64)) != 0)
            classertm(0, bugtext);
      }
   }
}
#endif


// ------ ORDINARY SQUARE -----------------------------------------------------

// unsigned square of a const input number 'a' into a pre-reserved result 'r'.
// 'r' must be long enough to hold the 2*alen long result.  there is no padding.
// length can be zero, numbers don't need to be normalised

template<typename small>
void osqu_plain(small* r, const small* a, index alen) {
   caassert(r != 0);
   caassert(a != 0);
   caassert((small)-1 > 0);  // must be unsigned type

   if (alen == 0) return;

   // unrolled inner loop on first element:
   // init the first 'alen' elements in 'r' with product without adding up old content.
   small of = 0;
   index i = 1;
   const small aa = a[0];
   r[0] = 0;
   while (i < alen) {
      of = ops<small>::muladdc(r[i], a[i], aa, 0, of);
      ++i;
   }
   r[i] = of;  // save overflow so the next add in the inner loop works ok.

   // now the main loop, below the diagonal
   for (index j=1; j<alen; ++j) {
      small* rm = r + j;
      const small aa = a[j];
      i = j + 1;
      of = 0;
      while (i < alen) {
         of = ops<small>::muladdc(rm[i], a[i], aa, rm[i], of);  // r[m] already inited
         ++i;
      }
      rm[i] = of;  // save overflow to the next element
   }

   // now, multiply the whole result with 2 and add the squares of the diagonal elements
   i = 0;
   of = 0;
   while (i < alen) {
      of = ops<small>::squaddonc(r+2*i, a[i], of);
      ++i;
   }
}


// alternative squaring by multiplication

#ifdef _M_X64
void osqu_omul_asmx64(uint64* r, const uint64* a, index alen) {
   omul_asmx64(r, a, alen, a, alen);
}

void osqu_omul2_asmx64(uint64* r, const uint64* a, index alen) {
   omul2_asmx64(r, a, alen, a, alen);
}

void osqu_omul3_asmx64(uint64* r, const uint64* a, index alen) {
   omul3_asmx64(r, a, alen, a, alen);
}

void osqu_omul4_asmx64(uint64* r, const uint64* a, index alen) {
   omul4_asmx64(r, a, alen, a, alen);
}
#endif


#ifdef CL_TEST_CODE
static void test_osqu() {
   {
      // simple case with 3*3 elements
      uint8 a[] = { 2, 3, 5 };
      uint8 r[2*NBEL(a)];

      osqu_plain(r, a, NBEL(a));
      //omul_ls(r, a, NBEL(a), a, NBEL(a));
      string s = dump_hex(r, NBEL(r));
      classert(s == "0x191e1d0c04");
   }
   {
      // simple case with 3*3 elements and overflow
      uint8 a[] = { 0xf2, 0xf3, 0xf5 };
      uint8 r[2*NBEL(a)];

      osqu_plain(r, a, NBEL(a));
      //omul_ls(r, a, NBEL(a), a, NBEL(a));
      string s = dump_hex(r, NBEL(r));
      classert(s == "0xec4cd5a950c4");
   }
}

#ifdef _M_X64
static void test_osqu_x64() {
   {
      // simple case with 3*3 elements
      uint64 a[] = { 2, 3, 5 };
      uint64 r[2*NBEL(a)];

      osqu_plain(r, a, NBEL(a));
      //omul_ls(r, a, NBEL(a), a, NBEL(a));
      string s = dump_hex(r, NBEL(r));
      classert(s == "0x19000000000000001e000000000000001d000000000000000c0000000000000004");
   }
   {
      // simple case with 3*3 elements and overflow
      uint64 a[] = { 0xf1f2f3f4f5f6f7f2, 0xf2f3f4f5f6f7f8f3, 0xf3f4f5f6f7f8f9f5 };
      uint64 r[2*NBEL(a)];

      osqu_plain(r, a, NBEL(a));
      //omul_ls(r, a, NBEL(a), a, NBEL(a));
      string s = dump_hex(r, NBEL(r));
      classert(s == "0xe87af558a5de02097935d049a2dcf905b49a53e2468194afc16406a847e47d7b4cc241ca5af290870f58b626a93ce0c4");
   }
}

template<typename small>
static void wipebuf(small* a, index alen, byte high_nibble = 0xf) {
   const uint64 filler = ((uint64)high_nibble << 60) | ((uint64)high_nibble << 52) | ((uint64)high_nibble << 44) | ((uint64)high_nibble << 36) | ((uint64)high_nibble << 28) | ((uint64)high_nibble << 20) | ((uint64)high_nibble << 12);
   a[0] = (small)0xf1f1f1f1f1f1f1f1;
   for (unsigned i=1; i<alen-1; ++i) a[i] = filler + i - 1;
   a[alen-1] = (small)0xfdfdfdfdfdfdfdfd;
}

static void test_squ_asmx64(squfunc64 squ, const char* bugtext) {
   const unsigned sz = 20;
   uint64 a[1+sz+1];
   uint64 r1[1+2*sz+1];
   uint64 r2[1+2*sz+1];

   wipebuf(a, NBEL(a), 0xa);

   // 0 words - nothing
   wipebuf(r1, NBEL(r1));
   wipebuf(r2, NBEL(r2));
   omul_plain(r1+1, a+1, 0, a+1, 0);
   squ(r2+1, a+1, 0);
   classert(memcmp(r1, r2, sizeof(r1)) == 0);

   // 1 word - only diagonal
   wipebuf(r1, NBEL(r1));
   wipebuf(r2, NBEL(r2));
   omul_plain(r1+1, a+1, 1, a+1, 1);
   squ(r2+1, a+1, 1);
   classert(memcmp(r1, r2, sizeof(r1)) == 0);

   // 2 words - use init once
   wipebuf(r1, NBEL(r1));
   wipebuf(r2, NBEL(r2));
   omul_plain(r1+1, a+1, 2, a+1, 2);
   squ(r2+1, a+1, 2);
   classert(memcmp(r1, r2, sizeof(r1)) == 0);

   // 6 words - use triple row init
   wipebuf(r1, NBEL(r1));
   wipebuf(r2, NBEL(r2));
   omul_plain(r1+1, a+1, 6, a+1, 6);
   squ(r2+1, a+1, 6);
   classert(memcmp(r1, r2, sizeof(r1)) == 0);

   // 7 words - use triple row once
   wipebuf(r1, NBEL(r1));
   wipebuf(r2, NBEL(r2));
   omul_plain(r1+1, a+1, 7, a+1, 7);
   squ(r2+1, a+1, 7);
   classert(memcmp(r1, r2, sizeof(r1)) == 0);

   // a full 8-unrolled round
   wipebuf(r1, NBEL(r1));
   wipebuf(r2, NBEL(r2));
   omul_plain(r1+1, a+1, 8, a+1, 8);
   squ(r2+1, a+1, 8);
   classert(memcmp(r1, r2, sizeof(r1)) == 0);

   // just part of it
   wipebuf(r1, NBEL(r1));
   wipebuf(r2, NBEL(r2));
   omul_plain(r1+1, a+1, 4, a+1, 4);
   squ(r2+1, a+1, 4);
   classert(memcmp(r1, r2, sizeof(r1)) == 0);

   // one word uneven
   wipebuf(r1, NBEL(r1));
   wipebuf(r2, NBEL(r2));
   omul_plain(r1+1, a+1, 5, a+1, 5);
   squ(r2+1, a+1, 5);
   classert(memcmp(r1, r2, sizeof(r1)) == 0);

   // two full 8-unrolled rounds
   wipebuf(r1, NBEL(r1));
   wipebuf(r2, NBEL(r2));
   omul_plain(r1+1, a+1, 16, a+1, 16);
   squ(r2+1, a+1, 16);
   classert(memcmp(r1, r2, sizeof(r1)) == 0);

   // two full and some 8-unrolled rounds
   wipebuf(r1, NBEL(r1));
   wipebuf(r2, NBEL(r2));
   omul_plain(r1+1, a+1, 20, a+1, 20);
   squ(r2+1, a+1, 20);
   classert(memcmp(r1, r2, sizeof(r1)) == 0);

   for (index i=1; i<=17; ++i) {
      classert(i > 0);
      classert(i < sz);
      wipebuf(r1, NBEL(r1));
      wipebuf(r2, NBEL(r2));
      omul_plain(r1+1, a+1, i, a+1, i);
      squ(r2+1, a+1, i);
      if (memcmp(r1, r2, sizeof(r1)) != 0)
         classertm(0, bugtext);
   }
}
#endif
#endif


// ------ MULTIPLICATION with 1 small -----------------------------------------

// length can be zero, numbers don't need to be normalised

template<typename small>
void mul1(small* r, const small* a, index alen, small b) {
   caassert(r != 0);
   caassert(a != 0);
   caassert((small)-1 > 0);  // must be unsigned type

   small of = 0;
   index i = 0;
   while (i < alen) {
      of = ops<small>::muladdc(r[i], a[i], b, 0, of);
      ++i;
   }
   r[i] = of;  // save overflow so the next add in the inner loop works ok.
}


// manually unrolled version of the inner mul loop on small's.
// length can be zero, numbers don't need to be normalised

template<typename small>
void mul1_unrolled(small* r, const small* a, index len, small b) {
   caassert(r != nullptr);
   caassert(a != nullptr);
   caassert((small)-1 > 0);  // must be unsigned type

   small of = 0;
   index i = 0;

#define s(o) of = ops<small>::muladdc(r[i+(o)], a[i+(o)], b, 0, of)
   UNROLL8(i, len, 0, l, s);
#undef s

   r[i] = of;  // save overflow so the next add in the inner loop works ok.
}


// ------ selftest ------------------------------------------------------------

#if !defined NDEBUG && 1
#define DOUBLE_CHECK 1
#else
#undef DOUBLE_CHECK
#endif

template<typename small>
static dword selftest_mul_fib(index bytes) {
   dword ret = 0;
   const index l = (bytes + sizeof(small) - 1) / sizeof(small);
   small* a = bignum_alloc<small>(l);
   zero(a, l);
   a[0] = 1;
   small* b = bignum_alloc<small>(l);
   zero(b, l);
   b[0] = 1;
   small* r = bignum_alloc<small>(l);

   small* t1 = bignum_alloc<small>(2*l);
   small* t2 = bignum_alloc<small>(2*l);
   small* t3 = bignum_alloc<small>(2*l);
#ifdef DOUBLE_CHECK
   small* tx = bignum_alloc<small>(2*l);
#endif

   index alen = normu(a, l);
   index blen = normu(b, l);
   index rlen;
   int n = 2;
   while (alen < l) {
      addu(r, alen+1, a, alen, b, blen);  // r = a + b
      rlen = normu(r, alen+1);

      if (n % 31 == 0) {
         // use cassini's identity: F(n+1) * F(n-1) - F(n)^2 == (-1)^n
         // here, r == F(n+1), a == F(n), b == F(n-1)
         // see concrete mathematics, 2nd ed, chapter 6.6
         mulu(t1, r, rlen, b, blen);  // t1 = r * b
#ifdef DOUBLE_CHECK
         {
            omul(tx, r, rlen, b, blen);
            if (compu(tx, rlen+blen, t1, rlen+blen) != 0) {
               DTOUT_PUTS(format("mul: in = %?\nin = %?\nshould = %?\nis = %?\n")
                  << dump_hex(r, rlen) << dump_hex(b, blen) << dump_hex(tx, rlen+blen) << dump_hex(t1, rlen+blen));
               classertm(0, "mul bug (in double check)!");
            }
         }
#endif
         index t1len = normu(t1, rlen + blen);
         squareu(t2, a, alen);   // t2 = a^2
#ifdef DOUBLE_CHECK
         {
            osqu(tx, a, alen);
            if (compu(tx, 2*alen, t2, 2*alen) != 0) {
               DTOUT_PUTS(format("square: in = %?\nshould = %?\nis = %?\n")
                  << dump_hex(a, alen) << dump_hex(tx, rlen) << dump_hex(t2, 2*alen));
               classertm(0, "squ bug (in double check)!");
            }
         }
#endif
         index t2len = normu(t2, 2*alen);

         subi(t3, 2*l, t1, t1len, t2, t2len);  // t1 -= t2
         index t3len = normi(t3, 2*l);
         const small one = n % 2 == 0 ? 1 : -1;  // (-1)^n
         if (compi(t3, t3len, &one, 1) != 0) {
            DTOUT_PUTS(format("a = %?\nb = %?\nr = %?\nt1 = %?\nt2 = %?\nt3 = %?\n")
               << dump_hex(a, alen) << dump_hex(b, blen) << dump_hex(r, rlen) << dump_hex(t1, t1len) << dump_hex(t2, t2len) << dump_hex(t3, t3len));
            classertm(0, "mul bug!");
            ret = n;
            goto out;
         }
      }

      blen = alen;
      copy(b, a, alen);  // b = a
      alen = rlen;
      copy(a, r, alen);  // a = r
      ++n;
   }
   classert(n >= bytes * 8);

out:
   bignum_free(a);
   bignum_free(b);
   bignum_free(r);
   bignum_free(t1);
   bignum_free(t2);
   bignum_free(t3);
#ifdef DOUBLE_CHECK
   bignum_free(tx);
#endif

   return ret;
}
#undef DOUBLE_CHECK

template<typename small>
static dword selftest_mul2() {
   dword r;
   // test omul only
   if ((r = selftest_mul_fib<small>(256)) != 0)
      return r;

   // test kmul
   switch_kmul(always_on);
   switch_ksqu(always_on);
   if ((r = selftest_mul_fib<small>(256)) != 0)
      return r;
   switch_kmul(always_off);
   switch_ksqu(always_off);

   // test t3mul
   switch_t3mul(always_on);
   switch_t3squ(always_on);
   if ((r = selftest_mul_fib<small>(256)) != 0)
      return r;
   switch_t3mul(always_off);
   switch_t3squ(always_off);

   // test smul
   switch_smul(always_on);
   switch_ssqu(always_on);
   if ((r = selftest_mul_fib<small>(256)) != 0)
      return r;
   switch_smul(always_off);
   switch_ssqu(always_off);

   return 0;
}

template<typename small>
dword selftest_mul() {
   const index oldk = switch_kmul(always_off);
   const index oldt3 = switch_t3mul(always_off);
   const index olds = switch_smul(always_off);
   const index oldsk = switch_ksqu(always_off);
   const index oldst3 = switch_t3squ(always_off);
   const index oldss = switch_ssqu(always_off);

   dword r = selftest_mul2<small>();

   switch_kmul(oldk);
   switch_t3mul(oldt3);
   switch_smul(olds);
   switch_ksqu(oldsk);
   switch_t3squ(oldst3);
   switch_ssqu(oldss);

   return r;
}

#ifdef CL_TEST_CODE
static void test_bignum_omul1() {
   uint16 a[] = { 0, 0 };
   uint16 b[] = { 0, 0 };
   uint16 r[] = { 0, 0, 0, 0 };
   uint64 rr, rx;

   a[0] = 1;
   omul_ls(r, a, 1, b, 0);
   classert(r[0] == 0);

   a[0] = 2;
   b[0] = 3;
   omul_ls(r, a, 1, b, 1);
   classert(r[0] == 6);

   a[0] = 2;
   a[1] = 7;
   b[0] = 3;
   omul_ls(r, a, 2, b, 1);
   rr = r[0] + ((uint64)r[1] << 16) + ((uint64)r[2] << 32) + ((uint64)r[3] << 48);
   rx = (uint64)0x70002 * 3;
   classert(rr == rx);

   a[0] = 2;
   a[1] = 7;
   b[0] = 3;
   b[1] = 9;
   omul_ls(r, a, 2, b, 2);
   rr = r[0] + ((uint64)r[1] << 16) + ((uint64)r[2] << 32) + ((uint64)r[3] << 48);
   rx = (uint64)0x70002 * 0x90003;
   classert(rr == rx);
}

#ifdef _M_X64
static void test_bignum_omul2() {
   uint64 a[] = { 0xc8a7213b333270f8, 0xfa63c8d9fa216a8f, 0 };
   uint64 r[] = { 0, 0, 0, 0, 0, 0 };
   string s = dump_hex(a, 3);
   classert(s == "0xfa63c8d9fa216a8fc8a7213b333270f8");

   uint64 hi;
   uint64 lo = ops<uint64>::muladdc(hi, 0xfa63c8d9fa216a8f, 0xfa63c8d9fa216a8f, 0xc4416c831592e186, 0xc4416c831592e186);
   classert(lo == 0xf4e70b2eb2b60a67);
   classert(hi == 0x76a837f01c5e7eed);

   squarei(r, a, 3);
   s = dump_hex(r, 6);
   classert(s == "0xf4e70b2eb2b60a6776a837f01c5e7eedbc9a73a757638246882d254322b9f040");
}
#endif

template<typename small>
static void test_small_mul() {
   small r;
   small of = ops<small>::muladdc(r, 0, 0, 0, 0);
   classert(r == 0);
   classert(of == 0);

   of = ops<small>::muladdc(r, 5, 7, 2, 1);
   classert(r == 38);
   classert(of == 0);

   of = ops<small>::muladdc(r, small(-1), small(-1), small(-1), small(-1));
   classert(r == small(-1));
   classert(of == small(-1));
}

static void test_small_squaddonc() {
   {
      uint8 r[2] = { 0x22, 0x33 };
      uint8 of = ops<uint8>::squaddonc(r, 0x44, 0x55);
      uint64 rc = 2 * 0x3322 + 0x44 * 0x44 + 0x55;
      uint64 rr = r[0] + r[1] * 256 + of * 65536;
      classert(rc == rr);
   }
   {
      uint8 r[2] = { 0xf8, 0xf9};
      uint8 of = ops<uint8>::squaddonc(r, 0xfa, 0xfb);
      uint64 rc = 2 * 0xf9f8 + 0xfa * 0xfa + 0xfb;
      uint64 rr = r[0] + r[1] * 256 + of * 65536;
      classert(rc == rr);
   }
}

#ifdef _M_X64
static void test_small_squaddonc_x64() {
   {
      uint64 r[3] = { 0x22, 0x33, 0 };
      r[2] = ops<uint64>::squaddonc(r, 0x44, 0x55);
      string s = dump_hex(r, 3);
      classert(s == "0x6600000000000012a9");
   }
   {
      uint64 r[3] = { 0xf1f2f3ff4ff5f6f8, 0xe1e2e3e4e5e6e7f9, 0 };
      r[2] = ops<uint64>::squaddonc(r, 0xd1d2d3d4d5d6d7fa, 0xfffffffffffffffb);
      string s = dump_hex(r, 3);
      classert(s == "0x26fbfb34b896dfa887488f1c27019ce0f");
   }
}
#endif

void _CL_CALL test_bignum_omul() {
   test_small_mul<uint16>();
   test_small_mul<uint32>();
   test_small_mul<uint8>();

#ifdef _M_X64
#ifdef BN_OMUL_USE_ASMX64
   test_mul_asmx64(omul_asmx64, "omul bug");
#endif
#ifdef BN_OMUL2_USE_ASMX64
   test_mul_asmx64(omul2_asmx64, "omul2 bug");
#endif
#ifdef BN_OMUL3_USE_ASMX64
   test_mul_asmx64(omul3_asmx64, "omul3 bug");
#endif
#ifdef BN_OMUL4_USE_ASMX64
   test_mul_asmx64(omul4_asmx64, "omul4 bug");
#endif

#ifdef BN_OSQU4_USE_ASMX64
   test_squ_asmx64(osqu4_asmx64, "osqu4 bug");
#endif
#endif

   test_bignum_omul1();
   test_omul(omul_unrolled<uint16>);
   test_omul(omul_interleaved<uint16>);
   test_omul(omul_interleaved_unrolled<uint16>);
   test_omul(omul_interleaved2<uint16>);
   test_omul(omul_interleaved2_unrolled<uint16>);
   test_omul(omul_interleaved3<uint16>);

#ifdef _M_X64
   test_small_mul<uint64>();
   test_bignum_omul2();
#endif

   test_small_squaddonc();
   test_osqu();

#ifdef _M_X64
   test_small_squaddonc_x64();
   test_osqu_x64();
#endif

   switch_kmul(always_off);
   switch_ksqu(always_off);
   switch_t3mul(always_off);
   switch_t3squ(always_off);
   selftest_mul_fib<uint16>(256);
   selftest_mul_fib<uint32>(256);
   selftest_mul_fib<uint8>(256);
#ifdef _M_X64
   selftest_mul_fib<uint64>(256);
#endif
   switch_kmul(kmul_thresh_init);
   switch_ksqu(kmul_thresh_init);
   switch_t3mul(kmul_thresh_init);
   switch_t3squ(kmul_thresh_init);
}
#endif


// ------ generate template functions -----------------------------------------

#pragma inline_depth(0)

template<typename small>
void generate_bignum_omuls() {
   muli<small>(0, 0, 0, 0, 0);
   mulu<small>(0, 0, 0, 0, 0);
   squarei<small>(0, 0, 0);
   squareu<small>(0, 0, 0);
   selftest_mul<small>();
   mul1<small>(0, 0, 0, 0);
   mul1_fast<small>(0, 0, 0, 0);
   omul_plain<small>(0, 0, 0, 0, 0);
   omul_unrolled<small>(0, 0, 0, 0, 0);
   omul_interleaved<small>(0, 0, 0, 0, 0);
   omul_interleaved_unrolled<small>(0, 0, 0, 0, 0);
   omul_interleaved2<small>(0, 0, 0, 0, 0);
   omul_interleaved2_unrolled<small>(0, 0, 0, 0, 0);
   omul_interleaved3<small>(0, 0, 0, 0, 0);
   osqu_plain<small>(0, 0, 0);
}

void _generate_bignum_omuls() {
   classertm(0, "must not be called");
   generate_bignum_omuls<uint8>();
   generate_bignum_omuls<uint16>();
   generate_bignum_omuls<uint32>();
#ifdef _M_X64
   generate_bignum_omuls<uint64>();
#endif
}

#pragma inline_depth()

}  // namespace bignum
