// $+HEADER$
// 
// Copyright 2015 Christoph Lueders
// 
// This file is part of the BIGNUM Library.
// 
// The BIGNUM Library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
// 
// The BIGNUM Library is distributed in the hope that it will be useful, but 
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public 
// License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with the BIGNUM Library.  If not, see <http://www.gnu.org/licenses/>.
// 
// $-HEADER$

// $Date: 2015-04-24 11:28:16 +0200 (Fr, 24 Apr 2015) $
// $Rev: 26590 $

/*
 *  arbitrary length number arithmetic - binary logic
 */

#include "precomp.h"
#include "impl.h"
#include "small.h"


// we must switch off "unreachable code" warning, since bool template parameters generate code paths that are never used
#pragma warning (disable:4702)

namespace bignum {


// ------ SHIFT RIGHT ---------------------------------------------------------

// shift value to the right, i.e. 110110 >> 1 yields 11011.
// should be ok if numbers overlap

template<typename small>
void shr(small* r, const index rmax, const small* a, const index alen, const bit_index shift_bits) {
   caassert(r != nullptr);
   caassert(a != nullptr);
   caassert(shift_bits / bits(small) < numeric_limits<index>::max());
   const index off = shift_bits / bits(small);
   caassert(rmax >= alen - off);  // must fit

   if (off >= alen) {  // shift count too large, result zero
      zero(r, rmax);
      return;
   }
   const index sh1 = shift_bits % bits(small);
   // we need to check here, since CPU might not shift AT ALL, if bits >= register-width
   if (sh1 == 0) {  // no need to shift, just move small's
      caassert(alen > off);
      copy(r, a+off, alen-off);
      zero(r, rmax, alen-off);
   }
   else {  // regular style, shift count is not a multiple of bits(small)
      index i = 0;
      if (alen > 0) {
         caassert(off < alen);
         const index sh2 = bits(small) - sh1;
         small of = a[off] >> sh1;
         while (i+1+off < alen) {
            of = ops<small>::shl(r[i], a[i+1+off], sh2, of);
            ++i;
         }
         if (i < rmax) r[i++] = of;
      }
      zero(r, rmax, i);
   }
}

#ifdef CL_TEST_CODE
static void test_bignum_shr() {
   uint16 a[] = { 0, 0, 0 };
   uint16 r[] = { 1, 2, 3, 4, 5, 6, 7 };

   a[0] = 12345;
   shr(r, 1, a, 1, 0);
   classert(r[0] == 12345);
   classert(r[1] == 2);

   shr(r, 2, a, 1, 0);
   classert(r[0] == 12345);
   classert(r[1] == 0);

   a[0] = 12345;
   r[1] = 2;
   shr(r, 1, a, 1, 1);
   classert(r[0] == 12345 >> 1);
   classert(r[1] == 2);

   a[0] = 12345;
   a[1] = 23455;
   shr(r, 2, a, 2, 1);
   classert(r[0] == (12345 >> 1) + 0x8000);
   classert(r[1] == 23455 >> 1);

   a[0] = 12345;
   a[1] = 23456;
   shr(r, 2, a, 2, 16);
   classert(r[0] == 23456);
   classert(r[1] == 0);

   a[0] = 12345;
   a[1] = 23456;
   shr(r, 2, a, 2, 24);
   classert(r[0] == (12345 + (23456 << 16)) >> 24);
   classert(r[1] == 0);

   a[0] = 12345;
   shr(r, 1, a, 1, 17);
   classert(r[0] == 0);
}
#endif


template<typename small>
void shr_on(small* a, const index alen, const bit_index shift_bits) {
   caassert(a != nullptr);
   caassert(shift_bits / bits(small) < numeric_limits<index>::max());
   const index off = shift_bits / bits(small);

   if (off >= alen) {  // shift count too large, result zero
      zero(a, alen);
      return;
   }
   const index sh1 = shift_bits % bits(small);
   // we need to check here, since CPU might not shift AT ALL, if bits >= register-width
   if (sh1 == 0) {  // no need to shift, just move small's
      caassert(alen > off);
      copy(a, a+off, alen-off);
      zero(a, alen, alen-off);
   }
   else {  // regular style, shift count is not a multiple of bits(small)
      index i = 0;
      if (alen > 0) {
         caassert(off < alen);
         const index sh2 = bits(small) - sh1;
         small of = a[off] >> sh1;
         while (i+1+off < alen) {
            of = ops<small>::shl(a[i], a[i+1+off], sh2, of);
            ++i;
         }
         caassert(i < alen);
         a[i++] = of;
      }
      zero(a, alen, i);
   }
}

#ifdef CL_TEST_CODE
static void test_bignum_shr_on() {
   uint16 a[] = { 0, 0, 0 };

   a[0] = 12345;
   a[1] = 999;
   shr_on(a, 1, 0);
   classert(a[0] == 12345);
   classert(a[1] == 999);

   a[0] = 12345;
   shr_on(a, 1, 1);
   classert(a[0] == 12345 >> 1);
   classert(a[1] == 999);

   a[0] = 12345;
   a[1] = 23455;
   shr_on(a, 2, 1);
   classert(a[0] == (12345 >> 1) + 0x8000);
   classert(a[1] == 23455 >> 1);

   a[0] = 12345;
   a[1] = 23456;
   shr_on(a, 2, 16);
   classert(a[0] == 23456);
   classert(a[1] == 0);

   a[0] = 12345;
   a[1] = 23456;
   shr_on(a, 2, 24);
   classert(a[0] == (12345 + (23456 << 16)) >> 24);
   classert(a[1] == 0);

   a[0] = 12345;
   shr_on(a, 1, 17);
   classert(a[0] == 0);
}
#endif


// ------ SHIFT LEFT ----------------------------------------------------------

// shift value to the left, i.e. 110110 << 1 yields 1101100.
// must not be used if numbers overlap!

template<typename small>
void shl(small* r, const index rmax, const small* a, const index alen, const bit_index shift_bits) {
   caassert(r != nullptr);
   caassert(a != nullptr);
   caassert(shift_bits / bits(small) < numeric_limits<index>::max());
   const index off = shift_bits / bits(small);
   caassert(rmax >= alen + off);  // must fit (the top word may be left out though)

   const index sh1 = shift_bits % bits(small);
   // we need to check here, since CPU might not shift AT ALL, if bits >= register-width
   if (sh1 == 0) {  // no need to shift, just move small's
      zero(r, off);
      copy(r+off, a, alen);
      zero(r, rmax, off+alen);
   }
   else {  // regular style, shift count is not a multiple of bits(small)
      zero(r, off);
      index i = 0;
      small of = 0;
      while (i < alen) {
         of = ops<small>::shl(r[i+off], a[i], sh1, of);
         ++i;
      }
      if (i+off < rmax) r[i++ + off] = of;
      zero(r, rmax, i+off);
   }
}

#ifdef CL_TEST_CODE
static void test_bignum_shl() {
   uint16 a[] = { 0, 0, 0 };
   uint16 r[] = { 1, 2, 3, 4, 5, 6, 7 };
   uint64 aa, rr;

   a[0] = 12345;
   shl(r, 1, a, 1, 0);
   classert(r[0] == 12345);
   classert(r[1] == 2);

   shl(r, 2, a, 1, 0);
   classert(r[0] == 12345);
   classert(r[1] == 0);

   a[0] = 12345;
   shl(r, 2, a, 1, 16);
   classert(r[0] == 0);
   classert(r[1] == 12345);

   a[0] = 12345;
   r[1] = 2;
   shl(r, 1, a, 1, 1);
   classert(r[0] == 12345 << 1);
   classert(r[1] == 2);

   a[0] = 12345;
   shl(r, 3, a, 1, 7);
   classert(r[0] + (r[1] << 16) + ((uint64)r[2] << 32) == 12345 << 7);

   a[0] = 12345;
   a[1] = 23456;
   aa = a[0] + ((uint64)a[1] << 16);
   shl(r, 3, a, 2, 7);
   rr = r[0] + ((uint64)r[1] << 16) + ((uint64)r[2] << 32);
   aa <<= 7;
   classert(rr == aa);

   a[0] = 1;
   aa = a[0];
   shl(r, 4, a, 1, 63);
   rr = r[0] + ((uint64)r[1] << 16) + ((uint64)r[2] << 32) + ((uint64)r[3] << 48);
   aa <<= 63;
   classert(rr == aa);

#ifdef _M_X64
   {
      uint64 a[] = { 0x1122334455667788, 0x88776655443322 };
      uint64 r[] = { 0, 0 };

      shl(r, 2, a, 2, 4);
      string s = dump_hex(r, 2);
      classert(s == "0x8877665544332211223344556677880");
   }
#endif
}
#endif

template<typename small>
void shl_on(small* a, const index alen, const bit_index shift_bits) {
   caassert(a != nullptr);
   caassert(shift_bits / bits(small) < numeric_limits<index>::max());
   const index off = shift_bits / bits(small);

   if (off >= alen) {  // shift count too large, result zero
      zero(a, alen);
      return;
   }
   const index sh1 = shift_bits % bits(small);
   // we need to check here, since CPU might not shift AT ALL, if bits >= register-width
   if (sh1 == 0) {  // no need to shift, just move small's
      copy_rev(a+off, a, alen-off);
      zero(a, off);
   }
   else {  // regular style, shift count is not a multiple of bits(small)
      const index sh2 = bits(small) - sh1;
      index i = alen-off-1;
      while (i > 0) {
         a[i+off] = (a[i-1] >> sh2) | (a[i] << sh1);
         --i;
      }
      a[off] = a[i] << sh1;
      zero(a, off);
   }
}

#ifdef CL_TEST_CODE
static void test_bignum_shl_on() {
   uint16 a[] = { 0, 0, 0, 0 };
   uint64 aa, rr;

   a[0] = 12345;
   a[1] = 99;
   shl_on(a, 1, 0);
   classert(a[0] == 12345);
   classert(a[1] == 99);

   a[0] = 12345;
   a[1] = 0;
   shl_on(a, 2, 16);
   classert(a[0] == 0);
   classert(a[1] == 12345);

   a[0] = 12345;
   a[1] = 99;
   shl_on(a, 1, 1);
   classert(a[0] == 12345 << 1);
   classert(a[1] == 99);

   a[0] = 12345;
   shl_on(a, 1, 7);
   classert(a[0] == (uint16)(12345 << 7));
   classert(a[1] == 99);

   a[0] = 12345;
   a[1] = 23456;
   a[2] = 0;
   aa = a[0] + ((uint64)a[1] << 16);
   shl_on(a, 3, 7);
   rr = a[0] + ((uint64)a[1] << 16) + ((uint64)a[2] << 32);
   aa <<= 7;
   classert(rr == aa);

   a[0] = 1;
   a[1] = 0;
   a[2] = 0;
   a[3] = 0;
   aa = a[0];
   shl_on(a, 4, 63);
   rr = a[0] + ((uint64)a[1] << 16) + ((uint64)a[2] << 32) + ((uint64)a[3] << 48);
   aa <<= 63;
   classert(rr == aa);

#ifdef _M_X64
   {
      uint64 a[] = { 0x1122334455667788, 0x88776655443322 };

      shl_on(a, 2, 4);
      string s = dump_hex(a, 2);
      classert(s == "0x8877665544332211223344556677880");
   }
#endif
}
#endif



// ------ BITWISE AND ---------------------------------------------------------

template<typename small>
void and_ls(small* r, index rmax, const small* a, index alen, const small* b, index blen) {
   caassert(r != nullptr);
   caassert(a != nullptr);
   caassert(b != nullptr);
   caassert(alen >= blen);
   caassert(rmax >= alen);

   // and up the length where we have two values
   index i = 0;
   while (i < blen)
      r[i++] = a[i] & b[i];

   // zero the rest of 'r'
   zero(r, rmax, i);
}

#ifdef CL_TEST_CODE
static void test_bignum_and() {
   uint16 a[] = { 0, 0, 0, 0 };
   uint16 b[] = { 0, 0, 0, 0 };
   uint16 r[] = { 1, 2, 3, 4 };

   a[0] = 7;
   b[0] = 0xffff;
   and(r, 1, a, 1, b, 1);
   classert(r[0] == 7);
   classert(r[1] == 2);

   and(r, 2, a, 1, b, 1);
   classert(r[0] == 7);
   classert(r[1] == 0);

   a[0] = 7;
   a[1] = 0x55;
   b[0] = 0xffff;
   and(r, 2, a, 2, b, 1);
   classert(r[0] == 7);
   classert(r[1] == 0);

   a[0] = 7;
   and(r, 1, a, 1, b, 0);
   classert(r[0] == 0);
}
#endif


// ------ BITWISE OR ----------------------------------------------------------

template<typename small>
void or_ls(small* r, index rmax, const small* a, index alen, const small* b, index blen) {
   caassert(r != nullptr);
   caassert(a != nullptr);
   caassert(b != nullptr);
   caassert(alen >= blen);
   caassert(rmax >= alen);

   // and up the length where we have two values
   index i = 0;
   while (i < blen)
      r[i++] = a[i] | b[i];

   // copy the rest of 'a'
   copy(r, a, alen, i);

   // zero the rest of 'r'
   zero(r, rmax, alen);
}

#ifdef CL_TEST_CODE
static void test_bignum_or() {
   uint16 a[] = { 0, 0, 0, 0 };
   uint16 b[] = { 0, 0, 0, 0 };
   uint16 r[] = { 1, 2, 3, 4 };

   a[0] = 7;
   b[0] = 0xff00;
   or(r, 1, a, 1, b, 1);
   classert(r[0] == 0xff07);
   classert(r[1] == 2);

   or(r, 2, a, 1, b, 1);
   classert(r[0] == 0xff07);
   classert(r[1] == 0);

   a[0] = 7;
   a[1] = 0x55;
   b[0] = 0xff00;
   or(r, 2, a, 2, b, 1);
   classert(r[0] == 0xff07);
   classert(r[1] == 0x55);

   a[0] = 7;
   or(r, 1, a, 1, b, 0);
   classert(r[0] == 7);
}
#endif


// ------ SET ALL ONES --------------------------------------------------------

// create a value of 2^p-1, that is p 1-bits.

template<typename small>
void set_all_ones(small* r, const index rmax, const bit_index p) {
   caassert(p / bits(small) < numeric_limits<index>::max());
   // improve: could be done without shl() and subu()
   if (rmax > 0) {
      const small one = 1;
      shl(r, rmax, &one, 1, p);
      subu_on(r, rmax, &one, 1);
   }
}

#ifdef CL_TEST_CODE
static void test_bignum_set_all_ones() {
   uint16 r[] = { 1, 2, 3, 4 };
   uint64 rr;

   set_all_ones(r, 1, 0);
   classert(r[0] == 0);

   set_all_ones(r, 1, 1);
   classert(r[0] == 1);

   set_all_ones(r, 1, 12);
   classert(r[0] == 0xfff);

   set_all_ones(r, 2, 12);
   classert(r[0] == 0xfff);
   classert(r[1] == 0);

   set_all_ones(r, 4, 63);
   rr = r[0] + ((uint64)r[1] << 16) + ((uint64)r[2] << 32) + ((uint64)r[3] << 48);
   classert(rr == 0x7fffffffffffffffui64);
}
#endif


// ------ MODULO 2^n-1 --------------------------------------------------------

// improve: could be done with more bit-fiddling, at least without allocation of mod

template<typename small>
void mod_pow2m1_on(small* r, const index len, const bit_index n) {
   caassert(n / bits(small) < numeric_limits<index>::max());
   const index mlen = (n + bits(small) - 1) / bits(small);
   if (mlen > len) return;  // is intrinsically true
   tape_alloc<small> mod(mlen);
   set_all_ones(mod.p, mlen, n);    // mod = 2^n-1

   tape_alloc<small> hi(len);
   while (compu_nn(r, len, mod.p, mlen) > 0) {   // r > mod ?
      shr(hi.p, len, r, len, n);    // hi = r >> n
      and_on(r, len, mod.p, mlen);  // r &= mod
      addu_on(r, len, hi.p, len);   // r += hi
   }
   if (compu_nn(r, len, mod.p, mlen) == 0) zero(r, len);
}

#ifdef CL_TEST_CODE
void _CL_CALL test_bignum_mod_2p1() {
   uint16 r[] = { 1, 2, 3, 4 };

   r[0] = 20;
   mod_pow2m1_on(r, 1, 3);  // mod 7
   classert(r[0] == 6);

   r[0] = 20;
   r[1] = 0;
   mod_pow2m1_on(r, 2, 3);  // mod 7
   classert(r[0] == 6);
   classert(r[1] == 0);

   r[0] = 0x840;
   mod_pow2m1_on(r, 1, 3);  // mod 7
   classert(r[0] == 0x840 % 7);

   r[0] = 7;
   mod_pow2m1_on(r, 1, 3);  // mod 7
   classert(r[0] == 0);
}
#endif


// ------ MODULO 2^n+1 --------------------------------------------------------

template<typename small>
void mod_pow2p1_aligned_on(small* r, index rlen, const bit_index n) {
   caassert(r != nullptr);
   caassert(n > 0);
   caassert(n / bits(small) < numeric_limits<index>::max());
   caassert(n % bits(small) == 0);
   const index plen = n / bits(small);  // length in smalls of 2^n
   rlen = normu(r, rlen);  // if there are leading zeroes, we don't need to handle them
   if (rlen <= plen) return;  // nothing to do and r[plen] might not be accessible

   // the parts of r (plen smalls each) are alternatingly subtraced and added to/from the bottom plen smalls.
   // overflows are collected.  repeat with next part.
   sindex of = 0;
   const small* hi = r;
   index hlen = rlen;
   for (;;) {
      hi += plen;
      hlen -= plen;
      if (hlen > plen) {  // for this to work, hlen must be length of normalised hi value
         of += subu_on(r, plen, hi, plen);
      }
      else {
         of += subu_on(r, plen, hi, hlen);
         break;
      }
      hi += plen;
      hlen -= plen;
      if (hlen > plen) {  // for this to work, hlen must be length of normalised hi value
         of -= addu_on(r, plen, hi, plen);
      }
      else {
         of -= addu_on(r, plen, hi, hlen);
         break;
      }
   }
   // byteorder: this needs intel order
   const small one = 1;
   if (of > 0) {
      const index olen = normu((small*)&of, sizeof(of) / sizeof(small));
      classert(olen <= 1);  // if small < index this might not be true with huge numbers, but who uses that?
      r[plen] = addu_on(r, plen, (small*)&of, olen);  // add overflow and set top bit of remainder
      if (r[plen] > 0) {
         classert(r[plen] == 1);
         // this is true for 2^n as well, but we don't need to check for that rare special case, since the code works fine on it
         r[plen] = subu_on(r, plen, &one, 1);
         if (r[plen] > 0)  // it had been 2^n, so revert what we have done
            r[plen] = addu_on(r, plen, &one, 1);
      }
   }
   else if (of < 0) {
      of = -of;
      const index olen = normu((small*)&of, sizeof(of) / sizeof(small));
      classert(olen <= 1);  // if small < index this might not be true with huge numbers, but who uses that?
      r[plen] = subu_on(r, plen, (small*)&of, olen);  // add overflow and set top bit of remainder
      if (r[plen] > 0) {
         classert(r[plen] == 1);
         r[plen] = addu_on(r, plen, &one, 1);
      }
   }
   else
      r[plen] = 0;
   zero(r, rlen, plen+1);  // zero out the rest
   classert(r[plen] == 0 || r[plen] == 1 && is_zero(r, plen));
}

#ifdef CL_TEST_CODE
static void test_mod_pow2p1_aligned_on() {
   const index b = bits(uint16);
   string s;
   {
      uint16 a[] = { 3 };
      s = dump_hex(a, NBEL(a));
      mod_pow2p1_aligned_on(a, NBEL(a), b);
      s = dump_hex(a, NBEL(a));
      classert(s == "3");
   }
   {
      uint16 a[] = { 3, 2 };
      s = dump_hex(a, NBEL(a));
      mod_pow2p1_aligned_on(a, NBEL(a), b);
      s = dump_hex(a, NBEL(a));
      classert(s == "1");
   }
   {
      uint16 a[] = { 3, 2, 0, 0 };
      s = dump_hex(a, NBEL(a));
      mod_pow2p1_aligned_on(a, NBEL(a), b);
      s = dump_hex(a, NBEL(a));
      classert(s == "1");
   }
   {
      uint16 a[] = { 1, 1 };
      s = dump_hex(a, NBEL(a));
      mod_pow2p1_aligned_on(a, NBEL(a), b);
      s = dump_hex(a, NBEL(a));
      classert(s == "0");
   }
   {
      uint16 a[] = { 0, 1 };
      s = dump_hex(a, NBEL(a));
      mod_pow2p1_aligned_on(a, NBEL(a), b);
      s = dump_hex(a, NBEL(a));
      classert(s == "0x10000");
   }
   {
      uint16 a[] = { 0, 2 };
      s = dump_hex(a, NBEL(a));
      mod_pow2p1_aligned_on(a, NBEL(a), b);
      s = dump_hex(a, NBEL(a));
      classert(s == "0xffff");
   }
   {
      uint16 a[] = { 0, 5 };
      s = dump_hex(a, NBEL(a));
      mod_pow2p1_aligned_on(a, NBEL(a), b);
      s = dump_hex(a, NBEL(a));
      classert(s == "0xfffc");
   }
   {
      uint16 a[] = { 4, 2, 1 };
      s = dump_hex(a, NBEL(a));
      mod_pow2p1_aligned_on(a, NBEL(a), b);
      s = dump_hex(a, NBEL(a));
      classert(s == "3");
   }
   {
      uint16 a[] = { 1, 2, 1 };
      s = dump_hex(a, NBEL(a));
      mod_pow2p1_aligned_on(a, NBEL(a), b);
      s = dump_hex(a, NBEL(a));
      classert(s == "0");
   }
   {
      uint16 a[] = { 7, 8, 9 };
      s = dump_hex(a, NBEL(a));
      mod_pow2p1_aligned_on(a, NBEL(a), b);
      s = dump_hex(a, NBEL(a));
      classert(s == "8");
   }
   {
      uint16 a[] = { 6, 8, 9, 9 };
      s = dump_hex(a, NBEL(a));
      mod_pow2p1_aligned_on(a, NBEL(a), b);
      s = dump_hex(a, NBEL(a));
      classert(s == "0xffff");
   }
   {
      uint16 a[] = { 0xffff, 1, 0xfffe, 9 };
      s = dump_hex(a, NBEL(a));
      mod_pow2p1_aligned_on(a, NBEL(a), b);
      s = dump_hex(a, NBEL(a));
      classert(s == "0xfff2");
   }
   {
      uint16 a[] = { 0xffff, 0, 0xfffe, 1, 6 };
      s = dump_hex(a, NBEL(a));
      mod_pow2p1_aligned_on(a, NBEL(a), b);
      s = dump_hex(a, NBEL(a));
      classert(s == "0");
   }
   {
      uint16 a[] = { 0x8000, 0, 0x8000 };
      s = dump_hex(a, NBEL(a));
      mod_pow2p1_aligned_on(a, NBEL(a), b);
      s = dump_hex(a, NBEL(a));
      classert(s == "0x10000");
   }
}
#endif


// ------ test code -----------------------------------------------------------

#ifdef CL_TEST_CODE
template<typename small>
void test_logic_t() {
   small init[] = {255, 0x55, 0xaa, 101, 0};
   small a[NBEL(init)];
   small r[NBEL(a)];
   small r2[NBEL(a)];
   small t[NBEL(a)];

   copy(a, init, NBEL(a));
   for (unsigned i=0; i<bits(small); ++i) {
      shl(r, NBEL(r), a, NBEL(a), 1);
      addu(r2, NBEL(r2), a, NBEL(a), a, NBEL(a));
      classert(compu_nn(r, NBEL(r), r2, NBEL(r2)) == 0);
      copy(a, r, NBEL(a));
   }

   copy(a, init, NBEL(a));
   for (unsigned i=0; i<bits(small); ++i) {
      addu(r2, NBEL(r2), a, NBEL(a), a, NBEL(a));
      shl_on(a, NBEL(a), 1);
      classert(compu_nn(a, NBEL(a), r2, NBEL(r2)) == 0);
   }

   copy(a, init, NBEL(a));
   for (unsigned i=0; i<bits(small); ++i) {
      shr(r, NBEL(r), a, NBEL(a), 1);
      shl(r2, NBEL(r2), r, NBEL(r), 1);
      subu(t, NBEL(t), a, NBEL(a), r2, NBEL(r2));
      classert(compu1_nn(t, NBEL(t), small(1)) <= 0);
      copy(a, r, NBEL(a));
   }

   copy(a, init, NBEL(a));
   for (unsigned i=0; i<bits(small); ++i) {
      copy(r, a, NBEL(r));
      shr_on(r, NBEL(r), 1);
      shl(r2, NBEL(r2), r, NBEL(r), 1);
      subu(t, NBEL(t), a, NBEL(a), r2, NBEL(r2));
      classert(compu1_nn(t, NBEL(t), small(1)) <= 0);
      copy(a, r, NBEL(a));
   }
}

static void test_bignum_bits() {
   int cnt = 0;
   dbg_assert_cnt = &cnt;
   {
      uint32 a = 2;
      classert(!_is_bit_set(a, 0));
      classert(_is_bit_set(a, 1));
      classert(!_is_bit_set(a, 2));

      _is_bit_set(a, 32);
      classert(cnt > 0);

      _clear_bit(a, 1);
      classert(a == 0);
      _set_bit(a, 16);
      classert(a == 65536);

      cnt = 0;
      _clear_bit(a, 32);
      classert(cnt > 0);

      cnt = 0;
      _set_bit(a, 32);
      classert(cnt > 0);
   }
   {
      uint32 a[4];
      index alen = NBEL(a);
      zero(a, alen);
      classert(compu1_nn(a, alen, uint32(0)) == 0);

      bit_index idx = bit_length(a, alen);
      classert(idx == 0);

      set_bit(a, alen, 65);
      classert(!is_bit_set(a, alen, 64));
      classert(is_bit_set(a, alen, 65));
      classert(!is_bit_set(a, alen, 66));

      clear_bit(a, alen, 64);
      classert(!is_bit_set(a, alen, 63));
      classert(!is_bit_set(a, alen, 64));
      classert(is_bit_set(a, alen, 65));

      clear_bit(a, alen, 65);
      classert(!is_bit_set(a, alen, 65));

      set_bit(a, alen, 65);
      idx = bit_length(a, alen);
      classert(idx == 65+1);

      cnt = 0;
      set_bit(a, alen, -1);
      classert(cnt > 0);

      cnt = 0;
      clear_bit(a, alen, -1);
      classert(cnt > 0);

      cnt = 0;
      is_bit_set(a, alen, -1);
      classert(cnt > 0);
   }
   dbg_assert_cnt = nullptr;
}

void _CL_CALL test_bignum_logic() {
   test_bignum_bits();
   test_bignum_shr();
   test_bignum_shr_on();
   test_bignum_shl();
   test_bignum_shl_on();
   test_bignum_set_all_ones();
   test_bignum_and();
   test_bignum_or();
   test_bignum_mod_2p1();
   test_mod_pow2p1_aligned_on();

   test_logic_t<uint8>();
   test_logic_t<uint16>();
   test_logic_t<uint32>();
#ifdef _M_X64
   test_logic_t<uint64>();
#endif
}
#endif


//#if _MSC_VER < 1300
#pragma inline_depth(0)
template<typename small>
void pull_bignum_logics_into_existance() {
   shr<small>(0, 0, 0, 0, 0);
   shr_on<small>(0, 0, 0);
   shl<small>(0, 0, 0, 0, 0);
   shl_on<small>(0, 0, 0);
   set_all_ones<small>(0, 0, 0);
   and<small>(0, 0, 0, 0, 0, 0);
   or<small>(0, 0, 0, 0, 0, 0);
   mod_pow2m1_on<small>(0, 0, 0);
   mod_pow2p1_aligned_on<small>(0, 00, 0);
}
void _pull_bignum_logic_into_existance() {
   classertm(0, "must not be called");
   pull_bignum_logics_into_existance<uint8>();
   pull_bignum_logics_into_existance<uint16>();
   pull_bignum_logics_into_existance<uint32>();
#ifdef _M_X64
   pull_bignum_logics_into_existance<uint64>();
#endif
}
#pragma inline_depth()
//#endif

}  // namespace bignum
