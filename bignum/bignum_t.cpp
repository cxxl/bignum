// $+HEADER$
// 
// Copyright 2015 Christoph Lueders
// 
// This file is part of the BIGNUM Library.
// 
// The BIGNUM Library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
// 
// The BIGNUM Library is distributed in the hope that it will be useful, but 
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public 
// License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with the BIGNUM Library.  If not, see <http://www.gnu.org/licenses/>.
// 
// $-HEADER$

// $Date: 2015-04-24 11:28:16 +0200 (Fr, 24 Apr 2015) $
// $Rev: 26590 $

/*
 *  arbitrary length number arithmetic - C++ long int class
 */

#include "precomp.h"
#include "bignum_t.h"
#include "cl/str_conv.h"
#include "cl/widechar.h"
#include "cl/int_math.h"

using namespace bignum;

namespace bignum {

#ifndef NDEBUG
size_t bignum_base::obj_cnt = 0;
#endif

#ifdef CL_TEST_CODE
void _CL_CALL test_bignum_t() {
   size_t ocnt = bignum_base::obj_cnt;
   size_t acnt = alloc_cnt;

   // test bignum_sx helper
   {
      bignum_sx<limb,byte,true> b(1);
      classert(b.len() == 1);
      classert(b.buf[0] == 1);
      b._set(255);
      classert(b.len() == 1);
      classert(b.buf[0] == 255);
   }
   classert(ocnt == bignum_base::obj_cnt); classert(acnt == alloc_cnt);

   {
      bignum_sx<limb,limb,true> u(1);
      classert(u.len() == 2);
      classert(u.buf[0] == 1);
      classert(u.buf[1] == 0);
      u._set((limb)-1);
      classert(u.len() == 2);
      classert(u.buf[0] == (limb)-1);
      classert(u.buf[1] == 0);  // limb is unsigned
   }
   classert(ocnt == bignum_base::obj_cnt); classert(acnt == alloc_cnt);

   {
      bignum_sx<limb,make_signed<limb>::type,true> s(-1);
      classert(s.len() == 1);
      classert(s.buf[0] == (limb)-1);
   }
   classert(ocnt == bignum_base::obj_cnt); classert(acnt == alloc_cnt);

   // empty ctor
   {
      bignum_t v;
      classert(v.len() == 0);
   }
   classert(ocnt == bignum_base::obj_cnt); classert(acnt == alloc_cnt);

   // ctor with int
   {
      bignum_t v(0xcaffee);
      classert(v.ptr != nullptr);
      classert(v.len() == 1);
      classert(v.ptr[0] == 0xcaffee);
   }
   classert(ocnt == bignum_base::obj_cnt); classert(acnt == alloc_cnt);

   // ctor with __int64
   {
      int64 u = 0xdecaf00caffee;
      bignum_t v(u);
      classert(v.ptr != nullptr);
      classert(v.len() == sizeof(int64) / sizeof(limb));
      classert(memcmp(v.ptr, &u, sizeof(int64)) == 0);
   }
   classert(ocnt == bignum_base::obj_cnt); classert(acnt == alloc_cnt);

   // ctor with limb size
   {
      bignum_t::small_type max = numeric_limits<bignum_t::small_type>::max();  // unsigned
      bignum_t v(max);
      classert(v.len() == 2);
      classert(v.ptr[0] == max);
      classert(v.ptr[1] == 0);  // sign extension
   }
   classert(ocnt == bignum_base::obj_cnt); classert(acnt == alloc_cnt);

   // copy ctor
   {
      bignum_t* a = new bignum_t(0xcaffee);
      bignum_t* b = new bignum_t(*a);
      classert(b->refcnt() == 2);
      classert(b->ptr[0] == 0xcaffee);
      delete a;
      classert(b->refcnt() == 1);
      classert(b->ptr[0] == 0xcaffee);
      delete b;
   }
   classert(ocnt == bignum_base::obj_cnt); classert(acnt == alloc_cnt);

   // assignment operator
   {
      bignum_t& a = *new bignum_t(0xcaffee);
      bignum_t& b = *new bignum_t;
      b = a;
      classert(a.ptr[0] == 0xcaffee);
      classert(b.ptr[0] == 0xcaffee);
      delete &a;
      classert(b.ptr[0] == 0xcaffee);
      delete &b;

      bignum_t c;
      c = 15;
      classert(c.ptr[0] == 15);

      c = c;
      classert(c == 15);
   }
   classert(ocnt == bignum_base::obj_cnt); classert(acnt == alloc_cnt);

   // comparison
   {
      bignum_t one = 1;
      bignum_t one2 = 1;
      bignum_t two = 2;
      bignum_t zero = 0;
      classert(one == one2);
      classert(one != two);
      classert(one < two);
      classert(two > one);
      classert(one <= two);
      classert(one <= one2);
      classert(two > one);
      classert(one2 >= one);
      classert(one == one);  // self

      classert(one == 1);
      classert(one != 2);
      classert(one < 2);
      classert(two > 1);
      classert(one <= 2);
      classert(one <= 1);
      classert(two > 1);
      classert(one >= 1);

      classert(one == (byte)1);
      classert(one != (byte)2);
      classert(one < (byte)2);
      classert(two > (byte)1);
      classert(one <= (byte)2);
      classert(one <= (byte)1);
      classert(two > (byte)1);
      classert(one >= (byte)1);

      classert(one == (int64)1);
      classert(one != (int64)2);
      classert(one < (int64)2);
      classert(two > (int64)1);
      classert(one <= (int64)2);
      classert(one <= (int64)1);
      classert(two > (int64)1);
      classert(one >= (int64)1);

      classert(one == (uint64)1);
      classert(one != (uint64)2);
      classert(one < (uint64)2);
      classert(two > (uint64)1);
      classert(one <= (uint64)2);
      classert(one <= (uint64)1);
      classert(two > (uint64)1);
      classert(one >= (uint64)1);

      classert(1 == one);
      classert(2 != one);
      classert(2 > one);
      classert(1 < two);
      classert(2 >= one);
      classert(1 >= one);
      classert(1 < two);
      classert(1 <= one);

      classert(one);
      classert(!zero);
   }
   classert(ocnt == bignum_base::obj_cnt); classert(acnt == alloc_cnt);

   // in-place addition
   {
      bignum_t a = 15;
      a += bignum_t(12);
      classert(a == 15+12);

      // force overflow and len extension
      a = numeric_limits<bignum_t::small_type>::max() / 2;  // can be saved in one word
      classert(a.len() == 1);
      a += bignum_t(12);
      classert(a.len() >= 2);
      classert(a.ptr[0] == numeric_limits<bignum_t::small_type>::max() / 2 + 12);
      classert(a.ptr[1] == 0);

      // force overflow and maxlen extension
      index old_len = a.maxlen();
      index i;
      for (i=0; i<old_len-1; ++i)
         a.ptr[i] = numeric_limits<bignum_t::small_type>::max();
      a.ptr[i] = ((bignum_t::small_type)1 << (bits(bignum_t::small_type) - 1)) - 1;
      a.len() = old_len;
      // a has the max positive value that can be stored in old_len words
      a += bignum_t(12);
      classert(a.len() > old_len);
      classert(a.ptr[0] == 11);
      for (i=1; i<old_len-1; ++i)
         classert(a.ptr[i] == 0);
      classert(a.ptr[i] == (bignum_t::small_type)1 << (bits(bignum_t::small_type) - 1));

      // refcnt == 2: force new allocation
      a = 15;
      bignum_t b = a;
      a += bignum_t(12);
      classert(a == 15+12);
      classert(b == 15);

      // check self add
      a = 15;
      a += a;
      classert(a == 30);

      // with build-in types
      a = 15;
      a += 12;
      classert(a == 15+12);

      a = numeric_limits<bignum_t::small_type>::max() / 2;  // can be saved in one word
      classert(a.len() == 1);
      a += 12;
      classert(a.len() >= 2);
      classert(a.ptr[0] == numeric_limits<bignum_t::small_type>::max() / 2 + 12);
      classert(a.ptr[1] == 0);

      // some signed tests
      a = 2;
      a += -3;
      classert(a == -1);

      a = -3;
      a += 2;
      classert(a == -1);

      a = -22;
      a += -3;
      classert(a == -25);

      a = numeric_limits<make_signed<bignum_t::small_type>::type>::min();
      a += -2;
      classert(a < numeric_limits<make_signed<bignum_t::small_type>::type>::min());
   }
   classert(ocnt == bignum_base::obj_cnt); classert(acnt == alloc_cnt);

   // regular addition
   {
      bignum_t a, b, c;
      a = 15;
      b = 12;
      c = a + b;
      classert(a == 15);
      classert(b == 12);
      classert(c == 15+12);

      a = 15;
      c = a + 12;
      classert(c == 15+12);

      // some signed tests
      a = 2;
      c = a + -3;
      classert(c == -1);

      a = -3;
      c = a + 2;
      classert(c == -1);

      a = -22;
      c = a + -3;
      classert(c == -25);

      a = numeric_limits<make_signed<bignum_t::small_type>::type>::min();
      c = a + -2;
      classert(c < numeric_limits<make_signed<bignum_t::small_type>::type>::min());

      bignum_t::small_type max = numeric_limits<bignum_t::small_type>::max();
      a = max;
      b = a + max;
      c = max + a;
      classert(b == c);
   }
   classert(ocnt == bignum_base::obj_cnt); classert(acnt == alloc_cnt);

   // in-place subtraction
   {
      bignum_t a = 15;
      a -= bignum_t(12);
      classert(a == 15-12);

      // force overflow and len extension
      a = numeric_limits<bignum_t::small_type>::max() / 2;  // can be saved in one word
      classert(a.len() == 1);
      a -= bignum_t(-12);
      classert(a.len() >= 2);
      classert(a.ptr[0] == numeric_limits<bignum_t::small_type>::max() / 2 + 12);
      classert(a.ptr[1] == 0);

      // force overflow and maxlen extension
      index old_len = a.maxlen();
      index i;
      for (i=0; i<old_len-1; ++i)
         a.ptr[i] = 0;
      a.ptr[i] = (bignum_t::small_type)1 << (bits(bignum_t::small_type) - 1);
      a.len() = old_len;
      classert(a < 0);
      // a has the min negative value that can be stored in old_len words
      a -= bignum_t(12);
      classert(a.len() > old_len);
      classert(a.ptr[0] == -12);
      for (i=1; i<old_len-1; ++i)
         classert(a.ptr[i] == (bignum_t::small_type)-1);
      classert(a.ptr[i] == ((bignum_t::small_type)1 << (bits(bignum_t::small_type) - 1)) - 1);
      classert(a.ptr[i+1] == (bignum_t::small_type)-1);

      // refcnt == 2: force new allocation
      a = 15;
      bignum_t b = a;
      a -= bignum_t(12);
      classert(a == 15-12);
      classert(b == 15);

      // check self sub
      a = 15;
      a -= a;
      classert(a == 0);

      // with build-in types
      a = 15;
      a -= 12;
      classert(a == 15-12);

      a = numeric_limits<bignum_t::small_type>::max() / 2;  // can be saved in one word
      classert(a.len() == 1);
      a -= -12;
      classert(a.len() >= 2);
      classert(a.ptr[0] == numeric_limits<bignum_t::small_type>::max() / 2 + 12);
      classert(a.ptr[1] == 0);

      // some signed tests
      a = 2;
      a -= 3;
      classert(a == -1);

      a = -3;
      a -= 2;
      classert(a == -5);

      a = -22;
      a -= -3;
      classert(a == -19);

      a = numeric_limits<make_signed<bignum_t::small_type>::type>::min();
      a -= 2;
      classert(a < numeric_limits<make_signed<bignum_t::small_type>::type>::min());
   }
   classert(ocnt == bignum_base::obj_cnt); classert(acnt == alloc_cnt);

   // regular subtraction
   {
      bignum_t a, b, c;
      a = 15;
      b = 12;
      c = a - b;
      classert(a == 15);
      classert(b == 12);
      classert(c == 15-12);

      a = 15;
      c = a - 12;
      classert(c == 15-12);

      // some signed tests
      a = 2;
      c = a - -3;
      classert(c == 5);

      a = -3;
      c = a - 2;
      classert(c == -5);

      a = -22;
      c = a - -3;
      classert(c == -19);

      a = numeric_limits<make_signed<bignum_t::small_type>::type>::min();
      c = a - 2;
      classert(c < numeric_limits<make_signed<bignum_t::small_type>::type>::min());

      bignum_t::small_type max = numeric_limits<bignum_t::small_type>::max();
      a = max;
      a = -a;
      b = a - max;
      c = max - a;
      classert(b == -c);
   }
   classert(ocnt == bignum_base::obj_cnt); classert(acnt == alloc_cnt);

   // unary minus
   {
      bignum_t a, b, c;
      a = numeric_limits<make_signed<bignum_t::small_type>::type>::max();
      classert(a.len() == 1);
      b = -a;
      classert(b.len() == 1);
      c = a + b;
      classert(c == 0);

      a += 1;
      classert(a.len() == 2);
      b = -a;  // b could be saved in 1 word, but what the heck
      c = a + b;
      classert(c == 0);
   }
   classert(ocnt == bignum_base::obj_cnt); classert(acnt == alloc_cnt);

   // in/decrement
   {
      bignum_t a, b, c;
      a = b = 12;
      c = ++b;
      classert(b == a + 1);
      classert(c == b);
      c = a++;
      classert(a == b);
      classert(c == a - 1);

      c = --b;
      classert(c == b);
      classert(b == a - 1);
      c = a--;
      classert(a == b);
      classert(c == a + 1);
   }
   classert(ocnt == bignum_base::obj_cnt); classert(acnt == alloc_cnt);

   // multiplication
   {
      bignum_t a, b, c;
      a = numeric_limits<bignum_t::small_type>::max();
      b = 3;
      c = a * b;
      classert(c == a + a + a);

      c = a;
      c *= 3;
      classert(c == a + a + a);

      // signed
      a = numeric_limits<bignum_t::small_type>::max();
      b = -3;
      c = a * b;
      classert(-c == a + a + a);

      c = a;
      c *= -3;
      classert(-c == a + a + a);

      bignum_t::small_type max = numeric_limits<bignum_t::small_type>::max();
      a = max;
      b = 3 * a;
      c = a * 3;
      classert(b == c);
   }
   classert(ocnt == bignum_base::obj_cnt); classert(acnt == alloc_cnt);

   // bitwise and
   {
      bignum_t a, b, c, d;
      a = 0x5555aaaa00caffeeui64;
      b = 0xffffffffffffffffui64; ++b;
      a += a*(b + b*(b + b*b));  // 0x5555aaaa00caffee5555aaaa00caffee5555aaaa00caffee5555aaaa00caffee
      b = 0x80550bad2342abd6ui64; b *= 0x10000;
      c = a & b;
      d = 0x80440105220200c2ui64; d *= 0x10000;
      classert(c == d);

      a = 1+2+8+16;
      a &= 1+8;
      classert(a == 1+8);

      a = 0x2983156316384123ui64;
      a <<= 64;
      a |= 0x8961943513694148ui64;
      b = 0xcaffee & a;
      c = a & 0xcaffee;
      classert(b == c);
   }
   classert(ocnt == bignum_base::obj_cnt); classert(acnt == alloc_cnt);

   // bitwise or
   {
      bignum_t a, b, c, d;
      a = 0x5555aaaa00caffeeui64;
      b = 0xffffffffffffffffui64; ++b;
      a += a*b;  // 0x5555aaaa00caffee5555aaaa00caffee
      b = 0xd8c6b74d76b4ffb4ui64; b *= 0x100000000ui64;  // 0xd8c6b74d76b4ffb400000000
      c = a | b;
      d = 0x5555aaaad8ceffefui64; d *= 0x100000000ui64; d *= 0x100000000ui64; d += 0x77f5ffbe00caffeeui64;  // 0x5555aaaad8ceffef77f5ffbe00caffee
      classert(c == d);

      a = 1+8+16;
      a |= 2+4+16;
      classert(a == 1+2+4+8+16);

      a = 0x2983156316384123ui64;
      a <<= 64;
      a |= 0x8961943513694148ui64;
      b = 0xcaffee | a;
      c = a | 0xcaffee;
      classert(b == c);
   }
   classert(ocnt == bignum_base::obj_cnt); classert(acnt == alloc_cnt);

   // shift left
   {
      bignum_t a, b, c;
      a = 0x123;
      a <<= 4;
      classert(a == 0x1230);

      a = 0x123;
      a <<= 64;
      b = 0x123; b *= 0x100000000ui64; b *= 0x100000000ui64;
      classert(a == b);

      a = 0x123;
      c = a << 4;
      classert(c == 0x1230);

      a = 0x123;
      c = a << 64;
      b = 0x123; b *= 0x100000000ui64; b *= 0x100000000ui64;
      classert(c == b);
   }
   classert(ocnt == bignum_base::obj_cnt); classert(acnt == alloc_cnt);

   // shift right
   {
      bignum_t a, b, c;
      a = 0x1230;
      a >>= 4;
      classert(a == 0x123);

      a = 0x123; a *= 0x100000000ui64; a *= 0x100000000ui64;
      a >>= 64;
      classert(a == 0x123);

      a = 0x1230;
      c = a >> 4;
      classert(c == 0x123);

      a = 0x123; a *= 0x100000000ui64; a *= 0x100000000ui64;
      c = a >> 64;
      classert(c == 0x123);
   }
   classert(ocnt == bignum_base::obj_cnt); classert(acnt == alloc_cnt);

   // type casting
   {
      bignum_t a;
      a = 22;
      uint8 u8 = a;
      classert(u8 == 22);
      uint32 u32 = a;
      classert(u32 == 22);
      uint64 u64 = a;
      classert(u64 == 22);
   }
   classert(ocnt == bignum_base::obj_cnt); classert(acnt == alloc_cnt);

   // division / modulo division
   {
      bignum_t a, b, c, d;
      a = 0x1234 * 0x8765 + 0x333;
      b = 0x1234;
      c = a / b;
      classert(c == 0x8765);
      d = a % b;
      classert(d == 0x333);

      const uint64 u = 4294967291ui64 * 4294967279;
      a = u;
      b = a / 4294967279;
      classert(b == 4294967291);
      d = 4294967279;
      c = u / d;
      classert(c == 4294967291);
   }
   classert(ocnt == bignum_base::obj_cnt); classert(acnt == alloc_cnt);

   // in-place division / modulo division
   {
      bignum_t a, b, c, d;
      a = 0x1234 * 0x8765 + 0x333;
      b = 0x1234;
      a /= b;
      classert(a == 0x8765);
      a = 0x1234 * 0x8765 + 0x333;
      a %= b;
      classert(a == 0x333);

      const uint64 u = 4294967291ui64 * 4294967279 + 4711;
      a = u;
      b = a % 4294967279;
      classert(b == 4711);
      d = 4294967291;
      c = u % d;
      classert(c == 4711);
   }
   classert(ocnt == bignum_base::obj_cnt); classert(acnt == alloc_cnt);

   // pow
   {
      bignum_t a, b, c;
      a = 2;
      b = a.pow(27);
      c = 1;  c <<= 27;
      classert(b == c);

      a = 3;
      b = a.pow(66*40);
      a = 3ui64*3*3*3*3*3*3*3*3*3*3*3*3*3*3*3*3*3*3*3*3*3*3*3*3*3*3*3*3*3*3*3*3*3*3*3*3*3*3*3;
      c = a.pow(66);
      classert(b == c);
   }

   // squ
   {
      bignum_t a, b, c;
      a = 4123456789;
      a.pow(52);  // long
      b = a.pow(2);
      c = a.squ();
      classert(b == c);
   }

   // mod_pow
   {
      bignum_t a, b, c, m;
      m = 1000000000;
      a = 2;
      b = a.mod_pow(29, m);
      c = 1;  c <<= 29;
      classert(b == c);

      m = 4294967291;  // prime
      a = 3;
      b = a.mod_pow(219, m);
      classert(b == 2152310325);
   }

   // to_dbl
   {
      bignum_t a;
      double d;
      a = 0;
      d = a.to_dbl();
      classert(d == 0.0);

      a = 12345678;
      d = a.to_dbl();
      classert(d == 12345678.0);

      a = 0xf234567812345678;
      a <<= 64;
      a |= 0xabcdef01abcdef01;
      // a = 0xf234567812345678abcdef01abcdef01 = 321944928216832870703692666415780589313 = 3.2194492821683288e+38
      d = a.to_dbl();
      classert(d > 3.219449282168328e+38 && d < 3.219449282168329e+38);

      a = 0x34567812345678;
      a <<= 64;
      a |= 0xabcdef01abcdef01;
      // a = 0x34567812345678abcdef01abcdef01 = 271753236883229460971357827937201921 = 2.7175323688322945e+35
      d = a.to_dbl();
      classert(d > 2.717532368832294e+35 && d < 2.717532368832295e+35);
   }

   // int_sqrt
   {
      bignum_t a, b, c;
      a = 0xf1974cbde73812bfui64;
      b = a*a;
      c = int_sqrt(b);
      classert(c == a);
      b += a*2;
      c = int_sqrt(b);
      classert(c == a);
      ++b;
      ++a;
      classert(b == a*a);
      c = int_sqrt(b);
      classert(c == a);
   }
   classert(ocnt == bignum_base::obj_cnt); classert(acnt == alloc_cnt);

   // mod_inv
   {
      bignum_t a, b, c;
      a = s_to<bignum_t>("2305776048495723047972304305982304832");
      b = s_to<bignum_t>("2305776048495723047972304305982304999");
      c = a.mod_inv(b);
      classert(a * c % b == 1);
   }
   classert(ocnt == bignum_base::obj_cnt); classert(acnt == alloc_cnt);

   // conversions from other small types, assignment op
   {
      // both types signed
      #define TESTO(x)                       \
      {                                      \
         basic_bignum<uint32> a;             \
         basic_bignum<uint16> d;             \
         a = x;                              \
         d = a;                              \
         classert(d == x);                   \
         a = d;                              \
         classert(a == x);                   \
      }
      TESTO(123);
      TESTO(0xff00ee11);
      TESTO(-1);
      TESTO(-2000000000);
      #undef TESTO

      // both types unsigned
      #define TESTO(x)                       \
      {                                      \
         basic_bignum<uint32, false> a;      \
         basic_bignum<uint16, false> d;      \
         a = x;                              \
         d = a;                              \
         classert(d == x);                   \
         a = d;                              \
         classert(a == x);                   \
      }
      TESTO(123);
      TESTO(0xff00ee11);
      #undef TESTO

      // large unsigned, small signed
      #define TESTO(x)                       \
      {                                      \
         basic_bignum<uint32, false> a;      \
         basic_bignum<uint16> d;             \
         a = x;                              \
         d = a;                              \
         classert(d == x);                   \
         a = d;                              \
         classert(a == x);                   \
      }
      TESTO(123);
      TESTO(0xff00ee11);
      #undef TESTO

      // large signed, small unsigned
      #define TESTO(x)                       \
      {                                      \
         basic_bignum<uint16, false> a;      \
         basic_bignum<uint32> d;             \
         a = x;                              \
         d = a;                              \
         classert(d == x);                   \
         a = d;                              \
         classert(a == x);                   \
      }
      TESTO(123);
      TESTO(0xff00ee11);
      #undef TESTO
   }
   classert(ocnt == bignum_base::obj_cnt); classert(acnt == alloc_cnt);

   // conversions from other small types, copy ctor
   {
      // both types signed
      #define TESTO(x)                       \
      {                                      \
         basic_bignum<uint32> a;             \
         a = x;                              \
         basic_bignum<uint16> d(a);          \
         classert(d == x);                   \
         basic_bignum<uint32> b(d);          \
         classert(b == x);                   \
      }
      TESTO(123);
      TESTO(0xff00ee11);
      TESTO(-1);
      TESTO(-2000000000);
      #undef TESTO

      // both types unsigned
      #define TESTO(x)                       \
      {                                      \
         basic_bignum<uint32, false> a;      \
         a = x;                              \
         basic_bignum<uint16, false> d(a);   \
         classert(d == x);                   \
         basic_bignum<uint32, false> b(d);   \
         classert(b == x);                   \
      }
      TESTO(123);
      TESTO(0xff00ee11);
      #undef TESTO

      // large unsigned, small signed
      #define TESTO(x)                       \
      {                                      \
         basic_bignum<uint32, false> a;      \
         a = x;                              \
         basic_bignum<uint16> d(a);          \
         classert(d == x);                   \
         basic_bignum<uint32, false> b(d);   \
         classert(b == x);                   \
      }
      TESTO(123);
      TESTO(0xff00ee11);
      #undef TESTO

      // large signed, small unsigned
      #define TESTO(x)                       \
      {                                      \
         basic_bignum<uint16, false> a;      \
         a = x;                              \
         basic_bignum<uint32> d(a);          \
         classert(d == x);                   \
         basic_bignum<uint16, false> b(d);   \
         classert(b == x);                   \
      }
      TESTO(123);
      TESTO(0xff00ee11);
      #undef TESTO
   }
   classert(ocnt == bignum_base::obj_cnt); classert(acnt == alloc_cnt);

   // pow2, signed
   {
      bignum_t a, b, c;
      a = a.pow2(0);
      classert(a == 1);

      a = a.pow2(1);
      classert(a == 2);

      b = 2;
      a = a.pow2(1234);
      c = b.pow(1234);
      classert(a == c);

      a = a.pow2(1023);
      c = b.pow(1023);
      classert(a == c);

      a = a.pow2(1024);
      c = b.pow(1024);
      classert(a == c);
   }
   classert(ocnt == bignum_base::obj_cnt); classert(acnt == alloc_cnt);

   // pow2, unsigned
   {
      ubignum_t a, b, c;
      a = a.pow2(0);
      classert(a == 1);

      a = a.pow2(1);
      classert(a == 2);

      b = 2;
      a = a.pow2(1234);
      c = b.pow(1234);
      classert(a == c);

      a = a.pow2(1023);
      c = b.pow(1023);
      classert(a == c);

      a = a.pow2(1024);
      c = b.pow(1024);
      classert(a == c);
   }
   classert(ocnt == bignum_base::obj_cnt); classert(acnt == alloc_cnt);

   // low_bits
   {
      bignum_t a, b;
      a = 0x471a;
      b = a.low_bits(4);
      classert(b == 0xa);
   }
   classert(ocnt == bignum_base::obj_cnt); classert(acnt == alloc_cnt);

   // xxx
   {
      bignum_t a, b, c;
   }
   classert(ocnt == bignum_base::obj_cnt); classert(acnt == alloc_cnt);
}


static bool test_bignum_ll(bit_index p) {
   bignum_t u = 4;
   bignum_t mod = 1;
   mod <<= p;
   mod -= 1;
   for (bit_index i=2; i<p; ++i) {
      u = (u*u - 2) % mod;
      // do some useless stuff
      u <<= 1;
      u /= 2;
      u += u;
      u *= 2;
      u >>= 2;
      u = u << 1;
      u = u / 2;
      u = u + u;
      u = u * 2;
      u = u >> 2;
   }
   return u == 0;
}

void _CL_CALL test_bignum_all_ll() {
   // lucas-lehmer test
   dword start = GetTickCount();
   bit_index primes[] = { 3, 5, 7, 13, 17, 19, 31, 61, 89, 107, 127/*, 521, 607, 1279*/ };
   for (size_t i=0; i<NBEL(primes); ++i) {
      auto p = primes[i];
      classert(test_bignum_ll(p));
   }
   dword diff = GetTickCount() - start;
   diff = diff;  // stop whining!
}
#endif  // def CL_TEST_CODE

CL_DECLSPEC_NOINLINE void wusel(bignum_t& a) {
   ++a;
}

}  // namespace bignum


namespace cl {

   // ------ bignum_t format -----------------------------------------------------

   template<class C, bool is_signed> CL_DECLSPEC_NOINLINE
   format_base<C>& _CL_CALL operator<<(format_base<C>& f, const bignum::basic_bignum<bignum::limb, is_signed>& b) {
      C t = f.get_type();
      basic_string<C> s;
      switch (t) {
      default:
      case 'd':
      case 'u':
      case 'i':
      case '?':
         s = i_to<basic_string<C>>(b);
         break;
      case 'o':
         s = i_to<8, i_to_charset, basic_string<C>>(b);
         break;
      case 'x':
         s = i_to<16, i_to_charset, basic_string<C>>(b);
         break;
      case 'X':
         s = i_to<16, i_to_charset_upr, basic_string<C>>(b);
         break;
      case 'z':  // base36 lower case
         s = i_to<36, i_to_charset, basic_string<C>>(b);
         break;
      case 'Z':  // base36 upper case
         s = i_to<36, i_to_charset_upr, basic_string<C>>(b);
         break;
      case 'm':  // base64
         s = i_to<64, i_to_charset_base64, basic_string<C>>(b);
         break;
      }
      f.print_string_low(s.data(), s.length());
      return f;
   }

   #pragma inline_depth(0)
      void _generate_bignum_t_format() {
         classertm("must not be called", 0);
         format("") << *(bignum_t*)1;
         wformat(L"") << *(bignum_t*)1;
         format("") << *(ubignum_t*)1;
         wformat(L"") << *(ubignum_t*)1;
      }
   #pragma inline_depth()

}  // namespace cl


namespace bignum {

#ifdef CL_TEST_CODE
   void test_bignum_format() {
      unsigned u = 1000000;
      bignum_t b = u;
      string su, sb;
      wstring wu, wb;

      const char* fmts[] = { "%?", "%i", "%u", "%d", "%x", "%X", "%o", "%m", "%z", "%Z", nullptr };
      for (unsigned i = 0; fmts[i] != nullptr; ++i) {
         const char* fmt = fmts[i];
         su = format(fmt) << u;
         sb = format(fmt) << b;
         classert(su == sb);
      }
      for (unsigned i = 0; fmts[i] != nullptr; ++i) {
         wstring fmt = str2wstr(fmts[i]);
         wu = wformat(fmt.c_str()) << u;
         wb = wformat(fmt.c_str()) << b;
         classert(wu == wb);
      }
   }
#endif

}  // namespace bignum
