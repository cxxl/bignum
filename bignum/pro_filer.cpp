// $+HEADER$
// 
// Copyright 2015 Christoph Lueders
// 
// This file is part of the BIGNUM Library.
// 
// The BIGNUM Library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
// 
// The BIGNUM Library is distributed in the hope that it will be useful, but 
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public 
// License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with the BIGNUM Library.  If not, see <http://www.gnu.org/licenses/>.
// 
// $-HEADER$

// $Date: 2015-04-24 11:28:16 +0200 (Fr, 24 Apr 2015) $
// $Rev: 26590 $

#include "pro_filer.h"
#include "cl/vio.h"
#include "cl/str_conv.h"
#include "cl/format.h"

using namespace std;
using namespace cl;

namespace bignum {

pro_filer::pro_filer() {
   active = false;
   cycle_adjust = 0;
   cumulated_adjust = 0;
   total_id = register_counter("total");
}

void pro_filer::all_start() {
   clear();
   calibrate();
   active = true;
   start(total_id);
}

void pro_filer::all_stop() {
   stop(total_id);
   active = false;
}

void pro_filer::clear() {
   for (size_t i=0; i<data.size(); ++i) {
      pro_filer_data& d = data[i];
      d.clear_counts();
   }
}

void pro_filer::calibrate() {
   bool old_act = active;
   active = true;
   cycle_adjust = 0;
   const size_t id = register_counter("$$calibrate");
   get_cyc();  // warm up
   const unsigned rounds = 10;
   for (unsigned i=0; i<rounds; ++i) {
      start(id);
      stop(id);
   }
   const pro_filer_data& d = data[id];
   cycle_adjust = d.cycle_sum / rounds;
   data.erase(data.begin() + id, data.end());
   active = old_act;
}

pro_filer_data* pro_filer::find(const string& name) const {
   for (size_t i=0; i<data.size(); ++i) {
      const pro_filer_data& d = data[i];
      if (d.name == name) return const_cast<pro_filer_data*>(&d);
   }
   return nullptr;
}

size_t pro_filer::register_counter(const string& name) {
   data.push_back(pro_filer_data(name));
   return data.size()-1;
}

string pro_filer::output() const {
   auto_cols ac;
   ac.def_col_spaces = 3;
   ac.set_next("function");
   ac.set_col_align(-1, auto_cols::align_left);
   ac.set_next("cycles");
   ac.set_col_align(-1, auto_cols::align_right);
   ac.set_next("cycle %");
   ac.set_col_align(-1, auto_cols::align_right);
   ac.set_next("calls");
   ac.set_col_align(-1, auto_cols::align_right);
   ac.newline();
   ac.newline();

   const pro_filer_data& td = data[total_id];
   for (size_t i=0; i<data.size(); ++i) {
      const pro_filer_data& d = data[i];
      ac.set_next(d.name);
      ac.set_next(format("%'?") << d.cycle_sum);
      const double perc = td.cycle_sum == 0 ? 0.0 : 100.0 * d.cycle_sum / td.cycle_sum;
      ac.set_next(format("%6.2?%%") << perc);
      ac.set_next(format("%'?") << d.call_count);
      ac.newline();
   }
   return ac.str_print() + (string)(format("\ncycle_adjust = %'?\n") << cycle_adjust);
}

}  // namespace bignum
