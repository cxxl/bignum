; $+HEADER$
; 
; Copyright 2015 Christoph Lueders
; 
; This file is part of the BIGNUM Library.
; 
; The BIGNUM Library is free software: you can redistribute it and/or modify
; it under the terms of the GNU Lesser General Public License as published by
; the Free Software Foundation, either version 3 of the License, or (at your 
; option) any later version.
; 
; The BIGNUM Library is distributed in the hope that it will be useful, but 
; WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
; or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public 
; License for more details.
; 
; You should have received a copy of the GNU Lesser General Public License
; along with the BIGNUM Library.  If not, see <http://www.gnu.org/licenses/>.
; 
; $-HEADER$

; $Date: 2015-04-24 11:28:16 +0200 (Fr, 24 Apr 2015) $
; $Rev: 26590 $


_TEXT SEGMENT

; ------ ADDITION/SUBTRACTION IN ONE ------------------------------------------

stackframe = 4*8

rbx_save = stackframe - 32
rsi_save = stackframe - 24
rdi_save = stackframe - 16
rbp_save = stackframe - 8

r12_save = stackframe + 8     ; shadow space
r13_save = stackframe + 16    ; shadow space
r14_save = stackframe + 24    ; shadow space
r15_save = stackframe + 32    ; shadow space
len$ = stackframe + 40        ; parameter on stack

   align 16

?addsub_asmx64@bignum@@YA_KPEA_K0PEB_K1_K@Z PROC

; uint64 __cdecl addsub_asmx64(uint64* sum, uint64* diff, const uint64* a, const uint64* b, const uint64 len) {

; parameters:
; rcx = sum
; rdx = diff
; r8 = a
; r9 = b
; [rsp+40] = len

; rbx, rsi, rdi, rbp, r12 - r15 must be preserved by callee
; may be destroyed: rax, rcx, rdx, r8, r9, r10, r11
; http://msdn.microsoft.com/en-us/magazine/cc300794.aspx

   sub   rsp, stackframe

   mov   QWORD PTR rbx_save[rsp], rbx
   mov   QWORD PTR rsi_save[rsp], rsi
   mov   QWORD PTR rdi_save[rsp], rdi
   mov   QWORD PTR rbp_save[rsp], rbp
   mov   QWORD PTR r12_save[rsp], r12
   mov   QWORD PTR r13_save[rsp], r13
   mov   QWORD PTR r14_save[rsp], r14
   mov   QWORD PTR r15_save[rsp], r15

   mov   rbx, r8                          ; r8 ~ a
   mov   rsi, r9                          ; r9 ~ b
   sub   rsi, rbx
   mov   rdi, rcx                         ; rcx ~ sum
   sub   rdi, rbx
   mov   rbp, rdx                         ; rdx ~ diff
   sub   rbp, rbx
   mov   r10, QWORD PTR len$[rsp]
   lea   rcx, QWORD PTR [rbx+r10*8]       ; end pointer

   xor   eax, eax
   xor   edx, edx
   and   r10, 7
   jz    add_loop_check

   align 16
init_loop:
   mov   r8, QWORD PTR [rbx]              ; a[i]
   mov   r9, QWORD PTR [rsi+rbx]          ; b[i]

   rcr   eax, 1                           ; load add carry

   mov   rax, r8
   adc   rax, r9
   mov   QWORD PTR [rdi+rbx], rax         ; sum[i]

   setc  al                               ; save add carry
   rcr   edx, 1                           ; load sub carry

   sbb   r8, r9
   mov   QWORD PTR [rbp+rbx], r8          ; diff[i]

   setc  dl                               ; save sub carry

   lea   rbx, QWORD PTR [rbx+8]
   dec   r10
   jnz   init_loop

   jmp   add_loop_check


add_loop:
   mov   r8, QWORD PTR [rbx]              ; a[0]
   mov   r10, QWORD PTR [rbx+8]           ; a[1]
   mov   r12, QWORD PTR [rbx+16]          ; a[2]
   mov   r14, QWORD PTR [rbx+24]          ; a[3]

   mov   r9, QWORD PTR [rsi+rbx]          ; b[0]
   mov   r11, QWORD PTR [rsi+rbx+8]       ; b[1]
   mov   r13, QWORD PTR [rsi+rbx+16]      ; b[2]
   mov   r15, QWORD PTR [rsi+rbx+24]      ; b[3]

   rcr   eax, 1                           ; load add carry

   mov   rax, r8
   adc   rax, r9
   mov   QWORD PTR [rdi+rbx], rax         ; sum[0]

   mov   rax, r10
   adc   rax, r11
   mov   QWORD PTR [rdi+rbx+8], rax       ; sum[1]

   mov   rax, r12
   adc   rax, r13
   mov   QWORD PTR [rdi+rbx+16], rax      ; sum[2]

   mov   rax, r14
   adc   rax, r15
   mov   QWORD PTR [rdi+rbx+24], rax      ; sum[3]

   setc  al                               ; save add carry
   rcr   edx, 1                           ; load sub carry

   sbb   r8, r9
   mov   QWORD PTR [rbp+rbx], r8          ; diff[0]

   sbb   r10, r11
   mov   QWORD PTR [rbp+rbx+8], r10       ; diff[1]

   sbb   r12, r13
   mov   QWORD PTR [rbp+rbx+16], r12      ; diff[2]

   sbb   r14, r15
   mov   QWORD PTR [rbp+rbx+24], r14      ; diff[3]

   mov   r8, QWORD PTR [rbx+32]           ; a[4]
   mov   r10, QWORD PTR [rbx+40]          ; a[5]
   mov   r12, QWORD PTR [rbx+48]          ; a[6]
   mov   r14, QWORD PTR [rbx+56]          ; a[7]

   mov   r9, QWORD PTR [rsi+rbx+32]       ; b[4]
   mov   r11, QWORD PTR [rsi+rbx+40]      ; b[5]
   mov   r13, QWORD PTR [rsi+rbx+48]      ; b[6]
   mov   r15, QWORD PTR [rsi+rbx+56]      ; b[7]

   mov   rdx, r8
   sbb   rdx, r9
   mov   QWORD PTR [rbp+rbx+32], rdx      ; diff[4]

   mov   rdx, r10
   sbb   rdx, r11
   mov   QWORD PTR [rbp+rbx+40], rdx      ; diff[5]

   mov   rdx, r12
   sbb   rdx, r13
   mov   QWORD PTR [rbp+rbx+48], rdx      ; diff[6]

   mov   rdx, r14
   sbb   rdx, r15
   mov   QWORD PTR [rbp+rbx+56], rdx      ; diff[7]

   setc  dl                               ; save sub carry
   rcr   eax, 1                           ; load add carry

   adc   r8, r9
   mov   QWORD PTR [rdi+rbx+32], r8       ; sum[4]

   adc   r10, r11
   mov   QWORD PTR [rdi+rbx+40], r10      ; sum[5]

   adc   r12, r13
   mov   QWORD PTR [rdi+rbx+48], r12      ; sum[6]

   adc   r14, r15
   mov   QWORD PTR [rdi+rbx+56], r14      ; sum[7]

   setc  al                               ; save add carry

   add   rbx, 8*8
add_loop_check:
   cmp   rbx, rcx
   jb    add_loop

   shl   edx, 1
   or    eax, edx                         ; return value is sub_carry:add_carry
   and   eax, 3

   mov   rbx, QWORD PTR rbx_save[rsp]
   mov   rsi, QWORD PTR rsi_save[rsp]
   mov   rdi, QWORD PTR rdi_save[rsp]
   mov   rbp, QWORD PTR rbp_save[rsp]
   mov   r12, QWORD PTR r12_save[rsp]
   mov   r13, QWORD PTR r13_save[rsp]
   mov   r14, QWORD PTR r14_save[rsp]
   mov   r15, QWORD PTR r15_save[rsp]

   add   rsp, stackframe

   ret   0

?addsub_asmx64@bignum@@YA_KPEA_K0PEB_K1_K@Z ENDP

_TEXT ENDS
END
