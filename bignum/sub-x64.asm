; $+HEADER$
; 
; Copyright 2015 Christoph Lueders
; 
; This file is part of the BIGNUM Library.
; 
; The BIGNUM Library is free software: you can redistribute it and/or modify
; it under the terms of the GNU Lesser General Public License as published by
; the Free Software Foundation, either version 3 of the License, or (at your 
; option) any later version.
; 
; The BIGNUM Library is distributed in the hope that it will be useful, but 
; WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
; or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public 
; License for more details.
; 
; You should have received a copy of the GNU Lesser General Public License
; along with the BIGNUM Library.  If not, see <http://www.gnu.org/licenses/>.
; 
; $-HEADER$

; $Date: 2015-04-24 11:28:16 +0200 (Fr, 24 Apr 2015) $
; $Rev: 26590 $


_TEXT SEGMENT

; ------ SUBTRACTION 8-times unrolled -----------------------------------------

   align 16

?sub8_asmx64@bignum@@YA_KPEA_KPEB_K1_K@Z PROC

; uint64 __fastcall sub8_asmx64(uint64* r, const uint64* a, const uint64* b, const uint64 len) {

; rcx = r
; rdx = a
; r8 = b
; r9 = len

; rbx, rsi, rdi, rbp, r12 - r15 must be preserved by callee
; may be destroyed: rax, rcx, rdx, r8, r9, r10, r11
; http://msdn.microsoft.com/en-us/magazine/cc300794.aspx
; even better:
; http://blogs.msdn.com/b/oldnewthing/archive/2004/01/14/58579.aspx

; rax = accu
; rcx = r
; rdx = a
; r8 = b
; r9 = len
; r10 = accu #2
; r11 = i

   mov   r11, r9
   and   r11, 7
   lea   r10, offset jump_table8
   add   r10, QWORD PTR [r10+r11*8]
   shr   r9, 3
   inc   r9
   clc
   jmp   r10


   align 16
sub8_loop:
   mov   rax, QWORD PTR [rdx+r11*8]
   mov   r10, QWORD PTR [r8+r11*8]
   sbb   rax, r10
   mov   QWORD PTR [rcx+r11*8], rax
   lea   r11, QWORD PTR [r11+8]

l8_7:
   mov   rax, QWORD PTR [rdx+r11*8-56]
   mov   r10, QWORD PTR [r8+r11*8-56]
   sbb   rax, r10
   mov   QWORD PTR [rcx+r11*8-56], rax

l8_6:
   mov   rax, QWORD PTR [rdx+r11*8-48]    ; rax = a[-6]
   mov   r10, QWORD PTR [r8+r11*8-48]     ; r10 = b[-6]
   sbb   rax, r10                         ; rax += r10 + cf
   mov   QWORD PTR [rcx+r11*8-48], rax

l8_5:
   mov   rax, QWORD PTR [rdx+r11*8-40]
   mov   r10, QWORD PTR [r8+r11*8-40]
   sbb   rax, r10
   mov   QWORD PTR [rcx+r11*8-40], rax

l8_4:
   mov   rax, QWORD PTR [rdx+r11*8-32]
   mov   r10, QWORD PTR [r8+r11*8-32]
   sbb   rax, r10
   mov   QWORD PTR [rcx+r11*8-32], rax

l8_3:
   mov   rax, QWORD PTR [rdx+r11*8-24]
   mov   r10, QWORD PTR [r8+r11*8-24]
   sbb   rax, r10
   mov   QWORD PTR [rcx+r11*8-24], rax

l8_2:
   mov   rax, QWORD PTR [rdx+r11*8-16]
   mov   r10, QWORD PTR [r8+r11*8-16]
   sbb   rax, r10
   mov   QWORD PTR [rcx+r11*8-16], rax

l8_1:
   mov   rax, QWORD PTR [rdx+r11*8-8]
   mov   r10, QWORD PTR [r8+r11*8-8]
   sbb   rax, r10
   mov   QWORD PTR [rcx+r11*8-8], rax

l8_0:
   dec   r9
   jnz   sub8_loop

   setc  al
   and   eax, 1                           ; rax contains carry
   ret   0


  align 16
jump_table8:
   DQ    l8_0 - jump_table8
   DQ    l8_1 - jump_table8
   DQ    l8_2 - jump_table8
   DQ    l8_3 - jump_table8
   DQ    l8_4 - jump_table8
   DQ    l8_5 - jump_table8
   DQ    l8_6 - jump_table8
   DQ    l8_7 - jump_table8

?sub8_asmx64@bignum@@YA_KPEA_KPEB_K1_K@Z ENDP



; ------ SUBTRACTION 4-times unrolled -----------------------------------------

   align 16

?sub4_asmx64@bignum@@YA_KPEA_KPEB_K1_K@Z PROC

; uint64 __fastcall sub4_asmx64(uint64* r, const uint64* a, const uint64* b, const uint64 len) {

; rcx = r
; rdx = a
; r8 = b
; r9 = len

; rbx, rsi, rdi, rbp, r12 - r15 must be preserved by callee
; may be destroyed: rax, rcx, rdx, r8, r9, r10, r11
; http://msdn.microsoft.com/en-us/magazine/cc300794.aspx
; even better:
; http://blogs.msdn.com/b/oldnewthing/archive/2004/01/14/58579.aspx

; rax = accu
; rcx = r
; rdx = a
; r8 = b
; r9 = len
; r10 = accu #2
; r11 = i

   mov   r11, r9
   and   r11, 3
   lea   r10, offset jump_table4
   add   r10, QWORD PTR [r10+r11*8]
   shr   r9, 2
   inc   r9
   clc
   jmp   r10


   align 16
sub4_loop:
   mov   rax, QWORD PTR [rdx+r11*8]
   mov   r10, QWORD PTR [r8+r11*8]
   sbb   rax, r10
   mov   QWORD PTR [rcx+r11*8], rax
   lea   r11, QWORD PTR [r11+4]

l43:
   mov   rax, QWORD PTR [rdx+r11*8-24]
   mov   r10, QWORD PTR [r8+r11*8-24]
   sbb   rax, r10
   mov   QWORD PTR [rcx+r11*8-24], rax

l42:
   mov   rax, QWORD PTR [rdx+r11*8-16]
   mov   r10, QWORD PTR [r8+r11*8-16]
   sbb   rax, r10
   mov   QWORD PTR [rcx+r11*8-16], rax

l41:
   mov   rax, QWORD PTR [rdx+r11*8-8]
   mov   r10, QWORD PTR [r8+r11*8-8]
   sbb   rax, r10
   mov   QWORD PTR [rcx+r11*8-8], rax

l40:
   dec   r9
   jnz   sub4_loop

   setc  al
   and   eax, 1                           ; rax contains carry
   ret   0


  align 16
jump_table4:
   DQ    l40 - jump_table4
   DQ    l41 - jump_table4
   DQ    l42 - jump_table4
   DQ    l43 - jump_table4

?sub4_asmx64@bignum@@YA_KPEA_KPEB_K1_K@Z ENDP



; ------ SUBTRACTION 2-times unrolled -----------------------------------------

   align 16

?sub2_asmx64@bignum@@YA_KPEA_KPEB_K1_K@Z PROC

; uint64 __fastcall sub2_asmx64(uint64* r, const uint64* a, const uint64* b, const uint64 len) {

; rcx = r
; rdx = a
; r8 = b
; r9 = len

; rbx, rsi, rdi, rbp, r12 - r15 must be preserved by callee
; may be destroyed: rax, rcx, rdx, r8, r9, r10, r11
; http://msdn.microsoft.com/en-us/magazine/cc300794.aspx
; even better:
; http://blogs.msdn.com/b/oldnewthing/archive/2004/01/14/58579.aspx

; rax = accu
; rcx = r
; rdx = a
; r8 = b
; r9 = len
; r10 = accu #2
; r11 = i

   mov   r11, r9
   shr   r9, 1
   inc   r9
   and   r11, 1                           ; CF is 0
   jnz   l21
   jmp   short l20

sub2_loop:
   mov   rax, QWORD PTR [rdx+r11*8]
   mov   r10, QWORD PTR [r8+r11*8]
   sbb   rax, r10
   mov   QWORD PTR [rcx+r11*8], rax
   lea   r11, QWORD PTR [r11+2]

l21:
   mov   rax, QWORD PTR [rdx+r11*8-8]
   mov   r10, QWORD PTR [r8+r11*8-8]
   sbb   rax, r10
   mov   QWORD PTR [rcx+r11*8-8], rax

l20:
   dec   r9
   jnz   sub2_loop

   setc  al
   and   eax, 1                           ; rax contains carry
   ret   0

?sub2_asmx64@bignum@@YA_KPEA_KPEB_K1_K@Z ENDP


_TEXT ENDS

END
