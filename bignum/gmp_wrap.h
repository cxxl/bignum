// $+HEADER$
// 
// Copyright 2015 Christoph Lueders
// 
// This file is part of the BIGNUM Library.
// 
// The BIGNUM Library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
// 
// The BIGNUM Library is distributed in the hope that it will be useful, but 
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public 
// License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with the BIGNUM Library.  If not, see <http://www.gnu.org/licenses/>.
// 
// $-HEADER$

// $Date: 2015-04-24 11:47:36 +0200 (Fr, 24 Apr 2015) $
// $Rev: 26592 $

#ifndef CL_BIGNUM_GMP_WRAP_H
#define CL_BIGNUM_GMP_WRAP_H 1

#include "impl.h"

#pragma pack(push, 8)  // arbitrary but fixed

namespace bignum {

   using namespace cl;

   template<typename small> void gmp_mul(small* r, const small* a, index alen, const small* b, index blen);
   template<typename small> void gmp_mul_n(small* r, const small* a, const small* b, index len);
   template<typename small> void gmp_sqr(small* r, const small* a, index len);
   template<typename small> void gmp_mul_basecase(small* r, const small* a, index alen, const small* b, index blen);

}  // namespace bignum

#pragma pack(pop)

#endif  // ndef CL_BIGNUM_GMP_WRAP_H
