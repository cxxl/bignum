; $+HEADER$
; 
; Copyright 2015 Christoph Lueders
; 
; This file is part of the BIGNUM Library.
; 
; The BIGNUM Library is free software: you can redistribute it and/or modify
; it under the terms of the GNU Lesser General Public License as published by
; the Free Software Foundation, either version 3 of the License, or (at your 
; option) any later version.
; 
; The BIGNUM Library is distributed in the hope that it will be useful, but 
; WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
; or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public 
; License for more details.
; 
; You should have received a copy of the GNU Lesser General Public License
; along with the BIGNUM Library.  If not, see <http://www.gnu.org/licenses/>.
; 
; $-HEADER$

; $Date: 2015-04-24 11:28:16 +0200 (Fr, 24 Apr 2015) $
; $Rev: 26590 $



_TEXT SEGMENT

stackframe = 9*8

inner_jumpin = stackframe - 72
alen$ = stackframe - 64
j$1$ = stackframe - 56
r14_save = stackframe - 40
r15_save = stackframe - 32
rdi_save = stackframe - 24
rsi_save = stackframe - 16
rbx_save = stackframe - 8

rbp_save = stackframe + 8     ; shadow space
r12_save = stackframe + 16    ; shadow space
r13_save = stackframe + 24    ; shadow space
b$ = stackframe + 32          ; shadow space
blen$ = stackframe + 40       ; parameter on stack


   align 16

?omul4_asmx64@bignum@@YAXPEA_KPEB_K_K12@Z PROC

; 343  : void omul4_asm(small* r, const small* a, index alen, const small* b, index blen) {

; parameters:
; rcx = r
; rdx = a
; r8 = alen
; r9 = b
; [rsp+40] = blen

; rbx, rsi, rdi, rbp, r12 - r15 must be preserved by callee
; may be destroyed: rax, rcx, rdx, r8, r9, r10, r11
; http://msdn.microsoft.com/en-us/magazine/cc300794.aspx

   sub   rsp, stackframe

   mov   QWORD PTR r15_save[rsp], r15
   mov   QWORD PTR rdi_save[rsp], rdi
   mov   QWORD PTR rsi_save[rsp], rsi
   mov   QWORD PTR rbx_save[rsp], rbx
   mov   QWORD PTR b$[rsp], r9            ; b
   mov   rbx, r9

   mov   r9, QWORD PTR blen$[rsp]         ; r9 = blen
   mov   alen$[rsp], r8                   ; r8 ~ alen
   mov   rsi, r8                          ; rsi = alen
   mov   r10, rdx
   mov   r15, rcx

; no need to check for zero, omul_ls() does that already
;; 350  :    if (blen == 0) {
;   test  r9, r9               ; r9 = blen
;   jnz   SHORT start_mul
;
;   test  r8, r8               ; r8 = alen
;   jz    pop_exit
;
;; 351  :       zero(r, alen+blen);
;
;   mov   rdi, rcx
;   xor   eax, eax
;   mov   rcx, r8
;   rep stosq
;   jmp   pop_exit
;
;   align 16
;
;start_mul:

; 357  :    // inner loop on first element:
; 358  :    // init the first 'alen' elements in result with product without adding up old memory contents.
; 359  :    small of1 = 0;
; 360  :    index i;
; 361  :    const small bb = b[0];

   mov   r8, QWORD PTR [rbx]              ; rbx ~ b
   mov   QWORD PTR r14_save[rsp], r14
   mov   QWORD PTR rbp_save[rsp], rbp
   mov   QWORD PTR r12_save[rsp], r12
   mov   QWORD PTR r13_save[rsp], r13

   mov   edi, esi                         ; rsi ~ alen
   and   edi, 7
   mov   rax, init_jump_table
   add   rax, QWORD PTR [rax+rdi*8]
   xor   ecx, ecx                         ; rcx ~ of1
   jmp   rax


;        of1 = ops<small>::muladdc(r[i+(o)], a[i+(o)], bb, 0, of1)

   align 16
init_loop:
   mov   rax, QWORD PTR [r10+rdi*8]
   mul   r8
   add   rax, rcx
   mov   rcx, rdx
   adc   rcx, 0
   mov   QWORD PTR [r15+rdi*8], rax
   add   rdi, 8

init7:
   mov   rax, QWORD PTR [r10+rdi*8-56]
   mul   r8
   add   rax, rcx
   mov   rcx, rdx
   adc   rcx, 0
   mov   QWORD PTR [r15+rdi*8-56], rax

init6:
   mov   rax, QWORD PTR [r10+rdi*8-48]
   mul   r8
   add   rax, rcx
   mov   rcx, rdx
   adc   rcx, 0
   mov   QWORD PTR [r15+rdi*8-48], rax

init5:
   mov   rax, QWORD PTR [r10+rdi*8-40]
   mul   r8
   add   rax, rcx
   mov   rcx, rdx
   adc   rcx, 0
   mov   QWORD PTR [r15+rdi*8-40], rax

init4:
   mov   rax, QWORD PTR [r10+rdi*8-32]
   mul   r8
   add   rax, rcx
   mov   rcx, rdx
   adc   rcx, 0
   mov   QWORD PTR [r15+rdi*8-32], rax

init3:
   mov   rax, QWORD PTR [r10+rdi*8-24]
   mul   r8
   add   rax, rcx
   mov   rcx, rdx
   adc   rcx, 0
   mov   QWORD PTR [r15+rdi*8-24], rax

init2:
   mov   rax, QWORD PTR [r10+rdi*8-16]
   mul   r8
   add   rax, rcx
   mov   rcx, rdx
   adc   rcx, 0
   mov   QWORD PTR [r15+rdi*8-16], rax

init1:
   mov   rax, QWORD PTR [r10+rdi*8-8]
   mul   r8
   add   rax, rcx
   mov   rcx, rdx
   adc   rcx, 0
   mov   QWORD PTR [r15+rdi*8-8], rax

init0:
   cmp   rdi, rsi
   jb    init_loop

; 367  :    r[i] = of1;  // save overflow so the next add in the inner loop works ok.

   mov   QWORD PTR [r15+rdi*8], rcx


; prepare for the main inner loop

   lea   r11, QWORD PTR [rsi-2]           ; rsi ~ alen
   mov   eax,0AAAAAAABh
   mul   r11d
   shr   edx, 1                           ; edx = (alen-2) / 3

   lea   ecx,[edx+edx*2]                  ; ecx = 3*edx
   mov   eax, r11d
   sub   eax, ecx                         ; eax = (alen-2) % 3

   mov   rdx, inner_jump_table
   add   rdx, QWORD PTR [rdx+rax*8]
   mov   inner_jumpin[rsp], rdx



; 369  :    // the main outer loop
; 370  :    index j = 1;

   mov   ebp, 1

   align 16
outer_main_loop:
; 372  :    while (j+2 < blen) {  // make sure three accesses will fit

; rbx : b
; rbx : of1    (yet to be loaded)
; rsi : alen
; rbp : j
; rbp : of2    (yet to be loaded)
; r8  : bb2    (constant, yet to be loaded)
; r9  : blen
; r9  : bb1    (constant, yet to be loaded)
; r10 : a
; r13 : bb0    (constant, yet to be loaded)
; r14 : of3    (yet to be loaded)
; r15 : r

   mov   QWORD PTR j$1$[rsp], rbp
   lea   rax, QWORD PTR [rbp+2]           ; rax = j + 2
   cmp   rax, r9                          ; r9 ~ blen
   jae   outer_leftover_check

; 374  :       const small bb0 = b[j];
; 375  :       const small bb1 = b[j+1];
; 376  :       const small bb2 = b[j+2];

   mov   r13, QWORD PTR [rbx+rbp*8]       ; r13 ~ bb0
   mov   r9, QWORD PTR [rbx+rbp*8+8]      ; r9 ~ bb1
   mov   r8, QWORD PTR [rbx+rax*8]        ; r8 ~ bb2

; 373  :       rm = r + j;

   lea   r12, QWORD PTR [r15+rbp*8]       ; r12 = rm = r+j
   mov   rcx, QWORD PTR [r12]             ; rcx = rm[0] = r[j]

; 377  :       small of2 = 0;
; 378  :       small of3 = 0;

   xor   r14d, r14d                       ; r14 ~ of3

; 381  :       of1 = ops<small>::muladdc(rm[0], a[0], bb0, rm[0], 0);  // of1:rm[0] = a[0] * b[0] + rm[0]

   mov   rax, QWORD PTR [r10]             ; rax = a[0]
   mul   r13                              ; a[0] * bb0

   add   rax, rcx                         ; kein input carry, da of1 = 0
   mov   rbx, rdx
   adc   rbx, 0                           ; rbx ~ output of1
   mov   QWORD PTR [r12], rax

; 383  :       of1 = ops<small>::muladdc(mx, a[0], bb1, rm[1], of1);  // of1:mx = a[0] * b[1] + rm[1] + of1

   mov   rcx, QWORD PTR [r12+8]           ; rcx = rm[1]
   mov   rax, QWORD PTR [r10]             ; a[0]
   mul   r9                               ; a[0] * bb1

   add   rcx, rbx                         ; rbx ~ input of1
   mov   rbx, rdx                         ; rbx ~ output of1
   adc   rbx, 0
   add   rcx, rax                         ; rcx ~ output mx
   adc   rbx, 0

;      :       of2 = ops<small>::muladdc(rm[1], a[1], bb0, mx, 0);  // of2:rm[1] = a[1] * b[0] + mx

   mov   rax, QWORD PTR [r10+8]           ; a[1]
   mul   r13                              ; a[1] * bb0

   add   rcx, rax                         ; kein input carry, da of2 = 0
   mov   rbp, rdx                         ; rbp ~ output of2
   adc   rbp, 0

   mov   QWORD PTR [r12+8], rcx


   mov   rax, inner_jumpin[rsp]
   jmp   rax

   align 16
start_inner2:
   mov   r11d, 2
   mov   rsi, QWORD PTR [r10+r11*8-16]
   mov   r15, QWORD PTR [r10+r11*8-8]
   jmp   inner2

   align 16
start_inner1:
   mov   r11d, 1
   mov   r15, QWORD PTR [r10+r11*8-8]
   mov   rdi, QWORD PTR [r10+r11*8]
   jmp   inner1

   align 16
start_inner0:
   xor   r11d, r11d
   mov   rdi, QWORD PTR [r10+r11*8]
   mov   rsi, QWORD PTR [r10+r11*8+8]
   jmp   inner0



   align 16
inner_loop:

;          of1 = ops<small>::muladdc(mx, a[i+(o)], bb2, rm[i+2+(o)], of1);
;          of2 = ops<small>::muladdc(mx, a[i+1+(o)], bb1, mx, of2);
;          of3 = ops<small>::muladdc(rm[i+2+(o)], a[i+2+(o)], bb0, mx, of3);

; register allocation in this loop:
; rax = scratch
; rbx = of1
; rcx = mx
; rdx = scratch
; rbp = of2
; rsi = memory a[]
; rdi = memory a[]
; r8  = bb2
; r9  = bb1
; r10 = a
; r11 = i
; r12 = rm
; r13 = bb0
; r14 = of3
; r15 = memory a[]

   ; r15 is free
   mov   rax, rdi                         ; QWORD PTR [r10+r11*8-24]
   ; rdi is now free
   mul   r8                               ; a[i] * bb2
   add   r11, 3
   mov   rcx, QWORD PTR [r12+r11*8-8]

   add   rcx, rbx                         ; rbx ~ input of1
   mov   rbx, rdx                         ; rbx ~ output of1
   adc   rbx, 0
   add   rcx, rax                         ; rcx ~ output mx
   adc   rbx, 0

   mov   rax, rsi                         ; QWORD PTR [r10+r11*8-16]
   mul   r9                               ; a[i+1] * bb1

   add   rcx, rbp                         ; rbp ~ input of2
   mov   rbp, rdx                         ; rbp ~ output of2
   adc   rbp, 0
   add   rcx, rax                         ; rcx ~ output mx
   adc   rbp, 0

   mov   rax, QWORD PTR [r10+r11*8-8]
   mov   r15, rax
   mul   r13                              ; a[i+2] * bb0

   add   rcx, r14                         ; r14 ~ input of3
   mov   r14, rdx                         ; r14 ~ output of3
   adc   r14, 0
   add   rcx, rax                         ; rcx ~ output mx
   adc   r14, 0
   mov   QWORD PTR [r12+r11*8-8], rcx

inner2:
   ; rdi is free
   mov   rax, rsi                         ; QWORD PTR [r10+r11*8-16]
   ; rsi is now free
   mul   r8                               ; a[i] * bb2
   mov   rcx, QWORD PTR [r12+r11*8]

   add   rcx, rbx                         ; rbx ~ input of1
   mov   rbx, rdx                         ; rbx ~ output of1
   adc   rbx, 0
   add   rcx, rax                         ; rcx ~ output mx
   adc   rbx, 0

   mov   rax, r15                         ; QWORD PTR [r10+r11*8-8]
   mul   r9                               ; a[i+1] * bb1

   add   rcx, rbp                         ; rbp ~ input of2
   mov   rbp, rdx                         ; rbp ~ output of2
   adc   rbp, 0
   add   rcx, rax                         ; rcx ~ output mx
   adc   rbp, 0

   mov   rax, QWORD PTR [r10+r11*8]
   mov   rdi, rax
   mul   r13                              ; a[i+2] * bb0

   add   rcx, r14                         ; r14 ~ input of3
   mov   r14, rdx                         ; r14 ~ output of3
   adc   r14, 0
   add   rcx, rax                         ; rcx ~ output mx
   adc   r14, 0
   mov   QWORD PTR [r12+r11*8], rcx

inner1:
   ; rsi is free
   mov   rax, r15                         ; QWORD PTR [r10+r11*8-8]
   ; r15 is now free
   mul   r8                               ; a[i] * bb2
   mov   rcx, QWORD PTR [r12+r11*8+8]

   add   rcx, rbx                         ; rbx ~ input of1
   mov   rbx, rdx                         ; rbx ~ output of1
   adc   rbx, 0
   add   rcx, rax                         ; rcx ~ output mx
   adc   rbx, 0

   mov   rax, rdi                         ; QWORD PTR [r10+r11*8]
   mul   r9                               ; a[i+1] * bb1

   add   rcx, rbp                         ; rbp ~ input of2
   mov   rbp, rdx                         ; rbp ~ output of2
   adc   rbp, 0
   add   rcx, rax                         ; rcx ~ output mx
   adc   rbp, 0

   mov   rax, QWORD PTR [r10+r11*8+8]
   mov   rsi, rax
   mul   r13                              ; a[i+2] * bb0

   add   rcx, r14                         ; r14 ~ input of3
   mov   r14, rdx                         ; r14 ~ output of3
   adc   r14, 0
   add   rcx, rax                         ; rcx ~ output mx
   adc   r14, 0
   mov   QWORD PTR [r12+r11*8+8], rcx

inner0:
   mov   rdx, alen$[rsp]                  ; rdx = alen
   lea   rax, QWORD PTR [r11+2]           ; r11 ~ i
   cmp   rax, rdx
   jb    inner_loop



;      of1 = ops<small>::muladdc(mx, a[i], bb2, of3, of1);  // of1:mx = a[i] * b[2] + of1 + of3
;      of2 = ops<small>::muladdc(rm[i+2], a[i+1], bb1, mx, of2);  // of2:rm[i+1] = a[i+1] * b[1] + mx + of2

   ; reload r15 with r = rm - j
   mov   rax, QWORD PTR j$1$[rsp]
   neg   rax
   lea   r15, QWORD PTR [r12+rax*8]       ; r12 ~ rm, r15 ~ r

   mov   rax, QWORD PTR [r10+r11*8]       ; a[i]
   mul   r8                               ; a[i] * bb2

   mov   rcx, rbx                         ; rbx ~ input of1
   add   rcx, r14                         ; r14 ~ input of3
   mov   rbx, rdx
   adc   rbx, 0
   add   rcx, rax                         ; rcx ~ mx
   adc   rbx, 0                           ; rbx ~ output of1

   mov   rax, QWORD PTR [r10+r11*8+8]     ; a[i+1]
   mul   r9                               ; a[i+1] * bb1

   add   rcx, rbp                         ; rbp ~ input of2
   mov   rbp, rdx
   adc   rbp, 0
   add   rcx, rax
   adc   rbp, 0                           ; rbp ~ output of2
   mov   QWORD PTR [r12+r11*8+16], rcx

;      of1 = ops<small>::muladdc(rm[i+3], a[i+1], bb2, of1, of2);  // of1:rm[i+2] = a[i+1] * b[2] + of1 + of2
;      rm[i+4] = of1;

   mov   rax, QWORD PTR [r10+r11*8+8]     ; a[i+1]
   mul   r8                               ; a[i+1] * bb2

   add   rbp, rbx
   mov   rbx, rdx
   adc   rbx, 0
   add   rbp, rax
   adc   rbx, 0
   mov   QWORD PTR [r12+r11*8+24], rbp
   mov   QWORD PTR [r12+r11*8+32], rbx

; 401  :       j += 3;

   mov   rbp, QWORD PTR j$1$[rsp]
   add   rbp, 3

   mov   r9, QWORD PTR blen$[rsp]
   mov   rbx, QWORD PTR b$[rsp]

   jmp   outer_main_loop



; 404  :    // lumpensammler: multiply what's still left
; 405  :    while (j < blen) {

   align 16
outer_leftover_loop:

; 406  :       rm = r + j;
; 407  :       const small bb = b[j];

   mov   rbx, QWORD PTR [rbx+rbp*8]       ; rbx = bb, rbx ~ b, rbp ~ j

; 411  :       UNROLL8(i, alen, 0, leftover, s);

   mov   rsi, alen$[rsp]                  ; alen
   mov   r9d, esi
   xor   ecx, ecx
   and   r9d, 7
   mov   rax, leftover_jump_table
   add   rax, QWORD PTR [rax+r9*8]
   lea   r11, QWORD PTR [r15+rbp*8]
   jmp   rax


   align 16
leftover_loop:
   mov   rax, QWORD PTR [r10+r9*8]
   mul   rbx
   mov   r8, QWORD PTR [r11+r9*8]

   add   r8, rcx
   mov   rcx, rdx
   adc   rcx, 0
   add   r8, rax
   adc   rcx, 0                           ; rcx ~ of
   mov   QWORD PTR [r11+r9*8], r8
   add   r9, 8

leftover7:
   mov   rax, QWORD PTR [r10+r9*8-56]
   mul   rbx
   mov   r8, QWORD PTR [r11+r9*8-56]

   add   r8, rcx
   mov   rcx, rdx
   adc   rcx, 0
   add   r8, rax
   adc   rcx, 0                           ; rcx ~ of
   mov   QWORD PTR [r11+r9*8-56], r8

leftover6:
   mov   rax, QWORD PTR [r10+r9*8-48]
   mul   rbx
   mov   r8, QWORD PTR [r11+r9*8-48]

   add   r8, rcx
   mov   rcx, rdx
   adc   rcx, 0
   add   r8, rax
   adc   rcx, 0                           ; rcx ~ of
   mov   QWORD PTR [r11+r9*8-48], r8

leftover5:
   mov   rax, QWORD PTR [r10+r9*8-40]
   mul   rbx
   mov   r8, QWORD PTR [r11+r9*8-40]

   add   r8, rcx
   mov   rcx, rdx
   adc   rcx, 0
   add   r8, rax
   adc   rcx, 0                           ; rcx ~ of
   mov   QWORD PTR [r11+r9*8-40], r8

leftover4:
   mov   rax, QWORD PTR [r10+r9*8-32]
   mul   rbx
   mov   r8, QWORD PTR [r11+r9*8-32]

   add   r8, rcx
   mov   rcx, rdx
   adc   rcx, 0
   add   r8, rax
   adc   rcx, 0                           ; rcx ~ of
   mov   QWORD PTR [r11+r9*8-32], r8

leftover3:
   mov   rax, QWORD PTR [r10+r9*8-24]
   mul   rbx
   mov   r8, QWORD PTR [r11+r9*8-24]

   add   r8, rcx
   mov   rcx, rdx
   adc   rcx, 0
   add   r8, rax
   adc   rcx, 0                           ; rcx ~ of
   mov   QWORD PTR [r11+r9*8-24], r8

leftover2:
   mov   rax, QWORD PTR [r10+r9*8-16]
   mul   rbx
   mov   r8, QWORD PTR [r11+r9*8-16]

   add   r8, rcx
   mov   rcx, rdx
   adc   rcx, 0
   add   r8, rax
   adc   rcx, 0                           ; rcx ~ of
   mov   QWORD PTR [r11+r9*8-16], r8

leftover1:
   mov   rax, QWORD PTR [r10+r9*8-8]
   mul   rbx
   mov   r8, QWORD PTR [r11+r9*8-8]

   add   r8, rcx
   mov   rcx, rdx
   adc   rcx, 0
   add   r8, rax
   adc   rcx, 0                           ; rcx ~ of
   mov   QWORD PTR [r11+r9*8-8], r8

leftover0:
   cmp   r9, rsi
   jb    leftover_loop


; 414  :       rm[i] = of1;
; 415  :       ++j;
; 416  :    }

   mov   rbx, QWORD PTR b$[rsp]
   mov   QWORD PTR [r11+r9*8], rcx
   mov   r9, QWORD PTR blen$[rsp]
   inc   rbp

outer_leftover_check:
   cmp   rbp, r9
   jb    outer_leftover_loop


no_leftover:
   mov   r14, QWORD PTR r14_save[rsp]
   mov   rbp, QWORD PTR rbp_save[rsp]
   mov   r12, QWORD PTR r12_save[rsp]
   mov   r13, QWORD PTR r13_save[rsp]

pop_exit:
   mov   r15, QWORD PTR r15_save[rsp]
   mov   rdi, QWORD PTR rdi_save[rsp]
   mov   rsi, QWORD PTR rsi_save[rsp]
   mov   rbx, QWORD PTR rbx_save[rsp]

   add   rsp, stackframe

   ret   0


   align 16
init_jump_table:
   DQ    init0 - init_jump_table
   DQ    init1 - init_jump_table
   DQ    init2 - init_jump_table
   DQ    init3 - init_jump_table
   DQ    init4 - init_jump_table
   DQ    init5 - init_jump_table
   DQ    init6 - init_jump_table
   DQ    init7 - init_jump_table

   align 16
inner_jump_table:
   DQ    start_inner0 - inner_jump_table
   DQ    start_inner1 - inner_jump_table
   DQ    start_inner2 - inner_jump_table

   align 16
leftover_jump_table:
   DQ    leftover0 - leftover_jump_table
   DQ    leftover1 - leftover_jump_table
   DQ    leftover2 - leftover_jump_table
   DQ    leftover3 - leftover_jump_table
   DQ    leftover4 - leftover_jump_table
   DQ    leftover5 - leftover_jump_table
   DQ    leftover6 - leftover_jump_table
   DQ    leftover7 - leftover_jump_table

?omul4_asmx64@bignum@@YAXPEA_KPEB_K_K12@Z ENDP

_TEXT ENDS
END
